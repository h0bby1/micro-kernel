[ORG 0x10000]
%include "mem_util.inc"
[BITS 16]
start_img	:
jmp start_16


testword: DW 0x55AA   
test_float: dd 0
test_int: dd 0


text_kern_imp_dll							:db 'kern_imp',0
text_kern_imp_fn_0							:db 'read_pci_config_c',0
text_kern_imp_fn_1							:db 'draw_car_c',0
text_kern_imp_fn_2							:db	'set_text_color_c',0
text_kern_imp_fn_3							:db	'get_text_color_c',0
text_kern_imp_fn_4							:db 'read_pci_dword_c',0
text_kern_imp_fn_5							:db 'write_pci_dword_c',0
text_kern_imp_fn_6							:db 'read_pci_word_c',0
text_kern_imp_fn_7							:db 'write_pci_word_c',0
text_kern_imp_fn_8							:db 'read_pci_byte_c',0
text_kern_imp_fn_9							:db 'write_pci_byte_c',0
text_kern_imp_fn_10							:db 'delay1_4ms_c',0
text_kern_imp_fn_11							:db 'delay_x_ms_c',0
text_kern_imp_fn_12							:db 'get_system_time_c',0
text_kern_imp_fn_13							:db 'in_32_c',0
text_kern_imp_fn_14							:db 'out_32_c',0
text_kern_imp_fn_15							:db 'in_16_c',0
text_kern_imp_fn_16							:db 'out_16_c',0
text_kern_imp_fn_17							:db 'in_8_c',0
text_kern_imp_fn_18							:db 'out_8_c',0
text_kern_imp_fn_19							:db 'pci_enable_device_c',0
text_kern_imp_fn_20							:db 'tpo_get_fn_entry_idx_c',0
text_kern_imp_fn_21							:db 'tpo_get_fn_entry_hash_c',0
text_kern_imp_fn_22							:db 'tpo_get_mod_entry_hash_c',0
text_kern_imp_fn_23							:db  'draw_dword_c',0
text_kern_imp_fn_24							:db 'setup_interupt_c',0
text_kern_imp_fn_25							:db 'calc_crc32_c',0
text_kern_imp_fn_26							:db 'kernel_memory_map_c',0
text_kern_imp_fn_27							:db 'tpo_mod_imp_func_addr_c',0
text_kern_imp_fn_28							:db 'tpo_mod_add_func_addr_c',0
text_kern_imp_fn_29							:db 'draw_ncar_c',0	
text_kern_imp_fn_31							:db 'create_code_stub',0
text_kern_imp_fn_32							:db 'bios_reset_drives_c',0
text_kern_imp_fn_33							:db 'bios_read_sector_c',0
text_kern_imp_fn_34							:db	'bios_get_sector_data_ptr_c',0
text_kern_imp_fn_35							:db 'bios_get_vesa_info_c',0
text_kern_imp_fn_36							:db 'bios_get_vesa_info_ptr_c',0
text_kern_imp_fn_37							:db 'bios_get_vesa_mode_info_c',0
text_kern_imp_fn_38							:db 'bios_get_vesa_mode_info_ptr_c',0
text_kern_imp_fn_39							:db 'bios_set_vesa_mode_c',0	
text_kern_imp_fn_40							:db 'toggle_output_buffer_c',0	
text_kern_imp_fn_41							:db 'get_output_buffer_line_c',0	
text_kern_imp_fn_42							:db 'set_output_buffer_addr_c',0	
text_kern_imp_fn_43							:db 'get_num_output_buffer_line_c',0	
text_kern_imp_fn_44							:db 'bios_enable_mouse_c',0	
text_kern_imp_fn_45							:db 'bios_reset_mouse_c',0	
text_kern_imp_fn_46							:db 'bios_get_mouse_status_c',0	
text_kern_imp_fn_47							:db 'set_char_buffer_ofset_c',0
text_kern_imp_fn_48							:db 'get_char_buffer_ofset_c',0
text_kern_imp_fn_49							:db 'bios_get_vga_info_c',0
text_kern_imp_fn_50							:db 'bios_set_vga_mode_c',0
text_kern_imp_fn_51							:db 'set_mmx_protect_c',0
text_kern_imp_fn_52							:db 'read_bios_disk_geom_c',0
text_kern_imp_fn_53							:db 'bios_get_boot_device_c',0
text_kern_imp_fn_54							:db 'bios_get_mem_map_c',0
text_kern_imp_fn_55							:db 'bios_get_edba_c',0
text_kern_imp_fn_56							:db 'get_gdt_ptr_c',0;
text_kern_imp_fn_57							:db 'set_gdt_ptr_c',0;				  ,
text_kern_imp_fn_58							:db 'do_bios_interupt_c',0;
text_kern_imp_fn_59							:db 'rep_in_byte',0;
text_kern_imp_fn_60							:db 'rep_in_word',0;
text_kern_imp_fn_61							:db 'rep_in_dword',0;
text_kern_imp_fn_62							:db 'rep_out_byte',0;
text_kern_imp_fn_63							:db 'rep_out_word',0
text_kern_imp_fn_64							:db 'rep_out_dword',0;
text_kern_imp_fn_65							:db 'draw_byte_c',0;
text_kern_imp_fn_66							:db 'tpo_add_mod_c',0;
text_kern_imp_fn_67							:db 'tpo_mod_add_section_c',0;
text_kern_imp_fn_68							:db 'tpo_mod_add_func_c',0;
text_kern_imp_fn_69							:db 'tpo_get_fn_entry_name_c',0;
text_kern_imp_fn_70							:db 'tpo_calc_exp_func_hash_c',0;
text_kern_imp_fn_71							:db 'tpo_calc_exp_func_hash_name_c',0;
text_kern_imp_fn_72							:db 'tpo_calc_imp_func_hash_name_c',0;
text_kern_imp_fn_73							:db 'next_task_c',0;
text_kern_imp_fn_74							:db 'dump_task_infos_c',0
text_kern_imp_fn_75							:db 'tpo_get_mod_entry_idx_c',0
text_kern_imp_fn_76							:db 'tpo_get_mod_sec_idx_c',0
text_kern_imp_fn_77							:db 'pic_init_c',0
text_kern_imp_fn_78							:db 'pic_set_irq_mask_c',0
text_kern_imp_fn_79							:db 'get_cpu_frequency_c',0
text_kern_imp_fn_80							:db 'dump_ivt_c',0
text_kern_imp_fn_81							:db 'pic_enable_irq_c',0
text_kern_imp_fn_82							:db 'compare_z_exchange_c',0
text_kern_imp_fn_83							:db 'swap_nz_array_c',0
text_kern_imp_fn_84							:db 'set_cpu_flags_c',0
text_kern_imp_fn_85							:db 'cur_task_id',0
text_kern_imp_fn_86							:db 'fetch_add_c',0
text_kern_imp_fn_87							:db 'acpi_get_rdsp_c',0

text_kern_imp_fn_88							:db 'apic_enable_irq_c',0
text_kern_imp_fn_89							:db 'init_lapic_c',0
text_kern_imp_fn_90							:db 'init_apic_c',0
text_kern_imp_fn_91							:db 'disable_apic_c',0

text_kern_imp_fn_92							:db 'get_interupt_mode_c',0




text_lib_c_dll								:db 'lib_c',0
text_lib_c_task_manager_next_task			:db 'task_manager_next_task',0
text_lib_c_task_manager_dump_task_post		:db 'task_manager_dump_task_post',0
text_lib_c_task_manager_dump_tpo_mod_infos	:db 'task_manager_dump_tpo_mod_infos',0

text_lib_c_interupt_end_callback			:db	'interupt_end_callback',0
text_lib_c_interupt_start_callback			:db 'interupt_start_callback',0

text_sys_stream_dll							:db 'sys_stream',0
text_lib_c_sys_init							:db 'sys_init',0

text2										:db 'kernel kick start !!',10,0
text3										:db 'detecting memory mapping',10,0
text_rsdt									:db ' rsdt entry ',0
text_acpi_rdst								:db ' acpi rdst ',0


text_nl										:db 10,0
bios_boot_device							:db 0
bios_edba									:dd 0
bios_edba_size								:dd 0

rsd_ptr										:dd 0x0
rsdt_ptr									:dd 0x0
memory_map_result							:times 20*64 db 0
num_mem_map_entry							:dw 0

IDTBASE:	times 1024*4	dd 0
gdt:
gdt_null:			; 0x0
	dw 0,0,0,0		
gdt_cs:				; 0x8	
	dw 0,0,0,0
gdt_ds:  			; 0x10
	dw 0,0,0,0
gdt_ss:  			; 0x18
	dw 0,0,0,0
gdt_screen:			; 0x20
	dw 0,0,0,0	
gdt_text:			; 0x28
	dw 0,0,0,0	
gdt_vga_bios_code:  ; 0x30
	dw 0,0,0,0	
gdt_vga_bios:  		; 0x38
	dw 0,0,0,0	
gdt_memory_zero:	; 0x40
	dw 0,0,0,0	
gdt_r_ss:			; 0x48
	dw 0,0,0,0		
gdt_bios_return:	; 0x50
	dw 0,0,0,0
gdtend:

gdtptr		: dw 0x0000
dd 0
idtptr		: dw 0x0000
dd 0
orig_idt	: dw 0x0000
dd 0

start_16	:

	mov ax,0x1000
	mov es,ax
	mov ds,ax


	mov [bios_boot_device addr_to_real_mode],dl
	
	lea esi,[text2 addr_to_real_mode]

	msg_print_1:
		lodsb
		test al,al
		jz msg_done_1
		mov bl,7
		mov ah,0eh
		int 10h
	jmp msg_print_1
    msg_done_1:


		
	mov ax,0x40
	mov ds,ax
	mov si,0x0E
	lodsw
	
	movzx ebx,ax
	shl   ebx,4

	
	mov ax,0x1000
	mov ds,ax

	mov [bios_edba addr_to_real_mode],ebx
	
	mov word [num_mem_map_entry addr_to_real_mode],0
	
	xor ebx,ebx
	lea edi ,[memory_map_result addr_to_real_mode]
	sti
	loop_bios_mem_map:
	
		mov eax,0x0000E820
		mov edx,0x534D4150
		mov ecx,20
		
		int 15H
		jc bios_mem_map_done
		
		cmp eax,0x534D4150 
		jne bios_mem_map_done


		cmp DWORD [edi] , bios_edba addr_to_real_mode
		jne next_map
			mov eax, [edi + 8]
			mov [bios_edba_size addr_to_real_mode], eax
		next_map:
		
		test ebx,ebx
		jz bios_mem_map_done
		
		inc word [num_mem_map_entry addr_to_real_mode]
		
		add edi,ecx
	jmp loop_bios_mem_map
	
	bios_mem_map_done:
	
	lea esi,[text3 addr_to_real_mode]

	msg_print_2:
		lodsb
		test al,al
		jz msg_done_2
		mov bl,7
		mov ah,0eh
		int 10h
	jmp msg_print_2
    msg_done_2:
    
    cli





	

; ouverture de la porte a20
a20:
	in al,0x64	; teste si on peut envoyer une commande
	test al,2
	jnz a20

	mov al,0xD1
	out 0x64,al
	jmp .1		; temporisation
.1:
	in al,0x64	; teste si on peut envoyer une commande
	test al,2
	jnz .1

	mov al,0xDF
	out 0x60,al
a20end:
    

;										 Gr/16|32bits
descInit start_seg				,0xFFFFF      ,CS_ACCES,1101b,(gdt_cs addr_to_real_mode)
descInit start_seg				,0xFFFFF      ,DS_ACCES,1101b,(gdt_ds addr_to_real_mode)
descInit start_seg				,0xFFFFF	  ,SS_ACCES,1101b,(gdt_ss addr_to_real_mode)
descInit 0xe8000000				,0xFFFFF      ,DS_ACCES,1101b,(gdt_screen addr_to_real_mode)			; ram ecran
descInit 0xB8000				,4000         ,DS_ACCES,0101b,(gdt_text	addr_to_real_mode)				; ram ecran

descInit kern_base				,0xFFFFF	   ,CS_ACCES,1001b,(gdt_vga_bios_code addr_to_real_mode)	 
descInit kern_base				,0xFFFFF	   ,DS_ACCES,1001b,(gdt_vga_bios addr_to_real_mode)			 

descInit kern_base				,0xFFFFF	   ,CS_ACCES,1101b,(gdt_bios_return addr_to_real_mode)		 
descInit REAL_MODE_STACK_START	,0xFFFFF	   ,SS_ACCES,0001b,(gdt_r_ss addr_to_real_mode)

; initialisation du pointeur sur la gdt

lea ax									,[gdtend-gdt]			; calcule la limite de gdt
mov word [gdtptr addr_to_real_mode]		,ax
mov dword [gdtptr addr_to_real_mode+2]	,gdt addr_to_real_mode
lgdt [gdtptr addr_to_real_mode]									; charge la gdt

mov eax,cr0
or  ax ,1
mov cr0,eax				; PE mis a 1 (CR0)



mov ax		,0x10		; segment de donne
mov es		,ax
mov ds		,ax
mov fs		,ax
mov gs		,ax



;*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=
jmp dword 0x08:start_32
;*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=



[BITS 32]

%include "print_put.inc"
%include "real_mode.inc"
%include "exceptions.inc"


sys_init_fn_addr						:dd 0 

%include "util.inc"
%include "pic.inc"
%include "apic.inc"
%include "crc32.inc"
%include "pci_type1.inc"
%include "pci_type2.inc"
%include "pci_common.inc"
%include "pci.inc"
%include "print.inc"
%include "tpo.inc"
%include "irq_hdl.inc"
%include "task.inc"

_dump_ivt_c:
pusha
	%assign i 0 
	%rep    32

		mov ecx,i
		call _draw_dword_hex

		mov edx,' '
		call _draw_car  
		
		mov ecx,dword [0x0+i*4]				;	get real mode interupt vector ofset from ivt
		call _draw_dword_hex

		mov edx,10
		call _draw_car  

	%assign i i+1 
	%endrep
		call _output_buffer
popa
ret


_find_acpi:

	mov esi, [bios_edba]
	mov ecx, [bios_edba_size]
	lea ecx, [esi+ecx]

	loop_rsd:
	 
		cmp dword [esi], 0x20445352    
		jne next_rsd1

			cmp dword [esi], 0x20445352
			jne next_rsd2

				mov [rsdt_ptr], esi

				mov eax,[esi+16]
				mov [rsd_ptr], eax
				jmp rsd_end

			next_rsd2:

		next_rsd1:
		
	add esi,16
	cmp esi,ecx
	jle loop_rsd

	cmp eax,1
	je rsd_end

	mov esi,0x000E0000
	mov ecx,0x1FFFF
	mov eax,1
	jmp loop_rsd

	rsd_end:

ret



 ;for(len = *((uint32_t*)(rsdt + 4)), ptr2 = rsdt + 36; ptr2 < rsdt + len; ptr2 += rsdt[0]=='X' ? 8 : 4) {
  ;  ptr = (uint8_t*)(uintptr_t)(rsdt[0]=='X' ? *((uint64_t*)ptr2) : *((uint32_t*)ptr2));
  ;  if(!memcmp(ptr, "APIC", 4)) {
  ;    // found MADT
  ;    lapic_ptr = (uint64_t)(*((uint32_t)(ptr+0x24)));
  ;    ptr2 = ptr + *((uint32_t*)(ptr + 4));
  ;   // iterate on variable length records
  ;   for(ptr += 44; ptr < ptr2; ptr += ptr[1]) {
  ;     switch(ptr[0]) {
  ;       case 0: if(ptr[4] & 1) lapic_ids[numcore++] = ptr[3]; break; // found Processor Local APIC
  ;       case 1: ioapic_ptr = (uint64_t)*((uint32_t*)(ptr+4); break;  // found IOAPIC
  ;       case 5: lapic_ptr = *((uint64_t*)(ptr+4); break;             // found 64 bit LAPIC
  ;     }
  ;   }
  ;   break;
  ; }
  ;}

ret

;*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=
;*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=
start_32:
;*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=
;*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=

mov ax		,0x18		; segment de pile
mov ss		,ax
mov esp		,0x200000
mov ebp		,esp

sidt	[orig_idt]

push (64*256*16)
call _kernel_memory_map_c
mov [sys_tpo_mod_exp_funcs_ptr],eax

lea esi						,	[text_kern_imp_dll]
mov ecx						,	64
call calc_crc32

push dword text_kern_imp_dll
push dword 0
push eax
call _tpo_add_mod_c
add esp,12

push dword kernel_image_end-kern_org
push dword kern_org
push dword 0
call _tpo_mod_add_section_c
add esp,12

set_op_code_str STI_OP_CODE,'sti'
set_op_code_str PUSH_OP_CODE,'push'
set_op_code_str POP_OP_CODE,'pop'

set_op_code_str JMP_REL8_OP_CODE,'jmp'
set_op_code_str JMP_REL_OP_CODE,'jmp'


set_op_code_str CALLF_OP_CODE,'callf'
set_op_code_str CALL_OP_CODE,'call'



set_op_code_str MOV_IMM_OP_CODE,'mov'

set_op_code_str INC_REG_OP_CODE,'inc'
set_op_code_str DEC_REG_OP_CODE,'dec'

set_op_code_str CMP_REG_TO_MEM_OP_CODE,'cmp'
set_op_code_str CMP_AL_IMM_OP_CODE,'cmp'
set_op_code_str CMP_AX_IMM_OP_CODE,'cmp'
set_op_code_str MOVE_REG_TO_OP_CODE,'mov'
set_op_code_str MOVE_TO_SEG_OP_CODE,'mov'
set_op_code_str MOV_IMM_TO_REG_OP_CODE,'mov'

set_op_code_str ADD_IM8_TO_REG_OP_CODE,'add'
set_op_code_str MOVE_IM8_TO_REG_OP_CODE,'mov'
set_op_code_str MOV_IMM8_OP_CODE,'mov'
set_op_code_str MOVE_MEM_TO_REG_OP_CODE,'mov'


set_op_code_str POPF_OP_CODE,'popf'
set_op_code_str POPA_OP_CODE,'popa'
set_op_code_str PUSHA_OP_CODE,'pusha'
set_op_code_str PUSH_IM8_OP_CODE,'push'

set_op_code_str JNE_IM8_OP_CODE,'jne'


set_op_code_str POP_SS_OP_CODE,'pop ss'
set_op_code_str POP_DS_OP_CODE,'pop ds'
set_op_code_str POP_ES_OP_CODE,'pop es'
set_op_code_str MOVE_AX_TO_OFSET_OP_CODE,'mov'
set_op_code_str MOVE_AL_TO_OFSET_OP_CODE,'mov'


set_op_code_str MOVE_OFSET_TO_AX_OP_CODE,'mov'
set_op_code_str RET_OP_CODE,'ret'
set_op_code_str IN_AL_DX_OP_CODE,'in al,dx'

idt_init_expt 0x00,IDTBASE+8*0
idt_init_expt 0x01,IDTBASE+8*1
idt_init_expt 0x02,IDTBASE+8*2
idt_init_expt 0x03,IDTBASE+8*3
idt_init_expt 0x04,IDTBASE+8*4
idt_init_expt 0x05,IDTBASE+8*5
idt_init_expt 0x06,IDTBASE+8*6
idt_init_expt 0x07,IDTBASE+8*7
idt_init_expt 0x08,IDTBASE+8*8
idt_init_expt 0x09,IDTBASE+8*9
idt_init_expt 0x0a,IDTBASE+8*10
idt_init_expt 0x0b,IDTBASE+8*11
idt_init_expt 0x0c,IDTBASE+8*12
idt_init_expt 0x0d,IDTBASE+8*13
idt_init_expt 0x0e,IDTBASE+8*14
idt_init_expt 0x0F,IDTBASE+8*15





idt_init_real_int 0x10	,IDTBASE+8*16
idt_init_real_int 0x13	,IDTBASE+8*19
idt_init_real_int 0x15  ,IDTBASE+8*21


idt_init_int 0x00,IDTBASE+8*32 
idt_init_int 0x01,IDTBASE+8*33
idt_init_int 0x02,IDTBASE+8*34
idt_init_int 0x03,IDTBASE+8*35
idt_init_int 0x04,IDTBASE+8*36
idt_init_int 0x05,IDTBASE+8*37
idt_init_int 0x06,IDTBASE+8*38
idt_init_int 0x07,IDTBASE+8*39
idt_init_int 0x08,IDTBASE+8*40
idt_init_int 0x09,IDTBASE+8*41
idt_init_int 0x0A,IDTBASE+8*42
idt_init_int 0x0B,IDTBASE+8*43
idt_init_int 0x0C,IDTBASE+8*44
idt_init_int 0x0D,IDTBASE+8*45
idt_init_int 0x0E,IDTBASE+8*46
idt_init_int 0x0F,IDTBASE+8*47

idt_init_int 0x10,IDTBASE+8*48
idt_init_int 0x11,IDTBASE+8*49
idt_init_int 0x12,IDTBASE+8*50
idt_init_int 0x13,IDTBASE+8*51
idt_init_int 0x14,IDTBASE+8*52
idt_init_int 0x15,IDTBASE+8*53
idt_init_int 0x16,IDTBASE+8*54
idt_init_int 0x17,IDTBASE+8*55
idt_init_int 0x18,IDTBASE+8*56
idt_init_int 0x19,IDTBASE+8*57
idt_init_int 0x1A,IDTBASE+8*58
idt_init_int 0x1B,IDTBASE+8*59
idt_init_int 0x1C,IDTBASE+8*60
idt_init_int 0x1D,IDTBASE+8*61
idt_init_int 0x1E,IDTBASE+8*62
idt_init_int 0x1F,IDTBASE+8*63



	
mov word  [idtptr]       ,8*256
mov dword [idtptr+2]	 ,IDTBASE addr_to_real_mode
lidt [idtptr]	        ; charge la idt

jmp 0x08:next
next:

  mov		esi,text1
  call		_draw_cars
  


  mov edx,' '
  call _draw_car  
  
  call      delay1_4ms  
		
  mov edx,10
  call _draw_car
  

  add_kernel_func _read_pci_config_c			  , text_kern_imp_fn_0
  add_kernel_func _draw_car_c					  , text_kern_imp_fn_1
  add_kernel_func _set_text_color_c				  , text_kern_imp_fn_2
  add_kernel_func _get_text_color_c				  , text_kern_imp_fn_3
  add_kernel_func _read_pci_dword_c				  , text_kern_imp_fn_4
  add_kernel_func _write_pci_dword_c			  , text_kern_imp_fn_5
  add_kernel_func _read_pci_word_c				  , text_kern_imp_fn_6
  add_kernel_func _write_pci_word_c				  , text_kern_imp_fn_7
  add_kernel_func _read_pci_byte_c				  , text_kern_imp_fn_8
  add_kernel_func _write_pci_byte_c				  , text_kern_imp_fn_9
  add_kernel_func _delay1_4ms_c					  , text_kern_imp_fn_10
  add_kernel_func _delay_x_ms_c					  , text_kern_imp_fn_11
  add_kernel_func _get_system_time_c			  , text_kern_imp_fn_12
  add_kernel_func _in_32_c						  , text_kern_imp_fn_13
  add_kernel_func _out_32_c						  , text_kern_imp_fn_14
  add_kernel_func _in_16_c						  , text_kern_imp_fn_15
  add_kernel_func _out_16_c						  , text_kern_imp_fn_16  
  add_kernel_func _in_8_c						  , text_kern_imp_fn_17  
  add_kernel_func _out_8_c						  , text_kern_imp_fn_18    
  add_kernel_func _pci_enable_device_c			  , text_kern_imp_fn_19      

  add_kernel_func _tpo_get_fn_entry_idx_c		 ,	text_kern_imp_fn_20
  add_kernel_func _tpo_get_fn_entry_hash_c		 , 	text_kern_imp_fn_21
  add_kernel_func _tpo_get_mod_entry_hash_c		 ,	text_kern_imp_fn_22
  
  add_kernel_func _draw_dword_c					 ,	text_kern_imp_fn_23


  add_kernel_func _setup_interupt_c				  , text_kern_imp_fn_24
  add_kernel_func _calc_crc32_c					  , text_kern_imp_fn_25
  add_kernel_func _kernel_memory_map_c			  , text_kern_imp_fn_26
  add_kernel_func _tpo_mod_imp_func_addr_c		  , text_kern_imp_fn_27
  add_kernel_func _tpo_mod_add_func_addr_c		  , text_kern_imp_fn_28
  
  add_kernel_func _draw_ncar_c					 ,	text_kern_imp_fn_29

  add_kernel_func _create_code_stub				  , text_kern_imp_fn_31
  
  add_kernel_func _bios_reset_drives_c			  , text_kern_imp_fn_32
  add_kernel_func _bios_read_sector_c			  , text_kern_imp_fn_33
  add_kernel_func _bios_get_sector_data_ptr_c	  , text_kern_imp_fn_34
  
  
  add_kernel_func _bios_get_vesa_info_c			  ,	text_kern_imp_fn_35 
  add_kernel_func _bios_get_vesa_info_ptr_c		  ,	text_kern_imp_fn_36 

  add_kernel_func _bios_get_vesa_mode_info_c	  ,	text_kern_imp_fn_37 
  add_kernel_func _bios_get_vesa_mode_info_ptr_c  ,	text_kern_imp_fn_38 
  
  add_kernel_func _bios_set_vesa_mode_c			  ,	text_kern_imp_fn_39 

  add_kernel_func _toggle_output_buffer_c		  , text_kern_imp_fn_40		
  add_kernel_func _get_output_buffer_line_c		  , text_kern_imp_fn_41		
  add_kernel_func _set_output_buffer_addr_c		  , text_kern_imp_fn_42		
  add_kernel_func _get_num_output_buffer_line_c	  , text_kern_imp_fn_43
  
  add_kernel_func _bios_enable_mouse_c			  , text_kern_imp_fn_44
  add_kernel_func _bios_reset_mouse_c			  , text_kern_imp_fn_45
  add_kernel_func _bios_get_mouse_status_c		  , text_kern_imp_fn_46
  

  add_kernel_func _set_char_buffer_ofset_c		  , text_kern_imp_fn_47
  add_kernel_func _get_char_buffer_ofset_c		  , text_kern_imp_fn_48
  
  add_kernel_func _bios_get_vga_info_c			  ,	text_kern_imp_fn_49 
  
  add_kernel_func _bios_set_vga_mode_c			  ,	text_kern_imp_fn_50
  
  add_kernel_func _set_mmx_protect_c			  ,	text_kern_imp_fn_51
  
  add_kernel_func _read_bios_disk_geom_c		  ,	text_kern_imp_fn_52
    
  add_kernel_func _bios_get_boot_device_c		  ,text_kern_imp_fn_53
  
  add_kernel_func _bios_get_mem_map_c			  ,text_kern_imp_fn_54
  
  add_kernel_func _bios_get_edba_c				  ,text_kern_imp_fn_55
  
  add_kernel_func _get_gdt_ptr_c				  ,text_kern_imp_fn_56
  
  add_kernel_func _set_gdt_ptr_c				  ,text_kern_imp_fn_57
  
  add_kernel_func _do_bios_interupt_c			  ,text_kern_imp_fn_58

  add_kernel_func _rep_in_byte					  ,text_kern_imp_fn_59
  
  add_kernel_func _rep_in_word					  ,text_kern_imp_fn_60
  
  add_kernel_func _rep_in_dword					  ,text_kern_imp_fn_61

  add_kernel_func _rep_out_byte					  ,text_kern_imp_fn_62
  
  add_kernel_func _rep_out_word					  ,text_kern_imp_fn_63
  
  add_kernel_func _rep_out_dword				  ,text_kern_imp_fn_64
  
  add_kernel_func _draw_byte_c					  ,text_kern_imp_fn_65
  
  add_kernel_func _tpo_add_mod_c				  ,text_kern_imp_fn_66
  
  add_kernel_func _tpo_mod_add_section_c		  ,text_kern_imp_fn_67
  
  add_kernel_func _tpo_mod_add_func_c			  ,text_kern_imp_fn_68
  
  add_kernel_func _tpo_get_fn_entry_name_c		  ,text_kern_imp_fn_69
  
  add_kernel_func _tpo_calc_exp_func_hash_c		  ,text_kern_imp_fn_70
  
  add_kernel_func _tpo_calc_exp_func_hash_name_c  ,text_kern_imp_fn_71
  
  add_kernel_func _tpo_calc_imp_func_hash_name_c  ,text_kern_imp_fn_72
  
  add_kernel_func _next_task_c					  ,text_kern_imp_fn_73
  
  add_kernel_func _dump_task_infos_c			  ,text_kern_imp_fn_74
  
  add_kernel_func _tpo_get_mod_entry_idx_c		  ,text_kern_imp_fn_75
  
  add_kernel_func _tpo_get_mod_sec_idx_c		  ,text_kern_imp_fn_76			
  
  add_kernel_func _pic_init_c					  ,text_kern_imp_fn_77
  
  add_kernel_func _pic_set_irq_mask_c			  ,text_kern_imp_fn_78
  
  add_kernel_func _get_cpu_frequency_c			  ,text_kern_imp_fn_79
  
  add_kernel_func _dump_ivt_c					  ,text_kern_imp_fn_80
  add_kernel_func _pic_enable_irq_c				  ,text_kern_imp_fn_81
  add_kernel_func _compare_z_exchange_c			  ,text_kern_imp_fn_82
  add_kernel_func _swap_nz_array_c				  ,text_kern_imp_fn_83
  
  add_kernel_func _set_cpu_flags_c				  ,text_kern_imp_fn_84
  
  add_kernel_func cur_task_id					  ,text_kern_imp_fn_85
  add_kernel_func _fetch_add_c					  ,text_kern_imp_fn_86

  add_kernel_func _acpi_get_rdsp_c				  ,text_kern_imp_fn_87
  
  add_kernel_func _apic_enable_irq_c			  ,text_kern_imp_fn_88
  add_kernel_func _init_lapic_c					  ,text_kern_imp_fn_89
  add_kernel_func _init_apic_c					  ,text_kern_imp_fn_90
  add_kernel_func _disable_apic_c				  ,text_kern_imp_fn_91
  add_kernel_func _get_interupt_mode_c			  ,text_kern_imp_fn_92

  	
  	
  	

  MOV EDX, CR0                  ; Start probe, get CR0
  AND EDX, (-1) - (2 + 4)       ; clear TS and EM to force fpu access
  MOV CR0, EDX 

  FNINIT                        ; load defaults to FPU
  FNSTSW [testword] 
  
  lea eax							,	[binary_data_lib_c]
  mov [tpo_file_binary_data_ptr]	,	eax
  call load_tpo_funcs 
 
  lea eax							,	[binary_data_zlib]
  mov [tpo_file_binary_data_ptr]	,	eax
  call load_tpo_funcs 	

  lea eax							,	[binary_data_sys_stream]
  mov [tpo_file_binary_data_ptr]	,	eax
  call load_tpo_funcs 
  
  push dword 0
  push dword text_lib_c_task_manager_next_task
  push dword text_lib_c_dll
  call sys_get_tpo_mod_func_name
  add  esp,12
  mov dword [task_manager_next_task_func],eax  
  
  push dword 0
  push dword text_lib_c_interupt_start_callback
  push dword text_lib_c_dll
  call sys_get_tpo_mod_func_name
  add  esp,12
  mov dword [interupt_start_callback_func],eax
  
  push dword 0
  push dword text_lib_c_interupt_end_callback
  push dword text_lib_c_dll
  call sys_get_tpo_mod_func_name
  add  esp,12
  mov dword [interupt_end_callback_func],eax
   
  push dword 0
  push dword text_lib_c_task_manager_dump_task_post
  push dword text_lib_c_dll
  call sys_get_tpo_mod_func_name
  add  esp,12
  mov dword [task_manager_dump_task_post_func],eax    
  

  push dword 0
  push dword text_lib_c_task_manager_dump_tpo_mod_infos
  push dword text_lib_c_dll
  call sys_get_tpo_mod_func_name
  add  esp,12
  mov dword [task_manager_dump_tpo_mod_infos_func],eax    

  
  
  push dword 0
  push dword text_lib_c_sys_init
  push dword text_sys_stream_dll
  call sys_get_tpo_mod_func_name
  add  esp,12
  mov dword [sys_init_fn_addr],eax
  
  	
  call _pci_detect_conf_type
  call _pci_display_conf_type
  
  mov  eax,0x00
  mov  edx,user_timer_hndl
  call set_user_ihdl
  
  mov  eax,0x01
  mov  edx,user_keybrd_hndl
  call set_user_ihdl

  ;One commonly performed task is setting the frequency of the channel 0, the system timer. 
  ;If a frequency of 100 hz is desired, we see that the necessary divisor is 1193182 / 100 = 11931. 
  ;This value must be sent to the PIT split into a high and low byte.

  mov al		, 0x36
  out 0x43		, al		;tell the PIT which channel we're setting
 
  mov ax    	, 11931		;100 hz pit
  out 0x40  	, al		;send low byte
  mov al		, ah
  out 0x40  	, al		;send high byte  
  
  xor eax,eax
  CPUID
  RDTSC
  mov [start_cpu_tick]	, eax
  mov [start_cpu_tick+4], edx    
    
  call _find_acpi

  push dword text_acpi_rdst
  call _draw_car_c
  add  esp,4

  push dword [rsdt_ptr]
  call _draw_dword_c
  add  esp,4

  push dword text_nl
  call _draw_car_c
  add  esp,4

  cmp dword [rsd_ptr],0
  je _kernel_rsd_ptr_null
	  call _detect_apic
  _kernel_rsd_ptr_null:

  ;call _init_pic
  ;call _init_apic
  ;call _init_lapic
  
  ;mov edx, 2
  ;mov ebx, 32
  ;mov ecx, 0

  ;call _apic_enable_irq

  ;mov edx, 1
  ;mov ebx, 33
  ;mov ecx, 0

  ;call _apic_enable_irq



  mov ecx,[sys_init_fn_addr]	    
  call _draw_dword_hex
	    	    
  mov edx,10
  call _draw_car 

  xor  ebp,ebp
  push ebp
		    
  call dword [sys_init_fn_addr]

pouet:
    nop 
jmp pouet



text1: db 'welcome in protected mode !!',10,0


cur_task_id:dd 0xFFFFFFFF

multi_card: dd 4
num_int:dd 0
;timer_count:dd 0




end_line						:db 10,0


cpu_frequency				:dd 0
last_cpu_tick				:dq 0
start_cpu_tick				:dq 0
num_pit_calls				:dd 0
interupt_mode				:dd 0x0

_get_cpu_frequency_c:

	loop_freq:
		mov  eax,[cpu_frequency]
		test eax,eax
	jz loop_freq
	
ret

user_timer_hndl:
	
	
	inc dword [num_pit_calls]
	cmp dword [num_pit_calls],100
	jle no_freq_comp
	
		cmp dword 	[cpu_frequency],0
		jne no_cp
			pushfd
			cli
			pusha 

			xor eax,eax
			CPUID
			RDTSC

			mov ebx					, [last_cpu_tick]
			mov ecx					, [last_cpu_tick+4]
			mov [last_cpu_tick]		, eax
			mov [last_cpu_tick+4]	, edx
			
			cmp ebx					,0
			je user_timer_hndl_no_last

			sub eax					, ebx					; Calculate TSC
			sbb edx					, ecx
					
			mov ebx					, 1000
			div ebx
			
			mov	[cpu_frequency]		, eax
			
			
			user_timer_hndl_no_last:
					
			popa
			popfd
		no_cp:
		
		mov dword [num_pit_calls],0
	no_freq_comp:
	
	
	
ret 

user_keybrd_hndl:

	;wait_key_buffer:
	;	in	 al,0x64
	;	test al,1
	;jz  wait_key_buffer
		
	mov byte [value+0]	 ,' '
	mov byte [value+1]	 , 0x07
	_put_car
		
	in al				 , 0x60
	test	al			 , 0x80
	jnz not_pressed
		and al				 ,~0x80
		mov byte [hex_value] , al
		call _put_byte_hex	
		
		mov byte [hex_value+1],0		
		mov byte [hex_value+2],0
		mov byte [hex_value+3],0
		
		cmp	 byte [hex_value],0x48
		jne not_scroll_text_down
				
		mov		edx,dword [char_buffer_offset]
		test	edx,edx
		jz not_scroll_text_down
			dec dword [char_buffer_offset]
			call _output_buffer
		not_scroll_text_down:
			
		
		cmp	 byte [hex_value],0x50
		jne not_scroll_text_up
			inc dword [char_buffer_offset]
			call _output_buffer
		not_scroll_text_up:
		
		jmp end_key_test
	not_pressed:


	end_key_test:
ret 



_bios_get_edba_c:
	 mov eax,[bios_edba]
ret

_acpi_get_rdsp_c:
	 mov eax,[rsdt_ptr]
ret

_get_interupt_mode_c:
	 mov eax,[interupt_mode]
ret


_bios_get_mem_map_c:
	
	mov eax	,	[esp+4]
	pusha
	
	lea edx		,   [memory_map_result]
	mov [eax]	,	edx
		
	popa
	
	movzx eax,word [num_mem_map_entry]
ret

_bios_get_boot_device_c:
	movzx eax,byte [bios_boot_device]
ret

_get_gdt_ptr_c:
	
	mov eax,[esp+4]
	SGDT [eax]
	
	mov eax		,1
ret

_set_gdt_ptr_c:
	mov eax									,[esp+4]
	and eax									,0xFFFF;
	mov word [gdtptr]						,ax
	
	mov eax,[esp+8]
	mov dword [gdtptr+2]					,eax
	lgdt [gdtptr]
	
	
	jmp 0x08:set_gdt_ptr_jmp
	set_gdt_ptr_jmp:
	
	mov ax		,0x10		; segment de donne
	mov es		,ax
	mov ds		,ax	
	
	mov eax									,1
ret



;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
;
;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
_nops:
 mov ecx,30000000
 loop_nops:
    nop
 loopnz loop_nops
ret

_nop:
 mov ecx,3000000
 loop_nop:
    nop
 loopnz loop_nop
ret








;TIMES 512-(($-$$)&0x01FF) DB 0	




;%include "pci_bus.inc"




;maximum addressable zone in real mode
;65536		*	16		= 1048576 
;0x10000	*	0x10	= 0x100000


;memory avaible in real mode before 0xA000 zone
;0x00A000	*	0x10	= 0x0A0000
;40.960		*	16		= 655360 = 640k

;maximum size of the image
;0x0A0000	- 0x10000	= 0x90000
;655360		- 65536		= 589824


kernel_image_end:

%include "lib_c.inc"
%include "zlib.inc"
%include "sys_stream.inc"