#define NDIS_API	C_EXPORT
#include <std_def.h>
#include <std_mem.h>

#include "kern.h"
#include "sys_pci.h"
#include "mem_base.h"
#include "lib_c.h"
#include "tree.h"
#include "sys/mem_stream.h"
#include "sys/async_stream.h"
#include "sys/file_system.h"
#include "sys/tpo_mod.h"
#include "sys/pci_device.h"
#include "sys/ndis.h"
#include "winnt_types.h"



#define ETH_ALEN        6               /* Octets in one ethernet addr   */
 #define ETH_HLEN        14              /* Total octets in header.       */
 #define ETH_ZLEN        60              /* Min. octets in frame sans FCS */
 #define ETH_DATA_LEN    1500            /* Max. octets in payload        */
 #define ETH_FRAME_LEN   1514            /* Max. octets in frame sans FCS */

#define SSID_MAX_WPA_IE_LEN 40
#define NDIS_ESSID_MAX_SIZE 32
#define NDIS_ENCODING_TOKEN_MAX 32
#define MAX_ENCR_KEYS 4
#define TX_RING_SIZE 16
#define NDIS_MAX_RATES 8
#define NDIS_MAX_RATES_EX 16
#define WLAN_EID_GENERIC 221
#define MAX_WPA_IE_LEN 64
#define MAX_STR_LEN 512


#define NDIS_STATUS_SUCCESS		0
#define NDIS_STATUS_PENDING		0x00000103
#define NDIS_STATUS_NOT_RECOGNIZED	0x00010001
#define NDIS_STATUS_NOT_COPIED		0x00010002
#define NDIS_STATUS_NOT_ACCEPTED	0x00010003
#define NDIS_STATUS_CALL_ACTIVE		0x00010007
#define NDIS_STATUS_ONLINE		0x40010003
#define NDIS_STATUS_RESET_START		0x40010004
#define NDIS_STATUS_RESET_END		0x40010005
#define NDIS_STATUS_RING_STATUS		0x40010006
#define NDIS_STATUS_CLOSED		0x40010007
#define NDIS_STATUS_WAN_LINE_UP		0x40010008
#define NDIS_STATUS_WAN_LINE_DOWN	0x40010009
#define NDIS_STATUS_WAN_FRAGMENT	0x4001000A
#define NDIS_STATUS_MEDIA_CONNECT	0x4001000B
#define NDIS_STATUS_MEDIA_DISCONNECT	0x4001000C
#define NDIS_STATUS_HARDWARE_LINE_UP	0x4001000D
#define NDIS_STATUS_HARDWARE_LINE_DOWN	0x4001000E
#define NDIS_STATUS_INTERFACE_UP	0x4001000F
#define NDIS_STATUS_INTERFACE_DOWN	0x40010010
#define NDIS_STATUS_MEDIA_BUSY		0x40010011
#define NDIS_STATUS_MEDIA_SPECIFIC_INDICATION	0x40010012
#define NDIS_STATUS_WW_INDICATION NDIS_STATUS_MEDIA_SPECIFIC_INDICATION
#define NDIS_STATUS_LINK_SPEED_CHANGE	0x40010013
#define NDIS_STATUS_WAN_GET_STATS	0x40010014
#define NDIS_STATUS_WAN_CO_FRAGMENT	0x40010015
#define NDIS_STATUS_WAN_CO_LINKPARAMS	0x40010016
#define NDIS_STATUS_NOT_RESETTABLE	0x80010001
#define NDIS_STATUS_SOFT_ERRORS		0x80010003
#define NDIS_STATUS_HARD_ERRORS		0x80010004
#define NDIS_STATUS_BUFFER_OVERFLOW	0x80000005
#define NDIS_STATUS_FAILURE		0xC0000001
#define NDIS_STATUS_INVALID_PARAMETER 0xC000000D
#define NDIS_STATUS_RESOURCES		0xC000009A
#define NDIS_STATUS_CLOSING		0xC0010002
#define NDIS_STATUS_BAD_VERSION		0xC0010004
#define NDIS_STATUS_BAD_CHARACTERISTICS	0xC0010005
#define NDIS_STATUS_ADAPTER_NOT_FOUND	0xC0010006
#define NDIS_STATUS_OPEN_FAILED		0xC0010007
#define NDIS_STATUS_DEVICE_FAILED	0xC0010008
#define NDIS_STATUS_MULTICAST_FULL	0xC0010009
#define NDIS_STATUS_MULTICAST_EXISTS	0xC001000A
#define NDIS_STATUS_MULTICAST_NOT_FOUND	0xC001000B
#define NDIS_STATUS_REQUEST_ABORTED	0xC001000C
#define NDIS_STATUS_RESET_IN_PROGRESS	0xC001000D
#define NDIS_STATUS_CLOSING_INDICATING	0xC001000E
#define NDIS_STATUS_BAD_VERSION		0xC0010004
#define NDIS_STATUS_NOT_SUPPORTED	0xC00000BB
#define NDIS_STATUS_INVALID_PACKET	0xC001000F
#define NDIS_STATUS_OPEN_LIST_FULL	0xC0010010
#define NDIS_STATUS_ADAPTER_NOT_READY	0xC0010011
#define NDIS_STATUS_ADAPTER_NOT_OPEN	0xC0010012
#define NDIS_STATUS_NOT_INDICATING	0xC0010013
#define NDIS_STATUS_INVALID_LENGTH	0xC0010014
#define NDIS_STATUS_INVALID_DATA	0xC0010015
#define NDIS_STATUS_BUFFER_TOO_SHORT	0xC0010016
#define NDIS_STATUS_INVALID_OID		0xC0010017
#define NDIS_STATUS_ADAPTER_REMOVED	0xC0010018
#define NDIS_STATUS_UNSUPPORTED_MEDIA	0xC0010019
#define NDIS_STATUS_GROUP_ADDRESS_IN_USE	0xC001001A
#define NDIS_STATUS_FILE_NOT_FOUND	0xC001001B
#define NDIS_STATUS_ERROR_READING_FILE	0xC001001C
#define NDIS_STATUS_ALREADY_MAPPED	0xC001001D
#define NDIS_STATUS_RESOURCE_CONFLICT	0xC001001E
#define NDIS_STATUS_NO_CABLE		0xC001001F
#define NDIS_STATUS_INVALID_SAP		0xC0010020
#define NDIS_STATUS_SAP_IN_USE		0xC0010021
#define NDIS_STATUS_INVALID_ADDRESS	0xC0010022
#define NDIS_STATUS_VC_NOT_ACTIVATED	0xC0010023
#define NDIS_STATUS_DEST_OUT_OF_ORDER	0xC0010024
#define NDIS_STATUS_VC_NOT_AVAILABLE	0xC0010025
#define NDIS_STATUS_CELLRATE_NOT_AVAILABLE	0xC0010026
#define NDIS_STATUS_INCOMPATABLE_QOS	0xC0010027
#define NDIS_STATUS_AAL_PARAMS_UNSUPPORTED	0xC0010028
#define NDIS_STATUS_NO_ROUTE_TO_DESTINATION	0xC0010029
#define NDIS_STATUS_TOKEN_RING_OPEN_ERROR	0xC0011000
#define NDIS_STATUS_INVALID_DEVICE_REQUEST	0xC0000010
#define NDIS_STATUS_NETWORK_UNREACHABLE         0xC000023C

/* Attribute flags to NdisMSetAtrributesEx */
#define NDIS_ATTRIBUTE_IGNORE_PACKET_TIMEOUT    0x00000001
#define NDIS_ATTRIBUTE_IGNORE_REQUEST_TIMEOUT   0x00000002
#define NDIS_ATTRIBUTE_IGNORE_TOKEN_RING_ERRORS 0x00000004
#define NDIS_ATTRIBUTE_BUS_MASTER               0x00000008
#define NDIS_ATTRIBUTE_INTERMEDIATE_DRIVER      0x00000010
#define NDIS_ATTRIBUTE_DESERIALIZE              0x00000020
#define NDIS_ATTRIBUTE_NO_HALT_ON_SUSPEND       0x00000040
#define NDIS_ATTRIBUTE_SURPRISE_REMOVE_OK       0x00000080
#define NDIS_ATTRIBUTE_NOT_CO_NDIS              0x00000100
#define NDIS_ATTRIBUTE_USES_SAFE_BUFFER_APIS    0x00000200



#define CmResourceTypeNull		0
#define CmResourceTypePort		1
#define CmResourceTypeInterrupt		2
#define CmResourceTypeMemory		3
#define CmResourceTypeDma		4
#define CmResourceTypeDeviceSpecific	5
#define CmResourceTypeBusNumber		6
#define CmResourceTypeMaximum		7

#define CM_RESOURCE_INTERRUPT_LEVEL_SENSITIVE	0
#define CM_RESOURCE_INTERRUPT_LATCHED		1
#define CM_RESOURCE_MEMORY_READ_WRITE		0x0000
#define CM_RESOURCE_MEMORY_READ_ONLY		0x0001
#define CM_RESOURCE_MEMORY_WRITE_ONLY		0x0002
#define CM_RESOURCE_MEMORY_PREFETCHABLE		0x0004

#define CM_RESOURCE_MEMORY_COMBINEDWRITE	0x0008
#define CM_RESOURCE_MEMORY_24			0x0010
#define CM_RESOURCE_MEMORY_CACHEABLE		0x0020

#define CM_RESOURCE_PORT_MEMORY			0x0000
#define CM_RESOURCE_PORT_IO			0x0001
#define CM_RESOURCE_PORT_10_BIT_DECODE		0x0004
#define CM_RESOURCE_PORT_12_BIT_DECODE		0x0008
#define CM_RESOURCE_PORT_16_BIT_DECODE		0x0010
#define CM_RESOURCE_PORT_POSITIVE_DECODE	0x0020
#define CM_RESOURCE_PORT_PASSIVE_DECODE		0x0040
#define CM_RESOURCE_PORT_WINDOW_DECODE		0x0080

#define PROTOCOL_RESERVED_SIZE_IN_PACKET (4 * sizeof(void *))



typedef UINT NDIS_STATUS;
typedef UCHAR NDIS_DMA_SIZE;
typedef LONG ndis_rssi;
typedef ULONG ndis_key_index;
typedef ULONG ndis_tx_power_level;
typedef ULONGULONG ndis_key_rsc;
typedef UCHAR ndis_rates[NDIS_MAX_RATES];
typedef UCHAR ndis_rates_ex[NDIS_MAX_RATES_EX];
typedef UCHAR mac_address[ETH_ALEN];
typedef ULONG ndis_fragmentation_threshold;
typedef ULONG ndis_rts_threshold;
typedef ULONG ndis_antenna;
typedef ULONG ndis_oid;

typedef ULONG64 NDIS_PHY_ADDRESS;

typedef unsigned int NDIS_HANDLE;




typedef struct {
  UINT PhysicalCount;
  UINT TotalLength;
  struct mdl *Head;
  struct mdl *Tail;
  struct ndis_packet_pool *Pool;
  UINT Count;
  ULONG Flags;
  BOOLEAN ValidCounts;
  UCHAR NdisPacketFlags;
  USHORT NdisPacketOobOffset;
} NDIS_PACKET_PRIVATE;

typedef struct _NDIS_PACKET {
  NDIS_PACKET_PRIVATE Private;
   union {
     struct {
      UCHAR MiniportReserved[2 * sizeof(mem_ptr)];
      UCHAR WrapperReserved[2 * sizeof(mem_ptr)];
    };
     struct {
      UCHAR MiniportReservedEx[3 * sizeof(mem_ptr)];
      UCHAR WrapperReservedEx[sizeof(mem_ptr)];
    };
     struct {
      UCHAR MacReserved[4 * sizeof(mem_ptr)];
    };
  };
  ULONG_PTR Reserved[2];
  UCHAR ProtocolReserved[1];
} NDIS_PACKET;

/*
  *      IEEE 802.3 Ethernet magic constants.  The frame sizes omit the preamble
  *      and FCS/CRC (frame check sequence).
  */
 
 #define ETH_ALEN        6               /* Octets in one ethernet addr   */
 #define ETH_HLEN        14              /* Total octets in header.       */
 #define ETH_ZLEN        60              /* Min. octets in frame sans FCS */
 #define ETH_DATA_LEN    1500            /* Max. octets in payload        */
 #define ETH_FRAME_LEN   1514            /* Max. octets in frame sans FCS */
 
 /*
  *      These are the defined Ethernet Protocol ID's.
  */
 
 #define ETH_P_LOOP      0x0060          /* Ethernet Loopback packet     */
 #define ETH_P_PUP       0x0200          /* Xerox PUP packet             */
 #define ETH_P_PUPAT     0x0201          /* Xerox PUP Addr Trans packet  */
 #define ETH_P_IP        0x0800          /* Internet Protocol packet     */
 #define ETH_P_X25       0x0805          /* CCITT X.25                   */
 #define ETH_P_ARP       0x0806          /* Address Resolution packet    */
 #define ETH_P_BPQ       0x08FF          /* G8BPQ AX.25 Ethernet Packet
                                          * [ NOT AN OFFICIAL ID ] */
 #define ETH_P_IEEEPUP   0x0a00          /* Xerox IEEE802.3 PUP packet */
 #define ETH_P_IEEEPUPAT 0x0a01          /* Xerox IEEE802.3 PUP Addr
                                          * Trans packet */
 #define ETH_P_DEC       0x6000          /* DEC Assigned proto           */
 #define ETH_P_DNA_DL    0x6001          /* DEC DNA Dump/Load            */
 #define ETH_P_DNA_RC    0x6002          /* DEC DNA Remote Console       */
 #define ETH_P_DNA_RT    0x6003          /* DEC DNA Routing              */
 #define ETH_P_LAT       0x6004          /* DEC LAT                      */
 #define ETH_P_DIAG      0x6005          /* DEC Diagnostics              */
 #define ETH_P_CUST      0x6006          /* DEC Customer use             */
 #define ETH_P_SCA       0x6007          /* DEC Systems Comms Arch       */
 #define ETH_P_RARP      0x8035          /* Reverse Addr Res packet      */
 #define ETH_P_ATALK     0x809B          /* Appletalk DDP                */
 #define ETH_P_AARP      0x80F3          /* Appletalk AARP               */
 #define ETH_P_8021Q     0x8100          /* 802.1Q VLAN Extended Header  */
 #define ETH_P_IPX       0x8137          /* IPX over DIX                 */
 #define ETH_P_IPV6      0x86DD          /* IPv6 over bluebook           */
 #define ETH_P_PPP_DISC  0x8863          /* PPPoE discovery messages     */
 #define ETH_P_PPP_SES   0x8864          /* PPPoE session messages       */
 #define ETH_P_ATMMPOA   0x884c          /* MultiProtocol Over ATM       */
 #define ETH_P_ATMFATE   0x8884          /* Frame-based ATM Transport
                                         * over Ethernet
                                     */
/*
 *      Non DIX types. Won't clash for 1500 types.
 */
#define ETH_P_802_3     0x0001          /* Dummy type for 802.3 frames  */
#define ETH_P_AX25      0x0002          /* Dummy protocol id for AX.25  */
#define ETH_P_ALL       0x0003          /* Every packet (be careful!!!) */
#define ETH_P_802_2     0x0004          /* 802.2 frames                 */
#define ETH_P_SNAP      0x0005          /* Internal only                */
#define ETH_P_DDCMP     0x0006          /* DEC DDCMP: Internal only     */
#define ETH_P_WAN_PPP   0x0007          /* Dummy type for WAN PPP frames*/
#define ETH_P_PPP_MP    0x0008          /* Dummy type for PPP MP frames */
#define ETH_P_LOCALTALK 0x0009          /* Localtalk pseudo type        */
#define ETH_P_PPPTALK   0x0010          /* Dummy type for Atalk over PPP*/
#define ETH_P_TR_802_2  0x0011i         /* 802.2 frames                 */
#define ETH_P_MOBITEX   0x0015          /* Mobitex (kaz@cafe.net)       */
#define ETH_P_CONTROL   0x0016          /* Card specific control frames */
#define ETH_P_IRDA      0x0017          /* Linux-IrDA                   */
#define ETH_P_ECONET    0x0018          /* Acorn Econet                 */

/*
 *      This is an Ethernet frame header.
 */

typedef struct  {
        unsigned char   h_dest[ETH_ALEN];       /* destination eth addr */
        unsigned char   h_source[ETH_ALEN];     /* source ether addr    */
        unsigned short  h_proto;                /* packet type ID field */
}eth_hdr;

typedef unsigned int	 sipaddr_t;
typedef unsigned short   udpport_t;

#define IP_PROTO_ICMP	1 
#define IP_PROTO_IGMP	2 
#define IP_PROTO_IP		4 
#define IP_PROTO_TCP	6
#define IP_PROTO_UDP	17 

typedef struct
{
unsigned char	version;
unsigned char	tos;
unsigned short	tot_len;
unsigned short	id;
unsigned char	flags;
unsigned short	frag_off;
unsigned char	ttl;
unsigned char	protocol;
unsigned short	check;
unsigned int	saddr;
unsigned int	daddr;
}ip_hdr;


typedef struct udp_hdr
{
     udpport_t			uh_src_port;
     udpport_t			uh_dst_port;
     unsigned short		uh_length;
     unsigned short		uh_chksum;
} udp_hdr_t;

typedef struct
{
	unsigned char 	op;
	unsigned char 	htype;
	unsigned char 	hlen;
	unsigned char 	hops;
	unsigned int  	xid;
	unsigned short	secs;
	unsigned short	flags;
	unsigned int	ciaddr;
	unsigned int	yiaddr;
	unsigned int	siaddr;
	unsigned int	giaddr;
	unsigned int	chaddr[4];
	char			sname[64];
	char			file[128];

}dhcp_mess;


#define NDIS_OOB_DATA_FROM_PACKET(_p)                                   \
                        (NDIS_PACKET_OOB_DATA *)((unsigned char *)(_p) +          \
                        (_p)->Private.NdisPacketOobOffset)
typedef struct 
{
    union
    {
        ULONGLONG   TimeToSend;
        ULONGLONG   TimeSent;
    };
    ULONGLONG       TimeReceived;
    UINT            HeaderSize;
    UINT            SizeMediaSpecificInfo;
    mem_ptr           MediaSpecificInformation;

    NDIS_STATUS     Status;
} NDIS_PACKET_OOB_DATA;

typedef void 		__stdcall ndis_isr_handler_fnc		(BOOLEAN *recognized, BOOLEAN *queue_handler,void *handle) ;
typedef void 		__stdcall ndis_interrupt_handler_fnc(void *ctx) ;
typedef void 		__stdcall enable_interrupt_fnc		(void *ctx) ;
typedef void 		__stdcall disable_interrupt_fnc		(void *ctx) ;
typedef NDIS_STATUS	__stdcall send_packets_fnc			(void *ctx, NDIS_PACKET **packets,INT nr_of_packets) ;
typedef NDIS_STATUS __stdcall send_fnc					(void *ctx, NDIS_PACKET *packet,UINT flags) ;
typedef NDIS_STATUS __stdcall co_send_packets_fnc		(void *vc_ctx, void **packets,UINT nr_of_packets) ;
typedef NDIS_STATUS __stdcall setinfo_fnc				(void *ctx, ndis_oid oid, void *buffer,ULONG buflen, ULONG *written,ULONG *needed) ;
typedef NDIS_STATUS __stdcall queryinfo_fnc				(void *ctx, ndis_oid oid, void *buffer,ULONG buflen, ULONG *written,ULONG *needed) ;


typedef ndis_isr_handler_fnc		*ndis_isr_handler;
typedef ndis_interrupt_handler_fnc	*ndis_interrupt_handler;
typedef enable_interrupt_fnc		*enable_interrupt_ptr;
typedef disable_interrupt_fnc		*disable_interrupt_ptr;
typedef	send_packets_fnc			*send_packets_ptr;
typedef	send_fnc					*send_fnc_ptr;
typedef	co_send_packets_fnc			*co_send_packets_ptr;
typedef	setinfo_fnc					*setinfo_fnc_ptr;
typedef	queryinfo_fnc				*queryinfo_fnc_ptr;

typedef NDIS_STATUS __stdcall MiniportInitialize_func (NDIS_STATUS *OpenErrorStatus,unsigned int  *SelectedMediumIndex,enum ndis_medium *MediumArray,unsigned int  MediumArraySize,struct driver_object *MiniportAdapterHandle,void *WrapperConfigurationContext);
typedef MiniportInitialize_func *MiniportInitialize_ptr;


struct miniport {
	/* NDIS 3.0 */
	UCHAR major_version;
	UCHAR minor_version;
	USHORT filler;
	UINT reserved;
	BOOLEAN (*hangcheck)(void *ctx) ;

	disable_interrupt_ptr	disable_interrupt;
	enable_interrupt_ptr	enable_interrupt;

	//void (*disable_interrupt)(void *ctx) ;
	//void (*enable_interrupt)(void *ctx) ;
	void (*mp_halt)(void *ctx) ;
	
	ndis_interrupt_handler handle_interrupt;
	MiniportInitialize_ptr	init;
	ndis_isr_handler		isr;

	queryinfo_fnc_ptr queryinfo ;
	void *reconfig;
	NDIS_STATUS (*reset)(BOOLEAN *reset_address, void *ctx) ;
	send_fnc_ptr send;
	setinfo_fnc_ptr	setinfo;

	NDIS_STATUS (*tx_data)(struct ndis_packet *ndis_packet,
			       UINT *bytes_txed, void *mp_ctx, void *rx_ctx,
			       UINT offset, UINT bytes_to_tx) ;
	/* NDIS 4.0 extensions */
	void (*return_packet)(void *ctx, void *packet) ;
	send_packets_ptr send_packets;

	void (*alloc_complete)(void *handle, void *virt,
			       NDIS_PHY_ADDRESS *phys,
			       ULONG size, void *ctx) ;
	/* NDIS 5.0 extensions */
	NDIS_STATUS (*co_create_vc)(void *ctx, void *vc_handle,
				    void *vc_ctx) ;
	NDIS_STATUS (*co_delete_vc)(void *vc_ctx) ;
	NDIS_STATUS (*co_activate_vc)(void *vc_ctx, void *call_params) ;
	NDIS_STATUS (*co_deactivate_vc)(void *vc_ctx) ;
	co_send_packets_ptr co_send_packets;
	NDIS_STATUS (*co_request)(void *ctx, void *vc_ctx, UINT *req) ;
	/* NDIS 5.1 extensions */
	void (*cancel_send_packets)(void *ctx, void *id) ;
	void (*pnp_event_notify)(void *ctx, enum ndis_device_pnp_event event,
				 void *inf_buf, ULONG inf_buf_len) ;
	void (*shutdown)(void *ctx) ;
	void *reserved1;
	void *reserved2;
	void *reserved3;
	void *reserved4;
};


enum ndis_parameter_type{NdisParameterInteger,NdisParameterHexInteger,NdisParameterString,NdisParameterMultiString};

typedef struct {
    enum ndis_parameter_type ParameterType;
    union {
        ULONG IntegerData;
        struct unicode_string StringData;
    } ParameterData;
} ndis_configuration_parameter;


/* NdisMSetTimer is a macro that calls NdisSetTimer with
 * ndis_mp_timer typecast to ndis_timer */
typedef struct
{
	DPC func;
	void *ctx;
}tt;




typedef struct _CM_PARTIAL_RESOURCE_DESCRIPTOR {
    UCHAR  Type;
    UCHAR  ShareDisposition;
    USHORT  Flags;
    union {
        struct {
            PHYSICAL_ADDRESS  Start;
            ULONG  Length;
        } Generic;
        struct {
            PHYSICAL_ADDRESS  Start;
            ULONG  Length;
        } Port;
        struct {
#if defined(NT_PROCESSOR_GROUPS)
            USHORT  Level;
            USHORT  Group;
#else
            ULONG  Level;
#endif
            ULONG  Vector;
            KAFFINITY Affinity;
        } Interrupt;

        // This member exists only on Windows Vista and later
        struct {
            union {
               struct {
#if defined(NT_PROCESSOR_GROUPS)
                   USHORT  Group;
#else
                   USHORT  Reserved;
#endif
                   USHORT  MessageCount;
                   ULONG  Vector;
                   KAFFINITY  Affinity;
               } Raw;

               struct {
#if defined(NT_PROCESSOR_GROUPS)
                   USHORT  Level;
                   USHORT  Group;
#else
                   ULONG  Level;
#endif
                   ULONG  Vector;
                   KAFFINITY  Affinity;
               } Translated;        
            };
        } MessageInterrupt;
        struct {
            PHYSICAL_ADDRESS  Start;
            ULONG  Length;
        } Memory;
        struct {
            ULONG  Channel;
            ULONG  Port;
            ULONG  Reserved1;
        } Dma;
        struct {
            ULONG  Data[3];
        } DevicePrivate;
        struct {
            ULONG  Start;
            ULONG  Length;
            ULONG  Reserved;
        } BusNumber;
        struct {
            ULONG  DataSize;
            ULONG  Reserved1;
            ULONG  Reserved2;
        } DeviceSpecificData;
        // The following structures provide support for memory-mapped
        // IO resources greater than MAXULONG
        struct {
            PHYSICAL_ADDRESS  Start;
            ULONG  Length40;
        } Memory40;
        struct {
            PHYSICAL_ADDRESS  Start;
            ULONG  Length48;
        } Memory48;
        struct {
            PHYSICAL_ADDRESS  Start;
            ULONG  Length64;
        } Memory64;
    } u;
} CM_PARTIAL_RESOURCE_DESCRIPTOR, *PCM_PARTIAL_RESOURCE_DESCRIPTOR;

typedef struct {
  USHORT							Version;
  USHORT							Revision;
  ULONG								Count;
  CM_PARTIAL_RESOURCE_DESCRIPTOR	PartialDescriptors[1];
} CM_PARTIAL_RESOURCE_LIST;

typedef struct
{
		unsigned int				driver_id;
		tpo_mod_file				*tpo;
		pci_device					*pci_dev;
		struct driver_object		*obj;
		mem_ptr						mp_ctx;
		mem_zone_ref				mp;
		CM_PARTIAL_RESOURCE_LIST	*list;
}ndis_module;

typedef		NTSTATUS __stdcall	entry_point_func		(struct driver_object *DriverObject,struct unicode_string *RegistryPath);
typedef		entry_point_func	*entry_point_func_ptr;

tt								ttt={0xFF};
NDIS_PACKET						*pack;
NDIS_PACKET						*pack_ptr[2]={0xff};;

struct mdl						*buffer={0xff};
mem_zone_ref					buf_zone={0xff};
ndis_configuration_parameter	param_out={0xFF};



#define DLL_PROCESS_ATTACH	1
#define DLL_PROCESS_DETACH	0
#define DLL_THREAD_ATTACH	2
#define DLL_THREAD_DETACH	3


mem_zone_ref		ndis_modules_ref;
ndis_module			*ndis_modules;
unsigned int		n_ndis_modules	=	0xFFFFFFFF;

unsigned int	initialised=0xFF;
unsigned int	irq_shared=0xFF;
unsigned int	irq_req_isr=0xFF;
unsigned int	attributes=0xFF;
unsigned int	kernel_log_id=0xFFFFFFFF;

unsigned int	 ndis_interupt_mode = 0xFFFFFFFF;
char string[1024]={0xFF};

ULONG __stdcall SPAN_PAGES(void *ptr, SIZE_T length)
{
	return PAGE_ALIGN(((unsigned long)ptr & (PAGE_SIZE - 1)) + length)
		>> PAGE_SHIFT;
}

ULONG __stdcall MmSizeOfMdl(void *base, ULONG length)
{
	return sizeof(struct mdl) +
	       (sizeof(PFN_NUMBER) * SPAN_PAGES(base, length));
}


void *ExAllocatePoolWithTag	(enum pool_type pool_type, SIZE_T size, ULONG tag)
{
	mem_zone_ref	ref;
	mem_ptr			ptr;
	mem_ptr			ptr_ret;

	ref.zone=PTR_NULL;

	allocate_new_zone(0,size+8,&ref);

	ptr		=	get_zone_ptr(&ref,0);
	ptr_ret	=	get_zone_ptr(&ref,4);

	*((unsigned int *)(ptr))=ref.zone;
	return ptr_ret;
}

void ExFreePoolWithTag(void *addr, ULONG tag)
{
	mem_zone_ref	free;
	unsigned int	*iaddr;

	iaddr		=	addr;
	iaddr--;
	free.zone	=	*iaddr;
	release_zone_ref(&free);
	
	
	return;
}

void ExFreePool(void *addr)
{
	ExFreePoolWithTag(addr, 0);
}

void *allocate_object(ULONG size, enum common_object_type type,
		      struct unicode_string *name)
{
	struct common_object_header *hdr;
	mem_ptr body;

	/* we pad header as prefix to body */
	hdr = ExAllocatePoolWithTag(NonPagedPool, OBJECT_SIZE(size), 0);
	if (!hdr) {
		//WARNING("couldn't allocate memory");
		return NULL;
	}
	memset_c(hdr, 0, OBJECT_SIZE(size));
	hdr->type		= type;
	hdr->ref_count	= 1;
//	spin_lock_bh(&ntoskernel_lock);
	/* threads are looked up often (in KeWaitForXXX), so optimize
	 * for fast lookups of threads */
	/*
	if (type == OBJECT_TYPE_NT_THREAD)
		InsertHeadList(&object_list, &hdr->list);
	else
		InsertTailList(&object_list, &hdr->list);
	*/
	//spin_unlock_bh(&ntoskernel_lock);
	body = HEADER_TO_OBJECT(hdr);
	
	//TRACE3("allocated hdr: %p, body: %p", hdr, body);
	return body;
}

NTSTATUS IoAllocateDriverObjectExtension(struct driver_object *drv_obj, void *client_id, ULONG extlen,void **ext)
{
	struct custom_ext *ce;

	
	ce = ExAllocatePoolWithTag(0,sizeof(struct custom_ext) + extlen,0);
	
	if (ce == NULL)
		return STATUS_INSUFFICIENT_RESOURCES;

	//IOTRACE("custom_ext: %p", ce);
	ce->client_id = client_id;
	//spin_lock_bh(&ntoskernel_lock);
	InsertTailList(&drv_obj->drv_ext->custom_ext, &ce->list);
	//spin_unlock_bh(&ntoskernel_lock);

	*ext = (char *)ce + sizeof(struct custom_ext);
	//IOTRACE("ext: %p", *ext);
	return STATUS_SUCCESS;
}

OS_API_C_FUNC(int) init_ndis(int_mode)
{
	
	ndis_interupt_mode = int_mode;

	kernel_log_id	=	get_new_kern_log_id("ndis :",0x0A);
	
	kernel_log			(kernel_log_id,"init ndis system \n");

	n_ndis_modules			=		0;
	initialised				=		0;
	ndis_modules_ref.zone	=		PTR_NULL;

	allocate_new_zone	(0x00,8*sizeof(ndis_module),&ndis_modules_ref);

	ndis_modules	=	get_zone_ptr(&ndis_modules_ref,0);

	memset_c			(ndis_modules,0,8*sizeof(ndis_module));

	

	
	return 1;
}


void create_eth_hdr		(mem_ptr packet,unsigned int tot_len)
{
   memset_c(((eth_hdr		*)(packet))->h_dest,0,ETH_ALEN);		/* destination eth addr */
   ((eth_hdr		*)(packet))->h_source;						/* source ether addr    */
   
   ((eth_hdr		*)(packet))->h_proto	=	0x0600	;  
   ((eth_hdr		*)(packet))->h_proto	=	0x0600	;  
}

void create_ip_hdr		(mem_ptr packet,unsigned int tot_len)
{
	((ip_hdr		*)(packet))->daddr		=	0xFFFFFFFF;
	((ip_hdr		*)(packet))->saddr		=	0x00000000;
	((ip_hdr		*)(packet))->tot_len	=	tot_len;
	((ip_hdr		*)(packet))->check		=	0;
	((ip_hdr		*)(packet))->flags		=	0;
	((ip_hdr		*)(packet))->check		=	0xFF;
	((ip_hdr		*)(packet))->frag_off	=	0;

	((ip_hdr		*)(packet))->protocol	=	IP_PROTO_UDP;
	((ip_hdr		*)(packet))->id			=	0xAABBCCDD;
	((ip_hdr		*)(packet))->ttl		=	255;
	((ip_hdr		*)(packet))->version	=	((0x04)|((sizeof(ip_hdr)&0x0F)<<4));
	((ip_hdr		*)(packet))->tos		=	0;
}
void create_udp_hdr		(mem_ptr packet,unsigned int tot_len)
{
     ((udp_hdr_t		*)(packet))->uh_src_port	=	0;
     ((udp_hdr_t		*)(packet))->uh_dst_port	=	68;
     ((udp_hdr_t		*)(packet))->uh_length		=	0x00;
     ((udp_hdr_t		*)(packet))->uh_chksum		=	0x00;
}


void send_upd_packet	(NDIS_PACKET *pack)
{
	unsigned char	*data_ptr;

	data_ptr	=	pack->Private.Head->startva;


}


void create_uni_str(struct unicode_string *uni,const char *str)
{
	unsigned int	n,nchar;
	mem_zone_ref	buffer;
	
	buffer.zone		=	PTR_NULL;
	n				=	strlen_c(str);
	nchar			=	n+1;
	allocate_new_zone	(0,nchar*2,&buffer);
	uni->length		=	nchar*2;
	uni->max_length	=	nchar*2;
	uni->buf		=	get_zone_ptr(&buffer,0);
	uni->buf[n]		=	0;
	while(n--)
	{
		uni->buf[n]		=	str[n];
	}
}

void uni_str_to_char(const struct unicode_string *uni,char *out_str,unsigned int len)
{
	unsigned int n;
	n			=	uni->length<len?uni->length:len;
	out_str[n]	=	0;
	while(n--)
	{
		out_str[n]=uni->buf[n];
	}
	 
}


ndis_module	*find_module(struct driver_object *obj)
{
	unsigned int n;

	n=0;
	while(n<n_ndis_modules)
	{
		if(ndis_modules[n].obj==obj)
			return &ndis_modules[n];
		
		n++;
	}

	return PTR_NULL;

}




ndis_module	*find_module_by_dev(pci_device *dev)
{
	unsigned int n;

	n=0;
	while(n<n_ndis_modules)
	{
		if((ndis_modules[n].pci_dev->bus_id==dev->bus_id)&&
			(ndis_modules[n].pci_dev->dev_id==dev->dev_id))
			return &ndis_modules[n];
		
		n++;
	}

	return PTR_NULL;

}


ndis_module	*find_module_by_id(unsigned int drv_id)
{
	unsigned int n;

	n=0;
	while(n<n_ndis_modules)
	{
		if(ndis_modules[n].driver_id==drv_id)
			return &ndis_modules[n];
		
		n++;
	}

	return PTR_NULL;

}

OS_API_C_FUNC(int) load_ndis_driver(pci_device		*device)
{
	mem_ptr					entry_point,tpo_addr;
	entry_point_func_ptr	entry_point_fnc;
	tpo_mod_file			*ndis_driver_mod;
//	ndis_driver				*driver_ptr;
	

	ndis_driver_mod			= &device->driver_mod;
	entry_point				= get_tpo_mod_exp_addr_name	(ndis_driver_mod,"EntryPoint");
	
	tpo_addr=get_zone_ptr(&ndis_driver_mod->data_sections,0);
	
	kernel_log	(kernel_log_id,"ndis driver entry point : ");
	writeptr	(entry_point);
	writestr	(" ");
	writeptr	(tpo_addr);
	writestr	("\n");

	if(entry_point!=0xFFFFFFFF)
	{
		NDIS_STATUS								error;
		unsigned int							selected;
//		ndis_driver								*driver_ptr;
		struct	driver_object					*drv_obj_ptr;
		ndis_module								*drv_mod_ptr;
		struct	miniport						*mp_ptr;
		unsigned int							ret;
		mem_zone_ref							drv_ext;
		enum ndis_medium medium_array[]			= {NdisMedium802_3};

	
		drv_obj_ptr					=	allocate_object(sizeof(struct driver_object), OBJECT_TYPE_DRIVER, NULL);
		drv_ext.zone				=	PTR_NULL;
		allocate_new_zone			(0,sizeof(struct driver_extension),&drv_ext);

		drv_obj_ptr->drv_ext			=	get_zone_ptr(&drv_ext,0);
		drv_obj_ptr->drv_ext->drv_obj	=	drv_obj_ptr;
		
		drv_mod_ptr						=	&ndis_modules[n_ndis_modules];
		drv_mod_ptr->driver_id			=	n_ndis_modules+1;
		drv_mod_ptr->obj				=	drv_obj_ptr;
		drv_mod_ptr->tpo				=	ndis_driver_mod;
		drv_mod_ptr->pci_dev			=	device;
		drv_mod_ptr->mp.zone			=	PTR_NULL;
		n_ndis_modules++;
			
		create_uni_str							(&drv_obj_ptr->name,"/tmp");

		//drv_obj_ptr->hardware_database		=	&drv_obj_ptr->name;
		//drv_obj_ptr->type					=	OBJECT_TYPE_DRIVER;
		//drv_obj_ptr->size					=	sizeof(struct driver_object);

		entry_point_fnc=entry_point;
		ret=entry_point_fnc		(drv_obj_ptr,&drv_obj_ptr->name);
		
		kernel_log		(kernel_log_id,"init miniport \n");
		
		
		mp_ptr		=	get_zone_ptr(&drv_mod_ptr->mp,0);
		writeptr		(mp_ptr->init);
		writestr		(" ");
		writeptr		(get_zone_ptr(&drv_mod_ptr->tpo->data_sections,0));
		writestr		(" ");
		writeptr		(&device->driver_conf_node);
		writestr		("\n");
		
		ret			=	mp_ptr->init	(&error,&selected,medium_array,sizeof(medium_array) / sizeof(medium_array[0]),drv_obj_ptr,device);
		initialised	=	1;
		kernel_log		(kernel_log_id," ret : ");
		writeint		(ret,16);
		writestr		("\n");
/*
		memset(&pack,0,sizeof(NDIS_PACKET));

		
		pack.Private.len	=	0;
		packs[0]		=&pack;

		mp_ptr->send_packets(drv_mod_ptr->mp_ctx,packs,1);
		*/
	}
	return 1;
}

//ntoskrnl.exe
void _allshl (){}
void _allmul (){}
//HAL.dll
void READ_PORT_UCHAR (){}
void WRITE_PORT_UCHAR (){}
//NDIS.SYS
void NdisMSetPeriodicTimer					(){}


void __stdcall NdisInitializeWrapper(	void **driver_handle			,struct driver_object	*driver,struct unicode_string *reg_path	, void *unused)
{
	ndis_module		*module_ptr;
	
	kernel_log		(kernel_log_id,"init ndis ");

	module_ptr	=	find_module(driver);
	if(module_ptr	!=	PTR_NULL)
	{
		writestr("[");
		writeint(module_ptr->driver_id,10);
		writestr("] ");
		writestr(module_ptr->tpo->name);
	}
	writestr("\n");
	*driver_handle = driver;
}


void __stdcall KeStallExecutionProcessor(ULONG usecs)
{
	snooze(usecs/1000);
}


NDIS_STATUS __stdcall NdisMRegisterMiniport(struct	driver_object	*drv_obj_ptr, struct miniport *mp, UINT length)
{
	
	ndis_module		*module_ptr;
	
	module_ptr	=	find_module(drv_obj_ptr);
	kernel_log		(kernel_log_id,"register miniport ndis");
	if(module_ptr	!=	PTR_NULL)
	{		
			writestr	(" [");
			writeint	(module_ptr->driver_id,10);
			writestr	("] ");
			writestr	(module_ptr->tpo->name);
	}

	writestr	(" v");
	writeint	(mp->major_version,10);
	writestr	(".");
	writeint	(mp->minor_version,10);
	writestr	(" ");
	writeint	(sizeof( struct miniport),10);
	writestr	(" ");
	writeint	(length,10);
	writestr	("\n");
	
	if(length>sizeof(struct miniport))
		allocate_new_zone	(0,length,&module_ptr->mp);
	else
		allocate_new_zone	(0,sizeof(struct miniport),&module_ptr->mp);
		

	
	memcpy_c			(get_zone_ptr(&module_ptr->mp,0),mp,length);

	if (mp->major_version < 4) {
		return NDIS_STATUS_BAD_VERSION;
	}
	
	return NDIS_STATUS_SUCCESS;
}



ULONG __stdcall NdisReadPciSlotInformation(struct driver_object *drv_obj, ULONG slot,ULONG offset, char *buf, ULONG len)
{

	pci_device	*dev;
	ndis_module *mod;
	int			 pci_bus,pci_dev,pci_fn;
	unsigned int val;
	

	mod	=	find_module(drv_obj);
	dev	=	mod->pci_dev;
/*
	writestr("read pci info ");
	writeint(drv_obj,16);
	writestr(" ");
	writestr(dev->driver_name);
	writestr(" ofset ");
	writeint(offset,16);
	writestr(" to ");
	writeint(len,10);
	writestr("\n");
*/
	pci_bus			=	(dev->pci_code>>8)	& 0xFF;
	pci_dev			=	(dev->pci_code>>3)	& 0x1F;
	pci_fn			=	dev->pci_code		& 0x07;

	switch(len)
	{
		case 1:
			val	=	read_pci_byte_c(pci_bus,pci_dev,pci_fn,offset);
			*((unsigned char*)(buf))=val;
		break;
		case 2:
			val=read_pci_word_c(pci_bus,pci_dev,pci_fn,offset);
			*((unsigned short*)(buf))=val;
		break;
		case 4:
			val=read_pci_dword_c(pci_bus,pci_dev,pci_fn,offset);
			*((unsigned int*)(buf))=val;
		break;
	}
	return len;
}

ULONG __stdcall NdisWritePciSlotInformation(struct driver_object *drv_obj, ULONG slot, ULONG offset, char *buf, ULONG len)
{

	pci_device	*dev;
	ndis_module *mod;
	int			 pci_bus,pci_dev,pci_fn;
	

	mod	=	find_module(drv_obj);
	dev	=	mod->pci_dev;

	kernel_log	(kernel_log_id,"write pci info ");
	writeptr	(drv_obj);
	writestr	(" ");
	writestr	(dev->driver_name);
	writestr	(" ofset ");
	writeint	(offset,16);
	writestr	(" to ");
	writeint	(len,10);
	writestr	("\n");

	pci_bus			=	(dev->pci_code>>8)	& 0xFF;
	pci_dev			=	(dev->pci_code>>3)	& 0x1F;
	pci_fn			=	dev->pci_code		& 0x07;

	switch(len)
	{
		case 1:
			write_pci_byte_c	(pci_bus,pci_dev,pci_fn,offset,*((unsigned char*)(buf)));
		break;
		case 2:
			write_pci_word_c	(pci_bus,pci_dev,pci_fn,offset,*((unsigned short*)(buf)));
		break;
		case 4:
			write_pci_dword_c	(pci_bus,pci_dev,pci_fn,offset,*((unsigned int*)(buf)));
		break;
	}
	return len;
}


int ndis_read_pci_bar(pci_device	*pci_device,unsigned int reg_idx,unsigned int *base,unsigned int *size,unsigned int *type)
{
	unsigned int		base_reg_region,base_reg_region_size,base_reg_val,base_reg_flags,base_reg_addr;
	unsigned short		pci_code;
	unsigned int		pci_bus,pci_dev,pci_func;


	pci_code			=	pci_device->pci_code;
	pci_bus				=	(pci_code>>8)	& 0xFF;
	pci_dev				=	(pci_code>>3)	& 0x1F;
	pci_func			=	pci_code		& 0x07;

	base_reg_val	=	read_pci_dword_c(pci_bus, pci_dev, pci_func,0x10+reg_idx*4);

	base_reg_addr	=	base_reg_val&0xFFFFFFF0;
	base_reg_flags	=	base_reg_val&0xF;

	*type=base_reg_flags;

	write_pci_dword_c	(pci_bus, pci_dev, pci_func,0x10+reg_idx*4,0xFFFFFFFF);
	base_reg_region		=	read_pci_dword_c(pci_bus, pci_dev, pci_func,0x10+reg_idx*4)&0xFFFFFFF0;
	write_pci_dword_c	(pci_bus, pci_dev, pci_func,0x10+reg_idx*4,base_reg_val);
	base_reg_region_size=	~base_reg_region+1;

	if(base_reg_region==0x00)
		return 0;

	if(base_reg_region==0xFFFFFFFF)
		return 0;

	kernel_log			(kernel_log_id,"pci ressource : ");
	writeint			(base_reg_addr,16);
	writestr			(" ");
	writeint			(base_reg_region_size,10);
	writestr			("\n");

	*base=base_reg_addr;
	*size=base_reg_region_size;

	return 1;


}


void __stdcall NdisMQueryAdapterResources(NDIS_STATUS *status, pci_device *dev,
	 CM_PARTIAL_RESOURCE_LIST *resource_list, UINT *size)
{
	int							n_bar;
	unsigned int				pci_bus,pci_dev,pci_func;
	unsigned int				list_cnt;
	
	
	ndis_module					*mod;
	kernel_log	(kernel_log_id,"ndis query resource device [");
	writeint	(dev->dev_id,16);
	writestr	(" ");
	writeint	(*size,10);
	writestr	("]\n");

	if((*size)>0)
	{
		
		mod				=	find_module_by_dev(dev);
		/*
		allocate_new_zone	(0x00,(*size),&descriptors);
		mod->list		=	get_zone_ptr(&descriptors,0);
		*/
		mod->list		=	resource_list;
	}

	list_cnt		=	0;
	n_bar			=	0;
	while(n_bar<6)
	{
		unsigned int	base,reg_size,type;

		if(ndis_read_pci_bar(dev,n_bar,&base,&reg_size,&type)==1)
		{
			if((*size)>0)
			{
				mod->list->PartialDescriptors[list_cnt].ShareDisposition	=	CmResourceShareDeviceExclusive;

				if(type&0x01)
				{
					mod->list->PartialDescriptors[list_cnt].Type			=	CmResourceTypePort;
					mod->list->PartialDescriptors[list_cnt].Flags			=	CM_RESOURCE_PORT_IO;
				}
				else
				{
					mod->list->PartialDescriptors[list_cnt].Type			=	CmResourceTypeMemory;
					mod->list->PartialDescriptors[list_cnt].Flags			=	CM_RESOURCE_MEMORY_READ_WRITE;
				}
				mod->list->PartialDescriptors[list_cnt].u.Generic.Start		=base;
				mod->list->PartialDescriptors[list_cnt].u.Generic.Length		=reg_size;
			}
			list_cnt++;
		}
		n_bar++;
	}
	
	if(*size==0)
	{
		unsigned int buf_size;
		buf_size	=	sizeof(CM_PARTIAL_RESOURCE_LIST)+((list_cnt)*sizeof(CM_PARTIAL_RESOURCE_DESCRIPTOR));
		*size		=	buf_size;
		*status		=	NDIS_STATUS_SUCCESS;
		return;
	}
	
	pci_bus															=	(dev->pci_code>>8)	& 0xFF;
	pci_dev															=	(dev->pci_code>>3)	& 0x1F;
	pci_func														=	dev->pci_code		& 0x07;
	/*dev->irq														=	read_pci_byte_c		(pci_bus, pci_dev, pci_func, 0x3C);*/


	if (ndis_interupt_mode == 0)
	{
		mem_zone_ref pic = { PTR_NULL };

		if (tree_manager_find_child_node(&dev->pci_node, NODE_HASH("PIC"), NODE_BUS_DEVICE_IRQ_INFOS, &pic))
		{
			unsigned int int_line, int_sharable, int_triggering, int_polarity;
			tree_manager_get_child_value_i32(&pic, NODE_HASH("int line"), &int_line);

			mod->list->PartialDescriptors[list_cnt].Type = CmResourceTypeInterrupt;
			mod->list->PartialDescriptors[list_cnt].Flags = CM_RESOURCE_INTERRUPT_LEVEL_SENSITIVE;
			mod->list->PartialDescriptors[list_cnt].ShareDisposition = CmResourceShareShared;
			mod->list->PartialDescriptors[list_cnt].u.Interrupt.Level = int_line;
			mod->list->PartialDescriptors[list_cnt].u.Interrupt.Vector = int_line;
			mod->list->PartialDescriptors[list_cnt].u.Interrupt.Affinity = -1;

			release_zone_ref(&pic);
		}
	}
	else
	{
		mem_zone_ref apic = { PTR_NULL };

		if (tree_manager_find_child_node(&dev->pci_node, NODE_HASH("APIC"), NODE_BUS_DEVICE_IRQ_INFOS, &apic))
		{
			unsigned int int_line, int_sharable, int_triggering, int_polarity;
			tree_manager_get_child_value_i32(&apic, NODE_HASH("int line"), &int_line);

			if (!tree_manager_get_child_value_i32(&apic, NODE_HASH("Polarity"), &int_polarity))
				int_polarity = 0;

			if (!tree_manager_get_child_value_i32(&apic, NODE_HASH("Triggering"), &int_triggering))
				int_triggering = 0;

			if (!tree_manager_get_child_value_i32(&apic, NODE_HASH("Sharable"), &int_sharable))
				int_triggering = 0;

			mod->list->PartialDescriptors[list_cnt].Type = CmResourceTypeInterrupt;

			mod->list->PartialDescriptors[list_cnt].Flags = 0;

			if(int_triggering)
				mod->list->PartialDescriptors[list_cnt].Flags |= CM_RESOURCE_INTERRUPT_LEVEL_SENSITIVE;

			if (int_polarity)
				mod->list->PartialDescriptors[list_cnt].Flags |= CM_RESOURCE_INTERRUPT_LATCHED;

			if(int_sharable)
				mod->list->PartialDescriptors[list_cnt].ShareDisposition = CmResourceShareShared;
			else
				mod->list->PartialDescriptors[list_cnt].ShareDisposition = 0;

			mod->list->PartialDescriptors[list_cnt].u.Interrupt.Level = int_line;
			mod->list->PartialDescriptors[list_cnt].u.Interrupt.Vector = int_line;
			mod->list->PartialDescriptors[list_cnt].u.Interrupt.Affinity = -1;

			release_zone_ref(&apic);
		}
		
	}


	list_cnt++;

	mod->list->Version	=	1;
	mod->list->Revision	=	1;
	mod->list->Count	=	list_cnt;

	resource_list		=	mod->list;

	*status				=	NDIS_STATUS_SUCCESS;
	return;

}


NDIS_STATUS __stdcall NdisMRegisterIoPortRange(void **virt, struct ndis_mp_block *nmb, UINT start, UINT len)
{
	*virt = (void *)(ULONG_PTR)start;
	return NDIS_STATUS_SUCCESS;
}

NDIS_STATUS __stdcall NdisMMapIoSpace(void **virt, struct ndis_mp_block *nmb,
	 NDIS_PHY_ADDRESS phy_addr, UINT len)
{
	*virt=phy_addr;
	return NDIS_STATUS_SUCCESS;
}

ULONG __stdcall READ_PORT_ULONG	(ULONG_PTR port)
{
	return in_32_c(port);
}

void __stdcall WRITE_PORT_ULONG(ULONG_PTR port, ULONG value)
{
	out_32_c(port,value);
}


void __stdcall NdisReadNetworkAddress(NDIS_STATUS *status, void **addr, UINT *len,
	 struct ndis_mp_block *nmb)
{

	*len	= 0;
	*addr	= PTR_NULL;
	*status = NDIS_STATUS_SUCCESS;
}

void __stdcall NdisInitializeEvent(struct ndis_event *ndis_event)
{
	
	//KeInitializeEvent(&ndis_event->nt_event, NotificationEvent, 0);
}

void __stdcall NdisResetEvent(struct ndis_event *ndis_event)
{
	//KeResetEvent(&ndis_event->nt_event);
}


void __stdcall NdisSetEvent(struct ndis_event *ndis_event)
{
	//KeSetEvent(&ndis_event->nt_event, 0, 0);
}

void __stdcall NdisAllocateBufferPool(NDIS_STATUS *status, struct ndis_buffer_pool **pool_handle,
	 UINT num_descr)
{
	mem_zone_ref	pool;

	kernel_log	(kernel_log_id,"allocate buffer\n");

	pool.zone		=PTR_NULL;
	allocate_new_zone(0x00,num_descr*sizeof(mem_zone_ref),&pool);

	*pool_handle	= pool.zone;
	*status			= NDIS_STATUS_SUCCESS;
	
	return	;
}

void NdisFreeBufferPool(NDIS_HANDLE PoolHandle )
{
}

 


 void __stdcall NdisAllocatePacketPoolEx(NDIS_STATUS *status, struct ndis_packet_pool **pool_handle,
	 UINT num_descr, UINT overflowsize, UINT proto_rsvd_length)
{
	mem_zone_ref	pool;

	pool.zone		=PTR_NULL;
	allocate_new_zone(0x00,num_descr*sizeof(mem_zone_ref),&pool);
	
	*pool_handle = pool.zone;
	*status		 = NDIS_STATUS_SUCCESS;
}
 void __stdcall NdisFreePacketPool(NDIS_HANDLE PoolHandle )
 {
 }


 void __stdcall  NdisAllocatePacket(NDIS_STATUS	 *status,struct NDIS_PACKET	**Packet,struct ndis_packet_pool *pool_handle)
 {
	 mem_zone_ref	p_ref;
	 mem_size		p_size;
	 NDIS_PACKET	*packet;

	 if(initialised==1)
		kernel_log	(kernel_log_id,"allocate packet ");

	 
	 p_ref.zone	=	PTR_NULL;
	 p_size		=	sizeof(*packet) - 1 + PROTOCOL_RESERVED_SIZE_IN_PACKET + sizeof(NDIS_PACKET_OOB_DATA);
	 
	 allocate_new_zone(0x00,p_size,&p_ref);

	 packet								 			=	get_zone_ptr(&p_ref,0);
	 packet->Private.NdisPacketOobOffset 			=	p_size - sizeof(NDIS_PACKET_OOB_DATA);
	

	
	 *Packet	=	packet;
	 *status	=	NDIS_STATUS_SUCCESS;
 }

void __stdcall NdisFreePacket(struct NDIS_PACKET	*Packet)
{
 
}


void __stdcall NdisAllocatePacketPool(NDIS_STATUS *status, struct ndis_packet_pool **pool_handle,
	 UINT num_descr, UINT proto_rsvd_length)
{
	NdisAllocatePacketPoolEx(status, pool_handle, num_descr, 0,
				 proto_rsvd_length);
	return;
}


void __stdcall NdisAllocateBuffer(NDIS_STATUS *status, struct mdl **buffer,
	 struct ndis_buffer_pool *pool, void *virt, UINT length)
{
	mem_zone_ref	buf_desc;
	struct mdl		*buf_desc_ptr;
	

	buf_desc.zone	=PTR_NULL;
	allocate_new_zone(0x00,sizeof(struct mdl),&buf_desc);
		
	buf_desc_ptr =	get_zone_ptr(&buf_desc,0);
	MmInitializeMdl(buf_desc_ptr, virt, length);

	*buffer		=	buf_desc_ptr;
	*status		=	NDIS_STATUS_SUCCESS;
}


void __stdcall NdisFreeBuffer(struct mdl *buffer)
{

}
void __stdcall NdisMRegisterAdapterShutdownHandler(struct ndis_mp_block *nmb, void *ctx, void *func)
{

}

void __stdcall NdisMDeregisterAdapterShutdownHandler(NDIS_HANDLE MiniportHandle)
{

	return;
}


void __stdcall timer_call(void *ctx)
{
	kernel_log	(kernel_log_id,"timer call \n");
	ttt.func(0,ctx,0,0);

}

void __stdcall NdisSetTimer(struct ndis_timer *timer, UINT duetime_ms)
{

	kernel_log	(kernel_log_id,"set timer call ");
	writeint	(duetime_ms,10);
	writestr	("\n");
	//add_timer_func(timer_call,ttt.ctx,duetime_ms);

	return;
}


void __stdcall NdisMInitializeTimer(struct ndis_mp_timer *timer, struct ndis_mp_block *nmb,
	 DPC func, void *ctx)
{
	kernel_log	(kernel_log_id,"timer init \n");
	ttt.func	=func;
	ttt.ctx		=ctx;
	return;
}

void __stdcall NdisMCancelTimer(struct ndis_mp_timer *Timer,BOOLEAN *TimerCanceled)
{
	*TimerCanceled=1;
}

enum ndis_request_type {
	NdisRequestQueryInformation, NdisRequestSetInformation,
	NdisRequestQueryStatistics, NdisRequestOpen, NdisRequestClose,
	NdisRequestSend, NdisRequestTransferData, NdisRequestReset,
	NdisRequestGeneric1, NdisRequestGeneric2, NdisRequestGeneric3,
	NdisRequestGeneric4
};



/* MiniportRequest(Query/Set)Information */
NDIS_STATUS mp_request(	enum ndis_request_type request,
						ndis_module *mod, ndis_oid oid,
						void *buf, ULONG buflen, ULONG *written, ULONG *needed)
{
	NDIS_STATUS res;
	ULONG		w, n;
	struct		miniport *mp;
			
	
	if (!written)
		written = &w;
	if (!needed)
		needed = &n;

	//mp = &wnd->wd->driver->ndis_driver->mp;
	
	//prepare_wait_condition(wnd->ndis_req_task, wnd->ndis_req_done, 0);
	//irql = serialize_lock_irql(wnd);
	//assert_irql(_irql_ == DISPATCH_LEVEL);
	
	switch (request) {
	case NdisRequestQueryInformation:
		
		res = mp->queryinfo( mod->mp_ctx, oid, buf, buflen, written, needed);
		break;
	case NdisRequestSetInformation:
		
		res = mp->setinfo	( mod->mp_ctx, oid, buf,buflen, written, needed);
		break;
	default:
		res = NDIS_STATUS_NOT_SUPPORTED;
		break;
	}
	return res;
}



NDIS_STATUS mp_query_info(ndis_module *wnd,
					ndis_oid oid, void *buf, ULONG buflen,
					ULONG *written, ULONG *needed)
{
	return mp_request(NdisRequestQueryInformation, wnd, oid,
			  buf, buflen, written, needed);
}

NDIS_STATUS mp_set_info(ndis_module *wnd,
				      ndis_oid oid, void *buf, ULONG buflen,
				      ULONG *written, ULONG *needed)
{
	return mp_request(NdisRequestSetInformation, wnd, oid,
			  buf, buflen, written, needed);
}

NDIS_STATUS mp_query(ndis_module *wnd, ndis_oid oid,
				   void *buf, ULONG buflen)
{
	return mp_request(NdisRequestQueryInformation, wnd, oid,
			  buf, buflen, NULL, NULL);
}

NDIS_STATUS mp_query_int(ndis_module *wnd,
				       ndis_oid oid, ULONG *data)
{
	return mp_request(NdisRequestQueryInformation, wnd, oid,
			  data, sizeof(ULONG), NULL, NULL);
}

void __stdcall networkcardready(void *drv_id)
{
	ndis_module			*mod;
	struct miniport		*mp;
	mem_ptr				buff_ptr;
	unsigned int		st;
	int					ret;
	NDIS_PACKET_OOB_DATA *oob_data;
	
	mod								=	find_module_by_id(drv_id);
	mp								=	((struct miniport *)(get_zone_ptr(&mod->mp,0)));


	
	allocate_new_zone(0x0,4096,&buf_zone);
	buff_ptr=get_zone_ptr(&buf_zone,0);

	NdisAllocateBuffer	(&st,&buffer,0x0,buff_ptr,4096);
	NdisAllocatePacket	(&st,&pack,0x0);

	pack->Private.PhysicalCount	=	1;
	pack->Private.TotalLength	=	4096;
	pack->Private.Head			=	buffer;
	pack->Private.Tail			=	buffer;
	pack->Private.Pool			=	PTR_NULL;
	pack->Private.Count			=	1;
	pack->Private.Flags			=	0;
	pack->Private.ValidCounts	=	1;
	pack->Private.NdisPacketFlags=	0;
	pack->Private.NdisPacketOobOffset=0;

	if(mp->send_packets)
	{
		kernel_log	(kernel_log_id,"send packets [");
		writeint	(mod->driver_id,16);
		writestr	("]\n");

		pack_ptr[0]=pack;
		mp->send_packets(mod->mp_ctx,pack_ptr,1);
	}
	else if(mp->send)
	{
		
		kernel_log	(kernel_log_id,"send packet [");
		writeint	(mod->driver_id,16);
		writestr	(" ");
		writeptr	(mp->send);
		writestr	("]\n");

		oob_data				=	NDIS_OOB_DATA_FROM_PACKET(pack);
		oob_data->Status		=	NDIS_STATUS_NOT_RECOGNIZED;
	//	oob_data->HeaderSize	=	sizeof(struct ethhdr);
		ret						=	mp->send(mod->mp_ctx,pack,pack->Private.Flags);

	}
	else if(mp->co_send_packets)
	{
		kernel_log	(kernel_log_id,"co send packet [");
		writeint	(mod->driver_id,16);
		writestr	("]\n");
		ret=mp->co_send_packets(mod->mp_ctx,&pack,1);
	}
	else
	{
		ret=0;
	}

	kernel_log	(kernel_log_id,"ret : [");
	writeint	(ret,16);
	writestr	("]\n");
}

OS_INT_C_FUNC(unsigned int) interupt_handler(unsigned int drv_id)
{
	ndis_module *mod;
	BOOLEAN InterruptRecognized,QueueMiniportHandleInterrupt;
	struct miniport *mp;

	InterruptRecognized				=	1;
	QueueMiniportHandleInterrupt	=	1;

	mod								=	find_module_by_id(drv_id);
	mp								=	((struct miniport *)(get_zone_ptr(&mod->mp,0)));
	kernel_log	(kernel_log_id,"interupt for driver [");
	writeint	(mod->driver_id,16);
	writestr	("] ");
	if(irq_shared)
		writestr	("shared");
	else
		writestr	("not shared");

	writestr("\n");

	if(initialised==0)
	{
		mp->isr	(&InterruptRecognized,&QueueMiniportHandleInterrupt,mod->mp_ctx);
		return 0;
	}

	
	if(irq_shared)
		mp->isr	(&InterruptRecognized,&QueueMiniportHandleInterrupt,mod->mp_ctx);
	else
	{
		InterruptRecognized				=	1;
		QueueMiniportHandleInterrupt	=	1;
		mp->disable_interrupt	(mod->mp_ctx);
	}
	
	if((InterruptRecognized==1)&&(QueueMiniportHandleInterrupt==1))
	{
		kernel_log	(kernel_log_id,"interupt handled\n");
		mp->handle_interrupt(mod->mp_ctx);
		if(attributes & NDIS_ATTRIBUTE_DESERIALIZE)
		{
			if (mp->enable_interrupt)
				mp->enable_interrupt(mod->mp_ctx);
		}

		//add_timer_func(&networkcardready,mod->driver_id,0);
	}
	
	kernel_log	(kernel_log_id,"interupt end\n");

	return 0;
		
}

NDIS_STATUS __stdcall NdisMRegisterInterrupt(struct ndis_mp_interrupt *mp_interrupt,struct driver_object *drv_obj, UINT ivector, UINT level, BOOLEAN req_isr, BOOLEAN shared, enum kinterrupt_mode mode)
{
	pci_device		*dev;
	ndis_module		*mod;
	struct miniport *mp;

	
	mod			=	find_module(drv_obj);
	dev			=	mod->pci_dev;
	mp			=	((struct miniport *)(get_zone_ptr(&mod->mp,0)));
	//mod->
	irq_shared	=	shared;
	irq_req_isr	=	req_isr;

	if (ndis_interupt_mode == 1)
	{
		unsigned int vector = APIC_BASE + ivector;

		if (mode & CM_RESOURCE_INTERRUPT_LEVEL_SENSITIVE)
			vector |= APIC_LEVEL_TRIGERING;

		if (mode & CM_RESOURCE_INTERRUPT_LATCHED)
			vector |= APIC_POLARITY_LOW;

		apic_enable_irq_c(ivector, vector, 0);
	}

	setup_interupt_c(ivector, interupt_handler,mod->driver_id);

	kernel_log	(kernel_log_id,"ndis int [");
	writeint	(mod->driver_id,16);
	writestr	("] vector ");
	writeint	(ivector,16);
	writestr	(" ");
	writeint	(irq_shared,10);
	writestr	(" ");
	writeint	(req_isr,10);
	writestr	("\n");
		
	return NDIS_STATUS_SUCCESS;
}

void NdisMDeregisterInterrupt( struct ndis_mp_interrupt *Interrupt)
{
	//setup_interupt_c(vector,PTR_NULL,0);
}

mem_ptr		 dma_ptr		=0xCDCDCDCD;
mem_ptr		 dma_ptr_phys	=0xCDCDCDCD;
mem_zone_ref dma_buff		={0xCDCDCDCD};
void __stdcall NdisMAllocateSharedMemory(struct ndis_mp_block *nmb, ULONG size,
	 BOOLEAN cached, void **virt, NDIS_PHY_ADDRESS *phys)
{
	dma_buff.zone=PTR_NULL;
	allocate_new_zone(0x00,size,&dma_buff);

	dma_ptr		 =	get_zone_ptr(&dma_buff,0);
	dma_ptr_phys =  mem_add(dma_ptr,0x00000);

	*virt		 =	dma_ptr;
	*phys		 =	dma_ptr_phys;
}

	 
NDIS_STATUS __stdcall NdisMAllocateMapRegisters(struct ndis_mp_block *nmb, UINT dmachan,
	 NDIS_DMA_SIZE dmasize, ULONG basemap, ULONG max_buf_size)
{

	return NDIS_STATUS_SUCCESS;
}


void __stdcall NdisMSetAttributesEx(struct driver_object *drv_obj, void *mp_ctx,UINT hangcheck_interval, UINT s_attributes, ULONG adaptertype)
{

	pci_device	*dev;
	ndis_module *mod;

	mod				=	find_module(drv_obj);
	dev				=	mod->pci_dev;
	mod->mp_ctx		=	mp_ctx;
	attributes		=	s_attributes;
	
	if (attributes & NDIS_ATTRIBUTE_BUS_MASTER)
	{
		unsigned int orig_pci_value,flags;
		int			 pci_bus,pci_dev,pci_fn;

		kernel_log	(kernel_log_id,"set pci bus master [");
		writeptr	(drv_obj);
		writestr	(" ");
		writestr	(dev->driver_name);
		writestr	("]\n");

		pci_bus			=	(dev->pci_code>>8)	& 0xFF;
		pci_dev			=	(dev->pci_code>>3)	& 0x1F;
		pci_fn			=	dev->pci_code		& 0x07;

		orig_pci_value	=	read_pci_word_c	(pci_bus,pci_dev,pci_fn, PCI_PCICMD);
		flags			=	 PCI_PCICMD_BME;
		write_pci_word_c	(pci_bus,pci_dev,pci_fn, PCI_PCICMD, orig_pci_value|flags);
	}

	return;
	
	/*
	ENTER1("%p, %p, %d, %08x, %d", nmb, mp_ctx, hangcheck_interval,
	       attributes, adaptertype);
	wnd = nmb->wnd;
	nmb->mp_ctx = mp_ctx;
	wnd->attributes = attributes;

	if ((attributes & NDIS_ATTRIBUTE_BUS_MASTER) &&
	    wrap_is_pci_bus(wnd->wd->dev_bus))
		pci_set_master(wnd->wd->pci.pdev);

	if (hangcheck_interval > 0)
		wnd->hangcheck_interval = 2 * hangcheck_interval * HZ;
	else
		wnd->hangcheck_interval = 2 * HZ;

	EXIT1(return);
	*/
}




void __stdcall NdisOpenConfiguration(NDIS_STATUS *status, mem_zone_ref **conf_handle,mem_zone_ref *handle)
{
	*conf_handle	= handle;
	*status			= NDIS_STATUS_SUCCESS;
}


void __stdcall NdisReadConfiguration(
		NDIS_STATUS *status, ndis_configuration_parameter **param,
		pci_device *pci_dev, struct unicode_string *key, enum ndis_parameter_type type)
{
	char			ckey[64];
	char			value[64];
	mem_zone_ref	reg_section_node;
	mem_zone_ref	reg_val_node;
	mem_zone_ref	*conf_ptr;
	int				n_reg_sec;
	
	uni_str_to_char(key,ckey,64);
	

	conf_ptr=&pci_dev->driver_conf_node;

	memset_c(value,0,64);

	reg_section_node.zone=PTR_NULL;
	n_reg_sec=0;
	while((tree_manager_get_child_at(conf_ptr,n_reg_sec,&reg_section_node))==1)
	{
		reg_val_node.zone=PTR_NULL;
		
		if(tree_node_find_child_by_name(&reg_section_node,ckey,&reg_val_node)==1)
		{
			mem_zone_ref	reg_def_val_node;

			reg_def_val_node.zone=PTR_NULL;
			if(tree_node_find_child_by_name(&reg_val_node,"default",&reg_def_val_node)==1)
			{
				char *cptr;

				cptr=tree_mamanger_get_node_data_ptr(&reg_def_val_node,0);
				if(cptr!=PTR_NULL)
					strcpy_s						(value,64,cptr);

				release_zone_ref				(&reg_def_val_node);
			}

			//tree_manager_dump_node_rec		(&reg_val_node,0);
			release_zone_ref				(&reg_val_node);
			break;
		}
		release_zone_ref(&reg_section_node);
		n_reg_sec++;
	}

	if(strlen_c(value)>0)
	{
		ndis_configuration_parameter *param_out_ptr;

		kernel_log	(kernel_log_id,"ndis read conf ");
		writestr	(ckey);
		writestr	(" ");
		writeint	(type,16);
		writestr	(" conf value [");
		writestr	(value);
		writestr	("]\n");

		param_out_ptr					=&param_out;

		param_out_ptr->ParameterType	=type;

		if(type==NdisParameterString)
			create_uni_str(&param_out_ptr->ParameterData.StringData,value);
		else
			param_out_ptr->ParameterData.IntegerData=strtol_c(value,PTR_NULL,10);


		*status = NDIS_STATUS_SUCCESS ;
		*param	= param_out_ptr;

	}
	else
	{
		*status = NDIS_STATUS_FAILURE;
		*param	= NULL;
	}

	
		

	return;
	
}


void __stdcall NdisCloseConfiguration(void *handle)
{
	/* instead of freeing all configuration parameters as we are
	 * supposed to do here, we free them when the device is
	 * removed */

	kernel_log	(kernel_log_id,"ndis conf closed \n");
	
	return;
}



void __stdcall NdisWriteErrorLogEntry(struct driver_object *drv_obj, ULONG error, ULONG count, ...)
{
	kernel_log	(kernel_log_id,"ndis write error log entry ");
	writeint	(error,16);
	writestr	(" ");
	writeint	(count,16);
	writestr	("\n");
	/*
	int i;
	ULONG code;
	
	va_list args;
	va_start(args, count);
	ERROR("log: %08X, count: %d, return_address: %p",error, count, __builtin_return_address(0));
	for (i = 0; i < count; i++) {
		code = va_arg(args, ULONG);
		ERROR("code: 0x%x", code);
	}
	va_end(args);
	
	EXIT2(return);
	*/
}


NDIS_STATUS __stdcall NdisAllocateMemoryWithTag(void **dest, UINT length, ULONG tag)
{
	
	*dest=ExAllocatePoolWithTag(NonPagedPool,length,tag);

	return NDIS_STATUS_SUCCESS;

}
void __stdcall NdisFreeMemory(void *addr, UINT length_tag, UINT flags)
{
	ExFreePool(addr);
}


void __stdcall NdisMFreeSharedMemory(struct ndis_mp_block *nmb, ULONG size, BOOLEAN cached,void *virt, NDIS_PHY_ADDRESS addr)
{
	return;
}
/*
void NdisMQueryAdapterResources				(){}
void NdisQueryBuffer						(){}
void NdisMStartBufferPhysicalMapping		(){}
void NDIS_BUFFER_TO_SPAN_PAGES				(){}
void NdisQueryBufferOffset					(){}
void NdisMCompleteBufferPhysicalMapping 	(){}
void NdisAdjustBufferLength					(){}
void NdisUnchainBufferAtFront				(){}
void NdisFreePacket							(){}
void NdisAllocatePacket						(){}
void NdisAllocatePacketPool					(){}
void NdisMFreeSharedMemory					(){}
void NdisMAllocateSharedMemory				(){}
void NdisFreeBuffer							(){}
void NdisAllocateBuffer						(){}
void NdisAllocateBufferPool					(){}
void NdisFreePacketPool 					(){}
void NdisFreeBufferPool 					(){}
void NdisTerminateWrapper 					(){}
void NdisSetEvent							(){}
void NdisInitializeEvent 					(){}
void NdisMInitializeTimer					(){}
void NdisMRegisterAdapterShutdownHandler	(){}
void NdisMRegisterInterrupt 				(){}
void NdisReadNetworkAddress 				(){}
void NdisMSetAttributesEx					(){}
void NdisCloseConfiguration					(){}
void NdisOpenConfiguration					(){}
void NdisMDeregisterInterrupt				(){}
void NdisWaitEvent							(){}
void NdisMCancelTimer						(){}
void NdisMDeregisterAdapterShutdownHandler	(){}
void NdisSetTimer							(){}
void NdisMRegisterIoPortRange				(){}
void NdisMMapIoSpace						(){}
void NdisMAllocateMapRegisters				(){}
void NdisMFreeMapRegisters					(){}
void NdisMDeregisterIoPortRange 			(){}
void NdisMUnmapIoSpace						(){}
void NdisWritePciSlotInformation			(){}
void NdisReadPciSlotInformation				(){}
void NdisReadConfiguration					(){}
void NdisAllocateMemoryWithTag				(){}
void NdisFreeMemory							(){}
void NdisWriteErrorLogEntry					(){}
void NdisMPciAssignResources				(){}
*/

