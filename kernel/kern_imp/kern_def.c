#ifdef __GNUC__
	#define ASM_API_FUNC __attribute__((__cdecl))
	#define DLL_EXPORT 
#endif

#ifdef _MSC_VER
	#define DLL_EXPORT   __declspec(dllexport) 
	#define ASM_API_FUNC __cdecl
	int				_fltused	=0;
	unsigned int __stdcall _DllMainCRTStartup(int *_a,int *_b,int *_c){return 1;}
#endif
struct gdt_t
{
	unsigned short		limit;
	void				*addr;
};

typedef struct gdt_t *gdt_ptr;
DLL_EXPORT unsigned int ASM_API_FUNC read_pci_config_c				(unsigned int bus,unsigned int dev,unsigned int fn,unsigned int *config_out){ return 0xFFFFFFFF; }
/*
DLL_EXPORT unsigned int ASM_API_FUNC pci_get_vendor_string_c			(unsigned short vendor_id, char *dst_string,unsigned int len){return 0;}
DLL_EXPORT unsigned int ASM_API_FUNC pci_get_vendor_device_string_c	(unsigned short vendor_id,unsigned short device_id, char *dst_vendor_string,unsigned int dst_vendor_len, char *dst_device_string,unsigned int dst_device_len){ return 0xFFFFFFFF; }
*/
DLL_EXPORT int				ASM_API_FUNC draw_car_c						(const char *string){ return 0; }
DLL_EXPORT int				ASM_API_FUNC draw_ncar_c					(const char *string,unsigned int p){ return 0; }
DLL_EXPORT int				ASM_API_FUNC draw_dword_c					(unsigned int dword){ return 0; }
DLL_EXPORT int				ASM_API_FUNC draw_byte_c					(unsigned int dword){ return 0; }
DLL_EXPORT void				ASM_API_FUNC set_text_color_c				(unsigned char color){return ;}
DLL_EXPORT unsigned char	ASM_API_FUNC get_text_color_c				(){return 0x07;}
DLL_EXPORT unsigned int 	ASM_API_FUNC read_pci_dword_c				(int bus, int device, int func,int reg ) {return 0;}
DLL_EXPORT void				ASM_API_FUNC write_pci_dword_c				(int bus, int device, int func, int reg , int value){}
DLL_EXPORT unsigned int 	ASM_API_FUNC read_pci_word_c					(int bus, int device, int func,int reg ) {return 0;}
DLL_EXPORT void				ASM_API_FUNC write_pci_word_c				(int bus, int device, int func, int reg , int value){}
DLL_EXPORT unsigned int 	ASM_API_FUNC read_pci_byte_c					(int bus, int device, int func,int reg ) {return 0;}
DLL_EXPORT void				ASM_API_FUNC write_pci_byte_c				(int bus, int device, int func, int reg , int value){}
DLL_EXPORT void				ASM_API_FUNC delay1_4ms_c					(){}
DLL_EXPORT void				ASM_API_FUNC delay_x_ms_c					(unsigned int ms){};
DLL_EXPORT unsigned int		ASM_API_FUNC get_system_time_c				(void *t){return 0;}
DLL_EXPORT unsigned int		ASM_API_FUNC get_system_time_diff_c			(void *t){return 0;}
DLL_EXPORT void				ASM_API_FUNC pci_enable_device_c				(int bus, int device, int func ){} ;
DLL_EXPORT unsigned int		ASM_API_FUNC calc_crc32_c					(char *string,unsigned int len){return 0;};

DLL_EXPORT void			 	ASM_API_FUNC out_32_c						(unsigned int v){}
DLL_EXPORT void				ASM_API_FUNC out_16_c						(unsigned short v){}
DLL_EXPORT void		 		ASM_API_FUNC out_8_c							(unsigned char v){}

DLL_EXPORT unsigned int 	ASM_API_FUNC in_32_c							(){return 0;}
DLL_EXPORT unsigned short 	ASM_API_FUNC in_16_c							(){return 0;}
DLL_EXPORT unsigned char 	ASM_API_FUNC in_8_c							(){return 0;}
DLL_EXPORT void			 	ASM_API_FUNC dump_ivt_c						(){return ;}

DLL_EXPORT unsigned int 	ASM_API_FUNC rep_in_byte						(unsigned int port,void *buffer,unsigned int byte_cnt){return 0;}
DLL_EXPORT unsigned short	ASM_API_FUNC rep_in_word						(unsigned int port,void *buffer,unsigned int word_cnt){return 0;}
DLL_EXPORT unsigned char 	ASM_API_FUNC rep_in_dword					(unsigned int port,void *buffer,unsigned int dword_cnt){return 0;}

DLL_EXPORT unsigned int 	ASM_API_FUNC rep_out_byte					(unsigned int port,void *buffer,unsigned int byte_cnt){return 0;}
DLL_EXPORT unsigned int		ASM_API_FUNC rep_out_word					(unsigned int port,void *buffer,unsigned int word_cnt){return 0;}
DLL_EXPORT unsigned int 	ASM_API_FUNC rep_out_dword					(unsigned int port,void *buffer,unsigned int dword_cnt){return 0;}

DLL_EXPORT void	*			ASM_API_FUNC kernel_memory_map_c			(unsigned int size){return 0;};

DLL_EXPORT unsigned int 	ASM_API_FUNC tpo_mod_imp_func_addr_c		(unsigned int crc_dll,unsigned int crc_func){return 0xFFFFFFFF;}
DLL_EXPORT unsigned int 	ASM_API_FUNC tpo_mod_add_func_addr_c		(unsigned int crc_dll,unsigned int crc_func,unsigned int func_addr){return 0xFFFFFFFF;};

DLL_EXPORT unsigned int 	ASM_API_FUNC tpo_add_mod_c					(unsigned int mod_hash,unsigned int deco_type	,unsigned int string_table_addr){return 0xFFFFFFFF;};
DLL_EXPORT unsigned int 	ASM_API_FUNC tpo_mod_add_section_c			(unsigned int mod_idx ,unsigned int section_addr,unsigned int section_size){return 0xFFFFFFFF;};
DLL_EXPORT unsigned int 	ASM_API_FUNC tpo_mod_add_func_c				(unsigned int mod_idx ,unsigned int func_addr	,unsigned int func_type,unsigned int string_id){return 0xFFFFFFFF;};

DLL_EXPORT unsigned int 	ASM_API_FUNC apic_enable_irq_c				(unsigned int input, unsigned int vector,unsigned int cpunum) { return 0; }
DLL_EXPORT unsigned int 	ASM_API_FUNC init_lapic_c					() { return 0; }
DLL_EXPORT unsigned int 	ASM_API_FUNC init_apic_c					() { return 0; }
DLL_EXPORT unsigned int 	ASM_API_FUNC disable_apic_c					() { return 0; }

DLL_EXPORT unsigned int 	ASM_API_FUNC get_interupt_mode_c() { return 0; }



DLL_EXPORT int				ASM_API_FUNC setup_interupt_c				(unsigned int interupt, unsigned int function){return 0;}
DLL_EXPORT unsigned char *  ASM_API_FUNC create_code_stub				(unsigned int data,unsigned int addr){return 0;}

DLL_EXPORT unsigned int		ASM_API_FUNC	bios_reset_drives_c				(){return 0;}
DLL_EXPORT unsigned int		ASM_API_FUNC	bios_get_boot_device_c			(){return 0;}
DLL_EXPORT unsigned char *  ASM_API_FUNC	bios_read_sector_c				(unsigned int drv,unsigned int cyl,unsigned int hd,unsigned int sec){return 0;}
DLL_EXPORT unsigned int		ASM_API_FUNC	read_bios_disk_geom_c			(unsigned int drv_id,struct bios_drive_t *infos){return 0;}
DLL_EXPORT unsigned int		ASM_API_FUNC	bios_get_sector_data_ptr_c		(){return 0;}

DLL_EXPORT unsigned int		ASM_API_FUNC	bios_get_vesa_info_c			(){return 0;}
DLL_EXPORT unsigned int		ASM_API_FUNC	bios_get_vga_info_c				(){return 0;}
DLL_EXPORT void*			ASM_API_FUNC	bios_get_vesa_info_ptr_c		(){return 0;}

DLL_EXPORT unsigned int		ASM_API_FUNC	bios_get_vesa_mode_info_c		(unsigned int idx){return 0;}
DLL_EXPORT unsigned char *	ASM_API_FUNC	bios_get_vesa_mode_info_ptr_c	(){return 0;}
DLL_EXPORT unsigned int		ASM_API_FUNC	bios_set_vesa_mode_c			(unsigned int idx){return 0;}
DLL_EXPORT unsigned int		ASM_API_FUNC	bios_set_vga_mode_c				(unsigned int idx){return 0;}
DLL_EXPORT void				ASM_API_FUNC	toggle_output_buffer_c			(unsigned int on){}

DLL_EXPORT unsigned int		ASM_API_FUNC	set_output_buffer_addr_c		(char *char_buffer,unsigned int *line_infos){return 0;}
DLL_EXPORT const short   *	ASM_API_FUNC	get_output_buffer_line_c		(unsigned int line_idx,unsigned int *line_len){return 0;}

DLL_EXPORT unsigned int		ASM_API_FUNC	get_num_output_buffer_line_c	(){return 0;}
DLL_EXPORT void				ASM_API_FUNC	set_char_buffer_ofset_c			(unsigned int ofset){return;}
DLL_EXPORT unsigned int		ASM_API_FUNC	get_char_buffer_ofset_c			(){return 0;}


DLL_EXPORT  unsigned int	ASM_API_FUNC	bios_reset_mouse_c			(){return 0;}
DLL_EXPORT unsigned int		ASM_API_FUNC	bios_enable_mouse_c			(){return 0;}
DLL_EXPORT short *			ASM_API_FUNC	bios_get_mouse_status_c		(){return 0;}

DLL_EXPORT unsigned int		ASM_API_FUNC	set_mmx_protect_c			(unsigned int on){return 0;}

DLL_EXPORT unsigned int		ASM_API_FUNC	bios_get_mem_map_c			(struct bios_memory_map_t **out_data){return 0;}
DLL_EXPORT void*			ASM_API_FUNC	bios_get_edba_c				() { return 0; }
DLL_EXPORT void*			ASM_API_FUNC	acpi_get_rdsp_c				() { return 0; }

DLL_EXPORT	unsigned int	ASM_API_FUNC	get_gdt_ptr_c				(gdt_ptr out_gdt){return 0;}
DLL_EXPORT	unsigned int	ASM_API_FUNC	set_gdt_ptr_c				(unsigned int size,void *entries){return 0;}
DLL_EXPORT	unsigned int	ASM_API_FUNC	do_bios_interupt_c			(unsigned int itr){return 0;}


DLL_EXPORT	struct kern_mod_fn_t	*ASM_API_FUNC 	tpo_get_fn_entry_idx_c			(unsigned int mod_hash,unsigned int idx_func){return 0;}
DLL_EXPORT	struct kern_mod_fn_t	*ASM_API_FUNC 	tpo_get_fn_entry_hash_c			(unsigned int mod_hash,unsigned int crc_func){return 0;}
DLL_EXPORT	struct kern_mod_fn_t	*ASM_API_FUNC 	tpo_get_fn_entry_name_c			(unsigned int mod_idx,unsigned int mod_hash,unsigned int str_idx,unsigned int deco_type){return 0;}
DLL_EXPORT	struct kern_mod_t		*ASM_API_FUNC 	tpo_get_mod_entry_hash_c		(unsigned int mod_hash){return 0;}
DLL_EXPORT	struct kern_mod_t		*ASM_API_FUNC 	tpo_get_mod_entry_idx_c			(unsigned int idx){return 0;}
DLL_EXPORT	struct kern_mod_sec_t	*ASM_API_FUNC 	tpo_get_mod_sec_idx_c			(unsigned int idx){return 0;}

DLL_EXPORT unsigned int 			ASM_API_FUNC	tpo_calc_exp_func_hash_c		(unsigned int mod_idx ,unsigned int string_id){return 0;}

DLL_EXPORT unsigned int 			ASM_API_FUNC	tpo_calc_exp_func_hash_name_c	(const char *func_name ,unsigned int deco_type){return 0;}
DLL_EXPORT unsigned int 			ASM_API_FUNC	tpo_calc_imp_func_hash_name_c	(const char *func_name ,unsigned int src_deco_type,unsigned int deco_type){return 0;}

DLL_EXPORT void			 			ASM_API_FUNC	next_task_c						(){return ;}
DLL_EXPORT void			 			ASM_API_FUNC	dump_task_infos_c				(){return ;}

DLL_EXPORT void			 			ASM_API_FUNC	pic_init_c						(){return ;}
DLL_EXPORT unsigned int				ASM_API_FUNC	get_cpu_frequency_c				(){return 0;}
DLL_EXPORT void			 			ASM_API_FUNC	pic_set_irq_mask_c				(unsigned short mask){return ;}
DLL_EXPORT void						ASM_API_FUNC	pic_enable_irq_c				(unsigned int on){return ;}
DLL_EXPORT unsigned int				ASM_API_FUNC	compare_z_exchange_c			(unsigned int *data,unsigned int new_value){return 0;}
DLL_EXPORT unsigned int				ASM_API_FUNC	fetch_add_c						(unsigned int *data,int new_value){return 0;}

DLL_EXPORT unsigned int				ASM_API_FUNC	swap_nz_array_c				(unsigned int *first,unsigned int *last,unsigned int *value){return 0;}

DLL_EXPORT unsigned int				ASM_API_FUNC	set_cpu_flags_c				(unsigned int flags){return 0;}



DLL_EXPORT unsigned int				cur_task_id=0xFFFFFFFF;