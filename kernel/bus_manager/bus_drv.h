#ifndef BUS_DRIVER_API
 #define BUS_DRIVER_API C_IMPORT
#endif

typedef struct
{
char								name[32];
unsigned int						bus_id;
unsigned int						controller_bus_id;
unsigned int						controller_dev_id;
unsigned int						bus_type;
tpo_mod_file						bus_tpo_file;
mem_zone_ref						bus_dev_str_list;
mem_zone_ref						bus_root_node;
mem_zone_ref						drivers_root_node;
}bus_driver;

typedef enum 
{
DEVICE_TYPE_AUDIO 	 	=0x00000001,
DEVICE_TYPE_VGA	  	 	=0x00000002,
DEVICE_TYPE_USB   	 	=0x00000003,

DEVICE_TYPE_KEYBOARD 	=0x00000004,

DEVICE_TYPE_MOUSE	 	=0x00000005,
DEVICE_TYPE_JOYSTICK 	=0x00000006,
DEVICE_TYPE_GAMEPAD	 	=0x00000007,
DEVICE_TYPE_DIGITIZER	=0x00000008,
DEVICE_TYPE_STORAGE	 	=0x00000009
}device_type_t;

typedef int	C_API_FUNC device_event_listener_func	(device_type_t dev_type,const mem_zone_ref *dev_event);
typedef device_event_listener_func *device_event_listener_func_ptr;


typedef struct
{
	device_type_t						dev_type;
	device_event_listener_func_ptr		listener_fnc;
}dev_event_listener_t;

BUS_DRIVER_API int						C_API_FUNC bus_manager_init						(unsigned int int_mode);
BUS_DRIVER_API unsigned int				C_API_FUNC bus_manager_load_bus_driver			(const char *system_name,const char *drv_file);
BUS_DRIVER_API void						C_API_FUNC bus_manager_set_bus_driver_ctrler	(unsigned int bus_drv_id,unsigned int bus_id,unsigned int dev_id);
BUS_DRIVER_API void						C_API_FUNC bus_manager_scan_bus_drivers			(unsigned int bus_drv_id,const char *fs_name,const char *dir_name);
BUS_DRIVER_API unsigned int				C_API_FUNC bus_manager_get_bus_driver_idx		(unsigned int bus_driver_id);

BUS_DRIVER_API void			 			C_API_FUNC bus_manager_parse_bus				(unsigned int bus_drv_id);
BUS_DRIVER_API void			 			C_API_FUNC bus_manager_scan_bus					(unsigned int bus_drv_id);
BUS_DRIVER_API unsigned	int	 			C_API_FUNC bus_manager_find_bus_by_type			(unsigned int type,unsigned int index);

BUS_DRIVER_API unsigned int	 			C_API_FUNC bus_manager_find_device_by_type		(unsigned int bus_drv_id,unsigned int type,unsigned int index);
BUS_DRIVER_API unsigned int	 			C_API_FUNC bus_manager_get_device_driver		(unsigned int bus_driver_id,unsigned int dev_id,const_tpo_mod_file_ptr *driver);
BUS_DRIVER_API int			 			C_API_FUNC bus_manager_start_bus_device_stream	(unsigned int bus_driver_id,unsigned int dev_id,unsigned int stream_idx);

BUS_DRIVER_API int			 			C_API_FUNC bus_manager_get_bus_device_format	(unsigned int bus_driver_id,unsigned int dev_id,unsigned int fmt_idx,unsigned int *fmt_ptr);
BUS_DRIVER_API int			 			C_API_FUNC bus_manager_set_bus_device_format	(unsigned int bus_driver_id,unsigned int dev_id,unsigned int fmt_idx);



BUS_DRIVER_API int						C_API_FUNC bus_manager_get_dev_str_infos		(unsigned int bus_driver_id,unsigned int dev_id,unsigned int stream_idx,mem_zone_ref *infos);
BUS_DRIVER_API int						C_API_FUNC bus_manager_set_dev_str_pos			(unsigned int bus_driver_id,unsigned int dev_id,unsigned int stream_idx,large_uint_t sec_pos);

BUS_DRIVER_API int						C_API_FUNC bus_manager_read_bus_device_stream	(unsigned int bus_driver_id,unsigned int dev_id,unsigned int stream_idx,mem_zone_ref *req);

BUS_DRIVER_API int						C_API_FUNC find_bus_drivers_class				(bus_driver *bus_drv,unsigned char class_id,unsigned char subclass_id,unsigned char pi_id,mem_zone_ref *out_node,unsigned int idx);
BUS_DRIVER_API unsigned int				C_API_FUNC find_bus_drivers						(bus_driver *bus_drv,unsigned short vendor_id,unsigned short device_id,unsigned int subsys_vendor_id,unsigned int subsys_id,mem_zone_ref *out_node,unsigned int idx);
BUS_DRIVER_API void						C_API_FUNC bus_manager_dump_bus_driver_tree		(unsigned int bus_driver_id);


BUS_DRIVER_API int						C_API_FUNC bus_manager_add_dev_event_listener	(device_type_t	dev_type,device_event_listener_func_ptr		func);
BUS_DRIVER_API dev_event_listener_t *	C_API_FUNC bus_manager_find_dev_event_listener	(device_type_t	dev_type,unsigned int index);
BUS_DRIVER_API unsigned int				C_API_FUNC bus_manager_get_bus_driver_name		(unsigned int bus_driver_idx,char *name,unsigned int str_len);
BUS_DRIVER_API unsigned int				C_API_FUNC bus_manager_get_bus_driver_id		(unsigned int bus_driver_idx);
BUS_DRIVER_API unsigned int				C_API_FUNC bus_manager_get_bus_dev_tree			(unsigned int bus_driver_id,mem_zone_ref_ptr out_dev);

BUS_DRIVER_API int						C_API_FUNC bus_manager_load_bus_device_string	(unsigned int bus_driver_id,const char *fs_name,const char *str_list_csv);
BUS_DRIVER_API unsigned int				C_API_FUNC bus_manager_read_bus_dev_string		(unsigned short bus_driver_id,unsigned short vendor_id,unsigned short dev_id,char *vendor_name,size_t vstr_len,char *product_name,size_t pstr_len);

BUS_DRIVER_API unsigned int				C_API_FUNC bus_manager_get_device				(unsigned int bus_driver_id, unsigned int bus_id, unsigned int dev_id, mem_zone_ref_ptr dev);
BUS_DRIVER_API bus_driver *				C_API_FUNC bus_manager_find_bus_driver			(unsigned int bus_driver_idx);

BUS_DRIVER_API int						C_API_FUNC bus_manager_get_bus_dev_icon			(unsigned short bus_driver_id, unsigned short dev_id, mem_zone_ref_ptr icon);
