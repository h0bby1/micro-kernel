#define BUS_DRIVER_API C_EXPORT
#include <std_def.h>
#include <std_mem.h>
#include "kern.h"
#include "mem_base.h"
#include "lib_c.h"
#include "tree.h"

#include "sys/read_ini.h"
#include "sys/mem_stream.h"
#include "sys/file_system.h"
#include "sys/tpo_mod.h"
#include "sys/stream_dev.h"
#include "sys/task.h"
#include "bus_drv.h"

//DLL interface

//internal function definition specfic to bus driver modules
typedef void		 C_API_FUNC bus_drv_init_bus_driver_func		(bus_driver *bus_drv, unsigned int int_mode);
typedef unsigned int C_API_FUNC bus_drv_init_new_device_func		(bus_driver	*bus_drv,mem_zone_ref *dev_node);
typedef unsigned int C_API_FUNC bus_drv_scan_func					(bus_driver	*bus_drv);


typedef unsigned int C_API_FUNC bus_drv_find_device_by_type_func	(unsigned int device_type,unsigned int index);
typedef int			 C_API_FUNC bus_drv_init_device_func			(unsigned int stream_dev_id);
typedef int			 C_API_FUNC bus_drv_start_device_stream_func	(unsigned int dev_id,unsigned int str_idx);

typedef int			 C_API_FUNC bus_drv_get_device_format_func		(unsigned int dev_id,unsigned int fmt_idx,unsigned int *fmt);
typedef int			 C_API_FUNC bus_drv_set_device_format_func		(unsigned int dev_id,unsigned int fmt_idx);

typedef	int			 C_API_FUNC get_device_driver_func				(unsigned int device_id,const tpo_mod_file **driver);

typedef	int			 C_API_FUNC get_device_icon_func				(unsigned int device_id, mem_zone_ref_ptr icon);

typedef	int			 C_API_FUNC bus_drv_get_stream_infos_func		(unsigned int pci_dev_id,unsigned int str_idx,mem_zone_ref	*infos_node);
typedef	int			 C_API_FUNC bus_drv_set_stream_pos_func			(unsigned int pci_dev_id,unsigned int str_idx,large_uint_t sec_pos);

typedef	int			 C_API_FUNC bus_drv_read_stream_func			(unsigned int pci_dev_id,unsigned int str_idx,mem_zone_ref	*req);



typedef bus_drv_init_bus_driver_func			*bus_drv_init_bus_driver_func_ptr;
typedef bus_drv_scan_func		 				*bus_drv_scan_func_ptr;
typedef bus_drv_init_new_device_func			*bus_drv_init_new_device_func_ptr;
typedef bus_drv_start_device_stream_func		*bus_drv_start_device_stream_func_ptr;
typedef bus_drv_init_device_func				*bus_drv_init_device_func_ptr;
typedef bus_drv_find_device_by_type_func		*bus_drv_find_device_by_type_func_ptr;
typedef get_device_driver_func					*get_device_driver_func_ptr;
typedef get_device_icon_func					*get_device_icon_func_ptr;

typedef bus_drv_get_device_format_func			*bus_drv_get_device_format_func_ptr;
typedef bus_drv_set_device_format_func			*bus_drv_set_device_format_func_ptr;

typedef bus_drv_get_stream_infos_func			*bus_drv_get_stream_infos_func_ptr;
typedef bus_drv_set_stream_pos_func				*bus_drv_set_stream_pos_func_ptr;

typedef bus_drv_read_stream_func				*bus_drv_read_stream_func_ptr;

//typedef bus_drv_setup_renderer_func				*bus_drv_setup_renderer_func_ptr;


mem_zone_ref				bus_drivers_array			=	{PTR_INVALID};
unsigned int				n_bus_drivers				=	0xFFFFFFFF;

mem_zone_ref				dev_event_listeners_array	=	{PTR_INVALID};
unsigned int				n_dev_event_listeners		=	0xFFFFFFFF;


unsigned int				kernel_log_id				=	0xFFFFFFFF;

unsigned int				interupt_mode				= 0xFFFFFFFF;



void init_bus_manager_driver			(bus_driver *drv)
{
	drv->bus_id							=	0xFFFFFFFF;
	drv->controller_bus_id				=	0xFFFFFFFF;
	drv->controller_dev_id				=	0xFFFFFFFF;
	drv->bus_type						=	0xFFFFFFFF;


	drv->bus_dev_str_list.zone			= PTR_NULL;
	drv->bus_root_node.zone				= PTR_NULL;
	drv->drivers_root_node.zone			= PTR_NULL;
	drv->bus_tpo_file.data_sections.zone= PTR_NULL;

}

OS_API_C_FUNC(int) bus_manager_init			(unsigned int int_mode)
{
	unsigned int	n;
	bus_driver		*drivers;

	interupt_mode					=  int_mode;

	bus_drivers_array.zone			=	PTR_NULL;
	dev_event_listeners_array.zone	=	PTR_NULL;

	n_bus_drivers					=	0;
	kernel_log_id					=	get_new_kern_log_id		("bus_mngr: ",0x02);

	
	n_dev_event_listeners			=	0;
	allocate_new_zone				(0,128*sizeof(dev_event_listener_t),&dev_event_listeners_array);

	
	allocate_new_zone				(0,32*sizeof(bus_driver),&bus_drivers_array);

	drivers	=	get_zone_ptr(&bus_drivers_array,0);
	n		=	0;

	while(n<32)
	{ 
		init_bus_manager_driver(&drivers[n]);
		n++;
	}

	return 1;
}



int add_bus_drivers(bus_driver	*bus_drv,unsigned short vendor_id,unsigned short device_id,unsigned int sub_sys_vendor,unsigned int sub_sys,const char *fs_name,const char *dll_name,unsigned int drv_type,mem_zone_ref *new_node)
{
	char		 v_name[32];
	char		 d_name[32];
	char		 s_name[32];
	char		 sv_name[32];
	mem_zone_ref vendor_node		={PTR_NULL};
	mem_zone_ref device_node		={PTR_NULL};
	mem_zone_ref subsys_node		={PTR_NULL};
	mem_zone_ref subsys_vendor_node	={PTR_NULL};
	mem_zone_ref driver_node		={PTR_NULL};
	unsigned int vendor_crc;
	unsigned int device_crc;
	unsigned int subsys_crc;
	unsigned int subsys_vendor_crc;

	if((vendor_id==0xFFFF)||(device_id==0xFFFF))return 0;


	memset_c	(v_name,0,32);
	itoa_s		(vendor_id,v_name,32,16);
	vendor_crc	=	calc_crc32_c(v_name,32);

	memset_c	(d_name,0,32);
	itoa_s		(device_id,d_name,32,16);
	device_crc	=	calc_crc32_c(d_name,32);

	memset_c	(s_name,0,32);
	itoa_s		(sub_sys,s_name,32,16);
	subsys_crc	=	calc_crc32_c(s_name,32);

	memset_c	(sv_name,0,32);
	itoa_s		(sub_sys_vendor,sv_name,32,16);
	subsys_vendor_crc	=	calc_crc32_c(sv_name,32);
		

	if(!tree_manager_find_child_node(&bus_drv->drivers_root_node,vendor_crc,NODE_BUS_DRIVER_VENDOR,&vendor_node))
	{
		if(!tree_manager_add_child_node	(&bus_drv->drivers_root_node,v_name,NODE_BUS_DRIVER_VENDOR,&vendor_node))return 0;
	}

	if(!tree_manager_find_child_node(&vendor_node,device_crc,NODE_BUS_DRIVER_DEVICE,&device_node))
	{
		if(!tree_manager_add_child_node	(&vendor_node,d_name,NODE_BUS_DRIVER_DEVICE,&device_node))
		{
			release_zone_ref(&vendor_node);
			return 0;
		}
	}	


	if(!tree_manager_find_child_node(&device_node,subsys_vendor_crc,NODE_BUS_DRIVER_V_SUBSYS,&subsys_vendor_node))
	{
		if(!tree_manager_add_child_node	(&device_node,sv_name,NODE_BUS_DRIVER_V_SUBSYS,&subsys_vendor_node))
		{
			release_zone_ref(&device_node);
			release_zone_ref(&vendor_node);
			return 0;
		}
	}	


	if(!tree_manager_find_child_node(&subsys_vendor_node,subsys_crc,NODE_BUS_DRIVER_SUBSYS,&subsys_node))
	{
		if(!tree_manager_add_child_node	(&subsys_vendor_node,s_name,NODE_BUS_DRIVER_SUBSYS,&subsys_node))
		{
			release_zone_ref(&subsys_vendor_node);
			release_zone_ref(&device_node);
			release_zone_ref(&vendor_node);
			return 0;
		}
	}	
	
	


	if(tree_manager_add_child_node	(&subsys_node,dll_name,drv_type,&driver_node)!=1)
	{
		release_zone_ref			(&subsys_node);
		release_zone_ref			(&device_node);
		release_zone_ref			(&vendor_node);
		return 0;
	}
	tree_manager_write_node_data(&driver_node,fs_name,0,strlen_c(fs_name)+1);


	if(new_node!=PTR_NULL)
		copy_zone_ref(new_node,&driver_node);

	release_zone_ref			(&driver_node);
	release_zone_ref			(&subsys_node);
	release_zone_ref			(&device_node);
	release_zone_ref			(&vendor_node);

	return 1;	
}


int add_bus_drivers_class(bus_driver	*bus_drv,unsigned char class_id,unsigned char sub_class_id,unsigned char pi_id,const char *fs_name,const char *dll_name)
{
	char		 c_name[32];
	char		 sc_name[32];
	char		 pi_name[32];
	unsigned int class_crc;
	unsigned int subclass_crc;
	unsigned int pi_crc;
	mem_zone_ref class_node;
	mem_zone_ref subclass_node;
	mem_zone_ref pi_node;
	mem_zone_ref driver_node;
	

	memset_c	(c_name,0,32);
	itoa_s		(class_id,c_name,32,16);
	class_crc	=	calc_crc32_c(c_name,32);

	memset_c	(sc_name,0,32);
	itoa_s		(sub_class_id,sc_name,32,16);
	subclass_crc	=	calc_crc32_c(sc_name,32);

	memset_c	(pi_name,0,32);
	itoa_s		(pi_id,pi_name,32,16);
	pi_crc	=	calc_crc32_c(pi_name,32);

	class_node.zone=PTR_NULL;
	subclass_node.zone=PTR_NULL;
	driver_node.zone=PTR_NULL;
	pi_node.zone=PTR_NULL;

	if(!tree_manager_find_child_node(&bus_drv->drivers_root_node,class_crc,NODE_BUS_DRIVER_CLASS,&class_node))
	{
		if(!tree_manager_add_child_node	(&bus_drv->drivers_root_node,c_name,NODE_BUS_DRIVER_CLASS,&class_node))return 0;
	}

	if(!tree_manager_find_child_node(&class_node,subclass_crc,NODE_BUS_DRIVER_SUBCLASS,&subclass_node))
	{
		if(!tree_manager_add_child_node	(&class_node,sc_name,NODE_BUS_DRIVER_SUBCLASS,&subclass_node))
		{
			release_zone_ref(&class_node);
			return 0;
		}
	}	
	if(!tree_manager_find_child_node(&subclass_node,pi_crc,NODE_BUS_DRIVER_PI,&pi_node))
	{
		if(!tree_manager_add_child_node	(&subclass_node,pi_name,NODE_BUS_DRIVER_PI,&pi_node))
		{
			release_zone_ref			(&class_node);
			release_zone_ref			(&subclass_node);
			return 0;
		}
	}	

	if(tree_manager_add_child_node	(&pi_node,dll_name,NODE_BUS_DRIVER,&driver_node)!=1)
	{
		release_zone_ref			(&pi_node);
		release_zone_ref			(&class_node);
		release_zone_ref			(&subclass_node);
		return 0;
	}

	tree_manager_write_node_data(&driver_node,fs_name,0,strlen_c(fs_name)+1);

	release_zone_ref			(&pi_node);
	release_zone_ref			(&driver_node);
	release_zone_ref			(&subclass_node);
	release_zone_ref			(&class_node);

	return 1;	
}

const char *get_file_ext(const char *file_name)
{
	size_t n;
	n=strlen_c(file_name);
	while(n--)
	{
		if(file_name[n]=='.')
			return &file_name[n+1];
	}
	return PTR_NULL;
}


void change_file_ext(char *file_name,const char *new_ext)
{
	size_t	n,cnt,len;

	len	=strlen_c(file_name);
	n	=len;
	while(n--)
	{
		if(file_name[n]=='.')
		{
			n++;
			cnt=0;
			while((n<len)&&(new_ext[cnt]!=0))
			{
				file_name[n]=new_ext[cnt];
				cnt++;
				n++;
			}
			file_name[n]=0;
			
			return;
			
		}
	}
	return ;
}

int init_bus_drivers_tree(bus_driver	*bus_drv,const char *fs_name,const char *dir_name)
{
	char				driver_path[128];
	char				inf_path[128];
	char				dll_path[128];
	char				file_name[64];
	mem_stream			driver;
	mem_zone_ref		cur_file;
	unsigned int		n;

	if(bus_drv->drivers_root_node.zone==PTR_NULL)
	{
		char name[32];

		strcpy_s(name, 32, bus_drv->name);
		strcat_s(name, 32, " DRIVERS");

		if(tree_manager_create_node		(name,NODE_BUS_DRIVER_ROOT,&bus_drv->drivers_root_node)!=1)
		{
			kernel_log	(kernel_log_id,"could not create drivers tree root \n");
			return 0;
		}
	}
	
	kernel_log	(kernel_log_id,"init bus [");
	writeint(bus_drv->bus_id,16);
	writestr("] ");
	writestr(bus_drv->name);
	writestr(" drivers tree \n");

	cur_file.zone=NULL;

#if 1
	n=0;
	while(file_system_get_dir_entry	(fs_name,"/drivers/ndis",&cur_file,NODE_FILE_SYSTEM_FILE,n)==1)
	{
		int					n_devs;
		char				conf_name[64];
		unsigned short		vendor_id,device_id;
		unsigned short		sub_sys,sub_sys_vendor;
		char				manuf_key[32];
		mem_zone_ref		manuf_node;
		mem_zone_ref		manuf_val_node;
		mem_zone_ref		manuf_driver_node;
		mem_zone_ref		device_conf_node;
		const char			*name;

		name=tree_mamanger_get_node_name(&cur_file);
		
		if((name!=PTR_NULL)&&(!strcmp_c(get_file_ext(name),"inf")))
		{
			mem_stream					ini_file;
			mem_zone_ref				ini_tree;
			
			strcpy_s			(file_name,64,name);
			release_zone_ref	(&cur_file);

			strcpy_s			(inf_path,128,"/drivers/ndis/");
			strcat_s			(inf_path,128,file_name);

			strcpy_s			(dll_path,128,inf_path);
			change_file_ext		(dll_path,"tpo");

			ini_file.data.zone							=	PTR_NULL;
			
			if(file_system_open_file	("isodisk",inf_path,&ini_file)==0)
			{
				kernel_log	(kernel_log_id,"could not open ini file \n");
				n++;
				continue;
			}
			
			
			ini_tree.zone			=	PTR_NULL;
			manuf_node.zone			=	PTR_NULL;
			manuf_val_node.zone		=	PTR_NULL;
			manuf_driver_node.zone	=	PTR_NULL;
			device_conf_node.zone	=	PTR_NULL;

			read_ini_file				(&ini_file.data,&ini_tree,"pouet");


			memset_c(manuf_key,0,32);

			
			if(tree_node_find_child_by_name(&ini_tree,"Manufacturer",&manuf_node)==0)
			{
				kernel_log			(kernel_log_id,"no manufacturer found \n");
				release_zone_ref	(&ini_tree);
				n++;
				continue;
			}


			
			if(tree_manager_get_child_at	(&manuf_node,0,&manuf_val_node)==0)
			{
				kernel_log			(kernel_log_id,"no manufacturer child node found \n");
				release_zone_ref	(&manuf_node);
				release_zone_ref	(&ini_tree);
				n++;
				continue;
			}
			tree_manager_get_node_str	(&manuf_val_node,0,manuf_key,32,16);


			release_zone_ref (&manuf_val_node);
			release_zone_ref (&manuf_node);
			

			if(tree_node_find_child_by_name(&ini_tree,manuf_key,&manuf_driver_node)==0)
			{
				kernel_log			(kernel_log_id,"manufacturer not found '");
				writestr			(manuf_key);
				writestr			("'\n");
				release_zone_ref	(&ini_tree);
				n++;
				continue;
			}
			


			n_devs=0;
			//for each device of this manufacturer
			while((read_ini_vendor_value(&manuf_driver_node,n_devs,&vendor_id,&device_id,&sub_sys_vendor,&sub_sys,conf_name,64))==1)
			{
				mem_zone_ref	pci_device_node={PTR_NULL};

				/*
				kernel_log	(kernel_log_id,"new manuf device '");
				writestr	(manuf_key);
				writestr	("' ");
				writeint	(vendor_id,16);
				writestr	(" ");
				writeint	(device_id,16);
				writestr	(" ");
				writeint	(sub_sys_vendor,16);
				writestr	(" ");
				writeint	(sub_sys,16);
				writestr	("\n");
				snooze		(2000000);
				*/
				
								
				if(add_bus_drivers(bus_drv,vendor_id,device_id,sub_sys_vendor,sub_sys,fs_name,dll_path,NODE_BUS_NDIS_DRIVER,&pci_device_node)==1)
				{

					if(tree_node_find_child_by_name(&ini_tree,conf_name,&device_conf_node)>0)
					{
						mem_zone_ref	device_conf_AddReg_node;
						mem_zone_ref	pci_device_conf_node;
						
						device_conf_AddReg_node.zone=PTR_NULL;
						pci_device_conf_node.zone	=PTR_NULL;


						
						if(tree_node_find_child_by_name(&device_conf_node,"AddReg",&device_conf_AddReg_node)>0)
						{
							size_t		reg_name_pos;
							size_t		reg_name_next_pos;
							char		*node_dats;
							size_t		node_size;


							tree_manager_add_child_node(&pci_device_node,"config",NODE_INI_REGCONF_NODE,&pci_device_conf_node);

							node_dats			=	tree_mamanger_get_node_data_ptr	(&device_conf_AddReg_node,0);
							node_size			=	strlen_c(node_dats);



							reg_name_pos		=	0;
							while(reg_name_pos<node_size)
							{
								char			reg_base_name[64];
								mem_zone_ref	reg_base;
								reg_name_next_pos	=	strlpos_c(node_dats,reg_name_pos,',');

								if(reg_name_next_pos==INVALID_SIZE)
									reg_name_next_pos=node_size;
								

								copy_without_space				(reg_base_name,64,node_dats,reg_name_pos,reg_name_next_pos);

								reg_base.zone=PTR_NULL;
								if(tree_node_find_child_by_name	(&ini_tree,reg_base_name,&reg_base)==1)
								{
									/*
									mem_zone_ref	new_conf_node;
								
									new_conf_node.zone=PTR_NULL;

									
									tree_manager_create_node		(tree_mamanger_get_node_name	(&reg_base),tree_mamanger_get_node_type	(&reg_base),&new_conf_node);
									tree_manager_node_add_child		(&pci_device_conf_node,&new_conf_node);
									

									tree_manager_add_child_node	(&pci_device_conf_node,tree_mamanger_get_node_name	(&reg_base),tree_mamanger_get_node_type	(&reg_base),&new_conf_node);
									*/

									//tree_manager_dump_node_rec	(&reg_base,0,2);
									
									tree_manager_node_add_child		(&pci_device_conf_node,&reg_base);
									
									//tree_manager_dump_node_rec	(&pci_device_conf_node,0,2);

									//tree_manager_dump_node_rec		(&new_conf_node,0);

									//release_zone_ref				(&new_conf_node);
									release_zone_ref				(&reg_base);

								}
									
								reg_name_pos		=	reg_name_next_pos+1;
								
							}

							//tree_manager_dump_node_rec	(&pci_device_conf_node,0);

							release_zone_ref(&pci_device_conf_node);
							release_zone_ref(&device_conf_AddReg_node);
						}
						release_zone_ref(&device_conf_node);
					}
					
					release_zone_ref			(&pci_device_node);
				}

				
				n_devs++;
			}


			
			release_zone_ref(&manuf_driver_node);
			release_zone_ref(&ini_tree);
			
			//load_ndis_driver		(&ndis_driver,dll_path);
		}
		n++;
	}
	release_zone_ref(&cur_file);
	
#endif


	


	n=0;
	while(file_system_get_dir_entry	(fs_name,dir_name,&cur_file,NODE_FILE_SYSTEM_FILE,n)==1)
	{
		const char *name;
		name=tree_mamanger_get_node_name(&cur_file);
		if(name!=PTR_NULL)
		{
			strcpy_s			(file_name,64,name);
			release_zone_ref	(&cur_file);

			strcpy_s(driver_path,128,dir_name);
			strcat_s(driver_path,128,"/");
			strcat_s(driver_path,128,file_name);

			kernel_log	(kernel_log_id,"new bus driver : ");
			writestr	(driver_path);
			writestr	("\n");

			driver.data.zone	=PTR_NULL;
			driver.current_ptr	=0;

			if(file_system_open_file	(fs_name,driver_path,&driver)==1)
			{
				unsigned short		*devices;
				unsigned char		*class_codes;
				unsigned int		nn;

				
				devices		=	tpo_mod_get_exp_addr(&driver,"devices_list");

				if(devices!=PTR_NULL)
				{
					nn=0;
					while(devices[nn]!=0)
					{
						add_bus_drivers(bus_drv,devices[nn+0],devices[nn+1],0xFFFF,0xFFFF,fs_name,driver_path,NODE_BUS_DRIVER,PTR_NULL);
						nn+=2;
					}
					
				}

				
				class_codes			=	tpo_mod_get_exp_addr(&driver,"class_codes");
				if(class_codes!=PTR_NULL)
				{
					nn=0;
					while(class_codes[nn]!=0)
					{
						if(!add_bus_drivers_class(bus_drv,class_codes[nn+0],class_codes[nn+1],class_codes[nn+2],fs_name,driver_path))
						{
							kernel_log	(kernel_log_id,"could not add driver class \n");

						}
						nn+=3;
					}
				}
				
				if((class_codes==PTR_NULL)&&(devices==PTR_NULL))
				{
					kernel_log	(kernel_log_id,"no device or class found \n");
				}
				
				mem_stream_close(&driver);
			}
			else
			{
				kernel_log	(kernel_log_id,"unable to open bus driver file \n");
			}
		}
		release_zone_ref(&cur_file);
		n++;
	}
	
	kernel_log	(kernel_log_id,"finished drivers scan \n");

	/*tree_manager_dump_node_rec(&bus_drv->drivers_root_node,0,4);*/
	
	//snooze(100000000);
	
	return 1;
}



OS_API_C_FUNC(int) find_bus_drivers_class(bus_driver *bus_drv,unsigned char class_id,unsigned char subclass_id,unsigned char pi_id,mem_zone_ref *out_node,unsigned int idx)
{
	char		 c_name[32];
	unsigned int class_crc,bypass;
	unsigned int n_found;
	
	
	mem_zone_ref class_node;
	mem_zone_ref subclass_node;
	mem_zone_ref pi_node;
	mem_zone_ref driver_node;
	bypass		=	calc_crc32_c("ff",32);

	memset_c	(c_name,0,32);
	itoa_s		(class_id,c_name,32,16);
	class_crc	=	calc_crc32_c(c_name,32);





	class_node.zone=PTR_NULL;
	
	

	if(!tree_manager_find_child_node(&bus_drv->drivers_root_node,class_crc,NODE_BUS_DRIVER_CLASS,&class_node))
	{
		return 0;
	}
	
	subclass_node.zone=PTR_NULL;

	if(!tree_manager_find_child_node(&class_node,bypass,NODE_BUS_DRIVER_SUBCLASS,&subclass_node))
	{
		unsigned int subclass_crc;
		char		 sc_name[32];
		memset_c	(sc_name,0,32);
		itoa_s		(subclass_id,sc_name,32,16);
		subclass_crc	=	calc_crc32_c(sc_name,32);

		if(!tree_manager_find_child_node(&class_node,subclass_crc,NODE_BUS_DRIVER_SUBCLASS,&subclass_node))
		{
			release_zone_ref(&class_node);
			return 0;
		}
	}

	pi_node.zone=PTR_NULL;

	if(!tree_manager_find_child_node(&subclass_node,bypass,NODE_BUS_DRIVER_PI,&pi_node))
	{
		char		 pi_name[32];
		unsigned int pi_crc;
		memset_c	(pi_name,0,32);
		itoa_s		(pi_id,pi_name,32,16);
		pi_crc	=	calc_crc32_c(pi_name,32);

		if(!tree_manager_find_child_node(&subclass_node	,pi_crc,NODE_BUS_DRIVER_PI,&pi_node))
		{
						release_zone_ref(&class_node);
			release_zone_ref(&subclass_node);
			return 0;
		}
	}



	driver_node.zone=PTR_NULL;

	n_found=0;

	while(tree_node_find_child_by_type(&pi_node,NODE_BUS_DRIVER,&driver_node,n_found)>0)
	{
		if(n_found>=idx)
		{
			copy_zone_ref	(out_node,&driver_node);
			//strcpy_s		(dll_name,str_len,tree_mamanger_get_node_name(&driver_node));

			release_zone_ref(&pi_node);
			release_zone_ref(&class_node);
			release_zone_ref(&subclass_node);
			release_zone_ref(&driver_node);
			return 1;
		}
		n_found++;		
	}

	
	release_zone_ref(&pi_node);
	release_zone_ref(&class_node);
	release_zone_ref(&subclass_node);
	return 0;

}


OS_API_C_FUNC(unsigned int) find_bus_drivers(bus_driver *bus_drv,unsigned short vendor_id,unsigned short device_id,unsigned int subsys_vendor_id,unsigned int subsys_id,mem_zone_ref *out_node,unsigned int idx)
{
	char		 v_name[32];
	char		 d_name[32];
	char		 s_name[32];
	char		 sv_name[32];
	char		 by_name[32];
	unsigned int vendor_crc;
	unsigned int device_crc;
	unsigned int subsys_crc;
	unsigned int vsubsys_crc;
	unsigned int bypass_crc;
	mem_zone_ref vendor_node;
	mem_zone_ref device_node;
	mem_zone_ref subsys_vendor_node;
	mem_zone_ref subsys_node;
	mem_zone_ref driver_node;
	unsigned int n_found;

	
	memset_c		(v_name,0,32);
	
	itoa_s			(vendor_id,v_name,32,16);
	
	vendor_crc	=	calc_crc32_c(v_name,32);

	memset_c		(d_name,0,32);
	itoa_s			(device_id,d_name,32,16);
	device_crc	=	calc_crc32_c(d_name,32);

	memset_c		(s_name,0,32);
	itoa_s			(subsys_id,s_name,32,16);
	subsys_crc	=	calc_crc32_c(s_name,32);
	
	memset_c		(sv_name,0,32);
	itoa_s			(subsys_vendor_id,sv_name,32,16);
	vsubsys_crc	=	calc_crc32_c(sv_name,32);

	memset_c		(by_name,0,32);
	itoa_s			(0xFFFF,by_name,32,16);
	bypass_crc	=	calc_crc32_c(by_name,32);

	


	vendor_node.zone=PTR_NULL;
	if(!tree_manager_find_child_node(&bus_drv->drivers_root_node,vendor_crc,NODE_BUS_DRIVER_VENDOR,&vendor_node))
	{
		return 0;
	}
	
	device_node.zone=PTR_NULL;
	if(!tree_manager_find_child_node(&vendor_node	,device_crc,NODE_BUS_DRIVER_DEVICE,&device_node))
	{
		release_zone_ref(&vendor_node);
		return 0;
	}

	subsys_vendor_node.zone=PTR_NULL;
	if(!tree_manager_find_child_node(&device_node	,vsubsys_crc,NODE_BUS_DRIVER_V_SUBSYS,&subsys_vendor_node))
	{
		if(!tree_manager_find_child_node(&device_node	,bypass_crc,NODE_BUS_DRIVER_V_SUBSYS,&subsys_vendor_node))
		{
			release_zone_ref(&device_node);
			release_zone_ref(&vendor_node);
			return 0;
		}
	}

	subsys_node.zone=PTR_NULL;
	if(!tree_manager_find_child_node(&subsys_vendor_node	,subsys_crc,NODE_BUS_DRIVER_SUBSYS,&subsys_node))
	{
		if(!tree_manager_find_child_node(&subsys_vendor_node	,bypass_crc,NODE_BUS_DRIVER_SUBSYS,&subsys_node))
		{
			release_zone_ref(&subsys_vendor_node);
			release_zone_ref(&device_node);
			release_zone_ref(&vendor_node);
			return 0;
		}
	}


	n_found=0;
	driver_node.zone=PTR_NULL;
	while(tree_node_find_child_by_type(&subsys_node,NODE_BUS_DRIVER,&driver_node,n_found)>0)
	{
		if(n_found>=idx)
		{
			copy_zone_ref(out_node,&driver_node);
			release_zone_ref(&subsys_vendor_node);
			release_zone_ref(&subsys_node);
			release_zone_ref(&vendor_node);
			release_zone_ref(&device_node);
			release_zone_ref(&driver_node);
			return 1;
		}
		n_found++;
	}

	
	release_zone_ref(&driver_node);
	release_zone_ref(&subsys_vendor_node);
	release_zone_ref(&subsys_node);
	release_zone_ref(&vendor_node);
	release_zone_ref(&device_node);
	
	return 0;
}


OS_API_C_FUNC(dev_event_listener_t *) bus_manager_find_dev_event_listener			(device_type_t	dev_type,unsigned int index)
{
	dev_event_listener_t		*listener_list,*listener_list_end;
	
	listener_list				=	get_zone_ptr		(&dev_event_listeners_array,0);
	listener_list_end			=	get_zone_ptr		(&dev_event_listeners_array,n_dev_event_listeners*sizeof(dev_event_listener_t));
	
	/*
	kernel_log	(kernel_log_id,"event listener ");
	writeptr	(listener_list);
	writestr	(" ");
	writeptr	(listener_list_end);
	writestr	("\n");
	*/
	
	
	while(listener_list<listener_list_end)
	{
		if(listener_list->dev_type==dev_type)
		{
			if(index==0)
				return listener_list;

			index--;
		}

		listener_list++;
	}

	return PTR_NULL;
}

OS_API_C_FUNC(int)	bus_manager_add_dev_event_listener			(device_type_t	dev_type,device_event_listener_func_ptr		func)
{
	dev_event_listener_t		*new_listener;
	
	kernel_log	(kernel_log_id,"new even listener [");
	writeint	(n_dev_event_listeners,10);
	writestr	("] type ");
	writeint	(dev_type,16);
	writestr	("\n");
	
	new_listener				=	get_zone_ptr		(&dev_event_listeners_array,n_dev_event_listeners*sizeof(dev_event_listener_t));
	new_listener->dev_type		=	dev_type;
	new_listener->listener_fnc	=	func;
	n_dev_event_listeners++;

	return 1;
}



OS_API_C_FUNC(unsigned int ) bus_manager_get_bus_driver_name		(unsigned int bus_driver_idx,char *name,unsigned int str_len)
{
	bus_driver		*driver;
	if(bus_driver_idx>=n_bus_drivers)return 0;

	driver	=	get_zone_ptr(&bus_drivers_array,bus_driver_idx*sizeof(bus_driver));

	strcpy_s	(name,str_len,driver->name);

	return 1;

}

OS_API_C_FUNC(unsigned int ) bus_manager_get_bus_driver_id		(unsigned int bus_driver_idx)
{
	bus_driver		*driver;
	if(bus_driver_idx>=n_bus_drivers)return 0;
	driver	=	get_zone_ptr(&bus_drivers_array,bus_driver_idx*sizeof(bus_driver));
	return driver->bus_id;
}


OS_API_C_FUNC(unsigned int ) bus_manager_get_bus_driver_idx		(unsigned int bus_driver_id)
{
	unsigned int	n;
	bus_driver		*driver;

	driver	=	get_zone_ptr(&bus_drivers_array,0);
	n		=	0;
	while(n<n_bus_drivers)
	{ 
		if(driver->bus_id==bus_driver_id)return n;
		driver++;
		n++;
	}
	return 0xFFFFFFFF;

}

OS_API_C_FUNC(bus_driver *) bus_manager_find_bus_driver	(unsigned int bus_driver_idx)
{
	unsigned int	n;
	bus_driver		*driver;

	driver	=	get_zone_ptr(&bus_drivers_array,0);
	n		=	0;
	while(n<n_bus_drivers)
	{ 
		if(driver->bus_id==bus_driver_idx)return driver;
		driver++;
		n++;
	}
	return PTR_NULL;

}


OS_API_C_FUNC(unsigned int) bus_manager_load_bus_driver(const char *fs_name,const char *drv_file)
{
	mem_stream							file;
	bus_drv_init_bus_driver_func_ptr	init_bus_driver;
	bus_driver							*bus_drv;

	file.data.zone	=PTR_NULL;
	if(expand_zone(&bus_drivers_array,(n_bus_drivers+1)*sizeof(bus_driver))<0)
	{
		kernel_log(kernel_log_id,"could not allocate new bus driver \n");
		return 0xFFFFFFFF;
	}


	if(file_system_open_file	(fs_name,drv_file,&file)!=1)
	{
		kernel_log(kernel_log_id,"cannot open bus driver file ");
		writestr(drv_file);
		writestr("\n"); 
		return 0xFFFFFFFF;
	}

	bus_drv					=	get_zone_ptr(&bus_drivers_array,n_bus_drivers*sizeof(bus_driver));
	
	tpo_mod_init			(&bus_drv->bus_tpo_file);
	tpo_mod_load_tpo		(&file,&bus_drv->bus_tpo_file,0);
	mem_stream_close		(&file);

	kernel_log(kernel_log_id, "init bus driver ");
	writestr(bus_drv->bus_tpo_file.name);
	writestr("\n");

	init_bus_driver		=	get_tpo_mod_exp_addr_name(&bus_drv->bus_tpo_file,"init_bus_driver");
	
	if(init_bus_driver != PTR_NULL)
		init_bus_driver			(bus_drv, interupt_mode);

	bus_drv->bus_id			= n_bus_drivers+1;
	
	kernel_log(kernel_log_id, "bus drv ");
	writestr( bus_drv->bus_tpo_file.name);
	writestr(" bus id ");
	writeint(bus_drv->bus_id, 16);
	writestr(" type ");
	writeint(bus_drv->bus_type, 16);
	writestr("\n");

	n_bus_drivers++;
	return bus_drv->bus_id;
		
}


OS_API_C_FUNC(void) bus_manager_set_bus_driver_ctrler		(unsigned int bus_drv_id,unsigned int bus_id,unsigned int dev_id)
{
	bus_driver					*bus_drv;
	
	bus_drv				=	bus_manager_find_bus_driver(bus_drv_id);
	if (bus_drv == PTR_NULL) {
		kernel_log(kernel_log_id, "bus_manager_set_bus_driver_ctrler bus drv not found ");
		writeint(bus_drv_id,10);
		writestr( "\^n");
		return;
	}

	bus_drv->controller_bus_id	=	bus_id;
	bus_drv->controller_dev_id	=	dev_id;
}
OS_API_C_FUNC(void) bus_manager_scan_bus_drivers(unsigned int bus_drv_id,const char *fs_name,const char *dir_name)
{
	bus_driver					*bus_drv;
	
	bus_drv	= bus_manager_find_bus_driver(bus_drv_id);
	if (bus_drv == PTR_NULL)
	{
		kernel_log(kernel_log_id, "bus_manager_scan_bus_drivers no bus driver \n");
		return;
	}

	init_bus_drivers_tree(bus_drv,fs_name,dir_name);
	
}




OS_API_C_FUNC(void) bus_manager_scan_bus(unsigned int bus_driver_id)
{
	bus_driver				*bus_drv;
	bus_drv_scan_func_ptr	scan_devices;

	bus_drv		=	bus_manager_find_bus_driver(bus_driver_id);
	if (bus_drv == PTR_NULL) {

		kernel_log(kernel_log_id, "bus_manager_scan_bus no bus driver ");
		writeint(bus_driver_id, 10);
		writestr("\n");
		return;
	}

	scan_devices	= get_tpo_mod_exp_addr_name			(&bus_drv->bus_tpo_file,"bus_scan_devices");

	
	if(scan_devices)
	{
		if (!scan_devices(bus_drv))
		{
			kernel_log(kernel_log_id, "scan_devices error ");
			writeint(bus_driver_id, 10);
			writestr("\n");
		}
	}
	else
	{
		kernel_log(kernel_log_id, "bus no scan_devices ");
		writeint(bus_driver_id, 10);
		writestr("\n");
		return;
	}

}

OS_API_C_FUNC(int) bus_manager_read_bus_device_stream(unsigned int bus_driver_id,unsigned int dev_id,unsigned int stream_idx,mem_zone_ref *req)
{
	bus_driver							*bus_drv;
	bus_drv_read_stream_func_ptr		bus_drv_read_device_stream;

	bus_drv		=	bus_manager_find_bus_driver(bus_driver_id);
	if (bus_drv == PTR_NULL) {
		kernel_log(kernel_log_id, "unable to find bus driver \n");
		return 0;
	}

	bus_drv_read_device_stream		= get_tpo_mod_exp_addr_name			(&bus_drv->bus_tpo_file,"bus_drv_read_device_stream");
	
	if(bus_drv_read_device_stream	== PTR_NULL) {
		kernel_log(kernel_log_id, "unable to find bus_drv_read_device_stream \n");
		return 0;
	}
	bus_drv_read_device_stream		(dev_id,stream_idx,req);

	return 1;
}

OS_API_C_FUNC(int) bus_manager_start_bus_device_stream(unsigned int bus_driver_id,unsigned int dev_id,unsigned int stream_idx)
{
	bus_driver							*bus_drv;
	bus_drv_start_device_stream_func_ptr bus_drv_start_device_stream;

	bus_drv		=	bus_manager_find_bus_driver(bus_driver_id);
	if(bus_drv	== PTR_NULL)return 0;

	bus_drv_start_device_stream		= get_tpo_mod_exp_addr_name			(&bus_drv->bus_tpo_file,"bus_drv_start_device_stream");
	
	if(bus_drv_start_device_stream	== PTR_NULL)return 0;

	bus_drv_start_device_stream		(dev_id,stream_idx);

	return 1;
}

OS_API_C_FUNC(int) bus_manager_get_dev_str_infos(unsigned int bus_driver_id,unsigned int dev_id,unsigned int stream_idx,mem_zone_ref *infos)
{
	bus_driver							*bus_drv;
	bus_drv_get_stream_infos_func_ptr	bus_drv_get_stream_infos;

	bus_drv		=	bus_manager_find_bus_driver(bus_driver_id);
	if(bus_drv	== PTR_NULL)return 0;

	bus_drv_get_stream_infos		= get_tpo_mod_exp_addr_name			(&bus_drv->bus_tpo_file,"bus_drv_get_stream_infos");
	
	if(bus_drv_get_stream_infos	== PTR_NULL)return 0;

	return bus_drv_get_stream_infos		(dev_id,stream_idx,infos);
}

OS_API_C_FUNC(int) bus_manager_set_dev_str_pos(unsigned int bus_driver_id,unsigned int dev_id,unsigned int stream_idx,large_uint_t sec_pos)
{
	bus_driver							*bus_drv;
	bus_drv_set_stream_pos_func_ptr	bus_drv_set_stream_pos;

	bus_drv		=	bus_manager_find_bus_driver(bus_driver_id);
	if(bus_drv	== PTR_NULL)return 0;

	bus_drv_set_stream_pos		= get_tpo_mod_exp_addr_name			(&bus_drv->bus_tpo_file,"bus_drv_set_stream_pos");
	
	if(bus_drv_set_stream_pos	== PTR_NULL)return 0;

	return bus_drv_set_stream_pos		(dev_id,stream_idx,sec_pos);
}


OS_API_C_FUNC(int) bus_manager_get_bus_device_format(unsigned int bus_driver_id,unsigned int dev_id,unsigned int fmt_idx,unsigned int *fmt_ptr)
{
	bus_driver								*bus_drv;
	bus_drv_get_device_format_func_ptr			bus_drv_get_device_format;

	bus_drv		=	bus_manager_find_bus_driver(bus_driver_id);
	if(bus_drv	== PTR_NULL)return 0;

	bus_drv_get_device_format		= get_tpo_mod_exp_addr_name			(&bus_drv->bus_tpo_file,"bus_drv_get_device_format");
	
	if(bus_drv_get_device_format	== PTR_NULL)return 0;

	return bus_drv_get_device_format		(dev_id,fmt_idx,fmt_ptr);
}

OS_API_C_FUNC(int) bus_manager_get_bus_dev_icon(unsigned int bus_driver_id, unsigned int dev_id, mem_zone_ref_ptr icon)
{
	bus_driver								*bus_drv;
	get_device_icon_func_ptr			get_device_icon;

	bus_drv = bus_manager_find_bus_driver(bus_driver_id);
	if (bus_drv == PTR_NULL)return 0;

	get_device_icon = get_tpo_mod_exp_addr_name(&bus_drv->bus_tpo_file, "get_device_icon");

	if (get_device_icon == PTR_NULL)return 0;

	return get_device_icon(dev_id, icon);
}

OS_API_C_FUNC(unsigned int) bus_manager_read_bus_dev_string(unsigned short bus_driver_id,unsigned short vendor_id,unsigned short dev_id,char *vendor_name,size_t vstr_len,char *product_name,size_t pstr_len)
{
	bus_driver		*bus_drv;
	size_t			current,next;
	const char		*csv_buff;
		
	bus_drv		=	bus_manager_find_bus_driver(bus_driver_id);
	if (bus_drv == PTR_NULL) { 
		kernel_log(kernel_log_id,"no bus\n");
		return 0; 
	}

	if(bus_drv->bus_dev_str_list.zone==PTR_NULL) {
		kernel_log(kernel_log_id, "no bus dev list\n");
		return 0;
	}

	csv_buff=get_zone_ptr(&bus_drv->bus_dev_str_list,0);
	current	=0;

	while((next=strlpos_c(csv_buff,current,'\n'))!=INVALID_SIZE)
	{
		unsigned int	col_idx;
		size_t			line_len,last_pos,cnt;
		const char		*cur_line;

		
			
		cur_line	=	&csv_buff[current];
		line_len	=	next-current;
		cnt			=	0;
		col_idx		=	0;

		while(cnt<line_len)
		{
			last_pos	=	cnt;
			cnt			=	strlpos_c(cur_line,cnt,',');
			if(cnt==INVALID_SIZE)
				cnt=line_len;
			else
				cnt++;

			if(col_idx==0)
			{
				unsigned int csv_vid;
				while(cur_line[last_pos]!='"')last_pos++;
				last_pos++;
				csv_vid		=	strtol_c(&cur_line[last_pos],PTR_NULL,16);

				if(csv_vid!=vendor_id)break;
			}
			if(col_idx==1)
			{
				unsigned int csv_pid;
				while(cur_line[last_pos]!='"')last_pos++;
				last_pos++;
				csv_pid		=	strtol_c(&cur_line[last_pos],PTR_NULL,16);

				if(csv_pid!=dev_id)break;
			}
			if(col_idx==2)
			{
				size_t	end_str;
				while(cur_line[last_pos]!='"')last_pos++;
				last_pos++;
				end_str=strlpos_c	(cur_line,last_pos,'"');
				strncpy_s			(vendor_name,vstr_len,&cur_line[last_pos],end_str-last_pos);
			}
			if(col_idx==3)
			{
				size_t	end_str;
				while(cur_line[last_pos]!='"')last_pos++;
				last_pos++;
				end_str=strlpos_c	(cur_line,last_pos,'"');
				strncpy_s			(product_name,pstr_len,&cur_line[last_pos],end_str-last_pos);
				return 1;
			}
			col_idx++;
		}
		current=next+1;
	}

	return 0;
}

OS_API_C_FUNC(int) bus_manager_load_bus_device_string(unsigned int bus_driver_id,const char *fs_name,const char *str_list_csv)
{
	mem_stream									csv_file;
	bus_driver									*bus_drv;


	bus_drv		=	bus_manager_find_bus_driver(bus_driver_id);
	if(bus_drv	== PTR_NULL)return 0;

	
	csv_file.data.zone	=	PTR_NULL;
	if(file_system_open_file	(fs_name,str_list_csv,&csv_file)==0)
	{
		kernel_log	(kernel_log_id,"could not open pci str file \n");
		return 0;
	}
	
	copy_zone_ref		(&bus_drv->bus_dev_str_list,&csv_file.data);
	mem_stream_close	(&csv_file);

	kernel_log(kernel_log_id, "open pci str file \n");
	

	return 1;
}

OS_API_C_FUNC(int) bus_manager_set_bus_device_format(unsigned int bus_driver_id,unsigned int dev_id,unsigned int fmt_idx)
{
	bus_driver								*bus_drv;
	bus_drv_set_device_format_func_ptr		bus_drv_set_device_format;

	bus_drv		=	bus_manager_find_bus_driver(bus_driver_id);
	if(bus_drv	== PTR_NULL)return 0;

	

	bus_drv_set_device_format		= get_tpo_mod_exp_addr_name			(&bus_drv->bus_tpo_file,"bus_drv_set_device_format");
	if(bus_drv_set_device_format	== PTR_NULL)return 0;

		

	return bus_drv_set_device_format		(dev_id,fmt_idx);
}


OS_API_C_FUNC(unsigned int) bus_manager_find_bus_by_type(unsigned int type,unsigned int index)
{
	unsigned int	n,found;
	bus_driver		*drivers;


	drivers	=	get_zone_ptr(&bus_drivers_array,0);
	n		=	0;
	found	=	0;

	while(n<n_bus_drivers)
	{ 
		if(drivers[n].bus_type==type)
		{
			if(found>=index)
				return drivers[n].bus_id;
			
			found++;
		}

		n++;
	}

	return 0xFFFFFFFF;




}

OS_API_C_FUNC(unsigned int) bus_manager_find_device_by_type(unsigned int bus_driver_id,unsigned int type,unsigned int index)
{
	bus_driver							*bus_drv;
	bus_drv_find_device_by_type_func_ptr find_device_by_type;
	unsigned int						 ret;

	bus_drv		=	bus_manager_find_bus_driver(bus_driver_id);
	if(bus_drv	== PTR_NULL)return 0xFFFFFFFF;

	find_device_by_type =	get_tpo_mod_exp_addr_name			(&bus_drv->bus_tpo_file,"find_device_by_type");
	ret					=	find_device_by_type					(type,index);

	return ret;

}

OS_API_C_FUNC(unsigned int) bus_manager_get_device(unsigned int bus_driver_id, unsigned int bus_id, unsigned int dev_id, mem_zone_ref_ptr dev)
{
	char id_buffer[32];
	mem_zone_ref my_bus = { PTR_NULL };
	int ret;

	bus_driver *bus_drv = bus_manager_find_bus_driver(bus_driver_id);
	if (bus_drv == PTR_NULL) {
		kernel_log(kernel_log_id, "no bus drv\n");
		return 0;
	}

	if(!tree_manager_get_child_at(&bus_drv->bus_root_node, bus_id, &my_bus)) {
		kernel_log(kernel_log_id, "no bus\n");
		return 0;
	}

	ret=tree_node_find_child_by_type_value(&my_bus, NODE_PCI_BUS_DEV, dev_id, dev);

	release_zone_ref(&my_bus);

	return ret;

}

OS_API_C_FUNC(unsigned int) bus_manager_get_device_driver(unsigned int bus_driver_id,unsigned int dev_id,const_tpo_mod_file_ptr *driver)
{

	bus_driver							*bus_drv;
	get_device_driver_func_ptr			 get_device_driver;
	unsigned int						 ret;

	bus_drv		=	bus_manager_find_bus_driver(bus_driver_id);
	if(bus_drv	== PTR_NULL)return 0xFFFFFFFF;

	get_device_driver	=	get_tpo_mod_exp_addr_name			(&bus_drv->bus_tpo_file,"get_device_driver");
	if(get_device_driver== PTR_NULL)return 0xFFFFFFFF;
		
	ret					=	get_device_driver					(dev_id,driver);

	if(ret == 0)
	{	
		kernel_log(kernel_log_id,"could not find device driver \n");
		return 0xFFFFFFFF;
	}

	return 1;

}
OS_API_C_FUNC(void) bus_manager_dump_bus_driver_tree(unsigned int bus_driver_id)
{
	bus_driver							*bus_drv;
	bus_drv		=	bus_manager_find_bus_driver(bus_driver_id);
	if(bus_drv	== PTR_NULL)return;


	tree_manager_dump_node_rec(&bus_drv->drivers_root_node,0,0xFF);
}

OS_API_C_FUNC(unsigned int) bus_manager_get_bus_dev_tree(unsigned int bus_driver_id,mem_zone_ref_ptr out_dev)
{
	bus_driver							*bus_drv;
	bus_drv		=		bus_manager_find_bus_driver(bus_driver_id);
	if(bus_drv	== PTR_NULL)return 0;

	copy_zone_ref(out_dev,&bus_drv->bus_root_node);

	return 1;
}


OS_API_C_FUNC(void) bus_manager_parse_bus(unsigned int bus_driver_id)
{
	bus_driver							*bus_drv;
	mem_zone_ref						device_node,init_node;
	unsigned int						new_dev_id;
	unsigned int						n,dev_init;
	bus_drv_init_new_device_func_ptr	init_new_bus_device;
	bus_drv_init_device_func_ptr		init_bus_device;


	

	bus_drv		=	bus_manager_find_bus_driver(bus_driver_id);
	if(bus_drv	== PTR_NULL)return;

	if(interupt_mode ==0)
		pic_set_irq_mask_c(0xFFFF);
	

	init_new_bus_device	= get_tpo_mod_exp_addr_name			(&bus_drv->bus_tpo_file,"init_new_bus_device");

	if(init_new_bus_device==PTR_NULL)
	{
		kernel_log	(kernel_log_id,"new bus device fnc not found '");
		writestr	(bus_drv->bus_tpo_file.name);
		writestr	("' for bus ");
		writeint	(bus_driver_id,16);
		writestr	("' \n");
		
		if (interupt_mode == 0)
			pic_set_irq_mask_c(0x00);
		return;
	}
	init_bus_device		= get_tpo_mod_exp_addr_name			(&bus_drv->bus_tpo_file,"init_bus_device");
	if(init_bus_device==PTR_NULL)
	{
		kernel_log	(kernel_log_id,"new bus device fnc not found '");
		writestr	(bus_drv->bus_tpo_file.name);
		writestr	("' \n");

		if (interupt_mode == 0)
			pic_set_irq_mask_c(0x00);
		return;
	}

	//kernel_log(kernel_log_id,"parsing bus tree \n");
	
	init_node.zone=PTR_NULL;
	device_node.zone=PTR_NULL;

	
	n=0;
	while(tree_node_list_child_by_type(&bus_drv->bus_root_node,NODE_BUS_DEVICE,&device_node,n)>0)
	{
		dev_init=0;
		if(tree_node_find_child_by_name(&device_node,"initialized",&init_node)==1)
		{
			tree_mamanger_get_node_dword	(&init_node,0,&dev_init);
			release_zone_ref				(&init_node);
		}
		/*
		else
		{
			tree_manager_dump_node_rec(&device_node,0,2);
		}
		*/

		if(dev_init==0)
		{
			
			if((new_dev_id=init_new_bus_device	(bus_drv,&device_node))!=0xFFFFFFFF)
			{
				if(init_bus_device					(new_dev_id)==1)
				{
					tree_manager_add_child_node		(&device_node,"initialized",NODE_GFX_INT,&init_node);
					tree_manager_write_node_dword	(&init_node,0,1);
					release_zone_ref				(&init_node);
				}
				else
				{
					tree_manager_add_child_node		(&device_node,"initialized",NODE_GFX_INT,&init_node);
					tree_manager_write_node_dword	(&init_node,0,2);
					release_zone_ref				(&init_node);
				}
			}
		}

		release_zone_ref(&device_node);
		n++;
	}

	
	if (interupt_mode == 0)
		pic_set_irq_mask_c(0x00);
	
	//kernel_log(kernel_log_id,"end parsing bus tree \n");
	
}


