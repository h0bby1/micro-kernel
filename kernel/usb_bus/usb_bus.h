


//setup pack initializer
void usb_create_get_status_packet_device			(usb_setup_packet_t *packet);
void usb_create_get_configuration_packet_device		(usb_setup_packet_t *packet);
void usb_create_enable_port_hub						(usb_setup_packet_t *packet,unsigned int port_idx);
void usb_create_get_descriptor_packet_device		(usb_setup_packet_t *packet);
void usb_create_get_descriptor_packet_conf			(usb_setup_packet_t *packet,unsigned int conf_idx,unsigned int size);
void usb_create_get_descriptor_packet_string		(usb_setup_packet_t *packet,unsigned int string_id,unsigned int lang_id,unsigned int str_size);
void usb_create_get_descriptor_packet_hub			(usb_setup_packet_t *packet);
void usb_create_get_port_status_hub					(usb_setup_packet_t *packet,unsigned int port_idx);
void usb_create_set_address_packet_device			(usb_setup_packet_t *packet,unsigned int dev_addr);
void usb_create_set_configuration_packet_device		(usb_setup_packet_t *packet,unsigned int conf_id);
void usb_create_get_hid_descriptor_repport			(usb_setup_packet_t *packet,unsigned int desc_idx,unsigned int iface_idx,unsigned int descriptorType,unsigned int descriptorSize);
void usb_create_get_hid_repport						(usb_setup_packet_t *packet,unsigned int report_id,unsigned int iface_id,unsigned int reportSize);
void usb_create_set_hid_idle						(usb_setup_packet_t *packet,unsigned int report_id,unsigned int iface_id,unsigned int duration);



//interface manipulation
struct usb_interface * get_device_interface			(struct usb_device *dev	,unsigned int conf_idx	,unsigned int iface_idx);



//usb transfers
int 	C_API_FUNC		do_usb_ctrl_transfer				(struct usb_device *dev	,usb_setup_packet_t	*setup_packet,mem_ptr data,mem_size size,unsigned int packet_size,int *ret_code);
int 			do_usb_intr_transfer_read			(struct usb_transfer_t		*tfer);






