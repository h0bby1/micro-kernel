/*
3 Usage Pages 
The following table lists the currently defined usage pages and the section in this document or the 
specification where each page is described. 
Table 1: Usage Page Summary 
Page ID Page Name Section or Document 
00 Undefined 
01 Generic Desktop Controls 4 
02 Simulation Controls 5 
03 VR Controls 0 
04 Sport Controls 7 
05 Game Controls 8 
06 Generic Device Controls 9 
07 Keyboard/Keypad 10 
08 LEDs 11 
09 Button 12 
0A Ordinal 13 
0B Telephony 14 
0C Consumer 15 
0D Digitizer 16 
0E Reserved 
0F PID Page USB Physical Interface Device definitions for force feedback and related devices. 
10 Unicode 17 
11-13 Reserved 
14 Alphanumeric Display 18 
15-3f Reserved 
40 Medical Instruments 19 
41-7F Reserved 
80-83 Monitor pages USB Device Class Definition for Monitor Devices
84-87 Power pages USB Device Class Definition for Power Devices
88-8B Reserved 
8C Bar Code Scanner page 
8D Scale page 
8E Magnetic Stripe Reading (MSR) Devices 
8F Reserved Point of Sale pages USB Device Class Definition for Point of Sale Devices
90 Camera Control Page USB Device Class Definition for Image Class Devices16 Universal Serial Bus HID Usage Tables Version 1.11 June 27, 2001 Page ID Page Name Section or Document 
91 Arcade Page OAAF Definitions for arcade and coinop related Devices 
92-FEFF Reserved FF00-FFFF Vendor-defined 

A bold usage definition in the following sections identifies a collection. Non-bold definitions are specific 
features related to a device that would be applied to individual controls that generate data. In many cases, 
specific usages can be used by a number of device types. 
*/

typedef enum
{
USAGE_PAGE_UNDEFINED						= 0x00,
USAGE_PAGE_DESKTOP							= 0x01 ,
USAGE_PAGE_SIMULATION						= 0x02 ,
USAGE_PAGE_VR								= 0x03 ,
USAGE_PAGE_SPORT							= 0x04 ,
USAGE_PAGE_GAME 							= 0x05 ,
USAGE_PAGE_GENERIC_DEVICE					= 0x06 ,
USAGE_PAGE_KEYPAD							= 0x07 ,
USAGE_PAGE_LED								= 0x08 ,
USAGE_PAGE_BUTTON							= 0x09 ,
USAGE_PAGE_ORDINAL							= 0x0A ,
USAGE_PAGE_TELEPHONY						= 0x0B ,
USAGE_PAGE_CONSUMER							= 0x0C ,
USAGE_PAGE_DIGITIZER						= 0x0D ,
USAGE_PAGE_FORCEFEEDBACK					= 0x0F ,
USAGE_PAGE_UNICODE							= 0x10 ,
USAGE_PAGE_ALPHANUMERIC						= 0x14 ,
USAGE_PAGE_MEDICAL							= 0x40 ,
USAGE_PAGE_BARCODE_SCANNER					= 0x8C ,
USAGE_PAGE_SCALE 							= 0x8D ,
USAGE_PAGE_MAGNETIC_STRIP					= 0x8E ,
USAGE_PAGE_POINT_OF_SALE					= 0x8F ,
USAGE_PAGE_CAMERA_CONTROL					= 0x90 ,
USAGE_PAGE_ARCADE							= 0x91 
/*
80-83	Monitor pages USB Device Class Definition for Monitor Devices
84-87	Power pages USB Device Class Definition for Power Devices
*/
}hid_usage_pages;


typedef enum
{
DESTKOP_USAGE_UNDEFINED									=	0x00 ,
DESTKOP_USAGE_Pointer									=	0x01 ,
DESTKOP_USAGE_Mouse										=	0x02 ,
DESTKOP_USAGE_Joystick									=	0x04 ,
DESTKOP_USAGE_GamePad									=	0x05 ,
DESTKOP_USAGE_Keyboard									=	0x06 ,
DESTKOP_USAGE_Keypad									=	0x07 ,
DESTKOP_USAGE_Multi_Axis								=	0x08 ,
DESTKOP_USAGE_X											=	0x30 ,
DESTKOP_USAGE_Y											=	0x31 ,
DESTKOP_USAGE_Z											=	0x32 ,
DESTKOP_USAGE_Rx										=	0x33 ,
DESTKOP_USAGE_Ry										=	0x34 ,
DESTKOP_USAGE_Rz										=	0x35 ,
DESTKOP_USAGE_Slider									=	0x36 ,
DESTKOP_USAGE_Dial										=	0x37 ,
DESTKOP_USAGE_Wheel										=	0x38 ,
DESTKOP_USAGE_Hat_switch								=	0x39 ,
DESTKOP_USAGE_Counted_Buffer							=	0x3A ,
DESTKOP_USAGE_Byte_Count								=	0x3B ,
DESTKOP_USAGE_Motion_Wakeup								=	0x3C ,
DESTKOP_USAGE_Start										=	0x3D ,
DESTKOP_USAGE_Select									=	0x3E ,
DESTKOP_USAGE_Vx										=	0x40 ,
DESTKOP_USAGE_Vy										=	0x41 ,
DESTKOP_USAGE_Vz										=	0x42 ,
DESTKOP_USAGE_Vbrx										=	0x43 ,
DESTKOP_USAGE_Vbry										=	0x44 ,
DESTKOP_USAGE_Vbrz										=	0x45 ,
DESTKOP_USAGE_Vno										=	0x46 ,
DESTKOP_USAGE_Feature_Notification						=	0x47 ,
DESTKOP_USAGE_System_Control							=	0x80 ,
DESTKOP_USAGE_System_PowerDown							=	0x81 ,
DESTKOP_USAGE_System_Sleep								=	0x82 ,
DESTKOP_USAGE_System_WakeUp								=	0x83 ,
DESTKOP_USAGE_System_ContextMenu						=	0x84 ,
DESTKOP_USAGE_System_MainMenu							=	0x85 ,
DESTKOP_USAGE_System_AppMenu							=	0x86 ,
DESTKOP_USAGE_System_MenuHelp							=	0x87 ,
DESTKOP_USAGE_System_MenuExit							=	0x88 ,
DESTKOP_USAGE_System_MenuSelect							=	0x89 ,
DESTKOP_USAGE_System_MenuRight							=	0x8A ,
DESTKOP_USAGE_System_MenuLeft							=	0x8B ,
DESTKOP_USAGE_System_MenuUp								=	0x8C ,
DESTKOP_USAGE_System_MenuDown							=	0x8D ,
DESTKOP_USAGE_System_ColdRestart						=	0x8E ,
DESTKOP_USAGE_System_WarmRestart						=	0x8F ,
DESTKOP_USAGE_Dpad_Up									=	0x90 ,
DESTKOP_USAGE_Dpad_Down									=	0x91 ,
DESTKOP_USAGE_Dpad_Right								=	0x92 ,
DESTKOP_USAGE_Dpad_Left									=	0x93 ,
DESTKOP_USAGE_System_Dock								=	0xA0 ,
DESTKOP_USAGE_System_Undock								=	0xA1 ,
DESTKOP_USAGE_System_Setup								=	0xA2 ,
DESTKOP_USAGE_System_Break								=	0xA3 ,
DESTKOP_USAGE_System_Debugger_Break						=	0xA4 ,
DESTKOP_USAGE_Application_Break							=	0xA5 ,
DESTKOP_USAGE_Application_Debugger_Break				=	0xA6 ,
DESTKOP_USAGE_System_Speaker_Mute						=	0xA7 ,
DESTKOP_USAGE_System_Hibernate							=	0xA8 ,
DESTKOP_USAGE_System_Display_Invert						=	0xB0 , 
DESTKOP_USAGE_System_Display_Internal					=	0xB1 , 
DESTKOP_USAGE_System_Display_External					=	0xB2 , 
DESTKOP_USAGE_System_Display_Both						=	0xB3 , 
DESTKOP_USAGE_System_Display_Dual						=	0xB4 , 
DESTKOP_USAGE_System_Display_Toggle_IntExt				=	0xB5 , 
DESTKOP_USAGE_System_Display_Swap_PrimarySecondary		=	0xB6 , 
DESTKOP_USAGE_System_Display_LCD_Autoscale				=	0xB7 , 
}hid_usage_desktop;								   


typedef enum
{
DIGITIZER_USAGE_Undefined							=0x00 ,
DIGITIZER_USAGE_Digitizer							=0x01 ,
DIGITIZER_USAGE_Pen									=0x02 ,
DIGITIZER_USAGE_Light_Pen							=0x03 ,
DIGITIZER_USAGE_Touch_Screen						=0x04 ,
DIGITIZER_USAGE_Touch_Pad							=0x05 ,
DIGITIZER_USAGE_White_Board							=0x06 ,
DIGITIZER_USAGE_Coordinate_Measuring_Machine		=0x07 ,
DIGITIZER_USAGE_3D_Digitizer						=0x08 ,
DIGITIZER_USAGE_Stereo_Plotter						=0x09 ,
DIGITIZER_USAGE_Articulated_Arm						=0x0A ,
DIGITIZER_USAGE_Armature							=0x0B ,
DIGITIZER_USAGE_Multiple_Point_Digitizer			=0x0C ,
DIGITIZER_USAGE_Free_Space_Wand						=0x0D ,
DIGITIZER_USAGE_1F_Reserved							=0x0E ,
DIGITIZER_USAGE_Stylus								=0x20 ,
DIGITIZER_USAGE_Puck								=0x21 ,
DIGITIZER_USAGE_Finger								=0x22 ,
DIGITIZER_USAGE_2F_Reserved							=0x23 ,
DIGITIZER_USAGE_Tip_Pressure						=0x30 ,
DIGITIZER_USAGE_Barrel_Pressure						=0x31 ,
DIGITIZER_USAGE_In_Range							=0x32 ,
DIGITIZER_USAGE_Touch								=0x33 ,
DIGITIZER_USAGE_Untouch								=0x34 ,
DIGITIZER_USAGE_Tap									=0x35 ,
DIGITIZER_USAGE_Quality								=0x36 ,
DIGITIZER_USAGE_Data_Valid							=0x37 ,
DIGITIZER_USAGE_Transducer_Index					=0x38 ,
DIGITIZER_USAGE_Tablet_Function_Keys				=0x39 ,
DIGITIZER_USAGE_Program_Change_Keys					=0x3A ,
DIGITIZER_USAGE_Battery_Strength					=0x3B ,
DIGITIZER_USAGE_Invert								=0x3C ,
DIGITIZER_USAGE_X_Tilt								=0x3D ,
DIGITIZER_USAGE_Y_Tilt								=0x3E ,
DIGITIZER_USAGE_Azimuth								=0x3F ,
DIGITIZER_USAGE_Altitude							=0x40 ,
DIGITIZER_USAGE_Twist								=0x41 ,
DIGITIZER_USAGE_Tip_Switch							=0x42 ,
DIGITIZER_USAGE_Secondary_Tip_Switch				=0x43 ,
DIGITIZER_USAGE_Barrel_Switch						=0x44 ,
DIGITIZER_USAGE_Eraser								=0x45 ,
DIGITIZER_USAGE_Tablet_Pick							=0x46 ,
}hid_usage_digitizer;	


unsigned int	kernel_log_id;



//#define HID_DEBUG

typedef enum
{
	HID_TYPE_MAIN		=0,
	HID_TYPE_GLOBAL		=1,
	HID_TYPE_LOCAL		=2,
	HID_TYPE_RESERVED	=3,
}hid_type_t;


/*
6.2.2.4 Main Items 
Main items are used to either define or group certain types of data fields within a 
Report descriptor. There are two types of Main items: data and non-data. Datatype Main items are used to create a field within a report and include Input, 
Output, and Feature. Other items do not create fields and are subsequently 
referred to as non-data Main items. 
Main item tag 
One-Byte 
Prefix (nn	represents size value) Valid Data 
Input 1000 00 nn 
Bit 0 {Data (0) | Constant (1)} 
Bit 1 {Array (0) | Variable (1)} 
Bit 2 {Absolute (0) | Relative (1)} 
Bit 3 {No Wrap (0) | Wrap (1)} 
Bit 4 {Linear (0) | Non Linear (1)} 
Bit 5 {Preferred State (0) | No Preferred (1)} 
Bit 6 {No Null position (0) | Null state(1)} 
Bit 7 Reserved (0) 
Bit 8 {Bit Field (0) | Buffered Bytes (1)} 
Bit 31-9 Reserved (0) 
Output 1001 00 nn 
Bit 0 {Data (0) | Constant (1)} 
Bit 1 {Array (0) | Variable (1)} 
Bit 2 {Absolute (0) | Relative (1)} 
Bit 3 {No Wrap (0) | Wrap (1)} 
Bit 4 {Linear (0) | Non Linear (1)} 
Bit 5 {Preferred State (0) | No Preferred (1)} 
Bit 6 {No Null position (0) | Null state(1)} 
Bit 7 {Non Volatile (0) | Volatile (1)} 
Bit 8 {Bit Field (0) | Buffered Bytes (1)} 
Bit 31-9 Reserved (0) 
Feature 1011 00 nn 
Bit 0 {Data (0) | Constant (1)} 
Bit 1 {Array (0) | Variable (1)} 
Bit 2 {Absolute (0) | Relative (1)} 
Bit 3 {No Wrap (0) | Wrap (1)} 
Bit 4 {Linear (0) | Non Linear (1)} 
Bit 5 {Preferred State (0) | No Preferred (1)} 
Bit 6 {No Null position (0) | Null state(1)} 
Bit 7 {Non Volatile (0) | Volatile (1)} 
Bit 8 {Bit Field (0) | Buffered Bytes (1)} 
Bit 31-9 Reserved (0) 
Collection 1010 00 nn 
0x00 Physical	 (group of axes) 
0x01 Application (mouse, keyboard) 
0x02 Logical	 (interrelated data) 
0x03 Report 
0x04 Named Array 
0x05 Usage Switch 
0x06 Usage Modifier 
0x07-0x7F Reserved 
0x80-0xFF Vendor-defined 
End Collection 1100 00 nn Not applicable. Closes an item collection. 
*/
typedef enum
{
	HID_TAG_MAIN_INPUT			=0x08,
	HID_TAG_MAIN_OUTPUT			=0x09,
	HID_TAG_MAIN_FEATURE		=0x0B,
	HID_TAG_MAIN_COLLECTION		=0x0A,
	HID_TAG_MAIN_END_COLLECTION	=0x0C
}hid_main_tag_t;

/*
6.2.2.6 Collection, End Collection Items 
A Collection item identifies a relationship between two or more data (Input, 
Output, or Feature.) For example, a mouse could be described as a collection of 
two to four data (x, y, button 1, button 2). While the Collection item opens a 
collection of data, the End Collection item closes a collection


Physical	   0x00 A physical collection is used for a set of data items that represent data points collected at one geometric point. This is useful for sensing devices which may need to associate sets of measured or sensed data with a single point. It does not indicate that a set of data values comes from one device, such as a keyboard. In the case of device which reports the position of multiple sensors, physical collections are used to show which data comes from each separate sensor. 
Application    0x01 A group of Main items that might be familiar to applications. It could also be used to identify item groups serving different purposes in a single device. Common examples are a keyboard or mouse. A keyboard with an integrated pointing device could be defined as two different application collections. Data reports are usually (but not necessarily) associated with application collections (at least one report ID per application). 
Logical		   0x02 A logical collection is used when a set of data items form a composite data structure. An example of this is the association between a data buffer and a byte count of the data. The collection establishes the link between the count and the buffer. Report 0x03 Defines a logical collection that wraps all the fields in a report. A unique report ID will be contained in this collection. An application can easily determine whether a device supports a certain function. Note that any valid Report ID value can be declared for a Report collection. 
Named Array    0x04 A named array is a logical collection contains an array of selector usages. For a given function the set of selectors used by similar devices may vary. The naming of fields is common practice when documenting hardware registers. To determine whether a device supports a particular function like Status, an application might have to query for several known Status selector usages before it could determine whether the device supported Status. The Named Array usages allows the Array field that contains the selectors to be named, thus the application only needs to query for the Status usage to determine that a device supports status information. 
Usage Switch   0x05 A Usage Switch is a logical collection that modifies the meaning of the usages that it contains. This collection type indicates to an application that the usages found in this collection must be special cased. For instance, rather than declaring a usage on the LED page for every possible function, an Indicator usage can be applied to a Usage Switch collection and the standard usages defined in that collection can now be identified as indicators for a function rather than the function itself. Note that this collection type is not used for the labeling Ordinal collections, a Logical collection type is used for that. 
Usage Modifier 0x06 Modifies the meaning of the usage attached to the encompassing collection. A usage typically defines a single operating mode for a control. The usage modifier allows the operating mode of a control to be extended. For instance, an LED is typically on or off. For particular states a device may want a generic method of blinking or choosing the color of a standard LED. Attaching the LED usage to a Usage Modifier collection will indicate to an application that the usage supports a new operating mode. 
*/

typedef enum
{
COLLECTION_Physical	      =0x00 ,
COLLECTION_Application    =0x01 ,
COLLECTION_Logical		  =0x02 ,
COLLECTION_Named_Array    =0x04 ,
COLLECTION_Usage_Switch   =0x05 ,
COLLECTION_Usage_Modifier =0x06 
}hid_collection_type;


/*
6.2.2.7 Global Items 
Global items describe rather than define data from a control. A new Main item 
assumes the characteristics of the item state table. Global items can change the 
state table. As a result Global item tags apply to all subsequently defined items 
unless overridden by another Global item. 
Global item tag 
One-Byte Prefix (nn represents size value) Description 
Usage Page			0000 01 nn Unsigned integer specifying the current Usage Page. Since a usage are 32 bit values, Usage Page items can be used to conserve space in a report descriptor by setting the high order 16 bits of a subsequent usages. Any usage that follows which is defines 16 bits or less is interpreted as a Usage ID and concatenated with the Usage Page to form a 32 bit Usage. 
Logical Minimum		0001 01 nn Extent value in logical units. This is the minimum value that a variable or array item will report. For example, a mouse reporting x position values from 0 to 128 would have a Logical Minimum of 0 and a Logical Maximum of 128. 
Logical Maximum		0010 01 nn Extent value in logical units. This is the maximum value that a variable or array item will report. 
Physical Minimum	0011 01 nn Minimum value for the physical extent of a variable item. This represents the Logical Minimum with units applied to it. 
Physical Maximum	0100 01 nn Maximum value for the physical extent of a variable item. 
Unit Exponent		0101 01 nn Value of the unit exponent in base 10. See the table later in this section for more information. 
Unit				0110 01 nn Unit values. 
Report Size			0111 01 nn Unsigned integer specifying the size of the report fields in bits. This allows the parser to build an item map for the report handler to use. 
Report ID	 		1000 01 nn Unsigned value that specifies the Report ID. If a Report ID tag is used anywhere in Report descriptor, all data reports for the device are preceded by a single byte ID field. All items succeeding the first Report ID tag but preceding a second Report ID tag are included in a report prefixed by a 1-byte ID. All items succeeding the second but preceding a third Report ID tag are included in a second report prefixed by a second ID, and so on. This Report ID value indicates the prefix added to a particular report. For example, a Report descriptor could define a 3-byte report with a Report ID of 01. This device would generate a 4-byte data report in which the first byte is 01. The device may also generate other reports, each with a unique ID. This allows the host to distinguish different types of reports arriving over a single interrupt in pipe. And allows the device to distinguish different types of reports arriving over a single interrupt out pipe. Report ID zero is reserved and should not be used. 
Report Count 		1001 01 nn Unsigned integer specifying the number of data fields for the item; determines how many fields are included in the report for this particular item (and consequently how many bits are added to the report). 
Push		 		1010 01 nn Places a copy of the global item state table on the stack. 
Pop			 		1011 01 nn Replaces the item state table with the top structure from the stack. 
Reserved	 		1100 01 nn to 1111 01 nn Range reserved for future use. 

*/
typedef enum
{
HID_TAG_GLOBAL_USAGE_PAGE	=0x00, 
HID_TAG_GLOBAL_LOGICAL_MIN	=0x01,
HID_TAG_GLOBAL_LOGICAL_MAX	=0x02,
HID_TAG_GLOBAL_PHYSICAL_MIN	=0x03,
HID_TAG_GLOBAL_PHYSICAL_MAX	=0x04,
HID_TAG_GLOBAL_UNIT_EXP		=0x05,
HID_TAG_GLOBAL_UNIT			=0x06,
HID_TAG_GLOBAL_REPORT_SIZE	=0x07,
HID_TAG_GLOBAL_REPORT_ID	=0x08,
HID_TAG_GLOBAL_REPORT_COUNT	=0x09,
HID_TAG_GLOBAL_PUSH			=0x0A,
HID_TAG_GLOBAL_POP			=0x0B
}hid_global_tag_t;

/*
6.2.2.8 Local Items 
Local item tags define characteristics of controls. These items do not carry over to 
the next Main item. If a Main item defines more than one control, it may be 
preceded by several similar Local item tags. For example, an Input item may 
have several Usage tags associated with it, one for each control. 
Description 40 Device Class Definition for Human Interface Devices (HID) Version 1.11 
6/27/00:
Tag 
One-Byte 
Prefix (nn represents  size value) Description 
Usage				0000 10 nn Usage index for an item usage; represents a suggested usage for the item or collection. In the case where an item represents multiple controls, a Usage tag may suggest a usage for every variable or element in an array. 
Usage Minimum		0001 10 nn Defines the starting usage associated with an array or bitmap. 
Usage Maximum		0010 10 nn Defines the ending usage associated with an array or bitmap. 
Designator Index	0011 10 nn Determines the body part used for a control. Index points to a designator in the Physical descriptor. 
Designator Minimum	0100 10 nn Defines the index of the starting designator associated with an array or bitmap. 
Designator Maximum	0101 10 nn Defines the index of the ending designator associated with an array or bitmap. 
String Index		0111 10 nn String index for a String descriptor; allows a string to be associated with a particular item or control. 
String Minimum		1000 10 nn Specifies the first string index when assigning a group of sequential strings to controls in an array or bitmap. 
String Maximum		1001 10 nn Specifies the last string index when assigning a group of sequential strings to controls in an array or bitmap. 
Delimiter			1010 10 nn Defines the beginning or end of a set of local items (1 = open set, 0 = close set). 
Reserved			1010 10 nn to 1111 10 nn
Reserved
*/

typedef enum
{
HID_TAG_LOCAL_USAGE				=0x00,
HID_TAG_LOCAL_USAGE_MIN			=0x01,
HID_TAG_LOCAL_USAGE_MAX			=0x02,
HID_TAG_LOCAL_DESIGNATOR_IDX	=0x03,
HID_TAG_LOCAL_DESIGNATOR_MIN	=0x04,
HID_TAG_LOCAL_DESIGNATOR_MAX	=0x05,
HID_TAG_LOCAL_STRING_INDEX		=0x07,
HID_TAG_LOCAL_STRING_MIN		=0x08,
HID_TAG_LOCAL_STRING_MAX		=0x09,
HID_TAG_LOCAL_DELIMITER			=0x0A
}hid_local_tag;


//hid functions
unsigned int    get_report_size						(struct usb_device *dev	,unsigned int iface_idx);
int			    get_hid_report						(struct usb_device *dev	,unsigned int conf_idx	,unsigned int iface_idx,unsigned int report_id,int *ret_code);
int			    set_hid_idle						(struct usb_device *dev	,unsigned int conf_idx	,unsigned int iface_idx,unsigned int report_id,unsigned int duration,int *ret_code);
device_type_t	get_hid_input_collection			(const mem_zone_ref *input_node);

