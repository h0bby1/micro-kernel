#define USB_DEVICE_DRIVER_FUNC	C_EXPORT
#include <std_def.h>
#include <std_mem.h>
#include "kern.h"
#include "sys_pci.h"
#include "mem_base.h"
#include "lib_c.h"
#include "tree.h"
#include "sys/async_stream.h"
#include "sys/mem_stream.h"
#include "sys/file_system.h"
#include "sys/tpo_mod.h"
#include "sys/stream_dev.h"
#include "../../../bus_manager/bus_drv.h"
#include "sys/usb_device.h"

#include "hid.h"

typedef int C_API_FUNC do_usb_ctrl_transfer_func		(struct usb_device *dev,usb_setup_packet_t	*setup_packet,mem_ptr data,mem_size size,unsigned int packet_size,int *ret_code);
typedef		do_usb_ctrl_transfer_func				*do_usb_ctrl_transfer_func_ptr;


USB_DEVICE_DRIVER_FUNC	  void  C_API_FUNC usb_handle_data				(struct usb_hid *hid,unsigned char *hid_data);
USB_DEVICE_DRIVER_FUNC	  int   C_API_FUNC get_interface_data_size		(struct usb_interface *iface,unsigned int *size);
USB_DEVICE_DRIVER_FUNC	  int   C_API_FUNC initialize_interface			(struct usb_device *dev	,unsigned int conf_idx,unsigned int iface_idx,int *ret_code);
USB_DEVICE_DRIVER_FUNC	  void  C_API_FUNC usb_handle_interface_data	(struct usb_interface *iface,unsigned char *hid_data);


USB_DEVICE_DRIVER_FUNC		do_usb_ctrl_transfer_func_ptr	do_usb_ctrl_transfer =	PTR_INVALID;
USB_DEVICE_DRIVER_FUNC		unsigned char  class_codes[]  = {0x03, 0xFF,0xFF,
														 0x00,0x00,0x00};

unsigned int	kernel_log_id	= 0xFFFFFFFF;

void get_usage_page_str(hid_usage_pages page,char *string)
{
	switch(page)
	{
		case USAGE_PAGE_UNDEFINED:strcpy_c(string,"undefined");break;
		default:
		case USAGE_PAGE_DESKTOP	:strcpy_c(string,"desktop");break;						
		case USAGE_PAGE_SIMULATION:strcpy_c(string,"simulation");break;			
		case USAGE_PAGE_VR:strcpy_c(string,"vr");break;				
		case USAGE_PAGE_SPORT:strcpy_c(string,"sport");break;
		case USAGE_PAGE_GAME:strcpy_c(string,"game");break;
		case USAGE_PAGE_GENERIC_DEVICE:strcpy_c(string,"generic");break;
		case USAGE_PAGE_KEYPAD:strcpy_c(string,"keypad");break;
		case USAGE_PAGE_LED:strcpy_c(string,"led");break;
		case USAGE_PAGE_BUTTON:strcpy_c(string,"button");break;
		case USAGE_PAGE_ORDINAL:strcpy_c(string,"ordinal");break;
		case USAGE_PAGE_TELEPHONY:strcpy_c(string,"telephony");break;
		case USAGE_PAGE_CONSUMER:strcpy_c(string,"consumer");break;
		case USAGE_PAGE_DIGITIZER:strcpy_c(string,"digitizer");break;
		case USAGE_PAGE_FORCEFEEDBACK:strcpy_c(string,"force feedback");break;
		case USAGE_PAGE_UNICODE:strcpy_c(string,"unicode");break;
		case USAGE_PAGE_ALPHANUMERIC:strcpy_c(string,"alphanumeric");break;
		case USAGE_PAGE_MEDICAL:strcpy_c(string,"medical");break;
		case USAGE_PAGE_BARCODE_SCANNER:strcpy_c(string,"barcode scanner");break;
		case USAGE_PAGE_SCALE:strcpy_c(string,"scale");break;
		case USAGE_PAGE_MAGNETIC_STRIP:strcpy_c(string,"magnetic strip");break;
		case USAGE_PAGE_POINT_OF_SALE:strcpy_c(string,"point of sale");break;
		case USAGE_PAGE_CAMERA_CONTROL:strcpy_c(string,"camera control");break;
		case USAGE_PAGE_ARCADE:strcpy_c(string,"arcade");break;
	}
}

unsigned char usb_create_request_type_byte(enum usb_transfer_direction dir,enum usb_transfer_type type,enum usb_transfer_recipient recipient)
{
	unsigned char dir_bits,type_bits,recpt_bits;
	unsigned char RequestType;

	switch(dir)
	{
		case USB_TRANSFER_DIRECTION_HOST_TO_DEVICE: dir_bits=0;	break;
		case USB_TRANSFER_DIRECTION_DEVICE_TO_HOST:	dir_bits=1; break;
	}

	switch(type)
	{
		case USB_TRANSFER_TYPE_STD		:type_bits=0;break;
		case USB_TRANSFER_TYPE_CLASS	:type_bits=1;break;	
		case USB_TRANSFER_TYPE_VENDOR	:type_bits=2;break;
		case USB_TRANSFER_TYPE_RESERVED	:type_bits=3;break;
	}

	switch(recipient)
	{
		case USB_TRANSFER_RECIPIENT_DEVICE		:recpt_bits=0;break;
		case USB_TRANSFER_RECIPIENT_INTERFACE	:recpt_bits=1;break;	
		case USB_TRANSFER_RECIPIENT_ENDPOINT	:recpt_bits=2;break;
		case USB_TRANSFER_RECIPIENT_OTHER		:recpt_bits=3;break;
	}
	
	RequestType	=	((dir_bits&0x1)<<7)|((type_bits&0x3)<<5)|(recpt_bits&0x3);

	return RequestType;
}

struct usb_interface *get_device_interface (struct usb_device *dev,unsigned int conf_idx,unsigned int iface_idx)
{
	struct usb_configuration		*config;
	struct usb_interface			*desc_if;

	if(conf_idx>=dev->device_desc.bNumConfigurations)return PTR_NULL;

	config					=	get_zone_ptr	(&dev->configs_ref,conf_idx*sizeof(struct usb_configuration));

	if(iface_idx>=config->conf_desc.bNumInterfaces)return PTR_NULL;

	desc_if					=	get_zone_ptr	(&config->interfaces_ref,iface_idx*sizeof (struct usb_interface));

	return desc_if;

}

void get_usage_str(hid_usage_pages usage_page,unsigned int usage,char *string)
{
	switch(usage_page)
	{
	default:
		case USAGE_PAGE_DESKTOP:
			switch(usage)
			{
				default:
				case DESTKOP_USAGE_UNDEFINED		:strcpy_c(string,"UNDEFINED");break;
				case DESTKOP_USAGE_Pointer			:strcpy_c(string,"Pointer");break;						
				case DESTKOP_USAGE_Mouse			:strcpy_c(string,"Mouse");break;			
				case DESTKOP_USAGE_Joystick			:strcpy_c(string,"Joystick");break;				
				case DESTKOP_USAGE_GamePad			:strcpy_c(string,"GamePad");break;
				case DESTKOP_USAGE_Keyboard			:strcpy_c(string,"Keyboard");break;
				case DESTKOP_USAGE_Keypad			:strcpy_c(string,"Keypad");break;
				case DESTKOP_USAGE_Multi_Axis		:strcpy_c(string,"Multi_Axis");break;
				case DESTKOP_USAGE_X				:strcpy_c(string,"X");break;
				case DESTKOP_USAGE_Y				:strcpy_c(string,"Y");break;
				case DESTKOP_USAGE_Z				:strcpy_c(string,"Z");break;
				case DESTKOP_USAGE_Rx				:strcpy_c(string,"Rx");break;
				case DESTKOP_USAGE_Ry				:strcpy_c(string,"Ry");break;
				case DESTKOP_USAGE_Rz				:strcpy_c(string,"Rz");break;
				case DESTKOP_USAGE_Slider			:strcpy_c(string,"Slider");break;
				case DESTKOP_USAGE_Dial				:strcpy_c(string,"Dial");break;
				case DESTKOP_USAGE_Wheel			:strcpy_c(string,"Wheel");break;
				case DESTKOP_USAGE_Hat_switch		:strcpy_c(string,"Hat_switch");break;
				case DESTKOP_USAGE_Counted_Buffer	:strcpy_c(string,"Counted_Buffer scanner");break;
				case DESTKOP_USAGE_Byte_Count		:strcpy_c(string,"Byte_Count");break;
				case DESTKOP_USAGE_Motion_Wakeup	:strcpy_c(string,"Motion_Wakeup");break;
				case DESTKOP_USAGE_Start			:strcpy_c(string,"Start");break;
				case DESTKOP_USAGE_Select			:strcpy_c(string,"Select");break;
				case DESTKOP_USAGE_Vx				:strcpy_c(string,"Vx");break;
				case DESTKOP_USAGE_Vy				:strcpy_c(string,"Vy");break;
				case DESTKOP_USAGE_Vz				:strcpy_c(string,"Vz");break;
				case DESTKOP_USAGE_Vbrx				:strcpy_c(string,"Vbrx");break;
				case DESTKOP_USAGE_Vbry				:strcpy_c(string,"Vbry");break;
				case DESTKOP_USAGE_Vbrz				:strcpy_c(string,"Vbrz");break;
				case DESTKOP_USAGE_Vno				:strcpy_c(string,"Vno");break;
			}
		break;
		case USAGE_PAGE_BUTTON:
			switch(usage)
			{
				case 0:strcpy_c(string,"no button");break;
				case 1:strcpy_c(string,"button 1");break;
				case 2:strcpy_c(string,"button 2");break;
				case 3:strcpy_c(string,"button 3");break;
				case 4:strcpy_c(string,"button 4");break;
				case 5:strcpy_c(string,"button 5");break;
				case 6:strcpy_c(string,"button 6");break;
				case 7:strcpy_c(string,"button 7");break;
				case 8:strcpy_c(string,"button 8");break;
				default:strcpy_c(string,"button");break;
			}
		break;
		case USAGE_PAGE_DIGITIZER:
			switch(usage)
			{
				case DIGITIZER_USAGE_Undefined							:strcpy_c(string,"Undefined");break;
				case DIGITIZER_USAGE_Digitizer							:strcpy_c(string,"Digitizer");break;
				case DIGITIZER_USAGE_Pen								:strcpy_c(string,"Pen");break;
				case DIGITIZER_USAGE_Light_Pen							:strcpy_c(string,"Light Pen");break;
				case DIGITIZER_USAGE_Touch_Screen						:strcpy_c(string,"Touch Screen");break;
				case DIGITIZER_USAGE_Touch_Pad							:strcpy_c(string,"Touch Pad");break;
				case DIGITIZER_USAGE_White_Board						:strcpy_c(string,"White Board");break;
				case DIGITIZER_USAGE_Coordinate_Measuring_Machine		:strcpy_c(string,"Coordinate Measuring Machine");break;
				case DIGITIZER_USAGE_3D_Digitizer						:strcpy_c(string,"3D Digitizer");break;
				case DIGITIZER_USAGE_Stereo_Plotter						:strcpy_c(string,"Stereo Plotter");break;
				case DIGITIZER_USAGE_Articulated_Arm					:strcpy_c(string,"Articulated Arm");break;
				case DIGITIZER_USAGE_Armature							:strcpy_c(string,"Armature");break;
				case DIGITIZER_USAGE_Multiple_Point_Digitizer			:strcpy_c(string,"Multiple Point Digitizer");break;
				case DIGITIZER_USAGE_Free_Space_Wand					:strcpy_c(string,"Free Space Wand");break;
				case DIGITIZER_USAGE_1F_Reserved						:strcpy_c(string,"1F Reserved");break;
				case DIGITIZER_USAGE_Stylus								:strcpy_c(string,"Stylus");break;
				case DIGITIZER_USAGE_Puck								:strcpy_c(string,"Puck");break;
				case DIGITIZER_USAGE_Finger								:strcpy_c(string,"Finger");break;
				case DIGITIZER_USAGE_2F_Reserved						:strcpy_c(string,"2F Reserved");break;
				case DIGITIZER_USAGE_Tip_Pressure						:strcpy_c(string,"Tip Pressure");break;
				case DIGITIZER_USAGE_Barrel_Pressure					:strcpy_c(string,"Barrel Pressure");break;
				case DIGITIZER_USAGE_In_Range							:strcpy_c(string,"In Range");break;
				case DIGITIZER_USAGE_Touch								:strcpy_c(string,"Touch");break;
				case DIGITIZER_USAGE_Untouch							:strcpy_c(string,"Untouch");break;
				case DIGITIZER_USAGE_Tap								:strcpy_c(string,"Tap");break;
				case DIGITIZER_USAGE_Quality							:strcpy_c(string,"Quality");break;
				case DIGITIZER_USAGE_Data_Valid							:strcpy_c(string,"Data Valid");break;
				case DIGITIZER_USAGE_Transducer_Index					:strcpy_c(string,"Transducer Index");break;
				case DIGITIZER_USAGE_Tablet_Function_Keys				:strcpy_c(string,"Tablet Function Keys");break;
				case DIGITIZER_USAGE_Program_Change_Keys				:strcpy_c(string,"Program Change Keys");break;
				case DIGITIZER_USAGE_Battery_Strength					:strcpy_c(string,"Battery Strength");break;
				case DIGITIZER_USAGE_Invert								:strcpy_c(string,"Invert");break;
				case DIGITIZER_USAGE_X_Tilt								:strcpy_c(string,"X Tilt");break;
				case DIGITIZER_USAGE_Y_Tilt								:strcpy_c(string,"Y Tilt");break;
				case DIGITIZER_USAGE_Azimuth							:strcpy_c(string,"Azimuth");break;
				case DIGITIZER_USAGE_Altitude							:strcpy_c(string,"Altitude");break;
				case DIGITIZER_USAGE_Twist								:strcpy_c(string,"Twist");break;
				case DIGITIZER_USAGE_Tip_Switch							:strcpy_c(string,"Tip Switch");break;
				case DIGITIZER_USAGE_Secondary_Tip_Switch				:strcpy_c(string,"Secondary Tip Switch");break;
				case DIGITIZER_USAGE_Barrel_Switch						:strcpy_c(string,"Barrel Switch");break;
				case DIGITIZER_USAGE_Eraser								:strcpy_c(string,"Eraser");break;
				case DIGITIZER_USAGE_Tablet_Pick						:strcpy_c(string,"Tablet Pick");break;
			}																	  
		break;																	  
	}
}

void get_hid_collection_string(hid_collection_type collection,char *string)
{
	switch(collection)
	{
		case COLLECTION_Physical	    :strcpy_c(string,"Physical");break;  
		case COLLECTION_Application		:strcpy_c(string,"Application");break;
		case COLLECTION_Logical			:strcpy_c(string,"Logical");break;
		case COLLECTION_Named_Array		:strcpy_c(string,"Named_Array");break;
		case COLLECTION_Usage_Switch	:strcpy_c(string,"Usage_Switch");break;
		case COLLECTION_Usage_Modifier	:strcpy_c(string,"Usage_Modifier");break;

	}
}


struct hid_report_desc_global
{
	unsigned int	usage_page;
	unsigned int	logical_min;
	unsigned int	logical_max;
	unsigned int	physical_min;
	unsigned int	physical_max;
	unsigned int	unit_exp;
	unsigned int	unit;
	unsigned int	report_id;
	unsigned int	report_size;
	unsigned int	report_count;
};

struct hid_usage
{
	unsigned short		page;
	unsigned short		usage;
};


struct hid_report_desc_context
{
struct hid_report_desc_global		globals_stack[8];
unsigned int						global_stack_depth;

struct hid_usage					usages[16];
unsigned int						n_usages;



};



void usb_create_get_hid_descriptor_repport(usb_setup_packet_t *packet,unsigned int desc_idx,unsigned int iface_idx,unsigned int descriptorType,unsigned int descriptorSize)
{
	packet->bmRequestType	=	usb_create_request_type_byte	(USB_TRANSFER_DIRECTION_DEVICE_TO_HOST,USB_TRANSFER_TYPE_STD,USB_TRANSFER_RECIPIENT_INTERFACE);
	packet->bRequest		=	USB_TRANSFER_REQ_GET_DESCRIPTOR;
	packet->wValue			=	(descriptorType<<8)|desc_idx;		//Descriptor Type and Descriptor Index
	packet->wIndex			=	iface_idx;							//Interface Number
	packet->wLength			=	descriptorSize;  
}


/*
7.2.1 Get_Report Request 
The Get_Report request allows the host to receive a report via the Control pipe. 
Part Description 
bmRequestType 10100001 
bRequest GET_REPORT 
wValue Report Type and Report ID 
wIndex Interface 
wLength Report Length 
Data Report 
! The wValue field specifies the Report Type in the high byte and the Report 
ID in the low byte. Set Report ID to 0 (zero) if Report IDs are not used. 
Report Type is specified as follows: 
Value Report Type 
01 Input 
02 Output 
03 Feature 
04-FF Reserved 
! This request is useful at initialization time for absolute items and for 
determining the state of feature items. This request is not intended to be used 
for polling the device state on a regular basis. 
! The Interrupt In pipe should be used for recurring Input reports. The Input
report reply has the same format as the reports from Interrupt pipe. 
! An Interrupt Out pipe may optionally be used for low latency Output
reports. Output reports over the Interrupt Out pipe have a format that is 
identical to output reports that are sent over the Control pipe, if an Interrupt 
Out endpoint is not declared. 
*/
void usb_create_get_hid_repport(usb_setup_packet_t *packet,unsigned int report_id,unsigned int iface_id,unsigned int reportSize)
{
	packet->bmRequestType	=	usb_create_request_type_byte	(USB_TRANSFER_DIRECTION_DEVICE_TO_HOST,USB_TRANSFER_TYPE_CLASS,USB_TRANSFER_RECIPIENT_INTERFACE);
	packet->bRequest		=	USB_HID_REQ_GET_REPORT;
	packet->wValue			=	(USB_HID_REPORT_INPUT<<8)|report_id;	//Descriptor Type and Descriptor Index
	packet->wIndex			=	iface_id;								//Interface Number
	packet->wLength			=	reportSize;  
}

void usb_create_set_hid_idle		(usb_setup_packet_t *packet,unsigned int report_id,unsigned int iface_id,unsigned int duration)
{
	packet->bmRequestType	=	usb_create_request_type_byte	(USB_TRANSFER_DIRECTION_HOST_TO_DEVICE,USB_TRANSFER_TYPE_CLASS,USB_TRANSFER_RECIPIENT_INTERFACE);
	packet->bRequest		=	USB_HID_REQ_SET_IDLE;
	packet->wValue			=	(duration<<8)|report_id;	//Descriptor Type and Descriptor Index
	packet->wIndex			=	iface_id;					//Interface Number
	packet->wLength			=	0;  
}

//context manipulation

void init_report_context(struct hid_report_desc_context	*context)
{
	int n;
	context->global_stack_depth					=	0;
	memset_c(context->globals_stack,0,8*sizeof(struct hid_report_desc_global));

	context->n_usages							=	0;
	memset_c(context->usages,0,16*sizeof(struct hid_usage));



	

	n=8;
	while(n--)
	{
		context->globals_stack[n].report_id	=	0xFFFFFFFF;
	}
}


void add_usage(struct hid_report_desc_context	*context,unsigned short page,unsigned short usage)
{
	struct hid_usage  *usages_ptr;

	usages_ptr			=	mem_add(context->usages,context->n_usages*sizeof(struct hid_usage));
	usages_ptr->page	=	page;
	usages_ptr->usage	=	usage;
	context->n_usages++;
}

void reset_usages(struct hid_report_desc_context	*context)
{
	memset_c(context->usages,0,sizeof(struct hid_usage)*16);
	context->n_usages=0;
}


//report manipulation
void init_hid_report	(struct hid_report_desc	*report)
{
	report->root_node.zone				=PTR_NULL;
	report->open_collection_node.zone	=PTR_NULL;

	tree_manager_create_node	("report",NODE_HID_REPORT,&report->root_node);
	tree_manager_create_node	("inputs",NODE_HID_INPUT_LIST,&report->input_list);

	report->has_report_id	=	0;

	
}

unsigned int get_collection_type(mem_zone_ref_const_ptr usage_node)
{
	unsigned int	ret_type;
	unsigned short	usage_page;
	unsigned short	usage_code;

	tree_mamanger_get_node_word		(usage_node,0,&usage_page);
	tree_mamanger_get_node_word		(usage_node,2,&usage_code);

	switch(usage_page)
	{
		case USAGE_PAGE_DESKTOP:
			switch(usage_code)
			{
				case DESTKOP_USAGE_Pointer			:ret_type=DEVICE_TYPE_MOUSE;break;
				case DESTKOP_USAGE_Mouse			:ret_type=DEVICE_TYPE_MOUSE;break;
				case DESTKOP_USAGE_Joystick			:ret_type=DEVICE_TYPE_JOYSTICK;break;
				case DESTKOP_USAGE_GamePad			:ret_type=DEVICE_TYPE_GAMEPAD;break;
				case DESTKOP_USAGE_Keyboard			:ret_type=DEVICE_TYPE_KEYBOARD;break;
				case DESTKOP_USAGE_Keypad			:ret_type=DEVICE_TYPE_KEYBOARD;break;
			}
		break;
		case USAGE_PAGE_DIGITIZER:
			switch(usage_code)
			{
				case DIGITIZER_USAGE_Undefined							:
				case DIGITIZER_USAGE_Digitizer							:
				case DIGITIZER_USAGE_Pen								:
				case DIGITIZER_USAGE_Light_Pen							:
				case DIGITIZER_USAGE_Touch_Screen						:
				case DIGITIZER_USAGE_Touch_Pad							:
				case DIGITIZER_USAGE_White_Board						:
				case DIGITIZER_USAGE_Coordinate_Measuring_Machine		:
				case DIGITIZER_USAGE_3D_Digitizer						:
				case DIGITIZER_USAGE_Stereo_Plotter						:
				case DIGITIZER_USAGE_Articulated_Arm					:
				case DIGITIZER_USAGE_Armature							:
				case DIGITIZER_USAGE_Multiple_Point_Digitizer			:
				case DIGITIZER_USAGE_Free_Space_Wand					:
				case DIGITIZER_USAGE_1F_Reserved						:
				case DIGITIZER_USAGE_Stylus								:
				case DIGITIZER_USAGE_Puck								:
				case DIGITIZER_USAGE_Finger								:
					ret_type=DEVICE_TYPE_DIGITIZER;
				break;
			}
		break;
	}

	return ret_type;
}
void open_collection	(struct hid_report_desc	*report,struct hid_report_desc_context *context,hid_collection_type collection_type)
{
	unsigned int		node_type;
	mem_zone_ref		out_node;

	switch(collection_type)
	{
		case COLLECTION_Physical:		node_type=NODE_HID_COLLECTION_PHYSICAL;break;
		case COLLECTION_Application:	node_type=NODE_HID_COLLECTION_APPLICATION;break;
		case COLLECTION_Logical:		node_type=NODE_HID_COLLECTION_LOGICAL;break;
	}




	out_node.zone=PTR_NULL;

	if(report->open_collection_node.zone==PTR_NULL)
		tree_manager_add_child_node	(&report->root_node				,"COLLECTION",node_type,&out_node);
	else
		tree_manager_add_child_node	(&report->open_collection_node	,"COLLECTION",node_type,&out_node);

	if(context->n_usages>0)
	{
		mem_zone_ref	type_node;
		char			name[256];

		get_usage_str					(context->usages[0].page,context->usages[0].usage,name);
		tree_manager_add_child_node		(&out_node	,name,NODE_HID_USAGE,&type_node);
		tree_manager_write_node_data	(&type_node,&context->usages[0].page	,0,2);
		tree_manager_write_node_data	(&type_node,&context->usages[0].usage	,2,2);

	

		if(node_type==NODE_HID_COLLECTION_APPLICATION)
		{
			unsigned int main_type=get_collection_type(&type_node);
			mem_zone_ref	t_node={PTR_NULL};

			//if(!tree_find_child_node_by_value(&report->input_list,NODE_HID_INPUT_TYPE,"type",main_type,&t_node))
			if(!tree_node_find_child_by_type_value(&report->input_list,NODE_HID_INPUT_TYPE,main_type,&t_node))
			{
				if(tree_manager_add_child_node				(&report->input_list,"type",NODE_HID_INPUT_TYPE,&t_node))
				{
					tree_manager_write_node_dword			(&t_node,0,main_type);
					release_zone_ref						(&t_node);
				}
			}
		}
		release_zone_ref				(&type_node);
	}

	copy_zone_ref		(&report->open_collection_node,&out_node);
	release_zone_ref	(&out_node);


}

void close_collection	(struct hid_report_desc	*report)
{
	if(report->open_collection_node.zone!=PTR_NULL)
	{
		release_zone_ref	(&report->open_collection_node);
		report->open_collection_node.zone	=	PTR_NULL;
	}
}


void add_input			(struct hid_report_desc	*report,struct hid_report_desc_context *context,unsigned int input_bits,unsigned int item_ofset)
{
	mem_zone_ref		input_node;
	mem_zone_ref		val_node;
	mem_zone_ref		t_node;
	struct hid_usage	*usage_ptr;
	unsigned int		n;

	t_node.zone		=	PTR_NULL;

	val_node.zone	=	PTR_NULL;
	input_node.zone	=	PTR_NULL;
	usage_ptr		=	context->usages;

	for(n=0;n<context->globals_stack[0].report_count;n++)
	{
		mem_zone_ref sub_val_node;
		char		 usage_name[256];

		if(report->open_collection_node.zone!=PTR_NULL)
		{
			tree_manager_add_child_node		(&report->open_collection_node	,"INPUT",NODE_HID_INPUT,&input_node);	
		}
		else
		{
			tree_manager_add_child_node		(&report->root_node				,"INPUT",NODE_HID_INPUT,&input_node);	
		}

		get_usage_str					(context->globals_stack[0].usage_page,usage_ptr->usage,usage_name);

		tree_manager_add_child_node		(&input_node	,usage_name  ,NODE_HID_USAGE	,&val_node);
		tree_manager_write_node_word	(&val_node		, 0, context->globals_stack[0].usage_page);// usage_ptr->page);
		tree_manager_write_node_word	(&val_node		,2,usage_ptr->usage);

		writestr_fmt("hid event %d %d %d\n", context->globals_stack[0].usage_page, usage_ptr->page, usage_ptr->usage);


		tree_manager_add_child_node		(&input_node	,"DATA TYPE"  ,NODE_GFX_INT	,&val_node);
		

		sub_val_node.zone	=	PTR_NULL;

		if(input_bits&2) 
		{
			

			if(input_bits&1)
				tree_manager_add_child_node		(&val_node		,"CONST"  ,NODE_GFX_BOOL	,&sub_val_node);
	
			if(input_bits&2)
				tree_manager_add_child_node		(&val_node		,"VAR"  ,NODE_GFX_BOOL		,&sub_val_node);

			if(input_bits&4)
				tree_manager_add_child_node		(&val_node		,"REL"  ,NODE_GFX_BOOL		,&sub_val_node);
			
			if(input_bits&8)
				tree_manager_add_child_node		(&val_node		,"WRAP"  ,NODE_GFX_BOOL		,&sub_val_node);

			if(input_bits&16)
				tree_manager_add_child_node		(&val_node		,"LINEAR"  ,NODE_GFX_BOOL	,&sub_val_node);
			
			if(input_bits&32)
				tree_manager_add_child_node		(&val_node		,"PREFERED"  ,NODE_GFX_BOOL	,&sub_val_node);
			
			if(input_bits&64)
				tree_manager_add_child_node		(&val_node		,"NULL"  ,NODE_GFX_BOOL		,&sub_val_node);

			if(input_bits&256)
				tree_manager_add_child_node		(&val_node		,"BYTES"  ,NODE_GFX_BOOL	,&sub_val_node);
		}
		else
		{
			//! The value returned by an Array item is an index so it is recommended that: 
			//1) An Array field returns a 0 value when no controls in the array are asserted. 
			//2) The Logical Minimum equals 1. 
			//3) The Logical Maximum equal the number of elements in the array
			
			if(input_bits&1)
				tree_manager_add_child_node		(&val_node		,"CONST",NODE_GFX_BOOL,&sub_val_node);
			
			if(input_bits&2)
				tree_manager_add_child_node		(&val_node		,"VAR"  ,NODE_GFX_BOOL	,&sub_val_node);
			
			if(input_bits&4)
				tree_manager_add_child_node		(&val_node		,"REL"  ,NODE_GFX_BOOL	,&sub_val_node);
		}
	

		release_zone_ref				(&sub_val_node);
			

		tree_manager_add_child_node		(&input_node	,"LOGICAL MIN"  ,NODE_GFX_INT,&val_node);	
		tree_manager_write_node_dword	(&val_node		,0,context->globals_stack[0].logical_min);

		if(context->globals_stack[0].logical_min>=0)
			tree_manager_set_child_value_i32(&input_node,"last value",0);
		else
			tree_manager_set_child_value_si32(&input_node,"last_value",0);

		tree_manager_add_child_node		(&input_node	,"LOGICAL MAX"  ,NODE_GFX_INT,&val_node);
		tree_manager_write_node_dword	(&val_node		,0,context->globals_stack[0].logical_max);

		tree_manager_add_child_node		(&input_node	,"PHYSICAL MIN" ,NODE_GFX_INT,&val_node);	
		tree_manager_write_node_dword	(&val_node		,0,context->globals_stack[0].physical_min);

		tree_manager_add_child_node		(&input_node	,"PHYSICAL MAX" ,NODE_GFX_INT,&val_node);
		tree_manager_write_node_dword	(&val_node		,0,context->globals_stack[0].physical_max);

		tree_manager_add_child_node		(&input_node	,"UNIT EXPONENT",NODE_GFX_INT,&val_node);
		tree_manager_write_node_dword	(&val_node		,0,context->globals_stack[0].unit_exp);

		tree_manager_add_child_node		(&input_node	,"UNIT"			,NODE_GFX_INT,&val_node);
		tree_manager_write_node_dword	(&val_node		,0,context->globals_stack[0].unit);

		tree_manager_add_child_node		(&input_node	,"REPORT OFSET"	,NODE_GFX_INT,&val_node);
		tree_manager_write_node_dword	(&val_node		,0,item_ofset);

		tree_manager_add_child_node		(&input_node	,"REPORT SIZE"	,NODE_GFX_INT,&val_node);
		tree_manager_write_node_dword	(&val_node		,0,context->globals_stack[0].report_size);
	
		if(context->globals_stack[0].report_id!=0xFFFFFFFF)
		{
			tree_manager_add_child_node		(&input_node	,"REPORT ID"	,NODE_GFX_INT,&val_node);
			tree_manager_write_node_dword	(&val_node		,0,context->globals_stack[0].report_id);
		}

		item_ofset		+=	context->globals_stack[0].report_size;

			
		//if(tree_find_child_node_by_value	(&report->input_list,NODE_HID_INPUT_TYPE,"type",get_hid_input_collection(&input_node),&t_node))
		if(tree_node_find_child_by_type_value(&report->input_list,NODE_HID_INPUT_TYPE,get_hid_input_collection(&input_node),&t_node))
		{
			tree_manager_node_add_child (&t_node,&input_node);
			release_zone_ref			(&t_node);
		}

		release_zone_ref	(&val_node);
		release_zone_ref	(&input_node);

		if((n+1)<context->n_usages)
			usage_ptr++;
		
	}

}


OS_API_C_FUNC(int) initialize_interface	(struct usb_device *dev	,unsigned int conf_idx,unsigned int iface_idx,int *ret_code)
{
	mem_zone_ref			packet_ref;
	mem_zone_ref			repport_ref;
	unsigned char			*repport_data;
	unsigned char			*repport_data_end;
	usb_setup_packet_t		*setup_packet;
	//usb_configuration		*config;
	struct usb_interface		*desc_if;
	//struct usb_hid_class_desc	*desc_hid_cls;
	int						ret;
	unsigned int			max_frame_size;
	unsigned int			n;

	kernel_log_id			= get_new_kern_log_id("usb_hid:",0xC);


	repport_ref.zone		= PTR_NULL;
	packet_ref.zone			= PTR_NULL;	
	allocate_new_zone			(0,sizeof(usb_setup_packet_t),&packet_ref);
	setup_packet				=	get_zone_ptr	(&packet_ref,0);

	/*
	config					=	get_zone_ptr	(&dev->configs_ref,conf_idx*sizeof(usb_configuration));
	desc_if					=	get_zone_ptr	(&config->interfaces_ref,iface_idx*sizeof (usb_interface));
	*/

	desc_if					=	get_device_interface(dev,conf_idx,iface_idx);
	if(desc_if		== PTR_NULL)
	{
		writestr("usb hid no such interface \n");
		return 0;
	}

	kernel_log		(kernel_log_id,"set idle interface\n");

	if(set_hid_idle	(dev,conf_idx,iface_idx,0,0,ret_code)==0)
	{
		kernel_log		(kernel_log_id,"set hid idle failed \n");
	}

	max_frame_size			=	dev->device_desc.bMaxPacketSize;

	kernel_log		(kernel_log_id,"fetch report\n");

	kernel_log	(kernel_log_id,"fetching ");
	writeint	(desc_if->hid.hid_desc.bNumDescriptors,10);
	writestr	(" hid report desc from iface [");
	writeint	(desc_if->iface_desc.bInterfaceNumber,10);
	writestr	("]\n");
	
	n=0;

	while(n<desc_if->hid.hid_desc.bNumDescriptors)
	{
		struct hid_report_desc_context	report_context;
		struct hid_report_desc			*cur_report;
		unsigned int				usage_min;
		unsigned int				usage_max;
		unsigned int				report_ofset;

		cur_report	=	&desc_if->hid.report[n];

		kernel_log	(kernel_log_id,"parsing hid report desc [");
		writeint	(n,10);
		writestr	("] ");
		writeint	(desc_if->hid.classes[n].wDescriptorLength,10);
		writestr	(" ");
		writeint	(desc_if->hid.classes[n].bDescriptorType,10);
		writestr	("\n");

		allocate_new_zone		(0x0,desc_if->hid.classes[n].wDescriptorLength,&repport_ref);

		repport_data		=	get_zone_ptr(&repport_ref,0);
		repport_data_end	=	get_zone_ptr(&repport_ref,desc_if->hid.classes[n].wDescriptorLength);
		

		usb_create_get_hid_descriptor_repport	(setup_packet,0,desc_if->iface_desc.bInterfaceNumber,desc_if->hid.classes[n].bDescriptorType,desc_if->hid.classes[n].wDescriptorLength);
		ret=do_usb_ctrl_transfer				(dev,setup_packet,repport_data,desc_if->hid.classes[n].wDescriptorLength,max_frame_size,ret_code);

		if(ret == 0)
		{
			writestr("hid error while fetching repport \n");
			release_zone_ref(&repport_ref);
			n++;
			continue;
		}


		init_hid_report			(cur_report);
		init_report_context		(&report_context);

		report_ofset			=	0;

		usage_min=0xFFFFFFFF;
		usage_max=0xFFFFFFFF;
		while(repport_data<repport_data_end)
		{
			unsigned int				usage;
			unsigned int				n_gstack;
			
			unsigned char				byte;
			char						s_byte;

			unsigned short  			word;
			short						s_word;
			unsigned char 				p_type;
			unsigned char 				p_size;
			unsigned char 				p_size_next;
			unsigned char 				p_tag;

			/*
			bSize Numeric expression specifying size of data: 
			0 = 0 bytes 
			1 = 1 byte 
			2 = 2 bytes 
			3 = 4 bytes 
			bType Numeric expression identifying type of item where: 
			0 = Main 
			1 = Global 
			2 = Local 
			3 = Reserved 
			bTag Numeric expression specifying the function of the item. 
			*/

			byte	=	(*repport_data);

			p_size	=	(byte&0x03);
			p_type	=	(byte>>2)&0x03;
			p_tag	=	(byte>>4)&0x0F;

			#ifdef HID_DEBUG
				writestr("descriptor [");
				writeint(n,10);
				writestr("] type : ");
				writeint(p_type,10);
				writestr(" size ");
				writeint(p_size,10);
				writestr(" tag ");
				writeint(p_tag,10);
				writestr(" ");
			#endif

			if(p_tag==0x0F)
			{
				#ifdef HID_DEBUG
					writestr("long item size : ");
					writeint(repport_data[1],10);
					writestr("tag : ");
					writeint(repport_data[2],10);
					writestr(" ");
				#endif

				p_size_next			=	repport_data[1];
				repport_data		+=	3;
			}	
			else
			{
				switch(p_size)
				{
					case 0:p_size_next= 0;break;
					case 1:p_size_next= 1;break;
					case 2:p_size_next= 2;break;
					case 3:p_size_next= 4;break;
				}
				repport_data		++;
			}

			if(p_size>0)
			{
				byte	=	(*repport_data);
				s_byte	=	*((char *)(repport_data));

				if(p_size_next<2)
				{
					s_word	=	s_byte;
					word	=	byte;
				}
				else if(p_size_next<4)
				{
					s_word	=	*((short *)(repport_data));
					word	=	*((unsigned short *)(repport_data));
				}

			}
			else
				byte = 0;
			
			switch(p_type)
			{
				case HID_TYPE_MAIN:

					switch(p_tag)
					{
						case HID_TAG_MAIN_INPUT:

							if((usage_min!=0xFFFFFFFF)&&(usage_max!=0xFFFFFFFF))
							{
								unsigned int usage_idx;

								for(usage_idx=usage_min;usage_idx<=usage_max;usage_idx++)
								{
									add_usage(&report_context,report_context.globals_stack[0].usage_page,usage_idx);
								}
							}
			

							add_input	(cur_report,&report_context,word,report_ofset);


							

							report_ofset	+=	 report_context.globals_stack[0].report_size*report_context.globals_stack[0].report_count;
				
							usage_min		=	0xFFFFFFFF;
							usage_max		=	0xFFFFFFFF;
							
							#ifdef HID_DEBUG

								writestr("input (");

								//If the Input item is an array, only the Data/Constant, Variable/Array and Absolute/Relative attributes apply
								if(word&2) 
								{
									if(word&1)		writestr("const,");		else writestr("data,");
									if(word&2)		writestr("var,");		else writestr("array,");
									if(word&4)		writestr("rel,");		else writestr("abs,");
									if(word&8)		writestr("wrap,");		else writestr("no wrap,");
									if(word&16)		writestr("non linear,");else writestr("linear,");
									if(word&32)		writestr("pref,");		else writestr("no pref,");
									if(word&64)		writestr("null,");		else writestr("no null,");
									if(word&256)	writestr("bytes");		else writestr("bitfield");
								}
								else
								{

									/*
									! The value returned by an Array item is an index so it is recommended that: 
									1) An Array field returns a 0 value when no controls in the array are asserted. 
									2) The Logical Minimum equals 1. 
									3) The Logical Maximum equal the number of elements in the array
									*/
									if(word&1)		writestr("const,");		else writestr("data,");
									if(word&2)		writestr("var,");		else writestr("array,");
									if(word&4)		writestr("rel,");		else writestr("abs,");
								}

								writestr		(") ");
							#endif

	
						break;
						case HID_TAG_MAIN_OUTPUT:
							//report_ofset	+=	report_context.globals_stack[0].report_size*report_context.globals_stack[0].report_count;
							
							kernel_log(kernel_log_id,"output \n");
						break;
						case HID_TAG_MAIN_FEATURE:
							//report_ofset	+=	report_context.globals_stack[0].report_size*report_context.globals_stack[0].report_count;
							
							kernel_log(kernel_log_id,"feature \n");
						break;
						case HID_TAG_MAIN_COLLECTION:
							open_collection(cur_report,&report_context,byte);

						


							

							#ifdef HID_DEBUG
							{
								char string[256];
								get_hid_collection_string	(byte,string);
								writestr					("collection (");
								writestr					(string);
								writestr					(") ");
							}
							#endif
	
						break;
						case HID_TAG_MAIN_END_COLLECTION:

							close_collection(cur_report);

							#ifdef HID_DEBUG
								writestr		("end collection ");
							#endif
	
						break;
					}
					reset_usages(&report_context);

				break;

				case HID_TYPE_GLOBAL:
					switch(p_tag)
					{
						case HID_TAG_GLOBAL_USAGE_PAGE:


							switch(p_size_next)
							{
								case 1:report_context.globals_stack[0].usage_page=byte;break;
								case 2:report_context.globals_stack[0].usage_page=word;break;
							}

						
							//#ifdef HID_DEBUG
							{
								char string[256];
								get_usage_page_str(report_context.globals_stack[0].usage_page,string);
								kernel_log(kernel_log_id,"usage page (");
								writestr(string);
								writestr(" ");
								writeint(p_size_next, 10);
								writestr(")\n");
							}
							//#endif
						break;
						case HID_TAG_GLOBAL_LOGICAL_MIN:

							switch(p_size_next)
							{
								case 1:report_context.globals_stack[0].logical_min=s_byte;break;
								case 2:report_context.globals_stack[0].logical_min=s_word;break;
							}

							#ifdef HID_DEBUG
								writestr("logical min ");
								writeint(report_context.globals_stack[0].logical_min,10);
								writestr(" ");
							#endif
							
						break;
						case HID_TAG_GLOBAL_LOGICAL_MAX:

							switch(p_size_next)
							{
								case 1:report_context.globals_stack[0].logical_max=s_byte;break;
								case 2:report_context.globals_stack[0].logical_max=s_word;break;
							}
							#ifdef HID_DEBUG
								writestr("logical max ");
								writeint(report_context.globals_stack[0].logical_max,10);
								writestr(" ");
							#endif

						break;
						case HID_TAG_GLOBAL_PHYSICAL_MIN:

							switch(p_size_next)
							{
								case 1:report_context.globals_stack[0].physical_min=s_byte;break;
								case 2:report_context.globals_stack[0].physical_min=s_word;break;
							}
							#ifdef HID_DEBUG
								writestr("physical min ");
								writeint(report_context.globals_stack[0].physical_min,10);
								writestr(" ");
							#endif
							
						break;
						case HID_TAG_GLOBAL_PHYSICAL_MAX:
							switch(p_size_next)
							{
								case 1:report_context.globals_stack[0].physical_max=s_byte;break;
								case 2:report_context.globals_stack[0].physical_max=s_word;break;
							}

							#ifdef HID_DEBUG
								writestr("physical max ");
								writeint(report_context.globals_stack[0].physical_max,10);
								writestr(" ");
							#endif

						break;
						case HID_TAG_GLOBAL_UNIT_EXP:

							switch(p_size_next)
							{
								case 1:report_context.globals_stack[0].unit_exp=byte;break;
								case 2:report_context.globals_stack[0].unit_exp=word;break;
							}


							#ifdef HID_DEBUG
								writestr("unit exp ");
								writeint(report_context.globals_stack[0].unit_exp,10);
								writestr(" ");
							#endif

						break;
						case HID_TAG_GLOBAL_UNIT:
							switch(p_size_next)
							{
								case 1:report_context.globals_stack[0].unit=byte;break;
								case 2:report_context.globals_stack[0].unit=word;break;
							}


							#ifdef HID_DEBUG
								writestr("unit ");
								writeint(report_context.globals_stack[0].unit,10);
								writestr(" ");
							#endif
						break;
						case HID_TAG_GLOBAL_REPORT_SIZE:

							switch(p_size_next)
							{
								case 1:report_context.globals_stack[0].report_size=byte;break;
								case 2:report_context.globals_stack[0].report_size=word;break;
							}


							#ifdef HID_DEBUG
								writestr("report size ");
								writeint(report_context.globals_stack[0].report_size,10);
								writestr(" ");
							#endif

						break;
						case HID_TAG_GLOBAL_REPORT_ID:

							switch(p_size_next)
							{
								case 1:report_context.globals_stack[0].report_id=byte;break;
								case 2:report_context.globals_stack[0].report_id=word;break;
							}
						
							cur_report->has_report_id=1;
							report_ofset	+=8;



							#ifdef HID_DEBUG
								writestr("report id ");
								writeint(report_context.globals_stack[0].report_id,10);
								writestr(" ");
							#endif

						break;

						case HID_TAG_GLOBAL_REPORT_COUNT:

							switch(p_size_next)
							{
								case 1:report_context.globals_stack[0].report_count=byte;break;
								case 2:report_context.globals_stack[0].report_count=word;break;
							}

							#ifdef HID_DEBUG
								writestr("report count ");
								writeint(report_context.globals_stack[0].report_count,10);
								writestr(" ");
							#endif

						break;
						case HID_TAG_GLOBAL_PUSH:
							

							report_context.global_stack_depth++;

							n_gstack	=	report_context.global_stack_depth;

							while(n_gstack--)
							{
								report_context.globals_stack[n_gstack+1]	=	report_context.globals_stack[n_gstack];
							}
							#ifdef HID_DEBUG
								writestr("push ");
							#endif

						break;
						case HID_TAG_GLOBAL_POP:

							n_gstack	=	report_context.global_stack_depth;
							while(n_gstack--)
							{
								report_context.globals_stack[n_gstack]	=	report_context.globals_stack[n_gstack+1];
							}
							report_context.global_stack_depth--;

							#ifdef HID_DEBUG
								writestr("pop ");
							#endif
						break;
					}
				break;

				case HID_TYPE_LOCAL:
					/*
						While Local items do not carry over to the next Main item, they may apply to 
						more than one control within a single item. For example, if an Input item 
						defining five controls is preceded by three Usage tags, the three usages would 
						be assigned sequentially to the first three controls, and the third usage would 
						also be assigned to the fourth and fifth controls. If an item has no controls 
						(Report Count = 0), the Local item tags apply to the Main item (usually a 
						collection item).
					*/
					switch(p_tag)
					{
						case	HID_TAG_LOCAL_USAGE:
							switch(p_size_next)
							{
								case 1:usage	=		(report_context.globals_stack[0].usage_page<<16)|byte;break;
								case 2:usage	=		(report_context.globals_stack[0].usage_page<<16)|word;break;
								//case 4:usage	=		dword;break;
							}

							

							add_usage		(&report_context,(usage>>16)&0xFFFF,usage&0xFFFF);

							#ifdef HID_DEBUG
							{
								char string[256];
								writestr("usage (");
								if(report_context.globals_stack[0].usage_page==USAGE_PAGE_BUTTON)
								{
									writestr("button ");
									writeint(usage&0xFFFF,10);
									writestr(" pressed ");
								}
								else
								{
									memset_c		(string,0,256);
									get_usage_str	((usage>>16)&0xFFFF,usage&0xFFFF,string);
									writestr		(string);
								}
								writestr(") ");
							}
							#endif
						break;
						case	HID_TAG_LOCAL_USAGE_MIN:

							switch(p_size_next)
							{
								case 1:usage_min	=	(report_context.globals_stack[0].usage_page<<16)|byte;break;
								case 2:usage_min	=	(report_context.globals_stack[0].usage_page<<16)|word;break;
								//case 4:usage_min	=		dword;break;
							}

							#ifdef HID_DEBUG
								writestr("usage min (");
								writeint(usage_min&0xFFFF,10);
								writestr(") ");
							#endif
						
						break;
						case	HID_TAG_LOCAL_USAGE_MAX:
							switch(p_size_next)
							{
								case 1:usage_max	=	(report_context.globals_stack[0].usage_page<<16)|byte;break;
								case 2:usage_max	=	(report_context.globals_stack[0].usage_page<<16)|word;break;
								//case 4:usage_min	=		dword;break;
							}

							#ifdef HID_DEBUG
								writestr("usage max (");
								writeint(usage_max&0xFFFF,10);
								writestr(") ");
							#endif
						break;
						case	HID_TAG_LOCAL_DESIGNATOR_IDX:
							writestr("des idx \n");
						break;
						case	HID_TAG_LOCAL_DESIGNATOR_MIN:
							writestr("des min \n");
						break;
						case	HID_TAG_LOCAL_DESIGNATOR_MAX:
							writestr("des max \n");
						break;
						case	HID_TAG_LOCAL_STRING_INDEX:
							writestr("str idx \n");
						break;
						case	HID_TAG_LOCAL_STRING_MIN:
							writestr("str min \n");
						break;
						case	HID_TAG_LOCAL_STRING_MAX:
							writestr("str max \n");
						break;
						case	HID_TAG_LOCAL_DELIMITER:
							writestr("delimit \n");	
						break;
					}
				break;
			}

			#ifdef HID_DEBUG
				writestr("\n");
			#endif

			repport_data+=p_size_next;
			
		}

		desc_if->hid.report[n].report_total_size=report_ofset;
		release_zone_ref			(&repport_ref);
		n++;
	}


	
	//tree_manager_dump_node_rec	(&desc_if->hid.report[0].root_node,0);
	return 1;
}


device_type_t	get_hid_input_collection	(const mem_zone_ref *input_node)
{
	mem_zone_ref   p_node,cur_node,usage_node;
	unsigned int	ret_type;

	p_node.zone		=	PTR_NULL;
	cur_node.zone	=	PTR_NULL;
	usage_node.zone	=	PTR_NULL;
	ret_type		=	0;

	copy_zone_ref(&cur_node,input_node);

	while((tree_mamanger_get_parent(&cur_node,&p_node)!=0)&&(ret_type==0))
	{
		unsigned int p_type		=tree_mamanger_get_node_type(&p_node);

		if(p_type==NODE_HID_COLLECTION_APPLICATION)
		{
			if(tree_node_find_child_by_type(&p_node,NODE_HID_USAGE,&usage_node,0)!=0)
			{
				ret_type=get_collection_type(&usage_node);
				release_zone_ref(&usage_node);
			}
			
		}
		copy_zone_ref		(&cur_node,&p_node);
		release_zone_ref	(&p_node);
	}
	release_zone_ref	(&cur_node);
	return ret_type;
}

OS_API_C_FUNC(void) usb_handle_interface_data(struct usb_interface *iface,unsigned char *hid_data)
{
	mem_zone_ref			input;
	mem_zone_ref			t_node;
	struct usb_hid			*hid;
	unsigned int			n;
	unsigned int			n_listener;
	dev_event_listener_t	*listener;
	n			=	0;

	hid			=	&iface->hid;
	
	input.zone	=	PTR_NULL;
	t_node.zone	=	PTR_NULL;
	//writestr						("hid report ");

	
	while(tree_node_list_child_by_type(&hid->report[0].root_node,NODE_HID_INPUT,&input,n)>0)
	{
		unsigned int		ofset,size;
		unsigned int		ofset_byte;
		unsigned int		ofset_bit;
		int					logical_min;
		int					logical_max;
		unsigned int		rep_data;
		int					rep_data_s;
		unsigned int		data_mask;
		device_type_t		main_type;
		unsigned short		usage_page,usage;
		mem_zone_ref		val_node;

		val_node.zone=PTR_NULL;

		tree_manager_get_child_value_i32(&input,NODE_HASH("REPORT OFSET"),&ofset);
		tree_manager_get_child_value_i32(&input,NODE_HASH("REPORT SIZE") ,&size);

		if(tree_node_find_child_by_type	(&input,NODE_HID_USAGE,&val_node,0)==0){release_zone_ref(&input);continue;}
		tree_mamanger_get_node_word		(&val_node,0,&usage_page);
		tree_mamanger_get_node_word		(&val_node,2,&usage);
		release_zone_ref				(&val_node);


		if(!tree_manager_get_child_value_si32(&input,NODE_HASH("LOGICAL MIN"),&logical_min))logical_min=-1;
		if(!tree_manager_get_child_value_si32(&input,NODE_HASH("LOGICAL MAX"),&logical_max))logical_max=-1;

		data_mask					=	~(0xFFFFFFFF << size);
		ofset_byte					=	ofset/8;
		ofset_bit					=	ofset%8;
		rep_data					=	(( (*((unsigned int *)(hid_data+ofset_byte)))>>ofset_bit)&data_mask);
	
		main_type					=	get_hid_input_collection	(&input);
		/*
		kernel_log(kernel_log_id,"event type");
		writeint(main_type,16);
		writestr("\n");
		*/



		if(logical_min>=0)
		{
			tree_manager_set_child_value_i32(&input,"last value",rep_data);
		}
		else
		{
			unsigned int sign_bit;

			sign_bit				=	(1<<(size-1));
			if(rep_data&sign_bit)
			{
				rep_data_s			=	 -(int)(((~rep_data)&data_mask)+1);
			}
			else
			{
				rep_data_s			=	 rep_data;
			}
			tree_manager_set_child_value_si32(&input,"last value",rep_data_s);
		}
		release_zone_ref				(&val_node);
		release_zone_ref				(&input);
		n++;
		
	}

	
	//snooze(4000000);

	//tree_manager_dump_node_rec(&hid->report[0].input_list,0,3);

	n=0;
	while(tree_manager_get_child_at(&hid->report[0].input_list,n,&t_node))
	{
		int type;
		tree_mamanger_get_node_dword	(&t_node,0,&type);
		n_listener=0;
		while((listener	= bus_manager_find_dev_event_listener	(type,n_listener))!=PTR_NULL)
		{
			if(listener->listener_fnc!=PTR_NULL)
			{
				listener->listener_fnc		(type,&t_node);
			}
			n_listener++;
		}
		release_zone_ref(&t_node);
		n++;
	}
}
int set_hid_idle				(struct usb_device *dev	,unsigned int conf_idx,unsigned int iface_idx,unsigned int report_id,unsigned int duration,int *ret_code)
{
	mem_zone_ref			packet_ref;
	usb_setup_packet_t		*setup_packet;
	struct usb_interface	*iface;
	unsigned int			max_frame_size,ret;

	iface					=	get_device_interface	(dev,conf_idx,iface_idx);
	if(iface==PTR_NULL)
	{
		writestr("hid set idle wrong interface \n");
		return 0;
	}

	packet_ref.zone			= PTR_NULL;	
	max_frame_size			=	dev->device_desc.bMaxPacketSize;

	allocate_new_zone			(0,sizeof(usb_setup_packet_t),&packet_ref);
	setup_packet			=	get_zone_ptr	(&packet_ref,0);

	usb_create_set_hid_idle			(setup_packet	,report_id,iface->iface_desc.bInterfaceNumber,duration);

	kernel_log		(kernel_log_id,"do transfer ");
	writeptr		(do_usb_ctrl_transfer);
	writestr		("\n");

	ret=do_usb_ctrl_transfer		(dev,setup_packet,PTR_NULL,0,max_frame_size,ret_code);

	kernel_log		(kernel_log_id,"transfer done ");
	writeint		(ret,16);
	writestr		("\n");
	if(ret==0)
	{
		writestr("hid set idle failed return code");
		writeint((*ret_code),16);
		writestr("\n");
		return 0;
	}

	return 1;
}
OS_API_C_FUNC(int) get_interface_data_size(struct usb_interface *iface,unsigned int *size)
{
	(*size)= (iface->hid.report[0].report_total_size+7)/8;

	return 1;
}

int get_hid_report				(struct usb_device *dev	,unsigned int conf_idx,unsigned int iface_idx,unsigned int report_id,int *ret_code)
{
	mem_zone_ref			packet_ref;
	mem_zone_ref			report_ref;
	usb_setup_packet_t		*setup_packet;
	unsigned char			*report_data;
	struct usb_interface	*iface;
	unsigned int			max_frame_size,ret;
	unsigned int			report_size_bytes;



	iface					=	get_device_interface	(dev,conf_idx,iface_idx);
	if(iface==PTR_NULL)
	{
		writestr("hid report wrong interface \n");
		return 0;
	}

	packet_ref.zone			= PTR_NULL;	
	report_ref.zone			= PTR_NULL;	
	report_size_bytes		=	(iface->hid.report[0].report_total_size+7)/8;


	allocate_new_zone			(0,sizeof(usb_setup_packet_t),&packet_ref);
	setup_packet			=	get_zone_ptr	(&packet_ref,0);

	allocate_new_zone			(0,report_size_bytes,&report_ref);
	report_data				=	get_zone_ptr	(&report_ref,0);

	writestr("hid get report [");
	writeint(report_id,10);
	writestr("] on interface[");
	writeint(iface->iface_desc.bInterfaceNumber,10);
	writestr("] length ");
	writeint(report_size_bytes,10);
	writestr("\n");
	

	max_frame_size					=	dev->device_desc.bMaxPacketSize;

	usb_create_get_hid_repport		(setup_packet,report_id,iface->iface_desc.bInterfaceNumber,report_size_bytes);
	ret=do_usb_ctrl_transfer		(dev,setup_packet,report_data,report_size_bytes,max_frame_size,ret_code);

	if(ret==0)
	{
		writestr("hid get report failed \n");
		return 0;
	}
	return 1;
}

