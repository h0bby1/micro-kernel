#define USB_BUS_DRIVER_FUNC	DLL_EXPORT
#include <std_def.h>
#include <std_mem.h>

#include "mem_base.h"
#include "sys/mem_stream.h"
#include "sys/tpo_mod.h"
#include "sys/usb_device.h"
#include "../bus_manager/bus_drv.h"
#include "usb_bus.h"



unsigned char usb_create_request_type_byte(enum usb_transfer_direction dir,enum usb_transfer_type type,enum usb_transfer_recipient recipient)
{
	unsigned char dir_bits,type_bits,recpt_bits;
	unsigned char RequestType;

	switch(dir)
	{
		case USB_TRANSFER_DIRECTION_HOST_TO_DEVICE: dir_bits=0;	break;
		case USB_TRANSFER_DIRECTION_DEVICE_TO_HOST:	dir_bits=1; break;
	}

	switch(type)
	{
		case USB_TRANSFER_TYPE_STD		:type_bits=0;break;
		case USB_TRANSFER_TYPE_CLASS	:type_bits=1;break;	
		case USB_TRANSFER_TYPE_VENDOR	:type_bits=2;break;
		case USB_TRANSFER_TYPE_RESERVED	:type_bits=3;break;
	}

	switch(recipient)
	{
		case USB_TRANSFER_RECIPIENT_DEVICE		:recpt_bits=0;break;
		case USB_TRANSFER_RECIPIENT_INTERFACE	:recpt_bits=1;break;	
		case USB_TRANSFER_RECIPIENT_ENDPOINT	:recpt_bits=2;break;
		case USB_TRANSFER_RECIPIENT_OTHER		:recpt_bits=3;break;
	}
	
	RequestType	=	((dir_bits&0x1)<<7)|((type_bits&0x3)<<5)|(recpt_bits&0x3);

	return RequestType;
}


void usb_create_get_status_packet_device(usb_setup_packet_t *packet)
{
	packet->bmRequestType	=	usb_create_request_type_byte	(USB_TRANSFER_DIRECTION_DEVICE_TO_HOST,USB_TRANSFER_TYPE_STD,USB_TRANSFER_RECIPIENT_DEVICE);
	packet->bRequest		=	USB_TRANSFER_REQ_GET_STATUS;
	packet->wValue			=	0;//Zero
	packet->wIndex			=	0;//Zero
	packet->wLength			=	2;//Two

	//data Device Status
	//The Get Status request directed at the device will return two bytes during the data stage with the following format,
	//D15	 D14	 D13	 D12	 D11	 D10	 D9	 D8	 D7	 D6	 D5	 D4	 D3	 D2		D1				D0
	//Reserved																		Remote Wakeup	Self Powered

}


void usb_create_set_configuration_packet_device(usb_setup_packet_t *packet,unsigned int conf_id)
{
	packet->bmRequestType	=	usb_create_request_type_byte	(USB_TRANSFER_DIRECTION_HOST_TO_DEVICE,USB_TRANSFER_TYPE_STD,USB_TRANSFER_RECIPIENT_DEVICE);
	packet->bRequest		=	USB_TRANSFER_REQ_SET_CONFIGURATION;
	packet->wValue			=	conf_id;	//Configuration Value
	packet->wIndex			=	0;			//Zero
	packet->wLength			=	0;			//Zero

	// Set Configuration is used to enable a device. 
	//It should contain the value of bConfigurationValue of the desired configuration descriptor 
	//in the lower byte of wValue to select which configuration to enable.
	//none
}

void usb_create_get_configuration_packet_device(usb_setup_packet_t *packet)
{
	packet->bmRequestType	=	usb_create_request_type_byte	(USB_TRANSFER_DIRECTION_DEVICE_TO_HOST,USB_TRANSFER_TYPE_STD,USB_TRANSFER_RECIPIENT_DEVICE);
	packet->bRequest		=	USB_TRANSFER_REQ_GET_CONFIGURATION;
	packet->wValue			=	0;	//Zero
	packet->wIndex			=	0;	//Zero
	packet->wLength			=	1;  //One

	//data Configuration Value
	//In the case of a Get Configuration request, a byte will be returned during the data stage indicating the devices status. 
	//A zero value means the device is not configured and a non-zero value indicates the device is configured. 
}

void usb_create_set_address_packet_device(usb_setup_packet_t *packet,unsigned int dev_addr)
{
	packet->bmRequestType	=	usb_create_request_type_byte	(USB_TRANSFER_DIRECTION_HOST_TO_DEVICE,USB_TRANSFER_TYPE_STD,USB_TRANSFER_RECIPIENT_DEVICE);
	packet->bRequest		=	USB_TRANSFER_REQ_SET_ADDRESS;
	packet->wValue			=	dev_addr;		//device address
	packet->wIndex			=	0;				//Zero
	packet->wLength			=	0;				//Zero

	//Set Address is used during enumeration to assign a unique address to the USB device. 
	//The address is specified in wValue and can only be a maximum of 127. 
	//This request is unique in that the device does not set its address until after the completion of the status stage. 
	//(See Control Transfers.) All other requests must complete before the status stage.
}





//Set Descriptor/Get Descriptor is used to return the specified descriptor in wValue. 
//A request for the configuration descriptor will return the device descriptor and all interface 
//and endpoint descriptors in the one request.
//
//  Endpoint Descriptors cannot be accessed directly by a GetDescriptor/SetDescriptor Request.
//  Interface Descriptors cannot be accessed directly by a GetDescriptor/SetDescriptor Request.
//  String Descriptors include a Language ID in wIndex to allow for multiple language support.

void usb_create_set_descriptor_packet_device(usb_setup_packet_t *packet)
{
	packet->bmRequestType	=	usb_create_request_type_byte	(USB_TRANSFER_DIRECTION_HOST_TO_DEVICE,USB_TRANSFER_TYPE_STD,USB_TRANSFER_RECIPIENT_DEVICE);
	packet->bRequest		=	USB_TRANSFER_REQ_SET_DESCRIPTOR;
	packet->wValue			=	0;				//Descriptor Type & Index
	packet->wIndex			=	0;				//Zero or Language ID
	packet->wLength			=	0;				//Descriptor Length

	//data Descriptor

}

//get standard device descriptors
void usb_create_get_descriptor_packet_device(usb_setup_packet_t *packet)
{
	packet->bmRequestType	=	usb_create_request_type_byte	(USB_TRANSFER_DIRECTION_DEVICE_TO_HOST,USB_TRANSFER_TYPE_STD,USB_TRANSFER_RECIPIENT_DEVICE);
	packet->bRequest		=	USB_TRANSFER_REQ_GET_DESCRIPTOR;
	
	//data : Descriptor 
	packet->wValue			=	(1<<8);	//USB_TRANSFER_DESC_TYPE_DEVICE_DESCRIPTOR
	packet->wIndex			=	0;	//Zero
	packet->wLength			=	sizeof(struct usb_device_desc);  
}

void usb_create_get_descriptor_packet_conf(usb_setup_packet_t *packet,unsigned int conf_idx,unsigned int size)
{
	packet->bmRequestType	=	usb_create_request_type_byte	(USB_TRANSFER_DIRECTION_DEVICE_TO_HOST,USB_TRANSFER_TYPE_STD,USB_TRANSFER_RECIPIENT_DEVICE);
	packet->bRequest		=	USB_TRANSFER_REQ_GET_DESCRIPTOR;
	packet->wValue			=	(2<<8)|conf_idx;	//USB_TRANSFER_DESC_TYPE_CONFIG_DESCRIPTOR:
	packet->wIndex			=	0;	//Zero
	packet->wLength			=	size;  
}



void usb_create_get_descriptor_packet_hub(usb_setup_packet_t *packet)
{
	packet->bmRequestType	=	usb_create_request_type_byte	(USB_TRANSFER_DIRECTION_DEVICE_TO_HOST,USB_TRANSFER_TYPE_CLASS,USB_TRANSFER_RECIPIENT_DEVICE);
	packet->bRequest		=	USB_TRANSFER_REQ_GET_DESCRIPTOR;
	packet->wValue			=	(USB_TRANSFER_DESC_TYPE_HUB_DESCRIPTOR<<8);	
	packet->wIndex			=	0;											//Zero
	packet->wLength			=	sizeof(struct usb_hubdesc);				
}


void usb_create_get_port_status_hub(usb_setup_packet_t *packet,unsigned int port_idx)
{
	packet->bmRequestType	=	usb_create_request_type_byte	(USB_TRANSFER_DIRECTION_DEVICE_TO_HOST,USB_TRANSFER_TYPE_CLASS,USB_TRANSFER_RECIPIENT_OTHER);
	packet->bRequest		=	USB_TRANSFER_REQ_GET_STATUS;
	packet->wValue			=	0;
	packet->wIndex			=	port_idx;											//Zero
	packet->wLength			=	4;				
}

void usb_create_enable_port_hub(usb_setup_packet_t *packet,unsigned int port_idx)
{
	packet->bmRequestType	=	usb_create_request_type_byte	(USB_TRANSFER_DIRECTION_HOST_TO_DEVICE,USB_TRANSFER_TYPE_CLASS,USB_TRANSFER_RECIPIENT_OTHER);
	packet->bRequest		=	0x03; //XXX: Why 0x03?  According to some docs it should be 0x0.  Check the specification!  
	packet->wValue			=	0x04;
	packet->wIndex			=	port_idx;										//Zero
	packet->wLength			=	0;				
}


void usb_create_get_descriptor_packet_string_first(usb_setup_packet_t *packet)
{
	packet->bmRequestType	=	usb_create_request_type_byte	(USB_TRANSFER_DIRECTION_DEVICE_TO_HOST,USB_TRANSFER_TYPE_STD,USB_TRANSFER_RECIPIENT_DEVICE);
	packet->bRequest		=	USB_TRANSFER_REQ_GET_DESCRIPTOR;

	packet->wValue			=	(3<<8);	//USB_TRANSFER_DESC_TYPE_STRING_DESCRIPTOR
	packet->wIndex			=	0;	//Zero or Language ID
	packet->wLength			=	sizeof(struct usb_string_desc_zero); 
}

void usb_create_get_descriptor_packet_string(usb_setup_packet_t *packet,unsigned int string_id,unsigned int lang_id,unsigned int str_size)
{
	packet->bmRequestType	=	usb_create_request_type_byte	(USB_TRANSFER_DIRECTION_DEVICE_TO_HOST,USB_TRANSFER_TYPE_STD,USB_TRANSFER_RECIPIENT_DEVICE);
	packet->bRequest		=	USB_TRANSFER_REQ_GET_DESCRIPTOR;

	packet->wValue			=	((3<<8)|string_id);			//USB_TRANSFER_DESC_TYPE_STRING_DESCRIPTOR
	packet->wIndex			=	lang_id;					//Zero or Language ID

	if(str_size>0)
		packet->wLength			=	str_size;
	else
		packet->wLength			=	2;
}




/*
0000 0000b	 CLEAR_FEATURE (0x01)	 Feature Selector	 Zero	 Zero	 None
0000 0000b	 SET_FEATURE (0x03)	 Feature Selector	 Zero	 Zero	 None
*/