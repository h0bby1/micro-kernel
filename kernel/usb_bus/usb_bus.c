#define USB_BUS_DRIVER_FUNC	C_EXPORT

#include <std_def.h>
#include <std_mem.h>
#include <mem_base.h>
#include <sys_pci.h>
#include <lib_c.h>
#include <tree.h>
#include <sys/async_stream.h>
#include <sys/mem_stream.h>
#include <sys/file_system.h>
#include <sys/tpo_mod.h>
#include <sys/stream_dev.h>
#include "../bus_manager/bus_drv.h"
#include <sys/usb_device.h>

#include "usb_bus.h"

/*
typedef struct
{
*/
usb_hubports_func_ptr		controller_usb_hubports			=	PTR_INVALID;
usb_detect_dev_func_ptr		controller_usb_detect_dev		=	PTR_INVALID;
usb_portstatus_func_ptr		controller_usb_portstatus		=	PTR_INVALID;
usb_ctrl_transfer_func_ptr	controller_usb_ctrl_transfer	=	PTR_INVALID;
usb_intr_transfer_func_ptr	controller_usb_intr_transfer	=	PTR_INVALID;
/*
}usb_controller;
usb_controller		controller					=	{PTR_INVALID};
*/

mem_zone_ref			usb_td_pool_td_list;
unsigned int			usb_td_pool_n_td_alloc;
	
mem_zone_ref			usb_td_pool_free_td_list;
unsigned int			usb_td_pool_n_free_td;
unsigned int			usb_td_pool_n_free_td_alloc;

mem_zone_ref			usb_td_pool_used_td_list;
unsigned int			usb_td_pool_n_used_td_alloc;
unsigned int			usb_td_pool_n_used_td;

USB_BUS_DRIVER_FUNC void		 C_API_FUNC init_bus_driver				(bus_driver *bus_drv, unsigned int int_mode);
USB_BUS_DRIVER_FUNC unsigned int C_API_FUNC bus_scan_devices			(bus_driver	*bus_drv);
USB_BUS_DRIVER_FUNC unsigned int C_API_FUNC init_new_bus_device			(bus_driver *bus_drv,mem_zone_ref *dev_node);
USB_BUS_DRIVER_FUNC unsigned int C_API_FUNC init_bus_device				(unsigned int device_id);
USB_BUS_DRIVER_FUNC unsigned int C_API_FUNC find_device_by_type			(unsigned int device_type,unsigned int index);


typedef int C_API_FUNC initialize_interface_func	(struct usb_device *dev	,unsigned int conf_idx,unsigned int iface_idx,int *ret_code);
typedef initialize_interface_func	*initialize_interface_func_ptr;

typedef int	C_API_FUNC usb_handle_interface_data_func(struct usb_interface *iface,unsigned char *hid_data);
typedef usb_handle_interface_data_func *usb_handle_interface_data_func_ptr;

typedef int	C_API_FUNC	get_interface_data_size_func		(struct usb_interface *iface,unsigned int *size);
typedef	get_interface_data_size_func	*get_interface_data_size_func_ptr;


		

mem_zone_ref		usb_devices_array			=	{PTR_INVALID};
unsigned int		n_usb_devices				=	0xFFFFFFFF;


mem_zone_ref		usb_transfer_array			=	{PTR_INVALID};
unsigned int		n_usb_transfer				=	0xFFFFFFFF;
unsigned int		n_usb_transfer_alloc		=	0xFFFFFFFF;
	
unsigned int		kernel_log_id				=	0xFFFFFFFF;
unsigned int		n_tfer_total				=	0xFFFFFFFF;

bus_driver			*global_bus_drv				=	PTR_INVALID;

unsigned char		addr_spots[128]				=	{0xFF};

unsigned int		usb_bus_interupt_mode		= 0xFFFFFFFF;



OS_API_C_FUNC(void) init_bus_driver				(bus_driver *bus_drv, unsigned int int_mode)
{
	unsigned int		n;
	struct usb_device	*devices_ptr;
	usb_td_ptr_t		td_list;
	usb_td_ptr_t	*free_td_list;
	usb_td_ptr_t	*used_td_list;
	usb_bus_interupt_mode = int_mode;

	kernel_log_id			=	get_new_kern_log_id		("usb_bus: ",0x04);
	
	strcpy_s				(bus_drv->name,32,"USB");
	

	global_bus_drv			=	bus_drv;
	usb_devices_array.zone	=	PTR_NULL;
	n_usb_devices			=	0;
	allocate_new_zone		(0,128*sizeof(struct usb_device),&usb_devices_array);


	usb_transfer_array.zone	=	PTR_NULL;
	n_usb_transfer_alloc	=	16;
	n_usb_transfer			=	0;
	allocate_new_zone			(0,n_usb_transfer_alloc*sizeof(struct usb_transfer_t),&usb_transfer_array);

	usb_td_pool_td_list.zone	=	PTR_NULL;
	usb_td_pool_n_td_alloc		=	1024;
	allocate_new_zone			(0,usb_td_pool_n_td_alloc*sizeof(struct usb_td_t),&usb_td_pool_td_list);

	usb_td_pool_free_td_list.zone	=	PTR_NULL;
	usb_td_pool_n_free_td_alloc		=	usb_td_pool_n_td_alloc;
	usb_td_pool_n_free_td			=	usb_td_pool_n_free_td_alloc;
	allocate_new_zone			(0,usb_td_pool_n_free_td_alloc*sizeof(usb_td_ptr_t),&usb_td_pool_free_td_list);

	usb_td_pool_used_td_list.zone	=	PTR_NULL;
	usb_td_pool_n_used_td_alloc		=	usb_td_pool_n_td_alloc;
	usb_td_pool_n_used_td			=	0;
	allocate_new_zone			(0,usb_td_pool_n_used_td_alloc*sizeof(usb_td_ptr_t),&usb_td_pool_used_td_list);

	
	td_list							=	get_zone_ptr(&usb_td_pool_td_list,0);
	free_td_list					=	get_zone_ptr(&usb_td_pool_free_td_list,0);
	used_td_list					=	get_zone_ptr(&usb_td_pool_used_td_list,0);

	for(n=0;n<usb_td_pool_n_td_alloc;n++)
	{
		(*free_td_list)	=	td_list;
		(*used_td_list)	=	PTR_NULL;
		used_td_list++;
		free_td_list++;
		td_list++;
	}

	devices_ptr				=	get_zone_ptr(&usb_devices_array,0);
	
	kernel_log		(kernel_log_id,"init usb driver ");
	writeptr		(devices_ptr);
	writestr		(" imd ");
	writeint		(usb_bus_interupt_mode,10);
	writestr		(" ");
	writeptr		(usb_devices_array.zone);
	writestr		("\n");

	memset_c	(addr_spots,0xFF,128);

	bus_drv->bus_type				=	2;
	n_tfer_total					=	0;

	n=0;
	while(n<64)
	{
		devices_ptr[n].bus_id			=0xFFFFFFFF;
		devices_ptr[n].dev_id			=0xFFFFFFFF;
		devices_ptr[n].cur_configuration=0xFFFFFFFF;
		devices_ptr[n].configs_ref.zone	=PTR_NULL;
		devices_ptr[n].usb_node.zone	=PTR_NULL;
		devices_ptr[n].toggle_control	=1;
		devices_ptr[n].used				=0;

		memset_c						(devices_ptr[n].driver_name,0,128);
		memset_c						(devices_ptr[n].name,0,128);
		n++;
	}
}



struct usb_device	 *get_new_device()
{
	struct usb_device	 *dev_list,*dev_list_end;

	dev_list		=	get_zone_ptr(&usb_devices_array,0);
	dev_list_end	=	get_zone_ptr(&usb_devices_array,0xFFFFFFFF);

	while(dev_list<dev_list_end)
	{
		if(!dev_list->used)
		{
			dev_list->used=1;
			dev_list->dev_id;
			return dev_list;
		}

		dev_list++;
	}

	return PTR_NULL;
}

struct usb_device	*find_usb_device_by_port(unsigned int port_id)
{
	unsigned int		  n;
	struct usb_device	  *devices_ptr;

	devices_ptr	=	get_zone_ptr(&usb_devices_array,0);
	n			=	0;

	while(n<n_usb_devices)
	{
		if(devices_ptr[n].port_id==port_id)return &devices_ptr[n];
		n++;
	}
	return PTR_NULL;

}

struct usb_device	*find_usb_device_by_id(unsigned int dev_id)
{
	unsigned int		  n;
	struct usb_device	  *devices_ptr;

	devices_ptr	=	get_zone_ptr(&usb_devices_array,0);
	n			=	0;
	while(n<n_usb_devices)
	{
		if(devices_ptr[n].dev_id==dev_id)return &devices_ptr[n];
		n++;
	}
	return PTR_NULL;

}
OS_API_C_FUNC(unsigned int) find_device_by_type(unsigned int device_type,unsigned int index)
{


	return 0xFFFFFFFF;
}


unsigned int	create_td_link(struct usb_td_t *td)
{
	unsigned int	td_link;
	unsigned int	td_ptr;

	if(td==PTR_NULL)
	{

		td_link			=	0;
		td_link			=	set_bit		(td_link,1,0); // terminate
	}
	else
	{
		td_ptr			=	mem_to_uint(td);
		td_link			=	td_ptr & 0xFFFFFFF0;

		td_link			=	set_bit		(td_link,0,0); // terminate
		td_link			=	set_bit		(td_link,0,1);	// QH = 1 , TD = 0
		td_link			=	set_bit		(td_link,1,2); // Depth = 1 , Queue = 0
		td_link			=	set_bit		(td_link,0,3); // reserved
	}
	return td_link;
}

usb_td_ptr_t get_new_td()
{
	usb_td_ptr_t		*free_td,*used_td;

	if(usb_td_pool_n_free_td==0)
	{
		writestr("usb bus: no more free td !! \n");
		snooze(10000000);
		return PTR_NULL;
	}

	usb_td_pool_n_free_td--;
	free_td		=	get_zone_ptr(&usb_td_pool_free_td_list,usb_td_pool_n_free_td*sizeof(usb_td_ptr_t));
	used_td		=	get_zone_ptr(&usb_td_pool_used_td_list,usb_td_pool_n_used_td*sizeof(usb_td_ptr_t));
	(*used_td)	=	(*free_td);
	usb_td_pool_n_used_td++;

	return (*free_td);
}

int	remove_used_td(struct td_chain_t *chain)
{
	unsigned int		n,n_td;
	usb_td_ptr_t		*used_td;
	usb_td_ptr_t		td;

	n_td=0;
		
	while(n_td<chain->n_td)
	{

		if(usb_td_pool_n_used_td<=0)return 0;

		td		=	chain->td_chain[n_td];
		used_td	=	get_zone_ptr(&usb_td_pool_used_td_list,0);
		n		=	0;

		while(n<usb_td_pool_n_used_td)
		{
			if((*used_td)==td)
			{
				usb_td_ptr_t		*new_free_td;
				usb_td_pool_n_used_td--;

				while(n<usb_td_pool_n_used_td)
				{		
					(*used_td)=(*(used_td+1));
					used_td++;
					n++;
				}

				new_free_td		= get_zone_ptr(&usb_td_pool_free_td_list,usb_td_pool_n_free_td*sizeof(usb_td_ptr_t));
				(*new_free_td)	= td;
				usb_td_pool_n_free_td++;
				
				break;
			}
			used_td++;
			n++;
		}
		n_td++;
	}

	chain->n_td=0;

	return 0;

}

void	allocate_td_chain			(struct td_chain_t	*chain,unsigned int data_size,unsigned int packet_size)
{
	unsigned int n,num_tds;

	if(data_size<=packet_size)
		num_tds		=  2;
	else
		num_tds		= (data_size+packet_size-1)/packet_size;
	
	n=0;
	while(n<num_tds)
	{
		chain->td_chain[n]	= get_new_td();
		n++;
	}
	chain->n_td	= num_tds;
	
}

void	build_td_chain						(struct td_chain_t *chain,usb_td_ptr_t	td)
{
	chain->n_td=0;
	while(td!=PTR_NULL)
	{
		chain->td_chain[chain->n_td]=td;
		td=td->link_phy;
		chain->n_td++;
	}
}


struct usb_transfer_t *usb_new_transfer	(unsigned int data_size,unsigned int packet_size)
{
	unsigned int			n;
	struct usb_transfer_t  *new_tfer;

	new_tfer	=	get_zone_ptr		(&usb_transfer_array,0);
	n=0;
	while(n<n_usb_transfer)
	{
		if(new_tfer->done==1)
		{
			release_zone_ref				(&new_tfer->data);
			remove_used_td					(&new_tfer->chain);
			new_tfer->done					=0;
			allocate_td_chain				(&new_tfer->chain,data_size,packet_size);
			return new_tfer;
		}

		new_tfer++;
		n++;
	}
	
	if((n_usb_transfer+1)>=n_usb_transfer_alloc)
	{
		n_usb_transfer_alloc*=2;
		realloc_zone		(&usb_transfer_array,n_usb_transfer_alloc*sizeof(struct usb_transfer_t));
	}

	new_tfer	=	get_zone_ptr		(&usb_transfer_array,n_usb_transfer*sizeof(struct usb_transfer_t));
	n_usb_transfer++;

	allocate_td_chain	(&new_tfer->chain,data_size,packet_size);
	return new_tfer;
}


int prepare_intr_data_read_tfer	(struct usb_device *dev,struct usb_transfer_t	*tfer,unsigned int iface_idx,unsigned int endp_idx,mem_zone_ref *data_ref,mem_size size_data)
{
	struct usb_interface		*iface;
	struct usb_endpoint		*endp;
	usb_td_ptr_t			td;
	usb_td_ptr_t			first_td;
	usb_td_ptr_t			last_td;
	unsigned char			*data_ptr;
	size_t					transfered_bytes;
	unsigned int			endp_id;
	unsigned int			packet_size;
	unsigned int			n_td;
	
	iface					=	get_device_interface(dev,dev->cur_configuration,iface_idx);
	if(iface	== PTR_NULL)
	{
		kernel_log		(kernel_log_id,"read intr no such interface \n");
		return 0;
	}

	if(endp_idx>=iface->iface_desc.bNumEndpoints)
	{
		kernel_log		(kernel_log_id,"read intr no such endpoint ");
		writeint		(endp_idx,16);
		writestr		("/");
		writeint		(iface->iface_desc.bNumEndpoints,16);
		writestr		("\n");
		return 0;
	}

	endp			=	get_zone_ptr(&iface->endpoints_ref,endp_idx*sizeof(struct usb_endpoint));
	endp_id			=	(endp->ep_desc.bEndpointAddress&0x0F);
	packet_size		=	(endp->ep_desc.wMaxPacketSize);

	
	copy_zone_ref		(&tfer->data,data_ref);

	tfer->done			=	0;
	

	tfer->iface_idx		=	iface_idx;
	tfer->iface_id		=	iface->iface_desc.bInterfaceNumber;

	tfer->endp_idx		=	endp_idx;
	tfer->endp_id		=	endp_id;

	tfer->packet_size	=	packet_size;

	tfer->dev			=	dev;
	tfer->data_size		=	size_data;

	transfered_bytes	=	0;
	data_ptr			=	get_zone_ptr(data_ref,0);
	last_td				=	PTR_NULL;
	first_td			=	PTR_NULL;

	n_td				=	0;
	

	while(transfered_bytes<size_data)
	{
		size_t	len_packet_data;

		len_packet_data	=	size_data-transfered_bytes;
		
		if(len_packet_data>packet_size)
			len_packet_data=packet_size;

		td									=	tfer->chain.td_chain[n_td];
	
		if(first_td==PTR_NULL)
			first_td			=	td;

		if(last_td!=PTR_NULL)
			last_td->link_phy	=	td;

		td->token								=	0;
		td->token								=	write_bits(td->token  ,0						  ,18  ,1);	//buffer rounding
		td->token								=	write_bits(td->token  ,TD_TOKEN_IN				  ,19  ,2);	//direction
		td->token								=	write_bits(td->token  ,0						  ,21  ,3);	//delay int
		td->token								=	write_bits(td->token  ,endp->data_tog		   	  ,24  ,1);	//data toggle
		td->token								=	write_bits(td->token  ,1						  ,25  ,1);	//data toggle TD
		td->token								=	write_bits(td->token  ,0						  ,26  ,2); //n errors
		td->token								=	write_bits(td->token  ,0						  ,28  ,4);	// status of the last attempted transaction

		td->buffer_phy							=	mem_add(data_ptr,transfered_bytes);
		td->end_buffer_phy						=	mem_add(data_ptr,size_data-1);

		endp->data_tog							^=	1;
		last_td									=	td;
		transfered_bytes						+=  len_packet_data;
		n_td++;
	}

	tfer->head				=	first_td;

	{
		struct usb_td_t							*dummy_td;
		
		dummy_td								=	tfer->chain.td_chain[n_td];
		dummy_td->token							=	0;
		dummy_td->token							=	write_bits(td->token  ,0						  ,18  ,1);	//buffer rounding
		dummy_td->token							=	write_bits(td->token  ,TD_TOKEN_IN				  ,19  ,2);	//direction
		dummy_td->token							=	write_bits(td->token  ,7						  ,21  ,3);	//delay int
		dummy_td->token							=	write_bits(td->token  ,1						  ,24  ,1);	//data toggle
		dummy_td->token							=	write_bits(td->token  ,1						  ,25  ,1);	//data toggle TD
		dummy_td->token							=	write_bits(td->token  ,0						  ,26  ,2); //n errors
		dummy_td->token							=	write_bits(td->token  ,0						  ,28  ,4);	// status of the last attempted transaction

		dummy_td->buffer_phy					=	PTR_NULL;
		dummy_td->end_buffer_phy				=	PTR_NULL;
		
		tfer->chain.td_chain[tfer->chain.n_td]	=	dummy_td;
		tfer->chain.n_td++;

		last_td->link_phy						=	dummy_td;

		tfer->tail								=	dummy_td;
	}
	

	tfer->tail->link_phy	=	PTR_NULL;
	tfer->done				=	0;


	return 1;
}


void C_API_FUNC transfer_done	(usb_td_ptr_t	td_done)
{
	unsigned int			n,found;
	struct usb_transfer_t	*transfer;
	mem_zone_ref			data_ref;
	unsigned char			*report_data;
	struct usb_interface	*iface;
	usb_handle_interface_data_func_ptr usb_handle_interface_data;

	//kernel_log		(kernel_log_id,"transfer done \n");
	


	transfer	=	get_zone_ptr(&usb_transfer_array,0);
	n			=	0;
	found		=	0;

	while(n<n_usb_transfer)
	{
		if((transfer->done==0)&&(transfer->head==td_done))
		{
			found			=	1;
			break;
		}
		transfer++;
		n++;
	}

	if(!found)
		return;

	
	

	iface	=	get_device_interface	(transfer->dev,transfer->dev->cur_configuration,transfer->iface_idx);
	if(iface	== PTR_NULL)return;
	
	report_data		=	get_zone_ptr(&transfer->data,0);

	
	
	
	usb_handle_interface_data	=	get_tpo_mod_exp_addr_name	(&iface->driver_mod,"usb_handle_interface_data");

	


	if(usb_handle_interface_data!=PTR_NULL)
		usb_handle_interface_data		(iface,report_data);
	else
		kernel_log(kernel_log_id,"no usb_handle_interface_data \n");

	

	
	

	data_ref.zone				=	PTR_NULL;
	copy_zone_ref					(&data_ref,&transfer->data);

	
	
	
	prepare_intr_data_read_tfer		(transfer->dev,transfer,transfer->iface_idx,transfer->endp_idx,&data_ref,transfer->data_size);
	do_usb_intr_transfer_read		(transfer);
	release_zone_ref				(&data_ref);

	
	

}




struct usb_endpoint *get_endpoint	(struct usb_interface *iface,unsigned int type,unsigned int dir,unsigned int *idx)
{
	struct usb_endpoint			*endp;
	unsigned int			n;

	n						=	0;
	while(n<iface->iface_desc.bNumEndpoints)
	{
		endp		=	get_zone_ptr(&iface->endpoints_ref,n*sizeof(struct usb_endpoint));
		if(	((endp->ep_desc.bmAttributes&0x03)==type)&&
			(((endp->ep_desc.bEndpointAddress&0x80)>>7)==dir))
		{
			if(idx!=PTR_NULL)
				(*idx)=n;

			return endp;
		}
		n++;
	}
	kernel_log		(kernel_log_id,"no interupt endpoint on this interface \n");
	return PTR_NULL;
}
int device_read_interface_data	(struct usb_device *dev	,unsigned int iface_idx)
{
	unsigned int						endp_idx;		
	unsigned int						report_size;
	mem_zone_ref						data_ref;
	struct usb_transfer_t				*tfer;
	struct usb_endpoint					*endp;
	struct usb_interface				*iface;
	get_interface_data_size_func_ptr	get_data_size;

	kernel_log(kernel_log_id,"read usb device \n");

	iface				=	get_device_interface(dev,dev->cur_configuration,iface_idx);
	endp				=	get_endpoint		(iface,0x03,1,&endp_idx);
	if(endp==PTR_NULL)
	{
		kernel_log(kernel_log_id,"usb no endpoint \n");
		return 0;
	}
	
	get_data_size		=	get_tpo_mod_exp_addr_name	(&iface->driver_mod,"get_interface_data_size");
				
	if(get_data_size==PTR_NULL)
	{
		kernel_log(kernel_log_id,"usb no get data size \n");
		return 0;

	}
		
	if(get_data_size(iface,&report_size)==1)
	{
		kernel_log(kernel_log_id,"usb interface data size");
		writeint(report_size,10);
		writestr("\n");
	
		data_ref.zone		=	PTR_NULL;
		allocate_new_zone		(0x0,report_size,&data_ref);

		tfer				=	usb_new_transfer	(report_size,endp->ep_desc.wMaxPacketSize);
		prepare_intr_data_read_tfer					(dev,tfer,iface_idx,endp_idx,&data_ref,report_size);
		do_usb_intr_transfer_read					(tfer);
		release_zone_ref							(&data_ref);
	}
	else
	{
		kernel_log(kernel_log_id,"usb interface data size failed \n");
		return 0;
	}


	return 1;
}


struct usb_td_t *create_setup_td(usb_setup_packet_t *setup_packet)
{
	
	usb_td_ptr_t		td;


	td					=	get_new_td();


	td->token			=	0;
	td->token			=	write_bits(td->token  ,0						  ,18  ,1);	//buffer rounding
	td->token			=	write_bits(td->token  ,TD_TOKEN_SETUP			  ,19  ,2);	//direction
	td->token			=	write_bits(td->token  ,7						  ,21  ,3);	//delay int
	td->token			=	write_bits(td->token  ,0						  ,24  ,1);	//data toggle
	td->token			=	write_bits(td->token  ,1						  ,25  ,1);	//data toggle TD
	td->token			=	write_bits(td->token  ,0						  ,26  ,2); //n errors
	td->token			=	write_bits(td->token  ,0						  ,28  ,4);	// status of the last attempted transaction
	td->buffer_phy		=	setup_packet;
	td->end_buffer_phy	=	mem_add(setup_packet,sizeof(usb_setup_packet_t)-1);
	

	

	return td;

	
}


struct usb_td_t	 *create_transfer			(struct usb_device *dev,mem_ptr data_dest,mem_size size_data,unsigned int max_pack_size,unsigned int dir,unsigned int *data_tog,usb_td_ptr_t tail_td )
{
	usb_td_ptr_t		td;
	usb_td_ptr_t		first_td;
	usb_td_ptr_t		last_td;
	unsigned char		*data_ptr;
	size_t				transfered_bytes;
	

	transfered_bytes	=	0;
	data_ptr			=	data_dest;
	last_td				=	PTR_NULL;
	first_td			=	PTR_NULL;
	

	while(transfered_bytes<size_data)
	{
		size_t	len_packet_data;

		len_packet_data	=	size_data-transfered_bytes;
		
		if(len_packet_data>max_pack_size)
			len_packet_data=max_pack_size;

		td				=	get_new_td();

		if(first_td==PTR_NULL)
			first_td=td;

		if(last_td!=PTR_NULL)
			last_td->link_phy	=	td;

		td->token			=	0;
		td->token			=	write_bits(td->token  ,0						  ,18  ,1);	//buffer rounding
		td->token			=	write_bits(td->token  ,dir						  ,19  ,2);	//direction
		td->token			=	write_bits(td->token  ,0						  ,21  ,3);	//delay int
		td->token			=	write_bits(td->token  ,(*data_tog)			   	  ,24  ,1);	//data toggle
		td->token			=	write_bits(td->token  ,1						  ,25  ,1);	//data toggle TD
		td->token			=	write_bits(td->token  ,0						  ,26  ,2); //n errors
		td->token			=	write_bits(td->token  ,0						  ,28  ,4);	// status of the last attempted transaction

		td->buffer_phy		=	mem_add(data_ptr,transfered_bytes);
		td->end_buffer_phy	=	mem_add(data_ptr,size_data-1);

		(*data_tog)			^=	1;
		last_td				=	td;
		transfered_bytes	+=  len_packet_data;
	}

	last_td->link_phy	=	tail_td;

	
		
	return first_td;	
}


usb_td_ptr_t	 create_control_status	(unsigned int dir)
{
	usb_td_ptr_t		td;

	td					=	get_new_td();


	td->token			=	0;
	td->token			=	write_bits(td->token  ,0						  ,18  ,1);	//buffer rounding
	td->token			=	write_bits(td->token  ,dir						  ,19  ,2);	//direction
	td->token			=	write_bits(td->token  ,7						  ,21  ,3);	//delay int
	td->token			=	write_bits(td->token  ,1						  ,24  ,1);	//data toggle
	td->token			=	write_bits(td->token  ,1						  ,25  ,1);	//data toggle TD
	td->token			=	write_bits(td->token  ,0						  ,26  ,2); //n errors
	td->token			=	write_bits(td->token  ,0						  ,28  ,4);	// status of the last attempted transaction

	td->buffer_phy		=	PTR_NULL;
	td->end_buffer_phy	=	PTR_NULL;
	
	return td;
}


int do_usb_intr_transfer_read		(struct usb_transfer_t		*tfer)
{
	unsigned int ret;
	ret	=	controller_usb_intr_transfer(tfer->dev,tfer->endp_id,tfer->packet_size,tfer->head,tfer->tail);

	return ret;
}

int C_API_FUNC  do_usb_ctrl_transfer			(struct usb_device *dev,usb_setup_packet_t	*setup_packet,mem_ptr data,mem_size size,unsigned int packet_size,int *ret_code)
{
	usb_td_ptr_t			setup_td;
	usb_td_ptr_t			data_td;
	usb_td_ptr_t			status_td;
	usb_td_ptr_t			dummy_td;
	struct td_chain_t		chain;
	int						ret;

	//kernel_log		(kernel_log_id,"control transfer setup \n");

	dummy_td				=	create_control_status		(TD_TOKEN_IN);
	dummy_td->link_phy		=	PTR_NULL;

	setup_td				=	create_setup_td						(setup_packet);
	setup_td->link_phy		=	dummy_td;

	build_td_chain			(&chain,setup_td);

	ret						=	controller_usb_ctrl_transfer		(dev->addr,dev->speed,dev->device_desc.bMaxPacketSize,setup_td,dummy_td);
	*ret_code				=	((setup_td->token)>>28);

	remove_used_td			(&chain);

	if(!ret)
	{
		kernel_log		(kernel_log_id,"control transfer failed in setup stage, return code : ");
		writeint		(*ret_code,16);
		writestr		("\n");

		return 0;
	}	

	if(size>0)
	{
		//kernel_log		(kernel_log_id,"control transfer data \n");
		dummy_td				=	create_control_status		(TD_TOKEN_IN);
		dummy_td->link_phy		=	PTR_NULL;

		if(setup_packet->bmRequestType&128)
			data_td					=	create_transfer				(dev,data,size,packet_size,TD_TOKEN_IN ,&dev->toggle_control,dummy_td);
		else
			data_td					=	create_transfer				(dev,data,size,packet_size,TD_TOKEN_OUT,&dev->toggle_control,dummy_td);


		build_td_chain			(&chain,data_td);

		ret						=	controller_usb_ctrl_transfer		(dev->addr,dev->speed,dev->device_desc.bMaxPacketSize,data_td,dummy_td);
		*ret_code				=	((data_td->token)>>28);
		if(!ret)
		{
			kernel_log		(kernel_log_id,"control transfer failed in data stage, return code : ");
			writeint		(*ret_code,16);
			writestr		("\n");
			return 0;
		}	

		remove_used_td			(&chain);
	}

	//kernel_log		(kernel_log_id,"control transfer status \n");
	dummy_td				=	create_control_status		(TD_TOKEN_IN);
	dummy_td->link_phy		=	PTR_NULL;

	if(setup_packet->bmRequestType&128)
		status_td				=	create_control_status		(TD_TOKEN_OUT);
	else
		status_td				=	create_control_status		(TD_TOKEN_IN);

	status_td->link_phy		=	dummy_td;


	build_td_chain			(&chain,status_td);

	ret						=	controller_usb_ctrl_transfer		(dev->addr,dev->speed,dev->device_desc.bMaxPacketSize,status_td,dummy_td);
	*ret_code				=	((status_td->token)>>28);

	remove_used_td			(&chain);
	if(!ret)
	{
		kernel_log		(kernel_log_id,"control transfer failed in status stage, return code : ");
		writeint(*ret_code,16);
		writestr("\n");
		return 0;
	}	

	//kernel_log		(kernel_log_id,"control transfer end \n");
	
	return 1;
}

int get_device_descriptor	(struct usb_device *dev,int *ret_code)
{
	mem_zone_ref				packet_ref;
	usb_setup_packet_t			*setup_packet;
	unsigned int				max_frame_size;
	struct usb_device_desc		device_desc;
	unsigned int				ret;
	


	memset_c(&device_desc,0,sizeof(struct usb_device_desc));
	max_frame_size													=	64;
	
	packet_ref.zone													=	PTR_NULL;
	allocate_new_zone												(0,sizeof(usb_setup_packet_t),&packet_ref);
	setup_packet			=	get_zone_ptr						(&packet_ref,0);
	

	usb_create_get_descriptor_packet_device							(setup_packet);
	ret=do_usb_ctrl_transfer										(dev,setup_packet,&device_desc,sizeof(struct usb_device_desc),max_frame_size,ret_code);
	release_zone_ref												(&packet_ref);
	if(ret==0)
	{
		kernel_log		(kernel_log_id,"get usb device descriptor failed \n");
		return 0;
	}

	dev->device_desc=device_desc;

	
	kernel_log		(kernel_log_id,"dev desc sz:");
	writeint		(dev->device_desc.bLength,10);
	writestr		(" packet sz:");
	writeint		(dev->device_desc.bMaxPacketSize,10);
	writestr		(" configs:");
	writeint		(dev->device_desc.bNumConfigurations,10);
	writestr		(" class:");
	writeint		(dev->device_desc.bDeviceClass,10);
	writestr		(" error:");
	writeint		(ret,16);
	writestr		("\n");
	return 1;
}


tpo_mod_file		*tpo=PTR_NULL;

int get_config_descriptor	(struct usb_device *dev,unsigned int conf_idx,int *ret_code)
{
	mem_zone_ref			packet_ref;
	usb_setup_packet_t		*setup_packet;
	struct usb_configuration*config;
	unsigned char			*config_data;
	mem_zone_ref			config_data_ref;
	char					*data,*data_end;
	int						currif;
	unsigned int			max_frame_size;
	unsigned int			config_len;
	int						ret;

	
	max_frame_size			=	dev->device_desc.bMaxPacketSize;
	config					=	get_zone_ptr	(&dev->configs_ref,conf_idx*sizeof(struct usb_configuration));
	
	packet_ref.zone				= PTR_NULL;
	allocate_new_zone			(0,sizeof(usb_setup_packet_t),&packet_ref);
	setup_packet			=	get_zone_ptr	(&packet_ref,0);

	usb_create_get_descriptor_packet_conf		(setup_packet,conf_idx,4);
	ret=do_usb_ctrl_transfer					(dev,setup_packet,&config->conf_desc,4,max_frame_size,ret_code);
	if(ret==0)
	{
		release_zone_ref	(&packet_ref);
		kernel_log			(kernel_log_id,"get device configuration descriptor size failed \n");
		return 0;
	}


	config_len	=	config->conf_desc.wTotalLength;
	
	kernel_log		(kernel_log_id,"desc length ");
	writeint		(config_len,10);
	writestr		("\n");

	config_data_ref.zone	=	PTR_NULL;
	allocate_new_zone							(0,config_len,&config_data_ref);
	config_data				=	get_zone_ptr	(&config_data_ref,0);
	memset_c									(config_data,0xFF,config_len);


	usb_create_get_descriptor_packet_conf		(setup_packet,conf_idx,config_len);
	ret=do_usb_ctrl_transfer					(dev,setup_packet,config_data,config_len,max_frame_size,ret_code);
	release_zone_ref							(&packet_ref);
	if(ret==0)
	{
		kernel_log		(kernel_log_id,"get device configuration descriptor failed \n");
		return 0;
	}

	memcpy_c	(&config->conf_desc,config_data,sizeof(struct usb_configuration_desc));


	kernel_log	(kernel_log_id,"length:");
	writeint	(config->conf_desc.wTotalLength,10);
	writestr	(" id:");
	writeint	(config->conf_desc.bConfigurationValue,16);
	writestr	(" ifaces:");
	writeint	(config->conf_desc.bNumInterfaces,10);
	writestr	(" pow:");
	writeint	(config->conf_desc.bMaxPower,10);
	writestr	(" ret:");
	writeint	(*ret_code,16);
	writestr	("\n");


	data		=	get_zone_ptr(&config_data_ref,config->conf_desc.bLength);
	data_end	=	get_zone_ptr(&config_data_ref,config->conf_desc.wTotalLength);
	currif		=	0;

	config->interfaces_ref.zone=PTR_NULL;
	allocate_new_zone(0x00,sizeof (struct usb_interface)*config->conf_desc.bNumInterfaces,&config->interfaces_ref);



	while(data<data_end)
	{
		struct usb_endpoint			*desc_ep;
		//struct usb_hid_class_desc	*desc_hid_cls;
		struct usb_interface		*desc_if;
		int							curep;
		int							n_hid_cls;
		unsigned int				hid_cls_ofs;
		
		switch(((struct usb_desc_header	*)(data))->bDescriptorType)
		{

			case 4:
				desc_if						=	get_zone_ptr(&config->interfaces_ref,currif*sizeof (struct usb_interface));
				memcpy_c					(&desc_if->iface_desc,data,sizeof(struct usb_interface_desc));


				desc_if->drv_initialized	=	0;
				memset_c			(desc_if->driver_mod.name,0,64);

				tpo					=&desc_if->driver_mod;

				desc_if->driver_mod.data_sections.zone=PTR_NULL;

				curep						=	0;
				
				desc_if->endpoints_ref.zone	=	PTR_NULL;			
				allocate_new_zone	(0x00,sizeof (struct usb_endpoint)*desc_if->iface_desc.bNumEndpoints,&desc_if->endpoints_ref);

				

				kernel_log	(kernel_log_id,"iface [");
				writeint	(currif,10);
				writestr	("] alt:");
				writeint	(desc_if->iface_desc.bAlternateSetting,10);
				writestr	(" num ep:");
				writeint	(desc_if->iface_desc.bNumEndpoints,10);
				writestr	(" class:");
				writeint	(desc_if->iface_desc.bInterfaceClass,10);
				writestr	(" sub class:");
				writeint	(desc_if->iface_desc.bInterfaceSubClass,10);
				writestr	("\n");

				currif++;
			break;
			case 33:
				memcpy_c		(&desc_if->hid.hid_desc,data,sizeof(struct usb_hid_desc));

				/*
				desc_if->hid.classes_ref.zone=PTR_NULL;
				allocate_new_zone	(0x00,sizeof (struct usb_hid_class_desc)*desc_if->hid.hid_desc.bNumDescriptors,&desc_if->hid.classes_ref);
				desc_hid_cls		=	get_zone_ptr(&desc_if->hid.classes_ref,0);
				*/

				hid_cls_ofs			=	sizeof(struct usb_hid_desc);
				
				kernel_log	(kernel_log_id,"hid desc:");
				writeint	(desc_if->hid.hid_desc.bLength,10);
				writestr	(" type:");
				writeint	(desc_if->hid.hid_desc.bDescriptorType,10);
				writestr	(" country:");
				writeint	(desc_if->hid.hid_desc.bCountryCode,10);
				writestr	(" ver:");
				writeint	(desc_if->hid.hid_desc.bcdHID,10);
				writestr	(" n cls:");
				writeint	(desc_if->hid.hid_desc.bNumDescriptors,10);


				//memcpy_c	(desc_hid_cls,&data[hid_cls_ofs],sizeof (struct usb_hid_class_desc)*desc_if->hid.hid_desc.bNumDescriptors);
				writestr	(" [");
				n_hid_cls=0;
				while(n_hid_cls<desc_if->hid.hid_desc.bNumDescriptors)
				{
					desc_if->hid.classes[n_hid_cls].bDescriptorType		=	data[hid_cls_ofs];
					desc_if->hid.classes[n_hid_cls].wDescriptorLength	=	*((unsigned short *)(&data[hid_cls_ofs+1]));

					writeint(desc_if->hid.classes[n_hid_cls].bDescriptorType,10);
					writestr(",");
					writeint(desc_if->hid.classes[n_hid_cls].wDescriptorLength,10);
					writestr("]");
					hid_cls_ofs+=3;
					n_hid_cls++;
				}
				writestr("\n");

			break;

			case 5:

				desc_ep		=	get_zone_ptr(&desc_if->endpoints_ref,curep*sizeof (struct usb_endpoint));
				memcpy_c		(&desc_ep->ep_desc,data,sizeof(struct usb_endpoint_desc));

				desc_ep->data_tog	=	1;
				
			
				kernel_log	(kernel_log_id,"endpoint [");
				writeint	(curep,10);
				writestr	("] packet size:");
				writeint	(desc_ep->ep_desc.wMaxPacketSize,10);
				writestr	(" interval:");
				writeint	(desc_ep->ep_desc.bInterval,10);
				switch(desc_ep->ep_desc.bmAttributes&0x3)
				{
					case 0 :writestr	(" ctl");break;
					case 1 :writestr	(" iso");break;
					case 2 :writestr	(" blk");break;
					case 3 :writestr	(" itr");break;
				}

				if(desc_ep->ep_desc.bEndpointAddress&128)
					writestr	(" in");
				else
					writestr	(" out");

				writestr	(" addr:");
				writeint	(desc_ep->ep_desc.bEndpointAddress&0x0F,10);
				writestr	("\n");
				curep		++;
			break;
		}
		data+=((struct usb_desc_header	*)(data))->bLength;
	}
	release_zone_ref		(&config_data_ref);
	kernel_log				(kernel_log_id,"device descriptor done \n");


	kernel_log(kernel_log_id,"tpo driver name '");
	if(tpo!=PTR_NULL)
		writestr		(tpo->name);
	writestr		("' ");
	writeptr		(tpo);
	writestr		("\n");
	



	return 1;
}



int get_device_string	(struct usb_device *dev,unsigned int str_id,unsigned int lang_id,char	*str_c,int *ret_code)
{
	mem_zone_ref		packet_ref;
	usb_setup_packet_t	*setup_packet;
	mem_zone_ref		str_ref;
	short				*str;
	unsigned int		max_frame_size;
	unsigned int		n,str_size;
	int					ret;
	


	max_frame_size			=	dev->device_desc.bMaxPacketSize;

	packet_ref.zone			= PTR_NULL;
	allocate_new_zone			(0,sizeof(usb_setup_packet_t),&packet_ref);
	setup_packet			=	get_zone_ptr	(&packet_ref,0);


	str_ref.zone			=PTR_NULL;
	allocate_new_zone			(0,sizeof(struct usb_string_desc),&str_ref);
	str						=	get_zone_ptr(&str_ref,0);

	usb_create_get_descriptor_packet_string			(setup_packet,str_id,lang_id,0);
	ret=do_usb_ctrl_transfer						(dev,setup_packet,str,2,max_frame_size,ret_code);
	
	if(ret==0)
	{
		kernel_log				(kernel_log_id,"get usb device string size failed \n");
		release_zone_ref		(&packet_ref);
		return 0;
	}

	str_size				=	str[0]&0xFF;

	release_zone_ref			(&str_ref);
	allocate_new_zone			(0,str_size,&str_ref);
	str						=	get_zone_ptr(&str_ref,0);


	usb_create_get_descriptor_packet_string							(setup_packet,str_id,lang_id,str_size);
	ret=do_usb_ctrl_transfer										(dev,setup_packet,str,str_size,max_frame_size,ret_code);
	release_zone_ref												(&packet_ref);
	if(ret==0)
	{
		release_zone_ref	(&str_ref);
		kernel_log			(kernel_log_id,"get usb device string failed \n");
		return 0;
	}

	n=0;
	while((n*2+2)<str_size)
	{
		str_c[n]=(char)str[n+1];
		n++;
	}

	kernel_log	(kernel_log_id,"desc len:");
	writeint	((str[0]&0xFF),10);
	writestr	(" type:");
	writeint	((str[0]>>8)&0xFF,10);
	writestr	(" string:'");
	writestr	(str_c);
	writestr	("' error:");
	writeint	(*ret_code,16);
	writestr	("\n");

	release_zone_ref	(&str_ref);
	return 1;
}




int reset_device_configuration	(struct usb_device *dev, int *ret_code)
{

	mem_zone_ref		packet_ref;
	usb_setup_packet_t	*setup_packet;
	unsigned int		max_frame_size;
	int					ret;
	

	max_frame_size			=	dev->device_desc.bMaxPacketSize;

	packet_ref.zone			=	PTR_NULL;
	allocate_new_zone			(0,sizeof(usb_setup_packet_t),&packet_ref);
	setup_packet			=	get_zone_ptr	(&packet_ref,0);

	usb_create_set_configuration_packet_device						(setup_packet,0);
	ret						= do_usb_ctrl_transfer					(dev,setup_packet,PTR_NULL,0,max_frame_size,ret_code);
	release_zone_ref												(&packet_ref);
	

	dev->cur_configuration	= 0xFFFFFFFF;

	kernel_log	(kernel_log_id,"reset usb config error:");
	writeint	(*ret_code,16);
	writestr	("\n");

	return 1;
}

int get_device_configuration (struct usb_device *dev,unsigned char *configuration_id,int *ret_code)
{
	mem_zone_ref		packet_ref;
	usb_setup_packet_t	*setup_packet;
	unsigned int		max_frame_size;
	int					ret;


	packet_ref.zone			=	PTR_NULL;
	allocate_new_zone			(0,sizeof(usb_setup_packet_t),&packet_ref);
	setup_packet			=	get_zone_ptr	(&packet_ref,0);

	max_frame_size			=	dev->device_desc.bMaxPacketSize;

	usb_create_get_configuration_packet_device						(setup_packet);
	ret						= do_usb_ctrl_transfer					(dev,setup_packet,configuration_id,1,max_frame_size,ret_code);
	release_zone_ref	(&packet_ref);
	if(ret == 0)
	{
		kernel_log	(kernel_log_id,"could not get device descriptors \n");
		return 0;
	}
	
	kernel_log	(kernel_log_id,"get config ");
	writeint	((*configuration_id),10);
	writestr	(" error:");
	writeint	(*ret_code,16);
	writestr	("\n");

	return ret;
}


int set_device_address	(struct usb_device *dev,int *ret_code)
{

	mem_zone_ref		packet_ref;
	usb_setup_packet_t	*setup_packet;
	unsigned int		max_frame_size;
	int					ret,i;
	unsigned int		target_addr;

	target_addr			=	0xFFFFFFFF;

	/* Assign a new address to the device.  */
	for (i = 1; i < 128; i++)
	{
	  if (addr_spots[i]==0xFF)
	  {
		  target_addr=i;
		  break;
	  }
	}

	if (target_addr==0xFFFFFFFF)
	{
		kernel_log	(kernel_log_id,"can't assign address to USB device \n");
		return 0;
	}

	

	max_frame_size			=	dev->device_desc.bMaxPacketSize;

	packet_ref.zone			= PTR_NULL;
	allocate_new_zone			(0,sizeof(usb_setup_packet_t),&packet_ref);
	setup_packet			=	get_zone_ptr	(&packet_ref,0);

	usb_create_set_address_packet_device							(setup_packet,target_addr);
	ret						= do_usb_ctrl_transfer					(dev,setup_packet,PTR_NULL,0,max_frame_size,ret_code);
	release_zone_ref												(&packet_ref);
	
	if(ret==0){
		kernel_log	(kernel_log_id,"couldn't set device addr ");
		writeint	(*ret_code,16);
		writestr	("\n");
		return 0;
	}

	kernel_log	(kernel_log_id,"set device addr ");
	writeint	(target_addr,10);
	writestr	(" error:");
	writeint	(*ret_code,16);
	writestr	("\n");
	
	
	dev->addr				= target_addr;	
	addr_spots[target_addr]	= 0;
	snooze					(100000);
	


	return 1;


}

int set_device_cnfiguration	(struct usb_device *dev, unsigned int config_idx,int *ret_code)
{

	mem_zone_ref		packet_ref;
	usb_setup_packet_t	*setup_packet;
	struct usb_configuration	*conf;
	unsigned int		max_frame_size;
	int					ret;
	


	max_frame_size			=	dev->device_desc.bMaxPacketSize;

	packet_ref.zone			= PTR_NULL;
	allocate_new_zone			(0,sizeof(usb_setup_packet_t),&packet_ref);
	setup_packet			=	get_zone_ptr	(&packet_ref,0);

	if(config_idx>=dev->device_desc.bNumConfigurations)
	{
		kernel_log	(kernel_log_id,"set conf wrong configuration idx \n");
		return 0;
	}

	conf					=	get_zone_ptr						(&dev->configs_ref,config_idx*sizeof(struct usb_configuration));

	usb_create_set_configuration_packet_device						(setup_packet,conf->conf_desc.bConfigurationValue);
	ret						= do_usb_ctrl_transfer					(dev,setup_packet,PTR_NULL,0,max_frame_size,ret_code);
	release_zone_ref												(&packet_ref);

	kernel_log	(kernel_log_id,"set usb config [");
	writeint	(config_idx,10);
	writestr	("]=");
	writeint	(conf->conf_desc.bConfigurationValue,10);

	if(conf->conf_desc.iConfiguration!=0)
	{
		int		str_ret;
		char    name[256];

		get_device_string	(dev,conf->conf_desc.iConfiguration,0,name,&str_ret);
		writestr			(" name:'");
		writestr			(name);
		writestr			("'");
	}
	writestr(" error:");
	writeint(*ret_code,16);
	writestr("\n");

	dev->cur_configuration	= config_idx;



	return 1;
}


struct usb_interface *get_device_interface (struct usb_device *dev,unsigned int conf_idx,unsigned int iface_idx)
{
	struct usb_configuration		*config;
	struct usb_interface			*desc_if;

	if(conf_idx>=dev->device_desc.bNumConfigurations)return PTR_NULL;

	config					=	get_zone_ptr	(&dev->configs_ref,conf_idx*sizeof(struct usb_configuration));

	if(iface_idx>=config->conf_desc.bNumInterfaces)return PTR_NULL;

	desc_if					=	get_zone_ptr	(&config->interfaces_ref,iface_idx*sizeof (struct usb_interface));

	return desc_if;

}






void enable_hid_polling		  (struct usb_device	*dev)
{
	unsigned int		if_idx;
	
	struct usb_interface		*iface;

	if_idx=0;
		
	while((iface=get_device_interface(dev,dev->cur_configuration,if_idx))!=PTR_NULL)
	{
		device_read_interface_data(dev,if_idx);
		if_idx++;
	}
}



struct usb_device	 *usb_hub_add_dev (usb_speed_t speed,unsigned int port)
{
	struct usb_device	 *dev;
	int			 ret_code;

	
	kernel_log(kernel_log_id, "usb_hub_add_dev 1 \n");
	dev					=	get_new_device();;
	dev->speed			=   speed;
	dev->addr			=	0;
	dev->port_id		=	port;
	dev->state			=	usb_device_reset;

	kernel_log(kernel_log_id, "usb_hub_add_dev 2 \n");

	if(get_device_descriptor (dev,&ret_code)==0)
	{
 		  kernel_log	(kernel_log_id,"error getting device descriptor \n");
		 return 0;
	}
	if(dev->device_desc.iProduct!=0)
	{
		if(get_device_string	(dev,dev->device_desc.iProduct,0,dev->name,&ret_code)!=0)
		{
			kernel_log	(kernel_log_id,"device name : ");
			writestr	(dev->name);
			writestr	("\n");
		}
	}

	kernel_log(kernel_log_id, "usb_hub_add_dev 3 \n");

    if(set_device_address(dev,&ret_code)==0)
    {
	    kernel_log	(kernel_log_id,"unable to set device address \n");
	    return PTR_NULL;
    }
	kernel_log(kernel_log_id, "usb_hub_add_dev 4 \n");
	dev->state			=	usb_device_addressed;
   n_usb_devices++;

  return dev;
}


int usb_hub_del_dev (struct usb_device	 *dev,unsigned int port)
{
	struct usb_device	 *dev_list,*dev_list_end;
	int			 i;

	kernel_log	(kernel_log_id,"deleting usb device \n");

	dev_list			=	get_zone_ptr(&usb_devices_array,0);
	dev_list_end		=	get_zone_ptr(&usb_devices_array,n_usb_devices*sizeof(struct usb_device));

	while(dev_list<dev_list_end)
	{
		if(dev_list==dev)
		{
			unsigned int		n;
			struct usb_transfer_t		*tfer;
			for(i=0;i<dev_list->device_desc.bNumConfigurations;i++)
			{
				int							n_iface,n_endp,n_reps;
				struct usb_configuration	*config;

				

				config	=	get_zone_ptr	(&dev_list->configs_ref,sizeof(struct usb_configuration)*i);
				n_iface	=	0;
				for(n_iface=0;n_iface<config->conf_desc.bNumInterfaces;n_iface++)
				{
					struct usb_interface	*iface;

					iface	=	get_zone_ptr(&config->interfaces_ref,n_iface*sizeof(struct usb_interface));
					n_endp	=	0;

					
					for(n_endp=0;n_endp<config->conf_desc.bNumInterfaces;n_endp++)
					{
						struct usb_endpoint	*endp;
						endp			=	get_zone_ptr(&iface->endpoints_ref,n_endp*sizeof(struct usb_endpoint));
					}
					release_zone_ref(&iface->endpoints_ref);
					for(n_reps=0;n_reps<8;n_reps++)
					{
						release_zone_ref			(&iface->hid.report[n_reps].open_collection_node);
						release_zone_ref			(&iface->hid.report[n_reps].root_node);
					}
				}
				release_zone_ref	(&config->interfaces_ref);
			}

			release_zone_ref		(&dev_list->configs_ref);
			release_zone_ref		(&dev_list->usb_node);
			
			dev_list->used	=	0;
			tfer			=	get_zone_ptr		(&usb_transfer_array,0);

			n=0;
			while(n<n_usb_transfer)
			{
				if(tfer->dev==dev_list)
					tfer->done=1;

				tfer++;
				n++;
			}

			addr_spots[dev_list->addr]=0xFF;
			dev_list->used			   =0;
			dev_list->state			   =usb_device_not_connected;
			return 1;
		}
		dev_list++;
	}
	return 0;
}


int usb_add_hub (bus_driver	*bus_drv,struct usb_device		*dev,unsigned int port)
{
	struct usb_hubdesc	hubdesc;
	mem_zone_ref		packet_ref;
	usb_setup_packet_t	*setup_packet;
	unsigned int		max_frame_size;
	int					i,ret,ret_code;

	max_frame_size			=	dev->device_desc.bMaxPacketSize;

	packet_ref.zone			= PTR_NULL;
	allocate_new_zone		(0,sizeof(usb_setup_packet_t),&packet_ref);
	setup_packet			=	get_zone_ptr		(&packet_ref,0);

	usb_create_get_descriptor_packet_hub			(setup_packet);
	ret		= do_usb_ctrl_transfer					(dev,setup_packet,&hubdesc,sizeof(struct usb_hubdesc),max_frame_size,&ret_code);

  /*
  grub_usb_control_msg (dev, (GRUB_USB_REQTYPE_IN
			      | GRUB_USB_REQTYPE_CLASS
			      | GRUB_USB_REQTYPE_TARGET_DEV),
			GRUB_USB_REQ_GET_DESCRIPTOR,
			(GRUB_USB_DESCRIPTOR_HUB << 8) | 0,
			0, sizeof (hubdesc), (char *) &hubdesc);
	*/
  

  // Iterate over the Hub ports.  
  for (i = 1; i <= hubdesc.portcnt; i++)
    {
      unsigned int status;

      // Get the port status.  
	  /*
      err = grub_usb_control_msg (dev, (GRUB_USB_REQTYPE_IN
					| GRUB_USB_REQTYPE_CLASS
					| GRUB_USB_REQTYPE_TARGET_OTHER),
				  GRUB_USB_REQ_HUB_GET_PORT_STATUS,
				  0, i, sizeof (status), (char *) &status);
	  */

	  usb_create_get_port_status_hub						(setup_packet,i);
	  ret		=	 do_usb_ctrl_transfer					(dev,setup_packet,&status,4,max_frame_size,&ret_code);

      // Just ignore the device if the Hub does not report the status.  
      if (!ret)
		continue;

		// If connected, reset and enable the port.  
		if (status & USB_HUB_STATUS_CONNECTED)
		{
			usb_speed_t speed;

			// Determine the device speed.  
			if (status & USB_HUB_STATUS_LOWSPEED)
				speed = USB_SPEED_LOW;
			else
			{
				if (status & USB_HUB_STATUS_HIGHSPEED)
					speed = USB_SPEED_HIGH;
				else
					speed = USB_SPEED_FULL;
			}
		    
			// A device is actually connected to this port, now enable
			// the port.  

			usb_create_enable_port_hub					(setup_packet,i);
			ret		=	 do_usb_ctrl_transfer			(dev,setup_packet,PTR_NULL,0,max_frame_size,&ret_code);

			
			/*
			err = grub_usb_control_msg (dev, (GRUB_USB_REQTYPE_OUT
							| GRUB_USB_REQTYPE_CLASS
							| GRUB_USB_REQTYPE_TARGET_OTHER),
						  0x3, 0x4, i, 0, 0);
			*/

			// If the Hub does not cooperate for this port, just skip the port.  
			if (!ret)
				continue;

			// Add the device and assign a device address to it.  
			usb_hub_add_dev (speed,port);
		}
    }
    return 1;
}
void add_hub_to_tree(bus_driver *bus_drv,struct usb_device *dev)
{

	tree_manager_add_child_node		(&bus_drv->bus_root_node,dev->name,NODE_USB_HUB,&dev->usb_node);
}
void add_device_configuration(struct usb_device *dev,unsigned int conf_idx)
{
	char						string[256];
	char						ord[32];
	int							ret_code;
	mem_zone_ref				conf_node;
	mem_zone_ref				prop_node,iface_node,endp_node;
	struct usb_configuration	*config;
	unsigned int				n,nn;
	
	prop_node.zone				=PTR_NULL;
	iface_node.zone				=PTR_NULL;
	endp_node.zone				=PTR_NULL;
		
	conf_node.zone=PTR_NULL;

	
	config=get_zone_ptr(&dev->configs_ref,conf_idx*sizeof(struct usb_configuration));

	memset_c(string,0,256);
	if(config->conf_desc.iConfiguration>0)
	{
		if(get_device_string	(dev,config->conf_desc.iConfiguration,0,string,&ret_code)==0)
			memset_c(string,0,256);
	}
		
	if(strlen_c(string)<=0)
	{
		itoa_s	(conf_idx, ord,32, 10);

		strcpy_s(string,256,"configuration [");
		strcat_s(string,256,ord);
		strcat_s(string,256,"]");
	}
;
	tree_manager_add_child_node		(&dev->usb_node,string,NODE_USB_CONFIGURATION,&conf_node);

	tree_manager_add_child_node		(&conf_node,"ID",NODE_GFX_INT,&prop_node);
	tree_manager_write_node_dword	(&prop_node,0,config->conf_desc.bConfigurationValue);

	tree_manager_add_child_node		(&conf_node,"attributes",NODE_GFX_INT,&prop_node);
	tree_manager_write_node_dword	(&prop_node,0,config->conf_desc.bmAttributes);

	tree_manager_add_child_node		(&conf_node,"max power",NODE_GFX_INT,&prop_node);
	tree_manager_write_node_dword	(&prop_node,0,config->conf_desc.bMaxPower);

	release_zone_ref				(&prop_node);

	for(n=0;n<config->conf_desc.bNumInterfaces;n++)
	{
		struct usb_interface	*iface;
		
		iface	=	get_device_interface(dev,conf_idx,n);

		memset_c(string,0,256);
		if(iface->iface_desc.iInterface>0)
		{
			if(get_device_string	(dev,iface->iface_desc.iInterface,0,string,&ret_code)==0)
				memset_c(string,0,256);
		}

		
		if(strlen_c(string)<=0)
		{
			

			itoa_s	(n, ord,32, 10);

			strcpy_s(string,256,"interface [");
			strcat_s(string,256,ord);
			strcat_s(string,256,"]");
		}

		

		tree_manager_add_child_node		(&conf_node,string,NODE_USB_INTERFACE,&iface_node);

		tree_manager_add_child_node		(&iface_node,"ID",NODE_GFX_INT,&prop_node);
		tree_manager_write_node_dword	(&prop_node,0,iface->iface_desc.bInterfaceNumber);

		tree_manager_add_child_node		(&iface_node,"class",NODE_GFX_INT,&prop_node);
		tree_manager_write_node_dword	(&prop_node,0,iface->iface_desc.bInterfaceClass);
		
		tree_manager_add_child_node		(&iface_node,"sub class",NODE_GFX_INT,&prop_node);
		tree_manager_write_node_dword	(&prop_node,0,iface->iface_desc.bInterfaceSubClass);

		tree_manager_add_child_node		(&iface_node,"protocol",NODE_GFX_INT,&prop_node);
		tree_manager_write_node_dword	(&prop_node,0,iface->iface_desc.bInterfaceProtocol);

		release_zone_ref				(&prop_node);

		

		if(iface->hid.hid_desc.bLength>0)
		{
			mem_zone_ref		hid_node={PTR_NULL};


			tree_manager_add_child_node		(&iface_node,"HID INFOS",NODE_HID_INFOS,&hid_node);

			


			tree_manager_add_child_node		(&hid_node,"hid ver",NODE_GFX_INT,&prop_node);
			tree_manager_write_node_dword	(&prop_node,0,iface->hid.hid_desc.bcdHID);

			

			tree_manager_add_child_node		(&hid_node,"country code",NODE_GFX_INT,&prop_node);
			tree_manager_write_node_dword	(&prop_node,0,iface->hid.hid_desc.bCountryCode);
			
			if(iface->hid.report[0].root_node.zone!=PTR_NULL)
				tree_manager_node_add_child		(&hid_node,&iface->hid.report[0].root_node);

			release_zone_ref				(&prop_node);
			release_zone_ref				(&hid_node);

		}
		
		for(nn=0;nn<iface->iface_desc.bNumEndpoints;nn++)
		{
			struct usb_endpoint	*endp;

			endp	=	get_zone_ptr(&iface->endpoints_ref,nn*sizeof(struct usb_endpoint));
			
			itoa_s	(nn, ord,32, 10);
			strcpy_s(string,256,"endpoint [");
			strcat_s(string,256,ord);
			strcat_s(string,256,"]");

			tree_manager_add_child_node		(&iface_node,string,NODE_USB_ENDPOINT,&endp_node);

			tree_manager_add_child_node		(&endp_node,"ID",NODE_GFX_INT,&prop_node);
			tree_manager_write_node_dword	(&prop_node,0,endp->ep_desc.bEndpointAddress&0x0F);

			tree_manager_add_child_node		(&endp_node,"direction",NODE_GFX_INT,&prop_node);
			tree_manager_write_node_dword	(&prop_node,0,((endp->ep_desc.bEndpointAddress&0xFF)>>7));

			tree_manager_add_child_node		(&endp_node,"interval",NODE_GFX_INT,&prop_node);
			tree_manager_write_node_dword	(&prop_node,0,endp->ep_desc.bInterval);
			
			tree_manager_add_child_node		(&endp_node,"attributes",NODE_GFX_INT,&prop_node);
			tree_manager_write_node_dword	(&prop_node,0,endp->ep_desc.bmAttributes);

			tree_manager_add_child_node		(&endp_node,"packet size",NODE_GFX_INT,&prop_node);
			tree_manager_write_node_dword	(&prop_node,0,endp->ep_desc.wMaxPacketSize);

			release_zone_ref				(&endp_node);
			release_zone_ref				(&prop_node);
		}
		release_zone_ref				(&iface_node);
	}
	release_zone_ref				(&conf_node);

}

void add_device_to_tree(bus_driver *bus_drv,struct usb_device *dev)
{
	char						string[256];
	int							ret_code;
	mem_zone_ref				prop_node;

	prop_node.zone				=PTR_NULL;
	

	tree_manager_add_child_node		(&bus_drv->bus_root_node,dev->name,NODE_BUS_DEVICE,&dev->usb_node);

	tree_manager_add_child_node		(&dev->usb_node,"address",NODE_GFX_INT,&prop_node);
	tree_manager_write_node_dword	(&prop_node,0,dev->addr);

	if(dev->device_desc.iManufacturer>0)
	{
		memset_c(string,0,256);
		if(get_device_string	(dev,dev->device_desc.iManufacturer,0,string,&ret_code)!=0)
		{
			tree_manager_add_child_node		(&dev->usb_node,"manufacturer",NODE_GFX_STR,&prop_node);
			tree_manager_write_node_data	(&prop_node,string,0,strlen_c(string)+1);
		}
	}
	if(dev->device_desc.iSerialNumber>0)
	{
		memset_c(string,0,256);
		if(get_device_string	(dev,dev->device_desc.iSerialNumber,0,string,&ret_code)!=0)
		{
			tree_manager_add_child_node		(&dev->usb_node,"serial number",NODE_GFX_STR,&prop_node);
			tree_manager_write_node_data	(&prop_node,string,0,strlen_c(string)+1);
		}
	}
	tree_manager_add_child_node		(&dev->usb_node,"id",NODE_GFX_INT,&prop_node);
	tree_manager_write_node_dword	(&prop_node,0,dev->dev_id);


	tree_manager_add_child_node		(&dev->usb_node,"name",NODE_GFX_INT,&prop_node);
	tree_manager_write_node_str		(&prop_node,0,dev->name);

	tree_manager_add_child_node		(&dev->usb_node,"id vendor",NODE_GFX_INT,&prop_node);
	tree_manager_write_node_dword	(&prop_node,0,dev->device_desc.idVendor);

	tree_manager_add_child_node		(&dev->usb_node,"id product",NODE_GFX_INT,&prop_node);
	tree_manager_write_node_dword	(&prop_node,0,dev->device_desc.idProduct);


	tree_manager_add_child_node		(&dev->usb_node,"class",NODE_GFX_INT,&prop_node);
	tree_manager_write_node_dword	(&prop_node,0,dev->device_desc.bDeviceClass);

	tree_manager_add_child_node		(&dev->usb_node,"sub class",NODE_GFX_INT,&prop_node);
	tree_manager_write_node_dword	(&prop_node,0,dev->device_desc.bDeviceSubClass);
	
	tree_manager_add_child_node		(&dev->usb_node,"protocol",NODE_GFX_INT,&prop_node);
	tree_manager_write_node_dword	(&prop_node,0,dev->device_desc.bDeviceProtocol);

	tree_manager_add_child_node		(&dev->usb_node,"usb ver",NODE_GFX_INT,&prop_node);
	tree_manager_write_node_dword	(&prop_node,0,dev->device_desc.bcdUSB);

	tree_manager_add_child_node		(&dev->usb_node,"dev ver",NODE_GFX_INT,&prop_node);
	tree_manager_write_node_dword	(&prop_node,0,dev->device_desc.bcdDevice);
	
	tree_manager_add_child_node		(&dev->usb_node,"packet size",NODE_GFX_INT,&prop_node);
	tree_manager_write_node_dword	(&prop_node,0,dev->device_desc.bMaxPacketSize);

	release_zone_ref				(&prop_node);

	

	
}


void	C_API_FUNC root_hub_change(unsigned int port_id,unsigned int port_status)
{
	struct usb_device		*dev;
	usb_speed_t				speed;
	
	kernel_log(kernel_log_id, "root_hub_change \n");

	speed=controller_usb_detect_dev(port_id);

	if(speed!=USB_SPEED_NONE)
	{
		kernel_log		(kernel_log_id,"new device connected on port ");
		writeint		(port_id,16);
		writestr		("\n");

		controller_usb_portstatus			(port_id, 1);
		dev			= usb_hub_add_dev		(speed,port_id);
		
		dev->bus_id	= global_bus_drv->bus_id;
		dev->dev_id = dev->addr;

		add_device_to_tree					(global_bus_drv,dev);

		//bus_manager_parse_bus				(global_bus_drv->bus_id,"isodisk");

		
	}
	else
	{
		kernel_log		(kernel_log_id,"device disconnected on port ");
		writeint		(port_id,16);
		writestr		("\n");

		dev	=	find_usb_device_by_port(port_id);
		if(dev!=PTR_NULL)
		{
			tree_remove_child_by_member_value_dword	(&global_bus_drv->bus_root_node,NODE_BUS_DEVICE,"id",dev->dev_id);
			usb_hub_del_dev							(dev,port_id);
			controller_usb_portstatus				(port_id, 0);
		}
	}
}

OS_API_C_FUNC(unsigned int) bus_scan_devices(bus_driver	*bus_drv)
{
	int							err;
	int							ports;
	int							i;
	const tpo_mod_file			*usb_controller_drv;
	struct usb_device			*dev;

	kernel_log	(kernel_log_id,"scanning usb bus \n");

	if(bus_manager_get_device_driver		(bus_drv->controller_bus_id,bus_drv->controller_dev_id,&usb_controller_drv)==0xFFFFFFFF)
	{
		kernel_log	(kernel_log_id,"could not find usb controller driver \n");
		return 0;
	}

	kernel_log	(kernel_log_id,"controller driver:'");
	writestr	(usb_controller_drv->name);
	writestr	("' bus id:");
	writeint	(bus_drv->controller_bus_id,16);
	writestr	(" dev id:");
	writeint	(bus_drv->controller_dev_id,16);
	writestr	("\n");


	controller_usb_hubports			=get_tpo_mod_exp_addr_name			(usb_controller_drv,"usb_hubports");
	controller_usb_detect_dev		=get_tpo_mod_exp_addr_name			(usb_controller_drv,"usb_detect_dev");
	controller_usb_portstatus		=get_tpo_mod_exp_addr_name			(usb_controller_drv,"usb_portstatus");
	controller_usb_ctrl_transfer	=get_tpo_mod_exp_addr_name			(usb_controller_drv,"usb_ctrl_transfer");
	controller_usb_intr_transfer	=get_tpo_mod_exp_addr_name			(usb_controller_drv,"usb_intr_transfer");

	
	set_tpo_mod_exp_value32_name			(usb_controller_drv,"transfer_done"	 ,mem_to_uint(transfer_done));


	/* Query the number of ports the root Hub has.  */
	ports = controller_usb_hubports ();

	
	bus_drv->bus_root_node.zone	=PTR_NULL;

	tree_manager_create_node			("USB ROOT HUB",NODE_BUS_DEV_ROOT,&bus_drv->bus_root_node);
	tree_manager_write_node_dword		(&bus_drv->bus_root_node,0,bus_drv->bus_id);

	set_tpo_mod_exp_value32_name(usb_controller_drv,"root_hub_change",mem_to_uint(PTR_INVALID));

	kernel_log	(kernel_log_id,"scanning root hub ports \n");

	for (i = 0; i < ports; i++)
    {
      usb_speed_t speed = controller_usb_detect_dev (i);

      if (speed != USB_SPEED_NONE)
	  {
			// Enable the port.
			err = controller_usb_portstatus  (i, 1);

			// Enable the port and create a device.
			dev = usb_hub_add_dev (speed,i);
			if (! dev)
				continue;

			dev->bus_id	= bus_drv->bus_id;
			dev->dev_id = dev->addr;


			// If the device is a Hub, scan it for more devices.
			if (dev->device_desc.bDeviceClass == 0x09)
			{
				usb_add_hub						(bus_drv,dev,i);
				add_hub_to_tree					(bus_drv,dev);
			}
			else
			{
				add_device_to_tree				(bus_drv,dev);
			}
		}
    }

	set_tpo_mod_exp_value32_name(usb_controller_drv,"root_hub_change",mem_to_uint(root_hub_change));
	//tree_manager_dump_node_rec(&bus_drv->bus_root_node,0);
	return 1;
}



OS_API_C_FUNC(unsigned int) init_new_bus_device(bus_driver *bus_drv,mem_zone_ref *dev_node)
{
	
	unsigned int				if_idx,product_code,vendor_code;
	int							n_conf;
	mem_zone_ref				prop_node,conf_node,iface_node;
	struct usb_device			*dev;
	unsigned int				class_code,subclass_code,protocol_code,dev_id;
	unsigned char				conf_id;
	int							ret_code;
	unsigned int				num_configs;
	unsigned int				drv_loaded;
	unsigned int i;

	prop_node.zone	=PTR_NULL;
	iface_node.zone	=PTR_NULL;
	conf_node.zone	=PTR_NULL;
	
	if(tree_node_find_child_by_name	(dev_node,"id",&prop_node)!=1)
	{
		kernel_log	(kernel_log_id,"device no id node\n");
		return 0xFFFFFFFF;
	}

	tree_mamanger_get_node_dword	(&prop_node,0,&dev_id);

	dev		=	find_usb_device_by_id		(dev_id);

	if(dev==PTR_NULL)return 0xFFFFFFFF;
	if(dev->state<usb_device_addressed)return 0xFFFFFFFF;

	if(dev->state<usb_device_configureed)
	{
		num_configs		   =	dev->device_desc.bNumConfigurations;
		allocate_new_zone	(0x00,sizeof (struct usb_configuration)*num_configs,&dev->configs_ref);
		for (i = 0; i < num_configs; i++)
		{
			if(get_config_descriptor	(dev,i,&ret_code)==1)
			{
				add_device_configuration(dev,i);
			}
			else
			{
				kernel_log	(kernel_log_id,"could get a device configuration descriptor \n");
			}
		}


		tree_node_find_child_by_name	(dev_node,"id vendor",&prop_node);
		tree_mamanger_get_node_dword	(&prop_node,0,&vendor_code);

		tree_node_find_child_by_name	(dev_node,"id product",&prop_node);
		tree_mamanger_get_node_dword	(&prop_node,0,&product_code);

		set_device_cnfiguration		(dev,0,&ret_code);
		snooze						(10000);

		dev->state			=	usb_device_configureed;
	}

	get_device_configuration	(dev,&conf_id,&ret_code);


	
	if_idx		=	0;
	n_conf		=	0;
	drv_loaded	=	0;

	while(tree_node_find_child_by_type	(dev_node,NODE_USB_CONFIGURATION,&conf_node,n_conf)!=0)
	{
		unsigned int node_conf_id;
		tree_node_find_child_by_name	(&conf_node,"ID",&prop_node);
		tree_mamanger_get_node_dword	(&prop_node,0,&node_conf_id);

		if(conf_id==node_conf_id)
		{
			while(tree_node_find_child_by_type	(&conf_node,NODE_USB_INTERFACE,&iface_node,if_idx)!=0)
			{
				char			driver_name[64];
				char			fs_name[64];
				mem_stream		driver_file;
				struct usb_interface *iface;
				mem_zone_ref	driver_node;

				driver_file.data.zone		=	PTR_NULL;
				driver_node.zone			=	PTR_NULL;

				iface=	get_device_interface	(dev,n_conf,if_idx);

				if(strlen_c(iface->driver_mod.name)<=0)
				{
					tree_node_find_child_by_name	(&iface_node,"class",&prop_node);
					tree_mamanger_get_node_dword	(&prop_node,0,&class_code);

					tree_node_find_child_by_name	(&iface_node,"sub class",&prop_node);
					tree_mamanger_get_node_dword	(&prop_node,0,&subclass_code);

					tree_node_find_child_by_name	(&iface_node,"protocol",&prop_node);
					tree_mamanger_get_node_dword	(&prop_node,0,&protocol_code);

					if(find_bus_drivers_class	(bus_drv,class_code,subclass_code,protocol_code,&driver_node,0)==1)
					{
						strcpy_s		(driver_name,64,tree_mamanger_get_node_name(&driver_node));
						strcpy_s		(fs_name,64,tree_mamanger_get_node_data_ptr(&driver_node,0));

						
						driver_file.data.zone=PTR_NULL;
						if(file_system_open_file(fs_name,driver_name,&driver_file)==1)
						{
							
							tpo_mod_init			(&iface->driver_mod);
							tpo_mod_load_tpo		(&driver_file,&iface->driver_mod,0);
							//register_tpo_exports	(&iface->driver_mod,iface->driver_mod.name);
							mem_stream_close		(&driver_file);
							iface->drv_initialized	=	0;
							drv_loaded				=	1;

							kernel_log	(kernel_log_id,"bus device driver opened \n ");
							//return 0xFFFFFFFF;
						}
						else
						{
							kernel_log	(kernel_log_id,"could not open bus driver file ");
							writestr	(driver_name);
							writestr	(" on ");
							writestr	(tree_mamanger_get_node_name(file_system_get_root_node(fs_name)));
							writestr	("\n");
						}
						
					}
					else
					{
						kernel_log	(kernel_log_id,"no driver found for ");
						writeint	(class_code,16);
						writestr	(" ");
						writeint	(subclass_code,16);
						writestr	(" ");
						writeint	(protocol_code,16);
						writestr	("\n");
					}
					
				}
				else
				{
					drv_loaded	=	1;
					kernel_log	(kernel_log_id,"driver already found ");
					writestr	(iface->driver_mod.name);
					writestr	("\n");

				}

				release_zone_ref		(&iface_node);
				if_idx++;
			}
		}
		release_zone_ref(&conf_node);
		n_conf++;
	}

	if(drv_loaded==1)
		return dev->dev_id;
	else
		return 0xFFFFFFFF;
}





OS_API_C_FUNC(unsigned int) init_bus_device	(unsigned int usb_dev_id)
{
	struct usb_device	*usb_device_ptr;
	struct usb_configuration *config;
	struct usb_interface	 *iface;
	int						i,ret_code;
	initialize_interface_func_ptr initialize_interface;
	
	kernel_log	(kernel_log_id,"start usb device [");
	writeint	(usb_dev_id,16);
	writestr	("]\n");

	usb_device_ptr		=	find_usb_device_by_id	(usb_dev_id);
	if(usb_device_ptr	== PTR_NULL)					
	{
		kernel_log	(kernel_log_id,"cannot find usb device \n"); return 0;
	}


	config	=	get_zone_ptr(&usb_device_ptr->configs_ref,usb_device_ptr->cur_configuration*sizeof(struct usb_configuration));

	i=0;
	while(i<config->conf_desc.bNumInterfaces)
	{
		
		iface					=	get_device_interface		(usb_device_ptr,usb_device_ptr->cur_configuration,i);

		if(iface->drv_initialized==0)
		{
			kernel_log		(kernel_log_id,"initialize usb interface '");
			writestr		(iface->driver_mod.name);
			writestr		("'\n");

			set_tpo_mod_exp_value32_name							(&iface->driver_mod,"do_usb_ctrl_transfer",mem_to_uint(do_usb_ctrl_transfer));

			initialize_interface	=	get_tpo_mod_exp_addr_name	(&iface->driver_mod,"initialize_interface");
			

			if(initialize_interface==PTR_NULL)
			{
				kernel_log	(kernel_log_id,"no initialize_interface \n");
				return 0;
			}
			if(initialize_interface			(usb_device_ptr,usb_device_ptr->cur_configuration,i,&ret_code)==1)
			{
				iface->drv_initialized			=	1;
				device_read_interface_data		(usb_device_ptr,i);
			}

		}

		i++;
	}


	
	return 1;
}



