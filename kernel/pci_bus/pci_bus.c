#define PCI_BUS_DRIVER_FUNC	C_EXPORT
#include <std_def.h>
#include <std_mem.h>
#include "kern.h"
#include "sys_pci.h"
#include "mem_base.h"
#include "lib_c.h"
#include "tree.h"
#include "sys/async_stream.h"
#include "sys/mem_stream.h"
#include "sys/file_system.h"
#include "sys/tpo_mod.h"
#include "sys/stream_dev.h"
#include "../bus_manager/bus_drv.h"
#include "sys/pci_device.h"

PCI_BUS_DRIVER_FUNC void		 C_API_FUNC init_bus_driver				(bus_driver *bus_drv, unsigned int int_mode);


PCI_BUS_DRIVER_FUNC unsigned int C_API_FUNC bus_scan_devices			(bus_driver	*bus_drv);
PCI_BUS_DRIVER_FUNC unsigned int C_API_FUNC init_new_bus_device			(bus_driver *bus_drv,mem_zone_ref *dev_node);
PCI_BUS_DRIVER_FUNC int			 C_API_FUNC get_device_driver			(unsigned int device_id,const tpo_mod_file **driver);
PCI_BUS_DRIVER_FUNC unsigned int C_API_FUNC find_device_by_type			(unsigned int device_type,unsigned int index);
PCI_BUS_DRIVER_FUNC void		 C_API_FUNC  read_pci_bar				(pci_device	*pci_device,unsigned int reg_idx);

mem_zone_ref		pci_devices_array	=	{PTR_INVALID};
unsigned int		n_pci_devices		=	0xFFFFFFFF;
unsigned int		kernel_log_id		= 	0xFFFFFFFF;
unsigned int		interupt_mode		= 0xFFFFFFFF;

OS_API_C_FUNC(void) init_bus_driver(bus_driver *bus_drv, unsigned int int_mode)
{
	unsigned int		n;
	pci_device			*devices_ptr;
	
	interupt_mode			= int_mode;
	
	kernel_log_id			=	get_new_kern_log_id		("pci_bus: ",0x09);

	strcpy_s(bus_drv->name,32,"PCI");

	bus_drv->bus_type		=	1;
	pci_devices_array.zone	=	PTR_NULL;
	n_pci_devices			=	0;
	allocate_new_zone		(0,64*sizeof(pci_device),&pci_devices_array);
	devices_ptr				=	get_zone_ptr(&pci_devices_array,0);

	kernel_log	(kernel_log_id,"init pci driver ");
	writeptr	(devices_ptr);
	writestr	(" ");
	writeptr	(pci_devices_array.zone);
	writestr	("\n");
	n=0;
	while(n<64)
	{
		devices_ptr[n].bus_id			=0xFFFFFFFF;
		devices_ptr[n].dev_id			=0xFFFFFFFF;
		devices_ptr[n].device_type		=0xFFFFFFFF;
		devices_ptr[n].device_sub_type	=0xFFFFFFFF;

		memset_c	(devices_ptr[n].driver_name,0,128);
		n++;
	}

	
}




pci_device * find_pci_device(unsigned int dev_id)
{
	unsigned int   n;
	pci_device	  *devices_ptr;

	if(n_pci_devices==0xFFFFFFFF)return PTR_NULL;
	if(get_zone_size(&pci_devices_array)<=0)return PTR_NULL;
	
	devices_ptr	=	get_zone_ptr(&pci_devices_array,0);
	n			=	0;

	while(n<n_pci_devices)
	{
		if(devices_ptr[n].dev_id==dev_id)return &devices_ptr[n];
		n++;
	}

	return PTR_NULL;
}

OS_API_C_FUNC(int) get_device_driver(unsigned int device_id,const tpo_mod_file **driver)
{
	pci_device	  *device_ptr;

	device_ptr= find_pci_device(device_id);

	if(device_ptr==PTR_NULL)
		return 0;

	if(get_zone_size(&device_ptr->driver_mod.data_sections)<=0)return 0;

	(*driver)=&device_ptr->driver_mod;

	return 1;

}

OS_API_C_FUNC(unsigned int) find_device_by_type(unsigned int device_type,unsigned int index)
{
	unsigned int   n;
	unsigned int   n_found;
	unsigned int   d_type,d_sub_type;
	pci_device	  *devices_ptr;

	if(n_pci_devices==0xFFFFFFFF)return 0xFFFFFFFF;
	if(get_zone_size(&pci_devices_array)<=0)return 0xFFFFFFFF;

	switch(device_type)
	{
		case DEVICE_TYPE_AUDIO:
			d_type			=	0x04;
			d_sub_type		=	0x01;
		break;
		case DEVICE_TYPE_VGA:
			d_type			=	0x03;
			d_sub_type		=	0x00;
		break;
		case DEVICE_TYPE_STORAGE:
			d_type			=	0x01;
			d_sub_type		=	0xFF;
		break;
		
		default :return 0xFFFFFFFF;break;
	}

	devices_ptr	=	get_zone_ptr(&pci_devices_array,0);
	n			=	0;
	n_found		=	0;

	while(n<n_pci_devices)
	{
		/*
		kernel_log(kernel_log_id,"find device ");
		writeint(devices_ptr[n].device_type,16);
		writestr(" ");
		writeint(devices_ptr[n].device_sub_type,16);
		writestr("\n");
		*/

		if(	(devices_ptr[n].device_type==d_type)&&
			((devices_ptr[n].device_sub_type==d_sub_type)||(d_sub_type==0xFF)))
		{
			if(index==n_found)
				return devices_ptr[n].dev_id;

			n_found++;
		}
		n++;
	}

	return 0xFFFFFFFF;
}

OS_API_C_FUNC(void) read_pci_bar(pci_device	*pci_device,unsigned int reg_idx)
{
	unsigned int		base_reg_region,base_reg_val,base_reg_flags,base_reg_addr;
	unsigned short		pci_code;
	unsigned int		pci_bus,pci_dev,pci_func;


	pci_code			=	pci_device->pci_code;
	pci_bus				=	(pci_code>>8)	& 0xFF;
	pci_dev				=	(pci_code>>3)	& 0x1F;
	pci_func			=	pci_code		& 0x07;

	base_reg_val	=	read_pci_dword_c(pci_bus, pci_dev, pci_func,0x10+reg_idx*4);

	base_reg_addr	=	base_reg_val&0xFFFFFFF0;
	base_reg_flags	=	base_reg_val&0xF;

	if(base_reg_flags&0x01)
	{	
		in_32_c	(base_reg_addr);
	}
	else
	{
		*(unsigned int *)(uint_to_mem(base_reg_addr));
	}

	write_pci_dword_c	(pci_bus, pci_dev, pci_func,0x10+reg_idx*4,0xFFFFFFFF);
	base_reg_region	=	read_pci_dword_c(pci_bus, pci_dev, pci_func,0x10+reg_idx*4);
	write_pci_dword_c	(pci_bus, pci_dev, pci_func,0x10+reg_idx*4,base_reg_val);
	
	kernel_log			(kernel_log_id,"pci ressource : ");
	writeint			(base_reg_val,16);
	writestr			(" ");
	writeint			(base_reg_region,16);
	writestr			("\n");

}



OS_API_C_FUNC(unsigned int) bus_scan_devices(bus_driver	*bus_drv)
{
	char				id_buffer[32];
	mem_zone_ref		ref_pci_bus_node;
	unsigned int		pci_bus;
	unsigned int		pci_dev;
	unsigned int		pci_func;
	unsigned int		pci_dword;
	int					bus_added;
	unsigned char		int_line;
	unsigned char		int_pin;
	
	
	bus_drv->bus_root_node.zone	=	PTR_NULL;

	if(tree_manager_create_node	("PCI_ROOT",NODE_BUS_DEV_ROOT,&bus_drv->bus_root_node)!=1)
	{
		kernel_log(kernel_log_id,"could not create pci root node \n");
		return 0;
	}

	tree_manager_write_node_dword		(&bus_drv->bus_root_node,0,bus_drv->bus_id);



	ref_pci_bus_node.zone=PTR_NULL;
	kernel_log(kernel_log_id,"starting pci scan \n");
	
	for(pci_bus=0;pci_bus<255;pci_bus++)
	{
		bus_added			=0;
		for(pci_dev=0;pci_dev<32;pci_dev++)
		{
			int					dev_added=0;
			mem_zone_ref		ref_pci_dev_node;

			ref_pci_dev_node.zone=PTR_NULL;

			for(pci_func=0;pci_func<8;pci_func++)
			{
				mem_zone_ref		ref_pci_fnc_node = { PTR_NULL };
				mem_zone_ref		pic_infos = { PTR_NULL };


				pci_dword	=	read_pci_dword_c(pci_bus, pci_dev, pci_func,0);
				if ((pci_dword & 0xFFFF) != 0xFFFF)
				{
					unsigned short dev_code;
					unsigned short vendor_id;
					unsigned short subsys_id;
					unsigned short subsys_vendor_id;
					unsigned short device_id;
					unsigned char class_code,class_sub_code,class_pi;


					dev_code=(pci_bus<<8)|(pci_dev<<3)|pci_func;

												
					if(!bus_added)
					{
						memset_c					(id_buffer,0,32);
						strcpy_s					(id_buffer,32,"PCI_BUS ");
						itoa_s						(pci_bus,&id_buffer[8],16,16);
						tree_manager_add_child_node	(&bus_drv->bus_root_node,id_buffer,NODE_PCI_BUS,&ref_pci_bus_node);
						bus_added					=1;
					}
			
					if(!dev_added)
					{
						memset_c						(id_buffer,0,32);
						strcpy_s						(id_buffer,32,"PCI_BUS_DEV ");
						itoa_s							(pci_dev,&id_buffer[12],16,16);

						tree_manager_add_child_node		(&ref_pci_bus_node,id_buffer	,NODE_PCI_BUS_DEV	,&ref_pci_dev_node);
						tree_manager_write_node_dword	(&ref_pci_dev_node, 0, pci_dev);


						dev_added					=1;
					}

					memset_c							(id_buffer,0,32);
					strcpy_s							(id_buffer,32,"PCI_DEV_FNC ");
					itoa_s								(pci_func,&id_buffer[12],16,16);
		



					vendor_id		=	read_pci_word_c(pci_bus, pci_dev, pci_func,0);
					device_id		=	read_pci_word_c(pci_bus, pci_dev, pci_func,2);
					class_code		=	read_pci_byte_c(pci_bus, pci_dev, pci_func,11);
					class_sub_code  =	read_pci_byte_c(pci_bus, pci_dev, pci_func,10);
					class_pi		=	read_pci_byte_c(pci_bus, pci_dev, pci_func,9);
					subsys_vendor_id=	read_pci_word_c(pci_bus, pci_dev, pci_func,44);
					subsys_id		=	read_pci_word_c(pci_bus, pci_dev, pci_func,46);

					int_line		=	read_pci_byte_c(pci_bus, pci_dev, pci_func, 60);
					int_pin			=	read_pci_byte_c(pci_bus, pci_dev, pci_func, 61);


					/*
					writestr("pci dev node ");
					writeptr(ref_pci_dev_node.zone);
					writestr("\n");
					*/
					

					
					tree_manager_add_child_node			(&ref_pci_dev_node,id_buffer,NODE_BUS_DEVICE	,&ref_pci_fnc_node);
					
					
					tree_manager_allocate_node_data		(&ref_pci_fnc_node,8);
					
					tree_manager_write_node_word		(&ref_pci_fnc_node,0,dev_code);
					tree_manager_write_node_word		(&ref_pci_fnc_node,2,vendor_id);
					tree_manager_write_node_word		(&ref_pci_fnc_node,4,device_id);
					tree_manager_write_node_byte		(&ref_pci_fnc_node,6,class_code);
					tree_manager_write_node_byte		(&ref_pci_fnc_node,7,class_sub_code);
					tree_manager_write_node_byte		(&ref_pci_fnc_node,8,class_pi);
					tree_manager_write_node_word		(&ref_pci_fnc_node,10,subsys_vendor_id);
					tree_manager_write_node_word		(&ref_pci_fnc_node,12,subsys_id);
					tree_manager_write_node_byte		(&ref_pci_fnc_node,14, int_line);
					tree_manager_write_node_byte		(&ref_pci_fnc_node,15, int_pin);

					
					
					tree_manager_set_child_value_i32	(&ref_pci_fnc_node,"id vendor",vendor_id);
					tree_manager_set_child_value_i32	(&ref_pci_fnc_node,"id product",device_id);
					tree_manager_set_child_value_i32	(&ref_pci_fnc_node,"id class",class_code);
					tree_manager_set_child_value_i32	(&ref_pci_fnc_node,"id subclass",class_sub_code);
					tree_manager_set_child_value_i32	(&ref_pci_fnc_node,"id rev",class_pi);
					tree_manager_set_child_value_i32	(&ref_pci_fnc_node,"id subsys vendor",subsys_vendor_id);
					tree_manager_set_child_value_i32	(&ref_pci_fnc_node,"id subsys",subsys_id);
					tree_manager_set_child_value_str	(&ref_pci_fnc_node, "name", "pci device");

					if (tree_manager_add_child_node(&ref_pci_fnc_node, "PIC", NODE_BUS_DEVICE_IRQ_INFOS, &pic_infos))
					{
						tree_manager_set_child_value_i32(&pic_infos, "int line", int_line);
						tree_manager_set_child_value_i32(&pic_infos, "int pin", int_pin);
						release_zone_ref(&pic_infos);
					}


					




					release_zone_ref		(&ref_pci_fnc_node);
				}
			}
			if(dev_added)release_zone_ref	(&ref_pci_dev_node);
		}
		if(bus_added)release_zone_ref(&ref_pci_bus_node);
		
	}
	//tree_manager_dump_node_rec(&bus_drv->bus_root_node,0,0xFF);
	kernel_log(kernel_log_id,"pci scan finished\n");
	//snooze(1000000);
	return 1;
}

void __stdcall default_func(unsigned int param)
{
	kernel_log(kernel_log_id,"func stub called ");
	writeint(param,16);
	writestr("\n");
	snooze(1000000);
	return ;
}

OS_API_C_FUNC(unsigned int) get_bus_device_irq(unsigned int dev_id)
{
	pci_device *dev=find_pci_device(dev_id);
	unsigned int int_line=0xFFFFFFFF;

	if (dev == PTR_NULL)
		return 0;
	
	if (interupt_mode == 0)
	{
		mem_zone_ref pic = { PTR_NULL };

		if (tree_manager_find_child_node(&dev->pci_node, NODE_HASH("PIC"), NODE_BUS_DEVICE_IRQ_INFOS, &pic))
		{
			tree_manager_get_child_value_i32(&pic, NODE_HASH("int line"), &int_line);
			release_zone_ref(&pic);
		}
		return int_line;
	}
	else 
	{
		mem_zone_ref apic = { PTR_NULL };

		if (tree_manager_find_child_node(&dev->pci_node, NODE_HASH("APIC"), NODE_BUS_DEVICE_IRQ_INFOS, &apic))
		{
			tree_manager_get_child_value_i32(&apic, NODE_HASH("int line"), &int_line);
			release_zone_ref(&apic);
		}
		return int_line;
	}
}

OS_API_C_FUNC(int) get_device_icon(unsigned int dev_id, mem_zone_ref_ptr icon)
{
	unsigned short class_code;
	unsigned char	dev_type, dev_subtype, dev_pi;
	pci_device *dev = find_pci_device(dev_id);
	if (dev == PTR_NULL)
		return 0;
	
	tree_mamanger_get_node_word(&dev->pci_node, 6, &class_code);
	
	dev_type = class_code & 0xFF;
	dev_subtype = (class_code >> 8) & 0xFF;

	tree_manager_create_node("icon", NODE_GFX_IMAGE, icon);

	switch (dev_type)
	{
		case 0x01:
			tree_manager_set_child_value_str(icon, "iconpath", "/system/icons/pci/storage_icon.png");
		break;
		case 0x02:
			tree_manager_set_child_value_str(icon, "iconpath", "/system/icons/pci/network_icon.png");
		break;
		case 0x03:
			tree_manager_set_child_value_str(icon, "iconpath", "/system/icons/pci/display_icon.png");
		break;
		case 0x04:
			tree_manager_set_child_value_str(icon, "iconpath", "/system/icons/pci/multimedia_icon.png");
		break;
		case 0x09:
			tree_manager_set_child_value_str(icon, "iconpath", "/system/icons/pci/input_icon.png");
		break;
		case 0x0C:
			tree_manager_set_child_value_str(icon, "iconpath", "/system/icons/pci/serial_icon.png");
		break;
		case 0x0D:
			tree_manager_set_child_value_str(icon, "iconpath", "/system/icons/pci/wireless_icon.png");
		break;
		default:
			tree_manager_set_child_value_str(icon, "iconpath", "/system/icons/pci/unknown_icon.png");
		break;
	}

	return 1;
}

OS_API_C_FUNC(unsigned int) init_new_bus_device(bus_driver *bus_drv,mem_zone_ref *dev_node)
{
	char							driver_name[64];
	char							fs_name[64];
	mem_stream						driver_file;
	unsigned short					vendor_id,device_id;
	unsigned short					subsys_vendor_id,subsys_id;
	unsigned short					pci_code;
	unsigned short					class_code;

	unsigned char					dev_type,dev_subtype,dev_pi;
	unsigned int					drv_type;
	pci_device						*pci_dev;
	mem_zone_ref					driver_node = { PTR_NULL };
	
	//tree_manager_dump_node_rec(dev_node,0);

	
	tree_mamanger_get_node_word	(dev_node,0,&pci_code);
	tree_mamanger_get_node_word	(dev_node,2,&vendor_id);
	tree_mamanger_get_node_word	(dev_node,4,&device_id);
	tree_mamanger_get_node_word	(dev_node,6,&class_code);
	tree_mamanger_get_node_byte	(dev_node,8,&dev_pi);

	tree_mamanger_get_node_word	(dev_node,10,&subsys_vendor_id);
	tree_mamanger_get_node_word	(dev_node,12,&subsys_id);

	dev_type			=	class_code & 0xFF;
	dev_subtype			=	(class_code >> 8 ) & 0xFF;

	memset_c(driver_name,0,64);
	
	if(find_bus_drivers(bus_drv,vendor_id,device_id,subsys_vendor_id,subsys_id,&driver_node,0)==0)
	{
		if(find_bus_drivers_class(bus_drv,dev_type,dev_subtype,dev_pi,&driver_node,0)==0)
		{
			return 0xFFFFFFFF;	
		}
	}

	strcpy_s		(driver_name,64,tree_mamanger_get_node_name(&driver_node));
	strcpy_s		(fs_name,64,tree_mamanger_get_node_data_ptr(&driver_node,0));
	drv_type	=	tree_mamanger_get_node_type(&driver_node);

/*
	n	=	0;
	st	=	0;

	memset_c(fs_name,0,64);
	memset_c(driver_name,0,64);

	while(n<strlen_c(driver_path))
	{
		if(driver_path[n]==':')
			st=(n+1);
		else
		{
			if(st==0)
				fs_name[n]		 =driver_path[n];
			else
				driver_name[n-st]=driver_path[n];
		}
		n++;
	}
	*/
	
	/*
	writestr(fs_name);
	writestr(" ");
	writestr(driver_name);
	writestr(" ");
	writeint(drv_type,16);
	writestr("\n");
	snooze(10000000);
	*/
	
	

	driver_file.data.zone=PTR_NULL;
	
	
	if(file_system_open_file(fs_name,driver_name,&driver_file)!=1)
	{
		kernel_log	(kernel_log_id,"could not open pci driver file ");
		writestr	(driver_name);
		writestr	(" on ");
		writestr	(tree_mamanger_get_node_name(file_system_get_root_node(fs_name)));
		writestr	("\n");

		return 0xFFFFFFFF;
	}
	
	
	pci_dev				=	get_zone_ptr(&pci_devices_array,n_pci_devices*sizeof(pci_device));
	
	
	pci_dev->driver_conf_node.zone			=PTR_NULL;
	pci_dev->pci_node.zone					=PTR_NULL;
	pci_dev->driver_mod.data_sections.zone	=PTR_NULL;

	pci_dev->sci		=PTR_NULL;
	

	tree_node_find_child_by_name(&driver_node,"config",&pci_dev->driver_conf_node);
		
	copy_zone_ref			(&pci_dev->pci_node,dev_node);
	strcpy_s				(pci_dev->driver_name,128,driver_name);
	pci_dev->pci_code	=	pci_code;
	pci_dev->dev_id		=	n_pci_devices+1;
	pci_dev->bus_id		=	bus_drv->bus_id;

	if(drv_type== NODE_BUS_NDIS_DRIVER)
		pci_dev->is_ndis	=	1;
	else
		pci_dev->is_ndis	=	0;
	
	

	tpo_mod_init			(&pci_dev->driver_mod);
	tpo_mod_load_tpo		(&driver_file,&pci_dev->driver_mod,mem_to_uint(default_func));
	
	mem_stream_close		(&driver_file);
	
	n_pci_devices++;
	/*
	writestr	("bus device loaded ");
	writeint	(pci_dev->dev_id,16);
	writestr	("\n");
	*/

	tree_manager_set_child_value_i32(dev_node, "id", pci_dev->dev_id);

	return pci_dev->dev_id;
}






