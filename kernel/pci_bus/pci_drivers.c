#define PCI_BUS_DRIVER_FUNC	C_EXPORT
#include <std_def.h>
#include <std_mem.h>
#include "kern.h"
#include "sys_pci.h"
#include "mem_base.h"
#include "lib_c.h"
#include "tree.h"
#include "sys/async_stream.h"
#include "sys/mem_stream.h"
#include "sys/file_system.h"
#include "sys/tpo_mod.h"
#include "sys/stream_dev.h"
#include "../bus_manager/bus_drv.h"
#include "sys/pci_device.h"
#include "sys/ndis.h"

//#include "gfx/graphic_base.h"

//PCI_BUS_DRIVER_FUNC int			 C_API_FUNC init_bus_drivers_tree			(bus_driver	*bus_drv,const char *fs_name);
PCI_BUS_DRIVER_FUNC int			 C_API_FUNC init_bus_device					(unsigned int stream_dev_id);
PCI_BUS_DRIVER_FUNC int			 C_API_FUNC bus_drv_start_device_stream		(unsigned int pci_dev_id,unsigned int str_idx);
PCI_BUS_DRIVER_FUNC int			 C_API_FUNC bus_drv_get_device_format		(unsigned int pci_dev_id,unsigned int fmt_idx,unsigned int *fmt_ptr);
PCI_BUS_DRIVER_FUNC int			 C_API_FUNC bus_drv_set_device_format		(unsigned int pci_dev_id,unsigned int fmt_idx);
PCI_BUS_DRIVER_FUNC int			 C_API_FUNC bus_drv_get_stream_infos		(unsigned int pci_dev_id,unsigned int str_idx,mem_zone_ref	*infos_node);
PCI_BUS_DRIVER_FUNC int			 C_API_FUNC bus_drv_set_stream_pos			(unsigned int pci_dev_id,unsigned int str_idx,large_uint_t sec_pos);
NDIS_API			int			 C_API_FUNC load_ndis_driver				(pci_device *dev);

extern pci_device	*find_pci_device(unsigned int dev_id);
extern unsigned int kernel_log_id;
extern unsigned int	 interupt_mode;
extern mem_zone_ref	 pci_devices_array ;
extern unsigned int	 n_pci_devices;




unsigned int pci_drv_probe_device		(pci_device		*pci_device)
{
	pci_probe_device_func_ptr		probe_device;
	pci_get_cmd_flag_func_ptr		get_pci_cmd;
	unsigned int					pci_bus,pci_dev,pci_fn;
	unsigned int					orig_pci_value;
	unsigned int					flags;
	int								ret;
	unsigned short					pci_code;
	
	
	probe_device	= get_tpo_mod_exp_addr_name	(&pci_device->driver_mod,"probe_device");
	if(probe_device==PTR_NULL)
	{
		kernel_log	(kernel_log_id,"no probe device\n");
		return 0;
	}

	get_pci_cmd		= get_tpo_mod_exp_addr_name			(&pci_device->driver_mod,"pci_get_device_cmd_flag");

	tree_mamanger_get_node_word	(&pci_device->pci_node,0,&pci_code);
	pci_bus			=	(pci_code>>8)	& 0xFF;
	pci_dev			=	(pci_code>>3)	& 0x1F;
	pci_fn			=	pci_code		& 0x07;

	read_pci_config_c	(pci_bus,pci_dev,pci_fn,(unsigned int *)&pci_device->config);


	if(get_pci_cmd!=PTR_NULL)
	{
		orig_pci_value	=	read_pci_word_c	(pci_bus,pci_dev,pci_fn, PCI_PCICMD);

		/*
		kernel_log			(kernel_log_id,"pci : dev vendor id : ");
		writeint			(pci_device->config.vendor_id,16);
		writestr			("dev id : ");
		writeint			(pci_device->config.device_id,16);
		writestr			("\n");
		*/

		flags			=	get_pci_cmd(pci_device->config.vendor_id,pci_device->config.device_id);

		/*
		kernel_log			(kernel_log_id,"pci cmd flags : ");
		writeint			(flags,16);
		writestr			("\n");
		*/

		write_pci_word_c	(pci_bus,pci_dev,pci_fn, PCI_PCICMD, orig_pci_value|flags);
	}

	read_pci_config_c	(pci_bus,pci_dev,pci_fn,(unsigned int *)&pci_device->config);

	pci_device->device_type		=	pci_device->config.class_code[2];
	pci_device->device_sub_type	=	pci_device->config.class_code[1];
	pci_device->device_pi		=	pci_device->config.class_code[0];

	
	kernel_log	(kernel_log_id,"probing device [");
	writeptr	(probe_device);
	writestr	("] type ");
	writeint	(pci_device->device_type,16);
	writestr	(" ");
	writeint	(pci_device->device_sub_type,16);
	writestr	(" ");
	writeint	(pci_device->device_pi,16);
	writestr	("\n");
	
	
	ret		=	probe_device	(pci_device);

	/*
	kernel_log	(kernel_log_id,"ret : ");
	writeint	(ret,16);
	writestr	("\n");
	*/

	if(ret!=1)
	{
		if(get_pci_cmd!=PTR_NULL)
			write_pci_word_c(pci_bus,pci_dev,pci_fn, PCI_PCICMD, orig_pci_value);

		return 0;
	}
	return 1;
}


int pci_drv_start_device		(pci_device 	*pci_device)
{
	pci_init_driver_func_ptr		init_driver;
	unsigned int					pci_bus,pci_dev,pci_fn;
	unsigned short					pci_code;
		
	init_driver	= get_tpo_mod_exp_addr_name			(&pci_device->driver_mod,"init_driver");
	if(init_driver==PTR_NULL)return 0;
	
	kernel_log	(kernel_log_id,"init driver \n");
	if(init_driver()!=1)return 0;
	
	tree_mamanger_get_node_word	(&pci_device->pci_node,0,&pci_code);
	pci_bus				=	(pci_code>>8)	& 0xFF;
	pci_dev				=	(pci_code>>3)	& 0x1F;
	pci_fn				=	pci_code		& 0x07;


	

	pci_enable_device_c	( pci_bus,pci_dev,pci_fn);
	
	return 1;
}

OS_API_C_FUNC(int)	bus_drv_get_device_format	(unsigned int pci_dev_id,unsigned int fmt_idx,unsigned int *fmt_ptr)
{
	pci_get_device_format_func_ptr		get_format;

	pci_device		*pci_device_ptr;
	
	pci_device_ptr		=	find_pci_device	(pci_dev_id);
	if(pci_device_ptr	== PTR_NULL)					
	{
		kernel_log	(kernel_log_id,"cannot find pci device (get format) \n"); return 0;
	}		

	get_format	= get_tpo_mod_exp_addr_name			(&pci_device_ptr->driver_mod,"get_format");
	if(get_format==PTR_NULL)return 0;

	return get_format(fmt_idx,fmt_ptr);
}

OS_API_C_FUNC(int)	bus_drv_set_device_format	(unsigned int pci_dev_id,unsigned int fmt_idx)
{
	pci_set_device_format_func_ptr		set_format;
	pci_device		*pci_device_ptr;
	
	pci_device_ptr		=	find_pci_device	(pci_dev_id);
	if(pci_device_ptr	== PTR_NULL)					
	{
		kernel_log	(kernel_log_id,"cannot find pci device (set format) \n"); return 0;
	}		

	set_format	= get_tpo_mod_exp_addr_name			(&pci_device_ptr->driver_mod,"set_format");
	if(set_format==PTR_NULL)return 0;

	return set_format(fmt_idx);
}

/*
int	bus_drv_setup_renderer	(unsigned int pci_dev_id)
{
	pci_device		*pci_device_ptr;
	
	pci_device_ptr		=	find_pci_device	(pci_dev_id);
	if(pci_device_ptr	== PTR_NULL)					
	{
		kernel_log	(kernel_log_id,"cannot find pci device (setup_renderer) \n"); 
		return 0;
	}		
	
	return gfx_new_renderer(&pci_device_ptr->driver_mod);
}
*/

OS_API_C_FUNC(int) bus_drv_start_device_stream	 (unsigned int pci_dev_id,unsigned int str_idx)
{
	pci_start_stream_func_ptr	start_stream;
	pci_device					*pci_dev;


	pci_dev			=	find_pci_device(pci_dev_id);
	if(pci_dev==PTR_NULL)return 0;


	start_stream	= get_tpo_mod_exp_addr_name			(&pci_dev->driver_mod,"start_stream");
	if(start_stream==PTR_NULL)
	{	
		kernel_log	(kernel_log_id,"start stream not found \n");
		return 0;
	}

	start_stream(str_idx);

	return 1;
}

OS_API_C_FUNC(int) bus_drv_read_device_stream	 (unsigned int pci_dev_id,unsigned int str_idx,mem_zone_ref	*req)
{
	bus_drv_read_stream_func_ptr		read_stream;
	pci_device							*pci_dev;

	/*
	kernel_log	(kernel_log_id,"pci read stream ");
	writeint	(str_idx,10);
	writestr	(" dev : ");
	writeint	(pci_dev_id,10);	
	writestr	("\n");
	*/

	pci_dev			=	find_pci_device(pci_dev_id);
	if(pci_dev==PTR_NULL) {
		kernel_log(kernel_log_id, "pci_dev not found \n");
		return 0;
	}

	/*
	kernel_log	(kernel_log_id," dev found : ");
	writestr	(pci_dev->name);
	writestr	("\n");
	*/

	read_stream	= get_tpo_mod_exp_addr_name			(&pci_dev->driver_mod,"read_stream");
	if(read_stream==PTR_NULL)
	{	
		kernel_log	(kernel_log_id,"read stream not found \n");
		return 0;
	}

	return read_stream(str_idx,req);

	
}

OS_API_C_FUNC(int) bus_drv_get_stream_infos	 (unsigned int pci_dev_id,unsigned int str_idx,mem_zone_ref	*infos_node)
{
	bus_drv_get_stream_infos_func_ptr	stream_infos;
	pci_device					*pci_dev;

	/*
	kernel_log	(kernel_log_id,"get pci device stream infos ");
	writeint	(str_idx,10);
	writestr	(" dev : ");
	writeint	(pci_dev_id,10);	
	writestr	("\n");
	*/

	pci_dev			=	find_pci_device(pci_dev_id);
	if(pci_dev==PTR_NULL)return 0;

	/*
	kernel_log	(kernel_log_id," dev found : ");
	writestr	(pci_dev->name);
	writestr	("\n");
	*/

	stream_infos	= get_tpo_mod_exp_addr_name			(&pci_dev->driver_mod,"stream_infos");
	if(stream_infos==PTR_NULL)
	{	
		kernel_log	(kernel_log_id,"get stream infos not found \n");
		return 0;
	}

	return stream_infos(str_idx,infos_node);

	
}

OS_API_C_FUNC(int) bus_drv_set_stream_pos	 (unsigned int pci_dev_id,unsigned int str_idx,large_uint_t sec_pos)
{
	bus_drv_set_stream_infos_func_ptr	stream_set_pos;
	pci_device					*pci_dev;

	kernel_log	(kernel_log_id,"set pci device stream pos ");
	writeint	(str_idx,10);
	writestr	(" dev : ");
	writeint	(pci_dev_id,10);	
	writestr	("\n");

	pci_dev			=	find_pci_device(pci_dev_id);
	if(pci_dev==PTR_NULL)return 0;

	kernel_log	(kernel_log_id," dev found : ");
	writestr	(pci_dev->name);
	writestr	("\n");

	stream_set_pos	= get_tpo_mod_exp_addr_name			(&pci_dev->driver_mod,"stream_set_pos");
	if(stream_set_pos==PTR_NULL)
	{	
		kernel_log	(kernel_log_id,"set stream pos not found \n");
		return 0;
	}

	return stream_set_pos(str_idx,sec_pos);
}

OS_INT_C_FUNC(unsigned int) bus_interupt_handler(void *param)
{
	unsigned int   n;
	pci_device	  *devices_ptr	=	PTR_NULL;
	interupt_func_ptr	intrh	=	PTR_NULL;
	unsigned int irq_data = 0;
	unsigned int irq_line = mem_to_uint(param);

	if (n_pci_devices == 0xFFFFFFFF)return 0;
	if (get_zone_size(&pci_devices_array) <= 0)return 0;

	/*
	kernel_log(kernel_log_id, "bus interupt ");
	writeint(irq_line,16);
	writestr("\n");
	*/

	for (devices_ptr = get_zone_ptr(&pci_devices_array, 0), n = 0; n < n_pci_devices; n++)
	{
		unsigned int dev_intl = 0xFFFFFFFF;


		if (devices_ptr->is_ndis)
			continue;

		if (devices_ptr[n].sci != PTR_NULL)
		{
			int i;
			for (i = 0; devices_ptr[n].sci[i].irq_line != 0xFFFFFFFF; i++)
			{
				if (devices_ptr[n].sci[i].irq_line == irq_line)
				{
					dev_intl = devices_ptr[n].sci[i].irq_line;
					irq_data = devices_ptr[n].sci[i].irq_data;
					break;
				}
			}
		}
		else if (interupt_mode == 0)
		{
			mem_zone_ref pic = { PTR_NULL };

			irq_data = 0;

			if (tree_manager_find_child_node(&devices_ptr[n].pci_node, NODE_HASH("PIC"), NODE_BUS_DEVICE_IRQ_INFOS, &pic))
			{
				if (!tree_manager_get_child_value_i32(&pic, NODE_HASH("int line"), &dev_intl))
					dev_intl = 0;

				release_zone_ref(&pic);
			}
		}
		else
		{
			mem_zone_ref apic = { PTR_NULL };

			irq_data = 0;

			if (tree_manager_find_child_node(&devices_ptr[n].pci_node, NODE_HASH("APIC"), NODE_BUS_DEVICE_IRQ_INFOS, &apic))
			{
				if (!tree_manager_get_child_value_i32(&apic, NODE_HASH("int line"), &dev_intl))
					dev_intl = 0;

				release_zone_ref(&apic);
			}
		}

		if (dev_intl == irq_line)
		{

			/*
			kernel_log(kernel_log_id, "bus device found ");
			writestr(devices_ptr[n].name);
			writestr("\n");
			*/

			intrh = get_tpo_mod_exp_addr_name(&devices_ptr[n].driver_mod, "interupt_handler");
			if (intrh != PTR_NULL)
			{
				intrh(uint_to_mem(irq_data));
			}
		}
	}

	return 1;
}


OS_API_C_FUNC(int) init_bus_device	(unsigned int pci_dev_id)
{
	pci_device		*pci_device_ptr;
	
	kernel_log	(kernel_log_id,"start device [");
	writeint	(pci_dev_id,16);
	writestr	("]\n");

	pci_device_ptr		=	find_pci_device	(pci_dev_id);
	if(pci_device_ptr	== PTR_NULL)					
	{
		kernel_log	(kernel_log_id,"cannot find pci device \n"); return 0;
	}

	if(pci_device_ptr->is_ndis==1)
	{
		kernel_log	(kernel_log_id,"ndis driver :");
		writestr	(pci_device_ptr->driver_name);
		writestr	("\n");

		load_ndis_driver(pci_device_ptr);
	}
	else
	{
		unsigned int int_line, int_sharable, int_triggering, int_polarity;

		if(pci_drv_probe_device	(pci_device_ptr)!=1){kernel_log	(kernel_log_id,"probe device failed \n"); return 0;}
		if(pci_drv_start_device	(pci_device_ptr)!=1){kernel_log	(kernel_log_id,"start device failed \n"); return 0;}

		if (pci_device_ptr->sci != PTR_NULL)
		{
			unsigned int n;

			for (n = 0; pci_device_ptr->sci[n].irq_line != 0xFFFFFFFF; n++)
			{
				if (interupt_mode == 1)
				{
					kernel_log(kernel_log_id, "bus sci irq ");
					writeint(pci_device_ptr->sci[n].irq_line, 16);
					writestr(" ");
					writeint(APIC_BASE + pci_device_ptr->sci[n].irq_line, 16);
					writestr("\n");

					apic_enable_irq_c(pci_device_ptr->sci[n].irq_line, APIC_BASE + pci_device_ptr->sci[n].irq_line, 0);
				}
					

				setup_interupt_c(pci_device_ptr->sci[n].irq_line, bus_interupt_handler, pci_device_ptr->sci[n].irq_line);


			}

			return 1;
		}
		else if (interupt_mode == 0)
		{
			mem_zone_ref pic = { PTR_NULL };

			if (tree_manager_find_child_node(&pci_device_ptr->pci_node, NODE_HASH("PIC"), NODE_BUS_DEVICE_IRQ_INFOS, &pic))
			{
				if (!tree_manager_get_child_value_i32(&pic, NODE_HASH("int line"), &int_line))
					int_line = 0;

				release_zone_ref(&pic);
			}
		}
		else
		{
			mem_zone_ref apic = { PTR_NULL };

			if (tree_manager_find_child_node(&pci_device_ptr->pci_node, NODE_HASH("APIC"), NODE_BUS_DEVICE_IRQ_INFOS, &apic))
			{
				if (!tree_manager_get_child_value_i32(&apic, NODE_HASH("int line"), &int_line))
					int_line = 0;

				if (!tree_manager_get_child_value_i32(&apic, NODE_HASH("Polarity"), &int_polarity))
					int_polarity = 0;

				if (!tree_manager_get_child_value_i32(&apic, NODE_HASH("Triggering"), &int_triggering))
					int_triggering = 0;

				if (!tree_manager_get_child_value_i32(&apic, NODE_HASH("Sharable"), &int_sharable))
					int_triggering = 0;


				release_zone_ref(&apic);
			}

			unsigned int vector;

			vector =  APIC_BASE+int_line;

			if (int_polarity)
				vector |= APIC_POLARITY_LOW;

			if (int_triggering)
				vector |= APIC_LEVEL_TRIGERING;


			kernel_log(kernel_log_id,"bus setup irq ");
			writeint(int_line, 16);
			writestr(" ");
			writeint(vector, 16);
			writestr("\n");

			apic_enable_irq_c(int_line, vector, 0);
		}
		setup_interupt_c(int_line, bus_interupt_handler, int_line);
		
	}
	return 1;
}

