#define DRIVER_FUNC	 C_EXPORT

#include <std_def.h>
#include <std_mem.h>
#include "kern.h"
#include "sys_pci.h"
#include "mem_base.h"
#include "lib_c.h"
#include "tree.h"
#include "sys/bitstream.h"
#include "sys/async_stream.h"
#include "sys/mem_stream.h"
#include "sys/tpo_mod.h"
#include "sys/file_system.h"
#include "sys/stream_dev.h"
#include "sys/pci_device.h"
#include "sys/usb_device.h"
#include "ohci.h"

#include <bus_manager/bus_drv.h>



#define OHCI_NO_INTRS           32
#define OHCI_HCCA_SIZE          256
#define OHCI_NO_EDS             (2*OHCI_NO_INTRS)


struct ohci_hcca {
         volatile unsigned int hcca_interrupt_table[OHCI_NO_INTRS];
         volatile unsigned int hcca_frame_number;
         volatile unsigned int hcca_done_head;
 #define OHCI_DONE_INTRS         1
 };
 
 //typedef struct ohci_hcca ohci_hcca_t;

 /* OHCI registers */
#define OHCI_REVISION           0x00    /* OHCI revision */
#define OHCI_REV_LO(rev)        ((rev) & 0xf)
#define OHCI_REV_HI(rev)        (((rev)>>4) & 0xf)
#define OHCI_REV_LEGACY(rev)    ((rev) & 0x100)
#define OHCI_CONTROL            0x04
#define OHCI_CBSR_MASK          0x00000003      /* Control/Bulk Service Ratio */
#define OHCI_RATIO_1_1          0x00000000
#define OHCI_RATIO_1_2          0x00000001
#define OHCI_RATIO_1_3          0x00000002
#define OHCI_RATIO_1_4          0x00000003
#define OHCI_PLE                0x00000004      /* Periodic List Enable */
#define OHCI_IE                 0x00000008      /* Isochronous Enable */
#define OHCI_CLE                0x00000010      /* Control List Enable */
#define OHCI_BLE                0x00000020      /* Bulk List Enable */
#define OHCI_HCFS_MASK          0x000000c0      /* HostControllerFunctionalState */
#define OHCI_HCFS_RESET         0x00000000
#define OHCI_HCFS_RESUME        0x00000040
#define OHCI_HCFS_OPERATIONAL   0x00000080
#define OHCI_HCFS_SUSPEND       0x000000c0
#define OHCI_IR                 0x00000100      /* Interrupt Routing */
#define OHCI_RWC                0x00000200      /* Remote Wakeup Connected */
#define OHCI_RWE                0x00000400      /* Remote Wakeup Enabled */


#define OHCI_COMMAND_STATUS     0x08
#define OHCI_HCR                0x00000001      /* Host Controller Reset */
#define OHCI_CLF                0x00000002      /* Control List Filled */
#define OHCI_BLF                0x00000004      /* Bulk List Filled */
#define OHCI_OCR                0x00000008      /* Ownership Change Request */
#define OHCI_SOC_MASK           0x00030000      /* Scheduling Overrun Count */
#define OHCI_INTERRUPT_STATUS   0x0c
#define OHCI_SO                 0x00000001      /* Scheduling Overrun */
#define OHCI_WDH                0x00000002      /* Writeback Done Head */
#define OHCI_SF                 0x00000004      /* Start of Frame */
#define OHCI_RD                 0x00000008      /* Resume Detected */
#define OHCI_UE                 0x00000010      /* Unrecoverable Error */
#define OHCI_FNO                0x00000020      /* Frame Number Overflow */
#define OHCI_RHSC               0x00000040      /* Root Hub Status Change */
#define OHCI_OC                 0x40000000      /* Ownership Change */
#define OHCI_MIE                0x80000000      /* Master Interrupt Enable */
#define OHCI_INTERRUPT_ENABLE   0x10
#define OHCI_INTERRUPT_DISABLE  0x14
#define OHCI_HCCA               0x18
#define OHCI_PERIOD_CURRENT_ED  0x1c
#define OHCI_CONTROL_HEAD_ED    0x20
#define OHCI_CONTROL_CURRENT_ED 0x24
#define OHCI_BULK_HEAD_ED       0x28
#define OHCI_BULK_CURRENT_ED    0x2c
#define OHCI_DONE_HEAD          0x30
#define OHCI_FM_INTERVAL        0x34
#define OHCI_GET_IVAL(s)        ((s) & 0x3fff)
#define OHCI_GET_FSMPS(s)       (((s) >> 16) & 0x7fff)
#define OHCI_FIT                0x80000000
#define OHCI_FM_REMAINING       0x38
#define OHCI_FM_NUMBER          0x3c
#define OHCI_PERIODIC_START     0x40
#define OHCI_LS_THRESHOLD       0x44
#define OHCI_RH_DESCRIPTOR_A    0x48
#define OHCI_GET_NDP(s)         ((s) & 0xff)
#define OHCI_PSM                0x0100  /* Power Switching Mode */
#define OHCI_NPS                0x0200  /* No Power Switching */
#define OHCI_DT                 0x0400  /* Device Type */
#define OHCI_OCPM               0x0800  /* Overcurrent Protection Mode */
#define OHCI_NOCP               0x1000  /* No Overcurrent Protection */
#define OHCI_GET_POTPGT(s)      ((s) >> 24)
#define OHCI_RH_DESCRIPTOR_B    0x4c
#define OHCI_RH_STATUS          0x50
#define OHCI_LPS                0x00000001      /* Local Power Status */
#define OHCI_OCI                0x00000002      /* OverCurrent Indicator */
#define OHCI_DRWE               0x00008000      /* Device Remote Wakeup Enable */
#define OHCI_LPSC               0x00010000      /* Local Power Status Change */
#define OHCI_CCIC               0x00020000      /* OverCurrent Indicator
                                                 * Change */
#define OHCI_CRWE               0x80000000      /* Clear Remote Wakeup Enable */
#define OHCI_RH_PORT_STATUS(n)  (0x50 + ((n)*4))        /* 1 based indexing */

#define OHCI_LES                (OHCI_PLE | OHCI_IE | OHCI_CLE | OHCI_BLE)
#define OHCI_ALL_INTRS          (OHCI_SO | OHCI_WDH | OHCI_SF |         \
                                OHCI_RD | OHCI_UE | OHCI_FNO |          \
                                OHCI_RHSC | OHCI_OC)
#define OHCI_NORMAL_INTRS       (OHCI_WDH |OHCI_RD | OHCI_UE | OHCI_RHSC)

#define OHCI_FSMPS(i)           (((i-210)*6/7) << 16)
#define OHCI_PERIODIC(i)        ((i)*9/10)


#define UPS_CURRENT_CONNECT_STATUS      0x0001
#define UPS_PORT_ENABLED                0x0002
#define UPS_SUSPEND                     0x0004
#define UPS_OVERCURRENT_INDICATOR       0x0008
#define UPS_RESET                       0x0010
#define UPS_PORT_L1                     0x0020  /* USB 2.0 only */
/* The link-state bits are valid for Super-Speed USB HUBs */
#define UPS_PORT_LINK_STATE_GET(x)      (((x) >> 5) & 0xF)
#define UPS_PORT_LINK_STATE_SET(x)      (((x) & 0xF) << 5)
#define UPS_PORT_LS_U0          0x00
#define UPS_PORT_LS_U1          0x01
#define UPS_PORT_LS_U2          0x02
#define UPS_PORT_LS_U3          0x03
#define UPS_PORT_LS_SS_DIS      0x04
#define UPS_PORT_LS_RX_DET      0x05
#define UPS_PORT_LS_SS_INA      0x06
#define UPS_PORT_LS_POLL        0x07
#define UPS_PORT_LS_RECOVER     0x08
#define UPS_PORT_LS_HOT_RST     0x09
#define UPS_PORT_LS_COMP_MODE   0x0A
#define UPS_PORT_LS_LOOPBACK    0x0B
#define UPS_PORT_LS_RESUME      0x0F
#define UPS_PORT_POWER                  0x0100
#define UPS_PORT_POWER_SS               0x0200  /* super-speed only */
#define UPS_LOW_SPEED                   0x0200
#define UPS_HIGH_SPEED                  0x0400
#define UPS_OTHER_SPEED                 0x0600  /* currently FreeBSD specific */
#define UPS_PORT_TEST                   0x0800
#define UPS_PORT_INDICATOR              0x1000
#define UPS_PORT_MODE_DEVICE            0x8000  /* currently FreeBSD specific */

//status change
#define UPS_C_CONNECT_STATUS            0x0001
#define UPS_C_PORT_ENABLED              0x0002
#define UPS_C_SUSPEND                   0x0004
#define UPS_C_OVERCURRENT_INDICATOR     0x0008
#define UPS_C_PORT_RESET                0x0010
#define UPS_C_PORT_L1                   0x0020  /* USB 2.0 only */
#define UPS_C_BH_PORT_RESET             0x0020  /* USB 3.0 only */
#define UPS_C_PORT_LINK_STATE           0x0040
#define UPS_C_PORT_CONFIG_ERROR         0x0080



DRIVER_FUNC unsigned int C_API_FUNC pci_get_device_cmd_flag	(unsigned short vendor_id ,unsigned short device_id);
DRIVER_FUNC int			 C_API_FUNC probe_device				(pci_device *device);
DRIVER_FUNC int			 C_API_FUNC init_driver				();


DRIVER_FUNC int			 C_API_FUNC usb_hubports				();
DRIVER_FUNC int			 C_API_FUNC usb_detect_dev				(int port);
DRIVER_FUNC int			 C_API_FUNC usb_portstatus				( unsigned int port, unsigned int enable);
DRIVER_FUNC int			 C_API_FUNC usb_ctrl_transfer			(unsigned int dev_addr,usb_speed_t speed,unsigned int max_frame_size,struct usb_td_t *first_td,struct usb_td_t *last_td);
DRIVER_FUNC int			 C_API_FUNC usb_intr_transfer			(struct usb_device *dev,unsigned int endp_id,unsigned int max_frame_size,struct usb_td_t *first_td,struct usb_td_t *last_td);


/*
typedef struct
{
*/
unsigned int			_config_regbase;
unsigned int			_config_bus_id;
unsigned int			_config_dev_id;
unsigned int			_config_bus_manager_id;
unsigned int			_config_num_ports;
unsigned int			_config_eintrs;
unsigned int			_config_noport;
mem_zone_ref			_config_ctrl_ed;
mem_zone_ref			_config_bulk_ed;
mem_zone_ref			_config_isoc_ed;
mem_zone_ref			_config_intr_ed;
mem_zone_ref			_config_hcca;
struct ohci_hcca		*_config_hcca_ptr;
unsigned int			*_config_data_test;
//unsigned char			_config_irq;
/*
}dev_config;

dev_config	_config											=	{0xFF};
*/

DRIVER_FUNC	root_hub_change_func_ptr	root_hub_change		=PTR_INVALID;
DRIVER_FUNC	transfer_done_func_ptr		transfer_done		=PTR_INVALID;


DRIVER_FUNC unsigned char  class_codes[]					= {	0x0C, 0x03,0x10,
																0x00,0x00,0x00};


unsigned int kernel_log_id									=	0xFFFFFFFF;


struct usb_interface *get_device_interface (struct usb_device *dev,unsigned int conf_idx,unsigned int iface_idx)
{
	struct usb_configuration		*config;
	struct usb_interface			*desc_if;

	if(conf_idx>=dev->device_desc.bNumConfigurations)return PTR_NULL;

	config					=	get_zone_ptr	(&dev->configs_ref,conf_idx*sizeof(struct usb_configuration));

	if(iface_idx>=config->conf_desc.bNumInterfaces)return PTR_NULL;

	desc_if					=	get_zone_ptr	(&config->interfaces_ref,iface_idx*sizeof (struct usb_interface));

	return desc_if;

}


void pci_write_io_8(unsigned short port,unsigned char value)
{
	out_8_c	(_config_regbase+port,value);
}
void pci_write_io_16(unsigned short port,unsigned short value)
{
	out_16_c	(_config_regbase+port,value);
}

void pci_write_io_32(unsigned short port,unsigned int value)
{
	//out_32_c	(_config_regbase+port,value);
	mem_ptr		ptr;
	ptr=uint_to_mem(_config_regbase+port);
	*((unsigned int *)(ptr))=value;
}


unsigned char pci_read_io_8(unsigned short port)
{
	return in_8_c	(_config_regbase+port);
}
unsigned short pci_read_io_16(unsigned short port)
{
	return in_16_c	(_config_regbase+port);
}

unsigned int pci_read_io_32(unsigned short port)
{
	mem_ptr		 ptr;
	unsigned int value;
	
	ptr		=	uint_to_mem(_config_regbase+port);

	value	=*	((unsigned int *)(ptr));
	return value;
}



OS_API_C_FUNC(unsigned int) pci_get_device_cmd_flag (unsigned short vendor_id ,unsigned short device_id)
{
/*
	// enable pci address access
	uint16 command = PCI_command_io | PCI_command_master | PCI_command_memory;
	command |= sPCIModule->read_pci_config(fPCIInfo->bus, fPCIInfo->device,
		fPCIInfo->function, PCI_command, 2);

	sPCIModule->write_pci_config(fPCIInfo->bus, fPCIInfo->device,
		fPCIInfo->function, PCI_command, 2, command);
*/
	return PCI_PCICMD_IOS | PCI_PCICMD_MSE | PCI_PCICMD_BME;
}




void ohci_dumpregs()
{
        struct ohci_hcca *hcca;


        kernel_log	(kernel_log_id," rev=");
		writeint	(pci_read_io_32(OHCI_REVISION),16);
		writestr	(" control=");
		writeint	(pci_read_io_32(OHCI_CONTROL),16);
		writestr	(" command=");
		writeint	(pci_read_io_32(OHCI_COMMAND_STATUS),16);
		writestr	("\n");
		
		

        kernel_log	(kernel_log_id,"intrstat=");
		writeint	(pci_read_io_32(OHCI_INTERRUPT_STATUS),16);
		writestr	(" intre=");
		writeint	(pci_read_io_32(OHCI_INTERRUPT_ENABLE),16);
		writestr	(" intrd=");
		writeint	(pci_read_io_32(OHCI_INTERRUPT_DISABLE),16);
		writestr	("\n");


        kernel_log	(kernel_log_id,"hcca=");
		writeint	(pci_read_io_32(OHCI_HCCA),16);
		writestr	(" percur=");
		writeint	(pci_read_io_32(OHCI_PERIOD_CURRENT_ED),16);
		writestr	(" ctrlhd=");
		writeint	(pci_read_io_32(OHCI_CONTROL_HEAD_ED),16);
		writestr	("\n");

     

        kernel_log	(kernel_log_id,"ctrlcur=");
		writeint	(pci_read_io_32(OHCI_CONTROL_CURRENT_ED),16);
		writestr	(" bulkhd=");
		writeint	(pci_read_io_32(OHCI_BULK_HEAD_ED),16);
		writestr	(" bulkcur=");
		writeint	(pci_read_io_32(OHCI_BULK_CURRENT_ED),16);
		writestr	("\n");


        kernel_log	(kernel_log_id,"done=");
		writeint	(pci_read_io_32(OHCI_DONE_HEAD),16);
		writestr	(" fmival=");
		writeint	(pci_read_io_32(OHCI_FM_INTERVAL),16);
		writestr	(" fmrem=");
		writeint	(pci_read_io_32(OHCI_FM_REMAINING),16);
		writestr	("\n");


        kernel_log	(kernel_log_id,"fmnum=");
		writeint	(pci_read_io_32(OHCI_FM_NUMBER),16);
		writestr	(" perst=");
		writeint	(pci_read_io_32(OHCI_PERIODIC_START),16);
		writestr	(" lsthrs=");
		writeint	(pci_read_io_32(OHCI_LS_THRESHOLD),16);
		writestr	("\n");



        kernel_log	(kernel_log_id,"desca=");
		writeint	(pci_read_io_32(OHCI_RH_DESCRIPTOR_A),16);
		writestr	(" descb=");
		writeint	(pci_read_io_32(OHCI_RH_DESCRIPTOR_B),16);
		writestr	(" stat=");
		writeint	(pci_read_io_32(OHCI_RH_STATUS),16);
		writestr	("\n");



        kernel_log		(kernel_log_id,"port1=");
		writeint		(pci_read_io_32(OHCI_RH_PORT_STATUS(1)),16);
		writestr		(" port2=");
		writeint		(pci_read_io_32(OHCI_RH_PORT_STATUS(2)),16);
		writestr		("\n");

		hcca =_config_hcca_ptr;


        kernel_log	(kernel_log_id,"HCCA: frame_number=");
		writeint	(hcca->hcca_frame_number,16);
		writestr	(" done_head=");
		writeint	(hcca->hcca_done_head,16);
		writestr	("\n");

}


OS_INT_C_FUNC(unsigned int) _interupt_handler(void *p)
{
  unsigned int status;
  unsigned int done;
  unsigned int n;

    
  done		= le32toh(_config_hcca_ptr->hcca_done_head);
  if(done!=0)
  {
   

	status = 0;
	if (done & ~OHCI_DONE_INTRS) 
	{
         status |= OHCI_WDH;
	}
	if (done & OHCI_DONE_INTRS) 
	{
         status |= pci_read_io_32( OHCI_INTERRUPT_STATUS);
	}



	if(transfer_done!=PTR_INVALID)
	{
		 usb_td_ptr_t		td;
		 struct td_chain_t	chain;
		 unsigned int	td_next,n_td;

		td	=	uint_to_mem(done&(~OHCI_DONE_INTRS));

		chain.n_td	=	0;

		while(td!=PTR_NULL)
		{
			/*
			kernel_log		(kernel_log_id,"log chain ");
			writeptr		(td);
			writestr		(" ");
			writeptr		(td->link_phy);
			writestr		("\n");
			*/
			

			chain.td_chain[chain.n_td]=td;
			chain.n_td++;
			td_next			=	mem_to_uint(td->link_phy)&(~OHCI_DONE_INTRS);
			if(td_next==mem_to_uint(td))break;
			td				=	uint_to_mem		(td_next);
			
		}

		//transfer_done	(chain.td_chain[0]);

		
		n_td=0;
		while(n_td<chain.n_td)
		{
			transfer_done	(chain.td_chain[n_td]);
			n_td++;
		}
		
	}

	_config_hcca_ptr->hcca_done_head=0;
 }
 else
 {
	  status  = pci_read_io_32(OHCI_INTERRUPT_STATUS) & ~OHCI_WDH;

 }



  if (status & OHCI_RHSC) 
  {
		unsigned int	rh_status,rh_change;
		unsigned int	portinfo;
		unsigned int	num_rh_ports;

		portinfo			= pci_read_io_32 (OHCI_RH_DESCRIPTOR_A);
		num_rh_ports		= portinfo&0xFF;	


		rh_status			= pci_read_io_32(OHCI_RH_DESCRIPTOR_A);
		rh_change			= (rh_status>>16)&0xFFFF;

		for (n = 0; n < num_rh_ports; n++)
		{
			unsigned int port_reg,port_status,port_change;

			port_reg	=	((n+1)*4);
			port_status =	pci_read_io_32 (OHCI_RH_STATUS +port_reg);
			port_change	=	port_status>>16;

			if(port_change)
			{
				if(root_hub_change!=PTR_INVALID)
					root_hub_change(n,port_status);
			}
		}
  }
    status &= ~OHCI_MIE;
	if (status == 0) 
	{
		 return 0;
	}
	pci_write_io_32(OHCI_INTERRUPT_STATUS, status);     /* Acknowledge */
  
	return 1;
}
int controller_reset()
{
	unsigned int	ctl;
	unsigned int	i;

	ctl = pci_read_io_32(OHCI_CONTROL);

    if (ctl & OHCI_IR) {
		/* SMM active, request change */
        kernel_log			(kernel_log_id,"SMM active, request owner change\n");

        pci_write_io_32(OHCI_COMMAND_STATUS, OHCI_OCR);
        for (i = 0; (i < 100) && (ctl & OHCI_IR); i++) 
		{
			snooze (100000);
            ctl = pci_read_io_32(OHCI_CONTROL);
        }
        if (ctl & OHCI_IR) 
		{

              kernel_log			(kernel_log_id,"SMM does not respond, resetting\n");
              pci_write_io_32(OHCI_CONTROL, OHCI_HCFS_RESET);
              goto reset;
         }
    } 
	else 
	{

			kernel_log			(kernel_log_id,"cold started\n");
			reset:
			/* controller was cold started */
			snooze (100000);
    }


    kernel_log		 (kernel_log_id," resetting\n");
	pci_write_io_32	 (OHCI_CONTROL, OHCI_HCFS_RESET);
	snooze			 (100000);

	return 1;

}

int controller_init()
{
	unsigned int	ctl;
	unsigned int	hcr;
	unsigned int	ival;
	unsigned int	per;
	unsigned int	desca;
	unsigned int	fm;
	unsigned int			physaddr;
	int				i;

   /* we now own the host controller and the bus has been reset */
	ival = OHCI_GET_IVAL(pci_read_io_32(OHCI_FM_INTERVAL));
	

	kernel_log		 (kernel_log_id,"interval:");
	writeint		 (ival,10);
	writestr		 ("\n");


	pci_write_io_32(OHCI_COMMAND_STATUS, OHCI_HCR);     /* Reset HC */
    /* nominal time for a reset is 10 us */
   for (i = 0; i < 10; i++) 
   {
       snooze (100000);
       hcr = pci_read_io_32(OHCI_COMMAND_STATUS) & OHCI_HCR;
       if (!hcr) 
	   {
			break;
	   }
	}
	if (hcr) 
	{

          kernel_log			(kernel_log_id,"reset timeout\n");
          return (0);
	}

	
    pci_write_io_32	(OHCI_HCCA				, mem_to_uint(_config_hcca_ptr));

	physaddr	=	mem_to_uint(get_zone_ptr(&_config_ctrl_ed,0));
	pci_write_io_32	(OHCI_CONTROL_HEAD_ED	, physaddr);

	physaddr	=	mem_to_uint(get_zone_ptr(&_config_bulk_ed,0));
	pci_write_io_32	(OHCI_BULK_HEAD_ED		, physaddr);

	/*setup_interupt_c	(_config_irq, interupt_handler,0);*/
    /* disable all interrupts and then switch on all desired interrupts */
	pci_write_io_32(OHCI_INTERRUPT_DISABLE, OHCI_ALL_INTRS|OHCI_MIE);
	pci_write_io_32(OHCI_INTERRUPT_ENABLE,  _config_eintrs|OHCI_MIE);

	
	/* switch on desired functional features */
	ctl = pci_read_io_32(OHCI_CONTROL);
	
	ctl &= ~(OHCI_CBSR_MASK | OHCI_LES | OHCI_HCFS_MASK | OHCI_IR);
	//ctl |= OHCI_PLE | OHCI_IE | OHCI_CLE | OHCI_BLE |	OHCI_RATIO_1_4 | OHCI_HCFS_OPERATIONAL;
	ctl	 |= (OHCI_CLE | OHCI_RATIO_1_4 | OHCI_HCFS_OPERATIONAL);

	/* And finally start it! */
	pci_write_io_32(OHCI_CONTROL, ctl);
	/*
	 * The controller is now OPERATIONAL.  Set a some final
	 * registers that should be set earlier, but that the
	 * controller ignores when in the SUSPEND state.
	 */

	
	fm = (pci_read_io_32(OHCI_FM_INTERVAL) & OHCI_FIT) ^ OHCI_FIT;
	fm |= OHCI_FSMPS(ival) | ival;
	pci_write_io_32(OHCI_FM_INTERVAL, fm);

	per = OHCI_PERIODIC(ival);      // 90% periodic 
	pci_write_io_32	(OHCI_PERIODIC_START, per);
	

	/* Fiddle the No OverCurrent Protection bit to avoid chip bug. */
	desca = pci_read_io_32(OHCI_RH_DESCRIPTOR_A);
	pci_write_io_32	(OHCI_RH_DESCRIPTOR_A, desca | OHCI_NOCP);
	pci_write_io_32	(OHCI_RH_STATUS, OHCI_LPSC); /* Enable port power */
	snooze			(100000);
	pci_write_io_32	(OHCI_RH_DESCRIPTOR_A, desca);

	ohci_dumpregs();
	writestr("\n");

	/*
	 * The AMD756 requires a delay before re-reading the register,
	 * otherwise it will occasionally report 0 ports.
	 */
	_config_noport = 0;
	for (i = 0; (i < 10) && (_config_noport == 0); i++) {
		snooze			 (100000);
		_config_noport = (pci_read_io_32 (OHCI_RH_DESCRIPTOR_A)& 0xFF);
	}
	
	kernel_log			(kernel_log_id,"no port : ");
	writeint			(_config_noport,16);
	writestr			("\n");


	return 1;
}

void ohci_init_ed(mem_zone_ref *ed_mem)
{
	 ohci_ed_t *ed;


	ed_mem->zone			=PTR_NULL;
    allocate_new_zone		(0,sizeof(ohci_ed_t),ed_mem);

    ed						= get_zone_ptr(ed_mem,0);
    ed->ed_flags			= htole32(OHCI_ED_SKIP);
	ed->ed_tailp			= mem_to_uint(PTR_NULL);
	ed->ed_headp			= mem_to_uint(PTR_NULL);
	ed->ed_next				= mem_to_uint(PTR_NULL);
}     

OS_API_C_FUNC(int) probe_device	(pci_device *pci_dev_ptr)
{
	struct ohci_hcca *hcca;
	unsigned int	fRegisterBase;
	unsigned int	base_addr_reg;
	unsigned int	i;
	ohci_ed_t		*intr_eds;
	mem_ptr			c_ptr_2;

	kernel_log_id		=	get_new_kern_log_id		("usb_ohci: ",0x03);

	c_ptr_2				= mem_add(&pci_dev_ptr->config,0x10);
	base_addr_reg		= *((unsigned int *)(c_ptr_2));
	fRegisterBase		=	base_addr_reg;
	
	kernel_log			(kernel_log_id,"iospace offset: ");
	writeint			(fRegisterBase,16);
	writestr			("\n");

	if (fRegisterBase == 0)return 0;

	/*
	pci_dev_ptr->irq	=	pci_dev_ptr->config.int_line;
	_config_irq			=	pci_dev_ptr->irq;
	*/
	_config_regbase		=	fRegisterBase&PCI_address_io_mask;
	
	if(controller_reset	()==0)
	{
		kernel_log		(kernel_log_id,"unable to reset the controller \n");
		return 0;
	}

	ohci_init_ed(&_config_ctrl_ed);
	ohci_init_ed(&_config_bulk_ed);
	ohci_init_ed(&_config_isoc_ed);


	_config_intr_ed.zone	=PTR_NULL;
	 allocate_new_zone		(0,OHCI_NO_EDS*sizeof(ohci_ed_t),&_config_intr_ed);

	

	 intr_eds	=	get_zone_ptr(&_config_intr_ed,0);

     for (i = 0; i != OHCI_NO_EDS; i++) 
	 {
		intr_eds->ed_flags			= htole32(OHCI_ED_SKIP);
		intr_eds->ed_tailp			= mem_to_uint(PTR_NULL);
		intr_eds->ed_headp			= mem_to_uint(PTR_NULL);
		intr_eds->ed_next			= mem_to_uint(PTR_NULL);
		intr_eds++;
	 }


	_config_hcca.zone		=PTR_NULL;
	allocate_new_zone		(0,512,&_config_hcca);

	hcca				=	uint_to_mem(mem_to_uint(get_zone_ptr(&_config_hcca,0))&0xFFFFFF00);
	hcca			   +=  256;
	_config_hcca_ptr	=	hcca;

	_config_bus_id	=	pci_dev_ptr->bus_id;
	_config_dev_id	=	pci_dev_ptr->dev_id;

	return 1;
	
}


OS_API_C_FUNC(int) usb_portstatus ( unsigned int port, unsigned int enable)
{
   unsigned int status;
   unsigned int port_reg;

   port_reg	=	((port+1)*4);

   /* Reset the port.  */
   status = pci_read_io_32 (OHCI_RH_STATUS + port_reg);
   status |= (1 << 4); /* XXX: Magic.  */
   pci_write_io_32 (OHCI_RH_STATUS + port_reg, status);
   snooze (100000);

   /* End the reset signaling.  */
   status = pci_read_io_32 (OHCI_RH_STATUS + port_reg);
   status |= (1 << 20); /* XXX: Magic.  */
   pci_write_io_32 (OHCI_RH_STATUS+ port_reg, status);
   snooze (100000);

   /* Enable the port.  */
   status = pci_read_io_32 (OHCI_RH_STATUS + port_reg);
   status |= (enable << 1); /* XXX: Magic.  */
   pci_write_io_32 (OHCI_RH_STATUS + port_reg, status);

   snooze (100000);

   status = pci_read_io_32 (OHCI_RH_STATUS + port_reg);

 
   kernel_log		(kernel_log_id,"portstatus=");
   writeint			(status,2);
   writestr			("\n");
   
   return 1;
}




OS_API_C_FUNC(int) usb_detect_dev (int port)
{
   unsigned int status;
   unsigned int port_reg;

   port_reg	=	((port+1)*4);
   status	=	pci_read_io_32 (OHCI_RH_STATUS +port_reg);
   
   
   kernel_log		(kernel_log_id,"detect_dev status=");
   writeint			(status,2);
   writestr			("\n");

   if (! (status & 1))
     return USB_SPEED_NONE;
   else if (status & (1 << 9))
     return USB_SPEED_LOW;
   else
     return USB_SPEED_FULL;
}


OS_API_C_FUNC(int) usb_hubports ()
{
  unsigned int portinfo;

  portinfo = pci_read_io_32 (OHCI_RH_DESCRIPTOR_A);

  
  kernel_log		(kernel_log_id,"root hub ports=");
  writeint			(portinfo & 0xFF,10);
  writestr			("\n");

  return portinfo & 0xFF;
}

OS_API_C_FUNC(int) usb_intr_transfer (struct usb_device *dev,unsigned int endp_id,unsigned int max_frame_size,struct usb_td_t *first_td,struct usb_td_t *last_td)
{
	struct ohci_hcca			*hcca;
	struct ohci_ed			*intr_ed;
	struct ohci_ed			*intr_eds;
	unsigned int		ed_flags,control,status;
	unsigned int	ed_idx;
	unsigned int		i;

	status				= pci_read_io_32(OHCI_COMMAND_STATUS);
	status				&= ~(OHCI_CLF|OHCI_BLF);
	pci_write_io_32		(OHCI_COMMAND_STATUS, status);

	control				= pci_read_io_32(OHCI_CONTROL);
	control				&= ~(OHCI_CLE|OHCI_PLE);
	pci_write_io_32		(OHCI_CONTROL, control);
	
	ed_flags 				= 0;
	ed_flags 				= write_bits(ed_flags ,dev->addr,0,7);		// device address
	ed_flags 				= write_bits(ed_flags ,endp_id	,7,4);		// number of endpoint


	ed_flags 				= write_bits(ed_flags ,0	,11,2);				// transfer direction

	if(dev->speed==USB_SPEED_LOW)
		ed_flags 				= write_bits(ed_flags ,1,13,1);				// 1 = lowspeed
	else
		ed_flags 				= write_bits(ed_flags ,0,13,1);				// 0 = fullspeed

	ed_flags 				= write_bits(ed_flags ,0,14,1);					// HC skips to the next ED w/o attempting access to the TD queue
	ed_flags 				= write_bits(ed_flags ,0,15,1);					// bit with isochronous transfers
	ed_flags 				= write_bits(ed_flags ,max_frame_size,16,11);	// maximum packet size
	ed_flags 				= write_bits(ed_flags ,0,27,5);					// available

	hcca					=	_config_hcca_ptr;


	ed_idx=dev->addr-1;
	
	for (i = 0; i != OHCI_NO_INTRS; i++) 
	{
		  intr_eds				=	get_zone_ptr(&_config_intr_ed,i*sizeof(ohci_ed_t));
		  intr_eds->ed_flags	=	write_bits(0 ,1,14,1);

		  _config_hcca_ptr->hcca_interrupt_table[i] = mem_to_uint(intr_eds);

		  if((i%8)==ed_idx)
		  {
			  intr_eds->ed_next	=	mem_to_uint(get_zone_ptr(&_config_intr_ed,(OHCI_NO_INTRS+ed_idx)*sizeof(ohci_ed_t)));
		  }

	}
	
			
	

/*
	for (i = 0; i != OHCI_NO_INTRS; i++) 
	{
		  intr_eds				=	get_zone_ptr(&_config_intr_ed,i*sizeof(ohci_ed_t));
		  intr_eds->ed_flags	=	write_bits(0 ,1,14,1);

		  _config_hcca_ptr->hcca_interrupt_table[i] = mem_to_uint(intr_eds);

		  for ( j= 0; j != OHCI_NO_INTRS; j++) 
		  {
			  intr_eds->ed_next	=	mem_to_uint(get_zone_ptr(&_config_intr_ed,(OHCI_NO_INTRS+j)*sizeof(ohci_ed_t)));
			  intr_eds			=	uint_to_mem(intr_eds->ed_next);
		  }
		  intr_eds->ed_next=PTR_NULL;
			
	}

	intr_ed	=	get_zone_ptr(&_config_intr_ed,OHCI_NO_INTRS*sizeof(ohci_ed_t));
    // Wait until the transfer is completed or STALLs.  
	while (1)
    {
		if( (intr_ed->ed_headp&0xf)!=0)intr_ed->ed_headp=PTR_NULL;
		if(intr_ed->ed_headp==PTR_NULL)break;
		if(intr_ed->ed_tailp==PTR_NULL)break;
		intr_ed++;
    }
	*/
/*
	kernel_log(kernel_log_id,"new int transfer ");
	writeptr(intr_ed);
	writestr(" ");
	writeptr(intr_ed->ed_headp);
	writestr(" ");
	writeptr(intr_ed->ed_tailp);
	writestr("\n");
*/
	intr_ed					=	get_zone_ptr(&_config_intr_ed,(OHCI_NO_INTRS+ed_idx)*sizeof(ohci_ed_t));//get_zone_ptr(&_config_intr_ed[OHCI_NO_INTRS+1],0);
	intr_ed->ed_headp		=	mem_to_uint	(first_td);
	intr_ed->ed_tailp		=	mem_to_uint	(last_td);
	intr_ed->ed_flags		=	htole32		(ed_flags);
	intr_ed->ed_next		=	mem_to_uint(PTR_NULL);

	/*
	for (i = 0; i != OHCI_NO_INTRS; i++) 
	{
		((struct ohci_ed *)(_config_hcca_ptr->hcca_interrupt_table[i]))->ed_next = mem_to_uint(intr_ed);

	
		 if(i&1)
		 {
			 ((struct ohci_ed *)(_config_hcca_ptr->hcca_interrupt_table[i]))->ed_next = mem_to_uint(intr_ed);
		 }
		 
	}
	*/

	control				= pci_read_io_32(OHCI_CONTROL);
	control				|= OHCI_PLE;
	pci_write_io_32		(OHCI_CONTROL, control);
	pci_write_io_32		(OHCI_INTERRUPT_ENABLE,  _config_eintrs|OHCI_PLE|OHCI_MIE);

	return 1;
}
OS_API_C_FUNC(int) usb_ctrl_transfer			(unsigned int dev_addr,usb_speed_t speed,unsigned int max_frame_size,struct usb_td_t *first_td,struct usb_td_t *last_td)
{
	struct ohci_hcca	*hcca;
	struct ohci_ed		*ctrl_ed;
	unsigned int		ed_flags,control,status;


	/*
	unsigned int		max_frame_size;
	max_frame_size			=	64;
	if(dev->device_desc.bMaxPacketSize!=0)
		max_frame_size			=	dev->device_desc.bMaxPacketSize;
	*/

	ed_flags 				= 0;
	ed_flags 				= write_bits(ed_flags ,dev_addr,0,7);		// device address
	ed_flags 				= write_bits(ed_flags ,0,7,4);				// number of endpoint

	ed_flags 				= write_bits(ed_flags ,0	,11,2);				// transfer direction

	if(speed==USB_SPEED_LOW)
		ed_flags 			= write_bits(ed_flags ,1,13,1);				// 1 = lowspeed
	else
		ed_flags 			= write_bits(ed_flags ,0,13,1);				// 0 = fullspeed

	ed_flags 				= write_bits(ed_flags ,0,14,1);					// HC skips to the next ED w/o attempting access to the TD queue
	ed_flags 				= write_bits(ed_flags ,0,15,1);					// bit with isochronous transfers
	ed_flags 				= write_bits(ed_flags ,max_frame_size,16,11);	// maximum packet size
	ed_flags 				= write_bits(ed_flags ,0,27,5);					// available


	ctrl_ed					=	get_zone_ptr(&_config_ctrl_ed,0);
	hcca					=	_config_hcca_ptr;

	

	control				= pci_read_io_32(OHCI_CONTROL);
	control				&= ~(OHCI_CLE|OHCI_PLE);
	pci_write_io_32		(OHCI_CONTROL, control);

	status				= pci_read_io_32(OHCI_COMMAND_STATUS);
	status				&= ~(OHCI_CLF|OHCI_BLF);
	pci_write_io_32		(OHCI_COMMAND_STATUS, status);

	ctrl_ed->ed_headp	=	mem_to_uint(first_td);
	ctrl_ed->ed_tailp	=	mem_to_uint(last_td);
	ctrl_ed->ed_flags	=	htole32(ed_flags);
	ctrl_ed->ed_next	=	mem_to_uint(PTR_NULL);

	pci_write_io_32		(OHCI_CONTROL_HEAD_ED	, mem_to_uint(ctrl_ed));
	
	control				= pci_read_io_32(OHCI_CONTROL);
	control				|= OHCI_CLE;
	pci_write_io_32		(OHCI_CONTROL, control);

	status				= pci_read_io_32(OHCI_COMMAND_STATUS);
	status				|= OHCI_CLF;
	pci_write_io_32		(OHCI_COMMAND_STATUS, status);


    // Wait until the transfer is completed or STALLs.  
	while ((ctrl_ed->ed_headp & ~0xf) != (ctrl_ed->ed_tailp & ~0xf))
    {
		//writeint(hcca->hcca_frame_number,16);
		//writestr("\n");
		snooze(1000);
		// Detected a STALL.  
		if (ctrl_ed->ed_headp & 1)
			break;
    }

	control				= pci_read_io_32(OHCI_CONTROL);
	control				&= ~OHCI_CLE;
	pci_write_io_32		(OHCI_CONTROL, control);

	status				= pci_read_io_32(OHCI_COMMAND_STATUS);
	status				&= ~OHCI_CLF;
	pci_write_io_32		(OHCI_COMMAND_STATUS, status);

	if(ctrl_ed->ed_headp & 1)
	{
		
		kernel_log		(kernel_log_id,"ohci control transfer stall ");
		writeint		(ed_flags,16);
		writestr		("\n");
		return 0;
	}

	return 1;


}



OS_API_C_FUNC(int)	init_driver		()
{
	unsigned int usb_bus_drv_id;
	unsigned int i;
	ohci_ed_t	*ed;
	

	
	/*
	* Fill HCCA interrupt table.  The bit reversal is to get
	* the tree set up properly to spread the interrupts.
	*/
	ed	=	get_zone_ptr(&_config_intr_ed,0);
	for (i = 0; i != OHCI_NO_INTRS; i++) 
	{
        _config_hcca_ptr->hcca_interrupt_table[i] = mem_to_uint(ed);
		ed->ed_next	= mem_to_uint(PTR_NULL);
		ed++;
	}
	_config_eintrs = OHCI_NORMAL_INTRS;

	

	if(!controller_init())
	{
		
		kernel_log		(kernel_log_id,"unable to initialize the controller \n");
		return 0;
	}
		

	usb_bus_drv_id		=	bus_manager_load_bus_driver	("ramdisk","/usb_bus.tpo");

	if(usb_bus_drv_id==0xFFFFFFFF)
	{
		kernel_log		(kernel_log_id,"could not load usb bus driver \n");
		return 0;
	}

	
	kernel_log		(kernel_log_id,"usb bus driver loaded ");
	writeint		(usb_bus_drv_id,16);
	writestr		("\n");

	_config_bus_manager_id			=	usb_bus_drv_id;

	bus_manager_set_bus_driver_ctrler	(_config_bus_manager_id,_config_bus_id,_config_dev_id);
	
	kernel_log		(kernel_log_id,"bus_manager_set_bus_driver_ctrler ");
	writeint		(_config_bus_id,16);
	writestr		(kernel_log_id," ");
	writeint		(_config_dev_id,16);
	writestr		("\n");

	return 1;
}

