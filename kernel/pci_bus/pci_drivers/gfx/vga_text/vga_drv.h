typedef struct 
{

   unsigned char			bModes[7];
   /*
   +0      1  bModes1      Video modes supported:
                             bit 0=mode 00H???bit 7=mode 07H
   +1      1  bModes2        bit 0=mode 08H???bit 7=mode 0FH
   +2      1  bModes3        bit 0=mode 10H???bit 3=mode 13H
   +3      4  res          (reserved could contain modes up to 56)
   */
   unsigned char	bScanLnsFlgs;	// scan lines available in text modes:bit 0=200, bit 1=350, bit 2=400;
   unsigned char	bFontBlks;		// font blocks available in text modes (4 for EGA, 8 for VGA)
   unsigned char	bMaxFonts;		// max active font blocks in text mode (2 for EGA and VGA)
   unsigned char	rMiscFlags1;	//miscellaneous capabilities:
/*   
0: 01H all modes on all displays
1: 02H gray-scale summing (INT 10H 101bH)
2: 04H char set loading (INT 10H 11H)
3: 08H default palette loading
4: 10H cursor emulation
5: 20H 64-color palette (INT 10H 10)
6: 40H video DAC palette loading
*/
   unsigned char	rMiscFlags2;//more miscellaneous capabilities:
/*
   0: 01H light pen support (INT 10H 04H)
   1: 02H save/restore state (INT 10H 1cH)
   2: 04H blink/bold mapping (INT 10H 1003H)
   3: 08H DCC control (INT 10H 1aH)
*/   
   unsigned short	res1;
   unsigned char	rSaveCaps;//save area capabilities
   /*0: 01H  multiple 512-character set
     1: 02H  dynamic save area
	 2: 04H  text font override
	 3: 08H  graphics font override
	 4: 10H  palette override
	*/
   unsigned char		res2;//          (reserved)
}VgaStaticFnalityRec;


typedef struct
{
	mem_ptr				   pfrFnality;	  //address of a VgaStaticFnalityRec.  This is a
										  //static (unchanging) table in ROM that describes
										  //your VGA system's capabilities.
   unsigned char			bCurMode;	  //current video mode
   unsigned short			screen_cols;  //screen columns (e.g., 80)
   unsigned short			wCrtBufLen;	  //size of displayed portion of video buffer
   unsigned short			pCrtPgStart;  //starting address (offset) in regen buffer
   unsigned char			awCrsrPos[16];//cursor positions for pages 0-7 (row,column,row,column,...)
   unsigned short			wCrsrType;    //cursor shape (start/end) as CX in INT 10H 01H
   unsigned char			bCurPg;		  // current active video page
   unsigned short			wCrtcPort;	  //CGA/MDA CRT ctrlr port addr (3b?=mono; 3d?=color)
   unsigned char			bModeSetReg;  //current setting of 3?8 reg (CGA Mode Select reg)
   unsigned char			bClrSetReg;   //current setting of 3?9 reg (CGA Color Select reg)
   unsigned char			bCrtRows;     //character rows on screen
   unsigned short			bCrtPoints;   //height of character matrix (scan lines per character cell; e.g., 14 or 8)
   unsigned char			bCurDcc;      //active Display Combination Code (see INT 10H 1aH)
   unsigned char			bAltDcc;      //alternate DCC (00 if only one video system)
   unsigned short			wMaxClrs;     //count of display pages in current video mode
   unsigned char			bMaxPgs;      //colors in current video mode (0000=monochrome)
   unsigned char			bScanLnsCode;  //bScanLnsCode scan lines in current video mode:  0=200, 1=350, 2=400, 3=480
   unsigned char			bFont1;		  //primary font block (based on INT 10H 1103H)
   unsigned char			bFont2;		  //secondary font block
   unsigned char			rMiscFlags;   //miscellaneous state information
   /*bit  mask
                   0: 01H  all modes on all displays active
                   1: 02H  gray-scale summing is active
                   2: 04H  monochrome display attached
                   3: 08H  default palette loading disabled
                   4: 10H  cursor emulation active
	*/
   unsigned char	res[3]	;		//	(reserved)
   unsigned char	bVidMemCode;	//	video memory available: 0=64K, 1=128K, 2=192K, 3=256K
   unsigned char	rSaveStatus;	//status of EgaSavePtrRec information
   /*
	0: 01H  512-character set active
	1: 02H  dynamic save area active
	2: 04H  text font override active
	3: 08H  graphics font override active
	4: 10H  palette override active
	*/
	//+33H    0dH res          (reserved)
    // 64               size of a VgaDynamicStateRec
}VgaDynamicStateRec;


typedef struct
{
   unsigned short BytesPerScanLine; //bytes per scan line

	// formerly optional information (now mandatory)

	unsigned short       XResolution;		 //horizontal resolution
	unsigned short       YResolution;		 //vertical resolution
	unsigned char        XCharSize;			 //character cell width
	unsigned char        YCharSize;			 //character cell height
	unsigned char        NumberOfPlanes;	 //number of memory planes
	unsigned char        BitsPerPixel;		 //bits per pixel
    unsigned int		 PhysBasePtr;			//physical address for flat frame buffer
	unsigned int		 PhysBasePtr_font;			//physical address for flat frame buffer

	
}video_mode_info_t;



typedef struct
{
	unsigned int	surf_id;
	unsigned int	surf_type;
	unsigned int	pix_type;
	unsigned int	d_type;
	unsigned int	pix_width;
	unsigned int	pix_height;

	unsigned int	pix_stride;

	unsigned int	width;
	unsigned int	height;
	unsigned int	scan_line;
	unsigned int	scan_line_shift;

	unsigned int	scan_shift;

	struct VBitmap				OrigImage;
	struct VBitmap				DrawArea;
	struct ResampleFilterData	fd;
	mem_zone_ref				pix_buff;
}surface_2D_t;



video_mode_info_t	*get_video_mode				();
int					init_render_engine			();
int					setup_render_engine			();