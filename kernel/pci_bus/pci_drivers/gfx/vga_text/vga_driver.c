#define DRIVER_FUNC		C_EXPORT

#include <std_def.h>
#include <std_mem.h>
#include "kern.h"
#include "sys_pci.h"
#include "mem_base.h"
#include "lib_c.h"
#include "sys/mem_stream.h"
#include "sys/tpo_mod.h"
#include "sys/pci_device.h"
#include "../../../../../graphic_base/filters/filters.h"
#include "vga_drv.h"

DRIVER_FUNC unsigned char  class_codes[]  = {0x03, 0x00,0x00,
											 0x00,0x00,0x00};

DRIVER_FUNC unsigned int C_API_FUNC  pci_get_device_cmd_flag	(unsigned short vendor_id ,unsigned short device_id);
DRIVER_FUNC int			 C_API_FUNC  probe_device				(pci_device *device);
DRIVER_FUNC int			 C_API_FUNC  init_driver				();
DRIVER_FUNC int			 C_API_FUNC  get_format					(unsigned int idx,unsigned int *fmt);
DRIVER_FUNC int			 C_API_FUNC  set_format					(unsigned int idx);

unsigned int	kernel_log_id	=	0xFFFFFFFF;

typedef struct
{
	int code;

	int txt_x_res;
	int txt_y_res;
	int bit_pix;
	int txt;
}vga_res_t;

typedef struct
{
	unsigned int		pci_dev_id;
	unsigned int		pci_bus_id;
	unsigned char		device_info_ptr[256];
	unsigned short		TotalMemory ;
	unsigned int		current_mode;
	mem_zone_ref		modes;
	mem_zone_ref		modes_idx;
	unsigned int		n_modes;
}device_config;

device_config			c										=	{0xff};
device_config			*_config								=	&c;

vga_res_t				vga_modes[]=							{ {0x0,40,25,4,1},
                                                                  {0x1,40,25,4,1},
                                                                  {0x2,80,25,4,1},
                                                                  {0x3,80,25,4,1},
																  {0x4,320,200,2,0},
																  {0x5,320,200,2,0},
																  {0x6,320,200,1,0},
																  {0x7,80,25,1,1},
																  {0xD,320,200,4,0},
																  {0xE,640,200,4,0},
																  {0xF,640,350,1,0},
																  {0x10,640,350,4,0},
																  {0x11,640,480,1,0},
																  {0x12,640,480,4,0},
																  {0x13,320,200,8,0},
																  {0xFF,0xFF,0xFF,0xFF,0xFF},
																};
/*
0 40�25 16-color text (color burst off) CGA,EGA,MCG
A,VGA
1 40�25 16-color text CGA,EGA,MCGA,VGA
2 80�25 16-color text (color burst off) CGA,EGA,MCG
A,VGA
3 80�25 16-color text CGA,EGA,MCGA,VGA

4 320�200 4 color
5 320�200 4 color (color burst off)
6 640�200 2 color

7 80�25 Monochrome text MDA,EGA,VGA 

EGA Graphics
D 320�200 16 color
E 640�200 16 color
F 640�350 Monochrome
10 640�350 16 color
VGA Graphics
11 640�480 2 color
12 640�480 16 color
13 320�200 256 color 


*/
OS_API_C_FUNC(int) probe_device	 (pci_device *device)
{	
	unsigned short  *video_mode_idxs_ptr;
	VgaDynamicStateRec	*static_state_ptr;
	VgaStaticFnalityRec	*StaticFnality;
	unsigned int		phys_ptr;
	unsigned int		fin_ptr;
	unsigned char		mode_sup[56];
	unsigned char		n_modes;
	unsigned int		fin_seg,fin_ofs;
	unsigned int		n,bit_ofs,byte_ofs;

	kernel_log_id	=	get_new_kern_log_id("vga_drv:",0x0C);

	_config->modes.zone=PTR_NULL;
	_config->current_mode=0xFFFFFFFF;


	
	_config->pci_bus_id=device->bus_id;
	_config->pci_dev_id=device->dev_id;

	bios_get_vga_info_c			();

	static_state_ptr	=	(VgaDynamicStateRec	*)bios_get_vesa_info_ptr_c	();

	fin_ptr				=	mem_to_uint(static_state_ptr->pfrFnality);
	fin_seg				=	(fin_ptr>>16);
	fin_ofs				=	(fin_ptr&0xFFFF);
	phys_ptr			=	fin_seg*16+fin_ofs;
	StaticFnality		=	uint_to_mem(phys_ptr);


	kernel_log	(kernel_log_id,"vga init ");
	writeint	(static_state_ptr->bCurMode,10);
	writestr	(" ");
	writeint	(static_state_ptr->screen_cols,10);
	writestr	(" ");
	writeint	(static_state_ptr->bCrtRows,10);
	writestr	(" ");
	writeint	(static_state_ptr->bScanLnsCode,10);
	writestr	(" ");
	writeptr	(static_state_ptr->pfrFnality);
	writestr	(" ");
	writeptr	(StaticFnality);
	writestr	(" ");
	writeint	(StaticFnality->bScanLnsFlgs,10);
	writestr	(" ");
	writeint	(StaticFnality->bMaxFonts,10);
	writestr	("\n");

	n_modes=0;
	n		=0;
	bit_ofs	=0;
	byte_ofs=0;
	while(n<56)
	{
		unsigned char mode_byte;
		
		mode_byte	=	StaticFnality->bModes[byte_ofs];
		
		if((mode_byte>>bit_ofs)&1)
		{
			mode_sup[n_modes]	=	n;
			n_modes++;
		}
		bit_ofs++;

		if(bit_ofs>=8)
		{
			bit_ofs=0;
			byte_ofs++;
		}

		n++;
	}

	_config->n_modes		=	n_modes;
	_config->modes_idx.zone	=	PTR_NULL;
	allocate_new_zone	(0x00,(_config->n_modes)*sizeof(unsigned short),&_config->modes_idx	);

	video_mode_idxs_ptr	=	get_zone_ptr(&_config->modes_idx,0);

	kernel_log	(kernel_log_id,"supported ");
	n=0;
	while(n<n_modes)
	{
		video_mode_idxs_ptr[n]=mode_sup[n];

		writeint	(video_mode_idxs_ptr[n],16);
		writestr	(" ");
		n++;
	}
	writestr	("\n");

	

/*	
	unsigned short  *video_mode_idxs;

	_config->n_modes=0;
	while(video_mode_idxs[_config->n_modes]!=0xFFFF){_config->n_modes++;}
	_config->modes_idx.zone=PTR_NULL;
	allocate_new_zone	(0x00,(_config->n_modes)*sizeof(unsigned short),&_config->modes_idx	);
	video_mode_idxs_ptr	=	get_zone_ptr(&_config->modes_idx,0);
*/
	
	
	return 1;
}

vga_res_t *find_vga_mode_infos(int mode_idx)
{
	unsigned int n;

	n=0;

	while(vga_modes[n].code!=0xFF)
	{
		if(vga_modes[n].code==mode_idx)
			return &vga_modes[n];
		n++;
	}

	return PTR_NULL;
}
OS_API_C_FUNC(int) init_driver	 ()
{
	unsigned int		cnt,n_videos;
	unsigned short		*video_mode_idxs_ptr;
	video_mode_info_t	*video_mode_ptr;
	vga_res_t			*res;


	if(_config->n_modes<=0)return 0;

	video_mode_idxs_ptr	=	get_zone_ptr(&_config->modes_idx,0);

	_config->modes.zone=PTR_NULL;
	allocate_new_zone(0x00,(_config->n_modes+1)*sizeof(video_mode_info_t),&_config->modes);

	cnt			=	0;
	n_videos	=	0;
	
	while(cnt<_config->n_modes)
	{
		unsigned short mode_idx;

		mode_idx		=	video_mode_idxs_ptr[cnt];
		video_mode_ptr	=	get_zone_ptr(&_config->modes,cnt*sizeof(video_mode_info_t));

		if((res=find_vga_mode_infos(mode_idx))!=PTR_NULL)
		{
			
			video_mode_ptr->BitsPerPixel	=res->bit_pix;
			video_mode_ptr->NumberOfPlanes	=1;

			if(res->txt==1)
			{
				video_mode_ptr->PhysBasePtr_font=0xB0000;//B0000h-B7FFFh
				video_mode_ptr->PhysBasePtr		=0xB8000;
			}
			else
				video_mode_ptr->PhysBasePtr		=0xA0000;

			video_mode_ptr->XResolution			=	res->txt_x_res;
			video_mode_ptr->YResolution			=	res->txt_y_res;
			video_mode_ptr->XCharSize			=	0;
			video_mode_ptr->YCharSize			=	0;
			video_mode_ptr->BytesPerScanLine	=	(res->txt_x_res*2);

			n_videos++;
			
		}
		cnt++;
	}

	

	kernel_log	(kernel_log_id,"driver inited ");
	writeint	(n_videos,10);
	writestr	("modes found \n");

	/*
	unsigned char		*video_mode_info;
	video_mode_info_t	*video_mode_ptr;
	unsigned short		*video_mode_idxs_ptr;

	if(_config->n_modes<=0)return 0;

	video_mode_idxs_ptr	=	get_zone_ptr(&_config->modes_idx,0);

	_config->modes.zone=PTR_NULL;
	allocate_new_zone(0x00,(_config->n_modes+1)*sizeof(video_mode_info_t),&_config->modes);

	cnt			=	0;
	n_videos	=	0;
	

	while(cnt<_config->n_modes)
	{
		unsigned short mode_idx;

		mode_idx		=	video_mode_idxs_ptr[cnt];
		video_mode_ptr	=	get_zone_ptr(&_config->modes,cnt*sizeof(video_mode_info_t));


		if(bios_get_vesa_mode_info_c(mode_idx)==1)
		{
			video_mode_info		=	bios_get_vesa_mode_info_ptr_c();


			memcpy_c				(video_mode_ptr,video_mode_info,256);

			n_videos++;
			
		}
		cnt++;
	}

	kernel_log	(kernel_log_id,"driver inited ");
	writeint	(n_videos,10);
	writestr	("modes found \n");
	*/

	init_render_engine();
	return 1;
}

OS_API_C_FUNC(unsigned int) pci_get_device_cmd_flag (unsigned short vendor_id ,unsigned short device_id)
{
	return 0;
}


OS_API_C_FUNC(int)	  get_format	(unsigned int idx,unsigned int *fmt)
{
	video_mode_info_t *video_mode;
	

	if(idx>=_config->n_modes)return 0;

	video_mode	=	get_zone_ptr(&_config->modes,idx*sizeof(video_mode_info_t));
	
	fmt[0]=video_mode->XResolution;
	fmt[1]=video_mode->YResolution;
	fmt[2]=video_mode->BytesPerScanLine;
	fmt[3]=video_mode->BitsPerPixel;
	fmt[4]=0;//video_mode->ModeAttributes;
	fmt[5]=0;//video_mode->MemoryModel;
	fmt[6]=video_mode->PhysBasePtr;
	return 1;


}
OS_API_C_FUNC(int)  set_format	(unsigned int idx)
{
	
	unsigned short		mode_idx;
	unsigned short		*video_mode_idxs_ptr;
	video_mode_idxs_ptr	=	get_zone_ptr(&_config->modes_idx,0);
	mode_idx			=	video_mode_idxs_ptr[idx];
	

	kernel_log	(kernel_log_id,"vga set mode ");
	writeint	(mode_idx,16);

	if(bios_set_vga_mode_c(mode_idx)==1)
	{
		//video_mode_info_t *video_mode;video_mode	=	get_zone_ptr(&_config->modes,idx*sizeof(video_mode_info_t));

		_config->current_mode=idx;
		writestr(" done \n");

		
		setup_render_engine();
		return 1;
	}
	
	writestr(" error \n");
	return 0;
}

video_mode_info_t	*get_video_mode()
{
	video_mode_info_t	*video_mode_ptr;
	if(_config->current_mode==0xFFFFFFFF)return PTR_NULL;

	video_mode_ptr	=	get_zone_ptr(&_config->modes,_config->current_mode*sizeof(video_mode_info_t));

	return video_mode_ptr;

}

