

typedef struct
{
   unsigned short ModeAttributes; // mode attributes
   unsigned char  WinAAttributes; //window A attributes
   unsigned char  WinBAttributes; //window B attributes
   unsigned short WinGranularity; //window granularity
   unsigned short WinSize; //window size
   unsigned short WinASegment; //window A start segment
   unsigned short WinBSegment; //window B start segment
   unsigned int   WinFuncPtr; //pointer to windor function
   unsigned short BytesPerScanLine; //bytes per scan line

	// formerly optional information (now mandatory)

	unsigned short       XResolution;		 //horizontal resolution
	unsigned short       YResolution;		 //vertical resolution
	unsigned char        XCharSize;			 //character cell width
	unsigned char        YCharSize;			 //character cell height
	unsigned char        NumberOfPlanes;	 //number of memory planes
	unsigned char        BitsPerPixel;		 //bits per pixel
	unsigned char        NumberOfBanks;		 //number of banks
	unsigned char        MemoryModel;		 //memory model type
	unsigned char        BankSize;			 //bank size in kb
	unsigned char        NumberOfImagePages; //number of images
	unsigned char        Reserved0;			 //reserved for page function

	// new Direct Color fields
	unsigned char        RedMaskSize;			//size of direct color red mask in bits
	unsigned char        RedFieldPosition;		//bit position of LSB of red mask
	unsigned char        GreenMaskSize;			//size of direct color green mask in bits
	unsigned char        GreenFieldPosition;	//bit position of LSB of green mask
	unsigned char        BlueMaskSize;			//size of direct color blue mask in bits
	unsigned char        BlueFieldPosition;		//bit position of LSB of blue mask
	unsigned char        RsvdMaskSize ;			//size of direct color reserved mask in bits
	unsigned char        DirectColorModeInfo;	//Direct Color mode attributes

	//Following is VBE 2.0+ only
    unsigned int		PhysBasePtr;			//physical address for flat frame buffer
    unsigned int		OffScreenMemOffset;		//pointer to start of off screen memory
    unsigned short		OffScreenMemSize;		//amount of off screen memory in 1k units
	unsigned char		Reserved1[206];		// remainder of ModeInfoBlock
}video_mode_info_t;



typedef struct
{
	unsigned int	surf_id;
	unsigned int	surf_type;
	unsigned int	pix_type;
	unsigned int	d_type;
	unsigned int	pix_width;
	unsigned int	pix_height;

	unsigned int	pix_stride;

	unsigned int	width;
	unsigned int	height;
	unsigned int	scan_line;
	unsigned int	scan_line_shift;

	unsigned int	scan_shift;

	struct VBitmap				OrigImage;
	struct VBitmap				DrawArea;
	struct ResampleFilterData	fd;
	mem_zone_ref				pix_buff;
}surface_2D_t;



video_mode_info_t	*get_video_mode				();
int					init_render_engine			();
int					setup_render_engine			();