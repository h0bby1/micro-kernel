#define DRIVER_FUNC		C_EXPORT
#include <std_def.h>
#include <std_mem.h>
#include "kern.h"
#include "sys_pci.h"
#include "mem_base.h"
#include "lib_c.h"
#include "sys/mem_stream.h"
#include "sys/tpo_mod.h"
#include "sys/pci_device.h"

#include "../../../../../graphic_base/filters/filters.h"
#include "vesa_drv.h"

DRIVER_FUNC unsigned char  class_codes[]  = {0x03, 0x00,0x00,
											 0x00,0x00,0x00};

DRIVER_FUNC unsigned int C_API_FUNC pci_get_device_cmd_flag (unsigned short vendor_id ,unsigned short device_id);
DRIVER_FUNC int			 C_API_FUNC probe_device			 (pci_device *device);
DRIVER_FUNC int			 C_API_FUNC init_driver			 ();
DRIVER_FUNC int			 C_API_FUNC  get_format			 (unsigned int idx,unsigned int *fmt);
DRIVER_FUNC int			 C_API_FUNC  set_format			 (unsigned int idx);

unsigned int	kernel_log_id	=	0xFFFFFFFF;
typedef struct
{
	unsigned int		pci_dev_id;
	unsigned int		pci_bus_id;
	unsigned char		device_info_ptr[256];
	unsigned char		vesa_sig[5];
	unsigned short		vesa_ver_major; 
	unsigned short		vesa_ver_minor; 
	unsigned char		*oem_str;
	unsigned short		TotalMemory ;
	unsigned int		stream_dev_id;
	unsigned int		current_mode;
	mem_zone_ref		modes;
	mem_zone_ref		modes_idx;
	unsigned int		n_modes;
}device_config;

device_config			c										=	{0xff};
device_config			*_config								=	&c;


OS_API_C_FUNC(int) probe_device	 (pci_device *device)
{
	unsigned char	*device_info_ptr;
	unsigned short  oem_ptr_real_s,oem_ptr_real_o;
	unsigned int	oem_ptr_phys;
	unsigned short  vmode_ptr_real_s,vmode_ptr_real_o;
	unsigned int	vmode_ptr_phys;
	unsigned int	retry;
	unsigned short  *video_mode_idxs;
	unsigned short  *video_mode_idxs_ptr;

	kernel_log_id	=	get_new_kern_log_id("vesa_drv:",0x0C);


	kernel_log(kernel_log_id,"vesa get bios infos\n");
	

	_config->modes.zone=PTR_NULL;
	_config->current_mode=0xFFFFFFFF;

	retry=0;
	while((bios_get_vesa_info_c	()!=1))
	{
		if(retry>=5)
		{
			kernel_log(kernel_log_id,"vesa bios not found !\n");
			snooze(1000000);
			return 0;
		}
		retry++;
		delay_x_ms_c(100);
	}

	
	

	device_info_ptr=bios_get_vesa_info_ptr_c();
	
	_config->pci_bus_id=device->bus_id;
	_config->pci_dev_id=device->dev_id;

	memcpy_c(_config->device_info_ptr,device_info_ptr,256);


	strncpy_c	(_config->vesa_sig,_config->device_info_ptr,4);
	_config->vesa_sig[5]=0;

	_config->vesa_ver_major=_config->device_info_ptr[5];
	_config->vesa_ver_minor=_config->device_info_ptr[4];

	oem_ptr_real_o=*((unsigned short *)(&_config->device_info_ptr[6]));
	oem_ptr_real_s=*((unsigned short *)(&_config->device_info_ptr[8]));
	oem_ptr_phys	=(oem_ptr_real_s<<4)+oem_ptr_real_o;

	vmode_ptr_real_o=*((unsigned short *)(&_config->device_info_ptr[14]));
	vmode_ptr_real_s=*((unsigned short *)(&_config->device_info_ptr[16]));
	vmode_ptr_phys	=(vmode_ptr_real_s<<4)+vmode_ptr_real_o;

	_config->TotalMemory	=*((unsigned short *)(&_config->device_info_ptr[18]));

	_config->oem_str			=uint_to_mem(oem_ptr_phys);
	video_mode_idxs				=uint_to_mem(vmode_ptr_phys);
	_config->n_modes=0;
	while(video_mode_idxs[_config->n_modes]!=0xFFFF){_config->n_modes++;}

	_config->modes_idx.zone=PTR_NULL;
	allocate_new_zone	(0x00,(_config->n_modes)*sizeof(unsigned short),&_config->modes_idx	);

	video_mode_idxs_ptr	=	get_zone_ptr(&_config->modes_idx,0);

	memcpy_c(video_mode_idxs_ptr,video_mode_idxs,(_config->n_modes)*sizeof(unsigned short));

	kernel_log	(kernel_log_id,"vesa bios found ! info ");
	writeptr	(device_info_ptr);
	writestr	(" ");
	writestr	(_config->vesa_sig);
	writestr	(" ");
	writeint	(_config->vesa_ver_major,10);
	writestr	(".");
	writeint	(_config->vesa_ver_minor,10);
	writestr	("\n");

	kernel_log	(kernel_log_id,_config->oem_str);
	writestr	(" ");
	writeint	(_config->n_modes,10);
	writestr	("\n");
	snooze		(10000);
	
	return 1;
	
		

	
}
OS_API_C_FUNC(int) init_driver	 ()
{
	unsigned int		cnt,n_videos;
	unsigned char		*video_mode_info;
	video_mode_info_t	*video_mode_ptr;
	unsigned short		*video_mode_idxs_ptr;

	if(_config->n_modes<=0)return 0;

	video_mode_idxs_ptr	=	get_zone_ptr(&_config->modes_idx,0);

	_config->modes.zone=PTR_NULL;
	allocate_new_zone(0x00,(_config->n_modes+1)*sizeof(video_mode_info_t),&_config->modes);

	cnt			=	0;
	n_videos	=	0;
	

	while(cnt<_config->n_modes)
	{
		unsigned short mode_idx;

		mode_idx		=	video_mode_idxs_ptr[cnt];
		video_mode_ptr	=	get_zone_ptr(&_config->modes,cnt*sizeof(video_mode_info_t));


		if(bios_get_vesa_mode_info_c(mode_idx)==1)
		{
			video_mode_info		=	bios_get_vesa_mode_info_ptr_c();


			memcpy_c				(video_mode_ptr,video_mode_info,256);

			n_videos++;
			
		}
		cnt++;
	}

	kernel_log	(kernel_log_id,"driver inited ");
	writeint	(n_videos,10);
	writestr	("modes found \n");

	init_render_engine();
	return 1;
}

OS_API_C_FUNC(unsigned int) pci_get_device_cmd_flag (unsigned short vendor_id ,unsigned short device_id)
{
	return 0;
}


OS_API_C_FUNC(int)	  get_format	(unsigned int idx,unsigned int *fmt)
{
	video_mode_info_t *video_mode;
	

	if(idx>=_config->n_modes)return 0;

	video_mode	=	get_zone_ptr(&_config->modes,idx*sizeof(video_mode_info_t));
	
	fmt[0]=video_mode->XResolution;
	fmt[1]=video_mode->YResolution;
	fmt[2]=video_mode->BytesPerScanLine;
	fmt[3]=video_mode->BitsPerPixel;
	fmt[4]=video_mode->ModeAttributes;
	fmt[5]=video_mode->MemoryModel;
	fmt[6]=video_mode->PhysBasePtr;
	return 1;


}
OS_API_C_FUNC(int)	  set_format	(unsigned int idx)
{
	unsigned short		*video_mode_idxs_ptr;
	unsigned short		mode_idx;
	video_mode_idxs_ptr	=	get_zone_ptr(&_config->modes_idx,0);
	mode_idx			=	video_mode_idxs_ptr[idx];
	

	kernel_log	(kernel_log_id,"vesa set mode ");
	writeint	(mode_idx,16);

	if(bios_set_vesa_mode_c(mode_idx)==1)
	{
		_config->current_mode=idx;
		writestr(" done \n");
		setup_render_engine();
		return 1;
	}
	writestr(" error \n");
	return 0;
}

video_mode_info_t	*get_video_mode()
{
	video_mode_info_t	*video_mode_ptr;
	if(_config->current_mode==0xFFFFFFFF)return PTR_NULL;

	video_mode_ptr	=	get_zone_ptr(&_config->modes,_config->current_mode*sizeof(video_mode_info_t));

	return video_mode_ptr;

}

