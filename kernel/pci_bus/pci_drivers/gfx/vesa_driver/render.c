#define RENDER_API		C_EXPORT
#include <std_def.h>
#include <std_mem.h>
#include "kern.h"

#include "mem_base.h"
#include "lib_c.h"

#include "../../../../../graphic_base/filters/filters.h"
#include "gfx/graphic_object.h"
#include "gfx/graphic_render.h"
#include "vesa_drv.h"


typedef struct
{
	unsigned int					id;
	unsigned int					surf_id;
	struct gfx_font_glyph_list_t	glyph_list;
	unsigned int					crc_name;
	unsigned int					surf_side;
	unsigned int					surf_height;
	unsigned int					surf_pix_type;
	unsigned int					x_pos;
	unsigned int					y_pos;
}gfx_render_font_t;
/*
typedef struct
{
	unsigned int					id;
	unsigned int					surf_id;
	struct gfx_rect					rects[8];
}gfx_render_cursor_t;
*/
typedef struct
{
	mem_zone_ref		surface_2D_list;
	unsigned int		num_surfaces_2D;
	unsigned int		next_surfaces_2D_id;

	/*
	mem_zone_ref		cursor_list;
	unsigned int		num_cursors;
	unsigned int		next_cursor_id;
	*/

	unsigned int		cursor_surf_id;
	mem_zone_ref		font_list;
	unsigned int		num_fonts;
	unsigned int		next_font_id;

	gfx_render_font_t	*selected_font;

	

	unsigned int		back_buf_id;
	surface_2D_t		*back_buf;
	
	unsigned int		frame_buffer_scan_lines[2048];
	video_mode_info_t	*video_mode;

	vec_4uc_t			color;
	vec_4uc_t			clear_color;

	gfx_rect_t			clip_rect;
}rendering_context_t;




mem_zone_ref		rendering_context_ref			={PTR_INVALID};
extern unsigned int kernel_log_id;

RENDER_API unsigned int C_API_FUNC create_surface_2D		(unsigned int surf_type	,unsigned int pix_type,unsigned int d_type,unsigned int width,unsigned int height,unsigned int d_pix_type,unsigned char *data);
RENDER_API unsigned int C_API_FUNC load_surface_data  		(unsigned int surface_id,unsigned int x,unsigned int y,gfx_rect_t *src_rect,unsigned int src_scan_line,unsigned int pix_type,mem_ptr pix_data);
RENDER_API void			C_API_FUNC free_surface_2D			(unsigned int surf_id);
RENDER_API void			C_API_FUNC swap_buffers				(unsigned int flags);
RENDER_API void			C_API_FUNC clear_buffers			(unsigned int flags);
RENDER_API void			C_API_FUNC set_color_4uc			(unsigned char r,unsigned char g,unsigned char b,unsigned char a);
RENDER_API void			C_API_FUNC set_clear_color_4uc		(unsigned char r,unsigned char g,unsigned char b,unsigned char a);
RENDER_API void		 	C_API_FUNC draw_rect				(const gfx_rect_t *rect);
RENDER_API void		 	C_API_FUNC draw_line				(const vec_2s_t p1,const vec_2s_t p2);
RENDER_API void		 	C_API_FUNC draw_hspan_list			(struct gfx_hspan_list_t *list,int x,int y);
RENDER_API void		 	C_API_FUNC draw_point				(const vec_2s_t	p1);
RENDER_API void		 	C_API_FUNC blit_surf				(unsigned int surface_id,int x_pos,int y_pos,gfx_rect_t *src_rect);
RENDER_API void		 	C_API_FUNC blit_surf_resize			(unsigned int surface_id, gfx_rect_t *dst_rect,gfx_rect_t *src_rect);
RENDER_API void		 	C_API_FUNC draw_text				(const char *text_txt,const vec_2s_t trans,const vec_2s_t zoom,vec_2s_t out_ext);
RENDER_API void		 	C_API_FUNC draw_image				(int x_pos, int y_pos,gfx_image_t *img,gfx_rect_t *src_rect);
RENDER_API void		 	C_API_FUNC draw_image_resize		(gfx_image_t *img, gfx_rect_t *dst_rect,gfx_rect_t *src_rect);
RENDER_API void		 	C_API_FUNC set_clip_rect			(int x,int y,unsigned int width,unsigned int height);
RENDER_API unsigned int C_API_FUNC select_font				(const char *font_name,unsigned int font_size_x,unsigned int font_size_y,unsigned int *new_font,struct gfx_font_glyph_list_t **out_list);
RENDER_API unsigned int C_API_FUNC load_font_surface_data	(gfx_rect_t *src_rect,unsigned int glyph_idx,unsigned int src_scan_line,unsigned int pix_type,mem_ptr pix_data);
RENDER_API void			C_API_FUNC get_text_vec				(const char *text,struct gfx_hspan_list_t *list,struct gfx_rect *text_box);
RENDER_API unsigned int C_API_FUNC load_cursor				(gfx_image_t		*cusor_img);
RENDER_API void			C_API_FUNC draw_cursor				(int x,int y);

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
									internal functions
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**/

mem_ptr	get_surf_addr(surface_2D_t	*surf,int x,int y)
{
	mem_ptr			mem_buffer;
	unsigned int	start_addr;

	start_addr		=	y*surf->scan_line+((x*surf->pix_stride)>>3);
	mem_buffer		=	get_zone_ptr(&surf->pix_buff,start_addr);
	
	return mem_buffer;


}
surface_2D_t			*get_surface_2D(unsigned int surf_id)
{
	rendering_context_t		*rendering_context_ptr;
	surface_2D_t			*surface_ptr,*last_surface_ptr;

	rendering_context_ptr	=	get_zone_ptr(&rendering_context_ref,0);
	surface_ptr				=	get_zone_ptr(&rendering_context_ptr->surface_2D_list,0);
	last_surface_ptr		=	get_zone_ptr(&rendering_context_ptr->surface_2D_list,rendering_context_ptr->num_surfaces_2D*sizeof(surface_2D_t));
	while(surface_ptr<last_surface_ptr)
	{
		if(surface_ptr->surf_id==surf_id)
			return surface_ptr;
		
		surface_ptr++;
	}
	return PTR_NULL;
}
int init_render_engine			()
{
	rendering_context_t		*rendering_context_ptr;
	
	rendering_context_ref.zone	=PTR_NULL;
	
	allocate_new_zone	(0x00,sizeof(rendering_context_t),&rendering_context_ref);
	rendering_context_ptr=get_zone_ptr(&rendering_context_ref,0);

	rendering_context_ptr->num_surfaces_2D				=0;
	rendering_context_ptr->next_surfaces_2D_id			=1;
	rendering_context_ptr->back_buf_id					=0;
	rendering_context_ptr->color[0]						=0;
	rendering_context_ptr->color[1]						=255;
	rendering_context_ptr->color[2]						=0;
	rendering_context_ptr->color[3]						=255;

	rendering_context_ptr->surface_2D_list.zone		=PTR_NULL;
	allocate_new_zone(0x00,16*sizeof(surface_2D_t),&rendering_context_ptr->surface_2D_list);

	rendering_context_ptr->font_list.zone=PTR_NULL;
	allocate_new_zone				(0x00,sizeof(gfx_render_font_t)*8		,&rendering_context_ptr->font_list);
	rendering_context_ptr->num_fonts=0;
	rendering_context_ptr->next_font_id=1;

	/*
	rendering_context_ptr->cursor_list.zone=PTR_NULL;
	allocate_new_zone				(0x00,sizeof(gfx_render_cursor_t)*8		,&rendering_context_ptr->cursor_list);
	rendering_context_ptr->num_cursors=0;
	rendering_context_ptr->next_cursor_id=1;
	*/

	rendering_context_ptr->selected_font	=PTR_NULL;
	

	return 1;
}

int setup_render_engine			()
{
	rendering_context_t		*rendering_context_ptr;
	unsigned int			cnt;
	unsigned int			scan_line_ptr;

	rendering_context_ptr=get_zone_ptr(&rendering_context_ref,0);
	
	if(rendering_context_ptr->back_buf_id!=0)
	{
		free_surface_2D(rendering_context_ptr->back_buf_id);
		rendering_context_ptr->back_buf_id=0;
	}

	rendering_context_ptr->cursor_surf_id = 0;

	rendering_context_ptr->video_mode	=	get_video_mode();

	rendering_context_ptr->back_buf_id	=	create_surface_2D	(1,GFX_PIX_FORMAT_RGBA,1,rendering_context_ptr->video_mode->XResolution,rendering_context_ptr->video_mode->YResolution,0,PTR_NULL);
	rendering_context_ptr->back_buf		=	get_surface_2D		(rendering_context_ptr->back_buf_id);

	cnt				=0;
	scan_line_ptr	=0;
	while(cnt<rendering_context_ptr->video_mode->YResolution)
	{
		rendering_context_ptr->frame_buffer_scan_lines[cnt]	 =	scan_line_ptr;
		scan_line_ptr										+=	rendering_context_ptr->video_mode->BytesPerScanLine;
		cnt++;
	}

	rendering_context_ptr->clip_rect.pos[0]		=	0;
	rendering_context_ptr->clip_rect.pos[1]		=	0;

	rendering_context_ptr->clip_rect.size[0]	=	rendering_context_ptr->video_mode->XResolution;
	rendering_context_ptr->clip_rect.size[1]	=	rendering_context_ptr->video_mode->YResolution;

	return 1;
}


gfx_render_font_t	*find_render_font_id(rendering_context_t	*renderer,unsigned int font_id)
{
	unsigned int			n;
	gfx_render_font_t		*fonts;
	
	fonts					=	get_zone_ptr(&renderer->font_list,0);

	n=0;
	while(n<renderer->num_fonts)
	{
		if(fonts[n].id==font_id)
			return &fonts[n];

		n++;
	}

	return PTR_NULL;
}


gfx_render_font_t *find_render_font(rendering_context_t		*renderer,const char *font_name,unsigned int size_x,unsigned int size_y)
{
	gfx_render_font_t		*fonts;
	unsigned int			n,crc_name;
	
	
	

	crc_name			=	calc_crc32_c(font_name,32);
	fonts				=	get_zone_ptr(&renderer->font_list,0);

	n=0;
	while(n<renderer->num_fonts)
	{
		if((fonts[n].crc_name==crc_name)&&(fonts[n].glyph_list.size_x==size_x)&&(fonts[n].glyph_list.size_y==size_y))
			return &fonts[n];

		n++;
	}

	return PTR_NULL;


}


gfx_render_font_t *get_new_render_font(rendering_context_t		*renderer,const char *font_name,unsigned int size_x,unsigned int size_y,unsigned int first_glyph,unsigned int last_glyph)
{
	gfx_render_font_t		*fonts,*new_font;
	unsigned int			crc_name;
	
	fonts						=	get_zone_ptr(&renderer->font_list,0);
	crc_name					=	calc_crc32_c(font_name,32);
	new_font					=	&fonts[renderer->num_fonts];
	new_font->id				=	renderer->next_font_id;
	new_font->surf_id			=	0;
	new_font->crc_name			=	crc_name;
	new_font->glyph_list.size_x	=	size_x;
	new_font->glyph_list.size_y	=	size_y;
	new_font->x_pos				=	0;
	new_font->y_pos				=	0;
	
	renderer->num_fonts++;
	renderer->next_font_id++;
	
	return new_font;
}


OS_API_C_FUNC(void)	get_text_vec	(const char *text,struct gfx_hspan_list_t *list,struct gfx_rect *text_box)
{
	int								text_pos,t_x;
	unsigned char					c;
	unsigned int					str_pos;
	rendering_context_t				*rendering_context_ptr;
	gfx_render_font_t				*rdr_font;
	int								min_y,max_y;


	rendering_context_ptr	=	get_zone_ptr	(&rendering_context_ref,0);

	if(rendering_context_ptr->selected_font==PTR_NULL)return;

	rdr_font				=	rendering_context_ptr->selected_font;

	str_pos			=0;
	text_pos		=0;

	1000000;
	1000000;



	c				 =text[str_pos];
	
	text_box->pos[0]	=rdr_font->glyph_list.glyph_metrics[c].box_left;
	
	text_box->size[0]	=0;
	text_box->size[1]	=0;

	t_x					=0;
	max_y				=-10000;
	min_y				= 10000;


	

	while(text[str_pos]!=0)
	{
		c=text[str_pos];
		if((c>=32)&&(rdr_font->glyph_list.glyph_metrics[c].adv_x>0))
		{
			int		c_min_y,c_max_y;
			text_pos		    =	t_x+rdr_font->glyph_list.glyph_metrics[c].box_left+rdr_font->glyph_list.glyph_box[c].size[0];
			t_x					=	t_x +	rdr_font->glyph_list.glyph_metrics[c].adv_x;

			c_min_y				=	-rdr_font->glyph_list.glyph_metrics[c].box_top;
			c_max_y				=	c_min_y+rdr_font->glyph_list.glyph_box[c].size[1];

			if(c_min_y<min_y)min_y		=c_min_y;
			if(c_max_y>max_y)max_y		=c_max_y;
		}
		str_pos++;
	}

	
	text_box->size[0] = text_pos-text_box->pos[0];
	text_box->size[1] = max_y-min_y;
	text_box->pos[1]  =	min_y;





#if 0
			if(font_spans->glyph_infos[c].glyph_span_first<font_spans->glyph_infos[c].glyph_span_last)
			{
				struct gfx_hspan_t		*in_span;

				in_span		=get_zone_ptr	(&font_spans->span_list.span_list,(font_spans->glyph_infos[c].glyph_span_first)*sizeof(struct gfx_hspan_t));
				

				if(list!=PTR_NULL)
				{
					struct gfx_hspan_t	*out_span;
					
					expand_zone					(&list->span_list,(list->num_spans+(font_spans->glyph_infos[c].glyph_span_last-font_spans->glyph_infos[c].glyph_span_first)+2)*sizeof(struct gfx_hspan_t));
					out_span=get_zone_ptr		(&list->span_list,(list->num_spans)*sizeof(struct gfx_hspan_t));
					g_idx		=font_spans->glyph_infos[c].glyph_span_first;
					while(g_idx<font_spans->glyph_infos[c].glyph_span_last)
					{

						out_span->pos[0]	=in_span->pos[0]+text_pos;
						out_span->pos[1]	=in_span->pos[1];
						out_span->len		=in_span->len;
						out_span->coverage	=in_span->coverage;

						if(out_span->pos[0]<min_x)min_x=out_span->pos[0];
						if(out_span->pos[1]<min_y)min_y=out_span->pos[1];
						one_glyph		=1;

						list->num_spans++;
						in_span++;
						out_span++;
						
						g_idx++;
					}
				}
				else
				{
					g_idx		=font_spans->glyph_infos[c].glyph_span_first;
					while(g_idx<font_spans->glyph_infos[c].glyph_span_last)
					{
						if((in_span->pos[0]+text_pos)<min_x)min_x=(in_span->pos[0]+text_pos);
						if(in_span->pos[1]<min_y)min_y=in_span->pos[1];
						one_glyph		=1;

						in_span++;
						g_idx++;
					}
				}
			}
			if(iabs_c(font_spans->glyph_metrics[c].adv_y)>max_y)max_y=iabs_c(font_spans->glyph_metrics[c].adv_y);
			text_pos=text_pos+(font_spans->glyph_metrics[c].adv_x);
		}
		str_pos++;
	 }
	text_box->pos[0]	=min_x;
	text_box->pos[1]	=min_y+font_spans->glyph_metrics[c].adv_y;
	text_box->size[0]	=(text_pos-min_x);
	text_box->size[1]	=max_y-min_y;
#endif
}


/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
									external functions
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**/

OS_API_C_FUNC(unsigned int) load_cursor			(gfx_image_t		*cusor_img)
{
	rendering_context_t			*rendering_context_ptr;
	struct		gfx_rect		src_rect;

	rendering_context_ptr			=	get_zone_ptr(&rendering_context_ref,0);
	if(rendering_context_ptr->back_buf==PTR_NULL)return 0;

	src_rect.pos[0]					=	0;
	src_rect.pos[1]					=	0;

	src_rect.size[0]				=	cusor_img->width;
	src_rect.size[1]				=	cusor_img->height;

	if (rendering_context_ptr->cursor_surf_id == 0)
	{
		rendering_context_ptr->cursor_surf_id = create_surface_2D(0, GFX_PIX_FORMAT_RGBA, 0x0, cusor_img->width, cusor_img->height, 0, PTR_NULL);

		writestr_fmt("load cursor surf %d %d %d\n", rendering_context_ptr->cursor_surf_id, cusor_img->width, cusor_img->height);
	}
		
	if(rendering_context_ptr->cursor_surf_id != 0)
		load_surface_data				(rendering_context_ptr->cursor_surf_id,0,0,&src_rect,cusor_img->line_size,cusor_img->pixfmt,get_zone_ptr(&cusor_img->image_data,0));

	

	return rendering_context_ptr->cursor_surf_id;
}


OS_API_C_FUNC(void)		draw_cursor				(int x,int y)
{
	rendering_context_t	 *rendering_context_ptr;
//	gfx_render_cursor_t			*cursor;
	struct		gfx_rect		src_rect;

	rendering_context_ptr = get_zone_ptr(&rendering_context_ref, 0);
	if (rendering_context_ptr->back_buf == PTR_NULL)return 0;

	blit_surf	(rendering_context_ptr->cursor_surf_id,x,y,PTR_NULL);
/*	
	cursor	=	find_cursor	(cursor_id);
	if(cursor==PTR_NULL)return;

	blit_surf	(cursor->surf_id,x,y,&cursor->rects[state]);
*/	


}
OS_API_C_FUNC(unsigned int) select_font(const char *font_name,unsigned int font_size_x,unsigned int font_size_y,unsigned int *new_font,struct gfx_font_glyph_list_t **out_list)
{
	gfx_render_font_t		*rdr_font;
	rendering_context_t		*rendering_context_ptr;
	
	rendering_context_ptr	=	get_zone_ptr	(&rendering_context_ref,0);

	if(font_name==PTR_NULL)return 0;
	if(strlen_c(font_name)==0)return 0;
	if(font_size_x==0)return 0;
	if(font_size_y==0)return 0;

	(*new_font)	=	0;

	rdr_font	=	find_render_font			(rendering_context_ptr,font_name,font_size_x,font_size_y);

	if(rdr_font		==	PTR_NULL)
	{
		unsigned int	surf_side;

		
		kernel_log					(kernel_log_id,"creating font '");
		writestr					(font_name);
		writestr					("' ");
		writeint					(font_size_x,10);
		writestr					("x");
		writeint					(font_size_y,10);
		writestr					("\n");
		
		rdr_font	=	get_new_render_font			(rendering_context_ptr,font_name,font_size_x,font_size_y,0,255);

		gfx_font_init								(&rdr_font->glyph_list,font_name,font_size_x,font_size_y,0,255);
		
		
		surf_side				=	isqrt((rdr_font->glyph_list.max_char_w*rdr_font->glyph_list.max_char_h)*(175-32))+1;
		rdr_font->surf_id		=	create_surface_2D(0,GFX_PIX_FORMAT_RGBA,0x0,surf_side,surf_side,0x0,PTR_NULL);
		
		kernel_log					(kernel_log_id,"font surface created [");
		writeint					(rdr_font->surf_id,16);
		writestr					("]\n");
			

		(*new_font)				=	1;
	}

	if(out_list!=PTR_NULL)
	{
		(*out_list)=&rdr_font->glyph_list;
	}

	rendering_context_ptr->selected_font=rdr_font;

	return rdr_font->id;
}


OS_API_C_FUNC(void) draw_text(const char *text_txt,const vec_2s_t trans,const vec_2s_t zoom,vec_2s_t out_ext)
{
	rendering_context_t		*rendering_context_ptr;
	gfx_render_font_t		*rdr_font;
	char					c;
	int						text_x,text_y;
	int						ext_x,ext_y;
	int						pos_adv_x;
	int						pos_adv_y;



	rendering_context_ptr	=	get_zone_ptr	(&rendering_context_ref,0);
	rdr_font				=	rendering_context_ptr->selected_font;
	text_x					=	trans[0];
	text_y					=	trans[1];

	ext_x					=	0;
	ext_y					=	0;
	/*
	struct gfx_hspan_list_t	span_list;
	gfx_rect_t				txt_rect;
	span_list.num_spans			=	0;
	span_list.span_list.zone	=	PTR_NULL;
	span_list.num_span_alloc	=	16;
	
	allocate_new_zone			(0,span_list.num_span_alloc*sizeof(struct gfx_hspan_t),&span_list.span_list);

	get_text_vec			(text_txt,&span_list,&txt_rect);
	
	if(span_list.num_spans>0)
	{
		draw_hspan_list			(&span_list,text_x,text_y);
	}
	release_zone_ref					(&span_list.span_list);
	*/
	while((c=(*text_txt++))!=0)
	{
		if(rdr_font->glyph_list.glyph_box[c].size[0]>0)
		{
			if(((zoom==PTR_NULL)||((zoom[0]==128)&&(zoom[1]==128))))
				blit_surf(rdr_font->surf_id,text_x+rdr_font->glyph_list.glyph_metrics[c].box_left,text_y-rdr_font->glyph_list.glyph_metrics[c].box_top,&rdr_font->glyph_list.glyph_box[c]);
			else
			{
				struct gfx_rect		dst_rect;

				dst_rect.pos[0]=text_x+rdr_font->glyph_list.glyph_metrics[c].box_left;
				dst_rect.pos[1]=text_y-rdr_font->glyph_list.glyph_metrics[c].box_top;

				dst_rect.size[0]=(rdr_font->glyph_list.glyph_box[c].size[0]*zoom[0])>>7;
				dst_rect.size[1]=(rdr_font->glyph_list.glyph_box[c].size[1]*zoom[1])>>7;

				blit_surf_resize(rdr_font->surf_id,&dst_rect,&rdr_font->glyph_list.glyph_box[c]);
			}
			
		}

		if(zoom!=PTR_NULL)
		{
			pos_adv_x	= ((rdr_font->glyph_list.glyph_metrics[c].adv_x*zoom[0])>>7);
			pos_adv_y	= ((rdr_font->glyph_list.glyph_metrics[c].adv_y*zoom[1])>>7);
		}
		else
		{
			pos_adv_x	= rdr_font->glyph_list.glyph_metrics[c].adv_x;
			pos_adv_y	= rdr_font->glyph_list.glyph_metrics[c].adv_y;
		}

		if(pos_adv_y>ext_y)
			ext_y=pos_adv_y;

		text_x	    = text_x+pos_adv_x;
		ext_x	    +=pos_adv_x;
	}

	if(out_ext!=PTR_NULL)
	{
		out_ext[0]=ext_x;
		out_ext[1]=ext_y;
	}

	
}	


OS_API_C_FUNC(void) free_surface_2D		(unsigned int surf_id)
{
	rendering_context_t		*rendering_context_ptr;
	surface_2D_t			*surface_ptr,*last_surface_ptr;

	rendering_context_ptr	=	get_zone_ptr	(&rendering_context_ref,0);
	surface_ptr				=	get_zone_ptr	(&rendering_context_ptr->surface_2D_list,0);	
	last_surface_ptr		=	get_zone_ptr	(&rendering_context_ptr->surface_2D_list,rendering_context_ptr->num_surfaces_2D*sizeof(surface_2D_t));

	while(surface_ptr<last_surface_ptr)
	{
		if(surface_ptr->surf_id==surf_id)
		{
			release_zone_ref	(&surface_ptr->pix_buff);
			
			while((&surface_ptr[1])<last_surface_ptr)
			{
				memcpy_c		(surface_ptr,&surface_ptr[1],sizeof(surface_2D_t));
				surface_ptr++;
			}
			rendering_context_ptr->num_surfaces_2D--;
			rendering_context_ptr->next_surfaces_2D_id=surf_id;


			return ;
		}
		surface_ptr++;
	}

	return ;
}



OS_API_C_FUNC(void) blit_surf_resize	(unsigned int surface_id, gfx_rect_t *dst_rect,gfx_rect_t *src_rect)
{
	surface_2D_t			*surface;
	gfx_rect_t				my_rect,*clip_rect	;
	rendering_context_t		*rendering_context_ptr;
	struct VBitmap		src,dst;
	

	surface_2D_t		*back_buf_ptr;

	surface		=	get_surface_2D(surface_id);
	if(surface==PTR_NULL)return ;

	rendering_context_ptr	=	get_zone_ptr(&rendering_context_ref,0);
	if(rendering_context_ptr==PTR_NULL)return ;

	clip_rect		=	&rendering_context_ptr->clip_rect;
	back_buf_ptr	=	rendering_context_ptr->back_buf;


	if(src_rect==PTR_NULL)
	{
		src_rect			=&my_rect;
		src_rect->pos[0]	=0;
		src_rect->pos[1]	=0;

		src_rect->size[0]	=surface->width;
		src_rect->size[1]	=surface->height;

	}
	if(dst_rect->size[0]<1)return;
	if(dst_rect->size[1]<1)return;

	if(src_rect->size[0]<1)return;
	if(src_rect->size[1]<1)return;

	if(dst_rect->pos[0]>=(clip_rect->pos[0]+clip_rect->size[0]))return;
	if(dst_rect->pos[1]>=(clip_rect->pos[1]+clip_rect->size[1]))return;

	if((dst_rect->pos[0]+dst_rect->size[0])<clip_rect->pos[0])return;
	if((dst_rect->pos[1]+dst_rect->size[1])<clip_rect->pos[1])return;


	if(setup_clipped_resize	(&surface->fd,clip_rect,dst_rect,src_rect,&src,surface->scan_line,&surface->pix_buff)==0)return ;

	surface->fd.max_x	=	rendering_context_ptr->back_buf->width;
	surface->fd.max_y	=	rendering_context_ptr->back_buf->height;
	

	if((surface->fd.new_x<16)||(surface->fd.new_y<16)||(surface->fd.in_width<16)||(surface->fd.in_height<16))
	{
		
		
		bmp_init			(&dst		,get_zone_ptr(&rendering_context_ptr->back_buf->pix_buff,0),rendering_context_ptr->back_buf->width, rendering_context_ptr->back_buf->scan_line,rendering_context_ptr->back_buf->height, 32);

		surface->fd.src					=	&src;
		surface->fd.dst					=	&dst;
		surface->fd.filter_mode			=	kLinearInterp;

		simple_resize			(&surface->fd,rendering_context_ptr->color);
	}
	else
	{
		bmp_init				(&dst  	,get_zone_ptr(&rendering_context_ptr->back_buf->pix_buff,0),rendering_context_ptr->back_buf->width, rendering_context_ptr->back_buf->scan_line,rendering_context_ptr->back_buf->height, surface->pix_stride);

		surface->fd.src				=	&src;
		surface->fd.dst				=	&dst;
		surface->fd.filter_mode		=	kLinearInterp;
		surface->fd.d_y				=	rendering_context_ptr->back_buf->height-(surface->fd.d_y+surface->fd.new_y);

		resize_param		(&surface->fd);
		resize_run			(&surface->fd,get_zone_ptr(&rendering_context_ptr->back_buf->pix_buff,0));
	}



}


OS_API_C_FUNC(void) draw_image_resize	(gfx_image_t *img, gfx_rect_t *dst_rect,gfx_rect_t *src_rect)
{
	gfx_rect_t				my_rect;
	rendering_context_t		*rendering_context_ptr;
	gfx_rect_t			*clip_rect;

	rendering_context_ptr	=	get_zone_ptr(&rendering_context_ref,0);

	if(rendering_context_ptr==PTR_NULL)return ;
	if(rendering_context_ptr==PTR_INVALID)return ;
	if(rendering_context_ptr->back_buf==PTR_NULL)return ;

	if(src_rect==PTR_NULL)
	{
		src_rect			=&my_rect;
		src_rect->pos[0]	=0;
		src_rect->pos[1]	=0;

		src_rect->size[0]	=img->width;
		src_rect->size[1]	=img->height;

	}

	
	clip_rect		=	&rendering_context_ptr->clip_rect;

	if(dst_rect->pos[0]>=(clip_rect->pos[0]+clip_rect->size[0]))return;
	if(dst_rect->pos[1]>=(clip_rect->pos[1]+clip_rect->size[1]))return;

	if((dst_rect->pos[0]+dst_rect->size[0])<clip_rect->pos[0])return;
	if((dst_rect->pos[1]+dst_rect->size[1])<clip_rect->pos[1])return;


	if(setup_clipped_resize	(&img->fd,clip_rect,dst_rect,src_rect,&img->OrigImage,img->line_size,&img->image_data)==0)return ;
		
	bmp_init			(&img->DrawArea		,get_zone_ptr(&rendering_context_ptr->back_buf->pix_buff,0),rendering_context_ptr->back_buf->width, rendering_context_ptr->back_buf->scan_line,rendering_context_ptr->back_buf->height, 32);

	img->fd.src					=	&img->OrigImage;
	img->fd.dst					=	&img->DrawArea;
	img->fd.filter_mode			=	kLinearInterp;
	img->fd.max_x				=	rendering_context_ptr->back_buf->width;
	img->fd.max_y				=	rendering_context_ptr->back_buf->height;
	img->fd.d_y					=	rendering_context_ptr->back_buf->height-(img->fd.d_y+img->fd.new_y);

	resize_param		(&img->fd);
	
	resize_run			(&img->fd,get_zone_ptr(&rendering_context_ptr->back_buf->pix_buff,0));
	
}


OS_API_C_FUNC(unsigned int) create_surface_2D	(unsigned int surf_type	,unsigned int pix_type,unsigned int d_type,unsigned int width,unsigned int height,unsigned int d_pix_type,unsigned char *data)
{
	rendering_context_t		*rendering_context_ptr;
	surface_2D_t			*new_surface_ptr;
	unsigned char			*dest_mem_buf;
	unsigned int			pix_size;
	unsigned int			nW,nH;
	unsigned int			pix_buf_size;
	unsigned int			scan_line;


	switch(pix_type)
	{
		case GFX_PIX_FORMAT_MONO:		pix_size=1;break;
		case GFX_PIX_FORMAT_INDEXED:	pix_size=8;break;
		case GFX_PIX_FORMAT_GRAY:		pix_size=8;break;
		case GFX_PIX_FORMAT_RGB:		pix_size=24;break;
		case GFX_PIX_FORMAT_RGBA:		pix_size=32;break;
		default:pix_size=0;break;
	}

	if(pix_size==0)return 0;
	
	rendering_context_ptr	=	get_zone_ptr(&rendering_context_ref,0);
	expand_zone				(&rendering_context_ptr->surface_2D_list,(rendering_context_ptr->num_surfaces_2D+1)*sizeof(surface_2D_t));
	new_surface_ptr			=get_zone_ptr(&rendering_context_ptr->surface_2D_list,rendering_context_ptr->num_surfaces_2D*sizeof(surface_2D_t));

	new_surface_ptr->surf_id	=rendering_context_ptr->next_surfaces_2D_id;
	new_surface_ptr->surf_type	=surf_type;
	new_surface_ptr->pix_type	=pix_type;
	new_surface_ptr->pix_width	=width;
	new_surface_ptr->pix_height	=height;
	new_surface_ptr->d_type		=d_type;
	new_surface_ptr->fd.resampler_ref.zone=PTR_NULL;

	new_surface_ptr->pix_stride	=pix_size;

	nW			=1;
	nH			=1;


	while(nW<width)	{nW=nW*2;}
	while(nH<height){nH=nH*2;}

	new_surface_ptr->width		=nW;
	new_surface_ptr->height		=nH;

	scan_line					=(new_surface_ptr->width*new_surface_ptr->pix_stride)/8;
/*
	nW					=1;
	scan_shift			=0;
	while(nW<scan_line)	{nW=nW*2;scan_shift++;}
	new_surface_ptr->scan_line			=	nW;
	new_surface_ptr->scan_line_shift	=	scan_shift;
*/

	new_surface_ptr->scan_line			=	scan_line;
	pix_buf_size						=	new_surface_ptr->scan_line*new_surface_ptr->height;

	new_surface_ptr->pix_buff.zone		=	PTR_NULL;

	allocate_new_zone					(0x00,pix_buf_size,&new_surface_ptr->pix_buff);
	dest_mem_buf			=			get_zone_ptr(&new_surface_ptr->pix_buff,0);

	memset_c(dest_mem_buf,255,pix_buf_size);

	rendering_context_ptr->num_surfaces_2D++;
	rendering_context_ptr->next_surfaces_2D_id++;

	init_resampler		(&new_surface_ptr->fd.resampler_ref);
	

	return new_surface_ptr->surf_id;
}





OS_API_C_FUNC(void)	set_clip_rect	   (int x,int y,unsigned int width,unsigned int height)
{
	rendering_context_t		*rendering_context_ptr;
	rendering_context_ptr	=	get_zone_ptr(&rendering_context_ref,0);

	if(x>0)
		rendering_context_ptr->clip_rect.pos[0]	=x;
	else
		rendering_context_ptr->clip_rect.pos[0]	=0;

	if(y>0)
		rendering_context_ptr->clip_rect.pos[1]	=y;
	else
		rendering_context_ptr->clip_rect.pos[1]	=0;

	if(width>0)
		rendering_context_ptr->clip_rect.size[0]=width;
	else
		rendering_context_ptr->clip_rect.size[0]=rendering_context_ptr->video_mode->XResolution;

	if(height>0)
		rendering_context_ptr->clip_rect.size[1]=height;
	else
		rendering_context_ptr->clip_rect.size[1]=rendering_context_ptr->video_mode->YResolution;

	

	
}

OS_API_C_FUNC(void)	set_clear_color_4uc(unsigned char r,unsigned char g,unsigned char b,unsigned char a)
{
	rendering_context_t		*rendering_context_ptr;
	rendering_context_ptr	=	get_zone_ptr(&rendering_context_ref,0);
	if(rendering_context_ptr==PTR_NULL)return;

	rendering_context_ptr->clear_color[0]=r;
	rendering_context_ptr->clear_color[1]=g;
	rendering_context_ptr->clear_color[2]=b;
	rendering_context_ptr->clear_color[3]=a;
}

OS_API_C_FUNC(void) set_color_4uc		(unsigned char r,unsigned char g,unsigned char b,unsigned char a)
{
	rendering_context_t		*rendering_context_ptr;
	rendering_context_ptr	=	get_zone_ptr(&rendering_context_ref,0);
	if(rendering_context_ptr==PTR_NULL)return;

	rendering_context_ptr->color[0]=r;
	rendering_context_ptr->color[1]=g;
	rendering_context_ptr->color[2]=b;
	rendering_context_ptr->color[3]=a;
}


OS_API_C_FUNC(unsigned int) load_font_surface_data (gfx_rect_t *src_rect,unsigned int glyph_idx,unsigned int src_scan_line,unsigned int pix_type,mem_ptr pix_data)
{
	surface_2D_t		*dest_surf;
	unsigned int		src_w;
	unsigned int		src_h;
	rendering_context_t	 *rendering_context_ptr;

	rendering_context_ptr	=	get_zone_ptr(&rendering_context_ref,0);
	if(rendering_context_ptr->selected_font==PTR_NULL)return 0;
	dest_surf	=	get_surface_2D(rendering_context_ptr->selected_font->surf_id);
	if(dest_surf==PTR_NULL)return 0;

	if((src_rect->size[0]<1)&&(src_rect->size[1]<1))return 1;

	src_w	=	src_rect->size[0];
	src_h	=	src_rect->size[1];

	if((rendering_context_ptr->selected_font->x_pos+rendering_context_ptr->selected_font->glyph_list.max_char_w)>=dest_surf->pix_width)
	{
		rendering_context_ptr->selected_font->x_pos			=	0;
		rendering_context_ptr->selected_font->y_pos			+=	rendering_context_ptr->selected_font->glyph_list.max_char_h;
	}
	rendering_context_ptr->selected_font->glyph_list.glyph_box[glyph_idx].pos[0]		=rendering_context_ptr->selected_font->x_pos;
	rendering_context_ptr->selected_font->glyph_list.glyph_box[glyph_idx].pos[1]		=rendering_context_ptr->selected_font->y_pos;

	rendering_context_ptr->selected_font->glyph_list.glyph_box[glyph_idx].size[0]		=src_w;
	rendering_context_ptr->selected_font->glyph_list.glyph_box[glyph_idx].size[1]		=src_h;
			
	load_surface_data			(dest_surf->surf_id,rendering_context_ptr->selected_font->x_pos,rendering_context_ptr->selected_font->y_pos,src_rect,src_scan_line,pix_type,pix_data);
							
	rendering_context_ptr->selected_font->x_pos	+=	rendering_context_ptr->selected_font->glyph_list.max_char_w;



	return 1;
}

OS_API_C_FUNC(unsigned int) load_surface_data (unsigned int surface_id,unsigned int x_pos,unsigned int y_pos,gfx_rect_t *src_rect,unsigned int src_scan_line,unsigned int pix_type,mem_ptr pix_data)
{
	unsigned int		y;
	surface_2D_t		*dest_surf;
	mem_ptr				dest_addr;
	mem_ptr				src_addr;
	unsigned int		clip_height;
	unsigned int		clip_width;
	unsigned int		pix_size;
	unsigned int		line_size;


	dest_surf	=	get_surface_2D(surface_id);

	if(dest_surf==PTR_NULL)return 0;

	if((src_rect->size[0]==0)&&(src_rect->size[1]==0))return 1;
	if(x_pos>=dest_surf->pix_width)return 1;
	if(y_pos>=dest_surf->pix_height)return 1;

	dest_addr	=	get_surf_addr(dest_surf,x_pos,y_pos);
	src_addr	=	pix_data;

	switch(pix_type)
	{
		case GFX_PIX_FORMAT_MONO:		pix_size=1;break;
		case GFX_PIX_FORMAT_INDEXED:	pix_size=8;break;
		case GFX_PIX_FORMAT_GRAY:		pix_size=8;break;
		case GFX_PIX_FORMAT_RGB:		pix_size=24;break;
		case GFX_PIX_FORMAT_RGBA:		pix_size=32;break;
		default:pix_size=0;break;
	}

	if(pix_size==0)return 0;

	if((x_pos+src_rect->size[0])>=dest_surf->pix_width)
		clip_width=dest_surf->pix_width-x_pos;
	else
		clip_width=src_rect->size[0];

	if((y_pos+src_rect->size[1])>=dest_surf->pix_height)
		clip_height=dest_surf->pix_height-y_pos;
	else
		clip_height=src_rect->size[1];

	y			=	0;
	line_size	=	clip_width;
	while(y<clip_height)
	{
		if((dest_surf->pix_type==GFX_PIX_FORMAT_RGBA)&&(pix_type==GFX_PIX_FORMAT_RGBA))
		{
			char *dst_ptr=dest_addr;
			char *src_ptr=src_addr;
			unsigned int n;

			n=0;
			while(n<line_size)
			{
				dst_ptr[n*4+0]=src_ptr[n*4+0];
				dst_ptr[n*4+1]=src_ptr[n*4+1];
				dst_ptr[n*4+2]=src_ptr[n*4+2];
				dst_ptr[n*4+3]=src_ptr[n*4+3];
				n++;
			}
		}
		else if(pix_type==dest_surf->pix_type)
			memcpy_c		(dest_addr,src_addr,((line_size*pix_size)>>3));

		if((dest_surf->pix_type==GFX_PIX_FORMAT_RGBA)&&(pix_type==GFX_PIX_FORMAT_GRAY))
		{
			char *dst_ptr=dest_addr;
			char *src_ptr=src_addr;
			unsigned int n;

			n=0;
			while(n<line_size)
			{
				dst_ptr[n*4+0]=(255*src_ptr[n])>>8;
				dst_ptr[n*4+1]=(255*src_ptr[n])>>8;
				dst_ptr[n*4+2]=(255*src_ptr[n])>>8;
				dst_ptr[n*4+3]=src_ptr[n];
				n++;
			}
		}




		y++;
		src_addr	=	mem_add(src_addr	,src_scan_line);
		dest_addr	=	mem_add(dest_addr	,dest_surf->scan_line);
	}
	return 1;
}

OS_API_C_FUNC(void) blit_surf			(unsigned int surface_id, int x_pos, int y_pos,gfx_rect_t *src_rect)
{
	rendering_context_t	*rendering_context_ptr;
	surface_2D_t		*src_surf;
	surface_2D_t		*back_buf_ptr;
	unsigned char		*dst_line_buffer;
	unsigned char		*dst_mem_buffer;
	unsigned char		*src_line_buffer;
	unsigned char		*src_mem_buffer;
	unsigned int		bit_pix_src,bit_pix_dst;
	unsigned int		draw_width,draw_height;
	int					draw_x,draw_y;
	int					src_x,src_y;
	unsigned int		d_x,d_y;
	unsigned char		src_pix[4];
	unsigned char		dest_pix[4];
	unsigned char		inv_alpha;
	gfx_rect_t			*clip_rect,my_rect;


	rendering_context_ptr	=	get_zone_ptr(&rendering_context_ref,0);
	if(rendering_context_ptr==PTR_NULL)return ;
	if(rendering_context_ptr==PTR_INVALID)return ;
	if(rendering_context_ptr->back_buf==PTR_NULL)return ;



	
	src_surf		=	get_surface_2D(surface_id);
	if(src_surf==PTR_NULL)return ;

	if(src_rect==PTR_NULL)
	{
		src_rect			=&my_rect;
		src_rect->pos[0]	=0;
		src_rect->pos[1]	=0;

		src_rect->size[0]	=src_surf->pix_width;
		src_rect->size[1]	=src_surf->pix_height;

	}

	back_buf_ptr	=	rendering_context_ptr->back_buf;
	clip_rect		=	&rendering_context_ptr->clip_rect;


	if(x_pos<0)return;
	if(y_pos<0)return;

	if(x_pos>=(clip_rect->pos[0]+clip_rect->size[0]))return;
	if(y_pos>=(clip_rect->pos[1]+clip_rect->size[1]))return;

	if((x_pos+src_rect->size[0])<clip_rect->pos[0])return;
	if((y_pos+src_rect->size[1])<clip_rect->pos[1])return;


	if(x_pos<clip_rect->pos[0])
	{
		draw_x		=clip_rect->pos[0];
		src_x		=draw_x-x_pos;
		if(src_x>=src_rect->size[0])return;
		draw_width	=src_rect->size[0]-src_x;
	}
	else
	{
		draw_x		=	x_pos;
		src_x		=	0;
		draw_width	=	src_rect->size[0];
	}

	if(y_pos<clip_rect->pos[1])
	{
		draw_y		=clip_rect->pos[1];
		src_y		=draw_y-y_pos;

		if(src_y>=src_rect->size[1])return;
		draw_height	=src_rect->size[1]-src_y;
	}
	else
	{
		draw_y		=	y_pos;
		src_y		=	0;
		draw_height	=	src_rect->size[1];
	}

	if( (int)(draw_x+draw_width)>
		(clip_rect->pos[0]+clip_rect->size[0]))
	{
		draw_width	=	(clip_rect->pos[0]+clip_rect->size[0])-draw_x;
	}

	if( (int)(draw_y+draw_height)>
		(clip_rect->pos[1]+clip_rect->size[1]))
	{
		draw_height	=	(clip_rect->pos[1]+clip_rect->size[1])-draw_y;
	}


	dst_mem_buffer		=	get_surf_addr(back_buf_ptr,draw_x,draw_y);
	src_mem_buffer		=   get_surf_addr(src_surf,src_rect->pos[0]+src_x,src_rect->pos[1]+src_y);



	d_y=0;
	while(d_y<draw_height)
	{
		d_x					 =	0;
		dst_line_buffer		 =	dst_mem_buffer;
		src_line_buffer		 =	src_mem_buffer;
		bit_pix_src			 =	0;
		bit_pix_dst			 =	0;
		while(d_x<draw_width)
		{

			if(src_surf->pix_stride==8)
			{
				src_pix[0]	= (rendering_context_ptr->color[0]*(*src_line_buffer))>>8;
				src_pix[1]	= (rendering_context_ptr->color[1]*(*src_line_buffer))>>8;
				src_pix[2]	= (rendering_context_ptr->color[2]*(*src_line_buffer))>>8;
				inv_alpha	= 255-(*src_line_buffer);
			}

			if(src_surf->pix_stride==32)
			{
				src_pix[0]	= (rendering_context_ptr->color[0]*(src_line_buffer[0]))>>8;
				src_pix[1]	= (rendering_context_ptr->color[1]*(src_line_buffer[1]))>>8;
				src_pix[2]	= (rendering_context_ptr->color[2]*(src_line_buffer[2]))>>8;
				inv_alpha	= 255-(src_line_buffer[3]);
			}

			if(back_buf_ptr->pix_stride==32)
			{
				*((unsigned int *)(dest_pix))=*((unsigned int *)(dst_line_buffer));
				dst_line_buffer[0]			=	((dest_pix[0]*inv_alpha)>>8)+src_pix[0];
				dst_line_buffer[1]			=	((dest_pix[1]*inv_alpha)>>8)+src_pix[1];
				dst_line_buffer[2]			=	((dest_pix[2]*inv_alpha)>>8)+src_pix[2];
			}

			bit_pix_dst			+=	back_buf_ptr->pix_stride;
			while(bit_pix_dst>=8)
			{
				dst_line_buffer++;
				bit_pix_dst-=8;
			}

			bit_pix_src			+=	src_surf->pix_stride;
			while(bit_pix_src>=8)
			{
				src_line_buffer++;
				bit_pix_src-=8;
			}


			d_x++;
		}
		dst_mem_buffer=mem_add(dst_mem_buffer,back_buf_ptr->scan_line);
		src_mem_buffer=mem_add(src_mem_buffer,src_surf->scan_line);
		d_y++;
	}
}

mem_ptr get_image_pixels_ptr	(gfx_image_t	*img,unsigned int pos_x,unsigned int pos_y)
{
	mem_ptr		 *line_table;
	mem_ptr		line_ptr;
	if(pos_y>=img->height)return NULL;
	if(pos_x>=img->width)return NULL;

	line_table	=	get_zone_ptr(&img->line_table,0);
	line_ptr	=	line_table[pos_y];

	switch(img->pixfmt)
	{
		case GFX_PIX_FORMAT_RGBA:
			line_ptr = mem_add(line_ptr,pos_x*4);
		break;
	}

	return line_ptr;
}


OS_API_C_FUNC(void) draw_image	(int x_pos, int y_pos,gfx_image_t *img,gfx_rect_t *src_rect)
{
	rendering_context_t	*rendering_context_ptr;
	surface_2D_t		*back_buf_ptr;
	unsigned char		*dst_line_buffer;
	unsigned char		*dst_mem_buffer;
	unsigned char		*src_line_buffer;
	unsigned char		*src_mem_buffer;
	unsigned int		bit_pix_src,bit_pix_dst;
	int					draw_width,draw_height;
	int					draw_x,draw_y;
	int					src_x,src_y;
	unsigned int		d_x,d_y;
	unsigned char		src_pix[4];
	unsigned char		dest_pix[4];
	unsigned char		inv_alpha,alpha;
	gfx_rect_t			*clip_rect,my_rect;


	rendering_context_ptr	=	get_zone_ptr(&rendering_context_ref,0);
	if(rendering_context_ptr==PTR_NULL)return ;
	if(rendering_context_ptr==PTR_INVALID)return ;
	if(rendering_context_ptr->back_buf==PTR_NULL)return ;

	
	back_buf_ptr	=	rendering_context_ptr->back_buf;
	clip_rect		=	&rendering_context_ptr->clip_rect;


	if(src_rect==PTR_NULL)
	{
		src_rect			=&my_rect;
		src_rect->pos[0]	=0;
		src_rect->pos[1]	=0;

		src_rect->size[0]	=img->width;
		src_rect->size[1]	=img->height;

	}

	if(x_pos>=(clip_rect->pos[0]+clip_rect->size[0]))return;
	if(y_pos>=(clip_rect->pos[1]+clip_rect->size[1]))return;

	if((x_pos+src_rect->size[0])<clip_rect->pos[0])return;
	if((y_pos+src_rect->size[1])<clip_rect->pos[1])return;


	src_x		=	0;
	draw_x		=	x_pos;
	
	
	if(draw_x<clip_rect->pos[0])
	{
		src_x		=	clip_rect->pos[0]-draw_x;
		draw_x		=	clip_rect->pos[0];
		draw_width	=	src_rect->size[0]-src_x;
	}
	else
	{
		
		draw_width	=	src_rect->size[0];
	}

	draw_y		=	y_pos;
	src_y		=	0;

	if(draw_y<clip_rect->pos[1])
	{
		src_y		=	clip_rect->pos[1]-draw_y;
		draw_y		=	clip_rect->pos[1];
		draw_height	=	src_rect->size[1]-src_y;
	}
	else
	{
		
		draw_height	=	src_rect->size[1];
	}



	if( (int)(draw_x+draw_width)>=
		(clip_rect->pos[0]+clip_rect->size[0]))
	{
		draw_width	=	(clip_rect->pos[0]+clip_rect->size[0])-(draw_x+src_x);
	}

	if( (int)(draw_y+draw_height)>=
		(clip_rect->pos[1]+clip_rect->size[1]))
	{
		draw_height	=	(clip_rect->pos[1]+clip_rect->size[1])-(draw_y+src_y);
	}


	dst_mem_buffer		=	get_surf_addr(back_buf_ptr,draw_x,draw_y);
	src_mem_buffer		=   get_image_pixels_ptr(img,src_rect->pos[0]+src_x,src_rect->pos[1]+src_y);


	d_y=0;
	while(d_y<draw_height)
	{
		d_x					 =	0;
		dst_line_buffer		 =	dst_mem_buffer;
		src_line_buffer		 =	src_mem_buffer;
		bit_pix_src			 =	0;
		bit_pix_dst			 =	0;
		while(d_x<draw_width)
		{

			switch(img->pixfmt)
			{
				case GFX_PIX_FORMAT_RGBA:
					alpha		= src_line_buffer[3];
					inv_alpha	= 255-alpha;

					src_pix[0]	= ((src_line_buffer[0]));
					src_pix[1]	= ((src_line_buffer[1]));
					src_pix[2]	= ((src_line_buffer[2]));
				break;
			}


			if(back_buf_ptr->pix_stride==32)
			{
			
				*((unsigned int *)(dest_pix))=*((unsigned int *)(dst_line_buffer));

				dst_line_buffer[0]			=	((dest_pix[0]*inv_alpha)>>8)+((src_pix[0]*alpha)>>8);
				dst_line_buffer[1]			=	((dest_pix[1]*inv_alpha)>>8)+((src_pix[1]*alpha)>>8);
				dst_line_buffer[2]			=	((dest_pix[2]*inv_alpha)>>8)+((src_pix[2]*alpha)>>8);

			}

			bit_pix_dst			+=	back_buf_ptr->pix_stride;
			while(bit_pix_dst>=8)
			{
				dst_line_buffer++;
				bit_pix_dst-=8;
			}

			bit_pix_src			+=	img->pix_stride;
			while(bit_pix_src>=8)
			{
				src_line_buffer++;	
				bit_pix_src-=8;
			}


			d_x++;
		}
		dst_mem_buffer=mem_add(dst_mem_buffer,back_buf_ptr->scan_line);
		src_mem_buffer=mem_add(src_mem_buffer,img->line_size);
		d_y++;
	}
}

OS_API_C_FUNC(void) draw_point			(const vec_2s_t	p1)
{


}

OS_API_C_FUNC(void) draw_hspan_list		(struct gfx_hspan_list_t *list,int x,int y)
{
	rendering_context_t			*rendering_context_ptr;
	surface_2D_t				*bck_buf;
	struct gfx_hspan_t			*span_list;
	struct gfx_rect				*clip_rect;
	unsigned int				n;
	int							x_pos,y_pos;

	unsigned char				*dst;
	
	rendering_context_ptr	=	get_zone_ptr(&rendering_context_ref,0);
	if(rendering_context_ptr==PTR_NULL)return;

	bck_buf=rendering_context_ptr->back_buf;
	if(bck_buf==PTR_NULL)return;

	clip_rect		=	&rendering_context_ptr->clip_rect;

	dst			=	get_zone_ptr(&bck_buf->pix_buff,0);
	span_list	=	get_zone_ptr(&list->span_list,0);
	n=0;
	while(n<list->num_spans)
	{
		unsigned int span_start,span_end;
	
		x_pos		=	(span_list->pos[0]+x);
		y_pos		=	(span_list->pos[1]+y);
		span_start	=	(bck_buf->scan_line*y_pos)+(x_pos<<2);
		span_end	=	span_start+(span_list->len<<2);

		
		while(span_start<span_end)
		{
			unsigned char inv_cov;
			inv_cov=255-span_list->coverage;

			if( (x_pos>=clip_rect->pos[0])&&(x_pos<(clip_rect->pos[0]+clip_rect->size[0])&&
				(y_pos>=clip_rect->pos[1])&&(y_pos<(clip_rect->pos[1]+clip_rect->size[1])))
				)
			{
				dst[span_start+0]=((dst[span_start+0]*inv_cov)>>8)+((rendering_context_ptr->color[0]*span_list->coverage)>>8);
				dst[span_start+1]=((dst[span_start+1]*inv_cov)>>8)+((rendering_context_ptr->color[1]*span_list->coverage)>>8);
				dst[span_start+2]=((dst[span_start+2]*inv_cov)>>8)+((rendering_context_ptr->color[2]*span_list->coverage)>>8);
				dst[span_start+3]=span_list->coverage;
			}

			span_start+=4;
			x_pos++;
		}
		span_list++;
		n++;
	}

}
OS_API_C_FUNC(void) draw_line			(const vec_2s_t	p1,const vec_2s_t p2)
{
	rendering_context_t			*rendering_context_ptr;
	surface_2D_t				*bck_buf;
	int							x,y;
	int							dx,dy;
	int							sx,sy,err;
	unsigned char				*dst;


	rendering_context_ptr	=	get_zone_ptr(&rendering_context_ref,0);
	if(rendering_context_ptr==PTR_NULL)return;

	bck_buf=rendering_context_ptr->back_buf;
	if(bck_buf==PTR_NULL)return;


	dst	=	get_zone_ptr(&bck_buf->pix_buff,0);

	dx=p2[0]-p1[0];
	dy=p2[1]-p1[1];
	if((dx==0)&&(dy==0))return;
	
	if(dx>0)
		sx=1;
	else
	{
		dx=-dx;
		sx=0;
	}

	if(dy>0)
		sy=1;
	else
	{
		dy=dy;
		sy=0;
	}
	if(dx>dy)
	{
		err=0;
		x=p1[0];
		y=p1[1];
		while(x!=p2[0])
		{
			*((unsigned int *)(&dst[(bck_buf->scan_line*y+x)<<2]))=*((unsigned int *)(&rendering_context_ptr->color));
			
			err=err+dy;
			while(err>=dx)
			{		
				if(sy)
					y++;
				else
					y--;
				err-=dx;
			}
			if(sx)
				x++;
			else
				x--;
		}
	}
	else
	{
		err	=0;
		x	=p1[0];
		y	=p1[1];
		while(y!=p2[1])	
		{
			*((unsigned int *)(&dst[(bck_buf->scan_line*y+x)<<2]))=*((unsigned int *)(&rendering_context_ptr->color));
			
			err=err+dx;
			while(err>=dy)
			{		
				if(sx)
					x++;
				else
					x--;
				
				err-=dy;
			}
			if(sy)
				y++;
			else
				y--;
		}
	}
}
/*
void DrawLine			(DeviceCommon *Device,int x1,int y1,int x2,int y2,unsigned int Color)
{
	unsigned char *Dest			=Device->PixelPtr;
	unsigned int  *LineTable	=Device->LineTable;
	unsigned int	*P;
	int				x,y;
	int				dx,dy;
	int				sx,sy,err;

	dx=x2-x1;
	dy=y2-y1;
	if((dx==0)&&(dy==0))return;
	
	if(dx>0)
		sx=1;
	else
	{
		dx=-dx;
		sx=0;
	}

	if(dy>0)
		sy=1;
	else
	{
		dy=dy;
		sy=0;
	}
	if(dx>dy)
	{
		err=0;
		x=x1;
		y=y1;
		while(x!=x2)
		{
			*((unsigned int *)(&Dest[(LineTable[y]+x)<<2]))=Color;
			
			err=err+dy;
			while(err>=dx)
			{		
				if(sy)
					y++;
				else
					y--;
				err-=dx;
			}
			if(sx)
				x++;
			else
				x--;
		}
	}
	else
	{
		err	=0;
		x	=x1;
		y	=y1;
		while(y!=y2)	
		{
			*((unsigned int *)(&Dest[(LineTable[y]+x)<<2]))=Color;
			
			err=err+dx;
			while(err>=dy)
			{		
				if(sx)
					x++;
				else
					x--;
				
				err-=dy;
			}
			if(sy)
				y++;
			else
				y--;
		}
	}
}
}
*/
OS_API_C_FUNC(void) draw_rect			(const gfx_rect_t *rect)
{
	rendering_context_t			*rendering_context_ptr;
	surface_2D_t				*back_buf_ptr;
	unsigned int				draw_width,draw_height;
	unsigned int				p_x,p_y;
	unsigned int				bit_pix;
	unsigned int				fill_color;
	unsigned char				*dst_mem_buffer;
	unsigned char				*line_buffer;
	gfx_rect_t					*clip_rect;
	unsigned int				draw_x,draw_y;
	unsigned int				src_x,src_y;
	rendering_context_ptr	=	get_zone_ptr(&rendering_context_ref,0);

	if(rendering_context_ptr->back_buf==PTR_NULL)return;
	
	back_buf_ptr	=	rendering_context_ptr->back_buf;
	clip_rect		=	&rendering_context_ptr->clip_rect;
	
	if(rect->pos[0]>=(clip_rect->pos[0]+clip_rect->size[0]))return;
	if(rect->pos[1]>=(clip_rect->pos[1]+clip_rect->size[1]))return;

	if((rect->pos[0]+rect->size[0])<clip_rect->pos[0])return;
	if((rect->pos[1]+rect->size[1])<clip_rect->pos[1])return;

	if(rect->pos[0]<clip_rect->pos[0])
	{
		draw_x		=clip_rect->pos[0];
		src_x		=draw_x-rect->pos[0];
		draw_width	=rect->size[0]-src_x;
	}
	else
	{
		draw_x		=	rect->pos[0];
		src_x		=	0;
		draw_width	=	rect->size[0];
	}

	if(rect->pos[1]<clip_rect->pos[1])
	{
		draw_y		=	clip_rect->pos[1];
		src_y		=	draw_y-rect->pos[1];
		draw_height	=	rect->size[1]-src_y;
	}
	else
	{
		draw_y		=	rect->pos[1];
		src_y		=	0;
		draw_height	=	rect->size[1];
	}


	if( (int)(draw_x+draw_width)>
		(clip_rect->pos[0]+clip_rect->size[0]))
	{
		draw_width	=	(clip_rect->pos[0]+clip_rect->size[0])-draw_x;
	}

	if( (int)(draw_y+draw_height)>
		(clip_rect->pos[1]+clip_rect->size[1]))
	{
		draw_height	=	(clip_rect->pos[1]+clip_rect->size[1])-draw_y;
	}

	dst_mem_buffer		=	get_surf_addr(back_buf_ptr,draw_x,draw_y);
	fill_color			=	*((unsigned int *)(rendering_context_ptr->color));

	p_y=0;
	while(p_y<draw_height)
	{
		p_x=0;
		line_buffer			 =	dst_mem_buffer;
		bit_pix				 =	0;
		while(p_x<draw_width)
		{
			if(back_buf_ptr->pix_stride==32)
				*((unsigned int *)(line_buffer))=fill_color;

			bit_pix			+=	back_buf_ptr->pix_stride;
			while(bit_pix>=8)
			{
				line_buffer		++;
				bit_pix-=8;
			}
			p_x++;
		}
		dst_mem_buffer=mem_add(dst_mem_buffer,back_buf_ptr->scan_line);
		p_y++;
	}

}
OS_API_C_FUNC(void) clear_buffers(unsigned int flags)
{
	unsigned char			*dest_mem_buffer,*dest_ln_buffer;
	rendering_context_t		*rendering_context_ptr;
	unsigned int			n_lines,x;	
	unsigned int			dest_stride;
	rendering_context_ptr	=	get_zone_ptr(&rendering_context_ref,0);

	if(rendering_context_ptr->back_buf==PTR_NULL)
		return;

	dest_mem_buffer			=	get_zone_ptr(&rendering_context_ptr->back_buf->pix_buff,0);
	dest_stride				=	rendering_context_ptr->back_buf->scan_line;
	
	n_lines			=	0;
	while(n_lines<rendering_context_ptr->video_mode->YResolution)
	{
		dest_ln_buffer=dest_mem_buffer;
		x=0;
		while(x<rendering_context_ptr->video_mode->XResolution)
		{
			dest_ln_buffer[0]=rendering_context_ptr->clear_color[0];
			dest_ln_buffer[1]=rendering_context_ptr->clear_color[1];
			dest_ln_buffer[2]=rendering_context_ptr->clear_color[2];
			dest_ln_buffer+=4;
			x++;
		}

		

		dest_mem_buffer	=mem_add(dest_mem_buffer,dest_stride);
		n_lines++;
	}
}

OS_API_C_FUNC(void) swap_buffers(unsigned int flags)
{
	unsigned char			*dest_mem_buffer;
	unsigned char			*src_mem_buffer;
	rendering_context_t		*rendering_context_ptr;
	unsigned int			n_lines;
	unsigned int			dest_stride;
	unsigned int			src_stride;
	
	rendering_context_ptr	=	get_zone_ptr(&rendering_context_ref,0);

	if(rendering_context_ptr->back_buf==PTR_NULL)
		return;

	
	
	dest_mem_buffer			=	uint_to_mem(rendering_context_ptr->video_mode->PhysBasePtr);
	dest_stride				=	rendering_context_ptr->video_mode->BytesPerScanLine;

	src_mem_buffer			=	get_zone_ptr(&rendering_context_ptr->back_buf->pix_buff,0);
	src_stride				=	rendering_context_ptr->back_buf->scan_line;



	n_lines			=	0;
	while(n_lines<rendering_context_ptr->video_mode->YResolution)
	{
		memcpy_c(dest_mem_buffer,src_mem_buffer,rendering_context_ptr->video_mode->XResolution*4);

		dest_mem_buffer	=mem_add(dest_mem_buffer,dest_stride);
		src_mem_buffer	=mem_add(src_mem_buffer,src_stride);
		n_lines++;
	}
	
	
}
