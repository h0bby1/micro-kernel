#include "uhci.h"

enum usb_transfer_direction
{
USB_TRANSFER_DIRECTION_HOST_TO_DEVICE=0,
USB_TRANSFER_DIRECTION_DEVICE_TO_HOST=1
};

enum usb_transfer_type
{
USB_TRANSFER_TYPE_STD		= 0,
USB_TRANSFER_TYPE_CLASS		= 1,
USB_TRANSFER_TYPE_VENDOR	= 2,
USB_TRANSFER_TYPE_RESERVED	= 3
};

enum usb_transfer_recipient
{

USB_TRANSFER_RECIPIENT_DEVICE	=	0,  
USB_TRANSFER_RECIPIENT_INTERFACE=	1,
USB_TRANSFER_RECIPIENT_ENDPOINT	=	2,
USB_TRANSFER_RECIPIENT_OTHER	=	3  
};

enum usb_transfer_request
{
USB_TRANSFER_REQ_GET_STATUS			=0x00,
USB_TRANSFER_REQ_CLEAR_FEATURE  	=0x01,
USB_TRANSFER_REQ_SET_FEATURE		=0x03,
USB_TRANSFER_REQ_SET_ADDRESS		=0x05,
USB_TRANSFER_REQ_GET_DESCRIPTOR 	=0x06,
USB_TRANSFER_REQ_SET_DESCRIPTOR 	=0x07,
USB_TRANSFER_REQ_GET_CONFIGURATION  =0x08,
USB_TRANSFER_REQ_SET_CONFIGURATION  =0x09
};

enum
{
USB_TRANSFER_DESC_TYPE_DEVICE_DESCRIPTOR	= 0x01,
USB_TRANSFER_DESC_TYPE_CONFIG_DESCRIPTOR	= 0x02,
USB_TRANSFER_DESC_TYPE_STRING_DESCRIPTOR	= 0x03,
USB_TRANSFER_DESC_TYPE_IFACE_DESCRIPTOR		= 0x04,
USB_TRANSFER_DESC_TYPE_ENDPT_DESCRIPTOR		= 0x05
}usb_transfer_descriptor_type;


/*
Configuration Descriptors
A USB device can have several different configurations although the majority of devices are simple and only have one. 
The configuration descriptor specifies how the device is powered, what the maximum power consumption is, the number of interfaces it has. 
Therefore it is possible to have two configurations, one for when the device is bus powered and another when it is mains powered. 
As this is a "header" to the Interface descriptors, its also feasible to have one configuration 
using a different transfer mode to that of another configuration.

Once all the configurations have been examined by the host, the host will send a SetConfiguration command 
with a non zero value which matches the bConfigurationValue of one of the configurations. 
This is used to select the desired configuration.

When the configuration descriptor is read, it returns the entire configuration hierarchy which includes 
all related interface and endpoint descriptors. 
*/
								//Offset   Size	 Value	 Description
typedef struct
{
unsigned char  bLength				;//0	    1	 Number		Size of Descriptor in Bytes
unsigned char  bDescriptorType		;//1	    1	 Constant	Configuration Descriptor (0x02)

//The wTotalLength field reflects the number of bytes in the hierarchy.
unsigned short wTotalLength			;//2	    2	 Number		Total length in bytes of data returned
//bNumInterfaces specifies the number of interfaces present for this configuration.
unsigned char  bNumInterfaces		;//4	    1	 Number		Number of Interfaces
//bConfigurationValue is used by the SetConfiguration request to select this configuration.
unsigned char  bConfigurationValue	;//5	    1	 Number		Value to use as an argument to select this configuration
//iConfiguration is a index to a string descriptor describing the configuration in human readable form.
unsigned char  iConfiguration		;//6	    1	 Index		Index of String Descriptor describing this configuration
//bmAttributes specify power parameters for the configuration. If a device is self powered, it sets D6. Bit D7 was used in USB 1.0 to indicate a bus powered device, but this is now done by bMaxPower. If a device uses any power from the bus, whether it be as a bus powered device or as a self powered device, it must report its power consumption in bMaxPower. Devices can also support remote wakeup which allows the device to wake up the host when the host is in suspend.
//D7 Reserved, set to 1. (USB 1.0 Bus Powered)
//D6 Self Powered
//D5 Remote Wakeup
//D4..0 Reserved, set to 0.
unsigned char  bmAttributes			;//7	    1	 bitmap
//bMaxPower defines the maximum power the device will drain from the bus. This is in 2mA units, thus a maximum of approximately 500mA can be specified. The specification allows a high powered bus powered device to drain no more than 500mA from Vbus. If a device loses external power, then it must not drain more than indicated in bMaxPower. It should fail any operation it cannot perform without external power.
unsigned char  bMaxPower			;//8	    1	 mA			Maximum Power Consumption in 2mA units
}usb_configuration_desc;

/*
Device Descriptors
The device descriptor of a USB device represents the entire device. 
As a result a USB device can only have one device descriptor. 

It specifies some basic, yet important information about the device such as 
the supported USB version, maximum packet size, vendor and product IDs and the number of possible configurations
the device can have. The format of the device descriptor is shown below.
*/

typedef struct
{
								     	//Offset	Size	 Value	 Description
unsigned char  bLength				;	//0		 	 1		Number		Size of the Descriptor in Bytes (18 bytes)
unsigned char  bDescriptorType		;	//1		 	 1		Constant	Device Descriptor (0x01)
//The bcdUSB field reports the highest version of USB the device supports. The value is in binary coded decimal with a format of 0xJJMN where JJ is the major version number, M is the minor version number and N is the sub minor version number. e.g. USB 2.0 is reported as 0x0200, USB 1.1 as 0x0110 and USB 1.0 as 0x0100.
unsigned short bcdUSB				;	//2	 	 	 2		BCD			USB Specification Number which device complies too.
//The bDeviceClass, bDeviceSubClass and bDeviceProtocol are used by the operating system to find a class driver for your device. 
//Typically only the bDeviceClass is set at the device level. 
//Most class specifications choose to identify itself at the interface level and as a result set the bDeviceClass as 0x00.

//This allows for the one device to support multiple classes.
//If equal to Zero, each interface specifies it�s own class code
//If equal to 0xFF, the class code is vendor specified.
//Otherwise field is valid Class Code.
unsigned char  bDeviceClass			;	//4		 	 1	 	Class		Class Code (Assigned by USB Org)
unsigned char  bDeviceSubClass		;	//5		 	 1	 	SubClass	Subclass Code (Assigned by USB Org)
unsigned char  bDeviceProtocol		;	//6		 	 1	 	Protocol	Protocol Code (Assigned by USB Org)
//The bMaxPacketSize field reports the maximum packet size for endpoint zero. All devices must support endpoint zero.
unsigned char  bMaxPacketSize		;	//7		 	 1	 	Number		Maximum Packet Size for Zero Endpoint. Valid Sizes are 8, 16, 32, 64
//The idVendor and idProduct are used by the operating system to find a driver for your device. The Vendor ID is assigned by the USB-IF.
unsigned short idVendor				;	//8		 	 2	 	ID			Vendor ID (Assigned by USB Org)
unsigned short idProduct			;	//10		 2	 	ID			Product ID (Assigned by Manufacturer)
//The bcdDevice has the same format than the bcdUSB and is used to provide a device version number. This value is assigned by the developer.
unsigned short bcdDevice			;	//12		 2	 	BCD			Device Release Number
//Three string descriptors exist to provide details of the manufacturer, product and serial number. 
//There is no requirement to have string descriptors. 
//If no string descriptor is present, a index of zero should be used.
unsigned char  iManufacturer		;	//14		 1	 	Index		Index of Manufacturer String Descriptor
unsigned char  iProduct				;	//15		 1	 	Index		Index of Product String Descriptor
unsigned char  iSerialNumber		;	//16		 1	 	Index		Index of Serial Number String Descriptor
//bNumConfigurations defines the number of configurations the device supports at its current speed.
unsigned char  bNumConfigurations	;	//17		 1	 	Integer		Number of Possible Configurations
}usb_device_desc;


/*
String Descriptors
String descriptors provide human readable information and are optional. 
If they are not used, any string index fields of descriptors must be set to zero indicating there is no string descriptor available.
The strings are encoded in the Unicode format and products can be made to support multiple languages. 
String Index 0 should return a list of supported languages. 
A list of USB Language IDs can be found in Universal Serial Bus Language Identifiers (LANGIDs) version 1.0
*/


typedef struct
{
								//Offset	Size	 Value	 Description
unsigned char  bLength			;//0		1		Number		Size of Descriptor in Bytes
unsigned char  bDescriptorType	;//1		1		Constant	String Descriptor (0x03)
unsigned short *wLANGID			;//2		2		number		Supported Language Code Zero (e.g. 0x0409 English - United States)
}usb_string_desc_zero;

//The above String Descriptor shows the format of String Descriptor Zero. 
//The host should read this descriptor to determine what languages are available. 
//If a language is supported, it can then be referenced by sending the language ID in the wIndex field of a Get Descriptor(String) request.
//All subsequent strings take on the format below,

typedef struct
{
									 //Offset	Size	 Value		Description
unsigned char bLength				;// 0		 1		 Number		Size of Descriptor in Bytes
unsigned char bDescriptorType		;// 1		 1		 Constant	String Descriptor (0x03)
unsigned char *bString				;// 2		 n		 Unicode	Unicode Encoded String
}usb_string_desc;


unsigned char usb_create_request_type_byte(enum usb_transfer_direction dir,enum usb_transfer_type type,enum usb_transfer_recipient recipient)
{
	unsigned char dir_bits,type_bits,recpt_bits;
	unsigned char RequestType;

	switch(dir)
	{
		case USB_TRANSFER_DIRECTION_HOST_TO_DEVICE: dir_bits=0;	break;
		case USB_TRANSFER_DIRECTION_DEVICE_TO_HOST:	dir_bits=1; break;
	}

	switch(type)
	{
		case USB_TRANSFER_TYPE_STD		:type_bits=0;break;
		case USB_TRANSFER_TYPE_CLASS	:type_bits=1;break;	
		case USB_TRANSFER_TYPE_VENDOR	:type_bits=2;break;
		case USB_TRANSFER_TYPE_RESERVED	:type_bits=3;break;
	}

	switch(recipient)
	{
		case USB_TRANSFER_RECIPIENT_DEVICE		:recpt_bits=0;break;
		case USB_TRANSFER_RECIPIENT_INTERFACE	:recpt_bits=1;break;	
		case USB_TRANSFER_RECIPIENT_ENDPOINT	:recpt_bits=2;break;
		case USB_TRANSFER_RECIPIENT_OTHER		:recpt_bits=3;break;
	}
	
	RequestType	=	((dir_bits&0x1)<<7)|((type_bits&0x3)<<5)|(recpt_bits&0x3);

	return RequestType;
}


void usb_create_get_status_packet_device(usb_setup_packet *packet)
{
	packet->bmRequestType	=	usb_create_request_type_byte	(USB_TRANSFER_DIRECTION_DEVICE_TO_HOST,USB_TRANSFER_TYPE_STD,USB_TRANSFER_RECIPIENT_DEVICE);
	packet->bRequest		=	USB_TRANSFER_REQ_GET_STATUS;
	packet->wValue			=	0;//Zero
	packet->wIndex			=	0;//Zero
	packet->wLength			=	2;//Two

	//data Device Status
	//The Get Status request directed at the device will return two bytes during the data stage with the following format,
	//D15	 D14	 D13	 D12	 D11	 D10	 D9	 D8	 D7	 D6	 D5	 D4	 D3	 D2		D1				D0
	//Reserved																		Remote Wakeup	Self Powered

}


void usb_create_set_configuration_packet_device(usb_setup_packet *packet)
{
	packet->bmRequestType	=	usb_create_request_type_byte	(USB_TRANSFER_DIRECTION_HOST_TO_DEVICE,USB_TRANSFER_TYPE_STD,USB_TRANSFER_RECIPIENT_DEVICE);
	packet->bRequest		=	USB_TRANSFER_REQ_SET_CONFIGURATION;
	packet->wValue			=	0;	//Configuration Value
	packet->wIndex			=	0;	//Zero
	packet->wLength			=	0;  //Zero

	// Set Configuration is used to enable a device. 
	//It should contain the value of bConfigurationValue of the desired configuration descriptor 
	//in the lower byte of wValue to select which configuration to enable.
	//none
}

void usb_create_get_configuration_packet_device(usb_setup_packet *packet)
{
	packet->bmRequestType	=	usb_create_request_type_byte	(USB_TRANSFER_DIRECTION_DEVICE_TO_HOST,USB_TRANSFER_TYPE_STD,USB_TRANSFER_RECIPIENT_DEVICE);
	packet->bRequest		=	USB_TRANSFER_REQ_GET_CONFIGURATION;
	packet->wValue			=	0;	//Zero
	packet->wIndex			=	0;	//Zero
	packet->wLength			=	1;  //One

	//data Configuration Value
	//In the case of a Get Configuration request, a byte will be returned during the data stage indicating the devices status. 
	//A zero value means the device is not configured and a non-zero value indicates the device is configured. 
}

void usb_create_set_address_packet_device(usb_setup_packet *packet,unsigned char dev_addr)
{
	packet->bmRequestType	=	usb_create_request_type_byte	(USB_TRANSFER_DIRECTION_HOST_TO_DEVICE,USB_TRANSFER_TYPE_STD,USB_TRANSFER_RECIPIENT_DEVICE);
	packet->bRequest		=	USB_TRANSFER_REQ_SET_ADDRESS;
	packet->wValue			=	dev_addr&0x7F;	//device address
	packet->wIndex			=	0;				//Zero
	packet->wLength			=	0;				//Zero

	//Set Address is used during enumeration to assign a unique address to the USB device. 
	//The address is specified in wValue and can only be a maximum of 127. 
	//This request is unique in that the device does not set its address until after the completion of the status stage. 
	//(See Control Transfers.) All other requests must complete before the status stage.
}



//Set Descriptor/Get Descriptor is used to return the specified descriptor in wValue. 
//A request for the configuration descriptor will return the device descriptor and all interface 
//and endpoint descriptors in the one request.
//
//  Endpoint Descriptors cannot be accessed directly by a GetDescriptor/SetDescriptor Request.
//  Interface Descriptors cannot be accessed directly by a GetDescriptor/SetDescriptor Request.
//  String Descriptors include a Language ID in wIndex to allow for multiple language support.

void usb_create_set_descriptor_packet_device(usb_setup_packet *packet)
{
	packet->bmRequestType	=	usb_create_request_type_byte	(USB_TRANSFER_DIRECTION_HOST_TO_DEVICE,USB_TRANSFER_TYPE_STD,USB_TRANSFER_RECIPIENT_DEVICE);
	packet->bRequest		=	USB_TRANSFER_REQ_SET_DESCRIPTOR;
	packet->wValue			=	0;				//Descriptor Type & Index
	packet->wIndex			=	0;				//Zero or Language ID
	packet->wLength			=	0;				//Descriptor Length

	//data Descriptor

}

//get standard device descriptors
void usb_create_get_descriptor_packet_device(usb_setup_packet *packet)
{
	packet->bmRequestType	=	usb_create_request_type_byte	(USB_TRANSFER_DIRECTION_DEVICE_TO_HOST,USB_TRANSFER_TYPE_STD,USB_TRANSFER_RECIPIENT_DEVICE);
	packet->bRequest		=	USB_TRANSFER_REQ_GET_DESCRIPTOR;
	
	//data : Descriptor 
	packet->wValue			=	1;	//USB_TRANSFER_DESC_TYPE_DEVICE_DESCRIPTOR
	packet->wIndex			=	0;	//Zero
	packet->wLength			=	sizeof(usb_device_desc);  
}

void usb_create_get_descriptor_packet_conf(usb_setup_packet *packet)
{
	packet->bmRequestType	=	usb_create_request_type_byte	(USB_TRANSFER_DIRECTION_DEVICE_TO_HOST,USB_TRANSFER_TYPE_STD,USB_TRANSFER_RECIPIENT_DEVICE);
	packet->bRequest		=	USB_TRANSFER_REQ_GET_DESCRIPTOR;
	packet->wValue			=	2;	//USB_TRANSFER_DESC_TYPE_CONFIG_DESCRIPTOR:
	packet->wIndex			=	0;	//Zero
	packet->wLength			=	sizeof(usb_configuration_desc);  
}

void usb_create_get_descriptor_packet_string_first(usb_setup_packet *packet)
{
	packet->bmRequestType	=	usb_create_request_type_byte	(USB_TRANSFER_DIRECTION_DEVICE_TO_HOST,USB_TRANSFER_TYPE_STD,USB_TRANSFER_RECIPIENT_DEVICE);
	packet->bRequest		=	USB_TRANSFER_REQ_GET_DESCRIPTOR;

	packet->wValue			=	3;	//USB_TRANSFER_DESC_TYPE_STRING_DESCRIPTOR
	packet->wIndex			=	0;	//Zero or Language ID
	packet->wLength			=	sizeof(usb_string_desc_zero); 
}

void usb_create_get_descriptor_packet_string(usb_setup_packet *packet,unsigned int lang_id)
{
	packet->bmRequestType	=	usb_create_request_type_byte	(USB_TRANSFER_DIRECTION_DEVICE_TO_HOST,USB_TRANSFER_TYPE_STD,USB_TRANSFER_RECIPIENT_DEVICE);
	packet->bRequest		=	USB_TRANSFER_REQ_GET_DESCRIPTOR;

	packet->wValue			=	3;			//USB_TRANSFER_DESC_TYPE_STRING_DESCRIPTOR
	packet->wIndex			=	lang_id;	//Zero or Language ID
	packet->wLength			=	sizeof(usb_string_desc); 
}


void usb_setup_control_pipe(unsigned int devi_id)
{
	

}


/*
0000 0000b	 CLEAR_FEATURE (0x01)	 Feature Selector	 Zero	 Zero	 None
0000 0000b	 SET_FEATURE (0x03)	 Feature Selector	 Zero	 Zero	 None
0000 0000b	 SET_CONFIGURATION (0x09)	 Configuration Value	 Zero	 Zero	 None
*/