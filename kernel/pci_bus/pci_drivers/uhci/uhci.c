#define DRIVER_FUNC	 C_EXPORT
#include <std_def.h>
#include <std_mem.h>
#include "kern.h"
#include "sys_pci.h"

#include "mem_base.h"
#include "lib_c.h"
#include "tree.h"

#include "sys/bitstream.h"
#include "sys/async_stream.h"
#include "sys/mem_stream.h"
#include "sys/tpo_mod.h"
#include "sys/file_system.h"
#include "sys/stream_dev.h"
#include "sys/pci_device.h"
#include "uhci.h"

#include <bus_manager/bus_drv.h>

#define MAX_TRANSFERS 64


// PCI register
#define PCI_LEGSUP 0xC0
#define PCI_LEGSUP_USBPIRQDEN	0x2000
#define PCI_UHCI_USBLEGSUP_RWC  0x8f00          /* the R/WC bits */

// Registers
#define UHCI_USBCMD				0x00 	// USB Command - word - R/W
#define UHCI_USBSTS				0x02	// USB Status - word - R/WC
#define UHCI_USBINTR			0x04	// USB Interrupt Enable - word - R/W
#define UHCI_FRNUM				0x06	// Frame number - word - R/W**
#define UHCI_FRBASEADD			0x08	// Frame List BAse Address - dword - R/W
#define UHCI_SOFMOD				0x0c	// Start of Frame Modify - byte - R/W
#define UHCI_PORTSC1			0x10	// Port 1 Status/Control - word - R/WC**
#define UHCI_PORTSC2			0x12	// Port 2 Status/Control - word - R/WC**


//PORTSC
#define UHCI_PORTSC_CURSTAT		0x0001	// Current connect status
#define UHCI_PORTSC_STATCHA		0x0002	// Current connect status change
#define UHCI_PORTSC_ENABLED		0x0004	// Port enabled/disabled
#define UHCI_PORTSC_ENABCHA		0x0008	// Change in enabled/disabled
#define UHCI_PORTSC_LINE_0		0x0010	// The status of D+
#define UHCI_PORTSC_LINE_1		0x0020	// The status of D-
#define UHCI_PORTSC_RESUME		0x0040	// Something with the suspend state ???
#define UHCI_PORTSC_LOWSPEED	0x0100	// Low speed device attached?
#define UHCI_PORTSC_RESET		0x0200	// Port is in reset
#define UHCI_PORTSC_SUSPEND		0x1000	// Set port in suspend state
#define UHCI_PORTSC_DATAMASK	0x13f5	// Mask that excludes the change bits

// USBCMD
#define UHCI_USBCMD_RS			0x01	// Run/Stop
#define UHCI_USBCMD_HCRESET		0x02 	// Host Controller Reset
#define UHCI_USBCMD_GRESET		0x04 	// Global Reset
#define UHCI_USBCMD_EGSM		0x08	// Enter Global Suspensd mode
#define UHCI_USBCMD_FGR			0x10	// Force Global resume
#define UHCI_USBCMD_SWDBG		0x20	// Software Debug
#define UHCI_USBCMD_CF			0x40	// Configure Flag
#define UHCI_USBCMD_MAXP		0x80	// Max packet

//USBSTS
#define UHCI_USBSTS_USBINT		0x01	// USB interrupt
#define UHCI_USBSTS_ERRINT		0x02	// USB error interrupt
#define UHCI_USBSTS_RESDET		0x04	// Resume Detect
#define UHCI_USBSTS_HOSTERR		0x08	// Host System Error
#define UHCI_USBSTS_HCPRERR		0x10	// Host Controller Process error
#define UHCI_USBSTS_HCHALT		0x20	// HCHalted
#define UHCI_INTERRUPT_MASK		0x3f	// Mask for all the interrupts

//USBINTR
#define UHCI_USBINTR_CRC		0x01	// Timeout/ CRC interrupt enable
#define UHCI_USBINTR_RESUME		0x02	// Resume interrupt enable
#define UHCI_USBINTR_IOC		0x04	// Interrupt on complete enable
#define UHCI_USBINTR_SHORT		0x08	// Short packet interrupt enable

// Framelist flags
#define FRAMELIST_TERMINATE    0x1
#define FRAMELIST_NEXT_IS_QH   0x2

  /* OC and OCC from Intel 430TX and later (not UHCI 1.1d spec) */
 #define   USBPORTSC_CSC         0x0002  /* Connect Status Change */
 #define   USBPORTSC_PE          0x0004  /* Port Enable */
 #define   USBPORTSC_PEC         0x0008  /* Port Enable Change */
 #define   USBPORTSC_OC          0x0400  /* Over Current condition */
 #define   USBPORTSC_OCC         0x0800  /* Over Current Change R/WC */
 #define   USBPORTSC_RES2        0x2000  /* reserved, write zeroes */
 #define   USBPORTSC_RES3        0x4000  /* reserved, write zeroes */
 #define   USBPORTSC_RES4        0x8000  /* reserved, write zeroes */

 /* must write as zeroes */
 #define WZ_BITS         (USBPORTSC_RES2 | USBPORTSC_RES3 | USBPORTSC_RES4)
 
 /* status change bits:  nonzero writes will clear */
 #define RWC_BITS        (USBPORTSC_OCC | USBPORTSC_PEC | USBPORTSC_CSC)

 #define CLR_RH_PORTSTAT(x) \
         status = pci_read_io_16(port_addr);   \
         status &= ~(RWC_BITS|WZ_BITS); \
         status &= ~(x); \
         status |= RWC_BITS & (x); \
         pci_write_io_16(port_addr,status)
 
 #define SET_RH_PORTSTAT(x) \
         status = pci_read_io_16(port_addr);   \
         status |= (x); \
         status &= ~(RWC_BITS|WZ_BITS); \
         pci_write_io_16(port_addr, status )

typedef unsigned int addr_t;

// Represents a Queue Head (QH)
typedef struct
{
	// Hardware part
	addr_t	link_phy;		// Link to the next TD/QH
	addr_t	element_phy;	// Pointer to the first element in the queue
} uhci_qh;




// Represents a Transfer Descriptor (TD)
typedef struct
{
	// Hardware part
	addr_t			link_phy;		// Link to the next TD/QH
	unsigned int	status;			// Status field
	unsigned int	token;			// Contains the packet header (where it needs to be sent)
	void			*buffer_phy;	// A pointer to the buffer with the actual packet
} uhci_td;


// Control and Status
#define TD_CONTROL_SPD				(1 << 29)
#define TD_CONTROL_3_ERRORS			(3 << 27)
#define TD_CONTROL_LOWSPEED			(1 << 26)
#define TD_CONTROL_ISOCHRONOUS		(1 << 25)
#define TD_CONTROL_IOC				(1 << 24)

#define TD_STATUS_ACTIVE			(1 << 23)
#define TD_STATUS_ERROR_STALLED		(1 << 22)
#define TD_STATUS_ERROR_BUFFER		(1 << 21)
#define TD_STATUS_ERROR_BABBLE		(1 << 20)
#define TD_STATUS_ERROR_NAK			(1 << 19)
#define TD_STATUS_ERROR_CRC			(1 << 18)
#define TD_STATUS_ERROR_TIMEOUT		(1 << 18)
#define TD_STATUS_ERROR_BITSTUFF	(1 << 17)

#define TD_STATUS_ACTLEN_MASK		0x07ff
#define TD_STATUS_ACTLEN_NULL		0x07ff

// Token
/*
#define TD_TOKEN_MAXLEN_SHIFT		21
#define TD_TOKEN_NULL_DATA			(0x07ff << TD_TOKEN_MAXLEN_SHIFT)
#define TD_TOKEN_DATA_TOGGLE_SHIFT	19
#define TD_TOKEN_DATA1				(1 << TD_TOKEN_DATA_TOGGLE_SHIFT)
*/

#define TD_TOKEN_SETUP				0x2d
#define TD_TOKEN_IN					0x69
#define TD_TOKEN_OUT				0xe1


#define TD_TOKEN_ENDPTADDR_SHIFT	15
#define TD_TOKEN_DEVADDR_SHIFT		8

#define TD_DEPTH_FIRST				0x04
#define TD_TERMINATE				0x01
#define TD_ERROR_MASK				0x440000
#define TD_ERROR_COUNT_SHIFT		27
#define TD_ERROR_COUNT_MASK			0x03
#define TD_LINK_MASK				0xfffffff0

#define QH_TERMINATE			0x01
#define QH_NEXT_IS_QH  			0x02
#define QH_LINK_MASK			0xfffffff0




#define USB_DELAY_BUS_RESET				100000
#define USB_DELAY_DEVICE_POWER_UP		300000
#define USB_DELAY_HUB_POWER_UP			200000
#define USB_DELAY_PORT_RESET			50000
#define USB_DELAY_PORT_RESET_RECOVERY	250000
#define USB_DELAY_SET_ADDRESS_RETRY		200000
#define USB_DELAY_SET_ADDRESS			10000
#define USB_DELAY_SET_CONFIGURATION		50000
#define USB_DELAY_FIRST_EXPLORE			5000000
#define USB_DELAY_HUB_EXPLORE			1000000

#define USB_DESCRIPTOR_HUB 0x29


/* request types (target & direction) for  send_request() */
/* cf USB Spec Rev 1.1, table 9-2, p 183 */
#define USB_REQTYPE_DEVICE_IN         0x80
#define USB_REQTYPE_DEVICE_OUT        0x00
#define USB_REQTYPE_INTERFACE_IN      0x81
#define USB_REQTYPE_INTERFACE_OUT     0x01
#define USB_REQTYPE_ENDPOINT_IN       0x82
#define USB_REQTYPE_ENDPOINT_OUT      0x02
#define USB_REQTYPE_OTHER_OUT         0x03
#define USB_REQTYPE_OTHER_IN          0x83

/* request types for send_request() */
/* cf USB Spec Rev 1.1, table 9-2, p 183 */
#define USB_REQTYPE_STANDARD          0x00
#define USB_REQTYPE_CLASS             0x20
#define USB_REQTYPE_VENDOR            0x40
#define USB_REQTYPE_RESERVED          0x60
#define USB_REQTYPE_MASK              0x9F

/* standard request values for send_request() */
/* cf USB Spec Rev 1.1, table 9-4, p 187 */
#define USB_REQUEST_GET_STATUS           0
#define USB_REQUEST_CLEAR_FEATURE        1
#define USB_REQUEST_SET_FEATURE          3
#define USB_REQUEST_SET_ADDRESS          5
#define USB_REQUEST_GET_DESCRIPTOR       6
#define USB_REQUEST_SET_DESCRIPTOR       7
#define USB_REQUEST_GET_CONFIGURATION    8
#define USB_REQUEST_SET_CONFIGURATION    9
#define USB_REQUEST_GET_INTERFACE       10
#define USB_REQUEST_SET_INTERFACE       11
#define USB_REQUEST_SYNCH_FRAME         12

DRIVER_FUNC unsigned int C_API_FUNC pci_get_device_cmd_flag (unsigned short vendor_id ,unsigned short device_id);
DRIVER_FUNC int			 C_API_FUNC probe_device			 (pci_device *device);
DRIVER_FUNC int			 C_API_FUNC init_driver			 ();
//DRIVER_FUNC void		 C_API_FUNC start_stream			 (unsigned int stream_idx);

typedef struct
{
	uhci_qh			*qh_start;
	uhci_td			*td_start;
}usb_transfer_chain;

DRIVER_FUNC unsigned char  class_codes[]  = {0x0C, 0x03,0x00,
											 0x00,0x00,0x00};
typedef struct
{
unsigned int			regbase;
unsigned int			num_ports;
mem_zone_ref			transfer_buffer;
mem_zone_ref			queue;
}dev_config;

dev_config	_config={0xFF};



void pci_write_io_8(unsigned short port,unsigned char value)
{
	out_8_c	(_config.regbase+port,value);
}
void pci_write_io_16(unsigned short port,unsigned short value)
{
	out_16_c	(_config.regbase+port,value);
}

void pci_write_io_32(unsigned short port,unsigned int value)
{
	out_32_c	(_config.regbase+port,value);
}


unsigned char pci_read_io_8(unsigned short port)
{
	return in_8_c	(_config.regbase+port);
}
unsigned short pci_read_io_16(unsigned short port)
{
	return in_16_c	(_config.regbase+port);
}

unsigned int pci_read_io_32(unsigned short port)
{
	unsigned int value;
	value		=	in_32_c	(_config.regbase+port);
	return value;
}


OS_API_C_FUNC(unsigned int) pci_get_device_cmd_flag (unsigned short vendor_id ,unsigned short device_id)
{
/*
	// enable pci address access
	uint16 command = PCI_command_io | PCI_command_master | PCI_command_memory;
	command |= sPCIModule->read_pci_config(fPCIInfo->bus, fPCIInfo->device,
		fPCIInfo->function, PCI_command, 2);

	sPCIModule->write_pci_config(fPCIInfo->bus, fPCIInfo->device,
		fPCIInfo->function, PCI_command, 2, command);
*/
	return PCI_PCICMD_IOS | PCI_PCICMD_MSE | PCI_PCICMD_BME;
}

OS_INT_C_FUNC(unsigned int) interupt_handler(unsigned int xx)
{
	writestr("usb interupt called \n");
	
	return mem_to_uint(PTR_NULL);
}

typedef struct {

    //struct uhci_mem_layout ml;
    uhci_td *td;
    uhci_td *td_next;
    unsigned int average;
    unsigned int td_status;
    unsigned int td_token;
    unsigned int len;
    unsigned int max_frame_size;
    unsigned int shortpkt;
    unsigned int setup_alt_next;
    unsigned int last_frame;
}uhci_std_temp ;

#if 0

void usb_control_pipe_setup(usb_transfer_chain *xfer,unsigned int max_frame_size,unsigned char low_spd)
{
    uhci_qh		*qh;
    uhci_td		*td_out;
  //struct uhci_std_temp temp;
   uhci_td		*td;
   uhci_td		*td_last;
   unsigned int td_status,td_token;
   unsigned char shortpkt,shortpkt_old;
   unsigned char last_frame;
   unsigned char setup_alt_next;
   unsigned int  temp_len,len_old;
    unsigned int x;

   /* get next DMA set */
	td						= xfer->td_start;

   /*temp.td			= NULL;
	temp.td_next		= td;
	temp.last_frame		= 0;
	temp.setup_alt_next	= xfer->flags_int.short_frames_ok;*/

	last_frame			= 0;
	setup_alt_next		= 1;//xfer->flags_int.short_frames_ok;

   //uhci_mem_layout_init(&temp.ml, xfer);

	td_status	=  htole32(UHCI_TD_ZERO_ACTLEN(UHCI_TD_SET_ERRCNT(3) |   UHCI_TD_ACTIVE));

	if (low_spd) 
	{
	   td_status |= htole32(UHCI_TD_LS);
	}
	td_token = htole32(UHCI_TD_SET_ENDPT(0) |  UHCI_TD_SET_DEVADDR(0));

	/*if (xfer->endpoint->toggle_next) {
		   // DATA1 is next 
		   temp.td_token |= htole32(UHCI_TD_SET_DT(1));
	}*/

	/* check if we should prepend a setup message */
	/*if (xfer->flags_int.control_xfr) 
	{
	*/
	td_token &= htole32(UHCI_TD_SET_DEVADDR(0x7F) |UHCI_TD_SET_ENDPT(0xF));
	td_token |= htole32(UHCI_TD_PID_SETUP | UHCI_TD_SET_DT(0));

	 //temp_len		= get_setup_descriptor();

	/*
	 temp.len		= xfer->frlengths[0];
	 temp.ml.buf_pc	= xfer->frbuffers + 0;
	 temp.shortpkt	= temp.len ? 1 : 0;
	 */

	shortpkt	= (temp_len > 0) ? 0 : 1;

	/*
	 // check for last frame 
	 if (xfer->nframes == 1) 
	 {
		   // no STATUS stage yet, SETUP is last 
		   if (xfer->flags_int.control_act) 
		   {
			   last_frame		= 1;
			   setup_alt_next	= 0;
		   }
	}
	*/
	{
		//uhci_setup_standard_chain_sub(&temp);
		td_alt_next		= NULL;
		shortpkt_old	= shortpkt;
		len_old			= temp_len;
		
		/* software is used to detect short incoming transfers */
		/*
		if ((td_token & htole32(UHCI_TD_PID)) == htole32(UHCI_TD_PID_IN)) {
			td_status |= htole32(UHCI_TD_SPD);
		} 
		else */
		{
			td_status &= ~htole32(UHCI_TD_SPD);
		}

		//temp->ml.buf_offset = 0;

		td_token &= ~htole32(UHCI_TD_SET_MAXLEN(0));
		td_token |=  htole32(UHCI_TD_SET_MAXLEN(max_frame_size));
		
		/*
		td		= temp->td;
		td_next = temp->td_next;
		*/

		while (1) 
		{
			if (temp_len == 0) 
			{
				if (shortpkt) 
				{
					break;
				}
				/* send a Zero Length Packet, ZLP, last */
 
				shortpkt   = 1;
				td_token  |= htole32(UHCI_TD_SET_MAXLEN(0));
				average    = 0;
			} 
			else 
			{
				average = max_frame_size;
				if (temp_len < max_frame_size) 
				{
					shortpkt = 1;
					td_token &= ~htole32(UHCI_TD_SET_MAXLEN(0));
					td_token |= htole32(UHCI_TD_SET_MAXLEN(temp_len));
					average	= temp_len;
				}
			}
			if (td_next == NULL) {
				panic("%s: out of UHCI transfer descriptors!", __FUNCTION__);
			}
			/* get next TD */
			td		   = td_next;
			td_next    = td->obj_next;
			temp->len -= average;
		}
        /* setup alt next pointer, if any */
        if (last_frame) {
            td_alt_next = NULL;
         } 
		else 
		{
			/* we use this field internally */
            td_alt_next = td_next;
        }

		{
			/* restore */
			shortpkt = shortpkt_old;
			temp_len = len_old;

			td_token &= ~htole32(UHCI_TD_SET_MAXLEN(0));
			td_token |= htole32(UHCI_TD_SET_MAXLEN(temp->average));
			/*
			td		= temp->td;
			td_next = temp->td_next;
			*/
			while (1) 
			{
				if (temp_len == 0) 
				{
					if (shortpkt) 
					{
						break;
					}
					/* send a Zero Length Packet, ZLP, last */

					shortpkt = 1;
					td_token |= htole32(UHCI_TD_SET_MAXLEN(0));
					average = 0;
				} 
				else 
				{
					average = temp->average;
					if (temp->len < average) 
					{
						shortpkt = 1;
						td_token &= ~htole32(UHCI_TD_SET_MAXLEN(0));
						td_token |= htole32(UHCI_TD_SET_MAXLEN(temp_len));
						average	= temp_len;
					}
				}

				if (td_next == NULL) {
						panic("%s: out of UHCI transfer descriptors!", __FUNCTION__);
				}
				/* get next TD */

				td = td_next;
				td_next = td->obj_next;

				/* fill out current TD */

				td->td_status = temp->td_status;
				td->td_token = temp->td_token;

				/* update data toggle */

				temp->td_token ^= htole32(UHCI_TD_SET_DT(1));

				if (average == 0) 
				{
					td->len = 0;
					td->td_buffer = 0;
					td->fix_pc = NULL;
				} 
				else 
				{
					/* update remaining length */
					temp_len -= average;
					td->len  = average;

					/* fill out buffer pointer and do fixup, if any */
					uhci_mem_layout_fixup(&temp->ml, td);
				}

				td->alt_next = td_alt_next;

				if ((td_next == td_alt_next) && temp->setup_alt_next) 
				{
					/* we need to receive these frames one by one ! */
						td->td_status |= htole32(UHCI_TD_IOC);
						td->td_next = htole32(UHCI_PTR_T);
				} 
				else 
				{
					if (td_next) 
					{
						/* link the current TD with the next one */
						td->td_next = td_next->td_self;
					}
				}
				usb_pc_cpu_flush(td->page_cache);
			}
			/*
			temp->td		= td;
			temp->td_next	= td_next;
			*/
		}

		//uhci_setup_standard_chain_sub(&temp);
		x = 1;
	}

	while (x != xfer->nframes) 
	{
		   /* DATA0 / DATA1 message */

		   temp.len			= xfer->frlengths[x];
		   temp.ml.buf_pc	= xfer->frbuffers + x;

		   x++;

		   if (x == xfer->nframes) 
		   {
			   if (xfer->flags_int.control_xfr) 
			   {
				   /* no STATUS stage yet, DATA is last */
				   if (xfer->flags_int.control_act) 
				   {
						   temp.last_frame = 1;
						   temp.setup_alt_next = 0;
				   }
				} 
			    else 
			    {
				   temp.last_frame = 1;
				   temp.setup_alt_next = 0;
				}
		   }
		   /*
			* Keep previous data toggle,
			* device address and endpoint number:
			*/

		   td_token &= htole32(UHCI_TD_SET_DEVADDR(0x7F) |	UHCI_TD_SET_ENDPT(0xF) | UHCI_TD_SET_DT(1));

		   if (temp.len == 0) {

				   /* make sure that we send an USB packet */

				   temp.shortpkt = 0;

		   } else {

				   /* regular data transfer */

				   temp.shortpkt = (xfer->flags.force_short_xfer) ? 0 : 1;
		   }

		   /* set endpoint direction */

		   td_token |=	UHCI_TD_PID_OUT;//(UE_GET_DIR(xfer->endpointno) == UE_DIR_IN) ?htole32(UHCI_TD_PID_IN) :htole32(UHCI_TD_PID_OUT);
		   uhci_setup_standard_chain_sub	(&temp);
	}

	/* check if we should append a status stage */

	if (xfer->flags_int.control_xfr && !xfer->flags_int.control_act) 
	{
		   /*
			* send a DATA1 message and reverse the current endpoint
			* direction
			*/

		   td_token &= htole32(UHCI_TD_SET_DEVADDR(0x7F) |UHCI_TD_SET_ENDPT(0xF) |UHCI_TD_SET_DT(1));
		   td_token |=  (UE_GET_DIR(xfer->endpointno) == UE_DIR_OUT) ?  htole32(UHCI_TD_PID_IN | UHCI_TD_SET_DT(1)) :  htole32(UHCI_TD_PID_OUT | UHCI_TD_SET_DT(1));

		   temp.len			   = 0;
		   temp.ml.buf_pc	   = NULL;
		   temp.shortpkt	   = 0;
		   temp.last_frame	   = 1;
		   temp.setup_alt_next = 0;

		   uhci_setup_standard_chain_sub(&temp);
	}
	
	td		   = xfer->td_start;
	/* Ensure that last TD is terminating: */
	td->td_next = htole32(UHCI_PTR_T);

	/* set interrupt bit */
	td->td_status |= htole32(UHCI_TD_IOC);

	usb_pc_cpu_flush(td->page_cache);

	/* must have at least one frame! */
	xfer->td_transfer_last = td;

	td_out	= (xfer->td_start);
    /* setup QH */
    qh		= queue_ptr;
 
    /*
     * NOTE: some devices choke on bandwidth- reclamation for control
     * transfers
     */
   if (xfer->xroot->udev->flags.self_suspended == 0) {
           if (xfer->xroot->udev->speed == USB_SPEED_LOW) {
                   UHCI_APPEND_QH(qh, sc->sc_ls_ctl_p_last);
           } else {
                   UHCI_APPEND_QH(qh, sc->sc_fs_ctl_p_last);
           }
   } else {
           usb_pc_cpu_flush(qh->page_cache);
   }
   /* put transfer on interrupt queue */
   uhci_transfer_intr_enqueue(xfer);
}

static void uhci_setup_standard_chain_sub(struct uhci_std_temp *temp)
{
        uhci_td_t *td;
        uhci_td_t *td_next;
        uhci_td_t *td_alt_next;
        uint32_t average;
        uint32_t len_old;
        uint8_t shortpkt_old;
        uint8_t precompute;

        td_alt_next = NULL;
        shortpkt_old = temp->shortpkt;
        len_old = temp->len;
        precompute = 1;

        /* software is used to detect short incoming transfers */

        if ((temp->td_token & htole32(UHCI_TD_PID)) == htole32(UHCI_TD_PID_IN)) {
                temp->td_status |= htole32(UHCI_TD_SPD);
        } else {
                temp->td_status &= ~htole32(UHCI_TD_SPD);
        }

        //temp->ml.buf_offset = 0;

restart:

        temp->td_token &= ~htole32(UHCI_TD_SET_MAXLEN(0));
        temp->td_token |= htole32(UHCI_TD_SET_MAXLEN(temp->average));

        td		= temp->td;
        td_next = temp->td_next;

        while (1) {

                if (temp->len == 0) {

                        if (temp->shortpkt) {
                                break;
                        }
                        /* send a Zero Length Packet, ZLP, last */

                        temp->shortpkt = 1;
                        temp->td_token |= htole32(UHCI_TD_SET_MAXLEN(0));
                        average = 0;

                } else {

                        average = temp->average;

                        if (temp->len < average) {
                                temp->shortpkt = 1;
                                temp->td_token &= ~htole32(UHCI_TD_SET_MAXLEN(0));
                                temp->td_token |= htole32(UHCI_TD_SET_MAXLEN(temp->len));
                                average			= temp->len;
                        }
                }

                if (td_next == NULL) {
                        panic("%s: out of UHCI transfer descriptors!", __FUNCTION__);
                }
                /* get next TD */

                td = td_next;
                td_next = td->obj_next;

                /* check if we are pre-computing */

                if (precompute) {

                        /* update remaining length */

                        temp->len -= average;

                        continue;
                }

                /* fill out current TD */

                td->td_status = temp->td_status;
                td->td_token = temp->td_token;

                /* update data toggle */

                temp->td_token ^= htole32(UHCI_TD_SET_DT(1));

                if (average == 0) {

                        td->len = 0;
                        td->td_buffer = 0;
                        td->fix_pc = NULL;

                } else {

                        /* update remaining length */

                        temp->len -= average;

                        td->len = average;

                        /* fill out buffer pointer and do fixup, if any */

                        uhci_mem_layout_fixup(&temp->ml, td);
                }

                td->alt_next = td_alt_next;

                if ((td_next == td_alt_next) && temp->setup_alt_next) {
                        /* we need to receive these frames one by one ! */
                        td->td_status |= htole32(UHCI_TD_IOC);
                        td->td_next = htole32(UHCI_PTR_T);
                } else {
                        if (td_next) {
                                /* link the current TD with the next one */
                                td->td_next = td_next->td_self;
                        }
                }

                usb_pc_cpu_flush(td->page_cache);
        }

        if (precompute) {
                precompute = 0;

                /* setup alt next pointer, if any */
                if (temp->last_frame) {
                        td_alt_next = NULL;
                } else {
                        /* we use this field internally */
                        td_alt_next = td_next;
                }

                /* restore */
                temp->shortpkt = shortpkt_old;
                temp->len = len_old;
                goto restart;
        }
        temp->td = td;
        temp->td_next = td_next;
}

uhci_td_t *uhci_setup_standard_chain(usb_transfer_chain *xfer)
{
   //struct uhci_std_temp temp;
   uhci_td_t		*td;
   uint32_t			x;

   //temp.average			= xfer->max_frame_size;
   //temp.max_frame_size	= xfer->max_frame_size;

   /* toggle the DMA set we are using */
   xfer->flags_int.curr_dma_set ^= 1;

   /* get next DMA set */
   td						= xfer->td_start;

   temp.td					= NULL;
   temp.td_next				= td;
   temp.last_frame			= 0;
   temp.setup_alt_next		= xfer->flags_int.short_frames_ok;

	//uhci_mem_layout_init(&temp.ml, xfer);

   temp.td_status	=  htole32(UHCI_TD_ZERO_ACTLEN(UHCI_TD_SET_ERRCNT(3) |   UHCI_TD_ACTIVE));

   if (xfer->xroot->udev->speed == USB_SPEED_LOW) 
   {
	   temp.td_status |= htole32(UHCI_TD_LS);
   }
   temp.td_token = htole32(UHCI_TD_SET_ENDPT(xfer->endpointno) |  UHCI_TD_SET_DEVADDR(xfer->address));

   if (xfer->endpoint->toggle_next) {
           /* DATA1 is next */
           temp.td_token |= htole32(UHCI_TD_SET_DT(1));
   }
   /* check if we should prepend a setup message */

   if (xfer->flags_int.control_xfr) 
   {
           if (xfer->flags_int.control_hdr) 
		   {
                   temp.td_token &= htole32(UHCI_TD_SET_DEVADDR(0x7F) |UHCI_TD_SET_ENDPT(0xF));
                   temp.td_token |= htole32(UHCI_TD_PID_SETUP | UHCI_TD_SET_DT(0));

                   temp.len			= xfer->frlengths[0];
                   temp.ml.buf_pc	= xfer->frbuffers + 0;
                   temp.shortpkt	= temp.len ? 1 : 0;
                   /* check for last frame */
                   if (xfer->nframes == 1) {
                           /* no STATUS stage yet, SETUP is last */
                           if (xfer->flags_int.control_act) {
                                   temp.last_frame = 1;
                                   temp.setup_alt_next = 0;
                           }
                   }
                   uhci_setup_standard_chain_sub(&temp);
           }
           x = 1;
   } else {
           x = 0;
   }

   while (x != xfer->nframes) 
   {
           /* DATA0 / DATA1 message */

           temp.len			= xfer->frlengths[x];
           temp.ml.buf_pc	= xfer->frbuffers + x;

           x++;

           if (x == xfer->nframes) {
                   if (xfer->flags_int.control_xfr) {
                           /* no STATUS stage yet, DATA is last */
                           if (xfer->flags_int.control_act) {
                                   temp.last_frame = 1;
                                   temp.setup_alt_next = 0;
                           }
                   } else {
                           temp.last_frame = 1;
                           temp.setup_alt_next = 0;
                   }
           }
           /*
            * Keep previous data toggle,
            * device address and endpoint number:
            */

           temp.td_token &= htole32(UHCI_TD_SET_DEVADDR(0x7F) |	UHCI_TD_SET_ENDPT(0xF) | UHCI_TD_SET_DT(1));

           if (temp.len == 0) {

                   /* make sure that we send an USB packet */

                   temp.shortpkt = 0;

           } else {

                   /* regular data transfer */

                   temp.shortpkt = (xfer->flags.force_short_xfer) ? 0 : 1;
           }

           /* set endpoint direction */

           temp.td_token |=	(UE_GET_DIR(xfer->endpointno) == UE_DIR_IN) ?htole32(UHCI_TD_PID_IN) :htole32(UHCI_TD_PID_OUT);

           uhci_setup_standard_chain_sub	(&temp);
   }

   /* check if we should append a status stage */

   if (xfer->flags_int.control_xfr &&
       !xfer->flags_int.control_act) {

           /*
            * send a DATA1 message and reverse the current endpoint
            * direction
            */

           temp.td_token &= htole32(UHCI_TD_SET_DEVADDR(0x7F) |
               UHCI_TD_SET_ENDPT(0xF) |
               UHCI_TD_SET_DT(1));
           temp.td_token |=
               (UE_GET_DIR(xfer->endpointno) == UE_DIR_OUT) ?
               htole32(UHCI_TD_PID_IN | UHCI_TD_SET_DT(1)) :
               htole32(UHCI_TD_PID_OUT | UHCI_TD_SET_DT(1));

           temp.len = 0;
           temp.ml.buf_pc = NULL;
           temp.shortpkt = 0;
           temp.last_frame = 1;
           temp.setup_alt_next = 0;

           uhci_setup_standard_chain_sub(&temp);
   }
   td = temp.td;

   /* Ensure that last TD is terminating: */
   td->td_next = htole32(UHCI_PTR_T);

   /* set interrupt bit */

   td->td_status |= htole32(UHCI_TD_IOC);

   usb_pc_cpu_flush(td->page_cache);

   /* must have at least one frame! */

   xfer->td_transfer_last = td;

#ifdef USB_DEBUG
        if (uhcidebug > 8) {
                DPRINTF("nexttog=%d; data before transfer:\n",
                    xfer->endpoint->toggle_next);
                uhci_dump_tds(xfer->td_transfer_first);
        }
#endif
        return (xfer->td_transfer_first);
}



/*------------------------------------------------------------------------*
 * uhci bulk support
*------------------------------------------------------------------------*/
static void uhci_device_bulk_open(struct usb_xfer *xfer)
 {
         return;
 }
 
static void uhci_device_bulk_close(struct usb_xfer *xfer)
{
  uhci_device_done(xfer, USB_ERR_CANCELLED);
}
 
 static void
 uhci_device_bulk_enter(struct usb_xfer *xfer)
 {
         return;
 }
 
 static void uhci_device_bulk_start(struct usb_xfer *xfer)
 {
    uhci_softc_t *sc = UHCI_BUS2SC(xfer->xroot->bus);
    uhci_td_t *td;
    uhci_qh_t *qh;
 
    /* setup TD's */
    td = uhci_setup_standard_chain(xfer);
 
    /* setup QH */
    qh = xfer->qh_start[xfer->flags_int.curr_dma_set];
 
	/*
    if (xfer->xroot->udev->flags.self_suspended == 0) 
	{
            UHCI_APPEND_QH	(qh, sc->sc_bulk_p_last);
            uhci_add_loop	(sc);
            xfer->flags_int.bandwidth_reclaimed = 1;
    } 
	else 
	{
           usb_pc_cpu_flush(qh->page_cache);
    }
    // put transfer on interrupt queue 
    uhci_transfer_intr_enqueue(xfer);
	*/
 }
 
 /*------------------------------------------------------------------------*
  * uhci control support
  *------------------------------------------------------------------------*/
 static void uhci_device_ctrl_open(struct usb_xfer *xfer)
 {
         return;
 }
 
 static void uhci_device_ctrl_close(struct usb_xfer *xfer)
 {
       uhci_device_done(xfer, USB_ERR_CANCELLED);
 }
 
 static void uhci_device_ctrl_enter(struct usb_xfer *xfer)
 {
         return;
 }
 
 static void uhci_device_ctrl_start(struct usb_xfer *xfer)
 {
    uhci_softc_t *sc = UHCI_BUS2SC(xfer->xroot->bus);
    uhci_qh_t *qh;
    uhci_td_t *td;
 
    /* setup TD's */
    td = uhci_setup_standard_chain(xfer);
 
    /* setup QH */
    qh = xfer->qh_start[xfer->flags_int.curr_dma_set];
 
    qh->e_next = td;
    qh->qh_e_next = td->td_self;
 
    /*
     * NOTE: some devices choke on bandwidth- reclamation for control
     * transfers
     */
   if (xfer->xroot->udev->flags.self_suspended == 0) {
           if (xfer->xroot->udev->speed == USB_SPEED_LOW) {
                   UHCI_APPEND_QH(qh, sc->sc_ls_ctl_p_last);
           } else {
                   UHCI_APPEND_QH(qh, sc->sc_fs_ctl_p_last);
           }
   } else {
           usb_pc_cpu_flush(qh->page_cache);
   }
   /* put transfer on interrupt queue */
   uhci_transfer_intr_enqueue(xfer);
}

unsigned int	get_setup_token(unsigned char dev_id, unsigned char endp)
{
	unsigned int ret;

	ret				=	TD_TOKEN_SETUP|((dev_id&0x7F)<<8)|((endp&0xf) << 15);
	ret				=	htole32 ;
}


void usb_control_create_setup_token_packet(uhci_td	*td,unsigned char dev_id)
{
	unsigned int	setup_packet;
	bitstream		packet_str;


	td->status			=0;
	td->token			=TD_TOKEN_SETUP;
	//td->link_phy		=next;
	td->


	bitstream_open		(&packet_str,td->buffer_phy ,8);
	bitstream_le_write	(&packet_str,TD_TOKEN_SETUP ,8);
	bitstream_le_write	(&packet_str,dev_id			,7);//dev id	(0 -> setup)
	bitstream_le_write	(&packet_str,0				,4);//endp		(0 -> control)
	bitstream_le_write	(&packet_str,0				,5);//crc
	bitstream_le_flush	(&packet_str);
	
	
}
#endif

unsigned char	transfer_desc_array_buffer[4096];
unsigned int	num_transfers;
mem_ptr			last_transfer_ptr;


mem_ptr get_new_qh			()
{
	mem_ptr 		*qh;

	qh					 =	last_transfer_ptr;

	memset_c(qh,0,sizeof(uhci_qh));
	last_transfer_ptr	=	mem_add(last_transfer_ptr,sizeof(uhci_qh));
	
	if(mem_to_uint(last_transfer_ptr)&0xF)
		last_transfer_ptr = uint_to_mem((mem_to_uint(last_transfer_ptr)&0xFFFFFFF0)+16);

	return qh;
}

mem_ptr get_new_td			()
{
	mem_ptr 		*td;

	td					 =	last_transfer_ptr;
	memset_c(td,0,sizeof(uhci_td));
	last_transfer_ptr	=	mem_add(last_transfer_ptr,sizeof(uhci_td));
	
	if(mem_to_uint(last_transfer_ptr)&0xF)
		last_transfer_ptr = uint_to_mem((mem_to_uint(last_transfer_ptr)&0xFFFFFFF0)+16);

	return td;
}


unsigned int	create_qh_link(uhci_qh *qh)
{
	unsigned int	qh_link;
	unsigned int	qh_ptr;

	if(qh==PTR_NULL)
	{

		qh_link			=		0;
		qh_link			=		set_bit		(qh_link,1,0); // terminate
	}
	else
	{
		qh_ptr			=	mem_to_uint(qh);
		qh_link			=	qh_ptr & 0xFFFFFFF0;

		qh_link			=	set_bit		(qh_link,0,0); // terminate
		qh_link			=	set_bit		(qh_link,1,1);	// QH = 1 , TD = 0
		qh_link			=	set_bit		(qh_link,1,2); // Depth = 1 , Queue = 0
		qh_link			=	set_bit		(qh_link,0,3); // reserved
	}

	return qh_link;
}

unsigned int	create_td_link(uhci_td *td)
{
	unsigned int	td_link;
	unsigned int	td_ptr;

	if(td==PTR_NULL)
	{

		td_link			=		0;
		td_link			=		set_bit		(td_link,1,0); // terminate
	}
	else
	{
		td_ptr			=	mem_to_uint(td);
		td_link			=	td_ptr & 0xFFFFFFF0;

		td_link			=	set_bit		(td_link,0,0); // terminate
		td_link			=	set_bit		(td_link,0,1);	// QH = 1 , TD = 0
		td_link			=	set_bit		(td_link,1,2); // Depth = 1 , Queue = 0
		td_link			=	set_bit		(td_link,0,3); // reserved
	}

	return td_link;



}



unsigned char create_status_byte()
{
	unsigned char	status;
	status				=	0;
	status				=	set_bit(status,1		 ,7);								//active
	status				=	set_bit(status,0		 ,6);								//stalled
	status				=	set_bit(status,0		 ,5);								//data buff error
	status				=	set_bit(status,0		 ,4);								//babble
	status				=	set_bit(status,0		 ,3);								//NAK
	status				=	set_bit(status,0		 ,2);								//CRC/timeout
	status				=	set_bit(status,0		 ,1);								//bitstuff error
	status				=	set_bit(status,0		 ,0);								//reserved

	return status;
}
uhci_td	 *create_setup_packet	(unsigned int dev_addr,usb_setup_packet *setup )
{
	uhci_td			*td;
	unsigned char	status;

	td				=	get_new_td();

	td->token		=	0;
	td->token		=	write_bits(td->token,TD_TOKEN_SETUP			  ,0  ,8);	//PID
	td->token		=	write_bits(td->token,dev_addr				  ,8  ,7);	//device addr
	td->token		=	write_bits(td->token,0						  ,15 ,4);	//end point (0=control)
	td->token		=	write_bits(td->token,0						  ,19 ,1);	//data toggle
	td->token		=	write_bits(td->token,0						  ,20 ,1);	//reserved
	td->token		=	write_bits(td->token,sizeof(usb_setup_packet) ,21 ,10);	//max frame size
	td->buffer_phy	=	setup;

	
	status			=	create_status_byte();

	td->status		=	write_bits(td->status,0		 ,0 ,11);					//actual len
	td->status		=	write_bits(td->status,0		 ,11,5);					//reserved
	td->status		=	write_bits(td->status,status  ,16,8);					//status byte
	td->status		=	write_bits(td->status,1		 ,24,1);					//Interupt on complete
	td->status		=	write_bits(td->status,0		 ,25,1);					//1=isochronous
	td->status		=	write_bits(td->status,0		 ,26,1);					//1=lowspeed
	td->status		=	write_bits(td->status,3		 ,27,2);					//n errors
	td->status		=	write_bits(td->status,1		 ,29,1);					//short packet
	td->status		=	write_bits(td->status,0		 ,30,2);					//reserved

	td->link_phy	=	create_td_link(PTR_NULL);
	return td;
}

uhci_td	 *create_control_todevice_status	(unsigned int dev_addr)
{
	uhci_td			*td;
	unsigned char	status;

	td				=	get_new_td();

	td->token		=	0;
	td->token		=	write_bits(td->token,TD_TOKEN_OUT			 ,0  ,8);	//PID
	td->token		=	write_bits(td->token,dev_addr				 ,8  ,7);	//device addr
	td->token		=	write_bits(td->token,0						 ,15 ,4);	//end point (0=control)
	td->token		=	write_bits(td->token,1						 ,19 ,1);	//data toggle
	td->token		=	write_bits(td->token,0						 ,20 ,1);	//reserved
	td->token		=	write_bits(td->token,0						 ,21 ,10);	//max frame size
	td->buffer_phy	=	PTR_NULL;

	
	status			=	create_status_byte();

	td->status		=	write_bits(td->status,0		 ,0 ,11);					//actual len
	td->status		=	write_bits(td->status,0		 ,11,5);					//reserved
	td->status		=	write_bits(td->status,status ,16,8);					//status byte
	td->status		=	write_bits(td->status,1		 ,24,1);					//Interupt on complete
	td->status		=	write_bits(td->status,0		 ,25,1);					//1=isochronous
	td->status		=	write_bits(td->status,0		 ,26,1);					//1=lowspeed
	td->status		=	write_bits(td->status,3		 ,27,2);					//n errors
	td->status		=	write_bits(td->status,1		 ,29,1);					//short packet
	td->status		=	write_bits(td->status,0		 ,30,2);					//reserved


	td->link_phy	=	create_td_link(PTR_NULL);
	return td;
}

uhci_td	 *create_control_tohost_transfer	(unsigned int dev_addr,mem_ptr data_dest,mem_size size_data,unsigned int max_pack_size )
{
	uhci_td			*td;
	uhci_td			*first_td;
	uhci_td			*last_td;
	unsigned char	*data_ptr;
	unsigned char	status;
	size_t			transfered_bytes;
	unsigned int	data_tog;

	transfered_bytes	=	0;
	data_ptr			=	data_dest;
	last_td				=	PTR_NULL;
	first_td			=	PTR_NULL;
	data_tog			=	0;

	while(transfered_bytes<size_data)
	{
		size_t	len_packet_data;

		len_packet_data	=	size_data-transfered_bytes;
		
		if(len_packet_data>max_pack_size)
			len_packet_data=max_pack_size;

		td				=	get_new_td();

		if(first_td==PTR_NULL)
			first_td=td;

		if(last_td!=PTR_NULL)
			last_td->link_phy	=	create_td_link(td);

		td->token		=	0;
		td->token		=	write_bits(td->token,TD_TOKEN_IN			  ,0  ,8);	//PID
		td->token		=	write_bits(td->token,dev_addr				  ,8  ,7);	//device addr
		td->token		=	write_bits(td->token,0						  ,15 ,4);	//end point (0=control)
		td->token		=	write_bits(td->token,data_tog				  ,19 ,1);	//data toggle
		td->token		=	write_bits(td->token,0						  ,20 ,1);	//reserved
		td->token		=	write_bits(td->token,len_packet_data		  ,21 ,10);	//max frame size
		td->buffer_phy	=	&data_ptr[transfered_bytes];
		
		status			=	create_status_byte();

		td->status		=	write_bits(td->status,0		  ,0,11);					//actual len
		td->status		=	write_bits(td->status,0		  ,11,5);					//reserved
		td->status		=	write_bits(td->status,status  ,16,8);					//status byte
		td->status		=	write_bits(td->status,1		  ,24,1);					//Interupt on complete
		td->status		=	write_bits(td->status,0		  ,25,1);					//1=isochronous
		td->status		=	write_bits(td->status,0		  ,26,1);					//1=lowspeed
		td->status		=	write_bits(td->status,3		  ,27,2);					//n errors
		td->status		=	write_bits(td->status,0		  ,29,1);					//short packet
		td->status		=	write_bits(td->status,0		  ,30,2);					//reserved

		data_tog		=	data_tog^1;
		last_td			=	td;
		transfered_bytes+=  len_packet_data;
	}

	last_td->link_phy	=	create_td_link(PTR_NULL);
		
	return first_td;	
}

uhci_td	 *get_last_td		(uhci_td	 *td)
{
	uhci_td	 *td_cur;

	td_cur	=	td;
	while((td_cur->link_phy&0x01)==0)
	{
		td_cur = uint_to_mem(td_cur->link_phy & 0xFFFFFFF0);
	}

	return td_cur;

}

uhci_qh *create_setup_queue		(unsigned int dev_addr,unsigned int max_frame_size,usb_setup_packet *setup )
{
	uhci_qh 		 *qh;
	uhci_td			 *td_setup;
	uhci_td			 *td_data;
	uhci_td			 *td_data_last;
	uhci_td			 *td_status;
	unsigned char	 data_buff[64];
	
	writestr("1 \n");
	qh						=	get_new_qh								();
	td_setup				=	create_setup_packet						(0,setup);
	td_data					=	create_control_tohost_transfer			(0,data_buff,64,8);
	td_status				=	create_control_todevice_status			(0);
	writestr("2 \n");


	qh->element_phy			=	create_td_link(td_setup);
	qh->link_phy			=	create_td_link(td_setup);
	td_data_last			=	get_last_td(td_data);

	writestr("3 \n");
	td_setup->link_phy		=	create_td_link(td_data);
	td_data_last->link_phy	=	create_td_link(td_status);
	td_status->link_phy		=	create_td_link(PTR_NULL);

	return qh;
}

int ResetPort(unsigned char index)
{
	unsigned int	port;
	unsigned short	status;
	int				i;
	if (index > 1)
		return -1;

	writestr("usb_uhci: reset port ");
	writeint(index,16);
	writestr("\n");


	port	 = UHCI_PORTSC1 + index * 2;
	status	 = pci_read_io_16(port);

	writestr("usb_uhci: port status : ");
	writeint(status,16);
	writestr("\n");


	status  &= UHCI_PORTSC_DATAMASK;
	pci_write_io_16(port, status | UHCI_PORTSC_RESET);
	snooze(250000);

	status = pci_read_io_16(port);
	status &= UHCI_PORTSC_DATAMASK;
	pci_write_io_16(port, status & ~UHCI_PORTSC_RESET);
	snooze(1000);

	status = pci_read_io_16(port);
	status &= UHCI_PORTSC_DATAMASK;
	pci_write_io_16(port, status | UHCI_PORTSC_ENABLED);
	snooze(50000);

	for (i = 10; i > 0; i--) {
		// try to enable the port

		status = pci_read_io_16(port);

		if ((status & UHCI_PORTSC_CURSTAT) == 0) {
			// no device connected. since we waited long enough we can assume
			// that the port was reset and no device is connected.
			break;
		}

		if (status & (UHCI_PORTSC_STATCHA | UHCI_PORTSC_ENABCHA)) {
			// port enabled changed or connection status were set.
			// acknowledge either / both and wait again.
			status &= UHCI_PORTSC_DATAMASK;
			pci_write_io_16(port, status | UHCI_PORTSC_STATCHA | UHCI_PORTSC_ENABCHA);
			continue;
		}

		if (status & UHCI_PORTSC_ENABLED) {
			// the port is enabled
			break;
		}
	}

	if (status & UHCI_PORTSC_ENABLED) 
	{
		writestr("usb_uhci: port was reset: ");
		writeint(pci_read_io_16(port),16);
		writestr("\n");
		// the port is enabled
		return 1;
	}
	else
	{
		writestr("usb_uhci: port was not reset: ");
		writeint(pci_read_io_16(port),16);
		writestr("\n");
		return 0;
	}
	//fPortResetChange |= (1 << index);
	
}

int start()
{
	int i;
	int running ;
	unsigned int usb_cmd,usb_sts;


	// Start the host controller, then start the Busmanager
	//write_str("usb_uhci: usbcmd reg 0x%04x, usbsts reg 0x%04x\n",ReadReg16(UHCI_USBCMD), ReadReg16(UHCI_USBSTS)));

	// Set the run bit in the command register
	usb_cmd		=	pci_read_io_16(UHCI_USBCMD);
	usb_sts		=	pci_read_io_16(UHCI_USBSTS);

	writestr("usb_uhci: starting UHCI BusManager ");
	writeint(usb_cmd,16);
	writestr(" ");
	writeint(usb_sts,16);
	writestr("\n");

	
	
    pci_write_io_16	(UHCI_USBCMD , UHCI_USBCMD_RS | UHCI_USBCMD_CF| UHCI_USBCMD_MAXP );

	usb_cmd		=	pci_read_io_16(UHCI_USBCMD);
	usb_sts		=	pci_read_io_16(UHCI_USBSTS);

	writestr("usb_uhci: starting UHCI BusManager ");
	writeint(usb_cmd,16);
	writestr(" ");
	writeint(usb_sts,16);
	writestr("\n");


	running = 0;
	for (i = 0; i < 10; i++) {
		unsigned short status = pci_read_io_16(UHCI_USBSTS);
		writestr("usb_uhci: current loop ");
		writeint(i,10);
		writestr(" status ");
		writeint(status,16);
		writestr("\n");
		if (status & UHCI_USBSTS_HCHALT)
			snooze(10000);
		else {
			running = 1;
			break;
		}
	}

	if (!running) {
		writestr("usb_uhci: controller won't start running\n");
		return 0;
	}
	

	// Set the max packet size for bandwidth reclamation to 64 bytes
	

	return 1;
}

#if 0
356 /*
357  * Make sure the controller is completely inactive, unable to
358  * generate interrupts or do DMA.
359  */
360 void uhci_reset_hc(struct pci_dev *pdev, unsigned long base)
361 {
362         /* Turn off PIRQ enable and SMI enable.  (This also turns off the
363          * BIOS's USB Legacy Support.)  Turn off all the R/WC bits too.
364          */
365         pci_write_config_word(pdev, UHCI_USBLEGSUP, UHCI_USBLEGSUP_RWC);
366 
367         /* Reset the HC - this will force us to get a
368          * new notification of any already connected
369          * ports due to the virtual disconnect that it
370          * implies.
371          */
372         outw(UHCI_USBCMD_HCRESET, base + UHCI_USBCMD);
373         mb();
374         udelay(5);
375         if (inw(base + UHCI_USBCMD) & UHCI_USBCMD_HCRESET)
376                 dev_warn(&pdev->dev, "HCRESET not completed yet!\n");
377 
378         /* Just to be safe, disable interrupt requests and
379          * make sure the controller is stopped.
380          */
381         outw(0, base + UHCI_USBINTR);
382         outw(0, base + UHCI_USBCMD);
383 }
384 EXPORT_SYMBOL_GPL(uhci_reset_hc);
385 
386 /*
387  * Initialize a controller that was newly discovered or has just been
388  * resumed.  In either case we can't be sure of its previous state.
389  *
390  * Returns: 1 if the controller was reset, 0 otherwise.
391  */
392 int uhci_check_and_reset_hc(struct pci_dev *pdev, unsigned long base)
393 {
394         u16 legsup;
395         unsigned int cmd, intr;
396 
397         /*
398          * When restarting a suspended controller, we expect all the
399          * settings to be the same as we left them:
400          *
401          *      PIRQ and SMI disabled, no R/W bits set in USBLEGSUP;
402          *      Controller is stopped and configured with EGSM set;
403          *      No interrupts enabled except possibly Resume Detect.
404          *
405          * If any of these conditions are violated we do a complete reset.
406          */
407         pci_read_config_word(pdev, UHCI_USBLEGSUP, &legsup);
408         if (legsup & ~(UHCI_USBLEGSUP_RO | UHCI_USBLEGSUP_RWC)) {
409                 dev_dbg(&pdev->dev, "%s: legsup = 0x%04x\n",
410                                 __func__, legsup);
411                 goto reset_needed;
412         }
413 
414         cmd = inw(base + UHCI_USBCMD);
415         if ((cmd & UHCI_USBCMD_RUN) || !(cmd & UHCI_USBCMD_CONFIGURE) ||
416                         !(cmd & UHCI_USBCMD_EGSM)) {
417                 dev_dbg(&pdev->dev, "%s: cmd = 0x%04x\n",
418                                 __func__, cmd);
419                 goto reset_needed;
420         }
421 
422         intr = inw(base + UHCI_USBINTR);
423         if (intr & (~UHCI_USBINTR_RESUME)) {
424                 dev_dbg(&pdev->dev, "%s: intr = 0x%04x\n",
425                                 __func__, intr);
426                 goto reset_needed;
427         }
428         return 0;
429 
430 reset_needed:
431         dev_dbg(&pdev->dev, "Performing full reset\n");
432         uhci_reset_hc(pdev, base);
433         return 1;
434 }
#endif

OS_API_C_FUNC(int) probe_device	(pci_device *pci_dev_ptr)
{
	unsigned int	fRegisterBase;
	unsigned int	base_addr_reg;
	unsigned short	pci_code;
	unsigned int	pci_bus;
	unsigned int	pci_dev;
	unsigned int	pci_fn;
	unsigned int	port;
	unsigned short	usb_cmd,usb_sts;
	int				tries;
	mem_ptr			c_ptr,c_ptr_2;

	//return 0;

	c_ptr			=	 mem_add(&pci_dev_ptr->config,0x20);
	c_ptr_2			=	 &pci_dev_ptr->config.base_addr_regs[0x10];

	base_addr_reg    = *((unsigned int *)(c_ptr_2));
	
	if(base_addr_reg  == 0)
		base_addr_reg  = *((unsigned int *)(&pci_dev_ptr->config.base_addr_regs[0x00]));

	fRegisterBase   =	base_addr_reg&0xFFFF;
	
	writestr("\nusb_uhci: iospace offset: ");
	writeint(fRegisterBase,16);
	writestr("\n");

	if (fRegisterBase == 0)return 0;

	_config.regbase		=	fRegisterBase&PCI_address_io_mask;

	tree_mamanger_get_node_word	(&pci_dev_ptr->pci_node,0,&pci_code);
	pci_bus			=	(pci_code>>8)	& 0xFF;
	pci_dev			=	(pci_code>>3)	& 0x1F;
	pci_fn			=	pci_code		& 0x07;

	// make sure we gain control of the UHCI controller instead of the BIOS
	//sPCIModule->write_pci_config(fPCIInfo->bus, fPCIInfo->device,fPCIInfo->function, PCI_LEGSUP, 2, PCI_LEGSUP_USBPIRQDEN);

	write_pci_word_c		(pci_bus,pci_dev,pci_fn,PCI_LEGSUP,PCI_UHCI_USBLEGSUP_RWC);
	

	pci_write_io_16(UHCI_USBINTR,0);

	// do a global and host reset
	pci_write_io_16(UHCI_USBCMD, UHCI_USBCMD_GRESET);
	snooze(1000000);

	pci_write_io_8(UHCI_SOFMOD, 64);
		
	pci_write_io_16(UHCI_USBCMD, UHCI_USBCMD_HCRESET);
	snooze(1000000);
	
	tries = 5;
	while (pci_read_io_16(UHCI_USBCMD) & UHCI_USBCMD_HCRESET) 
	{
		snooze(10000);
		if (tries-- < 0)
		{
			writestr("usb_uhci: reset failed (5 tries) \n");
			return 0;
		}
	}

	// disable interrupts
	
	//reset command
	pci_write_io_16 (UHCI_USBCMD,0);
	write_pci_word_c(pci_bus,pci_dev,pci_fn,PCI_LEGSUP,PCI_LEGSUP_USBPIRQDEN);
	
	pci_write_io_8	(UHCI_SOFMOD, 64);
	
	pci_write_io_16	(UHCI_USBINTR, UHCI_USBINTR_CRC | UHCI_USBINTR_RESUME| UHCI_USBINTR_IOC | UHCI_USBINTR_SHORT);
	

	
	writestr		(("usb_uhci: UHCI Host Controller Driver initialised \n"));


	usb_cmd		=	pci_read_io_16(UHCI_USBCMD);
	usb_sts		=	pci_read_io_16(UHCI_USBSTS);

	writestr(" command word : ");
	writeint(usb_cmd,16);
	writestr(" status word : ");
	writeint(usb_sts,16);
	writestr("\n");


	/* Determines number of ports on controller */

	/* The UHCI spec says devices must have 2 ports, and goes on to say
     * they may have more but gives no way to determine how many there
     * are.  However according to the UHCI spec, Bit 7 of the port
     * status and control register is always set to 1.  So we try to
     * use this to our advantage.  Another common failure mode when
     * a nonexistent register is addressed is to return all ones, so
     * we test for that also.
    */
	 #define   USBPORTSC_PR          0x0200  /* Port Reset */
     for (port = 0; port < 2; port++) 
	 {
			unsigned int status;
			unsigned int port_addr	; 
			 
			 port_addr			=	UHCI_PORTSC1 + (port * 2);


			 CLR_RH_PORTSTAT	(USBPORTSC_PR);
			 CLR_RH_PORTSTAT	(USBPORTSC_CSC | USBPORTSC_PEC);
			 SET_RH_PORTSTAT    (USBPORTSC_PE);

			 snooze				(1000000);

			 status = pci_read_io_16(UHCI_PORTSC1 + (port * 2));
			 if (!(status & 0x0080) || status == 0xffff)
				 break;
       }

      _config.num_ports=port;  
	  
  	   writestr("detected ");
	   writeint(_config.num_ports,16);
	   writestr(" ports \n");

	return 1;
	
}




OS_API_C_FUNC(int)	init_driver		()
{
	mem_ptr			physicalAddress;
	unsigned char	setup_packet[8];
	uhci_qh			*setup_q;
	unsigned int	*fFrameList;
	unsigned int	i;

	if(allocate_new_zone	(0,4096,&_config.transfer_buffer)<0)
	{
		writestr("could not allocated usb buffer \n");
		return 0;
	}

	physicalAddress	=	get_zone_ptr(&_config.transfer_buffer,0);
	fFrameList		=	physicalAddress;
	// Set base pointer and reset frame number
	pci_write_io_32		(UHCI_FRBASEADD	, mem_to_uint(physicalAddress));	
	pci_write_io_16		(UHCI_FRNUM		, 0);

	for(i=0;i<_config.num_ports;i++)
	{
		writestr("reset port ");
		writeint(i,16);
		writestr("\n");
		ResetPort(i);

	}

	writestr("create setup transfer \n");

	setup_q							=	create_setup_queue		(0,8,setup_packet);
	fFrameList[0]=create_qh_link			(setup_q);
	fFrameList[1]=create_qh_link			(setup_q);


	writestr("starting usb controller \n");
	start					();
	/*setup_interupt_c	(_config.irq, interupt_handler,0);*/


	return 1;

}


