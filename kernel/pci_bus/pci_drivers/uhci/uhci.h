

typedef struct
{
							//Ofset Size	 Value		Description
//0 = Host to Device
//1 = Device to Host
//D6..5 Type
//0 = Standard
//1 = Class
//2 = Vendor
//3 = Reserved
//D4..0 Recipient
//0 = Device
//1 = Interface
//2 = Endpoint
//3 = Other
//4..31 = Reserved
unsigned char  bmRequestType;	//0  1	 Bit-Map			D7 Data Phase Transfer Direction
unsigned char  bRequest;			//1	 1	 Value				Request
unsigned short wValue;			//2	 2	 Value				Value
unsigned short wIndex;			//4	 2	 Index or Offset	Index
unsigned short wLength;			//6	 2	 Count				Number of bytes to transfer if there is a data phase
}usb_setup_packet;

