#define DRIVER_FUNC	 C_EXPORT

#include <std_def.h>
#include <std_mem.h>
#include <kern.h>
#include <sys_pci.h>
#include <mem_base.h>
#include <lib_c.h>
#include <tree.h>
#include <sys/bitstream.h>
#include <sys/async_stream.h>
#include <sys/mem_stream.h>
#include <sys/tpo_mod.h>
#include <sys/task.h>
#include <sys/file_system.h>
#include <sys/stream_dev.h>
#include <sys/pci_device.h>

#include <bus_manager/bus_drv.h>

DRIVER_FUNC unsigned char  class_codes[]					= {	0x01, 0x01, 0xFF,
																0x01, 0x02, 0xFF,
																0x01, 0x05, 0x20,
																0x01, 0x05, 0x30,
																0x00, 0x00, 0x00};

DRIVER_FUNC unsigned int C_API_FUNC pci_get_device_cmd_flag		(unsigned short vendor_id ,unsigned short device_id);
DRIVER_FUNC int			 C_API_FUNC probe_device				(pci_device *device);
DRIVER_FUNC int			 C_API_FUNC init_driver					();
DRIVER_FUNC int			 C_API_FUNC stream_infos				(unsigned int str_idx		,mem_zone_ref	*infos_node);
DRIVER_FUNC int			 C_API_FUNC stream_set_pos				(unsigned int str_idx		,large_uint_t  sec_pos);
DRIVER_FUNC void		 C_API_FUNC read_stream					(unsigned int stream_idx	,mem_zone_ref *req);

#define		 ATA_BUFFER_SIZE									(unsigned int)0xFFFF

#define		 CMD_REG_FLAGS								0x01
#define		 BMIDE_REG_FLAGS							0x02
#define		 CTL_REG_FLAGS								0x00
#define		 REG_MASK									0x03



#define		 PRI_FLAGS									0x04
#define		 SEC_FLAGS									0x00
#define		 PS_MASK									0x04

#define		REG_INCOMPAT_DEVREG      0x0004   // set bits 7 and 5 to
                                          // to 1 in the Device
                                          // (Drive/Head) register
//-------------------------------------------
//ata register ofsets
//-------------------------------------------
typedef enum 
{
	REG_CMD_DATA      	=  0  ,		
	REG_CMD_FEAT_ERR  	=  1  ,		
	REG_CMD_SEC_CNT   	=  2  ,
	REG_CMD_SEC_ADDR   	=  3  ,
	REG_CMD_CYL_LOW  	=  4  ,
	REG_CMD_CYL_HI  	=  5  ,
	REG_CMD_HEADS       =  6  ,
	REG_CMD_STATUS_CMD	=  7
}ata_cmd_reg_t;

typedef enum 
{
	REG_STAT_ASTAT_DC		=	0x06,				
	REG_STAT_DEV_ADDR		=	0x07,
}ata_status_reg_t;

typedef enum 
{
	REG_BM_CMD  			=	0x00,           
	REG_BM_STATUS			=	0x02,
	REG_BM_PRD_ADDR_LOW		=	0x04,            // offset to prd addr reg low 16 bits
	REG_BM_PRD_ADDR_HIGH	=	0x06            // offset to prd addr reg high 16 bits
}ata_bm_reg_t;

//-------------------------------------------
//ata register flag values
//-------------------------------------------


//bm command reg flag
typedef enum 
{
	BM_CMD_READ    		=	0x00,          // read from memory
	BM_CMD_WRITE   		=	0x08,          // write to memory
	BM_CMD_START   		=	0x01,          // start transfer
	BM_CMD_STOP    		=	0x00,          // stop transfer
}bm_cmd_flags_t;

typedef enum 
{          
	REG_HEADS_LBA        =0x40,    // LBA bit
	REG_HEADS_DEV0       =0x00,    // select device 0
	REG_HEADS_DEV1       =0x10,    // select device 1
	REG_HEADS_OBSOLETE   =0xa0,    // bits 7 and 5 both 1 (obsolete)
}cmd_reg_flags_t;


typedef enum 
{    
REG_STATUS_BSY  =0x80,  // busy
REG_STATUS_RDY  =0x40,  // ready
REG_STATUS_DF   =0x20,  // device fault
REG_STATUS_WFT  =0x20,  // write fault (old name)
REG_STATUS_SKC  =0x10,  // seek complete (only SEEK command)
REG_STATUS_SERV =0x10,  // service (overlap/queued commands)
REG_STATUS_DRQ  =0x08,  // data request
REG_STATUS_CORR =0x04,  // corrected (obsolete)
REG_STATUS_IDX  =0x02,  // index (obsolete)
REG_STATUS_ERR  =0x01,  // error (ATA)
REG_STATUS_CHK  =0x01,  // check (ATAPI)
}cmd_status_flags_t;

typedef enum 
{    
REG_STATUS_TAG    =0xf8,   // ATAPI tag (mask)
REG_STATUS_REL    =0x04,   // ATAPI release
REG_STATUS_IO     =0x02,   // ATAPI I/O
REG_STATUS_CD     =0x01,   // ATAPI C/D
}cmd_reason_flags_t;


//bm status reg flag
typedef enum 
{          
BM_STATUS_SIMPLEX =0x80 ,				// simplex only
BM_STATUS_DRV1    =0x40 ,				// drive 1 can do dma
BM_STATUS_DRV0    =0x20 ,          		// drive 0 can do dma
BM_STATUS_INT     =0x04 ,          		// INTRQ signal asserted
BM_STATUS_ERR     =0x02 ,          		// error
BM_STATUS_ACT     =0x01 ,          		// active
}bm_status_flags_t;

// control reg flag
typedef enum 
{
CTL_LBA48  = 0x80  ,					// High Order Byte (48-bit LBA)
CTL_HD15   = 0x00  ,					// bit 3 is reserved
CTL_RESET  = 0x04  ,					// soft reset
CTL_NIEN   = 0x02  ,					// disable interrupts
}ctl_flag_t;


typedef enum 
{     
LBACHS =0,
LBA28  =28,          // last command used 28-bit LBA
LBA32  =32,          // last command used 32-bit LBA (Packet)
LBA48  =48,          // last command used 48-bit LBA
LBA64  =64,          // future use?
}lba_size_t;

typedef enum 
{
	SECTOR_TYPE_ANY			=	0x00	, // 000b Any Type (Mandatory) No checking of the Sector Type will be performed. The device shall always terminate a command, at the sector where a transition between CD-Rom and CD-DA occurs.
	SECTOR_TYPE_CDDA		=	0x01	, // 001b CD DA (Optional) Only Red Book (CD-DA) sectors shall be allowed. An attempt to read any other format shall result in the reporting of an error.
	SECTOR_TYPE_MODE_1		=	0x02	, // 010b Mode 1	(Mandatory)	Only Yellow Book sectors which have a �user� data field of 2048 bytes shall be	allowed. An attempt to read any other format shall result in the reporting of an error.
	SECTOR_TYPE_MODE_2		=	0x03	, // 011b Mode 2 (Mandatory) Only Yellow Book sectors which have a �user� data field of 2336 bytes shall be allowed. An attempt to read any other format shall result in the reporting of an error.
	SECTOR_TYPE_MODE_2_T1 	=	0x04	, // 100b Mode 2 Form 1 (Mandatory) Only Green Book sectors which have a �user� data field of 2048 shall be allowed. An attempt to read any other format shall result in the reporting of an error.
	SECTOR_TYPE_MODE_2_T2 	=	0x05	 // 101b Mode 2 Form 2 (Mandatory) Only Green Book sectors which have a �user� data field of 2324 shall be allowed. An attempt to read any other format shall result in the reporting of an error. Note that the spare data is included in the user data making the size 2324+4= 2328.
}expected_sector_type_t;



typedef enum 
{
	HEADER_TYPE_NONE		=	0x00	, // 00b None None of the header data shall be placed in the data stream.
	HEADER_TYPE_HDR			=	0x01	, // 01b HdrOnly Only the Mode 1 or Form 1 4-byte header will be returned in the data stream.
	HEADER_TYPE_SHDR		=	0x02	, // 10b SubheaderOnly Only the Mode 2 Form 1 or 2 Subheader will be placed into the data stream.
	HEADER_TYPE_ALL			=	0x03	  // 11b All Headers Both the Header and Subheader will be placed in the data stream.
}read_cd_hdr_t;

typedef enum 
{
	ERROR_TYPE_NONE		=	0x00	, // 00b None No Error information will be included in the data stream.
	ERROR_TYPE_ERR		=	0x01	, // 01b C2 Error Flag data The C2 Error Flag (Pointer) bits (2352 bits or 294 bytes) will be included in the data stream. When the C2 Error pointer bits are included in the data stream, there will be one bit for each byte in error in the sector (2352 total). The bit ordering is from the most significant bit to the least significant bit in each byte. The first bytes in the sector will be the first bits/bytes in the data stream.
	ERROR_TYPE_BLK		=	0x02	, // 10b C2 & Block Error Flags Both the C2 Error Flags (2352 bits or 294 bytes) and the Block Error Byte will be included in the data stream. The Block Error byte is the OR of all the C2 Error Flag bytes. So that the data stream will always be an even number of bytes, the Block Error byte will be padded with a byte (undefined). The Block Error byte will be first in the data stream followed by the pad byte. 
	ERROR_TYPE_ALL		=	0x03	  //11b Reserved Reserved for future enhancement.
}read_cd_err_t;




typedef enum 
{
	SUBC_TYPE_NONE		=	0x00	, // 000b No Sub-channel Data No Sub-channel data will be transferred Mandatory
	SUBC_TYPE_RAW		=	0x01	, // 001b RAW Raw Sub-channel data will be transferred Optional
	SUBC_TYPE_Q			=	0x02	, // 010b Q Q data will be transferred Optional
	SUBC_TYPE_RES		=	0x03	, // 011b Reserved
	SUBC_TYPE_RW		=	0x04	, // 100b R - W R-W data will be transferred Optional
}read_cd_subc_t;



typedef enum 
{
	USER_DATA_NO		=	0x00	,
	USER_DATA_YES		=	0x01	
}user_data_t;

typedef enum 
{
	SYNC_DATA_NO		=	0x00	, 
	SYNC_DATA_YES		=	0x01	
}sync_data_t;

typedef enum 
{
	ECC_DATA_NO			=	0x00	,
	ECC_DATA_YES		=	0x01	
}read_cd_ecc_t;

typedef enum 
{
	REG_CONFIG_TYPE_NONE	=	0,
	REG_CONFIG_TYPE_UNKN 	=	1,
	REG_CONFIG_TYPE_ATA		=	2,
	REG_CONFIG_TYPE_ATAPI	=	3,
	REG_CONFIG_TYPE_SATA	=	4
}ata_device_type_t;

//**************************************************************
//
// Most mandtory and optional ATA commands
//
//**************************************************************

typedef enum 
{
CMD_CFA_ERASE_SECTORS            =0xC0,
CMD_CFA_REQUEST_EXT_ERR_CODE     =0x03,
CMD_CFA_TRANSLATE_SECTOR         =0x87,
CMD_CFA_WRITE_MULTIPLE_WO_ERASE  =0xCD,
CMD_CFA_WRITE_SECTORS_WO_ERASE   =0x38,
CMD_CHECK_POWER_MODE1            =0xE5,
CMD_CHECK_POWER_MODE2            =0x98,
CMD_DEVICE_RESET                 =0x08,
CMD_EXECUTE_DEVICE_DIAGNOSTIC    =0x90,
CMD_FLUSH_CACHE                  =0xE7,
CMD_FLUSH_CACHE_EXT              =0xEA,
CMD_FORMAT_TRACK                 =0x50,
CMD_IDENTIFY_DEVICE              =0xEC,
CMD_IDENTIFY_DEVICE_PACKET       =0xA1,
CMD_IDENTIFY_PACKET_DEVICE       =0xA1,
CMD_IDLE1                        =0xE3,
CMD_IDLE2                        =0x97,
CMD_IDLE_IMMEDIATE1              =0xE1,
CMD_IDLE_IMMEDIATE2              =0x95,
CMD_INITIALIZE_DRIVE_PARAMETERS  =0x91,
CMD_INITIALIZE_DEVICE_PARAMETERS =0x91,
CMD_NOP                          =0x00,
CMD_PACKET                       =0xA0,
CMD_READ_BUFFER                  =0xE4,
CMD_READ_DMA                     =0xC8,
CMD_READ_DMA_EXT                 =0x25,
CMD_READ_MULTIPLE                =0xC4,
CMD_READ_MULTIPLE_EXT            =0x29,
CMD_READ_SECTORS                 =0x20,
CMD_READ_SECTORS_EXT             =0x24,
CMD_READ_VERIFY_SECTORS          =0x40,
CMD_READ_VERIFY_SECTORS_EXT      =0x42,
CMD_RECALIBRATE                  =0x10,
CMD_SEEK                         =0x70,
CMD_SET_FEATURES                 =0xEF,
CMD_SET_MULTIPLE_MODE            =0xC6,
CMD_SLEEP1                       =0xE6,
CMD_SLEEP2                       =0x99,
CMD_SMART                        =0xB0,
CMD_STANDBY1                     =0xE2,
CMD_STANDBY2                     =0x96,
CMD_STANDBY_IMMEDIATE1           =0xE0,
CMD_STANDBY_IMMEDIATE2           =0x94,
CMD_WRITE_BUFFER                 =0xE8,
CMD_WRITE_DMA                    =0xCA,
CMD_WRITE_DMA_EXT                =0x35,
CMD_WRITE_DMA_FUA_EXT            =0x3D,
CMD_WRITE_MULTIPLE               =0xC5,
CMD_WRITE_MULTIPLE_EXT           =0x39,
CMD_WRITE_MULTIPLE_FUA_EXT       =0xCE,
CMD_WRITE_SECTORS                =0x30,
CMD_WRITE_SECTORS_EXT            =0x34,
CMD_WRITE_VERIFY                 =0x3C
}ata_cmd_t;							



typedef enum 
{
	ATAPI_CMD_READ_TOC		=	0x43,    // command code
	ATAPI_CMD_READ_10		=	0x28,    // command code
	ATAPI_CMD_READ_12		=	0xA8,    // command code
	ATAPI_CMD_READ_CD		=	0xBE,    // command code
	ATAPI_CMD_SEEK		    =	0x2B,

}atapi_cmd_t;
typedef union
{
	struct{
		unsigned long lba;
	}lba;

	struct{
		unsigned short cyl;
		unsigned char  sect;
		unsigned char  head;
	}chs;
}lba_chs;


#define MAX_PRD 128

struct dma_region_desc_t
{
	unsigned int	addr;
	unsigned short	count;
	unsigned short	flags;
};

struct controller_t
{
	mem_zone_ref			req_list;
	mem_zone_ref			current_req;
	mem_zone_ref			vars;

/*	unsigned int			irq_line;*/
	unsigned int			is_cmd_io;
	unsigned int			cmd_base;

	unsigned int			*loop_used;
	unsigned int			*controller_used;

	unsigned int			is_ctl_io;
	unsigned int			ctl_base;

	unsigned int			bmide_base;
	unsigned int			bmide_io;

	ata_device_type_t		reg_config_info[2];
	unsigned int			stream_idx[2];

	unsigned int			statReg;
	unsigned int			rwControl;

	mem_zone_ref			dma_region_desc;
	mem_ptr					dma_region_desc_ptr;
	mem_zone_ref			dma_req;

	mem_ptr					sema;
};

unsigned int kernel_log_id								=	0xFFFFFFFF;

struct controller_t	*controllers[2]				=	{PTR_INVALID};
mem_zone_ref		controllers_ptr_ref			=	{PTR_INVALID};
unsigned int		int_intr_flag				=	0xFFFFFFFF;
unsigned char		int_bm_status				=	0xFF;
unsigned char		int_ata_status				=	0xFF;
unsigned int		_config_pci_bus_id			=	0xFFFFFFFF;
unsigned int		_config_pci_dev_id			=	0xFFFFFFFF;
unsigned int		stream_dev_id				= 0xFFFFFFF;
unsigned char		dev75						= 0xFF;


									
#define PRD_TYPE_SIMPLE  0       // use PRD buffer, simple list
#define PRD_TYPE_COMPLEX 1       // use PRD buffer, complex list
#define PRD_TYPE_LARGE   2       // use I/O buffer, overlay I/O buffer







unsigned int  write_bm_reg_8( ata_bm_reg_t reg,unsigned int primary,unsigned char x)
{
   if ( controllers[primary]->bmide_base < 0x0100 )	  return 0;
   if ( controllers[primary]->bmide_base== 0xFFFFFFFF) return 0;
   
   out_8_c( controllers[primary]->bmide_base + reg, x );

   return 1;
}

unsigned int  write_bm_reg_16( ata_bm_reg_t reg,unsigned int primary,unsigned short x)
{
   if ( controllers[primary]->bmide_base < 0x0100 )	  return 0;
   if ( controllers[primary]->bmide_base== 0xFFFFFFFF) return 0;
   
   out_16_c( controllers[primary]->bmide_base + reg, x );

   return 1;
}

unsigned int  read_bm_reg_8( ata_bm_reg_t reg,unsigned int primary,unsigned char *x)
{
   if ( controllers[primary]->bmide_base < 0x0100 )	  return 0;
   if ( controllers[primary]->bmide_base== 0xFFFFFFFF) return 0;
   
   *x=in_8_c( controllers[primary]->bmide_base + reg);

   return 1;
}

unsigned int write_cmd_reg_8	(ata_cmd_reg_t reg,unsigned int primary,unsigned char value)
{
	if(controllers[primary]->is_cmd_io)
	{
		out_8_c(controllers[primary]->cmd_base+reg,value);
	}
	else
	{
		*((unsigned char *)(uint_to_mem(controllers[primary]->cmd_base+reg)))=value;
	}
	return 1;
}

unsigned int read_cmd_reg_8	(ata_cmd_reg_t reg,unsigned int primary,unsigned char *value)
{
	if(controllers[primary]->is_cmd_io)
	{
		*value=in_8_c(controllers[primary]->cmd_base+reg);
	}
	else
	{
		*value=*((unsigned char *)(uint_to_mem(controllers[primary]->cmd_base+reg)));
	}
	return 1;
}

unsigned int write_stat_reg_8	(ata_status_reg_t reg,unsigned int primary,unsigned char value)
{
	if(controllers[primary]->is_ctl_io)
	{
		out_8_c(controllers[primary]->ctl_base+reg,value);
	}
	else
	{
		*((unsigned char *)(uint_to_mem(controllers[primary]->ctl_base+reg)))=value;
	}
	return 1;
}

unsigned int read_stat_reg_8	(ata_status_reg_t reg,unsigned int primary,unsigned char *value)
{
	if(controllers[primary]->is_ctl_io)
	{
		*value=in_8_c(controllers[primary]->ctl_base+reg);
	}
	else
	{
		*value=*((unsigned char *)(uint_to_mem(controllers[primary]->ctl_base+reg)));
	}
	return 1;

}


void sub_writeBusMstrCmd( unsigned int primary,unsigned char x )
{
   write_bm_reg_8(REG_BM_CMD,primary,x);
}
unsigned char sub_readBusMstrCmd( unsigned int primary )
{
   unsigned char x;
   read_bm_reg_8	(REG_BM_CMD,primary,&x );
   return x;
}


void sub_writeBusMstrStatus( unsigned int primary,unsigned char x )
{
	write_bm_reg_8(REG_BM_STATUS,primary,x);
}

unsigned char sub_readBusMstrStatus(  unsigned int primary )

{
   unsigned char x;
   read_bm_reg_8	(REG_BM_STATUS,primary,&x );
   return x;
}

OS_API_C_FUNC(unsigned int) pci_get_device_cmd_flag (unsigned short vendor_id ,unsigned short device_id)
{
	return PCI_PCICMD_IOS | PCI_PCICMD_MSE | PCI_PCICMD_BME;
}

unsigned short set_null_bar(unsigned short bar_addr,unsigned int type)
{
	unsigned short ret_bar;
	ret_bar		=	bar_addr  & 0xfffe;

	if(ret_bar==0)
	{
		if((type & REG_MASK)==CMD_REG_FLAGS)
		{
			if((type & PS_MASK)== PRI_FLAGS)
				ret_bar  = 0x1f0;
			else
				ret_bar  = 0x170;
				 
		}
		if((type & REG_MASK)==CTL_REG_FLAGS)
		{
			if((type & PS_MASK)==PRI_FLAGS)
				 ret_bar  = 0x3f0;
			else
				ret_bar  = 0x370;
				 
		}
	}
	else
	{
		if((type & REG_MASK)==CTL_REG_FLAGS)
			ret_bar -= 4;

		if(((type & REG_MASK)==BMIDE_REG_FLAGS)&&((type &PS_MASK)== SEC_FLAGS))
			ret_bar += 8;
	}
	return ret_bar;
}



unsigned short parse_bar(unsigned short bar_addr,unsigned int *is_io)
{
	unsigned short ret_bar;

	*(is_io)	=	bar_addr  & 0x01;
	ret_bar		=	bar_addr  & 0xfffe;

	
	return ret_bar;

}


void ATA_DELAY(unsigned int primary) 
{
	unsigned char val;

	read_stat_reg_8	(REG_STAT_ASTAT_DC,primary,&val);
	snooze			(500);
	read_stat_reg_8	(REG_STAT_ASTAT_DC,primary,&val);
}

void ata_select_device(unsigned int primary,unsigned int dev)
{
	unsigned char		dev_flg;
	dev_flg = 0;

	if(dev==0)
		dev_flg |= REG_HEADS_DEV0;
	else
		dev_flg |= REG_HEADS_DEV1;

   write_cmd_reg_8		(REG_CMD_HEADS		,primary,dev_flg|dev75 );
   ATA_DELAY			(primary);
}


unsigned int wait_device_reg_ready(unsigned int primary,unsigned int dev)
{
	unsigned int	infos_ret;
	unsigned char	sc;
	unsigned char	status;
	unsigned char	sn;
	unsigned int	time_start,time_cur;

	if ( controllers[primary]->reg_config_info[dev] == REG_CONFIG_TYPE_NONE )return 0;

	
	infos_ret=0;
	time_start=system_time();

	ata_select_device	(primary,dev);

	while ( 1 )
	{
		read_cmd_reg_8		( REG_CMD_SEC_CNT   ,primary, &sc);
		read_cmd_reg_8		( REG_CMD_SEC_ADDR  ,primary, &sn);

		 //sc = pio_inbyte( CB_SC );
		 //sn = pio_inbyte( CB_SN );

		 if ( ( sc == 0x01 ) && ( sn == 0x01 ) )
			break;

		 time_cur=system_time();
	 
		 if (( time_cur-time_start )>=2000)
		 {
			infos_ret = 1;
			break;
		 }
	}
  

  // Check if drive 1 has valid BSY=0 status

  if ( infos_ret == 0 )
  {
     //status = pio_inbyte( CB_STAT );
	 read_cmd_reg_8(REG_CMD_STATUS_CMD,primary,&status);
     if ( ( status == 0x7f ) || ( status & REG_STATUS_BSY ) )
     {
		infos_ret=1;
     }
  }
  
  return infos_ret;
}

unsigned int wait_device_busy_drq(unsigned int primary)
{
	unsigned int	time_start,time_cur;
	unsigned char	status;

   time_start=system_time();

   while ( 1 )
   {
	  read_cmd_reg_8(REG_CMD_STATUS_CMD,primary,&status);
      
	  if ( ( status & ( REG_STATUS_BSY | REG_STATUS_DRQ ) ) == 0 )
         break;

	  time_cur=system_time();

	  
      if ( time_cur-time_start )
      {
		 kernel_log			(kernel_log_id,"device busy drq timeout ");
		 writeint			(primary,10);
		 writestr			("\n");
         return 0;
      }
   }

   return 1;
}

int sub_select( unsigned int primary,int dev )

{
   unsigned int	 ret;

	
/*
   // determine value of Device (Drive/Head) register bits 7 and 5
   dev75 = 0;                    // normal value
   if ( reg_incompat_flags & REG_INCOMPAT_DEVREG )
      dev75 = CB_DH_OBSOLETE;    // obsolete value

   // PAY ATTENTION HERE
   // The caller may want to issue a command to a device that doesn't
   // exist (for example, Exec Dev Diag), so if we see this,
   // just select that device, skip all status checking and return.
   // We assume the caller knows what they are doing!

   if ( reg_config_info[ dev ] < REG_CONFIG_TYPE_ATA )
   {
      // select the device and return

      pio_outbyte( CB_DH, ( dev ? CB_DH_DEV1 : CB_DH_DEV0 ) | dev75 );
      ATA_DELAY();
      return 0;
   }
*/
	

	 if ( controllers[primary]->reg_config_info[ dev ] < REG_CONFIG_TYPE_ATA )
	 {
		 ata_select_device	(primary,dev);
		 kernel_log			(kernel_log_id,"device not configured ");
		 writeint			(primary,10);
		 writestr			(" ");
		 writeint			(dev,10);
		 writestr			("\n");
		 return 0;
	 }

   // The rest of this is the normal ATA stuff for device selection
   // and we don't expect the caller to be selecting a device that
   // does not exist.
   // We don't know which drive is currently selected but we should
   // wait BSY=0 and DRQ=0. Normally both BSY=0 and DRQ=0
   // unless something is very wrong!

	 wait_device_busy_drq	(primary);


   // Here we select the drive we really want to work with by
   // setting the DEV bit in the Drive/Head register.

	ata_select_device		(primary,dev);

   // Wait for the selected device to have BSY=0 and DRQ=0.
   // Normally the drive should be in this state unless
   // something is very wrong (or initial power up is still in
   // progress).

	ret=wait_device_busy_drq	(primary);

   // All done.  The return values of this function are described in
   // ATAIO.H.

   return ret;
}

void sub_setup_command_lba( int primary,int dev,unsigned short fr, unsigned short  sc,long lba,unsigned int use_intr ,lba_size_t lbaSize)
{
   unsigned char fr48[2];
   unsigned char sc48[2];
   unsigned char lba48[8];

   // determine value of Device (Drive/Head) register bits 7 and 5

   // WARNING: THIS CODE IS DESIGNED FOR A STUPID PROCESSOR
   // LIKE INTEL X86 THAT IS Little-Endian, THAT IS, A
   // PROCESSOR THAT STORES DATA IN MEMORY IN THE WRONG
   // BYTE ORDER !!!

   * (unsigned short *) fr48 		= fr;
   * (unsigned short *) sc48 		= sc;
   * (unsigned int *) ( lba48 + 4 ) = 0;
   * (unsigned int *) ( lba48 + 0 ) = lba;

   write_stat_reg_8	(REG_STAT_ASTAT_DC,primary, use_intr ? 0 : CTL_NIEN );

   if ( lbaSize == LBA28 )
   {
      // in ATA LBA28 mode

      write_cmd_reg_8( REG_CMD_FEAT_ERR	,primary, fr48[0] );
      write_cmd_reg_8( REG_CMD_SEC_CNT	,primary, sc48[0] );
      write_cmd_reg_8( REG_CMD_SEC_ADDR	,primary, lba48[0] );
      write_cmd_reg_8( REG_CMD_CYL_LOW	,primary, lba48[1] );
      write_cmd_reg_8( REG_CMD_CYL_HI	,primary, lba48[2] );
      write_cmd_reg_8( REG_CMD_HEADS	,primary, REG_HEADS_LBA | (dev ? REG_HEADS_DEV1 : REG_HEADS_DEV0)  | dev75 );
   }
   else if ( lbaSize == LBA48 )
   {
      // in ATA LBA48 mode


      write_cmd_reg_8( REG_CMD_FEAT_ERR	,primary, fr48[1] );
      write_cmd_reg_8( REG_CMD_SEC_CNT	,primary, sc48[1] );
      write_cmd_reg_8( REG_CMD_SEC_ADDR	,primary, lba48[3] );
      write_cmd_reg_8( REG_CMD_CYL_LOW	,primary, lba48[4] );
      write_cmd_reg_8( REG_CMD_CYL_HI	,primary, lba48[5] );

      write_cmd_reg_8( REG_CMD_FEAT_ERR	,primary, fr48[0] );
      write_cmd_reg_8( REG_CMD_SEC_CNT	,primary, sc48[0] );
      write_cmd_reg_8( REG_CMD_SEC_ADDR	,primary, lba48[0] );
      write_cmd_reg_8( REG_CMD_CYL_LOW	,primary, lba48[1] );
      write_cmd_reg_8( REG_CMD_CYL_HI	,primary, lba48[2] );

      write_cmd_reg_8( REG_CMD_HEADS	,primary, REG_HEADS_LBA | (dev ? REG_HEADS_DEV1 : REG_HEADS_DEV0)  | dev75 );


   }
   else
   {
      // in ATA CHS or ATAPI LBA32 mode

   }
}

void sub_setup_command_chs( int primary,int dev,unsigned char fr, unsigned short  sc,unsigned int cyl, unsigned int head, unsigned int sect,unsigned int use_intr )
{

	write_stat_reg_8	(REG_STAT_ASTAT_DC,primary, use_intr ? 0 : CTL_NIEN );

	write_cmd_reg_8		( REG_CMD_FEAT_ERR	,primary, fr  );
	write_cmd_reg_8		( REG_CMD_SEC_CNT	,primary, sc&0xFF  );
	write_cmd_reg_8		( REG_CMD_SEC_ADDR	,primary, sect  );
	write_cmd_reg_8		( REG_CMD_CYL_LOW	,primary,  cyl&0x00FF  );
	write_cmd_reg_8		( REG_CMD_CYL_HI	,primary, (cyl&0xFF00)>>8  );
	write_cmd_reg_8		( REG_CMD_HEADS		,primary, (dev ? REG_HEADS_DEV1 : REG_HEADS_DEV0 )| ( head & 0x0f ) | dev75 );
}



unsigned int reg_wait_poll(unsigned int primary, int we, int pe ,int use_intr)
{

	unsigned int	time_start,time_cur;
	unsigned int	infos_ret;
	unsigned char	status;

   time_start=system_time();

   // Wait for interrupt -or- wait for not BUSY -or- wait for time out.

   infos_ret=0;
   if ( we && use_intr )
   {
      while ( 1 )
      {
		   time_cur=system_time();
         if ( int_intr_flag )                // interrupt ?
         {
            status =  int_ata_status;         // get status
            break;
         }
         if ( (time_cur-time_start)>=2000 )            // time out ?
         {

			 kernel_log			(kernel_log_id,"wait poll time out 1");
			 writeint			(primary,10);
			 writestr			(" ");
			 writeint			(use_intr,10);
			 writestr			("\n");
			 infos_ret=1;
            break;
         }
      }
   }
   else
   {
      while ( 1 )
      {
		 read_stat_reg_8	(REG_STAT_ASTAT_DC,primary,&status);

		 
         if ( ( status & REG_STATUS_BSY ) == 0 )
            break;

		 time_cur=system_time();
         if ( (time_cur-time_start)>=2000 )              // time out yet ?
         {
			 kernel_log			(kernel_log_id,"wait poll time out 2");
			 writeint			(primary,10);
			 writestr			(" ");
			 writeint			(use_intr,10);
			 writestr			("\n");
			infos_ret=1;
            break;
         }
      }
   }
   // Reset the interrupt flag.
	/*
   if ( int_intr_flag > 1 )      // extra interrupt(s) ?
      reg_cmd_info.failbits |= FAILBIT15;
   */
   int_intr_flag = 0;

   return infos_ret;
}




static int set_up_xfer( unsigned int primary,int dir, size_t bc, mem_ptr data_addr )
{
   int					numPrd;                      // number of PRD required
 struct  dma_region_desc_t	*prdPtr;
 
   // disable/stop the dma channel, clear interrupt and error bits

   sub_writeBusMstrCmd		( primary,BM_CMD_STOP );
   sub_writeBusMstrStatus	( primary,controllers[primary]->statReg| BM_STATUS_INT | BM_STATUS_ERR );

   // build the PRD list...
   // ...PRD entry format:
   // +0 to +3 = memory address
   // +4 to +5 = 0x0000 (not EOT) or 0x8000 (EOT)
   // +6 to +7 = byte count
   // ...zero number of PRDs
   numPrd	= 0;
   prdPtr	= controllers[primary]->dma_region_desc_ptr;
   // ...loop to build each PRD
   while ( bc > 0 )
   {
      unsigned int	seg_cnt=65536L;
      if ( numPrd >= MAX_PRD )return 1;
      // set this PRD's address
	  prdPtr[numPrd].addr	= mem_to_uint(data_addr);
	  // set the end bit in the prd list
	  if ( bc <= seg_cnt )
	  {
		  prdPtr[numPrd].count  =   (unsigned short)(bc);
		  prdPtr[numPrd].flags  =   0x8000;
		  bc					=   0;
	  }
	  else
	  {
		  prdPtr[numPrd].flags   =  0;
		  prdPtr[numPrd].count   =  0;
		  bc					-=	seg_cnt;
	  }
	  writestr_fmt	("prd %p %x %x %x\n",prdPtr,prdPtr->addr,prdPtr->count,prdPtr->flags);
	  
	  data_addr	=	mem_add(data_addr,prdPtr[numPrd].count>0?prdPtr[numPrd].count:seg_cnt);
	  numPrd ++ ;
   }
   
   write_bm_reg_16(REG_BM_PRD_ADDR_LOW	,primary,  0x0000 );
   write_bm_reg_16(REG_BM_PRD_ADDR_HIGH ,primary,  mem_to_ushort(prdPtr,16));
   // set the read/write control:
   // PCI reads for ATA Write DMA commands,
   // PCI writes for ATA Read DMA commands.
   if ( dir )
	   controllers[primary]->rwControl=BM_CMD_READ;
   else
	   controllers[primary]->rwControl=BM_CMD_WRITE;

   sub_writeBusMstrCmd( primary,controllers[primary]->rwControl);     // ATA Write DMA
   
   return 0;
}



static int exec_pci_ata_cmd( unsigned int primary,int dev,unsigned short fr,unsigned int cmd,
                             mem_ptr	  addr,
                             lba_chs lba,unsigned int lba_size,unsigned short numSect )

{
   unsigned char status;

   // Quit now if no dma channel set up
   // or interrupts are not enabled.

   if (! controllers[primary]->bmide_base)
   {
	  kernel_log(kernel_log_id,"no bmid ide base dma \n");
      return 1;
   }

   // Set up the dma transfer
   if ( set_up_xfer(primary,( cmd == CMD_WRITE_DMA )|| ( cmd == CMD_WRITE_DMA_EXT )|| ( cmd == CMD_WRITE_DMA_FUA_EXT ),numSect * 512L, addr ) )
   {
	  kernel_log(kernel_log_id,"setup xfer over run \n");
      return 1;
   }

   // Select the drive - call the sub_select function.
   // Quit now if this fails.

   if ( sub_select( primary,dev ) == 0)
   {
	  kernel_log(kernel_log_id,"dma sub select fail\n");
      return 1;
   }
   
   // Set up all the registers except the command register.
	if(lba_size==LBACHS)
	   sub_setup_command_chs(primary,dev,((unsigned char)(fr&0xFF)),numSect,lba.chs.cyl,lba.chs.head,lba.chs.sect,1);
	else
	   sub_setup_command_lba(primary,dev,fr,numSect,lba.lba.lba,lba_size,1);

   // Start the command by setting the Command register.  The drive
   // should immediately set BUSY status.

	int_intr_flag=0;

    write_bm_reg_8	(REG_BM_CMD,primary,cmd);

   // The drive should start executing the command including any
   // data transfer.

   // Data transfer...
   // read the BMIDE regs
   // enable/start the dma channel.
   // read the BMIDE regs again

   sub_readBusMstrCmd	(primary);
   sub_readBusMstrStatus(primary);
   sub_writeBusMstrCmd	(primary, controllers[primary]->rwControl | BM_CMD_START );
   sub_readBusMstrCmd	(primary);
   sub_readBusMstrStatus(primary);

   // Data transfer...
   // the device and dma channel transfer the data here while we start
   // checking for command completion...
   // wait for the PCI BM Interrupt=1 (see ATAIOINT.C)...


   while ( !int_intr_flag )
   {
	   snooze(10000);
   }

   // End of command...
   // disable/stop the dma channel

   status		=   int_bm_status;					// read BM status
   status	   &= ~ BM_STATUS_ACT;					// ignore Active bit
   sub_writeBusMstrCmd	(primary,BM_CMD_STOP );		// shutdown DMA
   sub_readBusMstrCmd	(primary);                  // read BM cmd (just for trace)
   status |= sub_readBusMstrStatus(primary);        // read BM status again

   if ( status & BM_STATUS_ERR )					// bus master error?
   {
	   writestr("status error \n");
      return 0;
   }
   if ( status & BM_STATUS_ACT )            // end of PRD list?
   {
		writestr("status active \n");
		return 0;
   }

   // End of command...
   // If no error use the Status register value that was read
   // by the interrupt handler. If there was an error
   // read the Status register because it may not have been
   // read by the interrupt handler.

	status = int_ata_status;

	// Final status check...
	// if no error, check final status...
	// Error if BUSY, DEVICE FAULT, DRQ or ERROR status now.

	if ( status & (  REG_STATUS_BSY |  REG_STATUS_DF | REG_STATUS_DRQ |  REG_STATUS_ERR ) )
	{
		writestr("bad status \n");
		return 0;
	}
	return 1;
}


static int exec_non_data_cmd(unsigned int primary, unsigned int dev,unsigned int cmd,unsigned char fr,lba_chs lba,unsigned int lba_size,unsigned int use_intr )

{
   unsigned char status;
   unsigned char bm_error;
   int			 infos_ret;
   int polled = 0;

   // determine value of Device (Drive/Head) register bits 7 and 5



   // reset Bus Master Error bit

   infos_ret=0;
   write_bm_reg_8	(REG_BM_STATUS		,primary,BM_STATUS_ERR);
   // Set command time out.

   // PAY ATTENTION HERE
   // If the caller is attempting a Device Reset command, then
   // don't do most of the normal stuff.  Device Reset has no
   // parameters, should not generate an interrupt and it is the
   // only command that can be written to the Command register
   // when a device has BSY=1 (a very strange command!).  Not
   // all devices support this command (even some ATAPI devices
   // don't support the command.

   if ( cmd != CMD_DEVICE_RESET )
   {
      // Select the drive - call the sub_select function.
      // Quit now if this fails.

      if ( sub_select( primary,dev ) == 0)
      {
		 kernel_log			(kernel_log_id,"ata sub select non data failed\n ");
         return 0;
      }

      // Set up all the registers except the command register.
	   if(lba_size==LBACHS)
		   sub_setup_command_chs(primary,dev,fr,0,lba.chs.cyl,lba.chs.head,lba.chs.sect,use_intr);
	   else
		   sub_setup_command_lba(primary,dev,fr,0,lba.lba.lba,use_intr,lba_size);

      // For interrupt mode, install interrupt handler.

      //int_save_int_vect();
   }

   // Start the command by setting the Command register.  The drive
   // should immediately set BUSY status.

  write_cmd_reg_8( REG_CMD_STATUS_CMD,primary, cmd );


   // Waste some time by reading the alternate status a few times.
   // This gives the drive time to set BUSY in the status register on
   // really fast systems.  If we don't do this, a slow drive on a fast
   // system may not set BUSY fast enough and we would think it had
   // completed the command when it really had not even started the
   // command yet.

   ATA_DELAY(primary);
   //ATAPI_DELAY( 0 );
   //ATAPI_DELAY( 1 );

   // IF
   //    This is an Exec Dev Diag command (cmd=0x90)
   //    and there is no device 0 then
   //    there will be no interrupt. So we must
   //    poll device 1 until it allows register
   //    access and then do normal polling of the Status
   //    register for BSY=0.
   // ELSE
   // IF
   //    This is a Dev Reset command (cmd=0x08) then
   //    there should be no interrupt.  So we must
   //    poll for BSY=0.
   // ELSE
   //    Do the normal wait for interrupt or polling for
   //    completion.

   if ( ( cmd == CMD_EXECUTE_DEVICE_DIAGNOSTIC )
        &&
        ( controllers[primary]->reg_config_info[dev] == REG_CONFIG_TYPE_NONE )
      )
   {
      polled = 1;
      
	  /*
      while ( 1 )
      {
         pio_outbyte( CB_DH, CB_DH_DEV1 | dev75 );
         ATA_DELAY();

		 secCnt = pio_inbyte( CB_SC );
         secNum = pio_inbyte( CB_SN );
         if ( ( secCnt == 0x01 ) && ( secNum == 0x01 ) )
            break;
         if ( tmr_chk_timeout() )
         {
            trc_llt( 0, 0, TRC_LLT_TOUT );
            reg_cmd_info.to = 1;
            reg_cmd_info.ec = 24;
            trc_llt( 0, reg_cmd_info.ec, TRC_LLT_ERROR );
            break;
         }
      }
	  */
	  infos_ret	=	wait_device_reg_ready(primary,1);
   }
   else
   {
      if ( cmd == CMD_DEVICE_RESET )
      {
         // Wait for not BUSY -or- wait for time out.

         polled		=	1;
         infos_ret	=	reg_wait_poll(primary, 0, 23 ,0);
      }
      else
      {
         // Wait for interrupt -or- wait for not BUSY -or- wait for time out.

         if ( ! use_intr )
            polled = 1;
         infos_ret	=	reg_wait_poll(primary, 22, 23,use_intr );
      }
   }

   // If status was polled or if any error read the status register,
   // otherwise get the status that was read by the interrupt handler.

   if ( ( polled ) || ( infos_ret ) )
      read_cmd_reg_8(REG_CMD_STATUS_CMD ,primary,&status);
   else
      status = int_ata_status;

   // Error if BUSY, DEVICE FAULT, DRQ or ERROR status now.

   if (infos_ret == 0 )
   {
      if ( status & (REG_STATUS_BSY | REG_STATUS_DF | REG_STATUS_ERR |REG_STATUS_DRQ) )
      {
		 infos_ret=1;
      }
   }

   // BMIDE Error=1?
   read_bm_reg_8(REG_BM_STATUS,primary,&bm_error);
   if ( bm_error & BM_STATUS_ERR )
   {
      infos_ret=1;
   }

   // NON_DATA_DONE:

   // For interrupt mode, remove interrupt handler.

   //int_restore_int_vect();

   // mark end of ND cmd in low level trace

   // All done.  The return values of this function are described in
   // ATAIO.H.

   if ( infos_ret )
      return 0;
   return 1;
}

//*************************************************************
//
// reg_non_data_chs() - Execute a non-data command.
//
// Note special handling for Execute Device Diagnostics
// command when there is no device 0.
//
// See ATA-2 Section 9.5, ATA-3 Section 9.5,
// ATA-4 Section 8.8 Figure 12.  Also see Section 8.5.
//
//*************************************************************

int reg_non_data_chs(  unsigned int primary,unsigned int dev, unsigned int cmd,
                      unsigned char fr, unsigned short cyl, unsigned char head, unsigned char sect, unsigned int use_intr )

{
	lba_chs	sec_start;

   // Setup command parameters.
   // Execute the command.
   sec_start.chs.cyl=cyl;
   sec_start.chs.head=head;
   sec_start.chs.sect=sect;
	
   return exec_non_data_cmd(primary,dev,cmd,fr,sec_start,LBACHS,use_intr );
}

//*************************************************************
//
// reg_non_data_lba28() - Easy way to execute a non-data command
//                        using an LBA sector address.
//
//*************************************************************

int reg_non_data_lba28( unsigned int primary,unsigned int dev,unsigned int cmd,
                        unsigned int fr, 
                        unsigned long lba, unsigned int use_intr  )

{
	lba_chs	sec_start;

   // Execute the command.
   sec_start.lba.lba=lba;
	
   return exec_non_data_cmd(primary,dev,cmd,fr,sec_start,LBA28,use_intr );


/*
   // Setup current command information.

   sub_zero_return_data();
   reg_cmd_info.flg = TRC_FLAG_ATA;
   reg_cmd_info.ct  = TRC_TYPE_AND;
   reg_cmd_info.cmd = cmd;
   reg_cmd_info.fr1 = fr;
   reg_cmd_info.sc1 = sc;
   reg_cmd_info.dh1 = CB_DH_LBA | (dev ? CB_DH_DEV1 : CB_DH_DEV0 );
   reg_cmd_info.dc1 = int_use_intr_flag ? 0 : CB_DC_NIEN;
   reg_cmd_info.ns  = sc;
   reg_cmd_info.lbaSize = LBA28;
   reg_cmd_info.lbaHigh1 = 0L;
   reg_cmd_info.lbaLow1 = lba;

   // Execute the command.

   return exec_non_data_cmd( dev );
*/
}

//*************************************************************
//
// reg_non_data_lba48() - Easy way to execute a non-data command
//                        using an LBA sector address.
//
//*************************************************************

int reg_non_data_lba48( unsigned int primary, unsigned int dev, unsigned int cmd,
                        unsigned char fr,
                        unsigned long lbahi, unsigned long lbalo, unsigned int use_intr  )

{

	lba_chs	sec_start;

   // Execute the command.
   sec_start.lba.lba=lbalo;
	
   return exec_non_data_cmd(primary,dev,cmd,fr,sec_start,LBA48,use_intr );

	/*
   // Setup current command infomation.

   sub_zero_return_data();
   reg_cmd_info.flg = TRC_FLAG_ATA;
   reg_cmd_info.ct  = TRC_TYPE_AND;
   reg_cmd_info.cmd = cmd;
   reg_cmd_info.fr1 = fr;
   reg_cmd_info.sc1 = sc;
   reg_cmd_info.dh1 = CB_DH_LBA | (dev ? CB_DH_DEV1 : CB_DH_DEV0 );
   reg_cmd_info.dc1 = int_use_intr_flag ? 0 : CB_DC_NIEN;
   reg_cmd_info.ns  = sc;
   reg_cmd_info.lbaSize = LBA48;
   reg_cmd_info.lbaHigh1 = lbahi;
   reg_cmd_info.lbaLow1 = lbalo;

   // Execute the command.

   return exec_non_data_cmd( dev );
   */
}


//*************************************************************
//
// reg_reset() - Execute a Software Reset.
//
// See ATA-2 Section 9.2, ATA-3 Section 9.2, ATA-4 Section 8.3.
//
//*************************************************************

int reg_reset( int devRtrn, int primary,int use_intr)

{
  
   unsigned char devCtrl;
   unsigned char bm_error;
   unsigned int	 infos_ret;

   // determine value of Device (Drive/Head) register bits 7 and 5
   // setup register values

   infos_ret=0;

   devCtrl	= CTL_HD15 | ( use_intr ? 0 : CTL_NIEN );


   write_bm_reg_8	(REG_BM_STATUS		,primary,BM_STATUS_ERR);



   // set SRST=1
   write_stat_reg_8	(REG_STAT_ASTAT_DC,primary, devCtrl|CTL_RESET );
   ATA_DELAY		(primary);      // for trace of Alternate Status
   snooze			(100);
   write_stat_reg_8	(REG_STAT_ASTAT_DC,primary, devCtrl );
   ATA_DELAY		(primary);      

   //ATAPI_DELAY( 0 );
   //ATAPI_DELAY( 1 );

   // If there is a device 0, wait for device 0 to set BSY=0.

   
   wait_device_reg_ready		(primary,0);
   wait_device_reg_ready		(primary,1);
   // RESET_DONE:

   // We are done but now we must select the device the caller
   // requested before we trace the command.  This will cause
   // the correct data to be returned in reg_cmd_info.

   ata_select_device	(primary,devRtrn);
   

   // If possible, select a device that exists
   if ( controllers[primary]->reg_config_info[1] != REG_CONFIG_TYPE_NONE )
   {
		ata_select_device	(primary,1);
   }
   if ( controllers[primary]->reg_config_info[0] != REG_CONFIG_TYPE_NONE )
   {
		ata_select_device	(primary,0);
   }

   // BMIDE Error=1?

   read_bm_reg_8(REG_BM_STATUS,primary,&bm_error);
   if ( bm_error & BM_STATUS_ERR )
   {
      infos_ret=1;
   }

   return infos_ret;
}

void ata_detect_device(unsigned int primary,unsigned int dev)
{
    unsigned char		sc;
    unsigned char		sn;
	

	controllers[primary]->reg_config_info[dev] = REG_CONFIG_TYPE_NONE;
	
	ata_select_device	(primary,dev);

	write_cmd_reg_8		( REG_CMD_SEC_CNT	,primary, 0x55 );
	write_cmd_reg_8		( REG_CMD_SEC_ADDR	,primary, 0xaa );
	write_cmd_reg_8		( REG_CMD_SEC_CNT	,primary, 0xaa );
	write_cmd_reg_8		( REG_CMD_SEC_ADDR	,primary, 0x55 );
	write_cmd_reg_8		( REG_CMD_SEC_CNT	,primary, 0x55 );
	write_cmd_reg_8		( REG_CMD_SEC_ADDR	,primary, 0xaa );

	read_cmd_reg_8		( REG_CMD_SEC_CNT   ,primary, &sc);
	read_cmd_reg_8		( REG_CMD_SEC_ADDR  ,primary, &sn);

	if ( ( sc == 0x55 ) && ( sn == 0xaa ) )
		  controllers[primary]->reg_config_info[dev] = REG_CONFIG_TYPE_UNKN;
}

void ata_detect_device_type(unsigned int primary,unsigned int dev)
{
    unsigned char   sc;
    unsigned char   sn;
	unsigned char   cl;
	unsigned char   ch;
	unsigned char   st;

	ata_select_device	(primary,dev);
	read_cmd_reg_8		( REG_CMD_SEC_CNT		,primary, &sc);
	read_cmd_reg_8		( REG_CMD_SEC_ADDR		,primary, &sn);
	read_cmd_reg_8		( REG_CMD_STATUS_CMD	,primary, &st);

	if ( st == 0x7f )
	{
		controllers[primary]->reg_config_info[dev] = REG_CONFIG_TYPE_NONE;
	}
	else if ( ( sc == 0x01 ) && ( sn == 0x01 ) )
	{
		 // yep, there is a device, what kind is it?
		read_cmd_reg_8	( REG_CMD_STATUS_CMD	,primary, &st);
		read_cmd_reg_8	( REG_CMD_CYL_LOW		,primary, &cl);
		read_cmd_reg_8	( REG_CMD_CYL_HI		,primary, &ch);
		   
		if ( ( ( cl == 0x14 ) && ( ch == 0xeb ) )       // PATAPI
		   ||
		   ( ( cl == 0x69 ) && ( ch == 0x96 ) )       // SATAPI
		)
		{
			controllers[primary]->reg_config_info[dev] = REG_CONFIG_TYPE_ATAPI;
		}
		else if  ( ( cl == 0x00 ) && ( ch == 0x00 ) )     // PATA

		{
			controllers[primary]->reg_config_info[dev] = REG_CONFIG_TYPE_ATA;
		}
		else if ( ( cl == 0x3c ) && ( ch == 0xc3 ) )     // SATA
		{
			controllers[primary]->reg_config_info[dev] = REG_CONFIG_TYPE_SATA;
		}
	}
}




//void pio_drq_block_out( unsigned int addrDataReg,unsigned int bufSeg, unsigned int bufOff,long wordCnt )
void pio_drq_block_out( unsigned int primary,mem_ptr bufAddr,long wordCnt,char pio_xfer_width )
{
   long bCnt;
   unsigned short * uip1;
   unsigned short * uip2;
   unsigned char * ucp1;
   unsigned char * ucp2;

   // NOTE: wordCnt is the size of a DRQ block
   // in words. The maximum value of wordCnt is normally:
   // a) For ATA, 16384 words or 32768 bytes (64 sectors,
   //    only with READ/WRITE MULTIPLE commands),
   // b) For ATAPI, 32768 words or 65536 bytes
   //    (actually 65535 bytes plus a pad byte).

    if ( !controllers[primary]->is_cmd_io)
   {
	   //kernel_log(kernel_log_id,"mmio out txfr \n");

      if ( pio_xfer_width == 8 )
      {
         // PCMCIA Memory mode 8-bit
         bCnt   = wordCnt * 2L;
         ucp1   = bufAddr;//(unsigned char far *) MK_FP( pio_memory_seg, dataRegAddr );
		 ucp2	= uint_to_mem(controllers[primary]->cmd_base+REG_CMD_DATA);
         for ( ; bCnt > 0; bCnt -- )
         {
            * ucp2	= * ucp1;
            ucp1	=	mem_add(ucp1,sizeof(unsigned char));

         }

      }
      else
      {
         // PCMCIA Memory mode 16-bit
         uip1 = bufAddr;
		 uip2 = uint_to_mem(controllers[primary]->cmd_base+REG_CMD_DATA);
         for ( ; wordCnt > 0; wordCnt -- )
         {
           
            * uip2	= * uip1;
            uip1	=	mem_add(uip1,sizeof(unsigned short));
         }
      }
   }
	else
   {
      int pxw;
      long wc;

	   //kernel_log(kernel_log_id,"io out txfr \n");

      // adjust pio_xfer_width - don't use DWORD if wordCnt is odd.

      pxw = pio_xfer_width;
      if ( ( pxw == 32 ) && ( wordCnt & 0x00000001L ) )
         pxw = 16;

      // Data transfer using OUTS instruction.
      // Break the transfer into chunks of 32768 or fewer bytes.

      while ( wordCnt > 0 )
      {
         if ( wordCnt > 16384L )
            wc = 16384;
         else
            wc = wordCnt;
         if ( pxw == 8 )
         {
            // do REP OUTS
            rep_out_byte( controllers[primary]->cmd_base+REG_CMD_DATA,bufAddr, wc * 2L );
         }
         else
         if ( pxw == 32 )
         {
            // do REP OUTSD
            rep_out_dword( controllers[primary]->cmd_base+REG_CMD_DATA, bufAddr, wc / 2L );
         }
         else
         {
            // do REP OUTSW
            rep_out_word( controllers[primary]->cmd_base+REG_CMD_DATA, bufAddr, wc );
         }
         bufAddr = mem_add(bufAddr , ( wc * 2 ));
         wordCnt = wordCnt - wc;
      }
   }

   return;
}


void pio_drq_block_in( unsigned int primary,mem_ptr bufAddr,long wordCnt,char pio_xfer_width )

{
   long bCnt;
   unsigned short  * uip1;
   unsigned short  * uip2;
   unsigned char * ucp1;
   unsigned char * ucp2;
   

   // NOTE: wordCnt is the size of a DRQ block
   // in words. The maximum value of wordCnt is normally:
   // a) For ATA, 16384 words or 32768 bytes (64 sectors,
   //    only with READ/WRITE MULTIPLE commands),
   // b) For ATAPI, 32768 words or 65536 bytes
   //    (actually 65535 bytes plus a pad byte).

   // normalize bufSeg:bufOff

   if ( !controllers[primary]->is_cmd_io)
   {
	   //kernel_log(kernel_log_id,"mmio in txfr \n");

      if ( pio_xfer_width == 8 )
      {
         // PCMCIA Memory mode 8-bit
         bCnt   = wordCnt * 2L;
         ucp1   = uint_to_mem(controllers[primary]->cmd_base+REG_CMD_DATA);//(unsigned char far *) MK_FP( pio_memory_seg, dataRegAddr );
		 ucp2	= bufAddr;
         for ( ; bCnt > 0; bCnt -- )
         {
            * ucp2	= * ucp1;
            ucp2	=	mem_add(ucp2,sizeof(unsigned char));

         }

      }
      else
      {
         // PCMCIA Memory mode 16-bit
         uip1 = uint_to_mem(controllers[primary]->cmd_base+REG_CMD_DATA);
		 uip2 = bufAddr;
         for ( ; wordCnt > 0; wordCnt -- )
         {
           
            * uip2	= * uip1;
            uip2	=	mem_add(uip2,sizeof(unsigned short));
         }
      }
   }
   else
   {
      int pxw;
      long wc;

	  //kernel_log(kernel_log_id,"io in txfr \n");

      // adjust pio_xfer_width - don't use DWORD if wordCnt is odd.

      pxw = pio_xfer_width;
      if ( ( pxw == 32 ) && ( wordCnt & 0x00000001L ) )
         pxw = 16;

      // Data transfer using INS instruction.
      // Break the transfer into chunks of 32768 or fewer bytes.

      while ( wordCnt > 0 )
      {
         if ( wordCnt > 16384L )
            wc = 16384;
         else
            wc = wordCnt;
         if ( pxw == 8 )
         {
            // do REP INS
            rep_in_byte( controllers[primary]->cmd_base+REG_CMD_DATA, bufAddr, wc * 2L );
         }
         else
         if ( pxw == 32 )
         {
            // do REP INSD
            rep_in_dword( controllers[primary]->cmd_base+REG_CMD_DATA, bufAddr, wc / 2L );
         }
         else
         {
            // do REP INSW
            rep_in_word( controllers[primary]->cmd_base+REG_CMD_DATA, bufAddr  , wc );
         }
         bufAddr = mem_add(bufAddr , ( wc * 2 ));
         wordCnt = wordCnt - wc;
      }
   }

   return;
}



unsigned int exec_pio_data_in_cmd(int primary, int dev,int cmd,unsigned char fr,lba_chs lba,unsigned int lba_size,mem_ptr buffer,mem_size reg_buffer_size,unsigned short numSect, int multiCnt,unsigned int use_intr  )

{
   unsigned char	status;
   unsigned char	bm_error;
   unsigned int		time_start,time_cur;
   unsigned int		infos_ret;
   long				drqPackets;
   mem_size			totalBytesXfer,wordCnt;
   mem_ptr			saveBuffer=buffer;


    infos_ret=0;

	// mark start of PDI cmd in low level trace
	write_bm_reg_8			(REG_BM_STATUS	,primary,BM_STATUS_ERR);

	// Set command time out.
	time_start=system_time();
	time_cur=system_time();

   // Select the drive - call the sub_select function.
   // Quit now if this fails.

   if ( sub_select(primary, dev ) == 0)
   {

	   kernel_log			(kernel_log_id,"ata sub select failed\n ");
	   return 0;
   }

   // Set up all the registers except the command register.

   if(lba_size==LBACHS)
	   sub_setup_command_chs(primary,dev,fr,numSect,lba.chs.cyl,lba.chs.head,lba.chs.sect,use_intr);
   else
	   sub_setup_command_lba(primary,dev,fr,numSect,lba.lba.lba,use_intr,lba_size);
	   

   // For interrupt mode, install interrupt handler.

//   int_save_int_vect();

   // Start the command by setting the Command register.  The drive
   // should immediately set BUSY status.

   write_cmd_reg_8( REG_CMD_STATUS_CMD,primary, cmd );

   // Waste some time by reading the alternate status a few times.
   // This gives the drive time to set BUSY in the status register on
   // really fast systems.  If we don't do this, a slow drive on a fast
   // system may not set BUSY fast enough and we would think it had
   // completed the command when it really had not even started the
   // command yet.

   ATA_DELAY(primary);
   drqPackets=0;
totalBytesXfer=0;
   // Loop to read each sector.

   while ( 1 )
   {
	  
      // READ_LOOP:
      //
      // NOTE NOTE NOTE ...  The primary status register (1f7) MUST NOT be
      // read more than ONCE for each sector transferred!  When the
      // primary status register is read, the drive resets IRQ 14.  The
      // alternate status register (3f6) can be read any number of times.
      // After interrupt read the the primary status register ONCE
      // and transfer the 256 words (REP INSW).  AS SOON as BOTH the
      // primary status register has been read AND the last of the 256
      // words has been read, the drive is allowed to generate the next
      // IRQ 14 (newer and faster drives could generate the next IRQ 14 in
      // 50 microseconds or less).  If the primary status register is read
      // more than once, there is the possibility of a race between the
      // drive and the software and the next IRQ 14 could be reset before
      // the system interrupt controller sees it.

      // Wait for interrupt -or- wait for not BUSY -or- wait for time out.

      //ATAPI_DELAY( dev );
      infos_ret=reg_wait_poll(primary, 34, 35 , use_intr);

      // If polling or error read the status, otherwise
      // get the status that was read by the interrupt handler.

      if ( ( ! use_intr ) || ( infos_ret ) )
         read_cmd_reg_8(REG_CMD_STATUS_CMD ,primary,&status);
      else
         status = int_ata_status;

      // If there was a time out error, go to READ_DONE.

      if ( infos_ret )

         break;   // go to READ_DONE

      // If BSY=0 and DRQ=1, transfer the data,
      // even if we find out there is an error later.

      if ( ( status & ( REG_STATUS_BSY | REG_STATUS_DRQ  ) ) == REG_STATUS_DRQ )
      {
         // do the slow data transfer thing
/*
         if ( reg_slow_xfer_flag )
         {
            if ( numSect <= reg_slow_xfer_flag )
            {
               tmr_delay_xfer();
               reg_slow_xfer_flag = 0;
            }
         }
*/
         // increment number of DRQ packets

         drqPackets ++ ;

         // determine the number of sectors to transfer

         wordCnt = multiCnt ? multiCnt : 1;
         if ( wordCnt > numSect )
            wordCnt = numSect;
         wordCnt = wordCnt * 256;

         // Quit if buffer overrun.
         // Adjust buffer address when DRQ block call back in use.

		 /*
         if ( reg_drq_block_call_back )
         {
            if ( ( wordCnt << 1 ) > reg_buffer_size )
            {
              infos_ret=1;
               break;   // go to READ_DONE
            }
			buffer=saveBuffer;
         }
         else
         {
		 */
         if ( ( totalBytesXfer + ( wordCnt << 1 ) )> reg_buffer_size )
         {
             infos_ret=1;
			 
			 kernel_log			(kernel_log_id,"buffer too small ");
			 writesz			(totalBytesXfer,10);
			 writestr			(" ");
			 writesz			(wordCnt,10);
			 writestr			(" ");
			 writesz			(reg_buffer_size,10);
			 writestr			("\n");
             break;   // go to READ_DONE
         }
         //}

         // Do the REP INSW to read the data for one DRQ block.

         totalBytesXfer += ( wordCnt << 1 );
         pio_drq_block_in	( primary, buffer, wordCnt,16 );

         ATA_DELAY		(primary);   // delay so device can get the status updated

         // Note: The drive should have dropped DATA REQUEST by now.  If there
         // are more sectors to transfer, BUSY should be active now (unless
         // there is an error).

         // Call DRQ block call back function.
		 /*
         if ( reg_drq_block_call_back )
         {
            reg_cmd_info.drqPacketSize = ( wordCnt << 1 );
            (* reg_drq_block_call_back) ( & reg_cmd_info );
         }
		 */

         // Decrement the count of sectors to be transferred
         // and increment buffer address.

         numSect = numSect - ( multiCnt ? multiCnt : 1 );
         //seg	 = seg + ( 32 * ( multiCnt ? multiCnt : 1 ) );
		 buffer	 = 	mem_add(buffer,( 256 * (multiCnt ? multiCnt : 1 )));
      }
	  else
	  {
		kernel_log			(kernel_log_id,"ata error cond #1 ");
		writeint			(status,10);
		writestr			(" ");
		
		if(status &  REG_STATUS_BSY)
			 writestr			(" bsy ");
		 if(!(status &  REG_STATUS_DRQ))
			 writestr			(" no drq ");
		 

		 writeint			(use_intr,10);
		 writestr			("\n");
	  }

      // So was there any error condition?

      if ( status & ( REG_STATUS_BSY | REG_STATUS_DF | REG_STATUS_ERR ) )
      {
			 kernel_log			(kernel_log_id,"ata error cond #2 ");
			 writeint			(status,10);
			 if(status &  REG_STATUS_BSY)
				 writestr			(" bsy ");
			 if(status &  REG_STATUS_DF)
				 writestr			(" DEVICE FAULT ");
			 if(status &  REG_STATUS_ERR)
				 writestr			(" error ");

			 writestr			(" ");
			 writeint			(use_intr,10);
			 writestr			("\n");

		 infos_ret=1;
         break;   // go to READ_DONE
      }

      // DRQ should have been set -- was it?

      if ( ( status & REG_STATUS_DRQ ) == 0 )
      {
			 kernel_log			(kernel_log_id,"no drq ");
			 writeint			(status,10);
			 writestr			("\n");

         infos_ret=1;
         break;   // go to READ_DONE
      }

      // If all of the requested sectors have been transferred, make a
      // few more checks before we exit.

      if ( numSect < 1 )
      {
         // Since the drive has transferred all of the requested sectors
         // without error, the drive should not have BUSY, DEVICE FAULT,
         // DATA REQUEST or ERROR active now.

         //ATAPI_DELAY( dev );
         //status = pio_inbyte( CB_STAT );
		 read_cmd_reg_8(REG_CMD_STATUS_CMD ,primary,&status);
         if ( status & ( REG_STATUS_BSY | REG_STATUS_DF | REG_STATUS_ERR | REG_STATUS_DRQ ) )
         {

			 kernel_log			(kernel_log_id,"busy or df or err or drq set ");
			 writeint			(status,10);
			 writestr			("\n");

            infos_ret=1;
            break;   // go to READ_DONE
         }
         // All sectors have been read without error, go to READ_DONE.
         break;   // go to READ_DONE
      }

      // This is the end of the read loop.  If we get here, the loop is
      // repeated to read the next sector.  Go back to READ_LOOP.

   }

   // read the output registers and trace the command.

   //sub_trace_command();

   // BMIDE Error=1?


   
   read_bm_reg_8(REG_BM_STATUS,primary,&bm_error);

   if ( bm_error & BM_STATUS_ERR  )
   {
	  kernel_log		(kernel_log_id,"bm error ");
	  writeint			(bm_error,10);
	  writestr			("\n");
      infos_ret=1;
   }

   // READ_DONE:

   // For interrupt mode, remove interrupt handler.
   //int_restore_int_vect();


   // All done.  The return values of this function are described in
   // ATAIO.H.

   if ( infos_ret )return 0;
   return 1;
}



int reg_pio_data_in_chs( unsigned int primary,unsigned int dev, int cmd,
                         unsigned int fr, 
                         unsigned int cyl, unsigned int head, unsigned int sect,
                         mem_ptr	buffer,mem_size buffer_size,
                         unsigned short numSect, int multiCnt )

{

   // Reset error return data.
	lba_chs	sec_start;

   if (    ( cmd == CMD_IDENTIFY_DEVICE )
        || ( cmd == CMD_IDENTIFY_DEVICE_PACKET )
      )
      numSect = 1;
   // adjust multiple count
   if ( multiCnt & 0x0800 )
   {
      // assume caller knows what they are doing
      multiCnt &= 0x00ff;
   }
   else
   {
      // only multiple commands use multiCnt
      if (    ( cmd != CMD_READ_MULTIPLE )
           && ( cmd != CMD_READ_MULTIPLE_EXT )
         )
         multiCnt = 1;
   }
   sec_start.chs.cyl=cyl;
   sec_start.chs.head=head;
   sec_start.chs.sect=sect;
   
   return exec_pio_data_in_cmd(primary, dev,cmd,fr,sec_start,LBACHS,buffer,buffer_size, numSect, multiCnt,0 );
}




//***********************************************************
//
// dma_pci_chs() - PCI Bus Master for ATA R/W DMA commands
//
//***********************************************************

int dma_pci_chs( unsigned int primary,int dev, int cmd,
                 unsigned int fr, unsigned int sc,
                 unsigned int cyl, unsigned int head, unsigned int sect,
                 mem_ptr addr,
                  unsigned short  numSect )

{
 // Reset error return data.
   lba_chs	sec_start;

   sec_start.chs.cyl=cyl;
   sec_start.chs.head=head;
   sec_start.chs.sect=sect;
   return exec_pci_ata_cmd( primary,dev, fr,cmd,addr,sec_start,LBACHS,numSect );
}

//*************************************************************
//
// reg_pio_data_in_lba28() - Easy way to execute a PIO Data In command
//                           using an LBA sector address.
//
//*************************************************************

int reg_pio_data_in_lba28( unsigned int primary,unsigned int dev, int cmd,
                           unsigned int fr, 
                           unsigned long lba,
                           mem_ptr buffer,mem_size buffer_size,
                           unsigned short numSect, int multiCnt )

{

	lba_chs	sec_start;

   if (    ( cmd == CMD_IDENTIFY_DEVICE )
        || ( cmd == CMD_IDENTIFY_DEVICE_PACKET )
      )
      numSect = 1;
   // adjust multiple count
   if ( multiCnt & 0x0800 )
   {
      // assume caller knows what they are doing
      multiCnt &= 0x00ff;
   }
   else
   {
      // only multiple commands use multiCnt
      if (    ( cmd != CMD_READ_MULTIPLE )
           && ( cmd != CMD_READ_MULTIPLE_EXT )
         )
         multiCnt = 1;
   }
   sec_start.lba.lba	= lba;

   return exec_pio_data_in_cmd(primary, dev,cmd,fr,sec_start,LBA28,buffer,buffer_size, numSect, multiCnt,0 );

   // Reset error return data.

   
}



//***********************************************************
//
// dma_pci_lba28() - DMA in PCI Multiword for ATA R/W DMA
//
//***********************************************************

int dma_pci_lba28(unsigned int primary, int dev, int cmd,
                   unsigned int fr, unsigned int sc,
                   unsigned long lba,
                   mem_ptr mem_addr,
                    unsigned short  numSect )

{
	lba_chs	sec_start;

   sec_start.lba.lba	= lba;
   return exec_pci_ata_cmd( primary,dev, fr,cmd,mem_addr,sec_start,LBA28,numSect );
}


//*************************************************************
//
// reg_pio_data_in_lba48() - Easy way to execute a PIO Data In command
//                           using an LBA sector address.
//
//*************************************************************

int reg_pio_data_in_lba48( unsigned int primary,unsigned int dev, int cmd,
                           unsigned int fr, 
                           unsigned long lbahi, unsigned long lbalo,
                           mem_ptr buffer,mem_size buffer_size,
                           unsigned short numSect, int multiCnt )

{

	lba_chs	sec_start;
   if (    ( cmd == CMD_IDENTIFY_DEVICE )
        || ( cmd == CMD_IDENTIFY_DEVICE_PACKET )
      )
      numSect = 1;

   // adjust multiple count
   if ( multiCnt & 0x0800 )
   {
      // assume caller knows what they are doing
      multiCnt &= 0x00ff;
   }
   else
   {
      // only multiple commands use multiCnt
      if (    ( cmd != CMD_READ_MULTIPLE )
           && ( cmd != CMD_READ_MULTIPLE_EXT )
         )
         multiCnt = 1;
   }
   sec_start.lba.lba	= lbalo;

   return exec_pio_data_in_cmd(primary, dev,cmd,fr,sec_start,LBA48,buffer,buffer_size, numSect, multiCnt,0 );
}



//***********************************************************
//
// dma_pci_lba48() - DMA in PCI Multiword for ATA R/W DMA
//
//***********************************************************

int dma_pci_lba48( unsigned int primary,int dev, int cmd,
                   unsigned int fr, unsigned int sc,
                   unsigned long lbahi, unsigned long lbalo,
                   mem_ptr addr,
                    unsigned short  numSect )

{
	lba_chs	sec_start;
	sec_start.lba.lba	= lbalo;
	return exec_pci_ata_cmd(primary,dev,fr,cmd,addr,sec_start,LBA48,numSect);
   
	/*

	// Reset error return data.
   // Setup current command information.

   sub_zero_return_data();
   reg_cmd_info.flg = TRC_FLAG_ATA;
   reg_cmd_info.ct  = TRC_TYPE_ADMAI;
   if (    ( cmd == CMD_WRITE_DMA )
        || ( cmd == CMD_WRITE_DMA_EXT )
        || ( cmd == CMD_WRITE_DMA_FUA_EXT )
      )
      reg_cmd_info.ct  = TRC_TYPE_ADMAO;
   reg_cmd_info.cmd = cmd;
   reg_cmd_info.fr1 = fr;
   reg_cmd_info.sc1 = sc;
   reg_cmd_info.dh1 = CB_DH_LBA | (dev ? CB_DH_DEV1 : CB_DH_DEV0 );
   reg_cmd_info.dc1 = 0x00;      // nIEN=0 required on PCI !
   reg_cmd_info.ns  = numSect;
   reg_cmd_info.lbaSize = LBA48;
   reg_cmd_info.lbaHigh1 = lbahi;
   reg_cmd_info.lbaLow1 = lbalo;

   // Execute the command.

   return exec_pci_ata_cmd( dev, seg, off, numSect );
   */
}
//*************************************************************
//
// reg_packet() - Execute an ATAPI Packet (A0H) command.
//
// See ATA-4 Section 9.10, Figure 14.
//
//*************************************************************

int reg_packet( unsigned int primary,unsigned int dev,
                unsigned long cmd_size,
                mem_ptr		cmd_buffer,
                int dir,
                unsigned long data_size,
                mem_ptr	data_buffer,
                unsigned int use_intr)

{
   unsigned char status;
   unsigned char infos_ret;
   unsigned char reason;
   unsigned char lowCyl;
   unsigned char highCyl;
   unsigned char bm_error;
   unsigned int byteCnt;
   unsigned int totalBytesXfer,drqPackets;
   long wordCnt;
   mem_ptr savedpaddr;
   int slowXferCntr = 0;
   int prevFailBit7 = 0;

   // mark start of PI cmd in low level trace


   // reset Bus Master Error bit
   infos_ret=0;

  write_bm_reg_8		(REG_BM_STATUS		,primary,BM_STATUS_ERR);

   // Make sure the command packet size is either 12 or 16
   // and save the command packet size and data.

   cmd_size = cmd_size < 12 ? 12 : cmd_size;
   cmd_size = cmd_size > 12 ? 16 : cmd_size;

   // Setup current command information.
/*
   sub_zero_return_data();
   reg_cmd_info.flg = TRC_FLAG_ATAPI;
   reg_cmd_info.ct  = dir ? TRC_TYPE_PPDO : TRC_TYPE_PPDI;
   reg_cmd_info.cmd = CMD_PACKET;
   reg_cmd_info.fr1 = reg_atapi_reg_fr;
   reg_cmd_info.sc1 = reg_atapi_reg_sc;
   reg_cmd_info.sn1 = reg_atapi_reg_sn;
   reg_cmd_info.cl1 = (unsigned char) ( dpbc & 0x00ff );
   reg_cmd_info.ch1 = ( unsigned char) ( ( dpbc & 0xff00 ) >> 8 );
   reg_cmd_info.dh1 = ( dev ? CB_DH_DEV1 : CB_DH_DEV0 )
                      | ( reg_atapi_reg_dh & 0x0f );
   reg_cmd_info.dc1 = int_use_intr_flag ? 0 : CB_DC_NIEN;
   reg_cmd_info.lbaSize = LBA32;
   reg_cmd_info.lbaLow1 = lba;
   reg_cmd_info.lbaHigh1 = 0L;
   reg_atapi_cp_size = cpbc;
   cfp = MK_FP( cpseg, cpoff );
   for ( ndx = 0; ndx < cpbc; ndx ++ )
   {
      reg_atapi_cp_data[ndx] = * cfp;
      cfp ++ ;
   }
	
   // Zero the alternate ATAPI register data.

   reg_atapi_reg_fr = 0;
   reg_atapi_reg_sc = 0;
   reg_atapi_reg_sn = 0;
   reg_atapi_reg_dh = 0;

   // Set command time out.

   tmr_set_timeout();

   // Select the drive - call the sub_select function.
   // Quit now if this fails.
   */

   if ( sub_select( primary,dev ) ==0)
   {
      kernel_log			(kernel_log_id,"ata sub select atapi failed\n ");
      return 0;
   }

   // Set up all the registers except the command register.

  

   sub_setup_command_chs(primary,dev,0,0,data_size,0,0,use_intr);
   // For interrupt mode, install interrupt handler.

   //int_save_int_vect();

   // Start the command by setting the Command register.  The drive
   // should immediately set BUSY status.
	write_cmd_reg_8( REG_CMD_STATUS_CMD,primary, CMD_PACKET );
   

   // Waste some time by reading the alternate status a few times.
   // This gives the drive time to set BUSY in the status register on
   // really fast systems.  If we don't do this, a slow drive on a fast
   // system may not set BUSY fast enough and we would think it had
   // completed the command when it really had not even started the
   // command yet.

   ATA_DELAY(primary);
   //ATAPI_DELAY( dev );

   // Command packet transfer...
   // Check for protocol failures,
   // the device should have BSY=1 or
   // if BSY=0 then either DRQ=1 or CHK=1.

    

  
	read_stat_reg_8(REG_STAT_ASTAT_DC,primary,&status);
    if(!(status & REG_STATUS_BSY ) )
    {
      if (( status & ( REG_STATUS_DRQ | REG_STATUS_ERR ) )==0)
      {
		  infos_ret=1;

		    kernel_log(kernel_log_id,"bad status #0 ");
			writeint(status,16);
			writestr("\n");
      }
	}

   // Command packet transfer...
   // Poll Alternate Status for BSY=0.

	/*
   while ( 1 )
   {
	   read_status_reg_8(REG_STAT_ASTAT_DC,primary,&status);
      
	  status = pio_inbyte( CB_ASTAT );       // poll for not busy
      if ( ( status & CB_STAT_BSY ) == 0 )
         break;
      if ( tmr_chk_timeout() )               // time out yet ?
      {
         trc_llt( 0, 0, TRC_LLT_TOUT );      // yes
         reg_cmd_info.to = 1;
         reg_cmd_info.ec = 51;
         trc_llt( 0, reg_cmd_info.ec, TRC_LLT_ERROR );
         dir = -1;   // command done
         break;
      }
   }
   */

	infos_ret=reg_wait_poll(primary,1,1,use_intr);

	if(infos_ret)
	{
	    kernel_log(kernel_log_id,"bad reg poll #0 ");
		writeint(status,16);
		writestr("\n");
		dir = -1;
	}

   // Command packet transfer...
   // Check for protocol failures... no interrupt here please!
   // Clear any interrupt the command packet transfer may have caused.
/*
   if ( int_intr_flag )       // extra interrupt(s) ?
      reg_cmd_info.failbits |= FAILBIT1;
*/

   int_intr_flag = 0;

   // Command packet transfer...
   // If no error, transfer the command packet.

   if ( infos_ret == 0 )
   {

      // Command packet transfer...
      // Read the primary status register and the other ATAPI registers.
/*
      status = pio_inbyte( CB_STAT );
      reason = pio_inbyte( CB_SC );
      lowCyl = pio_inbyte( CB_CL );
      highCyl = pio_inbyte( CB_CH )
*/		  
	  read_cmd_reg_8	( REG_CMD_STATUS_CMD	,primary, &status);
	  read_cmd_reg_8	( REG_CMD_SEC_CNT		,primary, &reason);
	  read_cmd_reg_8	( REG_CMD_CYL_LOW		,primary, &lowCyl);
	  read_cmd_reg_8	( REG_CMD_CYL_HI		,primary, &highCyl);

      // Command packet transfer...
      // check status: must have BSY=0, DRQ=1 now

      if (    ( status & ( REG_STATUS_BSY | REG_STATUS_DRQ | REG_STATUS_ERR ) )!= REG_STATUS_DRQ)
      {
		  kernel_log(kernel_log_id,"bad status #10 ");
			writeint(status,16);
			writestr("\n");

         infos_ret	=	1;
         dir		= -1;   // command done
      }
      else
      {
         // Command packet transfer...
         // Check for protocol failures...
         // check: C/nD=1, IO=0.


         if ( ( reason &  ( REG_STATUS_TAG | REG_STATUS_REL | REG_STATUS_IO ) )|| ( ! ( reason & REG_STATUS_CD ) ))
		 {
			 kernel_log(kernel_log_id,"bad reason #52");
			writeint(reason,16);
			writestr("\n");

            infos_ret	=	1;
		 }

   
		 if ( ( lowCyl !=  (unsigned char) ( data_size & 0x00ff ) )|| 
			  ( highCyl != (( unsigned char)(( data_size & 0xff00 ) >> 8 )) ) )
		 {
			kernel_log(kernel_log_id,"bad cyl value ");
			writeint(lowCyl,16);
			writestr(" ");
			writeint(highCyl,16);
			writestr(" ");
			writeint(data_size,16);
			writestr("\n");

 		    infos_ret	=	1;
		}

         // Command packet transfer...
         // trace cdb byte 0,
         // xfer the command packet (the cdb)

         
        pio_drq_block_out	( primary,cmd_buffer,cmd_size >> 1,16 );

        ATA_DELAY			(primary);   // delay so device can get the status updated

		if ( status & (REG_STATUS_BSY | REG_STATUS_ERR ) )
		{
			infos_ret=1;

			kernel_log(kernel_log_id,"busy or error after atapi ccommand");
			writeint(status,16);
			writestr("\n");
		}
		if (( status & (REG_STATUS_BSY | REG_STATUS_DRQ ) )!= REG_STATUS_DRQ)
		{
			infos_ret=1;

			kernel_log(kernel_log_id,"no data request after atapi ccommand");
			writeint(status,16);
			writestr("\n");
		}
	  }
   }

   // Data transfer loop...
   // If there is no error, enter the data transfer loop.
   // First adjust the I/O buffer address so we are able to
   // transfer large amounts of data (more than 64K).
/*
   dpaddr = dpseg;
   dpaddr = dpaddr << 4;
   dpaddr = dpaddr + dpoff;
*/
   savedpaddr		=	data_buffer;
   totalBytesXfer	=	0;
   drqPackets		=	0;

   if( infos_ret == 1 )
	kernel_log(kernel_log_id,"infos ret set, no data loop \n");

   while ( infos_ret == 0 )
   {
      // Data transfer loop...
      // Wait for interrupt -or- wait for not BUSY -or- wait for time out.

      //ATAPI_DELAY( dev );
      infos_ret =reg_wait_poll( primary,53, 54 ,use_intr);

      // Data transfer loop...
      // If there was a time out error, exit the data transfer loop.

      if ( infos_ret )
      {
		kernel_log(kernel_log_id,"poll error ");
		writeint(use_intr,16);
		writestr("\n");

         dir = -1;   // command done
         break;
      }

      // Data transfer loop...
      // If using interrupts get the status read by the interrupt
      // handler, otherwise read the status register.


	   if (! use_intr )
			read_cmd_reg_8(REG_CMD_STATUS_CMD ,primary,&status);
	   else
			status = int_ata_status;

		read_cmd_reg_8	( REG_CMD_SEC_CNT		,primary, &reason);
	  read_cmd_reg_8	( REG_CMD_CYL_LOW		,primary, &lowCyl);
	  read_cmd_reg_8	( REG_CMD_CYL_HI		,primary, &highCyl);

      // Data transfer loop...
      // Exit the read data loop if the device indicates this
      // is the end of the command.

 
      if ( ( status & ( REG_STATUS_BSY | REG_STATUS_DRQ ) ) == 0 )
      {
		  /*
		 kernel_log	(kernel_log_id,"no drq and bsy in data loop, transfered ");
		 writeint	(totalBytesXfer,10);
		 writestr	(" bytes \n");
		 */
         dir = -1;   // command done
         break;
      }

      if ( ( status & ( REG_STATUS_BSY | REG_STATUS_DRQ ) ) != REG_STATUS_DRQ )
      {
		  /*
		 kernel_log	(kernel_log_id,"no data request, transfered ");
		 writeint	(totalBytesXfer,10);
		 writestr	("\n");
		 */
         dir = -1;   // command done
         break;
      }
      else
      {
         // Data transfer loop...
         // Check for protocol failures...
         // check: C/nD=0, IO=1 (read) or IO=0 (write).

		  if ( ( reason &  ( REG_STATUS_TAG | REG_STATUS_REL  ) )|| (reason & REG_STATUS_CD ))
		  {
			kernel_log(kernel_log_id,"bad reason #0");
			writeint(reason,16);
			writestr("\n");
		  }

		  if ( ( reason & REG_STATUS_IO ) && dir )
		  {
			kernel_log(kernel_log_id,"bad reason #1");
			writeint(reason,16);
			writestr("\n");
		  }
      }
	  /*
      reason = pio_inbyte( CB_SC );
      lowCyl = pio_inbyte( CB_CL );
      highCyl = pio_inbyte( CB_CH );

      // Data transfer loop...
      // Exit the read data loop if the device indicates this
      // is the end of the command.

      if ( ( status & ( CB_STAT_BSY | CB_STAT_DRQ ) ) == 0 )
      {
         dir = -1;   // command done
         break;
      }

      // Data transfer loop...
      // The device must want to transfer data...
      // check status: must have BSY=0, DRQ=1 now.

      if ( ( status & ( CB_STAT_BSY | CB_STAT_DRQ ) ) != CB_STAT_DRQ )
      {
         reg_cmd_info.ec = 55;
         trc_llt( 0, reg_cmd_info.ec, TRC_LLT_ERROR );
         dir = -1;   // command done
         break;
      }
      else
      {
         // Data transfer loop...
         // Check for protocol failures...
         // check: C/nD=0, IO=1 (read) or IO=0 (write).

         if ( ( reason &  ( CB_SC_P_TAG | CB_SC_P_REL ) )
              || ( reason &  CB_SC_P_CD )
            )
            reg_cmd_info.failbits |= FAILBIT4;
         if ( ( reason & CB_SC_P_IO ) && dir )
            reg_cmd_info.failbits |= FAILBIT5;
      }

      // do the slow data transfer thing

      if ( reg_slow_xfer_flag )
      {
         slowXferCntr ++ ;
         if ( slowXferCntr <= reg_slow_xfer_flag )
         {
            tmr_delay_xfer();
            reg_slow_xfer_flag = 0;
         }
      }
	  */

      // Data transfer loop...
      // get the byte count, check for zero...

      byteCnt = ( highCyl << 8 ) | lowCyl;
      if ( byteCnt < 1 )
      {
		kernel_log(kernel_log_id,"zero byteCnt ");
		writeint(byteCnt,16);
		writestr("\n");
		infos_ret=1;
		break;
      }

      // Data transfer loop...
      // and check protocol failures...

      if ( byteCnt > data_size )
	  {
		 kernel_log(kernel_log_id,"byteCnt too big ");
		 writeint(byteCnt,16);
		 writestr(" ");
		 writeint(data_size,16);
		 writestr("\n");

         infos_ret=1;
	  }
/*
      reg_cmd_info.failbits |= prevFailBit7;
      prevFailBit7 = 0;
      if ( byteCnt & 0x0001 )
         prevFailBit7 = FAILBIT7;
*/
      // Data transfer loop...
      // Quit if buffer overrun.
      // If DRQ call back in use adjust buffer address.
/*
      if ( reg_drq_block_call_back )
      {
         if ( byteCnt > reg_buffer_size )
         {
            reg_cmd_info.ec = 61;
            trc_llt( 0, reg_cmd_info.ec, TRC_LLT_ERROR );
            dir = -1;   // command done
            break;
         }
         reg_cmd_info.drqPacketSize = byteCnt;
         dpaddr = savedpaddr;
      }
      else
      {
	  */
	  if ( ( totalBytesXfer + byteCnt ) > data_size )
      {
		  kernel_log(kernel_log_id,"totalBytesXfer too big ");
		  writeint(totalBytesXfer,16);
		  writestr(" ");
		  writeint(byteCnt,16);
		  writestr(" ");
		  writeint(data_size,16);
		  writestr("\n");

		  infos_ret=1;
		  dir = -1;   // command done
		  break;
      }

      // increment number of DRQ packets

      drqPackets ++ ;

      // Data transfer loop...
      // transfer the data and update the i/o buffer address
      // and the number of bytes transfered.

      wordCnt = ( byteCnt >> 1 ) + ( byteCnt & 0x0001 );
      totalBytesXfer += ( wordCnt << 1 );
      if ( dir )
      {
         pio_drq_block_out( primary,data_buffer, wordCnt,16 );
      }
      else
      {
		  pio_drq_block_in( primary,data_buffer, wordCnt,16 );
      }

	  data_buffer	=	mem_add(data_buffer,byteCnt);
      

      ATA_DELAY(primary);   // delay so device can get the status updated
   }

   // End of command...
   // Wait for interrupt or poll for BSY=0,
   // but don't do this if there was any error or if this
   // was a commmand that did not transfer data.

   if ( ( infos_ret == 0 ) && ( dir >= 0 ) )
   {
      //ATAPI_DELAY( dev );
      infos_ret=reg_wait_poll(primary, 56, 57,use_intr );
   }

   // Final status check, only if no previous error.

   if ( infos_ret == 0 )
   {
      // Final status check...
      // If using interrupts get the status read by the interrupt
      // handler, otherwise read the status register.

   /*   if ( use_intr )
         status = int_ata_status;
      else
         status = pio_inbyte( CB_STAT );
      reason = pio_inbyte( CB_SC );
      lowCyl = pio_inbyte( CB_CL );
      highCyl = pio_inbyte( CB_CH );

      // Final status check...
      // check for any error.

      if ( status & ( CB_STAT_BSY | CB_STAT_DRQ | CB_STAT_ERR ) )
      {
         reg_cmd_info.ec = 58;
         trc_llt( 0, reg_cmd_info.ec, TRC_LLT_ERROR );
      }
*/
	   if (! use_intr )
			read_cmd_reg_8(REG_CMD_STATUS_CMD ,primary,&status);
	   else
			status = int_ata_status;

	  read_cmd_reg_8	( REG_CMD_SEC_CNT		,primary, &reason);
	  read_cmd_reg_8	( REG_CMD_CYL_LOW		,primary, &lowCyl);
	  read_cmd_reg_8	( REG_CMD_CYL_HI		,primary, &highCyl);

	   // Error if BUSY, DEVICE FAULT, DRQ or ERROR status now.

	  if ( status & (REG_STATUS_BSY | REG_STATUS_ERR |REG_STATUS_DRQ) )
	  {
		 infos_ret=1;
	     kernel_log(kernel_log_id,"bad status #6");
	     writeint(status,16);
	     writestr("\n");

	   }

   }


   // Final status check...
   // Check for protocol failures...
   // check: C/nD=1, IO=1.
/*
   if ( ( reason & ( CB_SC_P_TAG | CB_SC_P_REL ) )
        || ( ! ( reason & CB_SC_P_IO ) )
        || ( ! ( reason & CB_SC_P_CD ) )
      )
      reg_cmd_info.failbits |= FAILBIT8;
*/
	if ( ( reason &  ( REG_STATUS_TAG | REG_STATUS_REL  ) ) || (!(  reason & REG_STATUS_IO ))|| (!( reason & REG_STATUS_CD )))
	{
		
	    kernel_log(kernel_log_id,"bad after transfer");
	    writeint(reason,16);
	    writestr("\n");
        infos_ret=1;
		
	}
   // Done...
   // Read the output registers and trace the command.
/*
   if ( ! totalBytesXfer )
      reg_cmd_info.ct = TRC_TYPE_PND;
*/
   read_bm_reg_8(REG_BM_STATUS,primary,&bm_error);
   if ( bm_error & BM_STATUS_ERR )
   {
	     kernel_log(kernel_log_id,"bm error ");
	     writeint(bm_error,16);
	     writestr("\n");
      infos_ret=1;
   }


   /*int_restore_int_vect();*/

   if ( infos_ret )
      return 0;
   return 1;
}

//***********************************************************
//
// dma_pci_packet() - PCI Bus Master for ATAPI Packet command
//
//***********************************************************

int dma_pci_packet( unsigned int primary,int dev,
                    unsigned int cpbc,
                    mem_ptr	cp_addr,
                    int dir,
                    long dpbc,
                    mem_ptr	dp_addr)

{
   unsigned char status;
   unsigned char reason;
   unsigned char lowCyl;
   unsigned char highCyl;

   // Make sure the command packet size is either 12 or 16
   // and save the command packet size and data.
   cpbc = cpbc < 12 ? 12 : cpbc;
   cpbc = cpbc > 12 ? 16 : cpbc;
   // the data packet byte count must be even
   // and must not be zero

   if ( dpbc & 1L )
      dpbc ++ ;
   if ( dpbc < 2L )
      dpbc = 2L;

    // Set up the dma transfer

   if ( set_up_xfer(primary,dir,dpbc, dp_addr) )
   {
	  kernel_log(kernel_log_id,"setup xfer over run \n");
      return 1;
   }
   if ( sub_select( primary,dev ) ==0)
   {
	   kernel_log(kernel_log_id,"sub select pb\n");
      return 1;
   }

   // Set up all the registers except the command register.
   sub_setup_command_chs(primary,dev,1,0,0,0,0,1);

   // Start the command by setting the Command register.  The drive
   // should immediately set BUSY status.

   write_cmd_reg_8( REG_CMD_STATUS_CMD,primary, CMD_PACKET );

   // Waste some time by reading the alternate status a few times.
   // This gives the drive time to set BUSY in the status register on
   // really fast systems.  If we don't do this, a slow drive on a fast
   // system may not set BUSY fast enough and we would think it had
   // completed the command when it really had not started the
   // command yet.

   ATA_DELAY(primary);

   // Command packet transfer...
   // Check for protocol failures,
   // the device should have BSY=1 or
   // if BSY=0 then either DRQ=1 or CHK=1.

   //ATAPI_DELAY( dev );
   read_stat_reg_8(REG_STAT_ASTAT_DC,primary,&status);
   if ( status & REG_STATUS_BSY )
   {
      // BSY=1 is OK
   }
   else
   {
      if ( status & ( REG_STATUS_DRQ | REG_STATUS_ERR ) )
      {
         // BSY=0 and DRQ=1 is OK
         // BSY=0 and ERR=1 is OK
      }
      else
      {
		  kernel_log(kernel_log_id,"wrong status drq/err \n");
         return 0;
      }
   }

   // Command packet transfer...
   // Poll Alternate Status for BSY=0.

   

   while ( 1 )
   {
	  read_stat_reg_8(REG_STAT_ASTAT_DC,primary,&status);
      //status = pio_inbyte( CB_ASTAT );       // poll for not busy
      if ( ( status & REG_STATUS_BSY ) == 0 )
         break;
   }
	
   
  // Command packet transfer...
  // Read the primary status register and the other ATAPI registers.

  read_cmd_reg_8	( REG_CMD_STATUS_CMD	,primary, &status);
  read_cmd_reg_8	( REG_CMD_SEC_CNT		,primary, &reason);
  read_cmd_reg_8	( REG_CMD_CYL_LOW		,primary, &lowCyl);
  read_cmd_reg_8	( REG_CMD_CYL_HI		,primary, &highCyl);

  // Command packet transfer...
  // check status: must have BSY=0, DRQ=1 now
  if ( ( status & ( REG_STATUS_BSY | REG_STATUS_DRQ | REG_STATUS_ERR ) )!= REG_STATUS_DRQ )
  {
	 kernel_log(kernel_log_id,"wrong status 2 \n");
     return 0;
  }

  
    // Command packet transfer...
    // Check for protocol failures...
    // check: C/nD=1, IO=0.
    if ( ( reason &  ( REG_STATUS_TAG | REG_STATUS_REL | REG_STATUS_IO ) )|| ( ! ( reason &  REG_STATUS_CD ) ))
		kernel_log(kernel_log_id,"fail #1 \n");

    // Command packet transfer...
    // xfer the command packet (the cdb)

    int_intr_flag		= 0; 
    pio_drq_block_out	(primary,cp_addr, cpbc >> 1,16 );
  
  	// Data transfer...
	// The drive should start executing the command
	// including any data transfer.
	// If no error, set up and start the DMA,
	// and wait for the DMA to complete.


	// Data transfer...
	// read the BMIDE regs
	// enable/start the dma channel.
	// read the BMIDE regs again

	sub_readBusMstrCmd(primary);
	sub_readBusMstrStatus(primary);
	sub_writeBusMstrCmd(primary,controllers[primary]->rwControl | BM_CMD_START );
	sub_readBusMstrCmd(primary);
	sub_readBusMstrStatus(primary);

  	// Data transfer...
  	// the device and dma channel transfer the data here while we start
  	// checking for command completion...
  	// wait for the PCI BM Active=0 and Interrupt=1 or PCI BM Error=1...

  

	while ( !int_intr_flag  )
	{
	  unsigned char t;
	  read_stat_reg_8(REG_STAT_ASTAT_DC,primary,&t);
	}

	snooze(1000);
	/*
	// End of command...
	// disable/stop the dma channel

	status = int_bm_status;                // read BM status
	status &= ~ BM_STATUS_ACT;            // ignore Active bit
	sub_writeBusMstrCmd(primary, BM_CMD_STOP );    // shutdown DMA
	sub_readBusMstrCmd(primary);                      // read BM cmd (just for trace)
	status |= sub_readBusMstrStatus(primary);         // read BM statu again

	if ( status & ( REG_STATUS_ERR ) )        // bus master error?
	{
	  kernel_log(kernel_log_id,"error atapi #3\n");
	  return 0;
	}
	if ( ( status & BM_STATUS_ACT ) )        // end of PRD list?
	{
		kernel_log(kernel_log_id,"error atapi #4\n");
		return 0;
	}
	// End of command...
	// If no error use the Status register value that was read
	// by the interrupt handler. If there was an error
	// read the Status register because it may not have been
	// read by the interrupt handler.

	status = int_ata_status;

	// Final status check...
	// if no error, check final status...
	// Error if BUSY, DRQ or ERROR status now.

	if ( status & ( REG_STATUS_BSY | REG_STATUS_DRQ | REG_STATUS_ERR ) )
	{
		kernel_log(kernel_log_id,"error atapi #5 ");
		writeint(status,16);
		writestr("\n");
		return 0;
	}

	// Final status check...
	// Check for protocol failures...
	// check: C/nD=1, IO=1.

	// reason = pio_inbyte( CB_SC );
	
	read_cmd_reg_8	( REG_CMD_SEC_CNT		,primary, &reason);
	
	
	if ( ( reason & ( REG_STATUS_TAG | REG_STATUS_REL ) )|| ( ! ( reason & REG_STATUS_IO ) )|| ( ! ( reason & REG_STATUS_CD ) ))
    {
		kernel_log(kernel_log_id,"error atapi #6\n");
		return 0;
    }
	*/
	return 1;
}




OS_API_C_FUNC(int) probe_device	(pci_device *pci_dev_ptr)
{
	unsigned int	controller;
	unsigned int	use_intr;
	unsigned int	i,j;

	mem_ptr			c_ptr;

	kernel_log_id		=	get_new_kern_log_id		("ata_drv: ",0x02);
	
	kernel_log			(kernel_log_id,"ata device detected \n");

	allocate_new_zone	(0,sizeof(struct controller_t)*2,&controllers_ptr_ref);

	
	controllers[0]	=	get_zone_ptr(&controllers_ptr_ref,0);
	controllers[1]	=	get_zone_ptr(&controllers_ptr_ref,sizeof(struct controller_t));


	controllers[0]->req_list.zone=PTR_NULL;
	controllers[1]->req_list.zone=PTR_NULL;

	tree_manager_create_node("req_list",NODE_REQUEST,&controllers[0]->req_list);
	tree_manager_create_node("req_list",NODE_REQUEST,&controllers[1]->req_list);

	dev75				= 0;                    // normal value
	_config_pci_bus_id	=pci_dev_ptr->bus_id;
	_config_pci_dev_id	=pci_dev_ptr->dev_id;
	/*
   if ( reg_incompat_flags & REG_INCOMPAT_DEVREG )
      dev75 = REG_HEADS_OBSOLETE;    // obsolete value
	*/

	//GetPciWord( pciBus, pciDev, pciFun, 0x10 + ( 8 * priSec ) );
	//ctrlBase			= GetPciWord( pciBus, pciDev, pciFun, 0x14 + ( 8 * priSec ) );

	c_ptr				=  mem_add		(&pci_dev_ptr->config,0x10);
    controllers[0]->cmd_base			=  parse_bar	(*((unsigned short *)(c_ptr))		, &controllers[0]->is_cmd_io); 
	
	c_ptr				=  mem_add		(&pci_dev_ptr->config,0x14);
	controllers[0]->ctl_base			=  parse_bar	(*((unsigned short *)(c_ptr))		, &controllers[0]->is_ctl_io); 

	c_ptr				=  mem_add		(&pci_dev_ptr->config,0x10+8);
    controllers[1]->cmd_base			=  parse_bar	(*((unsigned short *)(c_ptr))		, &controllers[1]->is_cmd_io); 

	c_ptr				= mem_add		(&pci_dev_ptr->config,0x14+8);
	controllers[1]->ctl_base			= parse_bar		(*((unsigned short *)(c_ptr))		, &controllers[1]->is_ctl_io);

	controllers[0]->cmd_base		=  set_null_bar	(controllers[0]->cmd_base	,CMD_REG_FLAGS|PRI_FLAGS);
	controllers[0]->ctl_base		=  set_null_bar	(controllers[0]->ctl_base	,CTL_REG_FLAGS|PRI_FLAGS);
	controllers[1]->cmd_base		=  set_null_bar	(controllers[1]->cmd_base	,CMD_REG_FLAGS|SEC_FLAGS);
	controllers[1]->ctl_base		=  set_null_bar	(controllers[1]->ctl_base	,CTL_REG_FLAGS|SEC_FLAGS);


	//bmideBase			= GetPciWord( pciBus, pciDev, pciFun, 0x20 );
	c_ptr				= mem_add		(&pci_dev_ptr->config,0x20);
	controllers[0]->bmide_base		= parse_bar		(*((unsigned short *)(c_ptr))	,&controllers[0]->bmide_io);
	controllers[1]->bmide_base		= controllers[0]->bmide_base+8;
	controllers[1]->bmide_io		= controllers[0]->bmide_io;

	controllers[0]->is_cmd_io		=1;
	controllers[1]->is_cmd_io		=1;

	controllers[0]->is_ctl_io		=1;
	controllers[1]->is_ctl_io		=1;

	pci_dev_ptr->sci = malloc_c(sizeof(self_config_irq) * 3);

	pci_dev_ptr->sci[0].irq_line = 14;
	pci_dev_ptr->sci[0].irq_data = 0;
	pci_dev_ptr->sci[1].irq_line = 15;
	pci_dev_ptr->sci[1].irq_data = 1;
	pci_dev_ptr->sci[2].irq_line = 0xFFFFFFFF;
	pci_dev_ptr->sci[2].irq_data = 0xFFFFFFFF;

	/*
	controllers[0]->irq_line		=14;
	controllers[1]->irq_line		=15;
	*/

	use_intr			=0;

	strcpy_c(pci_dev_ptr->name,"ata controller");
    
	
	for(controller=0;controller<2;controller++)
	{
		if(controller==0)
			kernel_log	(kernel_log_id,"primary ");
		else
			kernel_log	(kernel_log_id,"secondary ");

		writestr	("bmide [");
		writeint	(controllers[controller]->bmide_base,16);
		writestr	(controllers[controller]->bmide_io?"] io ":"] mmap ");
		writestr	("*|* cmd : [");
		writeint	(controllers[controller]->cmd_base,16);
		writestr	(controllers[controller]->is_cmd_io?"] io ":"] mmap ");
		writestr	("*|* ctl : [");
		writeint	(controllers[controller]->ctl_base,16);
		writestr	(controllers[controller]->is_ctl_io?"] io ":"] mmap ");
		writestr	("\n");

		write_bm_reg_8		(REG_BM_STATUS		,controller,BM_STATUS_ERR);
		write_stat_reg_8	(REG_STAT_ASTAT_DC	,controller,0 );

		ata_detect_device	(controller,0);
		ata_detect_device	(controller,1);

		if(( controllers[controller]->reg_config_info[0]==REG_CONFIG_TYPE_NONE)&&( controllers[controller]->reg_config_info[1]==REG_CONFIG_TYPE_NONE))
			continue;

		

		controllers[controller]->dma_region_desc.zone=PTR_NULL;
		allocate_new_zone		(0,MAX_PRD*sizeof(struct dma_region_desc_t)+65536,&controllers[controller]->dma_region_desc);

		controllers[controller]->dma_region_desc_ptr = uint_to_mem((mem_to_uint(get_zone_ptr(&controllers[controller]->dma_region_desc,0))&0xFFFF0000)+0x10000);

	   ata_select_device		(controller,0);
	   reg_reset				(0,controller,use_intr);
	   ata_detect_device_type	(controller,0);
	   ata_detect_device_type	(controller,1);


		controllers[0]->vars.zone=PTR_NULL;
		controllers[1]->vars.zone=PTR_NULL;
		allocate_new_zone	(0,16,&controllers[0]->vars);
		allocate_new_zone	(0,16,&controllers[1]->vars);


		controllers[0]->controller_used	=	get_zone_ptr(&controllers[0]->vars,0);
		controllers[1]->controller_used	=	get_zone_ptr(&controllers[1]->vars,0);

		controllers[0]->loop_used		=	get_zone_ptr(&controllers[0]->vars,4);
		controllers[1]->loop_used		=	get_zone_ptr(&controllers[1]->vars,4);

		controllers[0]->current_req.zone=PTR_NULL;
		controllers[1]->current_req.zone=PTR_NULL;

	    
	}

	for(i=0;i<2;i++)
	{
		for(j=0;j<2;j++)
		{	
			kernel_log	(kernel_log_id,"ata device ");
			
			if(i==0)
				writestr("primary ");
			else
				writestr("secondary ");
			
			if(j==0)
				writestr("master ");
			else
				writestr("slave ");

			if(controllers[i]->reg_config_info[j]==REG_CONFIG_TYPE_NONE)
				writestr	("[none]");
			if(controllers[i]->reg_config_info[j]==REG_CONFIG_TYPE_ATA)
				writestr	("[ata]");
			if(controllers[i]->reg_config_info[j]==REG_CONFIG_TYPE_ATAPI)
				writestr	("[atapi]");
			if(controllers[i]->reg_config_info[j]==REG_CONFIG_TYPE_SATA)
				writestr	("[sata]");
			if(controllers[i]->reg_config_info[j]==REG_CONFIG_TYPE_UNKN)
				writestr	("[????]");

			writestr("\n");
		}
	}
	return 1;
}




void remove_done_request(unsigned int controller_idx)
{
	tree_remove_child_by_value_dword			(&controllers[controller_idx]->req_list,1);

}



void execute_req		(unsigned int controller_idx,mem_zone_ref_ptr req)
{
	unsigned int	start_sector,nBytes;
	mem_ptr			bufferPtr;
	unsigned int	n_sectors;
	unsigned int	dev_id;
	unsigned int	rc;
	unsigned int	async;
	unsigned char	cmd_buffer[16];
	unsigned int	atapi_rc;
	mem_zone_ref	data_buffer={PTR_NULL};

	kernel_log(kernel_log_id,"ata 3 \n");

	if(!compare_z_exchange_c			(controllers[controller_idx]->controller_used,1))return;

	kernel_log(kernel_log_id,"ata 4 \n");
	
	copy_zone_ref						(&controllers[controller_idx]->current_req,req);

	async	=	0;

	tree_manager_get_child_value_i32	(&controllers[controller_idx]->current_req,NODE_HASH("start_sector"),&start_sector);
	tree_manager_get_child_value_i32	(&controllers[controller_idx]->current_req,NODE_HASH("n_bytes")		,&nBytes);
	tree_manager_get_child_value_i32	(&controllers[controller_idx]->current_req,NODE_HASH("device_id")	,&dev_id);
	tree_manager_get_child_value_i32	(&controllers[controller_idx]->current_req,NODE_HASH("async")		,&async);

	kernel_log(kernel_log_id,"ata 5 \n");
			
	switch(controllers[controller_idx]->reg_config_info[dev_id])
	{
		case REG_CONFIG_TYPE_ATA:

			n_sectors								=	((nBytes+511)>>9);

			allocate_new_zone					(0,n_sectors*512+65536,&data_buffer);
			bufferPtr							=	uint_to_mem((mem_to_uint(get_zone_ptr(&data_buffer,0))&0xFFFF0000)+0x10000);

			tree_manager_set_child_value_ptr	(&controllers[controller_idx]->current_req,"data_buffer_zone"			,data_buffer.zone);
			tree_manager_set_child_value_ptr	(&controllers[controller_idx]->current_req,"data_buffer_ptr"			,bufferPtr);


			kernel_log	(kernel_log_id,"read ata sector ");
			writeptr	(bufferPtr);
			writestr	("\n");

			rc										=	reg_pio_data_in_lba28				(controller_idx,dev_id,CMD_READ_SECTORS,0,start_sector,bufferPtr,nBytes,nBytes/512, 0);

		break;

		case REG_CONFIG_TYPE_ATAPI:

			n_sectors								=	((nBytes+2047)>>11);
			allocate_new_zone							(0,n_sectors*2048+65536,&data_buffer);

			bufferPtr							=	uint_to_mem((mem_to_uint(get_zone_ptr(&data_buffer,0))&0xFFFF0000)+0x10000);

			tree_manager_set_child_value_ptr	(&controllers[controller_idx]->current_req,"data_buffer_zone"			,data_buffer.zone);
			tree_manager_set_child_value_ptr	(&controllers[controller_idx]->current_req,"data_buffer_ptr"			,bufferPtr);

			kernel_log		(kernel_log_id,"read atapi sectors ");
			writestr		(" ");
			writeint		(mem_to_uint(bufferPtr),16);
			writestr		(" ");
			writeint		(mem_to_uint(mem_add(bufferPtr,n_sectors*2048)),16);
			writestr		(" ");
			writeint		(start_sector,10);
			writestr		(" ");
			writeint		(n_sectors,10);
			writestr		("/");
			writeint		(nBytes,10);
			writestr		(" ");
			writeptr		(bufferPtr);
			writestr		("\n");

			memset_c								( cmd_buffer, 0, 16 );
			cmd_buffer[0]							=	ATAPI_CMD_READ_CD;		// command code
			cmd_buffer[1]							=	write_bits_8(cmd_buffer[1],SECTOR_TYPE_ANY,2,3);
			*((unsigned int *)(&cmd_buffer[2]))		=	htobe32(start_sector);
			htobe24									(n_sectors,&cmd_buffer[6]);

			cmd_buffer[9]							=	0;
			cmd_buffer[9]							=	write_bits_8(cmd_buffer[9] ,ERROR_TYPE_NONE	,1,2);
			cmd_buffer[9]							=	write_bits_8(cmd_buffer[9] ,ECC_DATA_NO		,3,1);
			cmd_buffer[9]							=	write_bits_8(cmd_buffer[9] ,USER_DATA_YES	,4,1);
			cmd_buffer[9]							=	write_bits_8(cmd_buffer[9] ,HEADER_TYPE_NONE,5,2);
			cmd_buffer[9]							=	write_bits_8(cmd_buffer[9] ,SYNC_DATA_NO	,7,1);

			cmd_buffer[10]							=	0;
			cmd_buffer[10]							=	write_bits_8(cmd_buffer[10],SUBC_TYPE_NONE	,0,3);

			//atapi_rc								=	reg_packet		(controller_idx,dev_id,12,cmd_buffer,0,n_sectors*2048, bufferPtr,0L  );

			if(async)
			{
				controllers[controller_idx]->statReg	=   sub_readBusMstrStatus(controller_idx) & 0x60;
				atapi_rc								=	dma_pci_packet	(controller_idx,dev_id,12,cmd_buffer,0,n_sectors*2048, bufferPtr);
			}
			else
			{
				atapi_rc								=	reg_packet		(controller_idx,dev_id,12,cmd_buffer,0,n_sectors*2048, bufferPtr,0L  );
			}
		break;
	}
	if(async)return;
	
	tree_manager_write_node_dword						(&controllers[controller_idx]->current_req,0,1);
	release_zone_ref									(&controllers[controller_idx]->current_req);
	(*controllers[controller_idx]->controller_used)	=	0;
	

}


unsigned int add_new_req(unsigned int controller_idx,unsigned int dev_id,mem_zone_ref_ptr req)
{
	int ret;
	tree_manager_write_node_dword		(req,0,0);
	tree_manager_set_child_value_i32	(req,"device_id",dev_id);

	return tree_manager_node_add_child(&controllers[controller_idx]->req_list, req);
}

void execute_controller_request(unsigned int controller_idx)
{
	unsigned int		n_reqs=0;
	mem_zone_ref		req_list={PTR_NULL};
	mem_zone_ref_ptr	req;

	kernel_log(kernel_log_id,"ata 1 \n");

	if(!tree_manager_get_first_child(&controllers[controller_idx]->req_list,&req_list,&req))return;

	while((req->zone!=PTR_NULL)&&((*controllers[controller_idx]->controller_used)==0))
	{
		kernel_log(kernel_log_id,"ata 2 \n");

		if(tree_mamanger_compare_node_dword(req,0,0)==0)
			execute_req						(controller_idx,req);

		tree_manager_get_next_child		(&req_list,&req);
	}
	if(req->zone!=PTR_NULL)
	{
		dec_zone_ref		(req);
		release_zone_ref	(&req_list);
	}
}



OS_INT_C_FUNC(unsigned int) interupt_handler(void *param)
{
	unsigned int primary=0;
	// PCI ATA controller...
    // ... read BMIDE status
    //int_bm_status = sub_readBusMstrStatus(0);

	read_bm_reg_8	(REG_BM_STATUS,primary,&int_bm_status);

	kernel_log	(kernel_log_id,"ata int ");
	writeint	(int_bm_status,16);
	writestr	("\n");

	
	//... check if Interrupt bit = 1
    if ( int_bm_status & BM_STATUS_INT )
    {
		unsigned int status;
		unsigned char reason;
        // ... Interrupt=1...
        // ... increment interrupt flag,
        // ... read ATA status,
        // ... reset Interrupt bit.
        int_intr_flag ++ ;
		
		read_cmd_reg_8	(REG_CMD_STATUS_CMD,primary,&int_ata_status );

		// End of command...
		// disable/stop the dma channel

		status		 =   int_bm_status;					// read BM status
		status		&= ~ BM_STATUS_ACT;					// ignore Active bit
		sub_writeBusMstrCmd(primary, BM_CMD_STOP );		// shutdown DMA
		sub_readBusMstrCmd (primary);                   // read BM cmd (just for trace)
		status |= sub_readBusMstrStatus(primary);       // read BM statu again

		if ( status & ( REG_STATUS_ERR ) )        // bus master error?
		{
			tree_manager_set_child_value_i32		(&controllers[primary]->current_req,"error",3);
			kernel_log(kernel_log_id,"error atapi #3\n");
		}
		if ( ( status & BM_STATUS_ACT ) )        // end of PRD list?
		{
			tree_manager_set_child_value_i32		(&controllers[primary]->current_req,"error",4);
			kernel_log(kernel_log_id,"error atapi #4\n");
		}
		// End of command...
		// If no error use the Status register value that was read
		// by the interrupt handler. If there was an error
		// read the Status register because it may not have been
		// read by the interrupt handler.

		status = int_ata_status;

		// Final status check...
		// if no error, check final status...
		// Error if BUSY, DRQ or ERROR status now.

		if ( status & ( REG_STATUS_BSY | REG_STATUS_DRQ | REG_STATUS_ERR ) )
		{
			tree_manager_set_child_value_i32		(&controllers[primary]->current_req,"error",5);

			kernel_log(kernel_log_id,"error atapi #5 ");
			writeint(status,16);
			writestr("\n");
		}
		// Final status check...
		// Check for protocol failures...
		// check: C/nD=1, IO=1.

		read_cmd_reg_8	( REG_CMD_SEC_CNT		,primary, &reason);
		if ( ( reason & ( REG_STATUS_TAG | REG_STATUS_REL ) )|| ( ! ( reason & REG_STATUS_IO ) )|| ( ! ( reason & REG_STATUS_CD ) ))
		{
			tree_manager_set_child_value_i32		(&controllers[primary]->current_req,"error",6);
			kernel_log								(kernel_log_id,"error atapi #6\n");
		}
		write_bm_reg_8	(REG_BM_STATUS,primary,BM_STATUS_INT);

		tree_manager_write_node_dword						(&controllers[primary]->current_req,0,1);
		release_zone_ref									(&controllers[primary]->current_req);
		(*controllers[primary]->controller_used)	=	0;

		if(compare_z_exchange_c(controllers[primary]->loop_used,1))
		{
			execute_controller_request			(primary);
			*(controllers[primary]->loop_used)=0;
		}
	}

	return 1;
}





/*
struct ata_identify
{


};
0	427Ah	15	ATA Device = Yes
 	 	17	Removable Media Device = No
 	 	2	Response Incomplete = No
1	3FFFh	-	(Obsolete) # of Logical Cylinders = 3FFFh
2	0000h	-	Specific Configuration = None
3	0010h	-	(Obsolete) # of Logical Heads = 10h
6	003Fh	-	(Obsolete) # of Sectors per Track = 3Fh
10-19	 	-	Serial Number = WD-WMA8E3265410
23-26	 	-	Firmware Revision = 16.06V1
27-46	 	-	Model Number = WDC WD800BB-75CAA0
47	8010h	7-0	Max # of sectors xfered per interrupt on R/W MULTIPLE cmds = 10h
48	0000h	-	Trusted Computing feature set options = 0h
53	0007h	2	Fields reported in word 88 valid = Yes
 	 	1	Fields reported in words 70:64 valid = Yes
59	0110h	8	Multiple sector setting is valid = Yes
60-61	 	-	Total number of user addressable sectors = 9502F90h (74.51 Gbytes)
63	0007h	-	*** MULTIWORD DMA SETTINGS ***
 	 	10	Multiword DMA Mode 2 selected = No
 	 	9	Multiword DMA Mode 1 selected = No
 	 	8	Multiword DMA Mode 0 selected = No
 	 	2	Multiword DMA Mode 2 and below supported = Yes
 	 	1	Multiword DMA Mode 1 and below supported = Yes
 	 	0	Multiword DMA Mode 0 supported = Yes
64	0003h	7-0	PIO Modes Supported = 3h
65	0078h	-	Minimum Multiword DMA xfer cycle time per word in nsecs = 120
66	0078h	-	Recommended Multiword DMA xfer cycle time in nsecs = 120
67	0078h	-	Minimum PIO xfer cycle time without flow control in nsecs = 120
68	0078h	-	Minimum PIO xfer cycle time with IORDY flow control in nsecs = 120
82	3469h	-	*** COMMAND SETS SUPPORTED ***
 	 	14	NOP command supported = No
 	 	13	READ BUFFER command supported = Yes
 	 	12	WRITE BUFFER command supported = Yes
 	 	10	Host Protected Area feature set supported = Yes
 	 	9	DEVICE RESET command supported = No
 	 	8	SERVICE interrupt supported = No
 	 	7	Release interrupt supported = No
 	 	6	Look-ahead supported = Yes
 	 	5	Write cache supported = Yes
 	 	3	Mandatory Power Management feature set supported = Yes
 	 	2	Removable Media feature set supported = No
 	 	1	Security Mode feature set supported = No
 	 	0	SMART feature set supported = Yes
83h	4B01h	-	*** COMMAND SETS SUPPORTED ***
 	 	13	FLUSH CACHE EXT command supported = No
 	 	12	Mandatory FLUSH CACHE command supported = No
 	 	11	Device Configuration Overlay feature set supported = Yes
 	 	10	48-bit Address feature set supported = No
 	 	9	Automatic Acoustic Management feature set supported = Yes
 	 	8	SET MAX security extension supported = Yes
 	 	6	SET FEATURES subcommand required to spinup after power-up = No
 	 	5	Power-Up In Standby feature set supported = No
 	 	4	Removable Media Status Notification feature set supported = No
 	 	3	Advanced Power Management feature set supported = No
 	 	2	CFA feature set supported = No
 	 	1	READ/WRITE DMA QUEUED supported = No
 	 	0	DOWNLOAD MICROCODE command supported = Yes
84	4000h	-	*** COMMAND SET/FEATURE SUPPORTED EXTENSION ***
 	 	12	Time-limited R/W feature set R/W continuous enabled = No
 	 	11	Time-limited R/W feature set supported = No
 	 	10	URG bit supported for WRITE STREAM DMA/PIO = No
 	 	9	URG bit supported for READ STREAM DMA/PIO = No
 	 	8	World wide name supported = No
 	 	7	WRITE DMA QUEUED FUA EXT command supported = No
 	 	6	WRITE DMA FUA EXT and WRITE MULTIPLE FUA EXT supported = No
 	 	5	General Purpose Logging feature set supported = No
 	 	4	Streaming feature set supported = No
 	 	3	Media Card Pass Through Command feature set supported = No
 	 	2	Media serial number supported = No
 	 	1	SMART self-test supported = No
 	 	0	SMART error logging supported = No
85	3469h	-	*** COMMAND SET/FEATURE ENABLED ***
 	 	14	NOP command enabled = No
 	 	13	READ BUFFER command enabled = Yes
 	 	12	WRITE BUFFER command enabled = Yes
 	 	10	Host Protected Area feature set enabled = Yes
 	 	9	DEVICE RESET command enabled = No
 	 	8	SERVICE interrupt enabled = No
 	 	7	Release interrupt enabled = No
 	 	6	Look-ahead enabled = Yes
 	 	5	Write cache enabled = Yes
 	 	3	Power Management feature set enabled = Yes
 	 	2	Removable Media feature set enabled = No
 	 	1	Security Mode feature set enabled = No
 	 	0	SMART feature set enabled = Yes
86	0A01h	-	*** COMMAND SET/FEATURE ENABLED ***
 	 	13	FLUSH CACHE EXT command supported = No
 	 	12	FLUSH CACHE command supported = No
 	 	11	Device Configuration Overlay supported = Yes
 	 	10	48-bit Address features set supported = No
 	 	9	Automatic Acoustic Management feature set enabled = Yes
 	 	8	SET MAX security ext enabled by SET MAX SET PASSWORD = No
 	 	6	SET FEATURES subcommand required to spin-up after power-up = No
 	 	5	Power-Up In Standby feature set enabled = No
 	 	4	Removable Media Status Notification feature set enabled = No
 	 	3	Advanced Power Management feature set enabled = No
 	 	2	CFA feature set enabled = No
 	 	1	READ/WRITE DMA QUEUED command supported = No
 	 	0	DOWNLOAD MICROCODE command supported = Yes
87	4000h	-	*** COMMAND SET/FEATURE DEFAULT ***
 	 	12	Time-limited R/W feature set R/W continuous enabled = No
 	 	11	Time-limited R/W feature set enabled = No
 	 	10	URG bit supported for WRITE STREAM DMA/PIO = No
 	 	9	URG bit supported for READ STREAM DMA/PIO = No
 	 	8	World wide name supported = No
 	 	7	WRITE DMA QUEUED FUA EXT command supported = No
 	 	6	WRITE DMA FUA EXT and WRITE MULTIPLE FUA EXT supported = No
 	 	5	General Purpose Logging feature set supported = No
 	 	4	Valid CONFIGURE STREAM command has been executed = No
 	 	3	Media Card Pass Through Command feature set enabled = No
 	 	2	Media serial number is valid = No
 	 	1	SMART self-test supported = No
 	 	0	SMART error logging supported = No
88	203Fh	-	*** ULTRA DMA SETTINGS ***
 	 	14	Ultra DMA Mode 6 Selected = Yes
 	 	13	Ultra DMA Mode 5 Selected = Yes
 	 	12	Ultra DMA Mode 4 Selected = No
 	 	11	Ultra DMA Mode 3 Selected = No
 	 	10	Ultra DMA Mode 2 Selected = No
 	 	9	Ultra DMA Mode 1 Selected = No
 	 	8	Ultra DMA Mode 0 Selected = No
 	 	6	Ultra DMA Mode 6 and below supported = No
 	 	5	Ultra DMA Mode 5 and below supported = Yes
 	 	4	Ultra DMA Mode 4 and below supported = Yes
 	 	3	Ultra DMA Mode 3 and below supported = Yes
 	 	2	Ultra DMA Mode 2 and below supported = Yes
 	 	1	Ultra DMA Mode 1 and below supported = Yes
 	 	0	Ultra DMA Mode 0 and below supported = Yes
94	8080h	15-8	Vendor's recommended acoustic management value = Minimum acoustic emanation level (80h)
 	 	7-0	Current automatic acoustic management value = Minimum acoustic emanation level (80h)
95	0000h	-	Stream Minimum Request Size = 0h
96	0000h	--	Streaming Transfer Time - DMA = 0h
97	0000h	-	Streaming Access Latency - DMA and PIO = 0h
98-99	 	-	Streaming Performance Granularity = 0h
100-103	 	-	Maximum user LBA for 48-bit address = 0000000000000000h
104	0000h	-	Streaming Transfer Time - PIO = 0h
127	0000h	1-0	Removable Media Status Notification = Not supported
176-205	 	-	Current media serial number =
255	4FA5h	15-8	Checksum = 4Fh
*/

OS_API_C_FUNC(int)	stream_set_pos (unsigned int str_idx,large_uint_t  sec_pos)
{
		int i,j;
	if(str_idx>=4)return 0;

	
	for(i=0;i<2;i++)
	{
		for(j=0;j<2;j++)
		{	
			unsigned int rc;

			if((controllers[i]->reg_config_info[j]==REG_CONFIG_TYPE_NONE)||(controllers[i]->reg_config_info[j]==REG_CONFIG_TYPE_UNKN))continue;
			if((i*2+j)==str_idx)
			{
				if(controllers[i]->reg_config_info[j]==REG_CONFIG_TYPE_ATA)
					rc = reg_non_data_lba28( i,j,CMD_SEEK,0, sec_pos.uint32.ints[0],0);

				if(controllers[i]->reg_config_info[j]==REG_CONFIG_TYPE_ATAPI)
					rc = reg_non_data_lba28( i,j,ATAPI_CMD_SEEK,0, sec_pos.uint32.ints[0],0);

				if(rc)
				{
					kernel_log(kernel_log_id,"seek command done \n");
				}
				else
				{
					kernel_log(kernel_log_id,"seek command failed \n");
					return 1;
				}				
			}
		}
	}

	return 1;
}

OS_API_C_FUNC(int)	stream_infos		(unsigned int str_idx,mem_zone_ref	*infos_node)
{

	int i,j;

	
	if(str_idx>=4)return 0;

	
	for(i=0;i<2;i++)
	{
		for(j=0;j<2;j++)
		{	
			if((controllers[i]->reg_config_info[j]==REG_CONFIG_TYPE_NONE)||(controllers[i]->reg_config_info[j]==REG_CONFIG_TYPE_UNKN))continue;
			if((i*2+j)==str_idx)
			{
				unsigned short bufferPtr[1024];
				
				unsigned int rc;
				
				memset_c( bufferPtr, 0, sizeof( bufferPtr ) );


				

				if(controllers[i]->reg_config_info[j]==REG_CONFIG_TYPE_ATA)
					rc = reg_pio_data_in_lba28(i,j,CMD_IDENTIFY_DEVICE,0,0,bufferPtr,2048,1L, 0);
				
				if(controllers[i]->reg_config_info[j]==REG_CONFIG_TYPE_ATAPI)
					rc = reg_pio_data_in_lba28(i,j,CMD_IDENTIFY_DEVICE_PACKET,0,0,bufferPtr,2048,1L, 0);
				
				if(rc)
				{
					mem_zone_ref info;
					
					int				n_txt;
					char		   txt[40];

					info.zone		=	PTR_NULL;
					


					tree_manager_create_node("ata infos",NODE_BUS_DEVICE_STREAM_INFOS,infos_node);

					tree_manager_add_child_node(infos_node,"drive controller",NODE_GFX_STR,&info);
					if(i==0)
						tree_manager_write_node_data(&info,"primary",0,strlen_c("primary")+1);
					else
						tree_manager_write_node_data(&info,"secondary",0,strlen_c("secondary")+1);

					release_zone_ref(&info);

					tree_manager_add_child_node(infos_node,"pos",NODE_GFX_STR,&info);
					if(j==0)
						tree_manager_write_node_data(&info,"master",0,strlen_c("master")+1);
					else
						tree_manager_write_node_data(&info,"slave",0,strlen_c("slave")+1);
						
					
					release_zone_ref(&info);

					tree_manager_add_child_node(infos_node,"drive type",NODE_GFX_STR,&info);
					switch(controllers[i]->reg_config_info[j])
					{


						default							:tree_manager_write_node_data(&info,"unknown",0,strlen_c("unknown")+1);break;
						case 	REG_CONFIG_TYPE_SATA	:tree_manager_write_node_data(&info,"sata",0,strlen_c("sata")+1);break;
						case 	REG_CONFIG_TYPE_ATA		:tree_manager_write_node_data(&info,"ata",0,strlen_c("ata")+1);break;
						case  	REG_CONFIG_TYPE_ATAPI	:tree_manager_write_node_data(&info,"atapi",0,strlen_c("atapi")+1);break;
					}
					

					release_zone_ref(&info);

					n_txt=0;
					while(n_txt<10)
					{

						txt[n_txt*2+0]= ((bufferPtr[10+n_txt]>>8) &0xFF);
						txt[n_txt*2+1]= ((bufferPtr[10+n_txt])	  &0xFF);

						n_txt++;
					}

					tree_manager_add_child_node	(infos_node,"serial",NODE_GFX_STR,&info);
					tree_manager_write_node_data(&info,txt,0,20);
					release_zone_ref			(&info);

					n_txt=0;
					while(n_txt<20)
					{

						txt[n_txt*2+0]= ((bufferPtr[27+n_txt]>>8) &0xFF);
						txt[n_txt*2+1]= ((bufferPtr[27+n_txt])	  &0xFF);

						n_txt++;
					}

					tree_manager_add_child_node	(infos_node,"model",NODE_GFX_STR,&info);
					tree_manager_write_node_data(&info,txt,0,40);
					release_zone_ref			(&info);

					if(controllers[i]->reg_config_info[j]==REG_CONFIG_TYPE_ATAPI)
					{
						unsigned char	cmd_buffer[16];
						mem_zone_ref	data_buffer_ref;
						unsigned char	*data_buffer;
						unsigned int	atapi_rc;
						memset_c( cmd_buffer, 0, 12 );
						cmd_buffer[0] = ATAPI_CMD_READ_TOC;		// command code
						cmd_buffer[1] = 0x02;					// MSF flag
						cmd_buffer[7] = 0x10;					// allocation length
						cmd_buffer[8] = 0x00;					// of 4096
						cmd_buffer[9] = 0x80;					// TOC format

						data_buffer_ref.zone=PTR_NULL;
						allocate_new_zone(0,1024,&data_buffer_ref);

						data_buffer=get_zone_ptr(&data_buffer_ref,0);
						memset_c( data_buffer, 0, 1024 );

						atapi_rc = reg_packet(i,j,12, cmd_buffer,0,1024, data_buffer,0L  );

									

						if(atapi_rc==1)
						{
							unsigned short toc_length,first_track,last_track;
							mem_ptr		   toc_ptr,toc_end;
							mem_zone_ref	session_list={PTR_NULL};
							mem_zone_ref	session		={PTR_NULL};

							toc_length	=	be16toh(*((unsigned short *)(data_buffer)));

							kernel_log		(kernel_log_id,"toc length ");
							writeint		(toc_length,16);
							writestr		("\n");

							first_track =	*((unsigned char *)(mem_add(data_buffer,2)));
							last_track	=	*((unsigned char *)(mem_add(data_buffer,3)));

							toc_end		=	mem_add(data_buffer,toc_length+2);
							toc_ptr		=	mem_add(data_buffer,4);




							tree_manager_add_child_node	(infos_node,"session_list",0,&session_list);

							while(toc_ptr<toc_end)
							{
								unsigned char sess_n,add_ctl,point,tno;
								unsigned int  ts;
								

								sess_n	=	*((unsigned char *)(mem_add(toc_ptr,0)));
								add_ctl	=	*((unsigned char *)(mem_add(toc_ptr,1)));
								tno		=	*((unsigned char *)(mem_add(toc_ptr,2)));
								point	=	*((unsigned char *)(mem_add(toc_ptr,3)));
								ts		=	(*((unsigned int *)(mem_add(toc_ptr,8))))&0x00FFFFFF;

								tree_manager_add_child_node		(&session_list,"session",0,&session);
								tree_manager_set_child_value_i32(&session,"number",sess_n);
								tree_manager_set_child_value_i32(&session,"track no",tno);
								tree_manager_set_child_value_i32(&session,"point",point);
								tree_manager_set_child_value_i32(&session,"ts",ts);
								

								toc_ptr=mem_add(toc_ptr,11);

								release_zone_ref(&session);

							}

							

							release_zone_ref(&session_list);
							//snooze(10000000);

							
						}
						else
						{
							kernel_log(kernel_log_id,"ATAPI read TOC command failed \n");
						}

						release_zone_ref(&data_buffer_ref);
					}	
					
					

					return 1;
				}
				else
				{
					kernel_log(kernel_log_id,"identify device command failed \n");
					return 1;
				}
			}
		}
	}
	
	return 1;
}




OS_API_C_FUNC(void) read_stream(unsigned int stream_idx,mem_zone_ref *req)
{

	int i,j;
	
	for(i=0;i<2;i++)
	{
		
		remove_done_request						(i);

		for(j=0;j<2;j++)
		{

			kernel_log(kernel_log_id, "loop req ");
			writeint(i, 10);
			writestr(" ");
			writeint(j, 10);
			writestr(" ");
			writeint(controllers[i]->stream_idx[j], 10);
			writestr(" ");
			writeint(stream_idx, 10);
			writestr("\n");

			

			if(controllers[i]->stream_idx[j]==stream_idx)
			{
				unsigned int	async;

				async	=	0;
				tree_manager_get_child_value_i32	(req,NODE_HASH("async")		,&async);

				add_new_req							(i,j,req);

				kernel_log							(kernel_log_id,"add req ");
				writeint							(*controllers[i]->loop_used,10);
				writestr							("\n");
				tree_manager_dump_node_rec			(&controllers[i]->req_list,0,3);

				


				if(compare_z_exchange_c(controllers[i]->loop_used,1))
				{
					kernel_log							(kernel_log_id,"start loop \n");
					while(tree_mamanger_compare_node_dword(req,0,1)!=0)
					{
						execute_controller_request			(i);
						if(async)break;
					}
					(*controllers[i]->loop_used)=0;
				}

				kernel_log							(kernel_log_id,"add req 2 ");
				writeint							(*controllers[i]->loop_used,10);
				writestr							("\n");
				return ;
			}
		}
	}
}

OS_API_C_FUNC(int)	init_driver		()
{
	unsigned int	i,j;
	stream_device	*str_dev;

	stream_dev_id		=  new_stream_device		(_config_pci_bus_id,_config_pci_dev_id);
	if(stream_dev_id==0xFFFFFFFF)
	{
		kernel_log(kernel_log_id,"could not create stream device \n");
		return 0;
	}

	str_dev				=  find_stream_device		(stream_dev_id);
	for(i=0;i<2;i++)
	{
		for(j=0;j<2;j++)
		{	
			if((controllers[i]->reg_config_info[j]!=REG_CONFIG_TYPE_NONE)&&(controllers[i]->reg_config_info[j]!=REG_CONFIG_TYPE_UNKN))
			{
				controllers[i]->stream_idx[j]	=	stream_dev_new_stream				(str_dev,ATA_BUFFER_SIZE);
			}
			else
			{
				controllers[i]->stream_idx[j]	=	0xFFFFFFFF;
			}
		}


		//setup_interupt_c	(controllers[i]->irq_line, interupt_handler,i);
	}
	return 1;
}

