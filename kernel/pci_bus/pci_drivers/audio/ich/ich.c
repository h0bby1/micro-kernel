#define DRIVER_FUNC			C_EXPORT

#include <std_def.h>
#include <std_mem.h>
#include "kern.h"
#include "sys_pci.h"
#include "mem_base.h"
#include "lib_c.h"
#include "sys/async_stream.h"
#include "sys/mem_stream.h"
#include "sys/tpo_mod.h"
#include "sys/file_system.h"
#include <bus_manager/bus_drv.h>
#include "sys/stream_dev.h"
#include "sys/pci_device.h"



#define GET_HW_SAMPLE_SIZE(cfg)	(((cfg)->sub_type == TYPE_SIS7012) ? 1 : 2)
#define	GET_REG_X_SR(cfg)		(((cfg)->sub_type == TYPE_SIS7012) ? _ICH_REG_X_PICB : _ICH_REG_X_SR)
#define	GET_REG_X_PICB(cfg)		(((cfg)->sub_type == TYPE_SIS7012) ? _ICH_REG_X_SR : _ICH_REG_X_PICB)

#define ICH4_MMBAR_SIZE	512
#define ICH4_MBBAR_SIZE	256

#define status_t			int
#define TYPE_DEFAULT		0x00
#define TYPE_ICH4			0x01
#define TYPE_SIS7012		0x02
#define BUFFER_SIZE			2048
#define BUFFER_COUNT		2
#define BUFFER_FRAMES_COUNT (BUFFER_SIZE / 4)

#define ICH_BD_COUNT		32


#define         AC97_POWER_ADC          (1 << 0)
#define         AC97_POWER_DAC          (1 << 1)
#define         AC97_POWER_ANL          (1 << 2)
#define         AC97_POWER_REF          (1 << 3)
#define         AC97_POWER_STATUS       (AC97_POWER_ADC | AC97_POWER_DAC | AC97_POWER_REF | AC97_POWER_ANL )

struct device_item_s
{
	unsigned short			vendor_id;
	unsigned short			device_id;
	unsigned char			type;
	const char				*name;
};



typedef struct
{
	unsigned int	pci_dev_id;
	unsigned int	pci_bus_id;
	unsigned int	nambar;
	unsigned int	nabmbar;
	unsigned int	mmbar; 
	unsigned int	mbbar; 
	mem_ptr			log_mmbar;
	mem_ptr			log_mbbar;	
	unsigned int	sub_type;
	unsigned int	codecoffset;
	unsigned int	reversed_eamp_polarity;
	unsigned int	ac97_codec_id;
} device_config;


/* The Hardware Buffer Descriptor */
struct ich_bd_s{
	volatile unsigned int	buffer;
	volatile unsigned short length;
	volatile unsigned short flags;
	#define ICH_BDC_FLAG_IOC 0x8000
} ;

/* the software channel descriptor */
struct ich_chan_s{
	struct ich_bd_s			ich_bd_array[ICH_BD_COUNT]					;
	unsigned int			stream_idx;
	unsigned int			regbase;
	volatile unsigned int	running;
} ;


/* Native Audio Bus Master Control Registers */
enum ICH_GLOBAL_REGISTER 
{
	ICH_REG_GLOB_CNT	= 0x2C,
	ICH_REG_GLOB_STA	= 0x30,
	ICH_REG_ACC_SEMA	= 0x34,
	ICH_REG_SDM			= 0x80
};

enum ICH_X_REGISTER_BASE /* base addresses for the following offsets */
{
	ICH_REG_PI_BASE		= 0x00,
	ICH_REG_PO_BASE		= 0x10,
	ICH_REG_MC_BASE		= 0x20
};

enum ICH_X_REGISTER_OFFSETS /* add base address to get the PI, PO or MC reg */
{
	ICH_REG_X_BDBAR		= 0x00,
	ICH_REG_X_CIV		= 0x04,
	ICH_REG_X_LVI		= 0x05,
	_ICH_REG_X_SR		= 0x06, /* use GET_REG_X_SR()   macro from config.h */
	_ICH_REG_X_PICB		= 0x08, /* use GET_REG_X_PICB() macro from config.h */
	ICH_REG_X_PIV		= 0x0A,
	ICH_REG_X_CR		= 0x0B
};

/* ICH_REG_X_SR (Status Register) Bits */
enum REG_X_SR_BITS 
{
	SR_DCH				= 0x0001,
	SR_CELV				= 0x0002,
	SR_LVBCI			= 0x0004,
	SR_BCIS				= 0x0008,
	SR_FIFOE			= 0x0010
};

/* ICH_REG_X_CR (Control Register) Bits */
enum REG_X_CR_BITS
{
	CR_RPBM				= 0x01,
	CR_RR				= 0x02,
	CR_LVBIE			= 0x04,
	CR_FEIE				= 0x08,
	CR_IOCE				= 0x10
};

/* ICH_REG_GLOB_CNT (Global Control Register) Bits */
enum REG_GLOB_CNT_BITS
{
	CNT_GIE				= 0x01,
	CNT_COLD			= 0x02,
	CNT_WARM			= 0x04,
	CNT_SHUT			= 0x08,
	CNT_PRIE			= 0x10,
	CNT_SRIE			= 0x20
};

/* ICH_REG_GLOB_STA (Global Status Register) Bits */
enum REG_GLOB_STA_BITS
{
	STA_GSCI			= 0x00000001, /* GPI Status Change Interrupt */
	STA_MIINT			= 0x00000002, /* Modem In Interrupt */
	STA_MOINT			= 0x00000004, /* Modem Out Interrupt */
	STA_PIINT			= 0x00000020, /* PCM In Interrupt */
	STA_POINT			= 0x00000040, /* PCM Out Interrupt */
	STA_MINT			= 0x00000080, /* Mic In Interrupt */
	STA_S0CR			= 0x00000100, /* AC_SDIN0 Codec Ready */
	STA_S1CR			= 0x00000200, /* AC_SDIN1 Codec Ready */
	STA_S0RI			= 0x00000400, /* AC_SDIN0 Resume Interrupt */
	STA_S1RI			= 0x00000800, /* AC_SDIN1 Resume Interrupt */
	STA_RCS				= 0x00008000, /* Read Completition Status */
	STA_AD3				= 0x00010000,
	STA_MD3				= 0x00020000,
	STA_M2INT			= 0x01000000,	/* Microphone 2 In Interrupt */
	STA_P2INT			= 0x02000000,	/* PCM In 2 Interrupt */
	STA_SPINT			= 0x04000000,	/* S/PDIF Interrupt */
	STA_BCS				= 0x08000000,	/* Bit Clock Stopped */
	STA_S2CR			= 0x10000000,	/* AC_SDIN2 Codec Ready */
	STA_S2RI			= 0x20000000,	/* AC_SDIN2 Resume Interrupt */
	STA_PRES			= 0x00000400,
	STA_SRES			= 0x00000800,

	STA_INTMASK			= (STA_MIINT | STA_MOINT | STA_PIINT | STA_POINT | STA_MINT | STA_S0RI | STA_S1RI | STA_M2INT | STA_P2INT | STA_SPINT | STA_S2RI)

};


/* PCI Configuration Space */


/** 
 * search for the ICH AC97 controller, and initialize the global config 
 */

// some codec_ids
enum {
	CODEC_ID_ALC201A	= 0x414c4710,
	CODEC_ID_AK4540		= 0x414b4d00,
	CODEC_ID_AK4542		= 0x414b4d01,
	CODEC_ID_AD1819 	= 0x41445303, // ok, AD1819A, AD1819B
	CODEC_ID_AD1881		= 0x41445340, // ok, AD1881
	CODEC_ID_AD1881A	= 0x41445348, // ok, AD1881A
	CODEC_ID_AD1885		= 0x41445360, // ok, AD1885
	CODEC_ID_AD1886		= 0x41445361, // ok, AD1886
	CODEC_ID_AD1886A 	= 0x41445363, // ok, AD1886A
	CODEC_ID_AD1887		= 0x41445362, // ok, AD1887
	CODEC_ID_AD1980		= 0x41445370, // ok, AD1980
	CODEC_ID_AD1981B	= 0x41445374, // ok, AD1981B
	CODEC_ID_AD1985		= 0x41445375, // ok, AD1985
	CODEC_ID_CS4299A	= 0x43525931,
	CODEC_ID_CS4299C	= 0x43525933,
	CODEC_ID_CS4299D	= 0x43525934,
	CODEC_ID_STAC9700	= 0x83847600, // ok, STAC9700
	CODEC_ID_STAC9704	= 0x83847604, // STAC9701/03, STAC9704/07, STAC9705 (???)
	CODEC_ID_STAC9705	= 0x83847605, // ???
	CODEC_ID_STAC9708	= 0x83847608, // ok, STAC9708/11
	CODEC_ID_STAC9721	= 0x83847609, // ok, STAC9721/23
	CODEC_ID_STAC9744	= 0x83847644, // ok, STAC9744
	CODEC_ID_STAC9752	= 0x83847652, // ok, STAC9752/53
	CODEC_ID_STAC9756	= 0x83847656, // ok, STAC9756/57
	CODEC_ID_STAC9766	= 0x83847666, // ok, STAC9766/67
};

// capabilities
enum ac97_capability {
	CAP_PCM_MIC				= 0x0000000000000001ULL, /* dedicated mic PCM channel */
	CAP_BASS_TREBLE_CTRL	= 0x0000000000000002ULL,
	CAP_SIMULATED_STEREO	= 0x0000000000000004ULL,
	CAP_HEADPHONE_OUT		= 0x0000000000000008ULL,
	CAP_LAUDNESS			= 0x0000000000000010ULL,
	CAP_DAC_18BIT			= 0x0000000000000020ULL,
	CAP_DAC_20BIT			= 0x0000000000000040ULL,
	CAP_ADC_18BIT			= 0x0000000000000080ULL,
	CAP_ADC_20BIT			= 0x0000000000000100ULL,
	CAP_3D_ENHANCEMENT		= 0x0000000000000200ULL,
	CAP_VARIABLE_PCM		= 0x0000000000000400ULL, /* variable rate PCM */
	CAP_DOUBLE_PCM			= 0x0000000000000800ULL, /* double rate PCM */
	CAP_SPDIF				= 0x0000000000001000ULL,
	CAP_VARIABLE_MIC		= 0x0000000000002000ULL, /* variable rate mic PCM */
	CAP_CENTER_DAC			= 0x0000000000004000ULL,
	CAP_SURR_DAC			= 0x0000000000008000ULL,
	CAP_LFE_DAC				= 0x0000000000010000ULL,
	CAP_AMAP				= 0x0000000000020000ULL,
	CAP_REV21				= 0x0000000000040000ULL,
	CAP_REV22				= 0x0000000000080000ULL,
	CAP_REV23				= 0x0000000000100000ULL,
	CAP_PCM_RATE_CONTINUOUS	= 0x0000000000200000ULL,
	CAP_PCM_RATE_8000		= 0x0000000000400000ULL,
	CAP_PCM_RATE_11025		= 0x0000000000800000ULL,
	CAP_PCM_RATE_12000		= 0x0000000001000000ULL,
	CAP_PCM_RATE_16000		= 0x0000000002000000ULL,
	CAP_PCM_RATE_22050		= 0x0000000004000000ULL,
	CAP_PCM_RATE_24000		= 0x0000000008000000ULL,
	CAP_PCM_RATE_32000		= 0x0000000010000000ULL,
	CAP_PCM_RATE_44100		= 0x0000000020000000ULL,
	CAP_PCM_RATE_48000		= 0x0000000040000000ULL,
	CAP_PCM_RATE_88200		= 0x0000000080000000ULL,
	CAP_PCM_RATE_96000		= 0x0000000100000000ULL,
	CAP_PCM_RATE_MASK		= ( CAP_PCM_RATE_CONTINUOUS | CAP_PCM_RATE_8000 | CAP_PCM_RATE_11025 |
								CAP_PCM_RATE_12000 | CAP_PCM_RATE_16000 | CAP_PCM_RATE_22050 |
								CAP_PCM_RATE_24000 | CAP_PCM_RATE_32000 | CAP_PCM_RATE_44100 |
								CAP_PCM_RATE_48000 | CAP_PCM_RATE_88200 | CAP_PCM_RATE_96000)
};

// AC97_EXTENDED_ID bits
enum {
	EXID_VRA 	= 0x0001,
	EXID_DRA 	= 0x0002,
	EXID_SPDIF 	= 0x0004,
	EXID_VRM 	= 0x0008,
	EXID_DSA0 	= 0x0010,
	EXID_DSA1 	= 0x0020,
	EXID_CDAC 	= 0x0040,
	EXID_SDAC 	= 0x0080,
	EXID_LDAC 	= 0x0100,
	EXID_AMAP 	= 0x0200,
	EXID_REV0 	= 0x0400,
	EXID_REV1 	= 0x0800,
	EXID_bit12 	= 0x1000,
	EXID_bit13 	= 0x2000,
	EXID_ID0 	= 0x4000,
	EXID_ID1 	= 0x8000
};

enum AC97_REGISTER {
	/* Baseline audio register set */
	AC97_RESET				= 0x00,
	AC97_MASTER_VOLUME		= 0x02,
	AC97_AUX_OUT_VOLUME		= 0x04,
	AC97_MONO_VOLUME		= 0x06,
	AC97_MASTER_TONE		= 0x08,
	AC97_PC_BEEP_VOLUME		= 0x0A,
	AC97_PHONE_VOLUME		= 0x0C,
	AC97_MIC_VOLUME			= 0x0E,
	AC97_LINE_IN_VOLUME		= 0x10,
	AC97_CD_VOLUME			= 0x12,
	AC97_VIDEO_VOLUME		= 0x14,
	AC97_AUX_IN_VOLUME		= 0x16,
	AC97_PCM_OUT_VOLUME		= 0x18,
	AC97_RECORD_SELECT		= 0x1A,
	AC97_RECORD_GAIN		= 0x1C,
	AC97_RECORD_GAIN_MIC	= 0x1E,
	AC97_GENERAL_PURPOSE	= 0x20,
	AC97_3D_CONTROL			= 0x22,
	AC97_PAGING				= 0x24,
	AC97_POWERDOWN			= 0x26,
	
	/* Extended audio register set */
	AC97_EXTENDED_ID		= 0x28,
	AC97_EXTENDED_STAT_CTRL = 0x2A,
	AC97_PCM_FRONT_DAC_RATE = 0x2C,
	AC97_PCM_SURR_DAC_RATE	= 0x2E,
	AC97_PCM_LFE_DAC_RATE	= 0x30,
	AC97_PCM_L_R_ADC_RATE	= 0x32,
	AC97_MIC_ADC_RATE		= 0x34,
	AC97_CENTER_LFE_VOLUME	= 0x36,
	AC97_SURR_VOLUME		= 0x38,
	AC97_SPDIF_CONTROL		= 0x3A,

	/* Vendor ID */
	AC97_VENDOR_ID1			= 0x7C,
	AC97_VENDOR_ID2			= 0x7E,
	
	/* Analog Devices */
	AC97_AD_JACK_SENSE		= 0x72,
	AC97_AD_SERIAL_CONFIG	= 0x74,
	AC97_AD_MISC_CONTROL	= 0x76,
	AC97_AD_SAMPLE_RATE_0	= 0x78,
	AC97_AD_SAMPLE_RATE_1	= 0x7a,
	
	/* Realtek ALC650 */
	AC97_ALC650_SPDIF_INPUT_CHAN_STATUS_LO = 0x60, /* only ALC650 Rev. E and later */
	AC97_ALC650_SPDIF_INPUT_CHAN_STATUS_HI = 0x62, /* only ALC650 Rev. E and later */
	AC97_ALC650_SURR_VOLUME		= 0x64,
	AC97_ALC650_CEN_LFE_VOLUME	= 0x66,
	AC97_ALC650_MULTI_CHAN_CTRL	= 0x6A,
	AC97_ALC650_MISC_CONTROL	= 0x74,
	AC97_ALC650_GPIO_SETUP		= 0x76,
	AC97_ALC650_GPIO_STATUS		= 0x78,
	AC97_ALC650_CLOCK_SOURCE	= 0x7A
};



struct device_item_s device_list[] = {
	{ 0x8086, 0x7195, TYPE_DEFAULT, "Intel 82443MX" },
	{ 0x8086, 0x2415, TYPE_DEFAULT, "Intel 82801AA (ICH)" },
	{ 0x8086, 0x2425, TYPE_DEFAULT, "Intel 82801AB (ICH0)" },
	{ 0x8086, 0x2445, TYPE_DEFAULT, "Intel 82801BA (ICH2), Intel 82801BAM (ICH2-M)" },
	{ 0x8086, 0x2485, TYPE_DEFAULT, "Intel 82801CA (ICH3-S), Intel 82801CAM (ICH3-M)" },
	{ 0x8086, 0x24C5, TYPE_ICH4,	"Intel 82801DB (ICH4)" },
	{ 0x8086, 0x24D5, TYPE_ICH4,	"Intel 82801EB (ICH5), Intel 82801ER (ICH5R)" },
	{ 0x8086, 0x266E, TYPE_ICH4,	"Intel 82801FB/FR/FW/FRW (ICH6)" },
	{ 0x8086, 0x27DE, TYPE_ICH4,	"Intel unknown (ICH7)" },
	{ 0x8086, 0x2698, TYPE_ICH4,	"Intel unknown (ESB2)" },
	{ 0x8086, 0x25A6, TYPE_ICH4,	"Intel unknown (ESB5)" },
	{ 0x1039, 0x7012, TYPE_SIS7012,	"SiS SI7012" },
	{ 0x10DE, 0x01B1, TYPE_DEFAULT,	"NVIDIA nForce (MCP)" },
	{ 0x10DE, 0x006A, TYPE_DEFAULT, "NVIDIA nForce 2 (MCP2)" },
	{ 0x10DE, 0x00DA, TYPE_DEFAULT,	"NVIDIA nForce 3 (MCP3)" },
	{ 0x10DE, 0x003A, TYPE_DEFAULT,	"NVIDIA unknown (MCP04)" },
	{ 0x10DE, 0x0059, TYPE_DEFAULT,	"NVIDIA unknown (CK804)" },
	{ 0x10DE, 0x008A, TYPE_DEFAULT,	"NVIDIA unknown (CK8)" },
	{ 0x10DE, 0x00EA, TYPE_DEFAULT,	"NVIDIA unknown (CK8S)" },
	{ 0x1022, 0x746D, TYPE_DEFAULT, "AMD AMD8111" },
	{ 0x1022, 0x7445, TYPE_DEFAULT, "AMD AMD768" },
//	{ 0x10B9, 0x5455, TYPE_DEFAULT, "Ali 5455" }, not yet supported
	{ 0x0000, 0x0000, 0x0000,0x00000000},
};



struct ich_chan_s		channels[3]								=	{0xFF};
device_config			c										=	{0xff};
device_config			*_config								=	&c;
bus_driver				*bus_drv	=	PTR_INVALID;
unsigned int			stream_dev_id = 0xFFFFFFFF;

DRIVER_FUNC unsigned int C_API_FUNC pci_get_device_cmd_flag		(unsigned short vendor_id ,unsigned short device_id);
DRIVER_FUNC status_t	 C_API_FUNC probe_device				(pci_device *device);
DRIVER_FUNC status_t	 C_API_FUNC init_driver					();
DRIVER_FUNC void		 C_API_FUNC start_stream				(unsigned int stream_idx);


OS_API_C_FUNC(unsigned int ) pci_get_device_cmd_flag (unsigned short vendor_id ,unsigned short device_id)
{
	struct	device_item_s *dev_list;

	for (dev_list = device_list; dev_list->vendor_id!=0; dev_list ++) 
	{
		unsigned short d_id,v_id;
		unsigned char	type;

		v_id	=dev_list->vendor_id;
		d_id	=dev_list->device_id;
		type	=dev_list->type;

		if (v_id ==vendor_id && d_id == device_id) 
		{
			if (type & TYPE_ICH4)
				return PCI_PCICMD_MSE | PCI_PCICMD_BME;
			else
				return PCI_PCICMD_IOS | PCI_PCICMD_BME;
		}
	}

	return 0;
	
}

OS_API_C_FUNC(status_t ) probe_device	(pci_device *device)
{
	struct	device_item_s	*dev_list;
	status_t				result;
	unsigned int			conf_type;
	pci_dev_config			*pciinfo;
	
	pciinfo		=	&device->config;
	
	bus_drv = bus_manager_find_bus_driver(device->bus_id);

	result = 1;

	for (dev_list = device_list; dev_list->vendor_id!=0; dev_list ++) 
	{
		unsigned short d_id,v_id;
		unsigned char	type;

		v_id	=dev_list->vendor_id;
		d_id	=dev_list->device_id;
		type	=dev_list->type;


	//for (i = 0; device_list[i].vendor_id; i++) {
		if (v_id == pciinfo->vendor_id && 
			d_id == pciinfo->device_id) 
		{
			strcpy_s(device->name,128,dev_list->name);
			_config->sub_type	=type;
			writestr(" name ");
			writestr(device->name);
			writestr(" type ");
			writeint(_config->sub_type,10);
			writestr("\n");

			conf_type	=	type;
			goto probe_ok;
		}
	}
	
	result = -1;
	goto probe_done;

probe_ok:

	// initialize whole config to 0
	memset_c(_config, 0, sizeof(device_config));

	/* read memory-io and port-io bars_
	 */

	_config->nambar		= *((unsigned int *)(&pciinfo->base_addr_regs[0x00]));//read_pci_dword_c	(bus, device, function, 0x10);
	_config->nabmbar	= *((unsigned int *)(&pciinfo->base_addr_regs[0x04]));//read_pci_dword_c	(bus, device, function, 0x14);
	_config->mmbar		= *((unsigned int *)(&pciinfo->base_addr_regs[0x08]));//read_pci_dword_c	(bus, device, function, 0x18);
	_config->mbbar		= *((unsigned int *)(&pciinfo->base_addr_regs[0x0C]));//read_pci_dword_c	(bus, device, function, 0x1C);
	
	_config->nambar    &= PCI_address_io_mask;
	_config->nabmbar   &= PCI_address_io_mask;
	_config->mmbar     &= PCI_address_memory_32_mask;
	_config->mbbar     &= PCI_address_memory_32_mask;       
	
	_config->pci_dev_id	= device->dev_id;	
	_config->pci_bus_id	= device->bus_id;

	/*
	if (_config->irq == 0 || _config->irq == 0xff) {
		writestr("WARNING: no interrupt configured\n");
		device->irq = 0;
	}
	else
	{
		writestr(" interupt : ");
		writeint(_config->irq,10);
		writestr("\n");
	}
	*/

	/* the ICH4 uses memory mapped IO */
	if ((_config->sub_type & TYPE_ICH4) && ((_config->mmbar == 0) || (_config->mbbar == 0))) {
		
		writestr			("ERROR: memory mapped IO not configured\n");
		result				= -1;
		_config->mmbar		=	PTR_NULL;

		_config->log_mbbar	=	uint_to_mem(_config->mmbar);
		
	}
	else
	{
		writestr(" mmbar : ");
		writeint(_config->mmbar,16);
		writestr(" ");
		writeint(_config->mbbar,16);
		writestr("\n");

		_config->log_mbbar	=	uint_to_mem(_config->mbbar);
		_config->log_mmbar	=	uint_to_mem(_config->mmbar);
	}


	/* all other ICHs use programmed IO */
	if (!(_config->sub_type & TYPE_ICH4) && ((_config->nambar == 0) || (_config->nabmbar == 0))) {
		writestr("ERROR: IO space not configured\n");
		result = -1;
	}
	else
	{
		writestr(" nambar : ");
		writeint(_config->nambar,16);
		writestr(" ");
		writeint(_config->nabmbar,16);
		writestr("\n");


	}
probe_done:
	return result;
}


status_t ich_codec_wait();


void pci_write_io_8(unsigned short port,unsigned char value)
{
	out_8_c	(port,value);
}
void pci_write_io_16(unsigned short port,unsigned short value)
{
	out_16_c	(port,value);
}

void pci_write_io_32(unsigned short port,unsigned int value)
{
	out_32_c	(port,value);
}


unsigned char pci_read_io_8(unsigned short port)
{
	return in_8_c	(port);
}
unsigned short pci_read_io_16(unsigned short port)
{
	return in_16_c	(port);
}

unsigned int pci_read_io_32(unsigned short port)
{
	unsigned int value;
	value		=	in_32_c	(port);
	return value;
}


#define ASSERT(a) 

unsigned char ich_reg_read_8(int regno)
{
	if (_config->sub_type & TYPE_ICH4) 
		return *(unsigned char *)(((char *)_config->log_mbbar) + regno);
	else
		return pci_read_io_8(_config->nabmbar + regno);
}

unsigned short ich_reg_read_16(int regno)
{
	if (_config->sub_type & TYPE_ICH4) 
		return *(unsigned short *)(((char *)_config->log_mbbar) + regno);
	else
		return pci_read_io_16(_config->nabmbar + regno);
}

unsigned int ich_reg_read_32(int regno)
{
	if (_config->sub_type & TYPE_ICH4) 
		return *(unsigned int *)(((char *)_config->log_mbbar) + regno);
	else
		return pci_read_io_32(_config->nabmbar + regno);
}


void ich_reg_write_8(int regno, unsigned char value)
{
	if (_config->sub_type & TYPE_ICH4) 
		*(unsigned char *)(((char *)_config->log_mbbar) + regno) = value;
	else
		pci_write_io_8(_config->nabmbar + regno, value);
}

void ich_reg_write_16(int regno, unsigned short value)
{
	if (_config->sub_type & TYPE_ICH4) 
		*(unsigned short *)(((char *)_config->log_mbbar) + regno) = value;
	else
		pci_write_io_16(_config->nabmbar + regno, value);
}

void ich_reg_write_32(int regno, unsigned int value)
{
	if (_config->sub_type & TYPE_ICH4) 
		*(unsigned int *)(((char *)_config->log_mbbar) + regno) = value;
	else
		pci_write_io_32(_config->nabmbar + regno, value);
}



status_t ich_codec_wait()
{
	int i;
	for (i = 0; i < 1100; i++) {
		if ((ich_reg_read_8(ICH_REG_ACC_SEMA) & 0x01) == 0)
			return 0;
		if (i > 100)
			delay1_4ms_c();
	}
	writestr("ich codec wait failled");
	return -1;
}

unsigned short ich_codec_read(int regno)
{
	status_t rv;

	if (regno == 0x54) // intel uses 0x54 for GPIO access, we filter it!
		return 0;

	rv = ich_codec_wait();

	if (rv != 0)	return -1;

	if (_config->sub_type & TYPE_ICH4) 
		return *(unsigned short *)(((char *)_config->log_mmbar) + regno);
	else
		return pci_read_io_16(_config->nambar + regno);
}

void ich_codec_write(int regno, unsigned short value)
{
	status_t rv;

	if (regno == 0x54) 	return;
	
	rv = ich_codec_wait();
	
	if (rv != 0)	return ;

	if (_config->sub_type & TYPE_ICH4) 
		*(unsigned short *)(((char *)_config->log_mmbar) + regno) = value;
	else
		pci_write_io_16(_config->nambar + regno, value);
}

void ac97_amp_enable(int yesno)
{
	unsigned int status;

	unsigned int i;
	_config->reversed_eamp_polarity=0;
	


	_config->ac97_codec_id = (ich_codec_read(AC97_VENDOR_ID1) << 16) | ich_codec_read(AC97_VENDOR_ID2);

	writestr("AC97 codec ID ");
	writeint(_config->ac97_codec_id , 16);
	writestr("\n");

	switch (_config->ac97_codec_id) {	
		case CODEC_ID_CS4299A:
		case CODEC_ID_CS4299C:
		case CODEC_ID_CS4299D:
			if (yesno)
				ich_codec_write(0x68, 0x8004);
			else
				ich_codec_write(0x68, 0);
			break;
		
		default:

			if (_config->reversed_eamp_polarity)
				yesno = !yesno;

			if(yesno)
				status = 0x8000;
			else
				status = 0;

			ich_codec_write(AC97_POWERDOWN, status);
			ich_codec_write(AC97_RESET, 0);
			snooze(100000);
			ich_codec_write(AC97_POWERDOWN, status);
			i = ich_codec_read(AC97_RESET);

			/*
			status = ich_codec_read(dev,AC97_POWERDOWN);

			if (yesno)
				ich_codec_write(dev,AC97_POWERDOWN,  status  & ~0x8000); // switch on (low active) 
			else
				ich_codec_write(dev,AC97_POWERDOWN,	 status  | 0x8000); // switch off 

			status = ich_codec_read(dev,AC97_POWERDOWN);
			*/

		break;
	}
}


void ich_setup_mixer()
{

	ich_codec_write	(AC97_MASTER_VOLUME		,0x0F0F);
	ich_codec_write	(AC97_AUX_OUT_VOLUME	,0x0F0F);
	ich_codec_write	(AC97_PHONE_VOLUME		,0x0F0F);
	ich_codec_write	(AC97_PCM_OUT_VOLUME	,0x0F0F);
	ich_codec_write	(AC97_RECORD_SELECT		,0x0404);
	ich_codec_write	(AC97_RECORD_GAIN		,0x8a06);
}


/* -------------------------------------------------------------------- */
/* The interrupt handler */
OS_INT_C_FUNC(unsigned int) _interupt_handler(void *param)
{
	unsigned int		civ,st, gs;
	struct ich_chan_s	*ch;

	
	gs				= ich_reg_read_32(ICH_REG_GLOB_STA) & STA_INTMASK;
	
	if (gs & (STA_PRES | STA_SRES)) {
		// Clear resume interrupt(s) - nothing doing with them 
		ich_reg_write_32(ICH_REG_GLOB_STA, gs);
	}
	gs &= ~(STA_PRES | STA_SRES);

	/*
	writestr("gs : ");
	writeint(gs,16);
	writestr("\n");
	*/
	

	if (gs & STA_POINT)
	{
		

		ch	= &channels[0];
		gs &= ~STA_POINT;
		st = ich_reg_read_16(ch->regbase + GET_REG_X_SR(_config));
		st &= SR_FIFOE | SR_BCIS | SR_LVBCI;



		if (st & (SR_BCIS | SR_LVBCI)) 
		{
			mem_size		zone_ofset,zone_size;
			unsigned int	bd_idx;
			struct ich_bd_s *bd_ptr;
			
			
			
			
			civ				=	ich_reg_read_8(ch->regbase + ICH_REG_X_CIV) ;
			bd_idx			=	(civ+1)%ICH_BD_COUNT;
			

			//zone_size		=	ch->ich_bd_array[bd_idx].length*GET_HW_SAMPLE_SIZE(_config);
			
			bd_ptr			=	&ch->ich_bd_array[bd_idx];
			zone_size		=	bd_ptr->length*GET_HW_SAMPLE_SIZE(_config);
			zone_ofset		=	((civ+1)%BUFFER_COUNT)*BUFFER_SIZE;

			async_stream_manager_set_dma_pos	(ch->stream_idx,zone_ofset);
			
			stream_dev_request_write_stream		(stream_dev_id,	ch->stream_idx,zone_size);
			
			ich_reg_write_8						(ch->regbase + ICH_REG_X_LVI,bd_idx);
		

		}
		// clear status bit 
		ich_reg_write_16(ch->regbase + GET_REG_X_SR(_config), st);
	}

	return 0;
}



static int ich_resetchan(int num)
{
	int					i, cr;
	struct ich_chan_s	*chan	=&channels[num];

	
	if(num==0)
		chan->regbase=ICH_REG_PO_BASE;
	else if(num==1)
		chan->regbase=ICH_REG_PI_BASE;
	else if(num==2)
		chan->regbase=ICH_REG_MC_BASE;
	else
		return -1;
	

	//chan->regbase=0;

	ich_reg_write_8		(chan->regbase + ICH_REG_X_CR, 0);
	snooze				(100000);
	ich_reg_write_8		(chan->regbase + ICH_REG_X_CR, CR_RR);


	for (i = 0; i < 1000; i++) {
		cr = ich_reg_read_8(chan->regbase + ICH_REG_X_CR);
		if (cr == 0)
		{
			writestr("channel [");
			writeint(num,10);
			writestr("] ");
			writeint(chan->regbase,16);
			writestr(" reseted ");
			chan->stream_idx=0xFFFFFFFF;
			return 0;
		}
		snooze		(1000);
	}

	writestr("cannot reset channel ");
	writeint(num,16);
	writestr(" ");
	
	return -1;
}

int init_chan(unsigned int str_dev_idx,unsigned int num)
{
	int					i;
	mem_ptr				buffer_base;
	unsigned int		new_stream_idx;
	struct ich_chan_s	*chan;
	stream_device		*str_dev;
	struct ich_bd_s		*bd_ptr;

	chan					=	&channels[num];
	str_dev					=  find_stream_device					(str_dev_idx);

	new_stream_idx			=	stream_dev_new_stream				(str_dev,BUFFER_SIZE*BUFFER_COUNT);
	chan->stream_idx		=	new_stream_idx;
	chan->running			= 	0;

	buffer_base				=	async_stream_manager_get_dma_zone	(new_stream_idx);

	writestr("dma zone :");
	writeptr(buffer_base);
	writestr("\n");

	bd_ptr=chan->ich_bd_array;


	for (i = 0; i < ICH_BD_COUNT; i++) {
		
		bd_ptr->buffer		= (mem_to_uint(buffer_base)) + (i % BUFFER_COUNT) * BUFFER_SIZE;
		bd_ptr->length		= BUFFER_SIZE / GET_HW_SAMPLE_SIZE(_config);
		bd_ptr->flags		= ICH_BDC_FLAG_IOC;

		bd_ptr++;
	}


	// set physical buffer descriptor base address
	ich_reg_write_32	(chan->regbase+ICH_REG_X_BDBAR, mem_to_uint(chan->ich_bd_array));

	return 1;
}


OS_API_C_FUNC(void) start_stream(unsigned int stream_idx)
{
	int			 civ;
	struct		 ich_chan_s	*chan,*chans_ptr;
	unsigned int chan_idx;
	unsigned int n;
	

	chan		=	PTR_NULL;
	chans_ptr	=	channels;
	chan_idx	=   0xFFFFFFFF;

	n			=   0;
	while(n<3)
	{
		if(chans_ptr->stream_idx	==	stream_idx)
		{
			chan		=	chans_ptr;
			chan_idx	=	n;
			break;
		}

		chans_ptr++;
		n++;
	}
	
	if(chan_idx==0xFFFFFFFF)return;
	if(chan==PTR_NULL)return;
	if (chan->running)return;

	
	/*
	
	chan_idx	=   0xFFFFFFFF;

	
	n			=   0;
	while(n<3)
	{
		if(chans_ptr->stream_idx	==	stream_idx)
			chan_idx	=	n;

		chans_ptr++;
		n++;
	}
	
	
	chan=&channels[chan_idx];
	if (chan->running)return;
	*/


	writestr("starting channel [");
	writeint(chan_idx,10);
	writestr("] stream ");
	writeint(stream_idx,10);
	writestr("\n");



	// step 1: clear status bits
	ich_reg_write_16	(chan->regbase + GET_REG_X_SR(_config), SR_FIFOE | SR_BCIS | SR_LVBCI); 
		
	// step 2: prepare buffer transfer
	civ				=	ich_reg_read_8(chan->regbase + ICH_REG_X_CIV);
	ich_reg_write_8		(chan->regbase + ICH_REG_X_LVI,(civ+1)%ICH_BD_COUNT);
	// step 3: enable interrupts & busmaster transfer
	ich_reg_write_8		(chan->regbase + ICH_REG_X_CR, CR_RPBM | CR_LVBIE | CR_IOCE );
	
	chan->running = 1;
}


void stop_chan(stream_device *dev,unsigned int num)
{
	struct ich_chan_s *chan=&channels[num];
	if(!chan->running)return;

	ich_reg_write_8	(chan->regbase + ICH_REG_X_CR, ich_reg_read_8(chan->regbase + ICH_REG_X_CR) & ~CR_RPBM);
	ich_reg_read_8	(chan->regbase + ICH_REG_X_CR); // force PCI-to-PCI bridge cache flush
	chan->running = 0;
	snooze(10000); // 10 ms
}




OS_API_C_FUNC(status_t) init_driver()
{
	unsigned int val,stat;
	unsigned int start;
	unsigned int irq;
	unsigned int s0cr, s1cr, s2cr;

	
	
	writestr("init_driver\n");
	writestr("reset starting, ICH_REG_GLOB_CNT = ");
	writeint(ich_reg_read_32(ICH_REG_GLOB_CNT),16);
	writestr("\n");
	
	// finish cold reset by writing a 1 and clear all other bits to 0
	
	ich_reg_write_32			(ICH_REG_GLOB_CNT, CNT_COLD);
	snooze						(600000);
	stat	=	ich_reg_read_32	(ICH_REG_GLOB_STA); // force PCI-to-PCI bridge cache flush
	snooze						(20000);

	if ((stat & STA_S0CR) == 0)
	{
		writestr("bad state register STA_S0CR = ");
		writeint(stat,16);
		writestr("\n");
		return -1;
	}
	
	ich_reg_write_32	(ICH_REG_GLOB_CNT, CNT_COLD | CNT_PRIE);
	snooze				(20000);

	if (ich_resetchan(0) || ich_resetchan(1) || ich_resetchan(2))
	{
		writestr("chan reset failed\n");
		return -1;
	}
	writestr("chan reset finished \n");

	/* detect which codecs are ready */
	s0cr = s1cr = s2cr = 0;
	start = system_time();
	do {
		val = ich_reg_read_32(ICH_REG_GLOB_STA);
		if (!s0cr && (val & STA_S0CR)) {
			s0cr = 1;
			writestr("AC_SDIN0 codec ready \n");
		}
		if (!s1cr && (val & STA_S1CR)) {
			s1cr = 1;
			writestr("AC_SDIN1 codec ready \n");
		}
		if (!s2cr && (val & STA_S2CR)) {
			s2cr = 1;
			writestr("AC_SDIN2 codec ready \n");
		}
		snooze(1000);
	} while ((system_time() - start) < 1000);

	if (!s0cr) {
		writestr(("AC_SDIN0 codec not ready\n"));
	}
	if (!s1cr) {
		writestr(("AC_SDIN1 codec not ready\n"));
	}
	if (!s2cr) {
		writestr(("AC_SDIN2 codec not ready\n"));
	}


	if (!s0cr && !s1cr && !s2cr) {
		writestr("compatible chipset found, but no codec ready!\n");
		return -1;
	}

	stat	=	ich_reg_read_32(ICH_REG_GLOB_STA);
	ich_reg_write_32(ICH_REG_GLOB_STA,stat|STA_POINT|STA_GSCI);	


	if (_config->sub_type & TYPE_ICH4) {
		/* we are using a ICH4 chipset, and assume that the codec beeing ready
		 * is the primary one.
		 */
		unsigned char sdin;
		unsigned short reset;
		unsigned char id;
		reset = ich_codec_read(0x00);	/* access the primary codec */
		if (reset == 0 || reset == 0xFFFF) {
			writestr(("primary codec not present\n"));
		} else {
			sdin = 0x02 & ich_reg_read_8(ICH_REG_SDM);
			id = 0x02 & (ich_codec_read(0x00 + 0x28) >> 14);
			writestr("primary codec id [XX] is connected to AC_SDIN [xxx]\n");//, id, sdin));
		}
		reset = ich_codec_read(0x80);	/* access the secondary codec */
		if (reset == 0 || reset == 0xFFFF) {
			writestr("secondary codec not present\n");
		} else {
			sdin = 0x02 & ich_reg_read_8(ICH_REG_SDM);
			id = 0x02 & (ich_codec_read(0x80 + 0x28) >> 14);
			writestr("secondary codec id [xx] is connected to AC_SDIN [xx]\n");//, id, sdin));
		}
		reset = ich_codec_read(0x100);	/* access the tertiary codec */
		if (reset == 0 || reset == 0xFFFF) {
			writestr("tertiary codec not present\n");
		} else {
			sdin = 0x02 & ich_reg_read_8(ICH_REG_SDM);
			id = 0x02 & (ich_codec_read(0x100 + 0x28) >> 14);
			writestr("tertiary codec id [xxx] is connected to AC_SDIN [xxx]\n");//, id, sdin));
		}
		
		/* XXX this may be wrong */
		ich_reg_write_8(ICH_REG_SDM, (ich_reg_read_8(ICH_REG_SDM) & 0x0F) | 0x08 | 0x90);
	} else {
		/* we are using a pre-ICH4 chipset, that has a fixed mapping of
		 * AC_SDIN0 = primary, AC_SDIN1 = secondary codec.
		 */
		if (!s0cr && s2cr) {
			// is is unknown if this really works, perhaps we should better abort here
			writestr("primary codec doesn't seem to be available, using secondary!\n");
			_config->codecoffset = 0x80;
		}
	}



	ac97_amp_enable		(1);
	ich_setup_mixer		();
	
	stream_dev_id		=	new_stream_device		(_config->pci_bus_id,_config->pci_dev_id);	
	
	/*
	get_bus_device_irq_func_ptr get_bus_device_irq;
	get_bus_device_irq = get_tpo_mod_exp_addr_name(&bus_drv->bus_tpo_file, "get_bus_device_irq");

	if (get_bus_device_irq != PTR_NULL)
	{
		irq = get_bus_device_irq(_config->pci_dev_id);

		writestr_fmt("ich get irw %d \n", irq);
	}
	else
	{
		irq = _config->irq;
	}
	*/

	/*setup_interupt_c	(irq, interupt_handler,0);*/
	init_chan			(stream_dev_id,0);
	//start_chan		(device,0);

	writestr			("init_driver finished!\n");
	return 1;
}


DRIVER_FUNC unsigned short devices_list[]= {0x8086, 0x7195,
										   0x8086, 0x2415,
										   0x8086, 0x2425,
										   0x8086, 0x2445,
										   0x8086, 0x2485,
										   0x8086, 0x24C5,
										   0x8086, 0x24D5,
										   0x8086, 0x266E,
										   0x8086, 0x27DE,
										   0x8086, 0x2698,
										   0x8086, 0x25A6,
										   0x1039, 0x7012,
										   0x10DE, 0x01B1,
										   0x10DE, 0x006A,
										   0x10DE, 0x00DA,
										   0x10DE, 0x003A,
										   0x10DE, 0x0059,
										   0x10DE, 0x008A,
										   0x10DE, 0x00EA,
										   0x1022, 0x746D,
										   0x1022, 0x7445,
										   0x10B9, 0x5455,
										   0x0000, 0x0000};


