#define APP_DLL_FUNC C_EXPORT
#include <std_def.h>
#include <std_mem.h>
#include "kern.h"
#include "mem_base.h"
#include "lib_c.h"
#include "tree.h"
#include "sys/async_stream.h"
#include "sys/stream_dev.h"
#include "sys/mem_stream.h"
#include "sys/file_system.h"
#include "sys/tpo_mod.h"
#include "sys/task.h"
#include "../../kernel/bus_manager/bus_drv.h"

#include "gfx/graphic_base.h"
#include "gfx_lib/scene.h"

#include <sys/ctrl.h>

mem_zone_ref	top_menu_node	={PTR_INVALID};
unsigned int	top_menu_id		=0xFFFFFFFF;
unsigned int 	menu_ctrl_id	=0xFFFFFFFF;
extern unsigned int 	sys_menu_kernel_log_id;


void create_bus_submenu(mem_zone_ref_ptr menu_node,unsigned int bus_id,const char *bus_name,unsigned int n_items)
{
	struct obj_array_t			menu;
	
	menu.char_buffer.zone	=	PTR_NULL;
	
	gfx_ctrl_new						(&menu,"{p_x:4,p_y:4,height:350,width:200,border:1}");
	gfx_ctrl_add_item					(&menu,"dev"		,"devices",1);
	tree_manager_add_obj_int_val		(&menu,"bus_id"		,bus_id);
	tree_manager_add_obj_str_val		(&menu,"bus_name"	,bus_name);

	gfx_ctrl_add_item					(&menu,"drv"		,"drivers",2);
	tree_manager_add_obj_int_val		(&menu,"bus_id"		,bus_id);
	tree_manager_add_obj_str_val		(&menu,"bus_name"	,bus_name);

	gfx_ctrl_create_object				(&menu,"menu",bus_name		,menu_node,n_items);

	tree_manager_free_obj_array			(&menu);

	
}
	

void create_bus_menu(mem_zone_ref_ptr menu_node,unsigned int n_items)
{
	char						bus_name[32];	
	struct obj_array_t			menu;
	mem_zone_ref				menu_entry		={PTR_NULL};
	mem_zone_ref				bus_menu_node	={PTR_NULL};
	unsigned int				n,bus_id;

	menu.char_buffer.zone	=	PTR_NULL;

	gfx_ctrl_new					(&menu,"{p_x:4,p_y:4,height:350,width:200,border:1}");

	n=0;
	while(bus_manager_get_bus_driver_name(n,bus_name,32))
	{
		bus_id=bus_manager_get_bus_driver_id	(n);

		gfx_ctrl_add_item				(&menu,bus_name,bus_name,n+1);
		tree_manager_add_obj_int_val	(&menu,"bus_id"		,bus_id);
		tree_manager_add_obj_str_val	(&menu,"bus_name"	,bus_name);
		n++;
	}
	gfx_ctrl_create_object			(&menu,"menu","bus list",menu_node,n_items);

	tree_manager_free_obj_array		(&menu);

	
}

void create_top_menu		(mem_zone_ref_ptr		sys_menu,unsigned int n_items)
{
	struct obj_array_t			menu_ctrl;
	
	menu_ctrl.char_buffer.zone	=	PTR_NULL;

	gfx_ctrl_new					(&menu_ctrl,"{p_x:4,p_y:4,height:450,width:200,border:1}");
	gfx_ctrl_add_item				(&menu_ctrl,"bus","bus",1);
	gfx_ctrl_add_item				(&menu_ctrl,"fs","fs",2);
	gfx_ctrl_create_object			(&menu_ctrl,"menu","sys menu",sys_menu,n_items);

	kernel_log						(sys_menu_kernel_log_id, get_zone_ptr(&menu_ctrl.char_buffer, 0));
	writestr						("\n");

	tree_manager_dump_node_rec		(sys_menu,0,3);

	tree_manager_free_obj_array		(&menu_ctrl);
}


OS_API_C_FUNC(int) init_app(mem_zone_ref_ptr	init_data)
{
	mem_zone_ref					new_node		={PTR_NULL};
	mem_zone_ref					bus_list_node	={PTR_NULL};
	mem_zone_ref					item_list		={PTR_NULL};
	mem_zone_ref					bus_entry		={PTR_NULL};
	mem_zone_ref					fs_menu_node	={PTR_NULL};
	mem_zone_ref					mem_menu_node	={PTR_NULL};
	unsigned int					menu_node_id	=0;
	unsigned int					n;
	unsigned int					sub_menu_node_id=0;
	unsigned int					dev_node_id		=0;


	sys_menu_kernel_log_id = get_new_kern_log_id("top menu", 0x02);

	top_menu_node.zone=PTR_NULL;
	menu_node_id			=0;
	sub_menu_node_id		=0;

	create_top_menu						(&top_menu_node,8);
	create_bus_menu						(&bus_list_node,8);	
	

	tree_node_find_child_by_type		(&bus_list_node,NODE_GFX_CTRL_ITEM_LIST,&item_list,0);
	
	
	n=0;
	while(tree_manager_get_child_at	(&item_list,n,&bus_entry))
	{
		unsigned int							bus_id;
		unsigned int							item_id;
		char									bus_name[32];
		mem_zone_ref							my_devs={PTR_NULL};
		mem_zone_ref							bus_menu_node={PTR_NULL};

		tree_manager_get_child_value_i32		(&bus_entry,NODE_HASH("bus_id")  ,&bus_id);
		tree_manager_get_child_value_str		(&bus_entry,NODE_HASH("bus_name"),bus_name,32,0);
		tree_manager_get_child_value_i32		(&bus_entry,NODE_HASH("item_id") ,&item_id);

		create_bus_submenu						(&bus_menu_node,bus_id,bus_name,8);

		bus_manager_get_bus_dev_tree			(bus_id ,&my_devs);

		gfx_ctrl_add_item_data					(&bus_menu_node,1,&my_devs);
		release_zone_ref						(&my_devs);

		gfx_ctrl_add_item_data					(&bus_list_node,item_id,&bus_menu_node);
		release_zone_ref						(&bus_menu_node);
		
		release_zone_ref						(&bus_entry);
		n++;
	}
	release_zone_ref						(&item_list);
	
	
	tree_manager_create_node			("fsroot"		,NODE_FILE_SYSTEM_LIST,&fs_menu_node);
	
	gfx_ctrl_add_item_data				(&top_menu_node,1,&bus_list_node);
	gfx_ctrl_add_item_data				(&top_menu_node,2,&fs_menu_node);
	release_zone_ref					(&fs_menu_node);
	release_zone_ref					(&bus_list_node);


	top_menu_id		=	gfx_create_render_container			("top menu"	 ,30,30,400,200);
	menu_ctrl_id	=	gfx_container_add_ctrl				(top_menu_id,&top_menu_node);

	while(1)
	{

		gfx_process_events();
	}


	return 1;
}