#define APP_DLL_FUNC C_EXPORT
#include <std_def.h>
#include <std_mem.h>
#include "kern.h"
#include "mem_base.h"
#include "lib_c.h"
#include "tree.h"
#include "sys/async_stream.h"
#include "sys/stream_dev.h"
#include "sys/mem_stream.h"
#include "sys/file_system.h"
#include "sys/tpo_mod.h"
#include "sys/task.h"
#include "../../kernel/bus_manager/bus_drv.h"

#include "gfx/graphic_base.h"
#include "gfx_lib/scene.h"

#include <sys/ctrl.h>

mem_zone_ref					fs_menu_node = { PTR_INVALID };
unsigned int	fs_menu_id		=0xFFFFFFFF;
unsigned int 	menu_ctrl_id	=0xFFFFFFFF;
extern unsigned int 	sys_menu_kernel_log_id;

OS_API_C_FUNC(int) init_app(mem_zone_ref_ptr	init_data)
{
	struct obj_array_t				ctrl_items;
	unsigned int					n;
	file_system						*fs;

	sys_menu_kernel_log_id = get_new_kern_log_id("filesystem menu", 0x02);

	gfx_ctrl_new(&ctrl_items, "{p_x:4,p_y:4,width:380,height:150,border:1, item_height:16}");
	fs_menu_node.zone = PTR_NULL;
	n = 0;
	while ((fs = file_system_get_at(n)) != PTR_NULL)
	{
		const char *name;
		name = tree_mamanger_get_node_name(&fs->root);
		gfx_ctrl_add_item(&ctrl_items, name, name, n + 1);
		n++;
	}

	if (gfx_ctrl_create_object(&ctrl_items, "menu", "filesystem list", &fs_menu_node, 4))
	{
		n = 0;
		while ((fs = file_system_get_at(n)) != PTR_NULL)
		{
			mem_zone_ref item_data = { PTR_NULL };
			if (tree_manager_create_node("filesystem", NODE_FILE_SYSTEM_PATH, &item_data))
			{
				tree_manager_set_child_value_str(&item_data, "fs name", tree_mamanger_get_node_name(&fs->root));
				tree_manager_set_child_value_str(&item_data, "path", "/");
				gfx_ctrl_add_item_data(&fs_menu_node, n + 1, &item_data);
				release_zone_ref(&item_data);
			}
			n++;
		}
		tree_manager_set_child_value_i32(&fs_menu_node, "first_items", 0);
		tree_manager_set_child_value_i32(&fs_menu_node, "total_items", n);
		tree_manager_set_child_value_i32(&fs_menu_node, "data_type", NODE_FILE_SYSTEM_LIST);
	}

	fs_menu_id		=	gfx_create_render_container		("filesystem menu" ,30,30,400,200);
	menu_ctrl_id	=	gfx_container_add_ctrl			(fs_menu_id,&fs_menu_node);

	release_zone_ref(&fs_menu_node);


	while(1)
	{

		gfx_process_events();
	}


	return 1;
}