#define APP_DLL_FUNC C_EXPORT
#include <std_def.h>
#include <std_mem.h>
#include "kern.h"
#include "mem_base.h"
#include "lib_c.h"
#include "tree.h"
#include "sys/async_stream.h"
#include "sys/stream_dev.h"
#include "sys/mem_stream.h"
#include "sys/file_system.h"
#include "sys/tpo_mod.h"
#include "sys/task.h"
#include "../../kernel/bus_manager/bus_drv.h"

#include "gfx/graphic_base.h"
#include "gfx_lib/scene.h"

#include <sys/ctrl.h>

unsigned int	kern_log_win_id = 0xFFFFFFFF;
unsigned int 	kern_txt_list_id = 0xFFFFFFFF;
unsigned int 	time_used = 0xFFFFFFFF;

unsigned int 					num_lines = 0xFFFFFFFF;
void update_log(unsigned int kern_log_win_id)
{
	unsigned int 	text_pos_x, text_pos_y;
	unsigned int 	new_txt_list_id;
	unsigned int 	n, len;
	const short	 	*line_txt;
	unsigned int 	end_line, num_lines;



	text_pos_y = 12;
	text_pos_x = 12;


	new_txt_list_id = gfx_container_add_text_list(kern_log_win_id, "{p_x:12,p_y:12,font_name:\"Flama\",font_size_x:12,font_size_y:12,visible:0}");

	if (new_txt_list_id > 0)
	{
		num_lines = get_num_output_buffer_line_c();
		n = num_lines - 50;
		end_line = num_lines;

		while ((n < end_line) && (line_txt = get_output_buffer_line_c(n, &len)) != PTR_NULL)
		{
			char		 text[1024];
			if (len > 0)
			{
				if (len > 1023)len = 1023;

				text[len] = 0;

				while (len--)
				{
					if (line_txt[len] == '"')
						text[len] = '\'';
					else
						text[len] = (line_txt[len] & 0xFF);
				}


				gfx_container_add_text_to_list(kern_log_win_id, new_txt_list_id, text, "{p_x:0,bk_color:0,text_color:0xFF0000FF}");


			}
			text_pos_y += 12;
			n++;
		}
	}


	if (kern_txt_list_id != 0xFFFFFFFF)
		gfx_container_rem_obj(kern_log_win_id, kern_txt_list_id);

	kern_txt_list_id = new_txt_list_id;

	gfx_container_add_obj_style(kern_log_win_id, kern_txt_list_id, "{visible:1}");

	//gfx_dump_container								(kern_log_win_id);
	/*
	unsigned int 	w,h;
	struct gfx_rect	win_rect;

	gfx_compute_container					(kern_log_win_id,vga_dev_render_id);
	gfx_render_containers_get_extent		(kern_log_win_id,&win_rect);
	gfx_render_containers_get_size			(kern_log_win_id,&w,&h);

	if(win_rect.size[1]>h)
		gfx_render_containers_set_scroll_y		(kern_log_win_id,0,(win_rect.size[1]-h+24));
	else
		gfx_render_containers_set_scroll_y		(kern_log_win_id,0,0);
	*/

}

OS_API_C_FUNC(int) init_app(mem_zone_ref_ptr	init_data)
{
	
	kern_log_win_id = gfx_create_render_container("kernel log", 300, 16, 400, 600);

	gfx_container_add_image(kern_log_win_id, 10, 10, "isodisk", "/system/imgs/image.png");
	update_log(kern_log_win_id);


	num_lines = 0;

	while(1)
	{
		if (num_lines < get_num_output_buffer_line_c())
		{
			update_log(kern_log_win_id);
			num_lines = get_num_output_buffer_line_c();
		}

		gfx_process_events();
	}


	return 1;
}