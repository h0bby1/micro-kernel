


#include <sys/acpi/acpi.h>
#include <sys/acpi/aclocal.h>
#include <sys/acpi/acobject.h>
#include <sys/acpi/acstruct.h>
#include <sys/acpi/acnamesp.h>
#include <sys/acpi/acglobal.h>
#include <sys/acpi/acpixf.h>


#include <std_def.h>
#include <std_mem.h>
#include <mem_base.h>
#include <std_str.h>
#include <tree.h>

#include <sys/mem_stream.h>
#include <sys/tpo_mod.h>
#include <sys/acpi.h>
#include <sys/task.h>

#include "../../kernel/bus_manager/bus_drv.h"

extern unsigned int kernel_log_id;
extern unsigned int interupt_mode;

struct RSDPDescriptor {
 char			Signature[8];
 unsigned char	Checksum;
 char			OEMID[6];
 unsigned char	Revision;
 unsigned int	RsdtAddress;
};

struct RSDPDescriptor20 {
 char				Signature[8];
 unsigned char		Checksum;
 char				OEMID[6];
 unsigned char		Revision;
 unsigned int		RsdtAddress;
 unsigned int		Length;
 unsigned int		XsdtAddress;
 unsigned int		XsdtAddress_hi;
 unsigned char		ExtendedChecksum;
 unsigned char		reserved[3];
};

struct ACPISDTHeader {
  char				Signature[4];
  unsigned int		Length;
  unsigned char		Revision;
  unsigned char		Checksum;
  char				OEMID[6];
  char				OEMTableID[8];
  unsigned int		OEMRevision;
  unsigned int		CreatorID;
  unsigned int		CreatorRevision;
};

struct GenericAddressStructure
{
  unsigned char		AddressSpace;
  unsigned char		BitWidth;
  unsigned char		BitOffset;
  unsigned char		AccessSize;
  unsigned int		Address;
  unsigned int		Address_hi;
};

struct FADT
{
    struct   		ACPISDTHeader h;
    unsigned int	FirmwareCtrl;
    unsigned int	Dsdt;
 
    // field 		used in ACPI 1.0; no longer in use, for compatibility only
    unsigned char	Reserved;
 
    unsigned char	PreferredPowerManagementProfile;
    unsigned short	SCI_Interrupt;
    unsigned int	SMI_CommandPort;
    unsigned char	AcpiEnable;
    unsigned char	AcpiDisable;
    unsigned char	S4BIOS_REQ;
    unsigned char	PSTATE_Control;
    unsigned int	PM1aEventBlock;
    unsigned int	PM1bEventBlock;
    unsigned int	PM1aControlBlock;
    unsigned int	PM1bControlBlock;
    unsigned int	PM2ControlBlock;
    unsigned int	PMTimerBlock;
    unsigned int	GPE0Block;
    unsigned int	GPE1Block;
    unsigned char	PM1EventLength;
    unsigned char	PM1ControlLength;
    unsigned char	PM2ControlLength;
    unsigned char	PMTimerLength;
    unsigned char	GPE0Length;
    unsigned char	GPE1Length;
    unsigned char	GPE1Base;
    unsigned char	CStateControl;
    unsigned short	WorstC2Latency;
    unsigned short	WorstC3Latency;
    unsigned short	FlushSize;
    unsigned short	FlushStride;
    unsigned char	DutyOffset;
    unsigned char	DutyWidth;
    unsigned char	DayAlarm;
    unsigned char	MonthAlarm;
    unsigned char	Century;
 
    // reserv		ed in ACPI 1.0; used since ACPI 2.0+
    unsigned short	BootArchitectureFlags;
 
    unsigned char	Reserved2;
    unsigned int	Flags;
 
    // 12 byte structure; see below for details
    struct GenericAddressStructure ResetReg;
 
    unsigned char	ResetValue;
    unsigned char	Reserved3[3];
 
    // 64bit pointers - Available on ACPI 2.0+
    unsigned int			X_FirmwareControl;
	unsigned int			X_FirmwareControl_hi;
    unsigned int			X_Dsdt;
	unsigned int			X_Dsdt_hi;
 
    struct GenericAddressStructure X_PM1aEventBlock;
    struct GenericAddressStructure X_PM1bEventBlock;
    struct GenericAddressStructure X_PM1aControlBlock;
    struct GenericAddressStructure X_PM1bControlBlock;
    struct GenericAddressStructure X_PM2ControlBlock;
    struct GenericAddressStructure X_PMTimerBlock;
    struct GenericAddressStructure X_GPE0Block;
    struct GenericAddressStructure X_GPE1Block;
};





unsigned int do_check_sum	(unsigned char *table_ptr,unsigned int size)
{
	unsigned int n;
	unsigned int checksum;

	checksum=0;
	n=0;
	while(n<size)
	{
		checksum	=	checksum + table_ptr[n];
		n++;
	}

	if((checksum&0xFF)==0)return 1;

	return 0;
}

unsigned int check_acpi_rsdp(mem_ptr *desc)
{
	unsigned int	version;

	if (desc == PTR_NULL)
	{
		writestr("acpi null rsdt \n");
		return 0;
	}

	if(do_check_sum((unsigned char *)desc,sizeof(struct RSDPDescriptor))==0)
	{
		writestr("acpi 1 checksum check failed ! \n");
		return 0;
	}

		
	if(((struct RSDPDescriptor *)(desc))->Revision>=1)
	{

		if(do_check_sum((unsigned char *)desc,sizeof(struct RSDPDescriptor20))==0)
		{
			writestr("acpi 2 checksum check failed ! \n");
			return 0;
		}
	}	

	version=((struct RSDPDescriptor *)(desc))->Revision+1;
	writestr("acpi rdsp v");
	writeint(version,10);
	writestr(".0 is valid \n");
	return version;
}


unsigned int parse_rsdt		(mem_ptr	rsdt_ptr,int v)
{
	char				 sig[5];
	unsigned int		 len;
	unsigned int		 size_after_hdr;
	struct ACPISDTHeader **header_ptr,**last_header_ptr;
	struct ACPISDTHeader *header;

	do_check_sum	(rsdt_ptr,sizeof(struct ACPISDTHeader));
	

	memcpy_c	(sig,((struct ACPISDTHeader *)(rsdt_ptr))->Signature,4);
	sig[4]			=	0;
	len				=	((struct ACPISDTHeader *)(rsdt_ptr))->Length;
	size_after_hdr	=	len-sizeof(struct ACPISDTHeader);

	writestr	("ACPI SDT ");
	writestr	(sig);	
	writestr	(" ");
	writeint	(len,10);
	writestr	(" ");
	writeint	(size_after_hdr,10);
	writestr	("\n");

	header_ptr			=	mem_add(rsdt_ptr,sizeof(struct ACPISDTHeader));
	last_header_ptr		=	mem_add(rsdt_ptr,len);

	while(header_ptr<last_header_ptr)
	{
		header	=*header_ptr;

		if(do_check_sum	((unsigned char *)header,header->Length))
		{
			char *aml_code;



			memcpy_c		(sig,	header->Signature,4);
			sig[4]			=	0;
			len				=	header->Length;
			size_after_hdr	=	header->Length-sizeof(struct ACPISDTHeader);

			
			writestr	("ACPI SDT ");
			writestr	(sig);	
			writestr	(" ");
			writeint	(len,10);
			writestr	(" ");
			writeint	(size_after_hdr,10);
			writestr	("\n");
			if(!strcmp_c(sig,"FACP"))
			{
				struct FADT		*fadt_hdr;
				char			*aml_code;

				fadt_hdr	=	(struct FADT		*)header;

				/*if(v>1)
					aml_code	=	mem_add(fadt_hdr->X_Dsdt,sizeof(struct ACPISDTHeader));
				else*/
					aml_code	=	mem_add(uint_to_mem(fadt_hdr->Dsdt)  ,sizeof(struct ACPISDTHeader));

					/*
				n_read		=	0;
				while(n_read<size_after_hdr)
				{
					char tt[2];

					tt[0]=aml_code[n_read];
					tt[1]=0;
					writestr(tt);
					n_read++;
				}*/
				


			}

			if(!strcmp_c(sig,"SSDT"))
			{

				aml_code	=	mem_add(header,sizeof(struct ACPISDTHeader));
				/*
				n_read		=	0;
				while(n_read<size_after_hdr)
				{
					char tt[2];

					tt[0]=aml_code[n_read];
					tt[1]=0;
					writestr(tt);
					n_read++;
				}
				writestr("\n");
				*/
			}
		}
		else
		{
			writestr	("ACPI SDT CHECKSUM FAIL \n");

		}


		header_ptr=mem_add(header_ptr,8);
	}

	
	
	return 1;
	
}



/******************************************************************************
 *
 * Example ACPICA handler and handler installation
 *
 *****************************************************************************/

static void
NotifyHandler (
    ACPI_HANDLE                 Device,
    UINT32                      Value,
    void                        *Context)
{

    kernel_log(kernel_log_id,"Received a notify ");
	writeint(Value,16);
	writestr("\n");
}


static ACPI_STATUS InstallHandlers (void)
{
    ACPI_STATUS             Status;


    /* Install global notify handler */

    Status = AcpiInstallNotifyHandler (ACPI_ROOT_OBJECT, ACPI_SYSTEM_NOTIFY,
                                        NotifyHandler, NULL);
    if (ACPI_FAILURE (Status))
    {
        kernel_log (kernel_log_id,"error While installing Notify handler \n");
        return (Status);
    }

    return (AE_OK);
}

#define ACPI_MAX_INIT_TABLES    32
static ACPI_TABLE_DESC      TableArray[ACPI_MAX_INIT_TABLES];


/*
 * This function would be called early in kernel initialization. After this
 * is called, all ACPI tables are available to the host.
 */
ACPI_STATUS
InitializeAcpiTables (
    void)
{
    ACPI_STATUS             Status;


    /* Initialize the ACPICA Table Manager and get all ACPI tables */

    Status = AcpiInitializeTables	(TableArray, ACPI_MAX_INIT_TABLES, TRUE);
    return (Status);
}



/******************************************************************************
 *
 * Examples of control method execution.
 *
 * _OSI is a predefined method that is implemented internally within ACPICA.
 *
 * Shows the following elements:
 *
 * 1) How to setup a control method argument and argument list
 * 2) How to setup the return value object
 * 3) How to invoke AcpiEvaluateObject
 * 4) How to check the returned ACPI_STATUS
 * 5) How to analyze the return value
 *
 *****************************************************************************/

static void
ExecuteOSI (void)
{
    ACPI_STATUS             Status;
    ACPI_OBJECT_LIST        ArgList;
    ACPI_OBJECT             Arg[1];
    ACPI_BUFFER             ReturnValue;
    ACPI_OBJECT             *Object;
	char					*tt="ppp";
	
	
    writestr ("Executing _OSI reserved method \n");


	


    /* Setup input argument */

    ArgList.Count = 1;
    ArgList.Pointer = Arg;

    Arg[0].Type = ACPI_TYPE_STRING;
    Arg[0].String.Pointer = "Windows 2001";
    Arg[0].String.Length = strlen_c (Arg[0].String.Pointer);

    /* Ask ACPICA to allocate space for the return object */

    ReturnValue.Length = ACPI_ALLOCATE_BUFFER;

    Status = AcpiEvaluateObject (NULL, "\\_OSI", &ArgList, &ReturnValue);
    if (ACPI_FAILURE (Status))
    {
        writestr ("execption while executing _OSI \n");
        return;
    }

    /* Ensure that the return object is large enough */

    if (ReturnValue.Length < sizeof (ACPI_OBJECT))
    {
        AcpiOsPrintf ("Return value from _OSI method too small, %.8X\n",ReturnValue.Length);
        goto ErrorExit;
    }

    /* Expect an integer return value from execution of _OSI */

    Object = ReturnValue.Pointer;
    if (Object->Type != ACPI_TYPE_INTEGER)
    {
        AcpiOsPrintf ("Invalid return type from _OSI, %.2X\n", Object->Type);
    }

    writestr_fmt ("_OSI returned 0x%8.8X \n",*((unsigned int *)(ReturnValue.Pointer)));


ErrorExit:

    /* Free a buffer created via ACPI_ALLOCATE_BUFFER */

    AcpiOsFree (ReturnValue.Pointer);
}


UINT32 event_test (void                            *Context)
{
	
 writestr ("event \n");

 return 0;
}


ACPI_STATUS device_acpi (ACPI_HANDLE  ObjHandle,UINT32   NestingLevel,void    *Context,void   **ReturnValue)
{
	ACPI_OBJECT_LIST        ArgList;
	ACPI_BUFFER             ReturnVal;
    ACPI_OBJECT             *Object;
	ACPI_STATUS				Status;
	ACPI_DEVICE_INFO		*Info_ptr;
	ACPI_BUFFER				Path;
	ACPI_OBJECT_TYPE		Type = 0;
	ACPI_PCI_ROUTING_TABLE	*prt_ptr;
	char					Buffer[256];
	unsigned int			pci_bus_drv_id;


	// Lookup the type of device-handle
	Status = AcpiGetType(ObjHandle, &Type);
	if (ACPI_FAILURE(Status)) {
		writestr_fmt("Failed to enumerate device at level %%d\n", NestingLevel);
		return AE_OK; // if it fails go to next
	}

	switch (Type) {
		case ACPI_TYPE_DEVICE:
			writestr("Device ");
			break;
		case ACPI_TYPE_PROCESSOR:
			writestr("Proc ");
			break;
		case ACPI_TYPE_THERMAL:
			writestr("Thermal ");
			break;
		case ACPI_TYPE_POWER:
			writestr("Power ");
		default: 
			writestr_fmt("Type %%d ", Type);
			return AE_OK;
	}

	Path.Length		= sizeof (Buffer);
	Path.Pointer	= Buffer;
	/* Get the full path of this device and print it */
	Status = AcpiNsHandleToPathname (ObjHandle, &Path);
	if (ACPI_SUCCESS (Status))
	{
		writestr (Path.Pointer);
		writestr (" ");
	}

	

	Info_ptr = PTR_NULL;
	Status = AcpiGetObjectInfo(ObjHandle, &Info_ptr);
	if (ACPI_SUCCESS(Status))
	{
		/* Get the device info for this device and print it */
		if (strncmp_c(Info_ptr->HardwareId.String, "PNP0A03", 7) == 0 ||
			strncmp_c(Info_ptr->HardwareId.String, "PNP0A08", 7) == 0) {

			if (strncmp_c(Info_ptr->HardwareId.String, "PNP0A08", 7) == 0) {
				// No support in kernel for PCI-Express
				writestr("Missing support for PCI-Express\n");
				return AE_NOT_IMPLEMENTED;
			}
			else {
				AcpiInstallAddressSpaceHandler(ObjHandle, ACPI_ADR_SPACE_PCI_CONFIG, ACPI_DEFAULT_HANDLER, NULL, NULL);
			}
		}
		else {
			//acpiDevice->Type = type;
		}

		writestr("HID:");
		if (Info_ptr->HardwareId.String)
			writestr(Info_ptr->HardwareId.String);
		else
			writestr("NULL");

		writestr(",ADR:");
		writeint(Info_ptr->Address, 16);
		writestr(",Status:");
		writeint(Info_ptr->CurrentStatus, 16);
	}
	writestr("\n");

	ReturnVal.Length	=	ACPI_ALLOCATE_BUFFER;
	ReturnVal.Pointer	=	PTR_NULL;


	if (ACPI_SUCCESS (AcpiGetIrqRoutingTable(ObjHandle, &ReturnVal)))
	{
		ACPI_PCI_ROUTING_TABLE	*end_ptr;

		pci_bus_drv_id = bus_manager_find_bus_by_type(1, 0);

		prt_ptr	=	ReturnVal.Pointer;
		end_ptr	=	mem_add(prt_ptr,ReturnVal.Length);

		writestr_fmt("PRT found %d \n",ReturnVal.Length);

		while(prt_ptr<end_ptr)
		{
			unsigned int n_fn = 0;
			mem_zone_ref my_dev = { PTR_NULL }, my_fn = { PTR_NULL };
			ACPI_HANDLE lnkdev = PTR_NULL;


			if(prt_ptr->Length==0)
				break;

			if (!bus_manager_get_device(pci_bus_drv_id, 0, prt_ptr->Address >> 16, &my_dev))
			{
				prt_ptr = mem_add(prt_ptr, prt_ptr->Length);
				continue;
			}

			/*
			if (tree_manager_get_child_at(&my_dev, 0, &my_fn))
			{
				unsigned int int_line;
				unsigned int int_pin;
				unsigned int class_code, class_sub_code;

				tree_manager_get_child_value_i32(&my_fn, NODE_HASH("int line"), &int_line);
				tree_manager_get_child_value_i32(&my_fn, NODE_HASH("int pin"), &int_pin);

				tree_manager_get_child_value_i32(&my_fn, NODE_HASH("id class"), &class_code);
				tree_manager_get_child_value_i32(&my_fn, NODE_HASH("id subclass"), &class_sub_code);

				
				writestr(" cl ");
				writeint(class_code, 16);

				writestr(" scl ");
				writeint(class_sub_code, 16);
				

				release_zone_ref(&my_fn);
			}*/

			/*
			writestr(" Ln ");
			writeint(prt_ptr->Length,16);
			*/
			n_fn = 0;
			while (tree_manager_get_child_at(&my_dev, n_fn, &my_fn))
			{
				mem_zone_ref pic_infos = { PTR_NULL };

				if (tree_manager_find_child_node(&my_fn, NODE_HASH("PIC"), NODE_BUS_DEVICE_IRQ_INFOS, &pic_infos))
				{
					unsigned int int_pin;
					if (tree_manager_get_child_value_i32(&pic_infos, NODE_HASH("int pin"), &int_pin)) {

						if (int_pin == (prt_ptr->Pin+1))
						{

							break;
						}
					}


					release_zone_ref(&pic_infos);
				}

				release_zone_ref(&my_fn);
				n_fn++;
			}
			release_zone_ref(&my_dev);

			if (my_fn.zone == PTR_NULL)
			{
				prt_ptr = mem_add(prt_ptr, prt_ptr->Length);
				continue;
			}
				


			writestr(" Pin ");
			writeint(prt_ptr->Pin, 16);
			writestr(" Src ");
			writestr(prt_ptr->Source);
			writestr(" ISrc ");
			writeint(prt_ptr->SourceIndex, 16);
			writestr(" Addr ");
			writeint(prt_ptr->Address >> 16, 16);
			writestr(" fn ");
			writeint(n_fn, 16);

			if (prt_ptr->SourceIndex != 0)
			{
				writestr(" dev sIRQ ");
				writeint(prt_ptr->SourceIndex, 16);

				mem_zone_ref apic_infos = { PTR_NULL };

				if (tree_manager_add_child_node(&my_fn, "APIC", NODE_BUS_DEVICE_IRQ_INFOS, &apic_infos))
				{
					tree_manager_set_child_value_i32(&apic_infos, "int line", prt_ptr->SourceIndex);
					tree_manager_set_child_value_i32(&apic_infos, "Polarity", 1);
					tree_manager_set_child_value_i32(&apic_infos, "Sharable", 1);
					tree_manager_set_child_value_i32(&apic_infos, "Triggering", 1);
					tree_manager_set_child_value_i32(&apic_infos, "WakeCapable", 0);
					release_zone_ref(&apic_infos);
				}

				release_zone_ref(&my_fn);
				
			}
			else
			{
				if (ACPI_SUCCESS(AcpiGetHandle(ObjHandle, prt_ptr->Source, &lnkdev))) {
					char			abuf[128];
					char			bbuf[128];
					ACPI_BUFFER		srsbuf;
					ACPI_RESOURCE	*end, *res;


					srsbuf.Pointer = PTR_NULL;
					srsbuf.Length = ACPI_ALLOCATE_BUFFER;

					if (ACPI_SUCCESS(AcpiGetCurrentResources(lnkdev, &srsbuf)))
					{
						res = (ACPI_RESOURCE *)srsbuf.Pointer;
						end = (ACPI_RESOURCE *)((char *)srsbuf.Pointer + srsbuf.Length);

						for (;;) {
							switch (res->Type) {
							case ACPI_RESOURCE_TYPE_START_DEPENDENT:
								writestr("ACPI_RESOURCE_TYPE_START_DEPENDENT \n");
								break;
							case ACPI_RESOURCE_TYPE_END_DEPENDENT:
								writestr("ACPI_RESOURCE_TYPE_END_DEPENDENT \n");
								break;
							case ACPI_RESOURCE_TYPE_IRQ:
							{
								mem_zone_ref apic_infos = { PTR_NULL };
								
								writestr(" dev IRQ ");
								writeint(res->Data.Irq.Interrupts[0], 16);

								if (tree_manager_add_child_node(&my_fn, "APIC", NODE_BUS_DEVICE_IRQ_INFOS, &apic_infos))
								{
									tree_manager_set_child_value_i32(&apic_infos, "int line", res->Data.Irq.Interrupts[0]);
									tree_manager_set_child_value_i32(&apic_infos, "Polarity", res->Data.Irq.Polarity);
									tree_manager_set_child_value_i32(&apic_infos, "Sharable", res->Data.Irq.Sharable);
									tree_manager_set_child_value_i32(&apic_infos, "Triggering", res->Data.Irq.Triggering);
									tree_manager_set_child_value_i32(&apic_infos, "WakeCapable", res->Data.Irq.WakeCapable);
									release_zone_ref(&apic_infos);
								}
							}
								
							release_zone_ref(&my_fn);
							
							break;
							case ACPI_RESOURCE_TYPE_EXTENDED_IRQ:
								writestr(" dev eIRQ");
								writeint(res->Data.Irq.Interrupts[0], 10);
								break;
							}

							if (res->Type == ACPI_RESOURCE_TYPE_END_TAG)
								break;

							res = ACPI_NEXT_RESOURCE(res);
						}
					}
					else
					{
						writestr("\n");
						kernel_log(kernel_log_id, "couldn't find PCI device ressource \n");
						snooze(10000000);
					}
					/*free_c(srsbuf);*/
				}
				else
				{
					writestr("\n");
					kernel_log(kernel_log_id, "couldn't find PCI interrupt link device \n");
					snooze(10000000);
					break;
				}
			}

			writestr("\n");
			
			prt_ptr=mem_add(prt_ptr,prt_ptr->Length);
		}
	}
	
	//*ReturnValue=0;

	return AE_OK;
}

static ACPI_STATUS __ExecutePIC(int interruptMode)
{
	ACPI_STATUS      status;
	ACPI_OBJECT_LIST arguments;
	ACPI_OBJECT      interruptModeArgument;

	// create the buffer object to hold the parameter
	// 0 = pic, 1 = ioapic
	interruptModeArgument.Type = ACPI_TYPE_INTEGER;
	interruptModeArgument.Integer.Value = interruptMode;

	arguments.Count = 1;
	arguments.Pointer = &interruptModeArgument;

	// No return value is required for _PIC
	status = AcpiEvaluateObject(NULL, "\\_PIC", &arguments, NULL);
	if (status == AE_NOT_FOUND) {
		return AE_OK;
	}
	return status;
}

/*
 * This function would be called after the kernel is initialized and
 * dynamic/virtual memory is available. It completes the initialization of
 * the ACPICA subsystem.
 */
ACPI_STATUS InitializeAcpi (void)
{
    ACPI_STATUS             Status;


    /* Initialize the ACPICA subsystem */

	InitializeAcpiTables();


    Status = AcpiInitializeSubsystem ();
    if (ACPI_FAILURE (Status))
    {

		writestr("acpi fail initialize subsystem\n");
		snooze(30000000);

        return (Status);
    }


    /* Copy the root table list to dynamic memory */

    Status = AcpiReallocateRootTable ();
    if (ACPI_FAILURE (Status))
    {
        return (Status);
    }

    /* Create the ACPI namespace from ACPI tables */

    Status = AcpiLoadTables ();
    if (ACPI_FAILURE (Status))
    {
        return (Status);
    }



    // Install local handlers 
    Status = InstallHandlers ();
    if (ACPI_FAILURE (Status))
    {
        writestr ( "error While installing handlers \n");
        return (Status);
    }

    /* Initialize the ACPI hardware */
    Status = AcpiEnableSubsystem (ACPI_FULL_INITIALIZATION);
    if (ACPI_FAILURE (Status))
    {
        return (Status);
    }

    /* Complete the ACPI namespace object initialization */
    Status = AcpiInitializeObjects (ACPI_FULL_INITIALIZATION);
    if (ACPI_FAILURE (Status))
    {
        return (Status);
    }


	__ExecutePIC(interupt_mode);


	AcpiEnableEvent					(ACPI_EVENT_PMTIMER,0);
	AcpiEnableEvent					(ACPI_EVENT_GLOBAL,0);
	AcpiEnableEvent					(ACPI_EVENT_POWER_BUTTON,0);
	AcpiEnableEvent					(ACPI_EVENT_SLEEP_BUTTON,0);
	AcpiEnableEvent					(ACPI_EVENT_RTC,0);
	
	AcpiInstallFixedEventHandler	(ACPI_EVENT_PMTIMER		 ,event_test,PTR_NULL);
	AcpiInstallFixedEventHandler	(ACPI_EVENT_GLOBAL		 ,event_test,PTR_NULL);
	AcpiInstallFixedEventHandler	(ACPI_EVENT_POWER_BUTTON ,event_test,PTR_NULL);
	AcpiInstallFixedEventHandler	(ACPI_EVENT_SLEEP_BUTTON ,event_test,PTR_NULL);
	AcpiInstallFixedEventHandler	(ACPI_EVENT_RTC			 ,event_test,PTR_NULL);


    return (AE_OK);
}

unsigned int acpi_devices()
{
	mem_ptr					ret_val;
	AcpiGetDevices(PTR_NULL, device_acpi, PTR_NULL, PTR_NULL);

	writestr("device ended \n");

	return 1;
}

unsigned int init_acpi		(mem_ptr acpi_rdsp_ptr)
{
	unsigned		int acpi_version;
	unsigned		int ret;
	acpi_version=check_acpi_rsdp	(acpi_rdsp_ptr);
	if(acpi_version==0)return 0;

	
	ret=0;
	if(acpi_version>1)
	{	
		ret=parse_rsdt		(uint_to_mem(((struct RSDPDescriptor20  *)(acpi_rdsp_ptr))->XsdtAddress),acpi_version);
	}
	else
	{
		ret=parse_rsdt		(uint_to_mem(((struct RSDPDescriptor20  *)(acpi_rdsp_ptr))->RsdtAddress),acpi_version);
	}
	
	InitializeAcpi();

	ExecuteOSI	();
	
	
	return ret;
}
