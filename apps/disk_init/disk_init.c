#define APP_DLL_FUNC C_EXPORT

#include <std_def.h>
#include <std_mem.h>

#include "kern.h"
#include "mem_base.h"
#include "lib_c.h"
#include "tree.h"
#include "sys/async_stream.h"
#include "sys/stream_dev.h"
#include "sys/mem_stream.h"
#include "sys/file_system.h"
#include "sys/tpo_mod.h"

#include "sys/task.h"
#include "../../kernel/bus_manager/bus_drv.h"
#include "sys/ndis.h"
#include "../../zlib-1.2.3/zlib.h"

#include "sys/acpi.h"

extern unsigned int init_acpi		(mem_ptr acpi_rdsp_ptr);
extern unsigned int	acpi_devices	();
extern mem_ptr						acpi_rdsp;

/*
mem_ptr						acpi_rdsp = PTR_INVALID;
unsigned int init_acpi(mem_ptr acpi_rdsp_ptr)
{
}
*/

unsigned int			kernel_log_id						= 	0xFFFFFFFF;

int read_iso_disk()
{
	unsigned int			boot_dev,boot_str;
	unsigned int n_dev,n_str;
	unsigned int pci_bus_drv_id, usb_bus_drv_id;
	unsigned int storage_dev_str_id,storage_dev_id;
	mem_zone_ref					read_request={PTR_NULL};
	mem_zone_ref					img_data={PTR_NULL};
	unsigned char					*bufferPtr;
	int								volume_size;
	mem_stream						img_stream;
	

	pci_bus_drv_id			=bus_manager_find_bus_by_type(1,0);
	boot_str				=0xFFFFFFFF;
	boot_dev				=0xFFFFFFFF;

	n_dev=0;
	while((storage_dev_id=	bus_manager_find_device_by_type			(pci_bus_drv_id,DEVICE_TYPE_STORAGE,n_dev))!=0xFFFFFFFF)
	{
		mem_zone_ref infos;
		infos.zone=PTR_NULL;

		n_str=0;
		while((storage_dev_str_id=	bus_manager_get_dev_str_infos (pci_bus_drv_id,storage_dev_id,n_str,&infos))!=0)
		{
			if(infos.zone!=PTR_NULL)
			{
				char *drv_ctrler,* drv_pos,* drv_type;

				

				drv_type	=	(char *)tree_manager_get_child_data(&infos,NODE_HASH("drive type"),0);
				drv_ctrler	=	(char *)tree_manager_get_child_data(&infos,NODE_HASH("drive controller"),0);
				drv_pos		=	(char *)tree_manager_get_child_data(&infos,NODE_HASH("pos"),0);

				writestr_fmt("str dev str id %x %s %s %s\n",storage_dev_str_id,drv_type,drv_ctrler,drv_pos);


				if(!strcmp_c(drv_type,"atapi"))
				{
					if(!strcmp_c(drv_ctrler,"primary"))
					{
						if(!strcmp_c(drv_pos,"slave"))
						{
							boot_str=storage_dev_str_id;
							boot_dev=storage_dev_id;
						}
					}
				}
			}
			n_str++;
		}
		n_dev++;
	}
	if(boot_str==0xFFFFFFFF){kernel_log(kernel_log_id,"error boot str\n");return 0;}
	if(boot_dev==0xFFFFFFFF){kernel_log(kernel_log_id,"error boot dev\n");return 0;}
	
	boot_str = 0;

	read_request.zone=PTR_NULL;
	tree_manager_create_node			("req_vol_start",NODE_REQUEST,&read_request);
	tree_manager_set_child_value_i32	(&read_request,"start_sector"	,16);
	tree_manager_set_child_value_i32	(&read_request,"n_bytes"		,2048);

	bus_manager_read_bus_device_stream	(pci_bus_drv_id,boot_dev,boot_str,&read_request);

	tree_manager_get_child_value_ptr	(&read_request,NODE_HASH("data_buffer_zone"),0,&img_data.zone);
	tree_manager_get_child_value_ptr	(&read_request,NODE_HASH("data_buffer_ptr")	,0,&bufferPtr);
	volume_size						=	*((int *)(&bufferPtr[80]));
	if(volume_size<=0)
	{
		kernel_log				(kernel_log_id,"bad volume size ");
		writeint				(volume_size,10);
		writestr				("\n");
		release_zone_ref		(&img_data);
		return 0;
	}

	kernel_log					(kernel_log_id,"reading iso image ");
	writeint					((volume_size*2048)/1024,10);
	writestr					("kb \n");
	release_zone_ref			(&img_data);
	release_zone_ref			(&read_request);

	read_request.zone	=PTR_NULL;
	
	tree_manager_create_node			("req_whole",NODE_REQUEST,&read_request);
	tree_manager_set_child_value_i32	(&read_request,"start_sector"	,0);
	tree_manager_set_child_value_i32	(&read_request,"n_bytes"		,volume_size*2048);

	release_zone_ref(&img_data);
	

	
	
	bus_manager_read_bus_device_stream	(pci_bus_drv_id,boot_dev,boot_str,&read_request);
	
	tree_manager_get_child_value_ptr	(&read_request,NODE_HASH("data_buffer_zone"),0,&img_data.zone);
	tree_manager_get_child_value_ptr	(&read_request,NODE_HASH("data_buffer_ptr")	,0,&bufferPtr);
	release_zone_ref					(&read_request);

	

	img_stream.data.zone				=PTR_NULL;
	mem_stream_init						(&img_stream,&img_data,mem_sub(get_zone_ptr(&img_data,0),bufferPtr));

	if(open_iso_image						(&img_stream,"isodisk",2)!=1)
	{
		kernel_log	(kernel_log_id,"could not read iso file ");

		writeptr	(get_zone_ptr(&img_data,0));
		writestr	(" ");
		writeptr	(get_zone_ptr(&img_stream.data,0));
		writestr	(get_zone_ptr(&img_stream.data,2048*16));
		writestr	("\n");
	}

	mem_stream_close	(&img_stream);
	release_zone_ref	(&img_data);

	

	return 1;

	/*
	int						volume_size;
	unsigned int			n_read;
	unsigned char			*sector;
	struct bios_drive_t		infos;
	mem_stream				iso_file;

	kernel_log					(kernel_log_id,"bios reset drives \n");
	bios_reset_drives_c			();

	boot_dsk					=	bios_get_boot_device_c();
	
	kernel_log					(kernel_log_id,"get disk infos ");
	writeint					(boot_dsk,16);
	writestr					("\n");

	if(read_bios_disk_geom_c		(boot_dsk,&infos)!=1)
	{
		kernel_log	(kernel_log_id,"get bios disk infos failed for device ");
		writeint	(boot_dsk,16);
		writestr	("\n");
		return;
	}

	
	
	kernel_log					(kernel_log_id,"bios read sector ");
	writeint					(infos.bios_id,16);
	writestr					(" ");
	writeint					(infos.is_lba,10);
	writestr					(" ");
	writeint					(infos.sector_size,10);
	writestr					(" ");
	writeint					(infos.cylinders,10);
	writestr					(" ");
	writeint					(infos.heads,10);
	writestr					(" ");
	writeint					(infos.sectors,10);
	writestr					(" ");
	writeint					(infos.hdXsecs,10);
	writestr					("\n");
	

	if(bios_read_sector_c			(&infos,((16*2048)/infos.sector_size))==0)
	{
		kernel_log		(kernel_log_id,"bios read data sector failed \n");
		writeint		(boot_dsk,16);
		writestr		("\n");
		return ;
	}

	sector					=	bios_get_sector_data_ptr_c	();

	volume_size				=	*((int *)(&sector[80]));

	if(volume_size<=0)
	{
		kernel_log				(kernel_log_id,"bad volume size ");
		writeint				(volume_size,10);
		writestr				("\n");

		return;
	}
	
	iso_mem.zone		=PTR_NULL;
	allocate_new_zone(0x00,volume_size*2048,&iso_mem);

	kernel_log					(kernel_log_id,"reading iso image ");
	writeint					((volume_size*2048)/1024,10);
	writestr					("kb => ");
	writeptr					(get_zone_ptr(&iso_mem,0));
	writestr					("\n");
	
	n_read					=	0;
	while(n_read*infos.sector_size<volume_size*2048)
	{
		if(bios_read_sector_c		(&infos,n_read)!=1)
		{
			kernel_log	(kernel_log_id,"error sector \n");
		}

		iso_mem_ptr				=	get_zone_ptr	(&iso_mem,n_read*infos.sector_size);
		sector					=	bios_get_sector_data_ptr_c	();
		memcpy_c					(iso_mem_ptr,sector,infos.sector_size);

		n_read++;
	}
	iso_file.data.zone	=PTR_NULL;
	
	
	
	
	mem_stream_init					(&iso_file,&iso_mem);

	if(open_iso_image					(&iso_file,"isodisk",2)!=1)
	{
		kernel_log	(kernel_log_id,"could not read iso file ");

		writeptr	(get_zone_ptr(&iso_mem,0));
		writestr	(" ");
		writeptr	(get_zone_ptr(&iso_file.data,0));
		writestr	(get_zone_ptr(&iso_file.data,2048*16));
		writestr	("\n");
	}

	mem_stream_close	(&iso_file);
	release_zone_ref	(&iso_mem);
	*/


}


int init_pci_bus		()
{
	mem_zone_ref			pci_root = { PTR_NULL };
	unsigned int			pci_bus_drv_id;


	pci_bus_drv_id		=	bus_manager_load_bus_driver	("ramdisk","/pci_bus.tpo");

	if(pci_bus_drv_id==0xFFFFFFFF)
	{
		kernel_log(kernel_log_id,"could not load pci bus driver \n");
		return 0;
	}

	kernel_log	(kernel_log_id,"pci bus driver loaded ");
	writeint	(pci_bus_drv_id,16);
	writestr	("\n");

	bus_manager_scan_bus_drivers	(pci_bus_drv_id,"ramdisk","/drivers/pci");
	bus_manager_scan_bus			(pci_bus_drv_id);
	
	/*
	if (bus_manager_get_bus_dev_tree(pci_bus_drv_id, &pci_root))
	{
		tree_manager_dump_node_rec(&pci_root, 0, 8);
		release_zone_ref(&pci_root);
	}
	*/

	return 1;	
}

int init_usb_bus		()
{
	mem_zone_ref			usb_root = { PTR_NULL };
	unsigned int			usb_bus_drv_id;

	usb_bus_drv_id	=	bus_manager_find_bus_by_type(2,0);

	if (usb_bus_drv_id == 0xFFFFFFFF) { 
		kernel_log(kernel_log_id, "usb bus driver not found \n");
		return 0; 
	}

	bus_manager_scan_bus_drivers		(usb_bus_drv_id,"ramdisk","/drivers/usb");
	bus_manager_scan_bus				(usb_bus_drv_id);

	if (bus_manager_get_bus_dev_tree(usb_bus_drv_id, &usb_root))
	{
		tree_manager_dump_node_rec(&usb_root, 0, 8);
		release_zone_ref(&usb_root);
	}


	return 1;	
}

unsigned int		cpu_freq = 0xFFFFFFFF;
unsigned int		interupt_mode = 0xFFFFFFFF;

OS_API_C_FUNC(int) run_func()
{
	tpo_mod_file			graphic_base_tpo;
	tpo_mod_file			filters_tpo;
	tpo_mod_file			sys_init_tpo;
	tpo_mod_file			gfx_lib_tpo;
	

	unsigned int n_dev,n_str;
	unsigned int pci_bus_drv_id;
	unsigned int usb_bus_drv_id;
	unsigned int storage_dev_str_id,storage_dev_id;
	mem_zone_ref new_node={PTR_NULL};

	kernel_log_id		=	get_new_kern_log_id		("disk_init: ",0x06);

	interupt_mode = get_interupt_mode_c();


	/*
	snooze				(1000000);
	bios_set_vga_mode_c	(0x03);
	snooze				(10000000);
	*/

	/*
	strcpy_c				(params,"[");

	strcat_s				(params,128,"{");
	add_params_object		(params,128,"entry 1",0x1234);
	add_params_uint_property(params,128,"id"	,12);
	add_params_uint_property(params,128,"pos_x"	,32);
	add_params_str_property (params,128,"text"	,"toto");
	strcat_s				(params,128,"}");

	strcat_s				(params,128,",");
	
	strcat_s				(params,128,"{");
	add_params_object		(params,128,"entry  2",0x1234);
	add_params_uint_property(params,128,"id"	  ,13);
	add_params_uint_property(params,128,"pos_x"	  ,42);
	add_params_str_property (params,128,"text"	  ,"tutu");
	strcat_s				(params,128,"}");

	strcat_s				(params,128,"]");

	writestr(params);
	writestr("\n");

	tree_manager_create_node_childs				("entry list",0,&new_node,params);
	tree_manager_dump_node_rec					(&new_node,0,4);

	while(1);
	*/

	

	/*
	mem_zone_ref	pouet={PTR_NULL};
	mem_zone_ref_ptr pouet_ptr;
		char							*str;
	unsigned int					str_len,n;

	tree_manager_create_node_childs("test_node",NODE_GFX_INT,&pouet,"[{name:pouet,type:0xFEDCBA,id:4},{name:pouetos,type:0xABCDEF,id:5}]");
	tree_manager_dump_node_rec(&pouet,0,4);
	str		=	"[{name:pouet,type:0},{name:pouetos,type:1}]";
	str_len =   strlen_c(str);

	pouet.zone=PTR_NULL;
	tree_manager_json_loadb(str,str_len,&pouet);

	writestr("parse done \n");


	pouet_ptr=get_zone_ptr(&pouet,0);

	n=0;
	while(pouet_ptr[n].zone!=NULL)
	{
		tree_manager_dump_node_rec(&pouet_ptr[n],0,4);
		n++;
	}
	while(1)
	*/

	


/*
	task_manager_new_task("ttt",task_test,PTR_NULL,uint_to_mem(20));
	task_manager_new_task("vvv",task_test,PTR_NULL,uint_to_mem(30));
*/
	//while(1);
/*
	writestr_fmt("\n test format string %d %x \n",4,8);
	snooze(10000000);
*/
	if (bus_manager_init(interupt_mode) != 1)
	{
		kernel_log(kernel_log_id, "could not init bus manager \n");
		return 0;
	}

	//if (interupt_mode == 1)
	{
		acpi_rdsp = acpi_get_rdsp_c();
		if (!init_acpi(acpi_rdsp))
			interupt_mode = 0;
	}

	

	if (init_ndis(interupt_mode) != 1)
	{
		kernel_log(kernel_log_id, "could not load ndis driver \n");
		return 0;
	}
	
	init_pci_bus			();

	if (interupt_mode == 1)
	{
		acpi_devices();
	}

	pci_bus_drv_id	= bus_manager_find_bus_by_type(1, 0);
	bus_manager_parse_bus(pci_bus_drv_id);


	usb_bus_drv_id = bus_manager_find_bus_by_type(2, 0);
	init_usb_bus();
	bus_manager_parse_bus(usb_bus_drv_id);



	n_dev=0;
	while((storage_dev_id=	bus_manager_find_device_by_type			(pci_bus_drv_id,DEVICE_TYPE_STORAGE,n_dev))!=0xFFFFFFFF)
	{
		mem_zone_ref infos;
		infos.zone=PTR_NULL;
		n_str=0;
		while((storage_dev_str_id=	bus_manager_get_dev_str_infos (pci_bus_drv_id,storage_dev_id,n_str,&infos))!=0)
		{
			if(infos.zone!=PTR_NULL)
			{
				tree_manager_dump_node_rec		(&infos,0,4);
			}
			n_str++;
			release_zone_ref(&infos);
		}
		n_dev++;
	}

	//snooze(10000000);


	graphic_base_tpo.data_sections.zone	= PTR_NULL;
	filters_tpo.data_sections.zone		= PTR_NULL;
	sys_init_tpo.data_sections.zone		= PTR_NULL;
	gfx_lib_tpo.data_sections.zone		= PTR_NULL;

	if(!read_iso_disk				())
	{
		kernel_log(kernel_log_id,"could not open iso disk fatal !!\n");
		return 0;
		
	}


	bus_manager_load_bus_device_string(pci_bus_drv_id, "isodisk", "/system/pci_strs.csv");


	load_tpo_dll				("isodisk","/system/libs/gfx_filters.tpo"	,&filters_tpo);
	load_tpo_dll				("isodisk","/system/libs/gfx_lib.tpo"		,&gfx_lib_tpo);
	load_tpo_dll				("isodisk","/drivers/graphic_base.tpo"		,&graphic_base_tpo);
	run_tpo						("isodisk","/apps/sys_init.tpo"				,&sys_init_tpo);

	
	return 1;

}

