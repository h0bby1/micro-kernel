#define ACPI_API C_EXPORT 
#define STREAM_API C_EXPORT



#include <sys/acpi/acpi.h>
#include <sys/acpi/accommon.h>
#include <sys/acpi/acutils.h>


#include <std_def.h>
#include <std_mem.h>
#include <mem_base.h>
#include <sys/acpi.h>
#include <sys/task.h>

#include <kern.h>
#include <sys_pci.h>
#include <std_str.h>

mem_ptr					acpi_rdsp = PTR_INVALID;
extern unsigned int		kernel_log_id;


#define _COMPONENT          ACPI_UTILITIES
ACPI_MODULE_NAME    ("acpi_os")
#define OS_API_C_FUNC(vv) vv

/*******************************************************************************
 *
 * FUNCTION:    AcpiOsOpenDirectory
 *
 * PARAMETERS:  DirPathname         - Full pathname to the directory
 *              WildcardSpec        - string of the form "*.c", etc.
 *              RequestedFileType   - Either a directory or normal file
 *
 * RETURN:      A directory "handle" to be used in subsequent search operations.
 *              NULL returned on failure.
 *
 * DESCRIPTION: Open a directory in preparation for a wildcard search
 *
 ******************************************************************************/

OS_API_C_FUNC(void *) AcpiOsOpenDirectory (char                    *DirPathname, char  *WildcardSpec,char   RequestedFileType)
{
    
    return PTR_NULL;
}


/******************************************************************************
 *
 * FUNCTION:    AcpiOsGetRootPointer
 *
 * PARAMETERS:  None
 *
 * RETURN:      RSDP physical address
 *
 * DESCRIPTION: Gets the root pointer (RSDP)
 *
 *****************************************************************************/

OS_API_C_FUNC(ACPI_PHYSICAL_ADDRESS) AcpiOsGetRootPointer (void)
{
	writestr("get root pointer ");
	writeint(mem_to_uint(acpi_get_rdsp_c()),16);
	writestr("\n");
    return mem_to_uint(acpi_get_rdsp_c());
}

/*******************************************************************************
 *
 * FUNCTION:    AcpiOsGetNextFilename
 *
 * PARAMETERS:  DirHandle           - Created via AcpiOsOpenDirectory
 *
 * RETURN:      Next filename matched. NULL if no more matches.
 *
 * DESCRIPTION: Get the next file in the directory that matches the wildcard
 *              specification.
 *
 ******************************************************************************/

OS_API_C_FUNC(char *) AcpiOsGetNextFilename (void                    *DirHandle)
{
    return PTR_NULL;
}


/*******************************************************************************
 *
 * FUNCTION:    AcpiOsCloseDirectory
 *
 * PARAMETERS:  DirHandle           - Created via AcpiOsOpenDirectory
 *
 * RETURN:      None
 *
 * DESCRIPTION: Close the open directory and cleanup.
 *
 ******************************************************************************/

OS_API_C_FUNC(void ) AcpiOsCloseDirectory (void   *DirHandle)
{
   
}







/*
 * Tables supported in the Windows registry. SSDTs are not placed into
 * the registry, a limitation.
 */
static char             *SupportedTables[] =
{
    "DSDT",
    "RSDT",
    "FACS",
    "FACP"
};

/* Max index for table above */

#define ACPI_OS_MAX_TABLE_INDEX     3



/******************************************************************************
 *
 * FUNCTION:    AcpiOsGetTableByAddress
 *
 * PARAMETERS:  Address         - Physical address of the ACPI table
 *              Table           - Where a pointer to the table is returned
 *
 * RETURN:      Status; Table buffer is returned if AE_OK.
 *              AE_NOT_FOUND: A valid table was not found at the address
 *
 * DESCRIPTION: Get an ACPI table via a physical memory address.
 *
 * NOTE:        Cannot be implemented without a Windows device driver.
 *
 *****************************************************************************/

OS_API_C_FUNC(ACPI_STATUS ) AcpiOsGetTableByAddress (ACPI_PHYSICAL_ADDRESS   Address,ACPI_TABLE_HEADER       **Table)
{

	writestr("get table address\n");
    //fprintf (stderr, "Get table by address is not supported on Windows\n");
    return (AE_SUPPORT);
}


/******************************************************************************
 *
 * FUNCTION:    AcpiOsGetTableByIndex
 *
 * PARAMETERS:  Index           - Which table to get
 *              Table           - Where a pointer to the table is returned
 *              Instance        - Where a pointer to the table instance no. is
 *                                returned
 *              Address         - Where the table physical address is returned
 *
 * RETURN:      Status; Table buffer and physical address returned if AE_OK.
 *              AE_LIMIT: Index is beyond valid limit
 *
 * DESCRIPTION: Get an ACPI table via an index value (0 through n). Returns
 *              AE_LIMIT when an invalid index is reached. Index is not
 *              necessarily an index into the RSDT/XSDT.
 *              Table is obtained from the Windows registry.
 *
 * NOTE:        Cannot get the physical address from the windows registry;
 *              zero is returned instead.
 *
 *****************************************************************************/

OS_API_C_FUNC(ACPI_STATUS ) AcpiOsGetTableByIndex (UINT32                  Index,ACPI_TABLE_HEADER       **Table,UINT32                  *Instance,ACPI_PHYSICAL_ADDRESS   *Address)
{
    ACPI_STATUS             Status;

	writestr("get table index\n");

    if (Index > ACPI_OS_MAX_TABLE_INDEX)
    {
        return (AE_LIMIT);
    }

    Status = AcpiOsGetTableByName (SupportedTables[Index], 0, Table, Address);
    return (Status);
}


/******************************************************************************
 *
 * FUNCTION:    AcpiOsGetTableByName
 *
 * PARAMETERS:  Signature       - ACPI Signature for desired table. Must be
 *                                a null terminated 4-character string.
 *              Instance        - For SSDTs (0...n). Use 0 otherwise.
 *              Table           - Where a pointer to the table is returned
 *              Address         - Where the table physical address is returned
 *
 * RETURN:      Status; Table buffer and physical address returned if AE_OK.
 *              AE_LIMIT: Instance is beyond valid limit
 *              AE_NOT_FOUND: A table with the signature was not found
 *
 * DESCRIPTION: Get an ACPI table via a table signature (4 ASCII characters).
 *              Returns AE_LIMIT when an invalid instance is reached.
 *              Table is obtained from the Windows registry.
 *
 * NOTE:        Assumes the input signature is uppercase.
 *              Cannot get the physical address from the windows registry;
 *              zero is returned instead.
 *
 *****************************************************************************/

OS_API_C_FUNC(ACPI_STATUS ) AcpiOsGetTableByName (char                    *Signature,UINT32                  Instance,ACPI_TABLE_HEADER       **Table,ACPI_PHYSICAL_ADDRESS   *Address)
{
	writestr("get table name\n");
    return (AE_OK);
}


/* These are here for acpidump only, so we don't need to link oswinxf */

#ifdef ACPI_DUMP_APP
/******************************************************************************
 *
 * FUNCTION:    AcpiOsMapMemory
 *
 * PARAMETERS:  Where               - Physical address of memory to be mapped
 *              Length              - How much memory to map
 *
 * RETURN:      Pointer to mapped memory. Null on error.
 *
 * DESCRIPTION: Map physical memory into caller's address space
 *
 *****************************************************************************/

void *
AcpiOsMapMemory (
    ACPI_PHYSICAL_ADDRESS   Where,
    ACPI_SIZE               Length)
{

    return (ACPI_TO_POINTER ((ACPI_SIZE) Where));
}


/******************************************************************************
 *
 * FUNCTION:    AcpiOsUnmapMemory
 *
 * PARAMETERS:  Where               - Logical address of memory to be unmapped
 *              Length              - How much memory to unmap
 *
 * RETURN:      None.
 *
 * DESCRIPTION: Delete a previously created mapping. Where and Length must
 *              correspond to a previous mapping exactly.
 *
 *****************************************************************************/

void
AcpiOsUnmapMemory (
    void                    *Where,
    ACPI_SIZE               Length)
{

    return;
}
#endif




#define ACPI_OS_DEBUG_TIMEOUT   30000 /* 30 seconds */


/* Upcalls to AcpiExec application */



OS_API_C_FUNC(void) AeTableOverride (ACPI_TABLE_HEADER       *ExistingTable,ACPI_TABLE_HEADER       **NewTable);

/*
 * Real semaphores are only used for a multi-threaded application
 */
#ifndef ACPI_SINGLE_THREADED

/* Semaphore information structure */

typedef struct acpi_os_semaphore_info
{
    UINT16                  MaxUnits;
    UINT16                  CurrentUnits;
    void                    *OsHandle;

} ACPI_OS_SEMAPHORE_INFO;

/* Need enough semaphores to run the large aslts suite */

#define ACPI_OS_MAX_SEMAPHORES  256

ACPI_OS_SEMAPHORE_INFO          AcpiGbl_Semaphores[ACPI_OS_MAX_SEMAPHORES]={0xFF};

#endif /* ACPI_SINGLE_THREADED */



/******************************************************************************
 *
 * FUNCTION:    AcpiOsTerminate
 *
 * PARAMETERS:  None
 *
 * RETURN:      Status
 *
 * DESCRIPTION: Nothing to do for windows
 *
 *****************************************************************************/

OS_API_C_FUNC(ACPI_STATUS) AcpiOsTerminate (
    void)
{
    return (AE_OK);
}


/******************************************************************************
 *
 * FUNCTION:    AcpiOsInitialize
 *
 * PARAMETERS:  None
 *
 * RETURN:      Status
 *
 * DESCRIPTION: Init this OSL
 *
 *****************************************************************************/

OS_API_C_FUNC(ACPI_STATUS ) AcpiOsInitialize (void)
{


#ifndef ACPI_SINGLE_THREADED
    /* Clear the semaphore info array */

    memset_c (AcpiGbl_Semaphores, 0x00, sizeof (AcpiGbl_Semaphores));
#endif

	/*
    AcpiGbl_OutputFile = stdout;

	
    LARGE_INTEGER           LocalTimerFrequency;

    // Get the timer frequency for use in AcpiOsGetTimer 

    TimerFrequency = 0;
    if (QueryPerformanceFrequency (&LocalTimerFrequency))
    {
        // Frequency is in ticks per second 

        TimerFrequency = LocalTimerFrequency.QuadPart;
    }
	*/
    return (AE_OK);
}




/******************************************************************************
 *
 * FUNCTION:    AcpiOsPredefinedOverride
 *
 * PARAMETERS:  InitVal             - Initial value of the predefined object
 *              NewVal              - The new value for the object
 *
 * RETURN:      Status, pointer to value. Null pointer returned if not
 *              overriding.
 *
 * DESCRIPTION: Allow the OS to override predefined names
 *
 *****************************************************************************/

OS_API_C_FUNC(ACPI_STATUS ) AcpiOsPredefinedOverride (const ACPI_PREDEFINED_NAMES *InitVal,ACPI_STRING                 *NewVal)
{

    if (!InitVal || !NewVal)
    {
        return (AE_BAD_PARAMETER);
    }

    *NewVal = NULL;
    return (AE_OK);
}


/******************************************************************************
 *
 * FUNCTION:    AcpiOsTableOverride
 *
 * PARAMETERS:  ExistingTable       - Header of current table (probably firmware)
 *              NewTable            - Where an entire new table is returned.
 *
 * RETURN:      Status, pointer to new table. Null pointer returned if no
 *              table is available to override
 *
 * DESCRIPTION: Return a different version of a table if one is available
 *
 *****************************************************************************/

OS_API_C_FUNC(ACPI_STATUS ) AcpiOsTableOverride (ACPI_TABLE_HEADER       *ExistingTable,ACPI_TABLE_HEADER       **NewTable)
{

    if (!ExistingTable || !NewTable)
    {
        return (AE_BAD_PARAMETER);
    }

    *NewTable = NULL;


#ifdef ACPI_EXEC_APP

    /* Call back up to AcpiExec */

    AeTableOverride (ExistingTable, NewTable);
#endif

    return (AE_OK);
}


/******************************************************************************
 *
 * FUNCTION:    AcpiOsPhysicalTableOverride
 *
 * PARAMETERS:  ExistingTable       - Header of current table (probably firmware)
 *              NewAddress          - Where new table address is returned
 *                                    (Physical address)
 *              NewTableLength      - Where new table length is returned
 *
 * RETURN:      Status, address/length of new table. Null pointer returned
 *              if no table is available to override.
 *
 * DESCRIPTION: Returns AE_SUPPORT, function not used in user space.
 *
 *****************************************************************************/

OS_API_C_FUNC(ACPI_STATUS ) AcpiOsPhysicalTableOverride (ACPI_TABLE_HEADER       *ExistingTable,ACPI_PHYSICAL_ADDRESS   *NewAddress,UINT32                  *NewTableLength)
{

    return (AE_SUPPORT);
}


/******************************************************************************
 *
 * FUNCTION:    AcpiOsGetTimer
 *
 * PARAMETERS:  None
 *
 * RETURN:      Current ticks in 100-nanosecond units
 *
 * DESCRIPTION: Get the value of a system timer
 *
 ******************************************************************************/

OS_API_C_FUNC(UINT64 ) AcpiOsGetTimer (void)
{
 
	UINT64 vv=0;


	return vv;
}


/******************************************************************************
 *
 * FUNCTION:    AcpiOsReadable
 *
 * PARAMETERS:  Pointer             - Area to be verified
 *              Length              - Size of area
 *
 * RETURN:      TRUE if readable for entire length
 *
 * DESCRIPTION: Verify that a pointer is valid for reading
 *
 *****************************************************************************/

OS_API_C_FUNC(BOOLEAN ) AcpiOsReadable (void                    *Pointer,ACPI_SIZE               Length)
{

    return 1;
}


/******************************************************************************
 *
 * FUNCTION:    AcpiOsWritable
 *
 * PARAMETERS:  Pointer             - Area to be verified
 *              Length              - Size of area
 *
 * RETURN:      TRUE if writable for entire length
 *
 * DESCRIPTION: Verify that a pointer is valid for writing
 *
 *****************************************************************************/

OS_API_C_FUNC(BOOLEAN ) AcpiOsWritable (void                    *Pointer,ACPI_SIZE               Length)
{

    return 1;
}


/******************************************************************************
 *
 * FUNCTION:    AcpiOsRedirectOutput
 *
 * PARAMETERS:  Destination         - An open file handle/pointer
 *
 * RETURN:      None
 *
 * DESCRIPTION: Causes redirect of AcpiOsPrintf and AcpiOsVprintf
 *
 *****************************************************************************/

OS_API_C_FUNC(void ) AcpiOsRedirectOutput (void                    *Destination)
{

    //AcpiGbl_OutputFile = Destination;
}


/******************************************************************************
 *
 * FUNCTION:    AcpiOsPrintf
 *
 * PARAMETERS:  Fmt, ...            - Standard printf format
 *
 * RETURN:      None
 *
 * DESCRIPTION: Formatted output
 *
 *****************************************************************************/

void ACPI_INTERNAL_VAR_XFACE AcpiOsPrintf (const char              *Fmt,...)
{
    va_list                 Args;
    va_start				(Args, Fmt);

	writestr_vfmt			(Fmt,Args);

	va_end					(Args);

	

    return;
}


/******************************************************************************
 *
 * FUNCTION:    AcpiOsVprintf
 *
 * PARAMETERS:  Fmt                 - Standard printf format
 *              Args                - Argument list
 *
 * RETURN:      None
 *
 * DESCRIPTION: Formatted output with argument list pointer
 *
 *****************************************************************************/

OS_API_C_FUNC(void ) AcpiOsVprintf (const char              *Fmt,va_list                 Args)
{
	writestr_vfmt			(Fmt,Args);

	//writestr(Fmt);
 
    return;
}


/******************************************************************************
 *
 * FUNCTION:    AcpiOsGetLine
 *
 * PARAMETERS:  Buffer              - Where to return the command line
 *              BufferLength        - Maximum length of Buffer
 *              BytesRead           - Where the actual byte count is returned
 *
 * RETURN:      Status and actual bytes read
 *
 * DESCRIPTION: Formatted input with argument list pointer
 *
 *****************************************************************************/

OS_API_C_FUNC(ACPI_STATUS ) AcpiOsGetLine (char                    *Buffer,UINT32                  BufferLength,UINT32                  *BytesRead)
{
  
    return (AE_OK);
}


/******************************************************************************
 *
 * FUNCTION:    AcpiOsMapMemory
 *
 * PARAMETERS:  Where               - Physical address of memory to be mapped
 *              Length              - How much memory to map
 *
 * RETURN:      Pointer to mapped memory. Null on error.
 *
 * DESCRIPTION: Map physical memory into caller's address space
 *
 *****************************************************************************/

OS_API_C_FUNC(void *) AcpiOsMapMemory (ACPI_PHYSICAL_ADDRESS   Where,ACPI_SIZE               Length)
{

    return (ACPI_TO_POINTER ((ACPI_SIZE) Where));
}


/******************************************************************************
 *
 * FUNCTION:    AcpiOsUnmapMemory
 *
 * PARAMETERS:  Where               - Logical address of memory to be unmapped
 *              Length              - How much memory to unmap
 *
 * RETURN:      None.
 *
 * DESCRIPTION: Delete a previously created mapping. Where and Length must
 *              correspond to a previous mapping exactly.
 *
 *****************************************************************************/

OS_API_C_FUNC(void ) AcpiOsUnmapMemory (void                    *Where,ACPI_SIZE               Length)
{

    return;
}


/******************************************************************************
 *
 * FUNCTION:    AcpiOsAllocate
 *
 * PARAMETERS:  Size                - Amount to allocate, in bytes
 *
 * RETURN:      Pointer to the new allocation. Null on error.
 *
 * DESCRIPTION: Allocate memory. Algorithm is dependent on the OS.
 *
 *****************************************************************************/

OS_API_C_FUNC(void *)  AcpiOsAllocate (ACPI_SIZE               Size)
{
    void                    *Mem;

    Mem = (void *) malloc_c ((size_t) Size);

    return (Mem);
}


/******************************************************************************
 *
 * FUNCTION:    AcpiOsFree
 *
 * PARAMETERS:  Mem                 - Pointer to previously allocated memory
 *
 * RETURN:      None.
 *
 * DESCRIPTION: Free memory allocated via AcpiOsAllocate
 *
 *****************************************************************************/

OS_API_C_FUNC(void ) AcpiOsFree ( void   *Mem)
{

    free_c (Mem);
}


#ifdef ACPI_SINGLE_THREADED
/******************************************************************************
 *
 * FUNCTION:    Semaphore stub functions
 *
 * DESCRIPTION: Stub functions used for single-thread applications that do
 *              not require semaphore synchronization. Full implementations
 *              of these functions appear after the stubs.
 *
 *****************************************************************************/

OS_API_C_FUNC(ACPI_STATUS )  AcpiOsCreateSemaphore (UINT32              MaxUnits,UINT32              InitialUnits,ACPI_HANDLE         *OutHandle)
{
    *OutHandle = (ACPI_HANDLE) 1;
    return (AE_OK);
}

OS_API_C_FUNC(ACPI_STATUS ) AcpiOsDeleteSemaphore (ACPI_HANDLE         Handle)
{
    return (AE_OK);
}

OS_API_C_FUNC(ACPI_STATUS ) AcpiOsWaitSemaphore (ACPI_HANDLE         Handle,UINT32              Units,UINT16              Timeout)
{
    return (AE_OK);
}

OS_API_C_FUNC(ACPI_STATUS ) AcpiOsSignalSemaphore (ACPI_HANDLE         Handle,UINT32              Units)
{
    return (AE_OK);
}

#else
/******************************************************************************
 *
 * FUNCTION:    AcpiOsCreateSemaphore
 *
 * PARAMETERS:  MaxUnits            - Maximum units that can be sent
 *              InitialUnits        - Units to be assigned to the new semaphore
 *              OutHandle           - Where a handle will be returned
 *
 * RETURN:      Status
 *
 * DESCRIPTION: Create an OS semaphore
 *
 *****************************************************************************/

OS_API_C_FUNC(ACPI_STATUS) AcpiOsCreateSemaphore (
    UINT32              MaxUnits,
    UINT32              InitialUnits,
    ACPI_SEMAPHORE      *OutHandle)
{
    void                *Mutex;
    UINT32              i;

    ACPI_FUNCTION_NAME (OsCreateSemaphore);




    if (MaxUnits == ACPI_UINT32_MAX)
    {
        MaxUnits = 255;
    }

    if (InitialUnits == ACPI_UINT32_MAX)
    {
        InitialUnits = MaxUnits;
    }

    if (InitialUnits > MaxUnits)
    {
        return (AE_BAD_PARAMETER);
    }

    /* Find an empty slot */

    for (i = 0; i < ACPI_OS_MAX_SEMAPHORES; i++)
    {
        if (!AcpiGbl_Semaphores[i].OsHandle)
        {
            break;
        }
    }
    if (i >= ACPI_OS_MAX_SEMAPHORES)
    {
        writestr ("Reached max semaphores (%u), could not create \n");
        return (AE_LIMIT);
    }

    /* Create an OS semaphore */

    //Mutex = CreateSemaphore (NULL, InitialUnits, MaxUnits, NULL);

	Mutex	=	task_manager_new_semaphore(InitialUnits, MaxUnits);
    if (!Mutex)
    {
        writestr ("Could not create semaphore \n");
        return (AE_NO_MEMORY);
    }

    AcpiGbl_Semaphores[i].MaxUnits		= (UINT16) MaxUnits;
    AcpiGbl_Semaphores[i].CurrentUnits	= (UINT16) InitialUnits;
    AcpiGbl_Semaphores[i].OsHandle		= Mutex;

    ACPI_DEBUG_PRINT ((ACPI_DB_MUTEX, "Handle=%u, Max=%u, Current=%u, OsHandle=%p\n",i, MaxUnits, InitialUnits, Mutex));

    *OutHandle = (void *) uint_to_mem(i);

    return (AE_OK);
}


/******************************************************************************
 *
 * FUNCTION:    AcpiOsDeleteSemaphore
 *
 * PARAMETERS:  Handle              - Handle returned by AcpiOsCreateSemaphore
 *
 * RETURN:      Status
 *
 * DESCRIPTION: Delete an OS semaphore
 *
 *****************************************************************************/

OS_API_C_FUNC(ACPI_STATUS) AcpiOsDeleteSemaphore (ACPI_SEMAPHORE      Handle)
{
    UINT32              Index = (UINT32) mem_to_uint(Handle);

    if ((Index >= ACPI_OS_MAX_SEMAPHORES) ||
        !AcpiGbl_Semaphores[Index].OsHandle)
    {
        return (AE_BAD_PARAMETER);
    }

    task_manager_delete_semaphore (AcpiGbl_Semaphores[Index].OsHandle);
    AcpiGbl_Semaphores[Index].OsHandle = NULL;
    return (AE_OK);
}


/******************************************************************************
 *
 * FUNCTION:    AcpiOsWaitSemaphore
 *
 * PARAMETERS:  Handle              - Handle returned by AcpiOsCreateSemaphore
 *              Units               - How many units to wait for
 *              Timeout             - How long to wait
 *
 * RETURN:      Status
 *
 * DESCRIPTION: Wait for units
 *
 *****************************************************************************/

OS_API_C_FUNC(ACPI_STATUS) AcpiOsWaitSemaphore (
    ACPI_SEMAPHORE      Handle,
    UINT32              Units,
    UINT16              Timeout)
{
    UINT32              Index = (UINT32)mem_to_uint( Handle);
    UINT32              WaitStatus;
    UINT32              OsTimeout = Timeout;


    ACPI_FUNCTION_ENTRY ();
	


    if ((Index >= ACPI_OS_MAX_SEMAPHORES) ||
        !AcpiGbl_Semaphores[Index].OsHandle)
    {
        return (AE_BAD_PARAMETER);
    }

    if (Units > 1)
    {
        //printf ("WaitSemaphore: Attempt to receive %u units\n", Units);
		writestr("WaitSemaphore: Attempt to receive ");
		writeint(Units,10);
		writestr(" units\n");

        return (AE_NOT_IMPLEMENTED);
    }

    if (Timeout == ACPI_WAIT_FOREVER)
    {
        OsTimeout = 0;//INFINITE;
    }
    else
    {
        /* Add 10ms to account for clock tick granularity */

        OsTimeout += 10;
    }

    WaitStatus = task_manager_aquire_semaphore (AcpiGbl_Semaphores[Index].OsHandle, OsTimeout);
    if (WaitStatus ==0)// WAIT_TIMEOUT)
    {
        return (AE_TIME);
    }	

    if (AcpiGbl_Semaphores[Index].CurrentUnits == 0)
    {
        writestr("%s - No unit received. Timeout 0x%X, OS_Status 0x%X");//,"Mutex"/*AcpiUtGetMutexName (Index)*/, Timeout, WaitStatus));

        return (AE_OK);
    }

    AcpiGbl_Semaphores[Index].CurrentUnits--;
    return (AE_OK);
}


/******************************************************************************
 *
 * FUNCTION:    AcpiOsSignalSemaphore
 *
 * PARAMETERS:  Handle              - Handle returned by AcpiOsCreateSemaphore
 *              Units               - Number of units to send
 *
 * RETURN:      Status
 *
 * DESCRIPTION: Send units
 *
 *****************************************************************************/

OS_API_C_FUNC(ACPI_STATUS) AcpiOsSignalSemaphore (ACPI_SEMAPHORE      Handle,UINT32              Units)
{
    UINT32              Index = (UINT32) mem_to_uint(Handle);


    ACPI_FUNCTION_ENTRY ();
	
    if (Index >= ACPI_OS_MAX_SEMAPHORES)
    {
        writestr ("SignalSemaphore: Index/Handle out of range: %2.2X\n");
        return (AE_BAD_PARAMETER);
    }

    if (!AcpiGbl_Semaphores[Index].OsHandle)
    {
        writestr ("SignalSemaphore: Null OS handle, Index %2.2X\n");
        return (AE_BAD_PARAMETER);
    }

    if (Units > 1)
    {
        writestr ("SignalSemaphore: Attempt to signal %u units, Index %2.2X\n");
        return (AE_NOT_IMPLEMENTED);
    }

    if ((AcpiGbl_Semaphores[Index].CurrentUnits + 1) >AcpiGbl_Semaphores[Index].MaxUnits)
    {
        writestr("Oversignalled semaphore[%u]! Current %u Max %u \n");
        return (AE_LIMIT);
    }

    AcpiGbl_Semaphores[Index].CurrentUnits++;
    task_manager_release_semaphore (AcpiGbl_Semaphores[Index].OsHandle, Units);

    return (AE_OK);
}

#endif /* ACPI_SINGLE_THREADED */


/******************************************************************************
 *
 * FUNCTION:    Spinlock interfaces
 *
 * DESCRIPTION: Map these interfaces to semaphore interfaces
 *
 *****************************************************************************/

OS_API_C_FUNC(ACPI_STATUS ) AcpiOsCreateLock (ACPI_SPINLOCK           *OutHandle)
{
    return (AcpiOsCreateSemaphore (1, 1, OutHandle));
}

OS_API_C_FUNC(void ) AcpiOsDeleteLock (ACPI_SPINLOCK           Handle)
{
    AcpiOsDeleteSemaphore (Handle);
}

OS_API_C_FUNC(ACPI_CPU_FLAGS ) AcpiOsAcquireLock (ACPI_SPINLOCK           Handle)
{
    AcpiOsWaitSemaphore (Handle, 1, 0xFFFF);
    return (0);
}

OS_API_C_FUNC(void ) AcpiOsReleaseLock (ACPI_SPINLOCK           Handle,ACPI_CPU_FLAGS          Flags)
{
    AcpiOsSignalSemaphore (Handle, 1);
}


#if ACPI_FUTURE_IMPLEMENTATION

/* Mutex interfaces, just implement with a semaphore */

OS_API_C_FUNC(ACPI_STATUS	) AcpiOsCreateMutex (ACPI_MUTEX              *OutHandle)
{
    return (AcpiOsCreateSemaphore (1, 1, OutHandle));
}

OS_API_C_FUNC(void ) AcpiOsDeleteMutex (ACPI_MUTEX              Handle)
{
    AcpiOsDeleteSemaphore (Handle);
}

OS_API_C_FUNC(ACPI_STATUS ) AcpiOsAcquireMutex (ACPI_MUTEX              Handle,UINT16                  Timeout)
{
    AcpiOsWaitSemaphore (Handle, 1, Timeout);
    return (0);
}

OS_API_C_FUNC(void ) AcpiOsReleaseMutex (ACPI_MUTEX              Handle)
{
    AcpiOsSignalSemaphore (Handle, 1);
}
#endif


/******************************************************************************
 *
 * FUNCTION:    AcpiOsInstallInterruptHandler
 *
 * PARAMETERS:  InterruptNumber     - Level handler should respond to.
 *              ServiceRoutine      - Address of the ACPI interrupt handler
 *              Context             - User context
 *
 * RETURN:      Handle to the newly installed handler.
 *
 * DESCRIPTION: Install an interrupt handler. Used to install the ACPI
 *              OS-independent handler.
 *
 *****************************************************************************/

OS_API_C_FUNC(UINT32 ) AcpiOsInstallInterruptHandler (UINT32                  InterruptNumber,ACPI_OSD_HANDLER        ServiceRoutine,void                    *Context)
{

	setup_interupt_c	(InterruptNumber,ServiceRoutine,mem_to_uint(Context));

	writestr("acpi interrupt [");
	writeint(InterruptNumber,16);
	writestr("]\n");
	
    return (AE_OK);
}


/******************************************************************************
 *
 * FUNCTION:    AcpiOsRemoveInterruptHandler
 *
 * PARAMETERS:  Handle              - Returned when handler was installed
 *
 * RETURN:      Status
 *
 * DESCRIPTION: Uninstalls an interrupt handler.
 *
 *****************************************************************************/

OS_API_C_FUNC(ACPI_STATUS	) AcpiOsRemoveInterruptHandler (UINT32                  InterruptNumber,ACPI_OSD_HANDLER        ServiceRoutine)
{
	writestr("acpi remove interrupt [");
	writeint(InterruptNumber,16);
	writestr("]\n");
	


	setup_interupt_c	(InterruptNumber,PTR_NULL,0);

    return (AE_OK);
}


/******************************************************************************
 *
 * FUNCTION:    AcpiOsStall
 *
 * PARAMETERS:  Microseconds        - Time to stall
 *
 * RETURN:      None. Blocks until stall is completed.
 *
 * DESCRIPTION: Sleep at microsecond granularity
 *
 *****************************************************************************/

OS_API_C_FUNC(void ) AcpiOsStall (UINT32                  Microseconds)
{

    //Sleep ((Microseconds / ACPI_USEC_PER_MSEC) + 1);

	snooze(Microseconds);
    return;
}


/******************************************************************************
 *
 * FUNCTION:    AcpiOsSleep
 *
 * PARAMETERS:  Milliseconds        - Time to sleep
 *
 * RETURN:      None. Blocks until sleep is completed.
 *
 * DESCRIPTION: Sleep at millisecond granularity
 *
 *****************************************************************************/

OS_API_C_FUNC(void ) AcpiOsSleep (UINT64                  Milliseconds)
{

    /* Add 10ms to account for clock tick granularity */

    snooze ((((unsigned long) Milliseconds) + 10)*1000);
    return;
}


/******************************************************************************
 *
 * FUNCTION:    AcpiOsReadPciConfiguration
 *
 * PARAMETERS:  PciId               - Seg/Bus/Dev
 *              Register            - Device Register
 *              Value               - Buffer where value is placed
 *              Width               - Number of bits
 *
 * RETURN:      Status
 *
 * DESCRIPTION: Read data from PCI configuration space
 *
 *****************************************************************************/

OS_API_C_FUNC(ACPI_STATUS ) AcpiOsReadPciConfiguration (
    ACPI_PCI_ID             *PciId,
    UINT32                  Register,
    UINT64                  *Value,
    UINT32                  Width)
{

	switch(Width)
	{
		case 32:
			(*Value)=read_pci_dword_c	(PciId->Bus, PciId->Device, PciId->Function,Register );
		break;
		case 16:
			(*Value)=read_pci_word_c	(PciId->Bus, PciId->Device, PciId->Function,Register );
		break;
		case 8:
			(*Value)=read_pci_byte_c	(PciId->Bus, PciId->Device, PciId->Function,Register );
		break;
		default:
			*Value = 0;
			return (AE_ERROR);
		break;
	}

    return (AE_OK);
}


/******************************************************************************
 *
 * FUNCTION:    AcpiOsWritePciConfiguration
 *
 * PARAMETERS:  PciId               - Seg/Bus/Dev
 *              Register            - Device Register
 *              Value               - Value to be written
 *              Width               - Number of bits
 *
 * RETURN:      Status
 *
 * DESCRIPTION: Write data to PCI configuration space
 *
 *****************************************************************************/

OS_API_C_FUNC(ACPI_STATUS ) AcpiOsWritePciConfiguration (
    ACPI_PCI_ID             *PciId,
    UINT32                  Register,
    UINT64                  Value,
    UINT32                  Width)
{

	writestr("write pci conf \n");
    return (AE_OK);
}


/******************************************************************************
 *
 * FUNCTION:    AcpiOsReadPort
 *
 * PARAMETERS:  Address             - Address of I/O port/register to read
 *              Value               - Where value is placed
 *              Width               - Number of bits
 *
 * RETURN:      Value read from port
 *
 * DESCRIPTION: Read data from an I/O port or register
 *
 *****************************************************************************/

OS_API_C_FUNC(ACPI_STATUS ) AcpiOsReadPort (ACPI_IO_ADDRESS         Address,UINT32                  *Value,UINT32                  Width)
{

	int ret;
	switch(Width)
	{
		case 8:
			*Value=in_8_c							(Address);
			 ret= (AE_OK);
		break;
		case 16:
			*Value=in_16_c							(Address);
			 ret=(AE_OK);
		break;
		case 32:
			*Value=in_32_c							(Address);
			ret=(AE_OK);
		break;
		default:
			ret=AE_ERROR;
		break;

	}


  
    return ret;
}


/******************************************************************************
 *
 * FUNCTION:    AcpiOsWritePort
 *
 * PARAMETERS:  Address             - Address of I/O port/register to write
 *              Value               - Value to write
 *              Width               - Number of bits
 *
 * RETURN:      None
 *
 * DESCRIPTION: Write data to an I/O port or register
 *
 *****************************************************************************/

OS_API_C_FUNC(ACPI_STATUS ) AcpiOsWritePort (ACPI_IO_ADDRESS         Address,UINT32                  Value,UINT32                  Width)
{
	int ret;
	switch(Width)
	{
		case 8:
			out_8_c							(Address,Value);
			 ret= (AE_OK);
		break;
		case 16:
			out_16_c							(Address,Value);
			 ret=(AE_OK);
		break;
		case 32:
			out_32_c							(Address,Value);
			 ret=(AE_OK);
		break;
		default:
			ret=AE_ERROR;
		break;

	}


	return ret;
}


/******************************************************************************
 *
 * FUNCTION:    AcpiOsReadMemory
 *
 * PARAMETERS:  Address             - Physical Memory Address to read
 *              Value               - Where value is placed
 *              Width               - Number of bits (8,16,32, or 64)
 *
 * RETURN:      Value read from physical memory address. Always returned
 *              as a 64-bit integer, regardless of the read width.
 *
 * DESCRIPTION: Read data from a physical memory address
 *
 *****************************************************************************/

OS_API_C_FUNC(ACPI_STATUS ) AcpiOsReadMemory (ACPI_PHYSICAL_ADDRESS   Address,UINT64                  *Value,UINT32                  Width)
{

    switch (Width)
    {
    case 8:
        *Value = *((unsigned char *)(uint_to_mem(Address)));
    break;
	case 16:
        *Value = *((unsigned short *)(uint_to_mem(Address)));
    break;
    case 32:
        *Value = *((unsigned int *)(uint_to_mem(Address)));
    break;
    case 64:
        *Value = *((unsigned char *)(uint_to_mem(Address)));
        break;

    default:

        return (AE_BAD_PARAMETER);
        break;
    }

    return (AE_OK);
}


/******************************************************************************
 *
 * FUNCTION:    AcpiOsWriteMemory
 *
 * PARAMETERS:  Address             - Physical Memory Address to write
 *              Value               - Value to write
 *              Width               - Number of bits (8,16,32, or 64)
 *
 * RETURN:      None
 *
 * DESCRIPTION: Write data to a physical memory address
 *
 *****************************************************************************/

OS_API_C_FUNC(ACPI_STATUS ) AcpiOsWriteMemory (ACPI_PHYSICAL_ADDRESS   Address,UINT64                  Value,UINT32                  Width)
{
switch (Width)
    {
    case 8:
        *((unsigned char *)(uint_to_mem(Address)))	=Value;
    break;
	case 16:
        *((unsigned short *)(uint_to_mem(Address)))	=Value;
    break;
    case 32:
        *((unsigned int *)(uint_to_mem(Address)))	=Value;
    break;
    case 64:
        *((unsigned int *)(uint_to_mem(Address)))	=Value;
     break;

    default:
        return (AE_BAD_PARAMETER);
       break;
    }

    
    return (AE_OK);
}


/******************************************************************************
 *
 * FUNCTION:    AcpiOsSignal
 *
 * PARAMETERS:  Function            - ACPI CA signal function code
 *              Info                - Pointer to function-dependent structure
 *
 * RETURN:      Status
 *
 * DESCRIPTION: Miscellaneous functions. Example implementation only.
 *
 *****************************************************************************/

OS_API_C_FUNC(ACPI_STATUS ) AcpiOsSignal (UINT32                  Function,void                    *Info)
{

    switch (Function)
    {
    case ACPI_SIGNAL_FATAL:

        break;

    case ACPI_SIGNAL_BREAKPOINT:

        break;

    default:

        break;
    }

    return (AE_OK);
}


/******************************************************************************
 *
 * FUNCTION:    Local cache interfaces
 *
 * DESCRIPTION: Implements cache interfaces via malloc/free for testing
 *              purposes only.
 *
 *****************************************************************************/

#ifndef ACPI_USE_LOCAL_CACHE

OS_API_C_FUNC(ACPI_STATUS) AcpiOsCreateCache (
    char                    *CacheName,
    UINT16                  ObjectSize,
    UINT16                  MaxDepth,
    ACPI_CACHE_T            **ReturnCache)
{
    ACPI_MEMORY_LIST        *NewCache;


    NewCache = malloc_c (sizeof (ACPI_MEMORY_LIST));
    if (!NewCache)
    {
        return (AE_NO_MEMORY);
    }

    memset_c (NewCache, 0, sizeof (ACPI_MEMORY_LIST));
    NewCache->ListName = CacheName;
    NewCache->ObjectSize = ObjectSize;
    NewCache->MaxDepth = MaxDepth;

    *ReturnCache = (ACPI_CACHE_T) NewCache;
    return (AE_OK);
}

OS_API_C_FUNC(ACPI_STATUS) AcpiOsDeleteCache (
    ACPI_CACHE_T            *Cache)
{
    free_c (Cache);
    return (AE_OK);
}

OS_API_C_FUNC(ACPI_STATUS) AcpiOsPurgeCache (
    ACPI_CACHE_T            *Cache)
{
    return (AE_OK);
}
/*
OS_API_C_FUNC(void *) AcpiOsAcquireObject (ACPI_CACHE_T            *Cache)
{
    void                    *NewObject;

    NewObject = malloc_c (((ACPI_MEMORY_LIST *) Cache)->ObjectSize);
    memset_c (NewObject, 0, ((ACPI_MEMORY_LIST *) Cache)->ObjectSize);

    return (NewObject);
}

OS_API_C_FUNC(ACPI_STATUS) AcpiOsReleaseObject (
    ACPI_CACHE_T            *Cache,
    void                    *Object)
{
    free_c (Object);
    return (AE_OK);
}
*/
#endif /* ACPI_USE_LOCAL_CACHE */


/* Optional multi-thread support */

#ifndef ACPI_SINGLE_THREADED
/******************************************************************************
 *
 * FUNCTION:    AcpiOsGetThreadId
 *
 * PARAMETERS:  None
 *
 * RETURN:      Id of the running thread
 *
 * DESCRIPTION: Get the Id of the current (running) thread
 *
 *****************************************************************************/

OS_API_C_FUNC(ACPI_THREAD_ID) AcpiOsGetThreadId (void)
{
    unsigned int   ThreadId;

    /* Ensure ID is never 0 */

    ThreadId = task_manager_get_current_task_id ();
    return ((ACPI_THREAD_ID) (ThreadId + 1));
}


/******************************************************************************
 *
 * FUNCTION:    AcpiOsExecute
 *
 * PARAMETERS:  Type                - Type of execution
 *              Function            - Address of the function to execute
 *              Context             - Passed as a parameter to the function
 *
 * RETURN:      Status
 *
 * DESCRIPTION: Execute a new thread
 *
 *****************************************************************************/

OS_API_C_FUNC(ACPI_STATUS) AcpiOsExecute (
    ACPI_EXECUTE_TYPE       Type,
    ACPI_OSD_EXEC_CALLBACK  Function,
    void                    *Context)
{
	task_manager_new_task("ACPI_CA",Function,PTR_NULL,Context);

    //_beginthread (Function, (unsigned) 0, Context);
    return (1);
}

#endif /* ACPI_SINGLE_THREADED */


/******************************************************************************
 *
 * FUNCTION:    AcpiOsWaitEventsComplete
 *
 * PARAMETERS:  None
 *
 * RETURN:      None
 *
 * DESCRIPTION: Wait for all asynchronous events to complete. This
 *              implementation does nothing.
 *
 *****************************************************************************/

OS_API_C_FUNC(void) AcpiOsWaitEventsComplete (
    void)
{
    return;
}
