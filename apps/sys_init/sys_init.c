#define APP_DLL_FUNC C_EXPORT
#include <std_def.h>
#include <std_mem.h>
#include "kern.h"
#include "mem_base.h"
#include "lib_c.h"
#include "tree.h"
#include "sys/async_stream.h"
#include "sys/stream_dev.h"
#include "sys/mem_stream.h"
#include "sys/file_system.h"
#include "sys/tpo_mod.h"
#include "sys/task.h"
#include "../../kernel/bus_manager/bus_drv.h"
#include "sys/ndis.h"
#include "../../graphic_base/filters/filters.h"
#include "gfx/graphic_object.h"
#include "gfx/graphic_base.h"
#include "../../zlib-1.2.3/zlib.h"

GFX_API		 int C_API_FUNC init_font_loader();


unsigned int			pci_bus_drv_id						= 	0xFFFFFFFF;
unsigned int			usb_bus_drv_id						= 	0xFFFFFFFF;
unsigned int			kernel_log_id						= 	0xFFFFFFFF;

unsigned int			loop_done							=	0xFFFFFFFF;
unsigned int			render_done							=	0xFFFFFFFF;
int setup_txt_dev()
{
	unsigned int					vga_dev_id;
	unsigned int					fmt_cnt;
	unsigned int					fmt_ptr[8];
	unsigned int					fmt_id;
	unsigned int					retry;

	
	vga_dev_id					=	bus_manager_find_device_by_type			(pci_bus_drv_id,DEVICE_TYPE_VGA,0);
	if(vga_dev_id==0xFFFFFFFF)
	{
		kernel_log(kernel_log_id,"no vga device found \n");
		return 0;
	}
		
	//kernel_log(kernel_log_id,"vga modes :");
	//toggle_output_buffer_c(0);
	fmt_cnt	=0;
	fmt_id	=0xFFFF;
	while(bus_manager_get_bus_device_format(pci_bus_drv_id,vga_dev_id,fmt_cnt,fmt_ptr)==1)
	{
		/*
		writeint(fmt_ptr[0],10);
		writestr("x");
		writeint(fmt_ptr[1],10);
		writestr("x");
		writeint(fmt_ptr[3],10);
		writestr(" ");
		writeint(fmt_ptr[5],16);
		writestr(" ");
		writeint(fmt_ptr[4],2);
		writestr(" ");
		writeint(fmt_ptr[6],16);
		writestr(" *|* ");
		*/
		
		if((fmt_ptr[0]==80)&&(fmt_ptr[1]==25)&&(fmt_ptr[3]==4))
		{
			fmt_id=fmt_cnt;
		}

		fmt_cnt++;
	}
	//writestr("\n");

	
	if(fmt_id==0xFFFF)
	{
		toggle_output_buffer_c(1);
		kernel_log(kernel_log_id,"format not found \n"); 
		return 0;
	}


	//toggle_output_buffer_c(0);
	retry=0;
	while(bus_manager_set_bus_device_format(pci_bus_drv_id,vga_dev_id,fmt_id)!=1)
	{
		if(retry>=5)
		{
			toggle_output_buffer_c(1);
			return 0;
		}
		retry++;
	}
	
	

	return 1;
	
}

void select_mode()
{

}

int setup_gfx_dev()
{
	unsigned int					vga_dev_id;
	unsigned int					fmt_cnt;
	unsigned int					fmt_ptr[8];
	unsigned int					fmt_id;
	unsigned int					retry;

	
	vga_dev_id					=	bus_manager_find_device_by_type			(pci_bus_drv_id,DEVICE_TYPE_VGA,0);
	if(vga_dev_id==0xFFFFFFFF)
	{
		kernel_log(kernel_log_id,"no vesa device found \n");
		return 0;
	}
	
	fmt_cnt	=0;
	fmt_id	=0xFFFF;
	while(bus_manager_get_bus_device_format(pci_bus_drv_id,vga_dev_id,fmt_cnt,fmt_ptr)==1)
	{
		if((fmt_ptr[0]==800)&&(fmt_ptr[1]==600)&&(fmt_ptr[3]==32))
		{
			fmt_id=fmt_cnt;
		}

		//writestr_fmt(" %dx%d %d bpp %d bpl \n",fmt_ptr[0],fmt_ptr[1],fmt_ptr[3],fmt_ptr[2]);
		fmt_cnt++;
	}
		
	if(fmt_id==0xFFFF)
	{
		kernel_log(kernel_log_id,"format not found \n"); 
		return 0;
	}

	
	toggle_output_buffer_c(0);
	retry=0;
	while(bus_manager_set_bus_device_format(pci_bus_drv_id,vga_dev_id,fmt_id)!=1)
	{
		if(retry>=5)
		{
			toggle_output_buffer_c(1);
			return 0;
		}
		retry++;
	}
	
		
	return 1;
}

OS_API_C_FUNC(int)	mouse_event(device_type_t	dev_type,const mem_zone_ref	*hid_infos)
{
	int				text_scroll;
	unsigned int	value;
	unsigned int	w_rel,y_rel;
	int				signed_value;

	mem_zone_ref	x_node		={PTR_NULL};
	mem_zone_ref	y_node		={PTR_NULL};
	mem_zone_ref	w_node		={PTR_NULL};
	mem_zone_ref	input_node	={PTR_NULL};
	mem_zone_ref	dtype		={PTR_NULL};

	

	tree_find_child_node_by_member_name(hid_infos,NODE_HID_INPUT,NODE_HID_USAGE,"X"		,	&x_node);
	tree_find_child_node_by_member_name(hid_infos,NODE_HID_INPUT,NODE_HID_USAGE,"Y"		,	&y_node);
	tree_find_child_node_by_member_name(hid_infos,NODE_HID_INPUT,NODE_HID_USAGE,"Wheel"	,	&w_node);

	
	


	text_scroll	=	get_char_buffer_ofset_c	();


	tree_node_find_child_by_name			(&y_node	,"DATA TYPE"	,&dtype);
	y_rel=tree_node_find_child_by_name		(&dtype		,"REL",PTR_NULL);
	release_zone_ref						(&dtype);

	tree_node_find_child_by_name			(&w_node	,"DATA TYPE"	,&dtype);
	w_rel=tree_node_find_child_by_name		(&dtype		,"REL",PTR_NULL);
	release_zone_ref						(&dtype);


	switch(tree_manager_get_child_type(&y_node,NODE_HASH("last value")))
	{
		case NODE_GFX_INT:
			tree_manager_get_child_value_i32		(&y_node,NODE_HASH("last value"),&value);

			if(y_rel)
				text_scroll	+=  value;
			else
				text_scroll	 =  value;

		break;
		
		case NODE_GFX_SIGNED_INT:		
			tree_manager_get_child_value_si32		(&y_node,NODE_HASH("last value"),&signed_value);

			if(y_rel)
				text_scroll	+=  signed_value;
			else
				text_scroll	 =  signed_value;

		break;
	}

	switch(tree_manager_get_child_type(&w_node,NODE_HASH("last value")))
	{
		case NODE_GFX_INT:

			tree_manager_get_child_value_si32		(&w_node,NODE_HASH("last value"),&value);
			

			if(w_rel)
				text_scroll	+=  value;
			else
				text_scroll	 =  value;
		break;
		
		case NODE_GFX_SIGNED_INT:		
			tree_manager_get_child_value_si32		(&w_node,NODE_HASH("last value"),&signed_value);

			if(w_rel)
				text_scroll	+=  signed_value;
			else
				text_scroll	 =  signed_value;
		break;
	}


	
	set_char_buffer_ofset_c	(text_scroll);

	release_zone_ref(&x_node);
	release_zone_ref(&y_node);
	release_zone_ref(&w_node);

		
	
	return 1;

}




void dump_storage()
{
	unsigned int 					n_dev=0;
	unsigned int 					storage_dev_id=0;
	unsigned int 					n_str=0;

	n_dev=0;
	while((storage_dev_id=	bus_manager_find_device_by_type			(pci_bus_drv_id,DEVICE_TYPE_STORAGE,n_dev))!=0xFFFFFFFF)
	{
		mem_zone_ref infos;
		infos.zone=PTR_NULL;
		n_str=0;
		while(bus_manager_get_dev_str_infos (pci_bus_drv_id,storage_dev_id,n_str,&infos)!=0)
		{
			if(infos.zone!=PTR_NULL)
			{
				unsigned int 					disk_win_id;
				char txt[128];

				tree_manager_get_child_value_str(&infos,NODE_HASH("model"),txt,128,0);
				disk_win_id		=	gfx_create_render_container	(txt,10,80+n_str*60,64,32);
				
			}
			n_str++;
			release_zone_ref(&infos);
		}
		n_dev++;
	}
}

unsigned int 					do_render=0xFFFFFFFF;

void render_thread(unsigned int vga_dev_render_id)
{
	unsigned int 					time,time2;
	time2			=0;
	render_done		=0;
	do_render		=1;
	get_system_time_c					(&time);

	writestr										("thread !\n");
	
	while(!loop_done)
	{
		if(do_render==1)
			gfx_render_all_containers(vga_dev_render_id);
		else
			gfx_render_all_containers(0);
	}

	writestr										("thread exit!\n");
	render_done=1;
}



unsigned int	 keyb_hdl(void *data)
{
	unsigned int text_scroll;
	char b,c,f;

	b			=	in_8_c(0x60);
	c			=	b &(0x7F);
	f			=	b &(0x80);

	
	//loop_done=1;
	text_scroll	=	get_char_buffer_ofset_c	();

	switch(c)
	{
		case 0x50:
			set_char_buffer_ofset_c	(text_scroll+1);
		break;
		case 0x48:
			set_char_buffer_ofset_c	(text_scroll-1);
		break;
		case 0x2F:
			do_render			=0;
			bios_set_vga_mode_c(0x03);
			toggle_output_buffer_c(1);
			writestr("\n");
		break;
		case 0x1F:
			setup_gfx_dev();
			do_render			=1;
		break;
		case 0x14:
			if(f)
				task_manager_dump();
		break;
		default:
			writeint(c,16);
			writestr("\n");
		break;

	}

	
		
	//loop_done=1;
	return 1;
}


OS_API_C_FUNC(int) run_func()
{
	char							font_name[32];
	tpo_mod_file					klog_mod, dev_menu_mod, fs_menu_mod;
	mem_zone_ref					pci_root = { PTR_NULL }, pci_str_buffer	={PTR_NULL};
	unsigned int					vga_dev_render_id;
	unsigned int					vga_dev_id;
	unsigned int					audio_dev_id;
	unsigned int					audio_stream_dev_stream_id;
	unsigned int					start_time;
	unsigned int 					b;
	unsigned int					i;
	
	

	
	kernel_log_id		=	get_new_kern_log_id		("sys_init: ",0x06);
	pci_bus_drv_id		=	bus_manager_find_bus_by_type(1,0);
	usb_bus_drv_id		=	bus_manager_find_bus_by_type(2,0);


	bus_manager_scan_bus_drivers		(pci_bus_drv_id,"isodisk","/drivers/pci");
	bus_manager_parse_bus				(pci_bus_drv_id);

	bus_manager_scan_bus_drivers		(usb_bus_drv_id,"isodisk","/drivers/usb");
	bus_manager_parse_bus				(usb_bus_drv_id);

	if (bus_manager_get_bus_dev_tree(pci_bus_drv_id, &pci_root))
	{
		tree_manager_dump_node_rec(&pci_root, 0, 8);
		release_zone_ref(&pci_root);
	}

	if (bus_manager_get_bus_dev_tree(usb_bus_drv_id, &pci_root))
	{
		tree_manager_dump_node_rec(&pci_root, 0, 8);
		release_zone_ref(&pci_root);
	}
	audio_dev_id				=	bus_manager_find_device_by_type			(pci_bus_drv_id,DEVICE_TYPE_AUDIO,0);

	if (audio_dev_id == 0xFFFFFFFF)
	{
		kernel_log(kernel_log_id, "could not find audio device ! \n");
	}
	else
	{
		audio_stream_dev_stream_id = get_stream_device_stream_bus(pci_bus_drv_id, audio_dev_id, 0);
		if (audio_dev_id == 0xFFFFFFFF)
			kernel_log(kernel_log_id, "could not find audio stream device ! \n");
		else
			bus_manager_start_bus_device_stream(pci_bus_drv_id, audio_dev_id, audio_stream_dev_stream_id);

	}	


	if(gfx_init()==0)
	{
		kernel_log(kernel_log_id,"could not init graphic system ! \n");
	}

	vga_dev_id					=	bus_manager_find_device_by_type			(pci_bus_drv_id,DEVICE_TYPE_VGA,0);
	vga_dev_render_id			=	gfx_init_render_device					(pci_bus_drv_id,vga_dev_id);


	bus_manager_add_dev_event_listener								(DEVICE_TYPE_MOUSE,mouse_event);

	gfx_load_font													("/system/fonts/Flama.otf" ,font_name, 32);
	gfx_load_font													("/system/fonts/arial.ttf", font_name, 32);

	

	loop_done		=	0;
	render_done		=	0;


	if (!setup_gfx_dev())
	{
		writestr("setup_gfx_dev failed \n");
		snooze(1000000);
	}

	/*
	if (!setup_txt_dev())
	{
		writestr("setup_txt_dev failed \n");
		snooze(1000000);
	}
	*/
	task_manager_new_task		("gfx render",render_thread,PTR_NULL,vga_dev_render_id);
	setup_interupt_c			(0x01,keyb_hdl,0);

	
	

	/*run_app					("isodisk","/apps/sys_menu.tpo",&sys_menu_mod,PTR_NULL);*/
	run_app					("isodisk","/apps/dev_menu.tpo",&dev_menu_mod,PTR_NULL);
	run_app					("isodisk","/apps/fs_menu.tpo",&fs_menu_mod,PTR_NULL); 
	run_app					("isodisk", "/apps/klog.tpo", &klog_mod, PTR_NULL);
	
	gfx_render_all_containers(vga_dev_render_id);
	
	writestr				("loop \n");



	dump_mem_used			(0);
	
	get_system_time_c		(&start_time);
	
	b						=0;

	while(!loop_done)
	{
		//dump_mem_used			(0xFFFFFFFF);

	
		//gfx_process_events					();
	
		bus_manager_parse_bus		(usb_bus_drv_id);

	}

	
	

	
	return 1;

}
