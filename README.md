# micro kernel



## Getting started

To run the operating system in virtual box

- add a floppy controller and set the image to bin/boot.img
- add an IDE controller
- add a CDDROM to the IDE controller and set the image to bin/root.iso on primary slave

It doesn't use ACPI so it should be disabled.


## Global boot process

### Inital boot process (micro kernel)

- BIOS loads boot.img ( boot_sec/)
- the bootsector load the kernel from root.iso ('/aaa.bin') at addresss 0x1000 and the ramdisk ('/ramdisk.iso') at address 0x7000 
- the kernel boots in 16 bits real mode, scan BIOS configuration, and switch to protected mode
- the kernel initialize the interuptions controller
- the kernel initialize the GDT ( memory layout)
- the kernel initialize exported function (kernel API) into the global export table
- the kernel loads embeded libc (lib_c/), zlib (zlib/) and sys_stream (sys_stream/)
- the kernel runs sys_stream::init

### system initialization

- sys_stream::init initialize CPU settings and lib_c
- sys_stream::init mount the ramdisk at address 0x7000
- sys_stream::init load pci and usb bus manager modules from the ramdisk
- sys_stream::init run disk_init application (/disk_init.tpo) from the ramdisk and execute it

### initialize root filesystem

- disk_init scan pci and usb to find boot device and load the drivers from the ramdisk
- disk_init initialize the root filesystem from boot.iso using the storage device driver
- disk_init loads graphic libraries from the root filesystem
- disk_init run sys_init app (apps/sys_init)

### system loop

- sysinit rescan the buses using the device descriptions list (bin/image/system/pci_strs.csv) and drivers from the root filesystem 
- sysinit initialize graphic device
- sysinit initialize audio device
- sysinit run sys menu app to explore devices and filesystems
- sysinit enter main loops
