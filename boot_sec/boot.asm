[BITS 16]
[ORG 0x0]
%define BIOS_START_ADDR 0x7C00
%define BIOS_START_SEG  (BIOS_START_ADDR>>4)


start_boot:
	mov ax ,BIOS_START_SEG	; 0x07C0
	mov ds ,ax
	mov es ,ax
	mov [BOOT_DEVICE],dl
	
	xor ax ,ax	; setup stack
	mov ss ,ax
	mov sp ,0x1000

	;mov ax,03H
	;int 10H

	;mov  al,'1'
	;call print_message_bios

	;reset disk
    xor ax    ,ax
    xor dx    ,dx
    int 13h
    
    
    loop_dsk:

		;-----------------------------------------
		;test for lba device
		;-----------------------------------------
		mov ah,41h
		mov dl,[BOOT_DEVICE]
		mov bx,0x55AA
		int 13h
		
		jc no_ext_lba
		;cmp bx,0xAA55
		;jne no_ext_lba
		test cx,1
		jz no_ext_lba
		
			;-----------------------------------------
			;get lba sector size
			;-----------------------------------------					
			mov ah,48h
			mov dl,[BOOT_DEVICE]
			mov si,data_available
			int 13h
			
			mov ax,[data_available+18h]
			mov word [sector_size]	,ax
			
			mov byte [has_lba]		,1
			
		jmp short geom_done
		no_ext_lba:
			;-----------------------------------------
			;chs device
			;-----------------------------------------
				
			mov word [sector_size]	,512
			mov byte [has_lba]		,0
			
			;-----------------------------------------
			;get chs disk geometry
			;-----------------------------------------			
			xor ax,ax
			mov es,ax
			xor di,di
			mov ah,0x08
			mov dl,[BOOT_DEVICE]
			int 13h
			jc	next_disk
			
			test cl,cl
			jz next_disk
			
			;-----------------------------------------
			;store result
			;-----------------------------------------		
						
			inc dh
			mov			[disk_nhds] ,dh
			
			mov [disk_ncyls]		,ch
			
			mov al,cl
			
			and al					,0x3F
			mov [disk_nsecs]		,al

			shr cl					,6
			mov [disk_ncyls+1]		,cl
			
			inc word [disk_ncyls]
				
			mov	al		,dh
			mul	byte [disk_nsecs]	
			mov [disk_hdsec],ax
	
		geom_done:
		
		;------------------------------------------------------------
		;check for iso file system at sector [iso_start]
		;------------------------------------------------------------		

		xor ax,ax
		mov word [iso_start],ax
		mov cx,2
			
		next_iso_test:
		
			call parse_iso
			test byte [iso_ret],1
			jnz iso_found
			inc byte [iso_start]
		
		loopnz next_iso_test
			
		next_disk:
		
		inc byte[BOOT_DEVICE]
	jmp loop_dsk
	
	iso_found:
	
	;mov  al,'2'
    ;call print_message_bios
   
    mov word [jmp_addr+0]	,0x0
    mov word [jmp_addr+2]	,0x1000
    
    mov dl,[BOOT_DEVICE]
    jmp far [jmp_addr]
    
    




read_disk_sec:
	push es
	
	movzx  eax	, word [esp+4]
	
	add	   ax	, [iso_start]
	
	test  byte [has_lba],1
	jz  read_no_lba
		mov dl				,[BOOT_DEVICE]
		xor ecx,ecx
		
		push ds	
		push ecx
		push ecx
		push ecx
		push dword eax
		push word es
		push word bx
		push word 1
		push word 18h
		
		retry_lba:
		
			mov ax	,	ss
			mov	ds	,	ax
			mov si	,	sp
				
			mov ah	,	0x42			;read sector
			int 13h
		jc retry_lba
		
		add sp	,	24
		pop ds
	
	jmp short read_disk_done
	
	
	read_no_lba:
		;LBA = ( (cylinder * heads_per_cylinder + heads ) * sectors_per_track ) + sector - 1
		;cylinder = LBA / (heads_per_cylinder * sectors_per_track)
		;temp = LBA % (heads_per_cylinder * sectors_per_track)
		;head = temp / sectors_per_track
		;sector = temp % sectors_per_track + 1


		push	bp
		sub		sp,4
		mov		bp,sp
		
		xor		dx,dx

		div word [disk_hdsec]
		mov [bp]	,	ax			;cylinder

		mov ax		,	dx	
		div byte	[disk_nsecs]
		mov [bp+2]	,	al			;head
		
		inc ah
		mov [bp+3]	,	ah			;sector
		
		
		
		;- the parameters in CX change depending on the number of cylinders;
		;  the track/cylinder number is a 10 bit value taken from the 2 high
		;  order bits of CL and the 8 bits in CH (low order 8 bits of track):

		;  |F|E|D|C|B|A|9|8|7|6|5-0|  CX
		;   | | | | | | | | | |	`-----	sector number
		;   | | | | | | | | `---------  high order 2 bits of track/cylinder
		;   `------------------------  low order 8 bits of track/cyl number		
		
		retry_chs_read:	
			mov ah,2				;read sector
			mov ch,[bp]				;cyl number
			mov dh,[bp+2]			;head number
			mov cl,[bp+3]			;sector number
			mov dl,[BOOT_DEVICE]	;drive number
			mov al,1				;read 1 sector
			int 0x13	
		jc retry_chs_read
	
		add sp,4
		pop	bp
	
	read_disk_done:	
	
	pop es
ret 2

iso_to_hard_sec:
	cmp word [sector_size],512
	jne	read_sec_has_2048
		shl  ax,2
	read_sec_has_2048:
ret



parse_iso:
		
	pusha
	

	mov byte [iso_ret],0
	
	;-----------------------------------------------------------------
	;setup stack space
	;-----------------------------------------------------------------
	sub sp	,	16
	mov bp	,	sp
		
	;-----------------------------------------------------------------
	;setup pointer to target memory
	;-----------------------------------------------------------------

	mov ax			, BIOS_START_SEG
	mov es			, ax
	mov bx			, root_entry
			

	;bp + 0 (word) => kern_file_start
	;bp + 2 (dword)=> kern_file_size
	;bp + 6 (word)=> cnt
			
	;-----------------------------------------------------------------
	;read file system entry
	;-----------------------------------------------------------------
			
	mov  ax,16
	call iso_to_hard_sec
		
	push  ax
	call  read_disk_sec
			
	
	
	
	cmp word[root_entry+1],0x4443
	jne end_loop_entries

	;128	=	volume size ofset
	;mov eax				, [root_entry+128]
	;mov [volume_size]		, eax
	;mov [bp]				, eax

	;158	=	root entry ofset
	;mov ax					, [root_entry+158]
	;shl eax					, 11
	;mov [ss:bp+4]			, eax
	;mov [root_entry_ext]	, eax
		
	;158+8	=	root entry size
	;mov eax					,[root_entry+158+8]
	;mov [root_entry_sz]		,eax
	;mov [ss:bp+8]				,eax

	;-----------------------------------------------------------------
	;read root entry
	;-----------------------------------------------------------------
	mov bx	, root_entry

	mov  ax,[root_entry+158]
	call iso_to_hard_sec
				
	push ax
	call  read_disk_sec
			
	;-----------------------------------------------------------------
	;parse root childs entry
	;-----------------------------------------------------------------
	mov word [bp+10],0x1000
	
	mov di,root_entry
	loop_entries:
		
		cmp		byte [di], 0
		je		end_loop_entries
			
		test	byte[di+25],0x02
		jnz	not_file
		
			;-----------------------------------------------------------------
			;read file
			;-----------------------------------------------------------------
			
			
			
			mov ax					, word[di+2]				;start sector
			call iso_to_hard_sec
			mov [bp]				, ax								
		
			mov eax					, dword[di+10]				;file size in bytes
			shr eax					, 9
			mov [bp+2]				, ax						;file size in sector
									
			mov	word [bp+6]			, 0
			loop_load_kern:
							
				mov     dx  ,  [bp+2]
				mov		ax	,  [bp+6]
				mov		bx  ,	ax
				
				cmp word [sector_size]	, 512
				jne	read_kern_has_2048_sec
					shr ax	,7			;128 sectors	= 1 segment (65536/512)
					and bx , 0x7F		;sector ofset
					shl bx , 9
					jmp short read_kern_sec_done
				read_kern_has_2048_sec:
					shr ax	,5			;32 sectors		= 1 segment (65536/2048)
					and bx , 0x1F		;sector ofset
					shl bx , 11	
					shr dx , 2
				read_kern_sec_done:
				inc dx
				
				shl ax	   , 12				;es = segment * 4096	 (segment * (65536/16) )
								
				add ax	   , [bp+10]
				mov es	   , ax
				
				push dx

				push word [bp]
				call  read_disk_sec
				
				pop dx

				inc	word [bp]
				inc	word [bp+6]
				
				;mov ax,word [bp+6]
				
								
			cmp [bp+6],dx
			jl loop_load_kern
			
			mov word [bp+10]		,0x7000
			mov byte [iso_ret],1
			;jmp end_loop_entries
			
		
		not_file:

		movzx	bx	,byte [di]
		add		di	,bx
	jmp short loop_entries
	
	end_loop_entries:
	
	add sp	,	16
	popa
	
ret


;print_message_bios:
;   mov bl,7
;   mov ah,0eh
;   int 10h
;ret

the_end:
	

TIMES 510-($-$$) DB 0	
dw 0xAA55

BOOT_DEVICE :db 0

sector_size		:dw 0
iso_start		:dw 0
rd_start		:dw 0
iso_ret			:db 0
has_lba			:db 0
disk_nhds		:db 0
disk_nsecs		:db 0
disk_ncyls		:dw 0
disk_hdsec		:dw 0
jmp_addr		:dd 0

root_entry		:times 256 db 0

data_available	:

TIMES 1024-($-$$) DB 0	