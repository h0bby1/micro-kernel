@echo off

SET THIS_PATH=C:\coding\devOS\tree
del %THIS_PATH%\bin\boot.img

@echo on

C:/coding/tools/code/nasm/NASM.exe -f bin -I%THIS_PATH% "%THIS_PATH%/boot_sec/boot.asm" -o "%THIS_PATH%/bin/boot.img"

pause
