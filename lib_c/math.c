#define LIBC_API C_EXPORT
#include <std_def.h>
#include <std_math.h>


OS_API_XTRN_ASM_FUNC(float)   fabsf(f);
OS_API_XTRN_ASM_FUNC(float)	  fabsd(f);
OS_API_XTRN_ASM_FUNC(float)	  powf(float x, float y);
OS_API_XTRN_ASM_FUNC(float)   sinf(float x);

OS_API_XTRN_ASM_FUNC(float)   cosf(float x);
OS_API_XTRN_ASM_FUNC(double)  sind(double x);
OS_API_XTRN_ASM_FUNC(double)  floord(double x);
OS_API_XTRN_ASM_FUNC(double)  ceild(double x);

OS_API_C_FUNC(float) fabsf_c(float f)
{
	return fabsf(f);

}
OS_API_C_FUNC(double) fabs_c(double f)
{
	return fabsd(f);

}
OS_API_C_FUNC(double) sin_c		(double f)
{
	return sind(f);

}

OS_API_C_FUNC(int) icosf(float f,float m)
{
	return (int)(cosf(f)*m);
}


OS_API_C_FUNC(double)	floor_c		(double f)
{
	return f;//floord(f);
}



OS_API_C_FUNC(double)	ceil_c		(double f)
{
	return f;//ceild(f);
}

OS_API_C_FUNC(float) sinf_c					(float x)
{
	return sinf	(x);
}

OS_API_C_FUNC(float) powf_c(float x,float y)
{
	return powf(x,y);

}

OS_API_C_FUNC(unsigned int) iabs_c (int v)
{

	if(v<0)
		return -v;
	else
		return  v;
}

OS_API_C_FUNC(unsigned long) isqrt(unsigned long x)  
{  
    register unsigned long op, res, one;  
  
    op = x;  
    res = 0;  
  
    /* "one" starts at the highest power of four <= than the argument. */  
    one = 1 << 30;  /* second-to-top bit set */  
    while (one > op) one >>= 2;  
  
    while (one != 0) {  
        if (op >= res + one) {  
            op -= res + one;  
            res += one << 1;  // <-- faster than 2 * one  
        }  
        res >>= 1;  
        one >>= 2;  
    }  
    return res;  
}  
