#define LIBC_API C_EXPORT
#include <std_def.h>
#include <kern.h>
#include <std_mem.h>
#include <mem_base.h>
#include <lib_c.h>
#include <tree.h>
#include <sys/task.h>



#define		TREE_BUFFER_SIZE 1024*1024*4

typedef struct 
{
	char						name[32];
	unsigned int				name_crc;
	unsigned int				node_type;
	size_t						image_position;
	size_t						image_size;
	mem_zone_ref				childs_ptr;
	mem_zone_ref				child_w_ptr[2];
	unsigned int				is_used[2];
	unsigned int				is_consumed[2];
	unsigned int				cur_write;
	unsigned int				last_write;
	mem_ptr						parent_zone;
	
}tree_node;

typedef		  tree_node *tree_node_ptr;
typedef const tree_node *tree_node_const_ptr;

unsigned int	tree_mem_area_id	=	0xCDCDCDCD;
unsigned int	tree_output			=	0xCDCDCDCD;


unsigned int inc_zone_ref	(mem_zone_ref_ptr zone_ref);
void swap_zone_ref			(mem_zone_ref_ptr dest_zone_ref,mem_zone_ref_ptr src_zone_ref);
void qsort_ctx_c			(mem_ptr base, mem_size num, mem_size width,unsigned int context,  int ( *comp)(unsigned int,const_mem_ptr,const_mem_ptr));
void set_zone_free			(mem_zone_ref_ptr ref,zone_free_func_ptr	free_func);
void dump_mem_used_after	(unsigned int area_id, unsigned int time);

#include "strbuffer.h"
#include "utf.h"

__inline tree_node_ptr get_node	(const mem_zone_ref *ref)
{
	return get_zone_ptr(ref,0);
}

__inline int is_node	(const mem_zone_ref *ref)
{
	if(get_node(ref)->name_crc==calc_crc32_c(get_node(ref)->name,32))
		return 1;
	else
		return 0;
}

void tree_manager_init()
{
	mem_ptr				start,end;
	if(tree_mem_area_id	!= 0xCDCDCDCD)return ;

	start			=	get_next_aligned_ptr(kernel_memory_map_c(TREE_BUFFER_SIZE+8));
	end				=	mem_add				(start,TREE_BUFFER_SIZE);
	tree_mem_area_id=	init_new_mem_area	(start,end,MEM_TYPE_TREE);
	tree_output		=	0;

	memset_c(start,0,mem_sub(start,end));
}

void tree_manager_dump_mem(unsigned int time)
{
	//dump_mem_used			(tree_mem_area_id);
	dump_mem_used_after		(tree_mem_area_id,time);
	
}






OS_API_C_FUNC(void) init_node_array	(mem_zone_ref_ptr node_array,unsigned int n_elems,const char *name,unsigned int type,unsigned int size_alloc)
{
	mem_zone_ref_ptr	node_array_ptr;


	allocate_new_zone	(tree_mem_area_id , (n_elems+1)*sizeof(mem_zone_ref),node_array);
	set_zone_free		(node_array,tree_manager_free_node_array);

	node_array_ptr=get_zone_ptr(node_array,0);

	while(n_elems--)
	{
		tree_manager_create_node			(name,type,node_array_ptr);
		tree_manager_allocate_node_data		(node_array_ptr,size_alloc+sizeof(unsigned int));
		tree_manager_write_node_dword		(node_array_ptr,0,0);
		
		node_array_ptr++;
	}
}

OS_API_C_FUNC(unsigned int) node_array_get_free_element	(mem_zone_ref_ptr node_array,mem_zone_ref_ptr	node)
{
	size_t				array_size;
	unsigned int		n;

	if(node_array->zone==PTR_NULL)return 0;


	array_size		=	get_zone_size(node_array)/sizeof(mem_zone_ref);
	n				=	0;

	while(n<array_size)
	{
		mem_zone_ref_ptr	node_ptr;
		unsigned int		*used_ptr;

		node_ptr	=	get_zone_ptr					(node_array,n*sizeof(mem_zone_ref));
		used_ptr	=	tree_mamanger_get_node_data_ptr (node_ptr,0);

		if((*used_ptr)==0)
		{
			*used_ptr		=	1;
			copy_zone_ref	(node,node_ptr);
			return 1;
		}
		n++;
	}
	return 0;
}


OS_API_C_FUNC(unsigned int) node_array_pop(mem_zone_ref_ptr node_array,mem_zone_ref_ptr	node)
{
	size_t				array_size;
	unsigned int		n;

	if(node_array->zone==PTR_NULL)return 0;


	array_size		=	get_zone_size(node_array)/sizeof(mem_zone_ref);
	n				=	0;

	while(n<array_size)
	{
		mem_zone_ref_ptr	node_ptr;
		unsigned int		*used_ptr;

		node_ptr	=	get_zone_ptr					(node_array,n*sizeof(mem_zone_ref));
		used_ptr	=	tree_mamanger_get_node_data_ptr (node_ptr,0);

		if((*used_ptr)==1)
		{
			copy_zone_ref	(node,node_ptr);
			return 1;
		}
		n++;
	}
	return 0;
}


int C_API_FUNC tree_manager_free_node_array	(mem_zone_ref_ptr childs_ref_ptr)
{
	mem_size			childs_size;
	mem_zone_ref_ptr	childs_ptr,last_ptr;

	if(childs_ref_ptr==PTR_NULL)return 0;
	if(childs_ref_ptr->zone==PTR_NULL)return 0;
	if((childs_size=get_zone_size(childs_ref_ptr))==0)return 0;

	childs_ptr  =get_zone_ptr(childs_ref_ptr,0);
	last_ptr	=mem_add(childs_ptr,childs_size);

	while(childs_ptr<last_ptr)
	{   
		if((childs_ptr->zone!=PTR_NULL)&&(childs_ptr->zone!=PTR_INVALID)&&(childs_ptr->zone!=uint_to_mem(0xDEF0DEF0)))
		{
			//get_node(childs_ptr)->parent_zone	=PTR_NULL;
			release_zone_ref(childs_ptr);
		}
		childs_ptr++;
		if(childs_ptr->zone==PTR_NULL)break;
	}

	return 1;
}


int C_API_FUNC tree_manager_free_node		(mem_zone_ref_ptr p_node_ref)
{
	tree_node		*p_node;


	if(p_node_ref==PTR_NULL)return 0;
	if(p_node_ref==uint_to_mem(0xDEF0DEF0))return 0;
	if(p_node_ref->zone==PTR_NULL)return 0;
	if(p_node_ref->zone==PTR_INVALID)return 0;
	if(p_node_ref->zone==uint_to_mem(0xDEF0DEF0))return 0;

	p_node=get_node(p_node_ref);

	if(p_node->child_w_ptr[0].zone!=PTR_NULL)release_zone_ref			(&p_node->child_w_ptr[0]);
	if(p_node->child_w_ptr[1].zone!=PTR_NULL)release_zone_ref			(&p_node->child_w_ptr[1]);

	if(p_node->childs_ptr.zone==PTR_NULL)return 1;
	if(p_node->childs_ptr.zone==uint_to_mem(0xDEF0DEF0))return 1;

	/*
		mem_zone_ref	*childs_ptr;

		unsigned int	n;
	if(get_zone_size(&p_node->childs_ptr)>0)
	{
		n			=	0;
		while(1)
		{
			if(p_node->childs_ptr.zone==PTR_NULL)break;
			if(p_node->childs_ptr.zone==uint_to_mem(0xDEF0DEF0))break;
			
			childs_ptr=get_zone_ptr(&p_node->childs_ptr,n*sizeof(mem_zone_ref));
			if(childs_ptr->zone==PTR_NULL)break;
			
			if((childs_ptr->zone!=uint_to_mem(0xDEF0DEF0))&&(childs_ptr->zone!=PTR_INVALID))
			{
				get_node(childs_ptr)->parent_zone=PTR_NULL;
				release_zone_ref				(childs_ptr);
			}
			n++;
		}
	}
	*/

	release_zone_ref			(&p_node->childs_ptr);


	return 1;
}

OS_API_C_FUNC(int) tree_manager_create_node(const char *name,unsigned int type,mem_zone_ref *ref_ptr)
{
	unsigned int			crc_name;
	

	if(tree_output)writestr("create #1 \n");

	if(allocate_new_zone	(tree_mem_area_id,sizeof(tree_node), ref_ptr)!=1)
	{
		writestr("could not allocate new node \n");
		return 0;
	}

	if(tree_output)writestr("create #2 \n");
	

	set_zone_free						(ref_ptr,tree_manager_free_node);

	crc_name							=	calc_crc32_c	(name,32);
	get_node(ref_ptr)->name_crc			=	crc_name;
	get_node(ref_ptr)->node_type		=	type;

	if(tree_output)writestr("create #3 \n");

	strcpy_s							(get_node(ref_ptr)->name,32,name);

	get_node(ref_ptr)->parent_zone		=	PTR_NULL;

	get_node(ref_ptr)->child_w_ptr[0].zone	=	PTR_NULL;
	get_node(ref_ptr)->child_w_ptr[1].zone	=	PTR_NULL;
	get_node(ref_ptr)->is_used[0]			=	0;
	get_node(ref_ptr)->is_used[1]			=	0;
	get_node(ref_ptr)->is_consumed[0]		=	0;
	get_node(ref_ptr)->is_consumed[1]		=	0;
	get_node(ref_ptr)->cur_write			=	0;
	get_node(ref_ptr)->last_write			=	0xFFFFFFFF;

	if(tree_output)writestr("create #4 \n");
	
	tree_manager_set_node_image_info(ref_ptr,0,0);

	get_node(ref_ptr)->childs_ptr.zone	=	PTR_NULL;

	/*
	if(tree_output)writestr("create #5 \n");
	if(allocate_new_empty_zone			(tree_mem_area_id,&get_node(ref_ptr)->childs_ptr)!=1)
	{
		release_zone_ref(ref_ptr);
		return 0;
	}
	set_zone_free			(&get_node(ref_ptr)->childs_ptr,tree_manager_free_node_array);
	*/

	if(tree_output)writestr("create #6 \n");
	return 1;
}


OS_API_C_FUNC(unsigned int) tree_manager_node_add_child(mem_zone_ref_ptr p_node_ref,mem_zone_ref_const_ptr child_ref_ptr)
{
	mem_zone_ref_ptr cur_child;
	mem_zone_ref_ptr cur_w_child_list;
	tree_node		 *p_node;
	mem_zone_ref	 source_list={PTR_NULL};
	mem_zone_ref_ptr new_childs;
	size_t	    	 new_child_size;

	if(p_node_ref==PTR_NULL)return 0;
	if(p_node_ref->zone==PTR_NULL)return 0; 


	if (child_ref_ptr == PTR_NULL)return 0;
	if (child_ref_ptr->zone == PTR_NULL)return 0;

	p_node				=	get_node			(p_node_ref);

	if((p_node->last_write==0xFFFFFFFF)||(p_node->is_consumed[p_node->last_write]))
		copy_zone_ref(&source_list	,&p_node->childs_ptr);
	else
		copy_zone_ref(&source_list	,&p_node->child_w_ptr[p_node->last_write]);
	
	while( ((p_node->last_write==p_node->cur_write)&&(p_node->is_consumed[p_node->last_write]==0))&&
		   (!compare_z_exchange_c(&p_node->is_used[p_node->cur_write],1)))
	{
		p_node->cur_write	^=	1;
	}

	if((source_list.zone!=PTR_NULL)&&(get_zone_size(&source_list)>0))
		new_child_size	= memchr_32_c(get_zone_ptr(&source_list,0),mem_to_uint(PTR_NULL),get_zone_size(&source_list));
	else
		new_child_size	= 0;

	cur_w_child_list				=	&p_node->child_w_ptr[p_node->cur_write];
	if(!allocate_new_zone			(tree_mem_area_id,new_child_size+sizeof(mem_zone_ref)*2,cur_w_child_list))
	{
		p_node->is_used[p_node->cur_write]		=	0;
		writestr("could not allocate new child list \n");
		snooze(2000000);
		return 0;
	}
	set_zone_free	(cur_w_child_list,tree_manager_free_node_array);


	
	
	new_childs	=	get_zone_ptr(cur_w_child_list,0);

	if(new_child_size>0)
	{
		mem_zone_ref_ptr last_child;
		
		cur_child	=	get_zone_ptr(&source_list,0);
		last_child	=	get_zone_ptr(&source_list,0xFFFFFFFF);

		if (tree_output) {
			writestr_fmt("copy source %p %p to %p %p\n", source_list.zone, cur_child, cur_w_child_list->zone, new_childs);
			snooze(5000000);
		}

		while(cur_child<last_child)
		{
			if (tree_output) {
				writestr_fmt("copy %p to %p\n", cur_child->zone, new_childs);
				snooze(1000000);
			}
				

			if(cur_child->zone==PTR_NULL)break;

			if((cur_child->zone!=PTR_NULL)&&(cur_child->zone!=uint_to_mem(0xDEF0DEF0))&&(cur_child->zone!=PTR_INVALID)&&(get_zone_size(cur_child)>0))
			{
				copy_zone_ref((new_childs++),cur_child);

				if(tree_output)
				{
					tree_manager_dump_node_rec(cur_child,0,1);
					snooze(1000000);
				}
			}

			cur_child++;
		}
	}
	else if (tree_output) {
		writestr_fmt("copy source %p to %p %p ( %p ) \n", source_list.zone, cur_w_child_list->zone, new_childs, child_ref_ptr->zone);
		snooze(5000000);
	}

	if(get_node(child_ref_ptr)->parent_zone	== PTR_NULL)	
		get_node(child_ref_ptr)->parent_zone	=	p_node_ref->zone;

	new_childs->zone						= 	PTR_NULL;

	if (tree_output) {
		writestr_fmt("copy %p to %p\n", child_ref_ptr->zone, new_childs);
		snooze(1000000);
	}
	copy_zone_ref	((new_childs++),child_ref_ptr);

	if (tree_output) {
		writestr_fmt("copy null to %p\n", new_childs);
		snooze(1000000);
	}

	new_childs->zone						 = 	PTR_NULL;
	

	release_zone_ref	(&source_list);
	p_node->is_consumed[p_node->cur_write]	 = 	0;
	p_node->is_used[p_node->cur_write]		 =	0;
	p_node->last_write						 =	p_node->cur_write;
	p_node->cur_write						^=	1;
	return 1;
}



int tree_manager_has_child_crc		(mem_zone_ref_ptr child_list,unsigned int crc,mem_zone_ref_ptr	*out)
{

	mem_zone_ref_ptr		child,last_child;
	child			=	get_zone_ptr(child_list,0);
	last_child		=	get_zone_ptr(child_list,0xFFFFFFFF);
	
	while((child<last_child)&&(child->zone!=PTR_NULL))
	{
		if((child->zone!=PTR_INVALID)&&(child->zone!=uint_to_mem(0xDEF0DEF0))&&(get_zone_size(child)>0))
		{
			if(get_node(child)->name_crc==crc)
			{	
				if(out!=PTR_NULL)(*out)=child;
				return 1;
			}
		}
		child++;
	}
	return 0;
}


OS_API_C_FUNC(int) tree_manager_get_first_child		(mem_zone_ref_const_ptr p_node_ref,mem_zone_ref_ptr child_list, mem_zone_ref_ptr *p_node_out_ref)
{
	unsigned int	cur_read,last_write;
	tree_node		*p_node;
	mem_zone_ref_ptr first_child;

	if(p_node_ref==PTR_NULL)return 0;
	if(p_node_ref->zone==PTR_NULL)return 0;
	if(p_node_ref->zone==uint_to_mem(0xDEF0DEF0))return 0;
	p_node=get_node(p_node_ref);

	cur_read			=	0;
	last_write			=	p_node->last_write;

	if((last_write!=0xFFFFFFFF)&&(p_node->is_consumed[last_write]==0))
	{
		if(compare_z_exchange_c(&p_node->is_used[last_write],1))
		{
			swap_zone_ref		(&p_node->childs_ptr,&p_node->child_w_ptr[last_write]);
			p_node->is_consumed[last_write]	=1;
			p_node->is_used[last_write]		=0;
		}
	}

	if(p_node->childs_ptr.zone==PTR_NULL)return 0;
	copy_zone_ref	(child_list,&p_node->childs_ptr);
	if(get_zone_size(child_list)==0)
	{
		release_zone_ref		(child_list);
		return 0;
	}

	first_child			=	get_zone_ptr(child_list,0);
	if(first_child->zone == PTR_NULL)
	{
		release_zone_ref		(child_list);
		return 0;
	}
	if(p_node_out_ref!=PTR_NULL)
	{
		inc_zone_ref			(first_child);
		(*p_node_out_ref)	=	first_child;
	}
	return 1;
}

OS_API_C_FUNC(int) tree_manager_get_next_child		(mem_zone_ref_ptr child_list,mem_zone_ref_ptr *p_node_out_ref)
{
	mem_zone_ref_ptr next_child;

	if((*p_node_out_ref)->zone	== PTR_NULL)return 0;
	if((*p_node_out_ref)->zone	== uint_to_mem(0xDEF0DEF0))return 0;
	if(get_zone_size((*p_node_out_ref))==0)return 0;
	
	dec_zone_ref		((*p_node_out_ref));

	next_child	=	(*p_node_out_ref)+1;

	if(	(next_child->zone	== PTR_NULL)||
		(next_child->zone	== uint_to_mem(0xDEF0DEF0))||
		(get_zone_size(next_child)==0))
	{
		(*p_node_out_ref)	=	next_child;
		release_zone_ref		(child_list);
		return 0;
	}
	inc_zone_ref			(next_child);
	(*p_node_out_ref)	=	next_child;
	return 1;
}

int tree_manager_find_child_crc		(mem_zone_ref_ptr child_list,unsigned int crc,mem_zone_ref_ptr *p_node_out_ref)
{
	mem_zone_ref_ptr		next_child;

	if(child_list->zone==PTR_NULL)return 0;

	next_child			=	(*p_node_out_ref);
	while(next_child->zone!=PTR_NULL)
	{
		if(get_node(next_child)->name_crc==crc)
		{
			(*p_node_out_ref)	=	next_child;
			return 1;
		}
		dec_zone_ref		(next_child);
		while(((++next_child)->zone)!=PTR_NULL)
		{
			if(inc_zone_ref		(next_child)==1)break;
		}
	}

	(*p_node_out_ref)	=	next_child;
	release_zone_ref		(child_list);

	return 0;
}


int cmp_child_value	(mem_zone_ref_const_ptr		node,unsigned int crc_prop,unsigned int value)
{
	mem_zone_ref	 child_list={PTR_NULL};
	mem_zone_ref_ptr cur_child;
	int				 ret;

	if(!tree_manager_get_first_child(node,&child_list,&cur_child))return 0;
	if(!tree_manager_find_child_crc(&child_list,crc_prop,&cur_child))return 0;
	ret		= tree_mamanger_compare_node_dword(cur_child,0,value);
	dec_zone_ref	(cur_child);
	release_zone_ref(&child_list); 
	if(ret==0)return 1;
	return 0;
}

OS_API_C_FUNC(int) tree_manager_add_node_childs	(mem_zone_ref_ptr parent_ref_ptr,const char *params,unsigned int merge)
{
	mem_zone_ref		new_childs_array={PTR_NULL};
	mem_zone_ref		new_childs		={PTR_NULL};
	unsigned int		to=tree_output;

	if(params==PTR_NULL)return 0;

	tree_output=0;
	if(tree_manager_json_loadb	(params,strlen_c(params),&new_childs)==0)
	{
		writestr("could not load new child ");
		writestr(params);
		writestr("\n");

		if(new_childs.zone!=PTR_NULL)
		{
			if(is_node(&new_childs))
				release_zone_ref				(&new_childs);
			else
				tree_manager_free_node_array	(&new_childs);
		}

		return 0;
	}
	tree_output=to;

	if(new_childs.zone==PTR_NULL)return 0;

	



	if(is_node(&new_childs))
	{
		mem_zone_ref		new_child_list	={PTR_NULL};
		mem_zone_ref		source_list		={PTR_NULL};
		mem_zone_ref_ptr	w_child_list;

		mem_zone_ref_ptr	new_child;
		mem_zone_ref_ptr	cur_child;
		mem_zone_ref_ptr	w_child;
		
		tree_node		 	*p_node;
		size_t	    	 	new_child_size;
		size_t	    	 	new_child_zone_size;
		size_t	    	 	org_child_size;
		size_t	    	 	total_new_child_size;
		

		if(!tree_manager_get_first_child	(&new_childs,&new_child_list,PTR_NULL))
		{
			release_zone_ref(&new_childs);
			return 1;
		}

		new_child_size		=	0;
		new_child_zone_size	=	get_zone_size(&new_child_list);
		
		if(new_child_zone_size>0)
			new_child_size	= memchr_32_c(get_zone_ptr(&new_child_list,0),mem_to_uint(PTR_NULL),new_child_zone_size);


		p_node				=	get_node(parent_ref_ptr);

		if((p_node->last_write==0xFFFFFFFF)||(p_node->is_consumed[p_node->last_write]))
			copy_zone_ref(&source_list,&p_node->childs_ptr);
		else
			copy_zone_ref(&source_list,&p_node->child_w_ptr[p_node->last_write]);

		org_child_size	=	0;

		if((source_list.zone!=PTR_NULL)&&(get_zone_size(&source_list)>0))
		{
			cur_child=get_zone_ptr(&source_list,0);
			while(cur_child->zone!=PTR_NULL)
			{
				if((cur_child->zone!=PTR_INVALID)&&(cur_child->zone!=uint_to_mem(0xDEF0DEF0))&&(get_zone_size(cur_child)>0))
				{
					unsigned int crc ;
					crc		=	get_node(cur_child)->name_crc;
					if(!tree_manager_has_child_crc  (&new_child_list,get_node(cur_child)->name_crc,PTR_NULL))
						org_child_size+=sizeof(mem_zone_ref);
				}
				cur_child++;
			}
		}

		while(!compare_z_exchange_c(&p_node->is_used[p_node->cur_write],1))
		{
			p_node->cur_write	^=	1;
		}

		total_new_child_size		=	org_child_size+new_child_size+sizeof(mem_zone_ref);
		w_child_list				=	&p_node->child_w_ptr[p_node->cur_write];

		if(!allocate_new_zone			(tree_mem_area_id,total_new_child_size,w_child_list))
		{
			release_zone_ref(&new_child_list);
			release_zone_ref(&source_list);
			release_zone_ref(&new_childs);

			p_node->is_used[p_node->cur_write]		=	0;
			writestr("could not allocate new child list \n");
			snooze(2000000);
			return 0;
		}
		set_zone_free		(w_child_list,tree_manager_free_node_array);

		w_child				=	get_zone_ptr(w_child_list,0);

		if(tree_output)writestr_fmt("allocate new child list %p %d %d %d %p\n",w_child,total_new_child_size,org_child_size,new_child_size,mem_add(w_child,total_new_child_size));

		if((source_list.zone!=PTR_NULL)&&(get_zone_size(&source_list)>0))
		{
			cur_child			=	get_zone_ptr(&source_list,0);
			
			while(cur_child->zone!=PTR_NULL)
			{
				mem_zone_ref_ptr found;

				if((cur_child->zone!=PTR_INVALID)&&(cur_child->zone!=uint_to_mem(0xDEF0DEF0))&&(get_zone_size(cur_child)>0))
				{
					unsigned int crc ;
					
					crc		=	get_node(cur_child)->name_crc;

					if(tree_manager_has_child_crc  (&new_child_list,crc,&found))
					{
						copy_zone_ref		(w_child,found);
						release_zone_ref	(found);
						found->zone=PTR_INVALID;
						get_node(w_child)->parent_zone=parent_ref_ptr->zone;
					}
					else
					{
						copy_zone_ref		(w_child,cur_child);
					}

					w_child++;
				}
				cur_child++;
			}
		}

		new_child	=	get_zone_ptr(&new_child_list,0);
		while(new_child->zone!=PTR_NULL)
		{
			if((new_child->zone!=PTR_INVALID)&&(new_child->zone!=uint_to_mem(0xDEF0DEF0))&&(get_zone_size(new_child)>0))
			{
				copy_zone_ref		(w_child,new_child);
				get_node(w_child)->parent_zone=parent_ref_ptr->zone;
				w_child++;
			}
			new_child++;
		}

		if(tree_output)writestr_fmt("copy NULL to %p\n",w_child);

		w_child->zone=PTR_NULL;

		release_zone_ref(&new_child_list);
		release_zone_ref(&source_list);
		release_zone_ref(&new_childs);

		p_node->is_consumed[p_node->cur_write]	 = 	0;
		p_node->last_write						 =	p_node->cur_write;
		p_node->is_used[p_node->cur_write]		 =	0;
		p_node->cur_write						^=	1;

		return 1;
	}
	return 1;
}



OS_API_C_FUNC(int) tree_manager_copy_children	(mem_zone_ref *dest_ref_ptr,mem_zone_ref_const_ptr src_ref_ptr)
{
	mem_zone_ref	 src_child_list={PTR_NULL};
	mem_zone_ref_ptr src_child;
	mem_zone_ref_ptr cur_w_child_list;
	tree_node		 *p_node;
	mem_zone_ref_ptr new_childs;
	size_t	    	 new_child_size;

	if(dest_ref_ptr==PTR_NULL)return 0;
	if(dest_ref_ptr->zone==PTR_NULL)return 0; 

	p_node				=	get_node			(dest_ref_ptr);
	
	while( ((p_node->last_write==p_node->cur_write)&&(p_node->is_consumed[p_node->last_write]==0))&&
		   (!compare_z_exchange_c(&p_node->is_used[p_node->cur_write],1)))
	{
		p_node->cur_write	^=	1;
	}
	cur_w_child_list				=	&p_node->child_w_ptr[p_node->cur_write];

	if(tree_manager_get_first_child(src_ref_ptr,&src_child_list,&src_child))
	{
		new_child_size	= memchr_32_c(get_zone_ptr(&src_child_list,0),mem_to_uint(PTR_NULL),get_zone_size(&src_child_list));
	
		if(!allocate_new_zone			(tree_mem_area_id,new_child_size+sizeof(mem_zone_ref),cur_w_child_list))
		{
			dec_zone_ref	(src_child);
			release_zone_ref(&src_child_list);
			p_node->is_used[p_node->cur_write]		=	0;
			writestr("could not allocate new child list \n");
			snooze(2000000);
			return 0;
		}
		set_zone_free	(cur_w_child_list,tree_manager_free_node_array);
		new_childs	=	get_zone_ptr(cur_w_child_list,0);

		while(src_child->zone!=PTR_NULL)
		{
			if((src_child->zone!=uint_to_mem(0xDEF0DEF0))&&(src_child->zone!=PTR_INVALID)&&(get_zone_size(src_child)>0))
			{
				tree_manager_node_dup(PTR_NULL,src_child,(new_childs++));
			}
			if(!tree_manager_get_next_child(&src_child_list,&src_child))break;
		}
		new_childs->zone						= 	PTR_NULL;
	}
	else
	{
		release_zone_ref(cur_w_child_list);
	}

	p_node->is_consumed[p_node->cur_write]	 = 	0;
	p_node->is_used[p_node->cur_write]		 =	0;
	p_node->last_write						 =	p_node->cur_write;
	p_node->cur_write						^=	1;
	return 1;
}


OS_API_C_FUNC(int)	tree_remove_children			(mem_zone_ref *p_node_ref)
{
	tree_node		 *p_node;
	if(p_node_ref==PTR_NULL)return 0;
	if(p_node_ref->zone==PTR_NULL)return 0; 

	p_node				=	get_node			(p_node_ref);
	
	while( ((p_node->last_write==p_node->cur_write)&&(p_node->is_consumed[p_node->last_write]==0))&&
		   (!compare_z_exchange_c(&p_node->is_used[p_node->cur_write],1)))
	{
		p_node->cur_write	^=	1;
	}

	release_zone_ref(&p_node->child_w_ptr[p_node->cur_write]);

	p_node->is_consumed[p_node->cur_write]	 = 	0;
	p_node->last_write						 =	p_node->cur_write;
	p_node->is_used[p_node->cur_write]		 =	0;
	p_node->cur_write						^=	1;
	return 1;
	/*
	mem_zone_ref	orig_childs={PTR_NULL};

	if(p_node_ref==PTR_NULL)return 0;
	if(p_node_ref->zone==PTR_NULL)return 0;
	if(get_node(p_node_ref)->childs_ptr.zone==PTR_NULL)return 0;
	if(get_zone_size(&get_node(p_node_ref)->childs_ptr)==0)return 1;

	copy_zone_ref			(&orig_childs,&get_node(p_node_ref)->childs_ptr);
	allocate_new_empty_zone	(tree_mem_area_id,&get_node(p_node_ref)->childs_ptr);
	set_zone_free			(&get_node(p_node_ref)->childs_ptr,tree_manager_free_node_array);
	
	
	release_zone_ref			(&orig_childs);
		
	return 1;
	*/
}

unsigned int get_node_array_num(mem_zone_ref_ptr	node_array)
{
	mem_zone_ref_ptr	array_ptr;
	unsigned int		n;

	n			=	0;
	array_ptr	=	get_zone_ptr(node_array,0);

	while(array_ptr->zone!=PTR_NULL){n++;array_ptr++;}


	return n;
}


OS_API_C_FUNC(int) tree_remove_child_by_id		(mem_zone_ref_ptr p_node_ref,unsigned int child_id)
{
	mem_zone_ref_ptr cur_child;
	mem_zone_ref_ptr cur_w_child_list;
	tree_node		 *p_node;
	mem_zone_ref_ptr new_childs,source_list;
	size_t	    	 new_child_size;
	unsigned int	has_rem;

	if(p_node_ref==PTR_NULL)return 0;
	if(p_node_ref->zone==PTR_NULL)return 0; 

	p_node				=	get_node			(p_node_ref);

	if((p_node->last_write==0xFFFFFFFF)||(p_node->is_consumed[p_node->last_write]))
		source_list	=	&p_node->childs_ptr;
	else
		source_list	=	&p_node->child_w_ptr[p_node->last_write];

	if(source_list->zone==PTR_NULL)return 1;
	if(get_zone_size(source_list)==0)return 1;

	cur_child			=	get_zone_ptr(source_list,0);
	new_child_size		=	0;
	has_rem				=	0;
	while(cur_child->zone!=PTR_NULL)
	{
		if(cmp_child_value	(cur_child,NODE_HASH("id"),child_id))
			has_rem=1;
		else
			new_child_size++;
		cur_child++;
	}
	if(!has_rem)return 1;
	
	while( ((p_node->last_write==p_node->cur_write)&&(p_node->is_consumed[p_node->last_write]==0))&&
		   (!compare_z_exchange_c(&p_node->is_used[p_node->cur_write],1)))
	{
		p_node->cur_write	^=	1;
	}
	
	cur_w_child_list				=	&p_node->child_w_ptr[p_node->cur_write];


	if(new_child_size==0)
	{
		if(!allocate_new_empty_zone(tree_mem_area_id,cur_w_child_list))
		{
			p_node->is_used[p_node->cur_write]		=	0;
			dec_zone_ref	(cur_child);
			writestr("could not allocate new empty child list \n");
			return 0;
		}
	}
	else
	{
		if(!allocate_new_zone			(tree_mem_area_id,new_child_size+sizeof(mem_zone_ref),cur_w_child_list))
		{
			p_node->is_used[p_node->cur_write]		=	0;
			dec_zone_ref	(cur_child);
			writestr("could not allocate new child list \n");
			return 0;
		}
		cur_child	=	get_zone_ptr(source_list,0);
		new_childs	=	get_zone_ptr(cur_w_child_list,0);
		while(cur_child->zone!=PTR_NULL)
		{
			if(!cmp_child_value	(cur_child,NODE_HASH("id"),child_id))
				copy_zone_ref((new_childs++),cur_child);

			cur_child++;
		}
		new_childs->zone						 = 	PTR_NULL;
	}

	set_zone_free	(cur_w_child_list,tree_manager_free_node_array);


	p_node->is_consumed[p_node->cur_write]	 = 	0;
	p_node->is_used[p_node->cur_write]		 =	0;
	p_node->last_write						 =	p_node->cur_write;
	p_node->cur_write						^=	1;

	return 1;
}

OS_API_C_FUNC(int) tree_remove_child_by_type		(mem_zone_ref_ptr p_node_ref,unsigned int child_type)
{
	mem_zone_ref_ptr cur_child;
	mem_zone_ref_ptr cur_w_child_list;
	tree_node		 *p_node;
	mem_zone_ref_ptr new_childs,source_list;
	size_t	    	 new_child_size;
	unsigned int	has_rem;

	if(p_node_ref==PTR_NULL)return 0;
	if(p_node_ref->zone==PTR_NULL)return 0; 

	p_node				=	get_node			(p_node_ref);

	if((p_node->last_write==0xFFFFFFFF)||(p_node->is_consumed[p_node->last_write]))
		source_list	=	&p_node->childs_ptr;
	else
		source_list	=	&p_node->child_w_ptr[p_node->last_write];

	if(source_list->zone==PTR_NULL)return 1;
	if(get_zone_size(source_list)==0)return 1;

	cur_child			=	get_zone_ptr(source_list,0);
	new_child_size		=	0;
	has_rem				=	0;
	while(cur_child->zone!=PTR_NULL)
	{
		if(get_node(cur_child)->node_type==child_type)
			has_rem	 =	1	;
		else
			new_child_size	+=	sizeof(mem_zone_ref);

		cur_child++;
	}
	if(!has_rem)return 1;
	
	while( ((p_node->last_write==p_node->cur_write)&&(p_node->is_consumed[p_node->last_write]==0))&&
		   (!compare_z_exchange_c(&p_node->is_used[p_node->cur_write],1)))
	{
		p_node->cur_write	^=	1;
	}
	
	cur_w_child_list				=	&p_node->child_w_ptr[p_node->cur_write];


	if(new_child_size==0)
	{
		if(!allocate_new_empty_zone(tree_mem_area_id,cur_w_child_list))
		{
			p_node->is_used[p_node->cur_write]		=	0;
			dec_zone_ref	(cur_child);
			writestr("could not allocate new empty child list \n");
			return 0;
		}
	}
	else
	{
		if(!allocate_new_zone			(tree_mem_area_id,new_child_size+sizeof(mem_zone_ref),cur_w_child_list))
		{
			p_node->is_used[p_node->cur_write]		=	0;
			dec_zone_ref	(cur_child);
			writestr("could not allocate new child list \n");
			return 0;
		}
		cur_child	=	get_zone_ptr(source_list,0);
		new_childs	=	get_zone_ptr(cur_w_child_list,0);
		while(cur_child->zone!=PTR_NULL)
		{
			if(get_node(cur_child)->node_type!=child_type)
				copy_zone_ref((new_childs++),cur_child);

			cur_child++;
		}
		new_childs->zone						 = 	PTR_NULL;
	}

	set_zone_free	(cur_w_child_list,tree_manager_free_node_array);


	p_node->is_consumed[p_node->cur_write]	 = 	0;
	p_node->is_used[p_node->cur_write]		 =	0;
	p_node->last_write						 =	p_node->cur_write;
	p_node->cur_write						^=	1;

	return 1;
}


OS_API_C_FUNC(int)	tree_remove_child_by_member_value_dword	(mem_zone_ref *p_node_ref,unsigned int child_type,const char *member_name,unsigned int value)
{
	mem_zone_ref_ptr cur_child;
	mem_zone_ref_ptr cur_w_child_list;
	tree_node		 *p_node;
	mem_zone_ref_ptr new_childs,source_list;
	size_t	    	 new_child_size;
	unsigned int	has_rem;
	unsigned int	crc_32;

	if(p_node_ref==PTR_NULL)return 0;
	if(p_node_ref->zone==PTR_NULL)return 0; 

	p_node				=	get_node			(p_node_ref);
	crc_32				=	NODE_HASH			(member_name);

	if((p_node->last_write==0xFFFFFFFF)||(p_node->is_consumed[p_node->last_write]))
		source_list	=	&p_node->childs_ptr;
	else
		source_list	=	&p_node->child_w_ptr[p_node->last_write];

	if(source_list->zone==PTR_NULL)return 1;
	if(get_zone_size(source_list)==0)return 1;

	cur_child			=	get_zone_ptr(source_list,0);
	new_child_size		=	0;
	has_rem				=	0;
	while(cur_child->zone!=PTR_NULL)
	{
		if((tree_mamanger_get_node_type(cur_child)==child_type)&&(cmp_child_value	(cur_child,crc_32,value)))
			has_rem	 =	1	;
		else
			new_child_size	+=	sizeof(mem_zone_ref);

		cur_child++;
	}
	if(!has_rem)return 1;
	
	while( ((p_node->last_write==p_node->cur_write)&&(p_node->is_consumed[p_node->last_write]==0))&&
		   (!compare_z_exchange_c(&p_node->is_used[p_node->cur_write],1)))
	{
		p_node->cur_write	^=	1;
	}
	
	p_node->is_consumed[p_node->cur_write]	 = 	0;
	cur_w_child_list						 =	&p_node->child_w_ptr[p_node->cur_write];

	if(new_child_size==0)
	{
		if(!allocate_new_empty_zone(tree_mem_area_id,cur_w_child_list))
		{
			p_node->is_used[p_node->cur_write]		=	0;
			dec_zone_ref	(cur_child);
			writestr("could not allocate new empty child list \n");
			return 0;
		}
	}
	else
	{
		if(!allocate_new_zone			(tree_mem_area_id,new_child_size+sizeof(mem_zone_ref),cur_w_child_list))
		{
			p_node->is_used[p_node->cur_write]		=	0;
			dec_zone_ref	(cur_child);
			writestr("could not allocate new child list \n");
			return 0;
		}
		cur_child	=	get_zone_ptr(source_list,0);
		new_childs	=	get_zone_ptr(cur_w_child_list,0);
		while(cur_child->zone!=PTR_NULL)
		{
			if((tree_mamanger_get_node_type(cur_child)!=child_type)||(!cmp_child_value	(cur_child,crc_32,value)))
				copy_zone_ref((new_childs++),cur_child);

			cur_child++;
		}
		new_childs->zone						 = 	PTR_NULL;
	}

	set_zone_free								(cur_w_child_list,tree_manager_free_node_array);


	
	p_node->is_used[p_node->cur_write]		 =	0;
	p_node->last_write						 =	p_node->cur_write;
	p_node->cur_write						^=	1;

	return 1;
	/*
	int				n;
	unsigned int    crc_member,node_value;
	mem_zone_ref	*childs_ptr;
	

	
	if(p_node_ref==PTR_NULL)return 0;
	if(p_node_ref->zone==PTR_NULL)return 0;
	if(get_node(p_node_ref)->childs_ptr.zone==PTR_NULL)return 0;
	if(get_zone_size(&get_node(p_node_ref)->childs_ptr)<=0)return 0;

	
	crc_member		=	calc_crc32_c(member_name,32);
	
	
	n=0;
	while(((childs_ptr	=	get_zone_ptr(&get_node(p_node_ref)->childs_ptr,0)))->zone!=PTR_NULL)
	{
		if(get_zone_size(childs_ptr)>0)
		{
			if(tree_mamanger_get_node_type(childs_ptr)==child_type)
			{
				if(tree_manager_get_child_value_i32(childs_ptr,crc_member,&node_value))
				{
					if(node_value==value)
					{
						release_zone_ref(childs_ptr);

						while((childs_ptr+1)->zone!=PTR_NULL)
						{
							childs_ptr->zone	=	(childs_ptr+1)->zone;
							childs_ptr++;
						}
						childs_ptr->zone=PTR_NULL;
						continue;
					}
				}
			}
		}
		n++;
	}

	return 1;
	*/
}




OS_API_C_FUNC(int)	tree_remove_child_by_value_dword	(mem_zone_ref *p_node_ref,unsigned int value)
{
	mem_zone_ref_ptr cur_child;
	mem_zone_ref_ptr cur_w_child_list;
	tree_node		 *p_node;
	mem_zone_ref_ptr new_childs,source_list;
	size_t	    	 new_child_size;
	unsigned int	has_rem;

	if(p_node_ref==PTR_NULL)return 0;
	if(p_node_ref->zone==PTR_NULL)return 0; 

	p_node				=	get_node			(p_node_ref);

	if((p_node->last_write==0xFFFFFFFF)||(p_node->is_consumed[p_node->last_write]))
		source_list	=	&p_node->childs_ptr;
	else
		source_list	=	&p_node->child_w_ptr[p_node->last_write];

	if(source_list->zone==PTR_NULL)return 1;
	if(get_zone_size(source_list)==0)return 1;

	cur_child			=	get_zone_ptr(source_list,0);
	new_child_size		=	0;
	has_rem				=	0;
	while(cur_child->zone!=PTR_NULL)
	{
		if(!tree_mamanger_compare_node_dword(cur_child,0,value))
			has_rem	 =	1	;
		else
			new_child_size	+=	sizeof(mem_zone_ref);

		cur_child++;
	}
	if(!has_rem)return 1;
	
	while( ((p_node->last_write==p_node->cur_write)&&(p_node->is_consumed[p_node->last_write]==0))&&
		   (!compare_z_exchange_c(&p_node->is_used[p_node->cur_write],1)))
	{
		p_node->cur_write	^=	1;
	}
	
	p_node->is_consumed[p_node->cur_write]	 = 	0;
	cur_w_child_list						 =	&p_node->child_w_ptr[p_node->cur_write];

	if(new_child_size==0)
	{
		if(!allocate_new_empty_zone(tree_mem_area_id,cur_w_child_list))
		{
			p_node->is_used[p_node->cur_write]		=	0;
			dec_zone_ref	(cur_child);
			writestr("could not allocate new empty child list \n");
			return 0;
		}
	}
	else
	{
		if(!allocate_new_zone			(tree_mem_area_id,new_child_size+sizeof(mem_zone_ref),cur_w_child_list))
		{
			p_node->is_used[p_node->cur_write]		=	0;
			dec_zone_ref	(cur_child);
			writestr("could not allocate new child list \n");
			return 0;
		}
		cur_child	=	get_zone_ptr(source_list,0);
		new_childs	=	get_zone_ptr(cur_w_child_list,0);
		while(cur_child->zone!=PTR_NULL)
		{
			if(tree_mamanger_compare_node_dword(cur_child,0,value))
				copy_zone_ref((new_childs++),cur_child);

			cur_child++;
		}
		new_childs->zone						 = 	PTR_NULL;
	}

	set_zone_free								(cur_w_child_list,tree_manager_free_node_array);


	
	p_node->is_used[p_node->cur_write]		 =	0;
	p_node->last_write						 =	p_node->cur_write;
	p_node->cur_write						^=	1;

	return 1;


	/*
	mem_zone_ref	*childs_ptr;

	
	if(p_node_ref==PTR_NULL)return 0;
	if(p_node_ref->zone==PTR_NULL)return 0;



	if(get_node(p_node_ref)->childs_ptr.zone==PTR_NULL)return 0;
	if(get_zone_size(&get_node(p_node_ref)->childs_ptr)<=0)return 0;

	
	n			=	0;
	while((childs_ptr=get_zone_ptr(&get_node(p_node_ref)->childs_ptr,n*sizeof(mem_zone_ref)))->zone!=PTR_NULL)
	{
		if(get_zone_size(childs_ptr)>0)
		{
			if(!tree_mamanger_compare_node_dword(childs_ptr,0,value))
			{
				while(childs_ptr->zone!=PTR_NULL)
				{
					copy_zone_ref		(childs_ptr,childs_ptr+1);
					release_zone_ref	(childs_ptr+1);
					childs_ptr++;
				}
				continue;
			}
		}
		n++;
	}
	*/

	return 1;
}





unsigned int tree_manager_find_node_list(mem_zone_ref_ptr child_list,mem_zone_ref_ptr cur_child,struct node_hash_val_t *list)
{
	unsigned int n_found=0;
	
	while(cur_child->zone!=PTR_NULL)
	{
		struct node_hash_val_t *list_loc;

		list_loc=list;

		while(list_loc->data!=PTR_NULL)
		{
			if(get_node(cur_child)->name_crc==list_loc->crc)
			{
				//writestr_fmt("node : '%s' %p %p %p %p\n",get_node(cur_child)->name,list_loc->data,cur_child,cur_child->zone,(cur_child+1)->zone);
				switch(get_node(cur_child)->node_type)
				{
					case NODE_GFX_SIGNED_INT:
					case NODE_GFX_SIGNED_INT64:
						tree_mamanger_get_node_signed_dword	(cur_child,0,list_loc->data);
					break;

					case NODE_GFX_INT:
					case NODE_GFX_INT64:
						tree_mamanger_get_node_dword		(cur_child,0,list_loc->data);		
					break;
					case NODE_GFX_STR:
						*((mem_ptr *)(list_loc->data))=tree_mamanger_get_node_data_ptr(cur_child,0);
					break;
					case NODE_GFX_4UC:
						tree_manager_get_node_4uc		(cur_child,0,list_loc->data);
					break;
				}
				n_found++;
				break;
			}
			list_loc++;
		}
		dec_zone_ref		(cur_child);
		
		while(((++cur_child)->zone)!=PTR_NULL)
		{
			if(inc_zone_ref		(cur_child)==1)break;
		}
	}

	release_zone_ref		(child_list);

	return n_found;


}



OS_API_C_FUNC(int) tree_manager_add_child_node	(mem_zone_ref *parent_ref_ptr,const char *name,unsigned int type,mem_zone_ref *ref_ptr)
{
	int					res;
	mem_zone_ref		new_node={PTR_NULL};

	if(parent_ref_ptr==PTR_NULL)return 0;
	if(parent_ref_ptr->zone==PTR_NULL)return 0;

	
	if(!tree_manager_create_node(name,type,&new_node))
	{
		writestr("could not create new child node \n");
		return 0;
	}
	res	=	tree_manager_node_add_child	(parent_ref_ptr,&new_node);

	if(ref_ptr!=PTR_NULL)
		copy_zone_ref						(ref_ptr,&new_node);
	release_zone_ref					(&new_node);
	return res;


}


OS_API_C_FUNC(int) tree_manager_get_last_child		(mem_zone_ref_const_ptr p_node_ref,mem_zone_ref_ptr child_list, mem_zone_ref_ptr *p_node_out_ref)
{
	unsigned int	cur_read,last_write;
	tree_node		*p_node;
	mem_zone_ref_ptr last_child,first_child;
	mem_size		childs_size,last_child_ofs;


	if(p_node_ref==PTR_NULL)return 0;
	if(p_node_ref->zone==PTR_NULL)return 0;
	if(p_node_ref->zone==uint_to_mem(0xDEF0DEF0))return 0;
	p_node=get_node(p_node_ref);

	cur_read			=	0;
	last_write			=	p_node->last_write;

	if((last_write!=0xFFFFFFFF)&&(compare_z_exchange_c(&p_node->is_used[last_write],1)))
	{
		if(!p_node->is_consumed[last_write])
		{
			swap_zone_ref		(&p_node->childs_ptr,&p_node->child_w_ptr[last_write]);

			//release_zone_ref	(&p_node->child_w_ptr[last_write]);
			p_node->is_consumed[last_write]=1;
		}
		p_node->is_used[last_write]=0;
	}

	if(p_node->childs_ptr.zone==PTR_NULL)return 0;
	copy_zone_ref	(child_list,&p_node->childs_ptr);

	childs_size=get_zone_size(child_list);
	if(childs_size==0)
	{
		release_zone_ref		(child_list);
		return 0;
	}

	first_child			=	get_zone_ptr(child_list,0);
	if(first_child->zone == PTR_NULL)
	{
		release_zone_ref		(child_list);
		return 0;
	}

	last_child_ofs		=	memchr_32_c(get_zone_ptr(child_list,0),mem_to_uint(PTR_NULL),childs_size);
	last_child			=	get_zone_ptr(child_list,last_child_ofs-sizeof(mem_zone_ref));

	if(p_node_out_ref!=PTR_NULL)
	{
		inc_zone_ref			(last_child);
		(*p_node_out_ref)	=	last_child;
	}
	return 1;

	/*
	tree_node		*p_node;
	mem_zone_ref_ptr last_child,first_child;
	mem_size		childs_size,last_child_ofs;

	if(p_node_ref==PTR_NULL)return 0;
	if(p_node_ref->zone==PTR_NULL)return 0;
	p_node=get_node(p_node_ref);
	if(p_node->childs_ptr.zone==PTR_NULL)return 0;
	childs_size=get_zone_size(&p_node->childs_ptr);
	if(childs_size==0)return 0;
	first_child			=	get_zone_ptr(&p_node->childs_ptr,0);
	if(first_child->zone==PTR_NULL)return 0;

	copy_zone_ref			(child_list,&p_node->childs_ptr);

	last_child_ofs		=	memchr_32_c(get_zone_ptr(child_list,0),mem_to_uint(PTR_NULL),childs_size);
	last_child			=	get_zone_ptr(child_list,last_child_ofs-sizeof(mem_zone_ref));

	inc_zone_ref			(last_child);
	(*p_node_out_ref)	=	last_child;
	return 1;
	*/
}

OS_API_C_FUNC(int) tree_manager_get_prev_child		(mem_zone_ref_ptr child_list,mem_zone_ref_ptr *p_node_out_ref)
{
	mem_zone_ref_ptr prev_child;
	mem_ptr			first_child;

	if((*p_node_out_ref) == PTR_NULL)return 0;
	if((*p_node_out_ref)->zone	== PTR_NULL)return 0;

	dec_zone_ref			((*p_node_out_ref));

	first_child=get_zone_ptr(child_list,0);
	if((*p_node_out_ref) == first_child)
	{
		release_zone_ref		(child_list);
		(*p_node_out_ref)	=	PTR_NULL;
		return 0;
	}

	prev_child			=	(*p_node_out_ref)-1;

	inc_zone_ref			(prev_child);
	(*p_node_out_ref)	=	prev_child;
	return 1;
}






OS_API_C_FUNC(int) tree_manager_create_node_childs(const char *name,unsigned int type,mem_zone_ref *ref_ptr,const char *params)
{
	mem_zone_ref_ptr	childs;
	mem_zone_ref		new_node={PTR_NULL};
		
	if(!tree_manager_create_node	(name,type,&new_node))
	{
		writestr						("could create new json node \n");
		return 0;
		
	}

	allocate_new_empty_zone(tree_mem_area_id,&get_node(&new_node)->childs_ptr);
	if(tree_manager_json_loadb	(params,strlen_c(params),&get_node(&new_node)->childs_ptr)==0)
	{
		writestr						("could not parse json data '");
		writestr						(params);
		writestr						("'\n");
		
		tree_manager_free_node_array	(&get_node(&new_node)->childs_ptr);
		release_zone_ref				(&get_node(&new_node)->childs_ptr);
		release_zone_ref				(&new_node);
		return 0;
	}

	childs	=	get_zone_ptr(&get_node(&new_node)->childs_ptr,0);
	while(childs->zone!=PTR_NULL) get_node(childs++)->parent_zone=new_node.zone;


	copy_zone_ref	(ref_ptr,&new_node);
	release_zone_ref(&new_node);
	return 1;

}

OS_API_C_FUNC(int) tree_manager_create_node_params(mem_zone_ref_ptr ref_ptr,const char *params)
{
	if(params==PTR_NULL)return 0;
	
	if(tree_manager_json_loadb	(params,strlen_c(params),ref_ptr)==1)return 1;

	writestr						("could not parse json data params '");
	writestr						(params);
	writestr						("'\n");
	release_zone_ref				(ref_ptr);
	return 0;
}


int node_comp(unsigned int ctx,const_mem_ptr d,const_mem_ptr s)
{
	unsigned int	d_type,s_type;

	mem_ptr			d_ptr,s_ptr;

	if((s_type=tree_manager_get_child_type	(s,ctx))!=0)
		tree_manager_get_child_data_ptr		(s,ctx,&s_ptr);
	else return 1;
	
	if((d_type=tree_manager_get_child_type(d,ctx))!=0)
		tree_manager_get_child_data_ptr		(d,ctx,&d_ptr);
	else return -1;
		

	if(d_type==s_type)
	{
		switch(d_type)
		{
			case NODE_GFX_INT:
				if((*((unsigned int *)(d_ptr)))>  (*((unsigned int *)(s_ptr))))return 1;
				if((*((unsigned int *)(d_ptr)))== (*((unsigned int *)(s_ptr))))return 0;
				return -1;
			break;
			case NODE_GFX_SIGNED_INT:
				if((*((int *)(d_ptr)))>  (*((int *)(s_ptr))))return 1;
				if((*((int *)(d_ptr)))== (*((int *)(s_ptr))))return 0;
				return -1;
			break;
			case NODE_GFX_STR:
				return strcmp_c(d_ptr,s_ptr);
			break;
		}
	}
	return -1;

}

int node_comp_inv(unsigned int ctx,const_mem_ptr d,const_mem_ptr s)
{
	unsigned int	d_type,s_type;
	mem_ptr			d_ptr,s_ptr;


	if((s_type=tree_manager_get_child_type	(s,ctx))!=0)
		tree_manager_get_child_data_ptr		(s,ctx,&s_ptr);
	else return -1;
	
	if((d_type=tree_manager_get_child_type(d,ctx))!=0)
		tree_manager_get_child_data_ptr		(d,ctx,&d_ptr);
	else return 1;

	if(d_type==s_type)
	{
		switch(d_type)
		{
			case NODE_GFX_INT:
				if((*((unsigned int *)(d_ptr)))<  (*((unsigned int *)(s_ptr))))return 1;
				if((*((unsigned int *)(d_ptr)))== (*((unsigned int *)(s_ptr))))return 0;
				return -1;
			break;
			case NODE_GFX_SIGNED_INT:
				if((*((int *)(d_ptr)))<  (*((int *)(s_ptr))))return 1;
				if((*((int *)(d_ptr)))== (*((int *)(s_ptr))))return 0;
				return -1;
			break;
			case NODE_GFX_STR:
				return -strcmp_c(d_ptr,s_ptr);
			break;
		}
	}
	return -1;

}
OS_API_C_FUNC(void) tree_manager_sort_childs		(mem_zone_ref_ptr parent_ref_ptr,const char *name,unsigned int dir)
{

mem_zone_ref	 src_child_list={PTR_NULL};
	mem_zone_ref_ptr src_child;
	mem_zone_ref_ptr cur_w_child_list;
	tree_node		 *p_node;
	mem_zone_ref_ptr new_childs;
	size_t	    	 child_size;

	if(parent_ref_ptr==PTR_NULL)return;
	if(parent_ref_ptr->zone==PTR_NULL)return; 
	if(!tree_manager_get_first_child(parent_ref_ptr,&src_child_list,&src_child))return;

	p_node				=	get_node			(parent_ref_ptr);
	
	while( ((p_node->last_write==p_node->cur_write)&&(p_node->is_consumed[p_node->last_write]==0))&&
		   (!compare_z_exchange_c(&p_node->is_used[p_node->cur_write],1)))
	{
		p_node->cur_write	^=	1;
	}
	cur_w_child_list				=	&p_node->child_w_ptr[p_node->cur_write];
	child_size						=	memchr_32_c(get_zone_ptr(&src_child_list,0),mem_to_uint(PTR_NULL),get_zone_size(&src_child_list));
	
	if(!allocate_new_zone			(tree_mem_area_id,child_size+sizeof(mem_zone_ref),cur_w_child_list))
	{
		dec_zone_ref	(src_child);
		release_zone_ref(&src_child_list);
		p_node->is_used[p_node->cur_write]		=	0;
		writestr("could not allocate new child list \n");
		snooze(2000000);
		return ;
	}
	set_zone_free	(cur_w_child_list,tree_manager_free_node_array);
	new_childs	=	get_zone_ptr(cur_w_child_list,0);

	while(src_child->zone!=PTR_NULL)
	{
		if((src_child->zone!=uint_to_mem(0xDEF0DEF0))&&(src_child->zone!=PTR_INVALID)&&(get_zone_size(src_child)>0))
		{
			copy_zone_ref((new_childs++),src_child);
		}
		if(!tree_manager_get_next_child(&src_child_list,&src_child))break;
	}
	new_childs->zone						= 	PTR_NULL;

	new_childs	=	get_zone_ptr(cur_w_child_list,0);
	
	if(dir==0)
		qsort_ctx_c		(new_childs,child_size/sizeof(mem_zone_ref),sizeof(mem_zone_ref),NODE_HASH(name),node_comp);
	else
		qsort_ctx_c		(new_childs,child_size/sizeof(mem_zone_ref),sizeof(mem_zone_ref),NODE_HASH(name),node_comp_inv);

	
	

	p_node->is_consumed[p_node->cur_write]	 = 	0;
	p_node->is_used[p_node->cur_write]		 =	0;
	p_node->last_write						 =	p_node->cur_write;
	p_node->cur_write						^=	1;

	return;

	/*
	tree_node		*p_node;
	mem_zone_ref	*childs_ptr;
	size_t			 num,child_size;

	if(parent_ref_ptr==PTR_NULL)return;
	if(parent_ref_ptr->zone==PTR_NULL)return;
	
	p_node=get_node(parent_ref_ptr);
	if(p_node->childs_ptr.zone==PTR_NULL)return;
	if((child_size=get_zone_size(&p_node->childs_ptr))==0)return;


	childs_ptr	=	get_zone_ptr(&p_node->childs_ptr,0);
	num			=	memchr_32_c	(childs_ptr,mem_to_uint(PTR_NULL),child_size)/sizeof(mem_zone_ref);
	
	if(dir==0)
		qsort_ctx_c		(childs_ptr,num,sizeof(mem_zone_ref),NODE_HASH(name),node_comp);
	else
		qsort_ctx_c		(childs_ptr,num,sizeof(mem_zone_ref),NODE_HASH(name),node_comp_inv);
		*/
}





OS_API_C_FUNC(unsigned int) tree_mamanger_get_node_type	(mem_zone_ref_const_ptr node_ref)
{
	if(node_ref->zone==PTR_NULL)return 0xFFFFFFFF;
	if(node_ref->zone==uint_to_mem(0xDEF0DEF0))return 0xFFFFFFFF;
	if(get_zone_size(node_ref)==0)return 0xFFFFFFFF;

	return ((tree_node *)(get_zone_ptr(node_ref,0)))->node_type;
}
OS_API_C_FUNC(const char *) tree_mamanger_get_node_name	(mem_zone_ref_const_ptr node_ref)
{
	if(node_ref->zone==PTR_NULL)return PTR_NULL;
	return get_node(node_ref)->name;
}

OS_API_C_FUNC(void) tree_manager_set_node_name	(mem_zone_ref_ptr node_ref,const char *name)
{
	if(node_ref==PTR_NULL)return ;
	if(node_ref->zone==PTR_NULL)return ;

	strcpy_s							(get_node(node_ref)->name,32,name);
	get_node(node_ref)->name_crc		=	calc_crc32_c	(name,32);
}



OS_API_C_FUNC(int) tree_manager_get_ancestor_by_type	(mem_zone_ref_const_ptr  node_ref,unsigned int node_type,mem_zone_ref_ptr  p_ref)
{
	mem_zone_ref	pzone_ref;
	
	if(get_node(node_ref)->parent_zone==PTR_NULL)
		return 0;

	pzone_ref.zone=get_node(node_ref)->parent_zone;

	if(get_node(&pzone_ref)->node_type==node_type)
	{
		copy_zone_ref	(p_ref	,&pzone_ref);
		return 1;
	}


	return tree_manager_get_ancestor_by_type(&pzone_ref,node_type,p_ref);
}
OS_API_C_FUNC(int) tree_mamanger_get_parent	(mem_zone_ref_const_ptr  node_ref,mem_zone_ref_ptr  p_ref)
{
	mem_zone_ref	pzone_ref;

	pzone_ref.zone	=	get_node(node_ref)->parent_zone;

	copy_zone_ref	(p_ref,&pzone_ref);
	return 1;
}

OS_API_C_FUNC(unsigned int) and_node_type(unsigned int type1,unsigned int type2)
{
	if((type1&0xFF000000)!=(type2&0xFF000000))return 0;
	return ((type1&0x00FFFFFF)&(type2&0x00FFFFFF));
}

OS_API_C_FUNC(void) tree_manager_set_output(int output)
{
	tree_output		=	output;
}

OS_API_C_FUNC(void) tree_manager_set_node_image_info	(mem_zone_ref *node_ref,mem_size position,mem_size size)
{
	get_node(node_ref)->image_position	=position;
	get_node(node_ref)->image_size		=size;
	
}
OS_API_C_FUNC(unsigned int) tree_manager_compare_node_crc		(mem_zone_ref *node_ref,unsigned int crc)
{
	unsigned int node_crc;

	node_crc=(get_node(node_ref))->name_crc;

	if(crc==node_crc)
		return 1;
	else
		return 0;

}

OS_API_C_FUNC(size_t) tree_manager_get_node_image_pos	(mem_zone_ref_const_ptr node_ref)
{
	return get_node(node_ref)->image_position;
}

OS_API_C_FUNC(size_t) tree_manager_get_node_image_size	(mem_zone_ref_const_ptr node_ref)
{
	return get_node(node_ref)->image_size;
}

OS_API_C_FUNC(int) tree_manager_allocate_node_data	(mem_zone_ref *node_ref,mem_size data_size)
{
	mem_zone_ref	child_list={PTR_NULL};
	mem_zone_ref_ptr child;

	if(node_ref==PTR_NULL)return 0;
	if(node_ref->zone==PTR_NULL)return 0;
	if(realloc_zone(node_ref,sizeof(tree_node)+data_size+16)<0)
	{
		writestr("error alloc node data \n");
		return 0;
	}
	if(tree_manager_get_first_child(node_ref,&child_list,&child))
	{
		while(child->zone!=PTR_NULL)
		{
			get_node(child)->parent_zone=node_ref->zone;
			if(!tree_manager_get_next_child(&child_list,&child))break;
		}
	}
	return 1;
}

__inline size_t tree_mamanger_get_node_data_size(mem_zone_ref_const_ptr node_ref)
{
	return get_zone_size(node_ref)-sizeof(tree_node);
}

__inline mem_ptr tree_manager_prepare_node_mem(mem_zone_ref_const_ptr node_ref,mem_size ofset,mem_size size)
{
	size_t			zone_size;

	if(node_ref==PTR_NULL)return PTR_NULL;
	if(node_ref->zone==PTR_NULL)return PTR_NULL;
	zone_size	=	 tree_mamanger_get_node_data_size(node_ref);
	if(zone_size==0)return PTR_NULL;
	if((ofset+size)>zone_size)return PTR_NULL;
	return get_zone_ptr(node_ref,ofset+sizeof(tree_node));

}


__inline mem_ptr tree_manager_prepare_node_mem_alloc(mem_zone_ref_ptr node_ref,mem_size ofset,mem_size size)
{
	mem_ptr			data_ptr;
	size_t			zone_size;
	size_t			end_write;

	if(node_ref==PTR_NULL)return PTR_NULL;
	if(node_ref->zone==PTR_NULL)return PTR_NULL;

	zone_size	=	 tree_mamanger_get_node_data_size(node_ref);
	end_write	=	ofset+size;
	
	if((end_write>zone_size))
	{
		if(!tree_manager_allocate_node_data	(node_ref,end_write+sizeof(tree_node)))return PTR_NULL;
	}
	data_ptr	=	get_zone_ptr(node_ref,ofset+sizeof(tree_node));
	return data_ptr;

}



OS_API_C_FUNC(int) tree_mamanger_get_node_signed_word(mem_zone_ref_const_ptr node_ref,mem_size ofset,short *val)
{
	mem_ptr			data_ptr;

	data_ptr	=	tree_manager_prepare_node_mem(node_ref,ofset,sizeof(short));
	if(data_ptr	== PTR_NULL)return 0;
	*val		=	*((short *)(data_ptr));
	return 1;



}

OS_API_C_FUNC(int) tree_manager_get_node_str(mem_zone_ref_const_ptr node_ref,mem_size ofset,char *str,unsigned int str_len,unsigned int base)
{
	mem_ptr			data_ptr;
	data_ptr	=	tree_mamanger_get_node_data_ptr(node_ref,ofset);
	if(data_ptr	== PTR_NULL)return 0;

	switch(get_node(node_ref)->node_type)
	{
		case NODE_GFX_STR		:strcpy_s	(str,str_len,data_ptr);break;
		case NODE_GFX_INT		:uitoa_s	(*((unsigned int *)	(data_ptr)),str,str_len,base);break;
		case NODE_GFX_EVENT		:uitoa_s	(*((unsigned int *)	(data_ptr)),str,str_len,base);break;
		case NODE_GFX_SIGNED_INT:uitoa_s	(*((int *)			(data_ptr)),str,str_len,base);break;
		default:*str=0;break;
	}
	
	return	1;
}

OS_API_C_FUNC(int) tree_manager_write_node_str(mem_zone_ref_ptr node_ref,mem_size ofset,const char *str)
{
	
	mem_ptr			data_ptr;
	data_ptr	=	tree_manager_prepare_node_mem_alloc(node_ref,ofset,strlen_c(str)+1);
	if(data_ptr	== PTR_NULL)return 0;
	memcpy_c	(data_ptr,str,strlen_c(str)+1);
	return	1;
}

/*
OS_API_C_FUNC(void) tree_mamanger_get_node_data_ref(mem_zone_ref_const_ptr node_ref,mem_zone_ref_ptr out)
{
	create_zone_ref(out,tree_mamanger_get_node_data_ptr(node_ref,0),tree_mamanger_get_node_data_size(node_ref));
	return ;
}*/



OS_API_C_FUNC(mem_ptr) tree_mamanger_get_node_data_ptr(mem_zone_ref_const_ptr node_ref,mem_size ofset)
{
	return tree_manager_prepare_node_mem(node_ref,ofset,0);
}


OS_API_C_FUNC(int) tree_manager_write_node_data	(mem_zone_ref *node_ref,const_mem_ptr data,mem_size ofset,mem_size size)
{
	mem_ptr			data_ptr;

	if(size==0)return 0;
	if(data==PTR_NULL)return 0;

	data_ptr	=	tree_manager_prepare_node_mem_alloc(node_ref,ofset,size);
	if(data_ptr	== PTR_NULL)
	{
		return 0;
	}
	memcpy_c	(data_ptr,data,size);
	return 1;
}

OS_API_C_FUNC(int) tree_manager_copy_node_data		(mem_zone_ref_ptr dst_node,mem_zone_ref_const_ptr src_node)
{
	mem_ptr			src_ptr,dst_ptr;
	size_t			zone_size;

	if(src_node==PTR_NULL)return 0;
	if(src_node->zone==PTR_NULL)return 0;

	zone_size	=	tree_mamanger_get_node_data_size	(src_node);
	src_ptr		=	tree_manager_prepare_node_mem		(src_node,0,zone_size);
	dst_ptr		=	tree_manager_prepare_node_mem_alloc	(dst_node,0,zone_size);

	if(src_ptr==PTR_NULL)return 0;
	if(dst_ptr==PTR_NULL)return 0;
	memcpy_c		(dst_ptr,src_ptr,zone_size);

	return 1;
}

OS_API_C_FUNC(int) tree_manager_write_node_4uc(mem_zone_ref_ptr node_ref,mem_size ofset,const vec_4uc_t val)
{
	unsigned char *data_ptr;

	data_ptr	=	tree_manager_prepare_node_mem_alloc(node_ref,ofset,sizeof(vec_4uc_t));
	if(data_ptr	== PTR_NULL)return 0;
	copy_vec4u_c(data_ptr,val);
	return 1;
}

OS_API_C_FUNC(int) tree_manager_write_node_dword	(mem_zone_ref *node_ref,mem_size ofset,unsigned int value)
{
	unsigned int	*data_ptr;

	data_ptr	=	tree_manager_prepare_node_mem_alloc(node_ref,ofset,sizeof(unsigned int));
	if(data_ptr	== PTR_NULL)
	{
		return 0;
	}
	(*data_ptr) =   value;
	return 1;
}


OS_API_C_FUNC(int) tree_manager_cmp_z_xchg_node_dword	(mem_zone_ref *node_ref,mem_size ofset,unsigned int value)
{
	unsigned int	*data_ptr;

	data_ptr	=	tree_manager_prepare_node_mem_alloc(node_ref,ofset,sizeof(unsigned int));
	if(data_ptr	== PTR_NULL)return 0;
	return compare_z_exchange_c(data_ptr,value);
}



OS_API_C_FUNC(int) tree_manager_write_node_qword	(mem_zone_ref *node_ref,mem_size ofset,uint64_t value)
{
	uint64_t	*data_ptr;

	data_ptr	=	tree_manager_prepare_node_mem_alloc(node_ref,ofset,sizeof(uint64_t));
	if(data_ptr	== PTR_NULL)
	{
		return 0;
	}
	(*data_ptr) =   value;
	return 1;
}

OS_API_C_FUNC(int) tree_manager_write_node_signed_qword	(mem_zone_ref *node_ref,mem_size ofset,int64_t value)
{
	int64_t	*data_ptr;

	data_ptr	=	tree_manager_prepare_node_mem_alloc(node_ref,ofset,sizeof(int64_t));
	if(data_ptr	== PTR_NULL)
	{
		return 0;
	}
	(*data_ptr) =   value;
	return 1;
}


OS_API_C_FUNC(int) tree_manager_write_node_ptr	(mem_zone_ref *node_ref,mem_size ofset,mem_ptr value)
{
	mem_ptr	*data_ptr;

	data_ptr	=	tree_manager_prepare_node_mem_alloc(node_ref,ofset,sizeof(mem_ptr));
	if(data_ptr	== PTR_NULL)return 0;
	(*data_ptr) =   value;
	return 1;

}


OS_API_C_FUNC(int) tree_manager_get_node_ptr	(mem_zone_ref *node_ref,mem_size ofset,mem_ptr *value)
{
	mem_ptr	*data_ptr;

	data_ptr	=	tree_manager_prepare_node_mem(node_ref,ofset,sizeof(mem_ptr));
	if(data_ptr	== PTR_NULL)return 0;
	*value		=	(*data_ptr);
	return 1;

}

OS_API_C_FUNC(int) tree_manager_write_node_word	(mem_zone_ref *node_ref,mem_size ofset,unsigned short value)
{
	unsigned short	*data_ptr;

	data_ptr	=	tree_manager_prepare_node_mem_alloc(node_ref,ofset,sizeof(unsigned short));
	if(data_ptr	== PTR_NULL)return 0;
	(*data_ptr) =   value;
	return 1;

}


OS_API_C_FUNC(int) tree_manager_write_node_byte	(mem_zone_ref *node_ref,mem_size ofset,unsigned char value)
{
	unsigned char	*data_ptr;

	data_ptr	=	tree_manager_prepare_node_mem_alloc(node_ref,ofset,sizeof(unsigned char));
	if(data_ptr	== PTR_NULL)return 0;
	(*data_ptr) =   value;
	return 1;
	
}


OS_API_C_FUNC(int) tree_manager_write_node_signed_dword	(mem_zone_ref *node_ref,mem_size ofset,int value)
{
	int	*data_ptr;



	data_ptr	=	tree_manager_prepare_node_mem_alloc(node_ref,ofset,sizeof(int));
	if(data_ptr	== PTR_NULL)return 0;
	(*data_ptr) =   value;
	return 1;

}


OS_API_C_FUNC(int) tree_manager_write_node_signed_word	(mem_zone_ref *node_ref,mem_size ofset,short value)
{
	short	*data_ptr;

	data_ptr	=	tree_manager_prepare_node_mem_alloc(node_ref,ofset,sizeof(short));
	if(data_ptr	== PTR_NULL)return 0;
	(*data_ptr) =   value;
	return 1;

	
}


OS_API_C_FUNC(int) tree_mamanger_get_node_byte(mem_zone_ref_const_ptr node_ref,mem_size ofset,unsigned char *val)
{
	mem_ptr			data_ptr;

	data_ptr	=	tree_manager_prepare_node_mem(node_ref,ofset,sizeof(unsigned char));
	if(data_ptr	== PTR_NULL)return 0;
	*val		=	*((unsigned char *)(data_ptr));
	return 1;

}

OS_API_C_FUNC(int) tree_mamanger_get_node_word(mem_zone_ref_const_ptr node_ref,mem_size ofset,unsigned short *val)
{
	mem_ptr			data_ptr;

	data_ptr	=	tree_manager_prepare_node_mem(node_ref,ofset,sizeof(unsigned short));
	if(data_ptr	== PTR_NULL)return 0;
	*val		=	*((unsigned short *)(data_ptr));
	return 1;
}

OS_API_C_FUNC(int) tree_mamanger_get_node_dword(mem_zone_ref_const_ptr node_ref,mem_size ofset,unsigned int *val)
{
	mem_ptr			data_ptr;

	data_ptr	=	tree_manager_prepare_node_mem(node_ref,ofset,sizeof(unsigned int));
	if(data_ptr	== PTR_NULL)return 0;
	*val		=	*((unsigned int *)(data_ptr));
	return 1;
}

OS_API_C_FUNC(int) tree_manager_get_node_4uc(mem_zone_ref_const_ptr node_ref,mem_size ofset,vec_4uc_t val)
{
	unsigned char *data_ptr;

	data_ptr	=	tree_manager_prepare_node_mem(node_ref,ofset,sizeof(vec_4uc_t));
	if(data_ptr	== PTR_NULL)return 0;
	copy_vec4u_c(val,data_ptr);
	return 1;
}

OS_API_C_FUNC(int) tree_mamanger_compare_node_dword(mem_zone_ref_ptr node_ref,mem_size ofset,unsigned int val)
{
	mem_ptr			data_ptr;

	data_ptr	=	tree_manager_prepare_node_mem(node_ref,ofset,sizeof(unsigned int));
	if(data_ptr	== PTR_NULL)return -1;

	if(val>(*((unsigned int *)(data_ptr))))return 1;
	if(val==(*((unsigned int *)(data_ptr))))return 0;
	return -1;
}

OS_API_C_FUNC(int) tree_mamanger_get_node_signed_dword(mem_zone_ref_const_ptr node_ref,mem_size ofset,int *val)
{
	mem_ptr			data_ptr;

	data_ptr	=	tree_manager_prepare_node_mem(node_ref,ofset,sizeof(int));
	if(data_ptr	== PTR_NULL)return 0;
	*val		=	*((int *)(data_ptr));
	return 1;
}




OS_API_C_FUNC(int) tree_node_find_child_by_name (mem_zone_ref_const_ptr p_node_ref,const char *name,mem_zone_ref_ptr p_node_out_ref)
{
	mem_zone_ref	 child_list={PTR_NULL};
	mem_zone_ref_ptr cur_child;
	unsigned int	 crc_name;

	crc_name	=	NODE_HASH(name);

	if(!tree_manager_get_first_child(p_node_ref,&child_list,&cur_child))return 0;
	if(!tree_manager_find_child_crc (&child_list,crc_name,&cur_child))return 0;
	
	if(p_node_out_ref!=PTR_NULL)
		copy_zone_ref	(p_node_out_ref,cur_child);
	
	dec_zone_ref	(cur_child);
	release_zone_ref(&child_list); 
	return 1;
}
OS_API_C_FUNC(int) tree_node_read_childs	(mem_zone_ref_const_ptr p_node_ref,struct node_hash_val_t *list)
{
	mem_zone_ref	 child_list={PTR_NULL};
	mem_zone_ref_ptr cur_child;


	if(!tree_manager_get_first_child(p_node_ref,&child_list,&cur_child))return 0;

	//tree_manager_dump_node_rec(p_node_ref,0,3);
	return tree_manager_find_node_list (&child_list,cur_child,list);
	
}




OS_API_C_FUNC(int) tree_manager_list_child_type		(mem_zone_ref_ptr child_list,unsigned int type,unsigned int *index,mem_zone_ref_ptr *p_node_out_ref)
{
	mem_zone_ref_ptr		next_child;
	next_child			=	(*p_node_out_ref);

	while(next_child->zone!=PTR_NULL)
	{
		if(and_node_type(get_node(next_child)->node_type,type))
		{
			if((*index)==0)
			{
				(*p_node_out_ref)	=	next_child;
				return 1;
			}
			(*index)--;
		}

		dec_zone_ref		(next_child);
		next_child			=	next_child+1;

		if((next_child)->zone==PTR_NULL)break;
		if(get_zone_size(next_child)==0)break;

		inc_zone_ref		(next_child);
	}
	(*p_node_out_ref)	=	next_child;
	release_zone_ref		(child_list);

	return 0;
}
OS_API_C_FUNC(int) tree_node_find_child_by_id	(mem_zone_ref_const_ptr p_node_ref,unsigned int node_id,mem_zone_ref_ptr p_node_out_ref)
{
	mem_zone_ref	 child_list={PTR_NULL};
	mem_zone_ref_ptr cur_child;

	if(cmp_child_value	(p_node_ref,NODE_HASH("id"),node_id))
	{
		copy_zone_ref(p_node_out_ref,p_node_ref);
		return 1;
	}	

	if(!tree_manager_get_first_child(p_node_ref,&child_list,&cur_child))return 0;

	while(cur_child->zone!=PTR_NULL)
	{
		if(tree_node_find_child_by_id(cur_child,node_id,p_node_out_ref))
		{
			dec_zone_ref		(cur_child);
			release_zone_ref	(&child_list); 
			return 1;
		}
		if(!tree_manager_get_next_child(&child_list,&cur_child))break;
	}
	return 0;
}


OS_API_C_FUNC(int) tree_find_child_node_by_id_name	(mem_zone_ref_const_ptr p_node_ref,unsigned int child_type,const char *id_name,unsigned int id_val,mem_zone_ref *p_node_out_ref)
{
	mem_zone_ref	 child_list={PTR_NULL};
	mem_zone_ref_ptr cur_child;
	unsigned int     crc_32; 

	if(!tree_manager_get_first_child(p_node_ref,&child_list,&cur_child))return 0;

	crc_32	=	NODE_HASH(id_name);

	while(cur_child->zone!=PTR_NULL)
	{
		if(tree_mamanger_get_node_type(cur_child)==child_type)
		{
			if(cmp_child_value	(cur_child,crc_32,id_val))
			{
				copy_zone_ref(p_node_out_ref,cur_child);
				return 1;
			}	
		}
		if(!tree_manager_get_next_child(&child_list,&cur_child))break;
	}
	return 0;
}


OS_API_C_FUNC(int) tree_swap_child_node_by_id	(mem_zone_ref_ptr p_node_ref,unsigned int node_id,mem_zone_ref_ptr node)
{
	mem_zone_ref	 child_list={PTR_NULL};
	mem_zone_ref	 new_child_list={PTR_NULL};
	mem_zone_ref_ptr cur_child,new_childs;
	mem_size		 child_size;

	if(!tree_manager_get_first_child(p_node_ref,&child_list,&cur_child))return 0;
	child_size=get_zone_size		(&child_list);
	if(!allocate_new_zone			(tree_mem_area_id,child_size,&new_child_list))
	{
		writestr("could not allocate new child list \n");
		return 0;
	}

	set_zone_free		(&new_child_list,tree_manager_free_node_array);

	new_childs=get_zone_ptr(&new_child_list,0);
	while(cur_child->zone!=PTR_NULL)
	{
		
		if(!cmp_child_value	(cur_child,NODE_HASH("id"),node_id))
			copy_zone_ref(new_childs,cur_child);
		else
		{
			copy_zone_ref		(new_childs	,	node);
			copy_zone_ref		(node		,	cur_child);
			
			get_node(node)->parent_zone			=	PTR_NULL;
			get_node(new_childs)->parent_zone	=	p_node_ref->zone;
		}

		new_childs++;
		tree_manager_get_next_child(&child_list,&cur_child);
	}
	new_childs->zone=PTR_NULL;

	
	swap_zone_ref	(&get_node(p_node_ref)->childs_ptr	,&new_child_list);
	release_zone_ref(&new_child_list);


	return 1;
}

int tree_node_list_child_by_type_rec	(mem_zone_ref_const_ptr node,unsigned int type,unsigned int *index,mem_zone_ref_ptr p_node_out_ref)
{
	mem_zone_ref	 child_list={PTR_NULL};
	mem_zone_ref_ptr cur_child;
	if(node==PTR_NULL)return 0;
	if(node->zone==PTR_NULL)return 0;

	if(!tree_manager_get_first_child	(node,&child_list,&cur_child))return 0;

	if(tree_manager_list_child_type		(&child_list,type,index,&cur_child))
	{
		copy_zone_ref		(p_node_out_ref,cur_child);
		dec_zone_ref		(cur_child);
		release_zone_ref	(&child_list);
		return 1;
	}

	if(!tree_manager_get_first_child	(node,&child_list,&cur_child))return 0;
	
	while(cur_child->zone!=PTR_NULL)
	{
		if(tree_node_list_child_by_type_rec(cur_child,type,index,p_node_out_ref))
		{
			dec_zone_ref		(cur_child);
			release_zone_ref	(&child_list); 
			return 1;
		}
		tree_manager_get_next_child(&child_list,&cur_child);
	}
	return 0;
}


OS_API_C_FUNC(int) tree_node_find_child_by_type_value	(mem_zone_ref_const_ptr node,unsigned int type,unsigned int value,mem_zone_ref_ptr p_node_out_ref)
{
	mem_zone_ref	 child_list={PTR_NULL};
	mem_zone_ref_ptr cur_child;
	
	if(node==PTR_NULL)return 0;
	if(node->zone==PTR_NULL)return 0;

	if(!tree_manager_get_first_child	(node,&child_list,&cur_child))return 0;

	while(cur_child->zone!=PTR_NULL)
	{
		if((get_node(cur_child)->node_type==type)&&((!tree_mamanger_compare_node_dword(cur_child,0,value))))
		{
			copy_zone_ref		(p_node_out_ref,cur_child);
			dec_zone_ref		(cur_child);
			release_zone_ref	(&child_list); 
			return 1;
		}
		tree_manager_get_next_child(&child_list,&cur_child);
	}
	return 0;
}
OS_API_C_FUNC(unsigned int) tree_node_list_child_by_type	(const mem_zone_ref *p_node_ref,unsigned int type,mem_zone_ref *p_node_out_ref,unsigned int index)
{
	unsigned int cnt;
	cnt	=	index;
	return tree_node_list_child_by_type_rec(p_node_ref,type,&cnt,p_node_out_ref);
}






OS_API_C_FUNC(int) tree_manager_node_dup		(mem_zone_ref *new_parent,mem_zone_ref *src_ref_ptr,mem_zone_ref *new_ref_ptr)
{
	mem_zone_ref		child_list={PTR_NULL};
	size_t				sz;
	tree_node			*src_node;
	tree_node			*new_node;
	mem_zone_ref_ptr	childs_ptr;

	src_node								= get_node(src_ref_ptr);

	tree_manager_create_node				(src_node->name,src_node->node_type,new_ref_ptr);

	new_node								=   get_node(new_ref_ptr);	

	new_node->image_position				=	src_node->image_position;
	new_node->image_size					=	src_node->image_size;


	if(tree_mamanger_get_node_data_size(src_ref_ptr)>0)
	{
		tree_manager_write_node_data(new_ref_ptr,tree_mamanger_get_node_data_ptr(src_ref_ptr,0),0,tree_mamanger_get_node_data_size(src_ref_ptr));
	}

	if(tree_manager_get_first_child(src_ref_ptr,&child_list,&childs_ptr))
	{
		
		sz	=	get_zone_size(&child_list);
		allocate_new_zone	(tree_mem_area_id,sz,&new_node->childs_ptr);

		set_zone_free		(&new_node->childs_ptr,tree_manager_free_node_array);

		while(childs_ptr->zone!=PTR_NULL)
		{
			mem_zone_ref	child_copy={PTR_NULL};
			tree_manager_node_dup		(new_ref_ptr,childs_ptr,&child_copy);
			release_zone_ref			(&child_copy);
			if(!tree_manager_get_next_child(&child_list,&childs_ptr))break;
		}
	}
	
	if(new_parent!=PTR_NULL)
		tree_manager_node_add_child	(new_parent,new_ref_ptr);

	return 1;

}


OS_API_C_FUNC(int) tree_manager_node_dup_one		(mem_zone_ref *src_ref_ptr,mem_zone_ref *new_ref_ptr)
{
	mem_zone_ref	new_node_ref={PTR_NULL};
	tree_node		*src_node;
	tree_node		*new_node;

	src_node								= get_node(src_ref_ptr);

	tree_manager_create_node(src_node->name,src_node->node_type,&new_node_ref);

	new_node								=   get_node(&new_node_ref);	

	new_node->image_position				=	src_node->image_position;
	new_node->image_size					=	src_node->image_size;


	if(tree_mamanger_get_node_data_size(src_ref_ptr)>0)
	{
		tree_manager_write_node_data(&new_node_ref,tree_mamanger_get_node_data_ptr(src_ref_ptr,0),0,tree_mamanger_get_node_data_size(src_ref_ptr));
	}

	if((get_node(src_ref_ptr)->childs_ptr.zone!=PTR_NULL)&&(get_zone_size(&get_node(src_ref_ptr)->childs_ptr)>0))
	{
		unsigned int	n;
		mem_zone_ref	*childs_ptr;
		n=0;
		while((childs_ptr=get_zone_ptr(&src_node->childs_ptr,n*sizeof(mem_zone_ref)))->zone!=PTR_NULL)
		{
			if(get_zone_size(childs_ptr)>0)
			{
				tree_manager_node_add_child	(&new_node_ref,childs_ptr);
			}
			n++;
		}
	}

	copy_zone_ref	(new_ref_ptr,&new_node_ref);
	release_zone_ref(&new_node_ref);
	
	

	return 1;

}



OS_API_C_FUNC(int) tree_find_child_node_by_member_name	(mem_zone_ref_const_ptr p_node_ref,unsigned int child_type,unsigned int child_member_type,const char *child_member_name,mem_zone_ref_ptr out_node)
{
	unsigned int     crc_member;
	mem_zone_ref_ptr cur_child;
	mem_zone_ref	 child_list={PTR_NULL};

	if(p_node_ref==PTR_NULL)return 0;
	if(p_node_ref->zone==PTR_NULL)return 0;
	
	

	if(!tree_manager_get_first_child(p_node_ref,&child_list,&cur_child))return 0;

	crc_member		=	calc_crc32_c(child_member_name,32);
	
	while(cur_child->zone!=PTR_NULL)
	{
		if(tree_mamanger_get_node_type(cur_child)==child_type)
		{
			if(tree_manager_find_child_node(cur_child,crc_member,child_member_type,PTR_NULL)==1)
			{
				copy_zone_ref	(out_node,cur_child);
				dec_zone_ref	(cur_child);
				release_zone_ref(&child_list);
				return 1;
			}
		}
		if(!tree_manager_get_next_child(&child_list,&cur_child))break;
	}

	return 0;
}


/*
OS_API_C_FUNC(int) tree_find_child_node_by_value	(mem_zone_ref_const_ptr p_node_ref,unsigned int child_type,const char *id_name,unsigned int id_val,mem_zone_ref *out_node)
{
	int				n;
	unsigned int    crc_id,nodeVal;
	mem_zone_ref	*childs_ptr;

	
	if(p_node_ref==PTR_NULL)return 0;
	if(p_node_ref->zone==PTR_NULL)return 0;
	if(get_zone_size(&get_node(p_node_ref)->childs_ptr)<=0)return 0;

	crc_id			=	calc_crc32_c(id_name,32);
	childs_ptr		=	get_zone_ptr(&get_node(p_node_ref)->childs_ptr,0);	
	n				=	0;
	while(childs_ptr[n].zone!=PTR_NULL)
	{
		if(get_zone_size(&childs_ptr[n])>0)
		{
			if( (tree_mamanger_get_node_type(&childs_ptr[n])==child_type)&&(tree_manager_compare_node_crc(&childs_ptr[n],crc_id)))
			{
				tree_mamanger_get_node_dword	(&childs_ptr[n],0,&nodeVal);

				if(nodeVal==id_val)
				{
					copy_zone_ref	(out_node,&childs_ptr[n]);
					return 1;
				}
			}
		}
		n++;
	}

	return 0;
}
*/




OS_API_C_FUNC(int) tree_find_child_node_idx_by_id	(mem_zone_ref *p_node_ref,unsigned int child_type,unsigned int child_id,unsigned int *out_idx)
{
	unsigned int		crc_id,cnt;
	mem_zone_ref		child_list={PTR_NULL};
	mem_zone_ref_ptr	cur_child;

	
	if(p_node_ref==PTR_NULL)return 0;
	if(p_node_ref->zone==PTR_NULL)return 0;
	if(!tree_manager_get_first_child(p_node_ref,&child_list,&cur_child))return 0;

	crc_id	=	NODE_HASH("id");

	cnt=0;
	while(cur_child->zone!=PTR_NULL)
	{
		if(get_node(cur_child)->node_type ==child_type)
		{
			if(cmp_child_value	(cur_child,crc_id,child_id))
			{
				dec_zone_ref		(cur_child);
				release_zone_ref	(&child_list);
				*out_idx=cnt;
				return 1;
			}
		}
		cnt++;
		if(!tree_manager_get_next_child(&child_list,&cur_child))break;
	}

	/*	
	if(get_zone_size(&get_node(p_node_ref)->childs_ptr)<=0)return 0;
	crc_id			=	calc_crc32_c(id_name,32);
	id_node.zone=	PTR_NULL;
	childs_ptr	=	get_zone_ptr(&get_node(p_node_ref)->childs_ptr,0);	
	n=0;
	while(childs_ptr[n].zone!=PTR_NULL)
	{
		if(get_zone_size(&childs_ptr[n])>0)
		{
			if(tree_mamanger_get_node_type(&childs_ptr[n])==child_type)
			{
				if(tree_manager_find_child_node(&childs_ptr[n],crc_id,0xFFFFFFFF,&id_node)==1)
				{
					tree_mamanger_get_node_dword	(&id_node,0,&nodeId);
					release_zone_ref				(&id_node);

					if(nodeId==id_val)
					{
						*out_idx=n;
						return 1;
					}
				}
			}
		}
		n++;
	}
	*/
	

	return 0;
}


OS_API_C_FUNC(size_t) tree_manager_get_node_num_children		(mem_zone_ref *p_node_ref)
{
	mem_zone_ref		child_list={PTR_NULL};
	size_t				num;

	if(p_node_ref==PTR_NULL)return 0;
	if(p_node_ref->zone==PTR_NULL)return 0;
	if(!tree_manager_get_first_child(p_node_ref,&child_list,PTR_NULL))return 0;

	(num)		=	memchr_32_c(get_zone_ptr(&child_list,0),mem_to_uint(PTR_NULL),get_zone_size(&child_list))/sizeof(mem_zone_ref);

	release_zone_ref(&child_list);

	return num;
}						




OS_API_C_FUNC(int) tree_manager_get_child_at(mem_zone_ref_const_ptr parent_ref_ptr,unsigned int index,mem_zone_ref_ptr ref_ptr)
{
	mem_zone_ref	 child_list={PTR_NULL};
	mem_zone_ref_ptr child;
	mem_size	     last_child;
	

	if(parent_ref_ptr==PTR_NULL)return 0;
	if(parent_ref_ptr->zone==PTR_NULL)return 0;

	if(!tree_manager_get_first_child(parent_ref_ptr,&child_list,PTR_NULL))return 0;


	last_child	=	memchr_32_c(get_zone_ptr(&child_list,0),mem_to_uint(PTR_NULL),get_zone_size(&child_list));
	if((index*sizeof(mem_zone_ref))>=last_child)
	{
		release_zone_ref(&child_list);
		return 0;
	}
	child=get_zone_ptr(&child_list,index*sizeof(mem_zone_ref));
	if(get_zone_size(child)==0)
	{
		release_zone_ref(&child_list);
		return 0;
	}
	copy_zone_ref(ref_ptr,child);
	release_zone_ref(&child_list);
	
	
	return 1;
}




OS_API_C_FUNC(int) tree_node_find_child_by_type (mem_zone_ref_const_ptr		p_node_ref,unsigned int node_type,mem_zone_ref_ptr p_node_out_ref,unsigned int index)
{
	mem_zone_ref_ptr	child;
	mem_zone_ref		child_list={PTR_NULL};

	if(p_node_ref->zone==PTR_NULL)return 0;
	
	if(!tree_manager_get_first_child(p_node_ref,&child_list,&child))return 0;
	if(!tree_manager_list_child_type(&child_list,node_type,&index,&child))return 0;
	{
		copy_zone_ref		(p_node_out_ref,child);
		dec_zone_ref		(child);
		release_zone_ref	(&child_list);
		return 1;
	}
	return 0;

}


OS_API_C_FUNC(int) tree_manager_find_child_node(mem_zone_ref_const_ptr parent_ref_ptr,unsigned int crc_name,unsigned int type,mem_zone_ref_ptr ref_ptr)
{
	unsigned int		cnt;
	mem_zone_ref_ptr	cur_child;
	mem_zone_ref		child_list={PTR_NULL};

	if(parent_ref_ptr->zone==PTR_NULL)return 0;
	if(!tree_manager_get_first_child(parent_ref_ptr,&child_list,&cur_child))return 0;

	cnt=0;

	while(cur_child->zone!=PTR_NULL)
	{
		if( ((crc_name==0xFFFFFFFF)||(get_node(cur_child)->name_crc  ==crc_name))&&
			((type	  ==0xFFFFFFFF)||(get_node(cur_child)->node_type ==type)))
		{
			if((cnt==0)&&(ref_ptr!=PTR_NULL))copy_zone_ref(ref_ptr,cur_child);
			cnt++;
		}
		tree_manager_get_next_child(&child_list,&cur_child);
	}
	return cnt;
}



OS_API_C_FUNC(int) tree_manager_get_child_value_str		(mem_zone_ref_const_ptr	p_node_ref,unsigned int crc_name,char *str,unsigned int str_len,unsigned int base)
{
	int				 ret;
	mem_zone_ref	 child_list={PTR_NULL};
	mem_zone_ref_ptr cur_child;
	if(!tree_manager_get_first_child(p_node_ref,&child_list,&cur_child))return 0;
	if(!tree_manager_find_child_crc (&child_list,crc_name,&cur_child))return 0;
	

	ret=tree_manager_get_node_str(cur_child,0,str,str_len,base);
	
	dec_zone_ref				(cur_child);
	release_zone_ref			(&child_list); 
	return ret;

}
/*
OS_API_C_FUNC(int) C_API_FUNC	tree_manager_get_child_state(const mem_zone_ref	*p_node_ref,unsigned int crc_name)
{
	int				n;
	mem_zone_ref	*childs_ptr;


	if(p_node_ref==PTR_NULL)return 0;
	if(p_node_ref->zone==PTR_NULL)return 0;
	if(get_node(p_node_ref)->childs_ptr.zone==PTR_NULL)return 0;
	if(get_zone_size(&get_node(p_node_ref)->childs_ptr)<=0)return 0;

	childs_ptr	=	get_zone_ptr(&get_node(p_node_ref)->childs_ptr,0);	

	n=0;
	while(childs_ptr[n].zone!=PTR_NULL)
	{
		if(get_zone_size(&childs_ptr[n])>0)
		{
			if(get_node(&childs_ptr[n])->name_crc==crc_name)
			{
				int				ret;
				unsigned int	value;

				ret=tree_mamanger_get_node_dword(&childs_ptr[n],0,&value);

				if(ret==0xFFFFFFFF)
					return 0;
				else
					return value;
			}
		}
		n++;
	}

	return 0;
}
*/
OS_API_C_FUNC(int) tree_manager_get_child_data_ptr		(const mem_zone_ref	*p_node_ref,unsigned int crc_name,mem_ptr *data_ptr)
{
	mem_zone_ref	 child_list={PTR_NULL};
	mem_zone_ref_ptr cur_child;

	(*data_ptr)=PTR_NULL;
	if(!tree_manager_get_first_child(p_node_ref,&child_list,&cur_child))return 0;
	if(!tree_manager_find_child_crc (&child_list,crc_name,&cur_child))return 0;
	

	(*data_ptr)					=tree_mamanger_get_node_data_ptr(cur_child,0);
	dec_zone_ref				(cur_child);
	release_zone_ref			(&child_list); 
	return 1;

}

OS_API_C_FUNC(int) tree_manager_get_child_value_i32		(const mem_zone_ref	*p_node_ref,unsigned int crc_name,unsigned int *value)
{
	int				ret;
	mem_zone_ref	 child_list={PTR_NULL};
	mem_zone_ref_ptr cur_child;

	if(!tree_manager_get_first_child(p_node_ref,&child_list,&cur_child))return 0;
	if(!tree_manager_find_child_crc (&child_list,crc_name,&cur_child))return 0;
	
	ret=tree_mamanger_get_node_dword(cur_child,0,value);
	dec_zone_ref				(cur_child);
	release_zone_ref			(&child_list); 
	return ret;

}


OS_API_C_FUNC(int) tree_manager_get_child_value_rect		(const mem_zone_ref	*p_node_ref,unsigned int crc_name,struct gfx_rect *rect)
{
	mem_zone_ref	 child_list={PTR_NULL};
	mem_zone_ref_ptr cur_child;

	if(!tree_manager_get_first_child(p_node_ref,&child_list,&cur_child))return 0;
	if(!tree_manager_find_child_crc (&child_list,crc_name,&cur_child))return 0;
	
	tree_mamanger_get_node_signed_dword		(cur_child,0	,&rect->pos[0]);
	tree_mamanger_get_node_signed_dword		(cur_child,4	,&rect->pos[1]);
	tree_mamanger_get_node_dword			(cur_child,8	,&rect->size[0]);
	tree_mamanger_get_node_dword			(cur_child,12	,&rect->size[1]);

	dec_zone_ref				(cur_child);
	release_zone_ref			(&child_list); 

	return 1;
}

OS_API_C_FUNC(int) tree_manager_get_child_value_4uc		(const mem_zone_ref	*p_node_ref,unsigned int crc_name,vec_4uc_t value)
{
		int				ret;
	mem_zone_ref	 child_list={PTR_NULL};
	mem_zone_ref_ptr cur_child;

	if(!tree_manager_get_first_child(p_node_ref,&child_list,&cur_child))return 0;
	if(!tree_manager_find_child_crc (&child_list,crc_name,&cur_child))return 0;
	
	ret=tree_manager_get_node_4uc(cur_child,0,value);
	dec_zone_ref				(cur_child);
	release_zone_ref			(&child_list); 
	return ret;

}




int tree_manager_get_child_value_i32_at		(const mem_zone_ref	*p_node_ref,unsigned int idx,unsigned int *value)
{
	size_t				zone_size;
	mem_zone_ref	*childs_ptr;


	if(p_node_ref==PTR_NULL)return 0;
	if(p_node_ref->zone==PTR_NULL)return 0;
	if(get_node(p_node_ref)->childs_ptr.zone==PTR_NULL)return 0;

	zone_size=get_zone_size(&get_node(p_node_ref)->childs_ptr);
	if(zone_size<=0)return 0;
	if(idx*sizeof(mem_zone_ref)>=zone_size)return 0;

	childs_ptr	=	get_zone_ptr(&get_node(p_node_ref)->childs_ptr,idx*sizeof(mem_zone_ref));
	return tree_mamanger_get_node_dword(childs_ptr,0,value);
}

OS_API_C_FUNC(int) tree_manager_get_child_type		(const mem_zone_ref	*p_node_ref,unsigned int crc_name)
{
	int				ret;
	mem_zone_ref	 child_list={PTR_NULL};
	mem_zone_ref_ptr cur_child;

	if(!tree_manager_get_first_child(p_node_ref,&child_list,&cur_child))return 0;
	if(!tree_manager_find_child_crc (&child_list,crc_name,&cur_child))return 0;
	
	ret=tree_mamanger_get_node_type(cur_child);
	dec_zone_ref				(cur_child);
	release_zone_ref			(&child_list); 
	return ret;

}



OS_API_C_FUNC(int) tree_manager_get_child_value_si32		(const mem_zone_ref	*p_node_ref,unsigned int crc_name,int *value)
{
	int				ret;
	mem_zone_ref	 child_list={PTR_NULL};
	mem_zone_ref_ptr cur_child;

	if(!tree_manager_get_first_child(p_node_ref,&child_list,&cur_child))return 0;
	if(!tree_manager_find_child_crc (&child_list,crc_name,&cur_child))return 0;
	
	ret=tree_mamanger_get_node_signed_dword(cur_child,0,value);
	dec_zone_ref				(cur_child);
	release_zone_ref			(&child_list); 
	return ret;


}





OS_API_C_FUNC(int) tree_manager_get_child_value_ptr		(mem_zone_ref_const_ptr	p_node_ref,unsigned int node_hash,unsigned int ofset,mem_ptr *value)
{
	mem_zone_ref	 child_list={PTR_NULL};
	mem_zone_ref_ptr cur_child;

	if(!tree_manager_get_first_child(p_node_ref,&child_list,&cur_child))return 0;
	if(!tree_manager_find_child_crc (&child_list,node_hash,&cur_child))return 0;
	
	tree_manager_get_node_ptr	(cur_child,ofset,value);
	dec_zone_ref				(cur_child);
	release_zone_ref			(&child_list); 
	return 1;

}



OS_API_C_FUNC(mem_ptr) tree_manager_get_child_data	(const mem_zone_ref	*p_node_ref,unsigned int crc_name,unsigned int ofset)
{

	mem_ptr				ret;
	mem_zone_ref	 child_list={PTR_NULL};
	mem_zone_ref_ptr cur_child;

	if(!tree_manager_get_first_child(p_node_ref,&child_list,&cur_child))return PTR_NULL;
	if(!tree_manager_find_child_crc (&child_list,crc_name,&cur_child))return PTR_NULL;
	
	ret=tree_mamanger_get_node_data_ptr	(cur_child,ofset);
	dec_zone_ref						(cur_child);
	release_zone_ref					(&child_list); 
	return ret;

}



OS_API_C_FUNC(int) tree_manager_allocate_child_data	(mem_zone_ref	*p_node_ref,char *name,unsigned int size)
{
	mem_zone_ref	 child_list={PTR_NULL};
	mem_zone_ref	 tree_node_ref={PTR_NULL};
	mem_zone_ref_ptr cur_child;
	unsigned int	 crc_name;
	int				 ret;
	
	if(p_node_ref==PTR_NULL)return 0;
	if(p_node_ref->zone==PTR_NULL)return 0;

	if(tree_manager_get_first_child(p_node_ref,&child_list,&cur_child))
	{
		crc_name			=	calc_crc32_c	(name,32);
		if(tree_manager_find_child_crc(&child_list,crc_name,&cur_child))
		{
			ret=tree_manager_allocate_node_data	(cur_child,size);
			dec_zone_ref						(cur_child);
			release_zone_ref					(&child_list); 
			return ret;
		}
	}
		
	if(!tree_manager_add_child_node		(p_node_ref,name,NODE_GFX_DATA,&tree_node_ref))
	{
		writestr("could not add child node \n");
		return 0;
	}
	ret=tree_manager_allocate_node_data	(&tree_node_ref,size);
	release_zone_ref				(&tree_node_ref); 
	return ret;
}



OS_API_C_FUNC(int) tree_manager_set_child_value_i32		(mem_zone_ref	*p_node_ref,const char *name,unsigned int value)
{
	mem_zone_ref	 child_list={PTR_NULL};
	mem_zone_ref	 tree_node_ref={PTR_NULL};
	mem_zone_ref_ptr cur_child;
	unsigned int	 crc_name;
	
	if(p_node_ref==PTR_NULL)return 0;
	if(p_node_ref->zone==PTR_NULL)return 0;

	if(tree_manager_get_first_child(p_node_ref,&child_list,&cur_child))
	{
		crc_name	=	NODE_HASH(name);
		if(tree_manager_find_child_crc(&child_list,crc_name,&cur_child))
		{
			tree_manager_write_node_dword	(cur_child,0,value);
			dec_zone_ref					(cur_child);
			release_zone_ref				(&child_list); 
			return 1;
		}
	}
		
	if(!tree_manager_add_child_node		(p_node_ref,name,NODE_GFX_INT,&tree_node_ref))
	{
		writestr("could not add child node \n");
		return 0;
	}

	tree_manager_write_node_dword	(&tree_node_ref,0,value);
	release_zone_ref				(&tree_node_ref); 

	return 1;



}
OS_API_C_FUNC(int) tree_manager_set_child_value_rect		(mem_zone_ref	*p_node_ref,const char *name,const struct gfx_rect *rect)
{
	mem_zone_ref	 child_list={PTR_NULL};
	mem_zone_ref	 tree_node_ref={PTR_NULL};
	mem_zone_ref_ptr cur_child;
	unsigned int	 crc_name;
	
	if(p_node_ref==PTR_NULL)return 0;
	if(p_node_ref->zone==PTR_NULL)return 0;
	if(rect==PTR_NULL)return 0;

	if(tree_manager_get_first_child(p_node_ref,&child_list,&cur_child))
	{
		crc_name			=	calc_crc32_c	(name,32);
		if(tree_manager_find_child_crc(&child_list,crc_name,&cur_child))
		{

			tree_manager_write_node_signed_dword	(cur_child,0	,rect->pos[0]);
			tree_manager_write_node_signed_dword	(cur_child,4	,rect->pos[1]);
			tree_manager_write_node_dword			(cur_child,8	,rect->size[0]);
			tree_manager_write_node_dword			(cur_child,12	,rect->size[1]);
			dec_zone_ref							(cur_child);
			release_zone_ref						(&child_list); 
			return 1;
		}
	}
		
	if(!tree_manager_add_child_node		(p_node_ref,name,NODE_GFX_INT,&tree_node_ref))
	{
		writestr("could not add child node \n");
		return 0;
	}

	tree_manager_write_node_signed_dword	(&tree_node_ref,0	,rect->pos[0]);
	tree_manager_write_node_signed_dword	(&tree_node_ref,4	,rect->pos[1]);
	tree_manager_write_node_dword			(&tree_node_ref,8	,rect->size[0]);
	tree_manager_write_node_dword			(&tree_node_ref,12	,rect->size[1]);
	release_zone_ref						(&tree_node_ref); 

	return 1;
}

OS_API_C_FUNC(int) tree_manager_set_child_value_4uc		(mem_zone_ref	*p_node_ref,const char *name,const vec_4uc_t value)
{
	mem_zone_ref	 child_list={PTR_NULL};
	mem_zone_ref	 tree_node_ref={PTR_NULL};
	mem_zone_ref_ptr cur_child;
	unsigned int	 crc_name;
	
	if(p_node_ref==PTR_NULL)return 0;
	if(p_node_ref->zone==PTR_NULL)return 0;

	if(tree_manager_get_first_child(p_node_ref,&child_list,&cur_child))
	{
		crc_name			=	calc_crc32_c	(name,32);
		if(tree_manager_find_child_crc(&child_list,crc_name,&cur_child))
		{
			tree_manager_write_node_4uc		(cur_child,0,value);
			dec_zone_ref					(cur_child);
			release_zone_ref				(&child_list); 
			return 1;
		}
	}
		
	if(!tree_manager_add_child_node		(p_node_ref,name,NODE_GFX_4UC,&tree_node_ref))
	{
		writestr("could not add child node \n");
		return 0;
	}

	tree_manager_write_node_4uc		(&tree_node_ref,0,value);
	release_zone_ref				(&tree_node_ref); 

	return 1;

}

OS_API_C_FUNC(int) tree_manager_set_child_value_i64		(mem_zone_ref_ptr	p_node_ref,char *name,uint64_t value)
{
	mem_zone_ref	 child_list={PTR_NULL};
	mem_zone_ref	 tree_node_ref={PTR_NULL};
	mem_zone_ref_ptr cur_child;
	unsigned int	 crc_name;
	
	if(p_node_ref==PTR_NULL)return 0;
	if(p_node_ref->zone==PTR_NULL)return 0;

	if(tree_manager_get_first_child(p_node_ref,&child_list,&cur_child))
	{
		crc_name			=	calc_crc32_c	(name,32);
		if(tree_manager_find_child_crc(&child_list,crc_name,&cur_child))
		{
			tree_manager_write_node_qword	(cur_child,0,value);
			dec_zone_ref					(cur_child);
			release_zone_ref				(&child_list); 
			return 1;
		}
	}
		
	if(!tree_manager_add_child_node		(p_node_ref,name,NODE_GFX_INT,&tree_node_ref))
	{
		writestr("could not add child node \n");
		return 0;
	}
	
	tree_manager_write_node_qword	(&tree_node_ref,0,value);
	release_zone_ref				(&tree_node_ref); 

	return 1;
}



OS_API_C_FUNC(int) tree_manager_set_child_value_si64		(mem_zone_ref_ptr p_node_ref,char *name,int64_t value)
{
	mem_zone_ref	 child_list={PTR_NULL};
	mem_zone_ref	 tree_node_ref={PTR_NULL};
	mem_zone_ref_ptr cur_child;
	unsigned int	 crc_name;
	
	if(p_node_ref==PTR_NULL)return 0;
	if(p_node_ref->zone==PTR_NULL)return 0;

	if(tree_manager_get_first_child(p_node_ref,&child_list,&cur_child))
	{
		crc_name			=	calc_crc32_c	(name,32);
		if(tree_manager_find_child_crc(&child_list,crc_name,&cur_child))
		{
			tree_manager_write_node_signed_qword(cur_child,0,value);
			dec_zone_ref						(cur_child);
			release_zone_ref					(&child_list); 
			return 1;
		}
	}
		
	if(!tree_manager_add_child_node		(p_node_ref,name,NODE_GFX_SIGNED_INT,&tree_node_ref))
	{
		writestr("could not add child node \n");
		return 0;
	}
	
	tree_manager_write_node_signed_qword	(&tree_node_ref,0,value);
	release_zone_ref						(&tree_node_ref); 
	
	return 1;
}



OS_API_C_FUNC(int) tree_manager_set_child_value_bool		(mem_zone_ref	*p_node_ref,const char *name,unsigned int value)
{

	mem_zone_ref	 child_list={PTR_NULL};
	mem_zone_ref	 tree_node_ref={PTR_NULL};
	mem_zone_ref_ptr cur_child;
	unsigned int	 crc_name;
	
	if(p_node_ref==PTR_NULL)return 0;
	if(p_node_ref->zone==PTR_NULL)return 0;

	if(tree_manager_get_first_child(p_node_ref,&child_list,&cur_child))
	{
		crc_name			=	calc_crc32_c	(name,32);
		if(tree_manager_find_child_crc(&child_list,crc_name,&cur_child))
		{
			tree_manager_write_node_dword	(cur_child,0,value);
			dec_zone_ref					(cur_child);
			release_zone_ref				(&child_list); 
			return 1;
		}
	}
		
	if(!tree_manager_add_child_node		(p_node_ref,name,NODE_GFX_BOOL,&tree_node_ref))
	{
		writestr("could not add child node \n");
		return 0;
	}
	
	tree_manager_write_node_dword	(&tree_node_ref,0,value);
	release_zone_ref				(&tree_node_ref); 

	return 1;
}



OS_API_C_FUNC(int) tree_manager_set_child_value_si32		(mem_zone_ref	*p_node_ref,const char *name,int value)
{
	mem_zone_ref	 child_list={PTR_NULL};
	mem_zone_ref	 tree_node_ref={PTR_NULL};
	mem_zone_ref_ptr cur_child;
	unsigned int	 crc_name;
	
	if(p_node_ref==PTR_NULL)return 0;
	if(p_node_ref->zone==PTR_NULL)return 0;

	if(tree_manager_get_first_child(p_node_ref,&child_list,&cur_child))
	{
		crc_name			=	calc_crc32_c	(name,32);
		if(tree_manager_find_child_crc(&child_list,crc_name,&cur_child))
		{
			tree_manager_write_node_signed_dword	(cur_child,0,value);
			dec_zone_ref					(cur_child);
			release_zone_ref				(&child_list); 
			return 1;
		}
	}
		
	if(!tree_manager_add_child_node		(p_node_ref,name,NODE_GFX_SIGNED_INT,&tree_node_ref))
	{
		writestr("could not add child node \n");
		return 0;
	}
	
	tree_manager_write_node_signed_dword	(&tree_node_ref,0,value);
	release_zone_ref				(&tree_node_ref); 
	return 1;
	
}

OS_API_C_FUNC(int) tree_manager_set_child_value_str	(mem_zone_ref_ptr p_node_ref,const char *name,const char *str)
{
	mem_zone_ref	 child_list={PTR_NULL};
	mem_zone_ref	 tree_node_ref={PTR_NULL};
	mem_zone_ref_ptr cur_child;
	unsigned int	 crc_name;
	
	if(p_node_ref==PTR_NULL)return 0;
	if(p_node_ref->zone==PTR_NULL)return 0;

	if(tree_manager_get_first_child(p_node_ref,&child_list,&cur_child))
	{
		crc_name			=	calc_crc32_c	(name,32);
		if(tree_manager_find_child_crc(&child_list,crc_name,&cur_child))
		{
			tree_manager_write_node_str		(cur_child,0,str);
			dec_zone_ref					(cur_child);
			release_zone_ref				(&child_list); 
			return 1;
		}
	}
		
	if(!tree_manager_add_child_node		(p_node_ref,name,NODE_GFX_STR,&tree_node_ref))
	{
		writestr("could not add child node \n");
		return 0;
	}
	
	tree_manager_write_node_str		(&tree_node_ref,0,str);
	release_zone_ref				(&tree_node_ref); 

	return 1;

}

OS_API_C_FUNC(int) tree_manager_set_child_value_ptr		(mem_zone_ref	*p_node_ref,const char *name,mem_ptr value)
{

	mem_zone_ref	 child_list={PTR_NULL};
	mem_zone_ref	 tree_node_ref={PTR_NULL};
	mem_zone_ref_ptr cur_child;
	unsigned int	 crc_name;
	
	if(p_node_ref==PTR_NULL)return 0;
	if(p_node_ref->zone==PTR_NULL)return 0;

	if(tree_manager_get_first_child(p_node_ref,&child_list,&cur_child))
	{
		crc_name			=	calc_crc32_c	(name,32);
		if(tree_manager_find_child_crc(&child_list,crc_name,&cur_child))
		{
			tree_manager_write_node_ptr		(cur_child,0,value);
			dec_zone_ref					(cur_child);
			release_zone_ref				(&child_list); 
			return 1;
		}
	}
		
	if(!tree_manager_add_child_node		(p_node_ref,name,NODE_GFX_PTR,&tree_node_ref))
	{
		writestr("could not add child node \n");
		return 0;
	}

	tree_manager_write_node_ptr		(&tree_node_ref,0,value);
	release_zone_ref				(&tree_node_ref); 

	return 1;

}




OS_API_C_FUNC(void) tree_manager_dump_node_rec		(mem_zone_ref_const_ptr node_ref,unsigned int rec_level,unsigned int max_rec)
{	
	mem_zone_ref		*childs_ptr=PTR_NULL;
	mem_zone_ref		child_list={PTR_NULL};
	mem_zone_ref_ptr	cur_child;
	unsigned int		n;
	size_t				node_size;
	unsigned int		node_val;

	unsigned int		size[2];
	int					pos[2];

	int					s_node_val;
	unsigned char		node_val_char;
	char				*string;

	if(node_ref==PTR_NULL)return;
	if(node_ref->zone==PTR_NULL){writestr("node zone null \n");return;}
	if(rec_level>=max_rec)return;
	if(get_node(node_ref)==PTR_INVALID)
	{
		n=0;
		while(n<rec_level)
		{
			writestr("  ");
			n++;
		}
		writestr("|-->");


		writestr("ERROR ");
		writeptr(node_ref->zone);
		writestr("\n");
		return;
	}	
	if(rec_level>0)
	{
		n=0;
		while(n<rec_level)
		{
			writestr("  ");
			n++;
		}
		writestr("|-->");
	}

	


	//writeptr(get_node(node_ref));
	
	writestr("p ");
	writeptr(get_node(node_ref)->parent_zone);
	writestr(" '");
	writestr(get_node(node_ref)->name);
	writestr("' [");
	writeint(get_node(node_ref)->node_type,16);
	writestr("] size ");
	if((node_size=tree_manager_get_node_image_size(node_ref))>0)
	{
		writesz(node_size,10);
	}
	else
	{
		writesz(tree_mamanger_get_node_data_size(node_ref),10);
	}
	
	switch(get_node(node_ref)->node_type)
	{
		case NODE_GFX_RECT:
			tree_mamanger_get_node_signed_dword	(node_ref,0,&pos[0]);
			tree_mamanger_get_node_signed_dword	(node_ref,4,&pos[1]);
			tree_mamanger_get_node_dword		(node_ref,8,&size[0]);
			tree_mamanger_get_node_dword		(node_ref,12,&size[1]);
			writestr_fmt(" val=[%d,%d,%d,%d] ",pos[0],pos[1],size[0],size[1]);
		break;
		case NODE_GFX_INT:
	
			
			tree_mamanger_get_node_dword(node_ref,0,&node_val);
			writestr(" val=[");
			writeint(node_val,16);
			writestr("] ");
		break;
		case NODE_GFX_SIGNED_INT:
	
			
			tree_mamanger_get_node_signed_dword(node_ref,0,&s_node_val);
			writestr(" val=[");
			writeint(s_node_val,10);
			writestr("] ");
		break;
		case NODE_GFX_STR:
			
			string			=	tree_mamanger_get_node_data_ptr(node_ref,0);
			writestr(" val='");

			if(string==PTR_NULL)
				writestr("null");
			else
				writestr(string);
			writestr("' ");
		break;
	
		case NODE_GFX_BOOL:
			tree_mamanger_get_node_byte(node_ref,0,&node_val_char);
			writestr(" val=[");
			writeint(node_val_char,10);
			writestr("] ");
		break;
	}

	writestr("\n");

	if(!tree_manager_get_first_child(node_ref,&child_list,&cur_child))return;

	if((rec_level+1)<max_rec)
		writestr_fmt("child list ptr %p %p\n",child_list.zone,cur_child);

	while(cur_child->zone!=PTR_NULL)
	{
		tree_manager_dump_node_rec(cur_child,rec_level+1,max_rec);
		if(!tree_manager_get_next_child(&child_list,&cur_child))break;
	}
}



int tree_manager_json_loadb(const char *buffer, size_t buflen,mem_zone_ref_ptr result)
{
    lex_t			lex;
	int				ret;
    buffer_data_t	stream_data;

    if (buffer == NULL)return 0;
	if (buflen < 2)return 0;



    stream_data.data = buffer;
    stream_data.pos  = 0;
    stream_data.len  = buflen+1;

    if(lex_init(&lex, (void *)&stream_data))
        return 0;

    lex_scan(&lex);
    if(lex.token != '[' && lex.token != '{')return 0;

    ret=parse_value(&lex,result);


    lex_close(&lex);
    return ret;
}







