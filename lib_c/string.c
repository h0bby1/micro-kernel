#define LIBC_API C_EXPORT
#include <std_def.h>
#include <std_mem.h>
#include "kern.h"
#include "mem_base.h"
#include "lib_c.h"
#include "sys/task.h"


typedef struct
{
	unsigned short	id;
	char			prefix[16];
	unsigned char	color;

}kernel_str_log_t;




mem_zone_ref		kernel_logs_ref	=	{PTR_INVALID};
unsigned int		num_kern_logs	=	0xFFFFFFFF;
mem_ptr				write_sema		=	{PTR_INVALID};





#define MAX_TEXT_BUFFER_LINES 1024
#define MAX_LINE_CHAR 1024


void init_kernel_log()
{
	num_kern_logs				=	0;
	kernel_logs_ref.zone		=	PTR_NULL;
	allocate_new_zone			(0,sizeof(kernel_str_log_t)*128,&kernel_logs_ref);

	
}



OS_API_C_FUNC(char * ) itoa_s(int value, char *string,size_t len, int radix)
{
	char		tmp[33];
	char		*tp	= tmp;
	int			i;
	unsigned	v;
	int			sign;
	char		*sp;

	if (radix > 36 || radix <= 1)return 0;
	sign = (radix == 10 && value < 0);
	if (sign)
		v = -value;
	else
		v = (unsigned)value;

	while (v || tp == tmp)
	{
		i = v % radix;
		v = v / radix;
		if (i < 10)
		  *tp++ = i+'0';
		else
		  *tp++ = i + 'a' - 10;
	}

	sp = string;

	if (sign)
		*sp++ = '-';
	while (tp > tmp)
		*sp++ = *--tp;
	*sp = 0;
	return string;
}

OS_API_C_FUNC(char * ) uitoa_s(size_t value, char *string,size_t len, int radix)
{
	char		tmp[33];
	char		*tp	= tmp;
	int			i;
	unsigned	v;
	char		*sp;

	if (radix > 36 || radix <= 1)return 0;
	
	v = (unsigned int)value;

	while (v || tp == tmp)
	{
		i = v % radix;
		v = v / radix;
		if (i < 10)
		  *tp++ = i+'0';
		else
		  *tp++ = i + 'a' - 10;
	}

	sp = string;

	while (tp > tmp)
		*sp++ = *--tp;
	*sp = 0;
	return string;
}

OS_API_C_FUNC(char *)strncpy_c(char *string,const char *src_string,size_t		 cnt)
{
	unsigned int n;
	char src;
	
	n			=0;
	while(((src=src_string[n])!=0))
	{
		if(n>=cnt)break;
		string[n]=src;
		n++;
	}
	string[n]=0;

	return string;
}


OS_API_C_FUNC(char *)strncpy_s(char *string,size_t str_len,const char *src_string,size_t cnt)
{
	unsigned int n;
	char src;
	
	n			=0;
	while(((src=src_string[n])!=0))
	{
		if((n+1)>=str_len)break;
		if(n>=cnt)break;
		string[n]=src;
		n++;
	}
	string[n]=0;

	return string;
}

OS_API_C_FUNC(char *)strcpy_c(char *string,const char *src_string)
{
	unsigned int n;
	char src;
	
	n			=0;
	while(((src=src_string[n])!=0))
	{
		string[n]=src;
		n++;
	}
	string[n]=0;

	return string;
}


OS_API_C_FUNC(char *)strcpy(char *string,const char *src_string)
{
	unsigned int n;
	char src;
	
	n			=0;
	while(((src=src_string[n])!=0))
	{
		string[n]=src;
		n++;
	}
	string[n]=0;

	return string;
}

OS_API_C_FUNC(int)  strcpy_s(char *string,size_t size,const char *src_string)
{
	unsigned int n;
	char src;
	if(src_string==PTR_NULL)return -1;
	if(string==PTR_NULL)return -1;
	if(size==0)return 0;
	n			=0;
	while(((src=src_string[n])!=0))
	{
		if((n+1)>=size)break;
		string[n]=src;
		n++;
	}
	string[n]=0;

	return 0;
}

OS_API_C_FUNC(int)  strcat_s(char *string,size_t size,const char *src_string)
{
	unsigned int n;
	unsigned int dst_len;
	char src;
	
	if(src_string==PTR_NULL)return -1;
	if(string==PTR_NULL)return -1;
	if(size==0)return 0;

	dst_len=0;
	while(string[dst_len]!=0){ dst_len++; }

	n			=0;
	while(((src=src_string[n])!=0))
	{
		if((dst_len+n+1)>=size)break;
		string[dst_len+n]=src;
		n++;
	}
	string[dst_len+n]=0;

	return 0;
}


OS_API_C_FUNC(int)  strcat_escaped_s(char *string,size_t size,const char *src_string)
{
	unsigned int src_n,dst_n,dst_len;
	char src;
	if(src_string==PTR_NULL)return -1;
	if(string==PTR_NULL)return -1;
	if(size==0)return 0;

	dst_len=0;
	while(string[dst_len]!=0){ dst_len++; }

	src_n	=0;
	dst_n	=dst_len;
	while(((src=src_string[src_n])!=0))
	{
		

		if(isalpha_c(src)||isdigit_c(src)||(src=='_'))
		{
			if((dst_n+1)>=size)break;
			string[dst_n]=src;
			dst_n++;
		}
		else
		{
			if((dst_n+3)>=size)break;
			string[dst_n]  ='\\';
			string[dst_n+1]=hex_chars[(src&0xF0)>>4];
			string[dst_n+2]=hex_chars[(src&0xF)];
			dst_n+=3;
		}
		src_n++;
	}
	string[dst_n]=0;

	return 0;
}


int  strcat_strval_s(char *string,size_t size,const char *name,const char *val)
{
	strcat_s			(string,size,name);
	strcat_s			(string,size,":\"");
	strcat_escaped_s	(string,size,val);
	strcat_s			(string,size,"\"");

	return 1;
}

OS_API_C_FUNC(int)  strcat_intval_s(char *string,size_t size,const char *name,int val,int radix)
{
	size_t l;
	strcat_s	(string,size,name);
	strcat_s	(string,size,":");
	if(radix==16)
		strcat_s	(string,size,"0x");

	l=strlen_c	(string);
	itoa_s		(val,&string[l],size-l,radix);

	return 1;
}

OS_API_C_FUNC(int)  strcat_uintval_s(char *string,size_t size,const char *name,unsigned int val,int radix)
{
	size_t l;
	strcat_s	(string,size,name);
	strcat_s	(string,size,":");
	if(radix==16)
		strcat_s	(string,size,"0x");

	l=strlen_c	(string);
	uitoa_s		(val,&string[l],size-l,radix);

	return 1;
}

OS_API_C_FUNC(int)  strcat_c(char *string,const char *src_string)
{
	unsigned int n;
	char src;
	char dst_len;
	if(src_string==PTR_NULL)return -1;
	if(string==PTR_NULL)return -1;

	dst_len=0;
	while(string[dst_len]!=0){ dst_len++; }

	n			=0;
	while(((src=src_string[n])!=0))
	{
		string[dst_len+n]=src;
		n++;
	}
	string[dst_len+n]=0;

	return 0;
}

OS_API_C_FUNC(int)  strncat_c(char *string,const char *src_string,size_t max)
{
	size_t n;
	char src;
	size_t dst_len;
	if(src_string==PTR_NULL)return -1;
	if(string==PTR_NULL)return -1;

	dst_len=0;
	while(string[dst_len]!=0){ dst_len++; }

	n			=0;
	while(((src=src_string[n])!=0)&&(n<max))
	{
		string[dst_len+n]=src;
		n++;
	}
	string[dst_len+n]=0;

	return 0;
}

 OS_API_C_FUNC(int)  strcmp_c(const char *string1,const char *string2)
{
	int		n;
	char	c1=0xFF,c2=0xFF;

	n=0;
	while((c1!=0)&&(c2!=0))
	{
		c1=string1[n];
		c2=string2[n];
		if(c1<c2)return -1;
		if(c1>c2)return 1;
		n++;
	}
	return 0;
}


 OS_API_C_FUNC(int)  strncmp_c(const char *string1,const char *string2,size_t len)
{
	unsigned int		n;
	char	c1=0xFF,c2=0xFF;

	n=0;
	while((c1!=0)&&(c2!=0))
	{
		if(n>=len)return 0;
		c1=string1[n];
		c2=string2[n];
		if(c1<c2)return -1;
		if(c1>c2)return 1;
		n++;
	}
	return 0;
}


OS_API_C_FUNC(size_t) strlen_c(const char *string)
{
	int n=0;
	
	while(string[n]!=0){n++;}

	return n;
}

OS_API_C_FUNC(char) first_char(const char *str)
{
	while((*str)!=0)
	{
		if((*str)!=' ')
			return (*str);

		str++;
	}

	return (*str);
}



OS_API_C_FUNC(size_t) strlpos_c(const char *string,size_t ofset,char c)
{
	size_t		n;
	char		sc;

	n=ofset;

	while((sc=string[n])!=0)
	{ 
		if(sc==c)return n;
		n++;
	}
	return INVALID_SIZE;

}
OS_API_C_FUNC(const char *) strrchr_c		(const char *string,int c)
{
	size_t		n;
	
	n	=	strlen_c(string);
	while(n>0)
	{ 
		if(string[n]==c)return &string[n];

		n--;
	}

	return PTR_NULL;


}

OS_API_C_FUNC(size_t) strrpos_c(const char *string,char c)
{
	size_t		n;
	n=strlen_c(string);
	while(n>0)
	{ 
		if(string[n]==c)return (n+1);

		n--;
	}

	return 0;

}


OS_API_C_FUNC(const char *) strstr_c(const char *buf, const char *sub)
{
    const char *bp;
    const char *sp;

    if (!*sub)
	return buf;
    while (*buf) {
	bp = buf;
	sp = sub;
	do {
	    if (!*sp)
		return buf;
	} while (*bp++ == *sp++);
	buf += 1;
    }
	return PTR_NULL;
}





OS_API_C_FUNC(int) isupper_c(int c)
{
    return (c >= 'A' && c <= 'Z');
}

OS_API_C_FUNC(int) islower_c(int c)
{
    return (c >= 'a' && c <= 'z');
}


OS_API_C_FUNC(int) isalpha_c(int c)
{
    return ((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z'));
}


OS_API_C_FUNC(int) isspace_c(int c)
{
    return (c == ' ' || c == '\t' || c == '\n' || c == '\12');
}

OS_API_C_FUNC(int) isdigit_c(int c)
{
    return (c >= '0' && c <= '9');
}
OS_API_C_FUNC(int) isxdigit_c(int c)
{

	  return ((c >= '0' && c <= '9')||(c >= 'A' && c <= 'F'));
}

OS_API_C_FUNC(int) toupper_c(int c)
{

	if (islower_c(c)) return(c - ('a'-'A'));
	return(c);
}




/*
 * Convert a string to a long integer.
 *
 * Ignores `locale' stuff.  Assumes that the upper and lower case
 * alphabets and digits are each contiguous.
 */
OS_API_C_FUNC(unsigned long) strtoul_c(const char *nptr, char **endptr, int base)
{
	register const char *s = nptr;
	register unsigned long acc;
	register unsigned long cutoff;
	register int c;
	register int any, cutlim;

	/*
	 * Skip white space and pick up leading +/- sign if any.
	 * If base is 0, allow 0x for hex and 0 for octal, else
	 * assume decimal; if base is already 16, allow 0x.
	 */
	do {
		c = *s++;
	} while (isspace_c(c));
	

	if ((base == 0 || base == 16) && c == '0' && (*s == 'x' || *s == 'X')) {
		c = s[1];
		s += 2;
		base = 16;
	} else if ((base == 0 || base == 2) &&  c == '0' && (*s == 'b' || *s == 'B')) {
		c = s[1];
		s += 2;
		base = 2;
	}
	if (base == 0)
		base = c == '0' ? 8 : 10;

	/*
	 * Compute the cutoff value between legal numbers and illegal
	 * numbers.  That is the largest legal value, divided by the
	 * base.  An input number that is greater than this value, if
	 * followed by a legal input character, is too big.  One that
	 * is equal to this value may be valid or not; the limit
	 * between valid and invalid numbers is then based on the last
	 * digit.  For instance, if the range for longs is
	 * [-2147483648..2147483647] and the input base is 10,
	 * cutoff will be set to 214748364 and cutlim to either
	 * 7 (neg==0) or 8 (neg==1), meaning that if we have accumulated
	 * a value > 214748364, or equal but the next digit is > 7 (or 8),
	 * the number is too big, and we will return a range error.
	 *
	 * Set any if any `digits' consumed; make it negative to indicate
	 * overflow.
	 */
	cutoff = ULONG_MAX;
	cutlim = cutoff % (unsigned long)base;
	cutoff /= (unsigned long)base;
	for (acc = 0, any = 0;; c = *s++) {
		if (isdigit_c(c))
			c -= '0';
		else if (isalpha_c(c))
			c -= isupper_c(c) ? 'A' - 10 : 'a' - 10;
		else
			break;
		if (c >= base)
			break;


		if (any < 0 || acc > cutoff || acc == cutoff && c > cutlim)
			any = -1;
		else {
			any = 1;
			acc *= base;
			acc += c;
		}
	}
	if (any < 0) {
		acc = ULONG_MAX;
//		errno = ERANGE;
	}
	if (endptr != 0)
		*endptr = (char *)(any ? s - 1 : nptr);
	return (acc);
}

/*
 * Convert a string to a long integer.
 *
 * Ignores `locale' stuff.  Assumes that the upper and lower case
 * alphabets and digits are each contiguous.
 */
OS_API_C_FUNC(long) strtol_c(const char *nptr, char **endptr, int base)
{
	register const char *s = nptr;
	register unsigned long acc;
	register int c;
	register unsigned long cutoff;
	register int neg = 0, any, cutlim;

	/*
	 * Skip white space and pick up leading +/- sign if any.
	 * If base is 0, allow 0x for hex and 0 for octal, else
	 * assume decimal; if base is already 16, allow 0x.
	 */
	do {
		c = *s++;
	} while (isspace_c(c));
	if (c == '-') {
		neg = 1;
		c = *s++;
	} else if (c == '+')
		c = *s++;
	if ((base == 0 || base == 16) &&
	    c == '0' && (*s == 'x' || *s == 'X')) {
		c = s[1];
		s += 2;
		base = 16;
	} else if ((base == 0 || base == 2) &&
	    c == '0' && (*s == 'b' || *s == 'B')) {
		c = s[1];
		s += 2;
		base = 2;
	}
	if (base == 0)
		base = c == '0' ? 8 : 10;

	/*
	 * Compute the cutoff value between legal numbers and illegal
	 * numbers.  That is the largest legal value, divided by the
	 * base.  An input number that is greater than this value, if
	 * followed by a legal input character, is too big.  One that
	 * is equal to this value may be valid or not; the limit
	 * between valid and invalid numbers is then based on the last
	 * digit.  For instance, if the range for longs is
	 * [-2147483648..2147483647] and the input base is 10,
	 * cutoff will be set to 214748364 and cutlim to either
	 * 7 (neg==0) or 8 (neg==1), meaning that if we have accumulated
	 * a value > 214748364, or equal but the next digit is > 7 (or 8),
	 * the number is too big, and we will return a range error.
	 *
	 * Set any if any `digits' consumed; make it negative to indicate
	 * overflow.
	 */
	cutoff = neg ? -(unsigned long)LONG_MIN : LONG_MAX;
	cutlim = cutoff % (unsigned long)base;
	cutoff /= (unsigned long)base;
	for (acc = 0, any = 0;; c = *s++) {
		if (isdigit_c(c))
			c -= '0';
		else if (isalpha_c(c))
			c -= isupper_c(c) ? 'A' - 10 : 'a' - 10;
		else
			break;
		if (c >= base)
			break;
		if (any < 0 || acc > cutoff || acc == cutoff && c > cutlim)
			any = -1;
		else {
			any = 1;
			acc *= base;
			acc += c;
		}
	}
	if (any < 0) {
		acc = neg ? LONG_MIN : LONG_MAX;
//		errno = ERANGE;
	} else if (neg)
		acc = -acc;
	if (endptr != 0)
		*endptr = (char *)(any ? s - 1 : nptr);
	return (acc);
}


OS_API_C_FUNC(short) strtos_c(const char *nptr, char **endptr, int base)
{
	unsigned int ret;
	
	ret	=	strtol_c(nptr,endptr,base);

	return ((short)(ret));
}
OS_API_C_FUNC(long)		atol_c			(const char *str )
{
	return strtol_c(str,PTR_NULL,10);
}


OS_API_C_FUNC(unsigned int)	get_new_kern_log_id(const char *pref,unsigned char color)
{
	kernel_str_log_t    *new_kern_log;
	kernel_str_log_t    *kern_log,*kern_end;

	kern_log		=	get_zone_ptr(&kernel_logs_ref,0);
	if(kern_log	== PTR_NULL)return 0;
	kern_end		=	get_zone_ptr(&kernel_logs_ref,sizeof(kernel_str_log_t)*num_kern_logs);

	while(kern_log<kern_end)
	{
		if( (kern_log->color==color)&&
			(!strcmp_c(kern_log->prefix,pref)))
		{
			return kern_log->id;
		}

		kern_log++;
	}

	new_kern_log		=	get_zone_ptr(&kernel_logs_ref,sizeof(kernel_str_log_t)*num_kern_logs);
	new_kern_log->id	=	(num_kern_logs+1);
	new_kern_log->color	=	color;
	strcpy_s				(new_kern_log->prefix,16,pref);
	num_kern_logs++;
	
	return new_kern_log->id;
}


OS_API_C_FUNC(kernel_str_log_t    *) get_kern_log(unsigned int id)
{
	kernel_str_log_t    *kern_log,*kern_end;

	if(id==0)return PTR_NULL;
	if(id==0xFFFFFFFF)return PTR_NULL;

	kern_log		=	get_zone_ptr(&kernel_logs_ref,0);

	if(kern_log	== PTR_NULL)return PTR_NULL;

	kern_end		=	get_zone_ptr(&kernel_logs_ref,sizeof(kernel_str_log_t)*num_kern_logs);

	while(kern_log<kern_end)
	{
		if(kern_log->id==id)
			return kern_log;

		kern_log++;
	}

	return PTR_NULL;
}


OS_API_C_FUNC(void) kernel_log(unsigned int kern_log_id,const char *str)
{
	kernel_str_log_t    *kern_log;
	unsigned char		original_color;

	original_color	=	get_text_color_c();
	kern_log		=	get_kern_log	(kern_log_id);

	task_manager_aquire_semaphore(write_sema,0);

	if(kern_log!=PTR_NULL)
	{
		set_text_color_c					(kern_log->color);
		draw_car_c							(kern_log->prefix);
	}
	set_text_color_c					(original_color);
	draw_car_c							(str);

	if(strlpos_c(str,0,'\n')!=INVALID_SIZE)
		task_manager_release_semaphore		(write_sema,1);

	
	
}
/*
OS_API_C_FUNC(int)  writestr_color(const char *text,unsigned char color)
{
	unsigned char original_color;

	original_color	=	get_text_color_c();

	task_manager_aquire_semaphore		(write_sema,0);

	set_text_color_c					(color);
	draw_car_c							(text);
	set_text_color_c					(original_color);

	if(strlpos_c(text,0,'\n')!=INVALID_SIZE)
		task_manager_release_semaphore		(write_sema,1);	

	return 0;

}
*/

OS_API_C_FUNC(int) writenstr(const char *text,size_t n)
{
	task_manager_aquire_semaphore		(write_sema,0);

	draw_ncar_c	(text,(unsigned int)n);
	
	if(strlpos_c(text,0,'\n')!=INVALID_SIZE)
		task_manager_release_semaphore		(write_sema,1);
	
	return 0;
}

OS_API_C_FUNC(int)  writestr(const char *text)
{
	task_manager_aquire_semaphore		(write_sema,0);

	draw_car_c	(text);
	
	if(strlpos_c(text,0,'\n')!=INVALID_SIZE)
		task_manager_release_semaphore		(write_sema,1);
	
	return 0;
}





OS_API_C_FUNC(void)  writestr_vfmt(const char *fmt,va_list Args)
{
	
	const char					*fmt_ptr;
	const char					*last_fmt_ptr;
	const char					*end_fmt_ptr;

	fmt_ptr		=fmt;
	last_fmt_ptr=fmt_ptr;
	end_fmt_ptr	=mem_add(fmt,strlen_c(fmt));

	task_manager_aquire_semaphore		(write_sema,0);
	
    
	while((*fmt_ptr)!=0)
	{
		if((*fmt_ptr)=='%')
		{
			char		itoa_buff[64];
			char		zero_buff[16]={'0'};
			int			d1,d2,t;
			int			sign;
			size_t		len;


			if(last_fmt_ptr!=fmt_ptr)
			{
				len			=	mem_sub (last_fmt_ptr,fmt_ptr);
				draw_ncar_c				(last_fmt_ptr,(unsigned int)len);
			}
			
			fmt_ptr++;
			
			 d1=0;
			 d2=0;
			 sign=0;

			if((*fmt_ptr)=='-')
			{
				sign=1;
				fmt_ptr++;
			}


			while(isdigit_c	((*fmt_ptr)))
			{
				d1			=   (*fmt_ptr)-48;
				fmt_ptr++;
			}

			if((*fmt_ptr)=='.')
				fmt_ptr++;

			while(isdigit_c				((*fmt_ptr)))
			{
				d2			=   (*fmt_ptr)-48;
				fmt_ptr++;
			}


			if((*fmt_ptr)=='l')
			{
				fmt_ptr				++;
				
			}
			t					=	(*fmt_ptr);
			fmt_ptr				++;
			
	

			if((toupper_c(t)=='D')||(toupper_c(t)=='U'))
			{
				memset_c	(itoa_buff,0,64);
				itoa_s		(va_arg(Args,unsigned int),itoa_buff,64,10);

				if((d1==0)&&(d2==0))
					draw_car_c	(itoa_buff);
				else
				{
					size_t		 len;
					unsigned int l;
					len=strlen_c(itoa_buff);

					l=d1>0?d1:d2;

					if(len<l)
						draw_ncar_c	(zero_buff,l-len);
						
					draw_car_c	(itoa_buff);
				}
			}

			if(toupper_c(t)=='X')
			{
				
				memset_c	(itoa_buff,0,64);
				itoa_s		(va_arg(Args,unsigned int),itoa_buff,64,16);

				if((d1==0)&&(d2==0))
					draw_car_c	(itoa_buff);
				else
				{
					size_t len;
					unsigned int l;
					len=strlen_c(itoa_buff);

					l=d1>0?d1:d2;

					if(len<l)
						draw_ncar_c	(zero_buff,l-len);
						
					draw_car_c	(itoa_buff);
				}
			}



			if(t=='p')
			{
				writeptr(va_arg(Args,mem_ptr));
			}

			if(t=='s')
			{
				char *str;

				str=va_arg(Args,char *);

				if((d1==0)&&(d2==0))
					draw_car_c	(str);
				else
					draw_ncar_c	(str,d1);

			}	
			last_fmt_ptr		=	fmt_ptr;
		}

		fmt_ptr++;
	}


	if(last_fmt_ptr<end_fmt_ptr)
		draw_car_c				(last_fmt_ptr);

	if(strlpos_c(fmt,0,'\n')!=INVALID_SIZE)
		task_manager_release_semaphore		(write_sema,1);


    
}

OS_API_C_FUNC(void)  writestr_fmt(const char *fmt,...)
{
    va_list                 Args;
    va_start				(Args, fmt);

	writestr_vfmt			(fmt,Args);

	va_end					(Args);
}

OS_API_C_FUNC(void)  writesz(size_t val,unsigned int radix)
{
	char		itoa_buff[64];	

	memset_c	(itoa_buff,0,64);
	uitoa_s		(val,itoa_buff,64,radix);

	task_manager_aquire_semaphore		(write_sema,0);

	if(radix==16)
		draw_car_c	("0x");

	draw_car_c	(itoa_buff);
	
}
OS_API_C_FUNC(void)  writeint(int val,unsigned int radix)
{
	char		itoa_buff[64];	

	memset_c	(itoa_buff,0,64);
	itoa_s		(val,itoa_buff,64,radix);

	task_manager_aquire_semaphore		(write_sema,0);

	if(radix==16)
		draw_car_c	("0x");

	draw_car_c	(itoa_buff);
	
}

 OS_API_C_FUNC(void) writeptr(const_mem_ptr val)
{
	char		itoa_buff[64];	
	

	memset_c	(itoa_buff,0,64);
	itoa_s		(mem_to_uint(val),itoa_buff,64,16);

	task_manager_aquire_semaphore		(write_sema,0);


	draw_car_c	("@0x");
	draw_car_c	(itoa_buff);
	
}








OS_API_C_FUNC(unsigned int) write_bits	 (unsigned int data_orig,unsigned int data_bits,unsigned int ofset,unsigned int n_bits)
{
	unsigned int dest_data_mask;
	unsigned int bit_mask;
	unsigned int data_ret;
	unsigned int data_sf;



	bit_mask		 =	 ~(0xFFFFFFFF << n_bits);
	dest_data_mask	 =	 ~(bit_mask << ofset);
	data_ret		 =	 data_orig&dest_data_mask;
	data_sf			 =	 ((data_bits&bit_mask)<<ofset); 
	data_ret		|=	 (data_sf);
	return data_ret;
}


OS_API_C_FUNC(unsigned char ) write_bits_8	 (unsigned char data_orig,unsigned char data_bits,unsigned char ofset,unsigned char n_bits)
{
	unsigned char dest_data_mask;
	unsigned char bit_mask;
	unsigned char data_ret;
	unsigned char data_sf;

	bit_mask		 =	 ~(0xFF << n_bits);
	dest_data_mask	 =	 ~(bit_mask << ofset);
	data_ret		 =	 data_orig&dest_data_mask;
	data_sf			 =	 ((data_bits&bit_mask)<<ofset); 
	data_ret		|=	 (data_sf);
	return data_ret;
}

OS_API_C_FUNC(unsigned int) set_bit	 (unsigned int data_orig,unsigned int value,unsigned int ofset)
{
	unsigned int dest_data_mask;
	unsigned int data_ret;

	dest_data_mask	 =	 ~(0x01 << ofset);
	data_ret		 =	 data_orig&dest_data_mask;
	value			 =	 value&0x01;
	data_ret		|=	 (value<<ofset);

	return data_ret;
}



OS_API_C_FUNC(int) tolower_c(int _c)
{
	return 1;
}


OS_API_C_FUNC(int) isprint_c(int _C)
{
	return 1;
}




unsigned int	errno=0xFFFFFFFF;