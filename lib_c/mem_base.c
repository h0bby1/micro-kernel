#define LIBC_API C_EXPORT
#include <std_def.h>
#include <std_mem.h>
#include <kern.h>
#include <mem_base.h>
#include <lib_c.h>

#include <sys/task.h>

#include <tree.h>

#include "strbuffer.h"



char mem_area_list_columns[]=
"id:area_id{width:18,bk_color:0xFF00FF00}, \
type:type_str{width:48,bk_color:0xFF00FF00}, \
start addr:start{width:64,bk_color:0xFF00FF00}, \
end addr:end{width:64,bk_color:0xFF00FF00}, \
used:used{width:48,bk_color:0xFF00FF00,int_base:10}, \
zones:n_zones{width:48,bk_color:0xFF00FF00,int_base:10}";

char mem_area_zone_list_columns[]=
"zone:zone{width:64,bk_color:0xFF00FF00}, \
 addr:addr{width:64,bk_color:0xFF00FF00}, \
 size:size{width:28,bk_color:0xFF00FF00,int_base:10}, \
 refs:refs{width:18,bk_color:0xFF00FF00,int_base:10}, \
 time:time{width:28,bk_color:0xFF00FF00,int_base:10}";

char tree_area_zone_list_columns[]=
"zone:zone_idx{width:32,bk_color:0xFF00FF00}, \
 addr:addr{width:64,bk_color:0xFF00FF00}, \
 refs:refs{width:28,bk_color:0xFF00FF00,int_base:10}, \
 time:time{width:32,bk_color:0xFF00FF00,int_base:10}, \
 name:node_name{width:64,bk_color:0xFF00FF00}, \
 type:node_type{width:64,bk_color:0xFF00FF00}, \
 chld:n_child{width:28,bk_color:0xFF00FF00,int_base:10}";


			
			
typedef struct
{
	mem_ptr			ptr;
	mem_size		size;
}mem_zone_desc;

typedef struct
{
	mem_zone_desc		mem;
	unsigned int		area_id;
	unsigned int		n_refs;
	unsigned int		time;
	zone_free_func_ptr  free_func;
}mem_zone;

typedef			mem_zone			*mem_zone_ptr;
typedef const	mem_zone_desc		*mem_zone_desc_ptr;
int			   out_debug			=	0xFFFFFFFF;

typedef struct
{
	unsigned int			area_id;
	mem_area_type_t			type;
	mem_ptr					lock_sema;
	mem_ptr					area_start;
	mem_ptr					area_end;
	mem_zone_desc			zones_free[MAX_MEM_ZONES];
	mem_zone_ptr			zones_buffer;
	

}mem_area;

mem_area	*__global_mem_areas		=	PTR_INVALID;
mem_zone	*__global_mem_zones		=	PTR_INVALID;

unsigned int	default_mem_area_id	=	0xCDCDCDCD;
mem_zone		mapped_zones[32]	=	{0xFF};
unsigned int	n_mapped_zones		=	0xCDCDCDCD;

int C_API_FUNC tree_manager_free_node		(mem_zone_ref_ptr p_node_ref);
int C_API_FUNC tree_manager_free_node_array	(mem_zone_ref_ptr childs_ref_ptr);

unsigned int get_node_array_num(mem_zone_ref_ptr	node_array);
void write_word(gdt_entry_ptr_t ptr,unsigned int ofset,unsigned short word)
{
	unsigned short *s_ptr;
	s_ptr=mem_add(ptr,ofset);

	(*s_ptr)=word;

}
unsigned short read_word(gdt_entry_ptr_t ptr,unsigned int ofset)
{
	unsigned short *s_ptr;
	s_ptr=mem_add(ptr,ofset);
	return (*s_ptr);
}


void write_byte(gdt_entry_ptr_t ptr,unsigned int ofset,unsigned char byte)
{
	unsigned char  *b_ptr;
	b_ptr=mem_add(ptr,ofset);

	(*b_ptr)=byte;

}

unsigned char  read_byte(gdt_entry_ptr_t ptr,unsigned int ofset)
{
	unsigned char  *b_ptr;
	b_ptr=mem_add(ptr,ofset);

	return (*b_ptr);

}
//base(32),limite(20/32),acces(8),flags(4/8),adresse(32)
int set_gdt_entry(gdt_ptr gdt,mem_zone_ref *gdt_addr_ref,unsigned int ofset,unsigned int base,unsigned int limit,unsigned char access,unsigned char flags)
{
	unsigned int gdt_limit;
	gdt_limit	=	gdt->limit;
	if((ofset+8)>=gdt_limit)
	{
		unsigned int end_ofset;

		end_ofset	=	ofset+8;
		if(end_ofset>0xFFFF)
			end_ofset=0xFFFF;

		gdt->limit	=	end_ofset;
		realloc_zone(gdt_addr_ref,gdt->limit);
		gdt->addr=get_zone_ptr(gdt_addr_ref,0);
	}
	

	write_word(gdt->addr,ofset+0,limit&0xFFFF);
	write_word(gdt->addr,ofset+2,base&0xFFFF);
	write_byte(gdt->addr,ofset+4,(base >> 16)&0xFF);
	write_byte(gdt->addr,ofset+5,access);
	write_byte(gdt->addr,ofset+6,(((limit>>16)&0x0F))|((flags&0x0F)<<4));
	write_byte(gdt->addr,ofset+7,(base >> 24)&0xFF);
	
	
	return 1;

}



int get_gdt_entry(gdt_ptr gdt,unsigned int idx,struct gdt_entry_desc_t *desc)
{
	unsigned int ofset;

	ofset=idx*sizeof(gdt_entry_t);

	desc->limit = ((read_word(gdt->addr,ofset+0))|((read_byte(gdt->addr,ofset+6)&0x0F)<<16));
	desc->base  = ((read_word(gdt->addr,ofset+2))|((read_byte(gdt->addr,ofset+4))<<16)|((read_byte(gdt->addr,ofset+7))<<24));
	desc->access= read_byte(gdt->addr,ofset+5);
	desc->flags = read_byte(gdt->addr,ofset+6)>>4;
	desc->ofset	= ofset;	

	return 1;
}

int copy_gdt_entry(gdt_ptr gdt,unsigned int idx,gdt_ptr new_gdt)
{
	unsigned int ofset;

	ofset=idx*sizeof(gdt_entry_t);
	memcpy_c(mem_add(new_gdt->addr,ofset),mem_add(gdt->addr,ofset),sizeof(gdt_entry_t));

	return 1;
}
#define GDT_ENTRY_AVL (0x01) //(0x01)
#define GDT_ENTRY_32bit  (0x01<<2)
#define GDT_ENTRY_G   (0x01<<3)

#define GDT_ACCESS_PRESENT	  (0x01<<7)
#define GDT_ACCESS_SYSTEM	  (0x01<<4)
#define GDT_ACCESS_CODE		  (0x01<<3)
#define GDT_ACCESS_WRITE	  (0x01<<1)
#define GDT_ACCESS_ACCESSED	  (0x01)



void dump_gdt(gdt_ptr gdt)
{
	unsigned int		n;
	unsigned int		n_entry;

	writestr("gdt table ");
	writeptr(gdt);
	writestr(" ");
	writeptr(gdt->addr);
	writestr(" ");
	writeint(gdt->limit,16);
	writestr("\n");

	n_entry		=	(gdt->limit+7)/8;
	n			=	0;
	while(n<n_entry)
	{
		struct gdt_entry_desc_t entry;

		get_gdt_entry	(gdt,n,&entry);
		
		if((entry.access&GDT_ACCESS_PRESENT)!=0)
		{
			writestr("gdt entry [");
			writeint(n,16);
			writestr("] ");
			writeint(entry.ofset,16);
			writestr(" ");
			writeint(entry.base,16);
			writestr(" ");
			writeint(entry.limit,16);
			writestr(" ");
			writeint(entry.access,16);
			writestr(" ");
			writeint(entry.flags,16);
			writestr("\n");
		}
		n++;
	}
}

OS_API_C_FUNC(void) do_gdt_real_mode(gdt_ptr new_gdt)
{
	struct gdt_t		gdt;
	mem_zone_ref		new_gdt_entries_ref;

	unsigned int		n;
	unsigned char		seg_access,seg_code_ax,flags;		
	unsigned int		n_entry;


	gdt.addr=PTR_NULL;
	gdt.limit=0;
	get_gdt_ptr_c		(&gdt);

	writestr("parsing gdt table ");
	writeptr(gdt.addr);
	writestr(" ");
	writeint(gdt.limit,16);
	writestr("\n");
	dump_gdt			(&gdt);

	new_gdt->limit	=	gdt.limit;
	n_entry			=	gdt.limit/8;

	new_gdt_entries_ref.zone=PTR_NULL;
	allocate_new_zone(0,new_gdt->limit,&new_gdt_entries_ref);
	new_gdt->addr	=	get_zone_ptr(&new_gdt_entries_ref,0);

	n=0;
	while(n<n_entry)
	{
		copy_gdt_entry	(&gdt,n,new_gdt);
		n++;
	}

	seg_access	=GDT_ACCESS_PRESENT|GDT_ACCESS_WRITE|GDT_ACCESS_SYSTEM;
	seg_code_ax	=GDT_ACCESS_PRESENT|GDT_ACCESS_WRITE|GDT_ACCESS_SYSTEM|GDT_ACCESS_CODE;
	flags		=GDT_ENTRY_AVL;//|GDT_ENTRY_32bit;
	

	//set_gdt_entry(new_gdt,&new_gdt_entries_ref,0x0,0x0,0xFFFF,seg_access,flags);

	
	set_gdt_entry(new_gdt,&new_gdt_entries_ref,0x0C00,0x000C0000,0xFFFF,seg_code_ax,flags);
	set_gdt_entry(new_gdt,&new_gdt_entries_ref,0x0F00,0x000F0000,0xFFFF,seg_code_ax,flags);

	set_gdt_entry(new_gdt,&new_gdt_entries_ref,0x0040,0x00000400,0xFFFF,seg_access,flags);
	set_gdt_entry(new_gdt,&new_gdt_entries_ref,0x9FC0,0x0009FC00,0xFFFF,seg_access,flags);
	

	set_gdt_entry(new_gdt,&new_gdt_entries_ref,0xA000,0x000A0000,0xFFFF,seg_access,flags);
	set_gdt_entry(new_gdt,&new_gdt_entries_ref,0xB000,0x000B0000,0xFFFF,seg_access,flags);
	set_gdt_entry(new_gdt,&new_gdt_entries_ref,0xB800,0x000B8000,0xFFFF,seg_access,flags);
	set_gdt_entry(new_gdt,&new_gdt_entries_ref,0xC000,0x000C0000,0xFFFF,	seg_access,flags);
	set_gdt_entry(new_gdt,&new_gdt_entries_ref,0xD000,0x000D0000,0xFFFF,seg_access,flags);
	set_gdt_entry(new_gdt,&new_gdt_entries_ref,0xE000,0x000E0000,0xFFFF,seg_access,flags);
	set_gdt_entry(new_gdt,&new_gdt_entries_ref,0xF000,0x000F0000,0xFFFF,seg_access,flags);

	set_gdt_entry(new_gdt,&new_gdt_entries_ref,0x0100,0x00001000,0xFFFF,seg_access,flags);
	set_gdt_entry(new_gdt,&new_gdt_entries_ref,0x1000,0x00010000,0xFFFF,seg_access,flags);

	set_gdt_ptr_c(new_gdt->limit,new_gdt->addr);

	
/*
	dump_gdt	 (new_gdt);
	snooze		 (1000000);

	writestr("set gdt tabe \n");
	snooze		 (1000000);
	

	writestr("gdt tabe ok\n");
	snooze		 (1000000);
*/
	

}

C_EXPORT mem_ptr __cdecl memset(mem_ptr ptr,unsigned char v,mem_size size)
{
	unsigned char *cptr=ptr;
	while(size--){cptr[size]=v;  }
	return ptr;

}
C_EXPORT mem_ptr __cdecl memcpy(mem_ptr dst_ptr,const_mem_ptr src_ptr,mem_size size)
{
	const unsigned char *sptr	=src_ptr;
	unsigned char *dptr			=dst_ptr;
	unsigned int	n			=0;;

	while(n<size){dptr[n]=sptr[n];n++;}

	return dst_ptr;
	
}

OS_API_C_FUNC(mem_ptr) memset_c(mem_ptr ptr,unsigned char v,mem_size size)
{
	unsigned char *cptr=ptr;
	while(size--){cptr[size]=v;  }
	return ptr;

}


OS_API_C_FUNC(mem_ptr) memset_32_c(mem_ptr ptr,unsigned int v,mem_size size)
{
	unsigned int *cptr=ptr;

	size>>=2;

	while(size--){cptr[size]=v;  }
	return ptr;

}

OS_API_C_FUNC(size_t) memchr_32_c(const_mem_ptr ptr,unsigned int value,mem_size size)
{
	const unsigned int *uint;
	const unsigned int *last_uint;

	if(size==0)return INVALID_SIZE;

	uint		=ptr;
	last_uint	=mem_add(ptr,size & (~0x03));
	
	while(uint<last_uint){ if((*uint)==value)return mem_sub(ptr,uint); uint++;}

	return INVALID_SIZE;

}

OS_API_C_FUNC(const_mem_ptr) memchr_c(const_mem_ptr ptr,int value,mem_size size)
{
	unsigned int n;
	const unsigned char *uchar;

	uchar	=ptr;
	n		=0;
	while(n<size){ if(uchar[n]==value)return &uchar[n]; n++;}

	return PTR_NULL;

}

OS_API_C_FUNC(int) memcmp_c(const_mem_ptr ptr_1,const_mem_ptr ptr_2,size_t size)
{
	unsigned int n;
	const unsigned char *ptr1,*ptr2;
	n=0;
	ptr1=ptr_1;
	ptr2=ptr_2;

	while(n<size){ if(ptr1[n]>ptr2[n])return 1; if(ptr1[n]<ptr2[n])return -1; n++;}
	return 0;

}
typedef	int word;		/* "word" used for optimal copy speed */
#define	wsize	sizeof(word)
#define	wmask	(wsize - 1)

OS_API_C_FUNC(mem_ptr)	memmove_c(mem_ptr dst_ptr,const_mem_ptr src_ptr,mem_size length)
{
	/*
	register char *dst = dst_ptr;
	register const char *src = src_ptr;
	register size_t t;
	*/
	char *dst = dst_ptr;
	const char *src = src_ptr;
	mem_size t;

	if (length == 0 || dst == src)		/* nothing to do */
	    goto done;

	/*
	 * Macros: loop-t-times; and loop-t-times, t>0
	 */
#define	TLOOP(s) if (t) TLOOP1(s)
#define	TLOOP1(s) do { s; } while (--t)

	if (mem_to_uint(dst) < mem_to_uint(src)) {
		/*
		 * Copy forward.
		 */
		t = mem_to_uint(src);	/* only need low bits */
		if ((t | mem_to_uint(dst)) & wmask) {
			/*
			 * Try to align operands.  This cannot be done
			 * unless the low bits match.
			 */
			if ((t ^ mem_to_uint(dst)) & wmask || length < wsize)
			    t = length;
			else
			    t = wsize - (t & wmask);
			length -= t;
			TLOOP1(*dst++ = *src++);
		}
		/*
		 * Copy whole words, then mop up any trailing bytes.
		 */
		t = length / wsize;
		TLOOP(*(word *)dst = *(word *)src; src += wsize; dst += wsize);
		t = length & wmask;
		TLOOP(*dst++ = *src++);
	} else {
		/*
		 * Copy backwards.  Otherwise essentially the same.
		 * Alignment works as before, except that it takes
		 * (t&wmask) bytes to align, not wsize-(t&wmask).
		 */
		src += length;
		dst += length;
		t = mem_to_uint(src);
		if ((t | mem_to_uint(dst)) & wmask) {
			if ((t ^  mem_to_uint(dst)) & wmask || length <= wsize)
			    t = length;
			else
			    t &= wmask;
			length -= t;
			TLOOP1(*--dst = *--src);
		}
		t = length / wsize;
		TLOOP(src -= wsize; dst -= wsize; *(word *)dst = *(word *)src);
		t = length & wmask;
		TLOOP(*--dst = *--src);
	}
    done:
	
	return dst_ptr;
}
/*
mem_ptr _intel_fast_memcpy(mem_ptr dst_ptr,const_mem_ptr src_ptr,mem_size size)
{
	return memcpy_c(dst_ptr,src_ptr,size);
}
*/
OS_API_C_FUNC(mem_ptr) memcpy_c(mem_ptr dst_ptr,const_mem_ptr src_ptr,mem_size size)
{
	const unsigned char *sptr	=src_ptr;
	unsigned char *dptr			=dst_ptr;
	unsigned int	n			=0;;

	while(n<size){dptr[n]=sptr[n];n++;}

	return dst_ptr;
	
}

OS_API_C_FUNC(mem_area *) get_area(unsigned int area_id)
{
	int n;
	n=0;
	if(__global_mem_areas==PTR_NULL)return PTR_NULL;
	if(__global_mem_zones==PTR_NULL)return PTR_NULL;

	if(area_id==0)
	{
		n=0;
		while(n<MAX_MEM_AREAS)
		{
			if(__global_mem_areas[n].area_id==default_mem_area_id)
				return &__global_mem_areas[n];
			n++;
		}
		return PTR_NULL;
	}


	while(n<MAX_MEM_AREAS)
	{
		if(__global_mem_areas[n].area_id==area_id)
			return &__global_mem_areas[n];
		n++;
	}
	return PTR_NULL;
}

/*
OS_API_C_FUNC(unsigned int) update_tree_area_zone_list(mem_zone_ref_ptr mem_area_zone_list_node ,unsigned int first,unsigned int num)
{
	mem_zone_ref	item_list={PTR_NULL};
	mem_area				*area_ptr;
	unsigned int			n,cnt,c_cnt;
	unsigned int			area_id;

	if(__global_mem_areas==PTR_NULL)return 0;
	if(__global_mem_zones==PTR_NULL)return 0;

	if(!tree_manager_get_child_value_i32 (mem_area_zone_list_node,NODE_HASH("area_id")	,&area_id))
	{
		writestr(" area id not found (get tree zone list) \n");
		return 0;
	}

	area_ptr=get_area(area_id);

	if(area_ptr==PTR_NULL){
		writestr(" area not found (get tree zone list) \n");
		return 0;
	}


	if(!tree_node_find_child_by_type		(mem_area_zone_list_node,NODE_GFX_CTRL_ITEM_LIST,&item_list,0))
	{
		writestr(" item list not found (get tree zone list) \n");
		return 0;
	}



	
	n				=   0;
	cnt				=	0;
	c_cnt			=	0;
	while(n<MAX_MEM_ZONES)
	{
		mem_zone_ptr zone;

		
		zone=&area_ptr->zones_buffer[n];
		if(zone->mem.ptr != PTR_NULL)
		{
			if(zone->free_func	==	tree_manager_free_node)
			{
				mem_zone_ref parent_node	={PTR_NULL};
				mem_zone_ref node			={PTR_NULL};

				node.zone	=	zone;

				tree_mamanger_get_parent			(&node,&parent_node);

				if((parent_node.zone==PTR_NULL)||(((mem_zone *)(parent_node.zone))->area_id!=area_id))
				{
					if(cnt>=first)
					{
						mem_zone_ref		item			={PTR_NULL};	
						if(tree_find_child_node_by_id_name	(&item_list,NODE_GFX_CTRL_ITEM,"item_idx",c_cnt,&item))
						{						

							const char					*node_name;
							unsigned int				node_type;
							size_t						num_children;

							node_type	=	tree_mamanger_get_node_type			(&node);
							node_name	=	tree_mamanger_get_node_name			(&node);
							num_children=	tree_manager_get_node_num_children	(&node);

							tree_manager_set_child_value_i32	(&item,"addr"		,mem_to_uint(zone->mem.ptr));
							tree_manager_set_child_value_i32	(&item,"refs"		,zone->n_refs);
							tree_manager_set_child_value_i32	(&item,"time"		,zone->time);
							tree_manager_set_child_value_i32	(&item,"zone_ptr"	,mem_to_uint(zone));
							tree_manager_set_child_value_i32	(&item,"zone_idx"	,n);

							tree_manager_set_child_value_str	(&item,"node_name"	,node_name);
							tree_manager_set_child_value_i32	(&item,"node_type"	,node_type);
							tree_manager_set_child_value_i32	(&item,"n_child"	,num_children);

							release_zone_ref(&item);
						}
						c_cnt++;
						
					}	
					cnt++;
				}
				
				release_zone_ref	(&parent_node);
			}

			/*
			if(zone->free_func	==	tree_manager_free_node_array)
			{
				mem_zone_ref array_node={PTR_NULL};

				array_node.zone	=	zone;
				tree_manager_set_child_value_str	(&item,"node_name"	,"X");
				tree_manager_set_child_value_i32	(&item,"node_type"	,0);
				tree_manager_set_child_value_i32	(&item,"n_child"	,get_node_array_num(&array_node));
			}

			if(zone->free_func	==	PTR_NULL)
			{
				tree_manager_set_child_value_str	(&item,"node_name"	,"X");
				tree_manager_set_child_value_i32	(&item,"node_type"	,0);
				tree_manager_set_child_value_i32	(&item,"n_child"	,0);
			}
			*
			

			if(c_cnt>=num)break;
			
		}
		n++;
	}

	release_zone_ref(&item_list);

	return 1;
}


OS_API_C_FUNC(unsigned int) get_tree_area_zone_list(unsigned int area_id,struct obj_array_t *ctrl_items,unsigned int first,unsigned int num,const char *style)
{

mem_area				*area_ptr;
	unsigned int			n,cnt,c_cnt;

	n=0;
	if(__global_mem_areas==PTR_NULL)return 0;
	if(__global_mem_zones==PTR_NULL)return 0;

	area_ptr=get_area(area_id);

	if(area_ptr==PTR_NULL){
		writestr(" area not found (get zone list) \n");
		return 0;
	}

	gfx_ctrl_new_row					(ctrl_items,style,	tree_area_zone_list_columns);
	n				=   0;
	cnt				=	0;
	c_cnt			=	0;
	while(n<MAX_MEM_ZONES)
	{
		mem_zone_ptr zone;

		zone=&area_ptr->zones_buffer[n];
		if(zone->mem.ptr != PTR_NULL)
		{
			if(zone->free_func	==	tree_manager_free_node)
			{
				mem_zone_ref parent_node={PTR_NULL};
				mem_zone_ref node={PTR_NULL};

				node.zone	=	zone;
		
				tree_mamanger_get_parent			(&node,&parent_node);

				if((parent_node.zone==PTR_NULL)||(((mem_zone *)(parent_node.zone))->area_id!=area_id))
				{	
					if((cnt>=first)&&(c_cnt<num))
					{
						const char  *node_name;
						unsigned int node_type;
						size_t		 num_children;
		
						node_type		=	tree_mamanger_get_node_type			(&node);
						node_name		=	tree_mamanger_get_node_name			(&node);
						num_children	=	tree_manager_get_node_num_children	(&node);

						gfx_ctrl_add_item				(ctrl_items,"zone","zone",c_cnt+1);
						tree_manager_add_obj_int_val	(ctrl_items,"zone_ptr"	,mem_to_uint(zone));
						tree_manager_add_obj_int_val	(ctrl_items,"zone_idx"	,n);
						tree_manager_add_obj_int_val	(ctrl_items,"item_idx"	,c_cnt);
						tree_manager_add_obj_int_val	(ctrl_items,"addr"		,mem_to_uint(zone->mem.ptr));
						tree_manager_add_obj_int_val	(ctrl_items,"refs"		,zone->n_refs);
						tree_manager_add_obj_int_val	(ctrl_items,"time"		,zone->time);
						tree_manager_add_obj_str_val	(ctrl_items,"node_name"	,node_name);
						tree_manager_add_obj_int_val	(ctrl_items,"node_type"	,node_type);
						tree_manager_add_obj_int_val	(ctrl_items,"n_child"	,num_children);

						c_cnt++;
					}
					cnt++;
				}
				
				release_zone_ref(&parent_node);
			}
			
			/*
			if(zone->free_func	==	tree_manager_free_node_array)
			{
				mem_zone_ref array_node={PTR_NULL};

				array_node.zone	=	zone;

				tree_manager_add_obj_str_val	(ctrl_items,"node_name"	,"X");
				tree_manager_add_obj_int_val	(ctrl_items,"node_type"	,0);
				tree_manager_add_obj_int_val	(ctrl_items,"n_child"	,get_node_array_num(&array_node));
			}
			if(zone->free_func	==	PTR_NULL)
			{
				tree_manager_add_obj_str_val	(ctrl_items,"node_name"	,"X");
				tree_manager_add_obj_int_val	(ctrl_items,"node_type"	,0);
				tree_manager_add_obj_int_val	(ctrl_items,"n_child"	,0);
			}
			*
		
			
		}
		n++;
	}

	


	return cnt;


}

OS_API_C_FUNC(unsigned int) update_mem_area_zone_list(mem_zone_ref_ptr mem_area_zone_list_node,unsigned int first,unsigned int num)
{
	mem_zone_ref	item_list={PTR_NULL};
	mem_area				*area_ptr;
	unsigned int			n,cnt,c_cnt;
	unsigned int			area_id;

	if(__global_mem_areas==PTR_NULL)return 0;
	if(__global_mem_zones==PTR_NULL)return 0;

	tree_manager_get_child_value_i32 (mem_area_zone_list_node,NODE_HASH("area_id")	,&area_id);

	area_ptr=get_area(area_id);

	if(area_ptr==PTR_NULL){
		writestr(" area not found (get zone list) \n");
		return 0;
	}

	if(!tree_node_find_child_by_type		(mem_area_zone_list_node,NODE_GFX_CTRL_ITEM_LIST,&item_list,0))return 0;

	
	n				=   0;
	cnt				=	0;
	c_cnt			=	0;
	while(n<MAX_MEM_ZONES)
	{
		mem_zone_ptr zone;

		zone=&area_ptr->zones_buffer[n];
		if(zone->mem.ptr != PTR_NULL)
		{
			if(cnt>=first)
			{
				mem_zone_ref		item			={PTR_NULL};
				if(tree_find_child_node_by_id_name	(&item_list,NODE_GFX_CTRL_ITEM,"item_idx",c_cnt,&item))
				{
					tree_manager_set_child_value_i32 (&item,"area_id"	,area_ptr->area_id);
					tree_manager_set_child_value_i32 (&item,"zone_ptr"	,mem_to_uint(zone));
					tree_manager_set_child_value_i32 (&item,"zone_idx"	,n);

					tree_manager_set_child_value_i32 (&item,"addr"		,mem_to_uint(zone->mem.ptr));
					tree_manager_set_child_value_i32 (&item,"size"		,zone->mem.size);
					tree_manager_set_child_value_i32 (&item,"refs"		,zone->n_refs);
					tree_manager_set_child_value_i32 (&item,"time"		,zone->time);
					c_cnt++;
					release_zone_ref(&item);
				}
				if(c_cnt>=num)break;
			}
			cnt++;
		}
		n++;
	}
	release_zone_ref(&item_list);
	return 1;
}


OS_API_C_FUNC(unsigned int) get_mem_area_zone_list(unsigned int area_id,struct obj_array_t *ctrl_items,unsigned int first,unsigned int num,const char *style)
{
	mem_area				*area_ptr;
	unsigned int			n,cnt,c_cnt;

	n=0;
	if(__global_mem_areas==PTR_NULL)return 0;
	if(__global_mem_zones==PTR_NULL)return 0;

	area_ptr=get_area(area_id);

	if(area_ptr==PTR_NULL){
		writestr(" area not found (get zone list) \n");
		return 0;
	}

	gfx_ctrl_new_row					(ctrl_items,style,	mem_area_zone_list_columns);
	n				=   0;
	cnt				=	0;
	c_cnt			=	0;
	while(n<MAX_MEM_ZONES)
	{
		mem_zone_ptr zone;

		zone=&area_ptr->zones_buffer[n];
		if(zone->mem.ptr != PTR_NULL)
		{
			if((cnt>=first)&&(c_cnt<num))
			{
				gfx_ctrl_add_item				(ctrl_items,"zone","zone",n+1);
				tree_manager_add_obj_int_val	(ctrl_items,"area_id"	,area_ptr->area_id);
				tree_manager_add_obj_int_val	(ctrl_items,"item_idx"	,c_cnt);
				tree_manager_add_obj_int_val	(ctrl_items,"zone"		,mem_to_uint(zone));
				tree_manager_add_obj_int_val	(ctrl_items,"addr"		,mem_to_uint(zone->mem.ptr));
				tree_manager_add_obj_int_val	(ctrl_items,"size"		,zone->mem.size);
				tree_manager_add_obj_int_val	(ctrl_items,"refs"		,zone->n_refs);
				tree_manager_add_obj_int_val	(ctrl_items,"time"		,zone->time);
				c_cnt++;
			}
			cnt++;
		}
		n++;
	}


	return cnt;

}



OS_API_C_FUNC(unsigned int) get_mem_area_list(struct obj_array_t *ctrl_items,unsigned int first,unsigned int num,const char *style)
{
	size_t					mem_size;
	int						n_zones;
	mem_area				*area_ptr;
	unsigned int			n,cnt,c_cnt;

	n=0;
	if(__global_mem_areas==PTR_NULL)return 0;
	if(__global_mem_zones==PTR_NULL)return 0;


	gfx_ctrl_new_row					(ctrl_items,style,mem_area_list_columns);

	cnt=0;
	c_cnt=0;
	n=0;
	while(n<MAX_MEM_AREAS)
	{
		char			area_name[32];
		char			area_id[32];
		unsigned int	_n;

		area_ptr		=	&__global_mem_areas[n];

		if(area_ptr->area_start!=0x00000000)
		{
			if((cnt>=first)&&(c_cnt<num))
			{
				mem_size		=  0;
				n_zones			=  0;
				_n				=  0;
				while(_n<MAX_MEM_ZONES)
				{
					if(area_ptr->zones_buffer[_n].mem.ptr != PTR_NULL)
					{
						mem_size+=area_ptr->zones_buffer[_n].mem.size;
						n_zones++;
					}
					_n++;
				}
				itoa_s							(__global_mem_areas[n].area_id,area_id,32,16);
				strcpy_s						(area_name,32,"memory area ");
				strcat_s						(area_name,32,area_id);

				

				gfx_ctrl_add_item				(ctrl_items,area_name,area_name,n+1);

				tree_manager_add_obj_int_val	(ctrl_items,"area_id"	,area_ptr->area_id);
				tree_manager_add_obj_int_val	(ctrl_items,"item_idx"	,c_cnt);
				tree_manager_add_obj_int_val	(ctrl_items,"type"		,area_ptr->type);
				switch(area_ptr->type)
				{
					case 1	:tree_manager_add_obj_str_val	(ctrl_items,"type_str"		,"data");break;
					case 2	:tree_manager_add_obj_str_val	(ctrl_items,"type_str"		,"nodes");break;
					default	:tree_manager_add_obj_str_val	(ctrl_items,"type_str"		," X ");break;
				}
				
				tree_manager_add_obj_int_val	(ctrl_items,"start"		,mem_to_uint	(area_ptr->area_start));
				tree_manager_add_obj_int_val	(ctrl_items,"end"		,mem_to_uint	(area_ptr->area_end));
				tree_manager_add_obj_int_val	(ctrl_items,"used"		,mem_size/1024);
				tree_manager_add_obj_int_val	(ctrl_items,"n_zones"	,n_zones);

				c_cnt++;
			}
			cnt++;
		}
		
		n++;
	}


	return cnt;

}


OS_API_C_FUNC(unsigned int) update_mem_area_list(mem_zone_ref_ptr mem_area_list_node,unsigned int first,unsigned int num)
{
	mem_zone_ref	item_list={PTR_NULL};
	mem_area				*area_ptr;
	unsigned int			n,cnt,c_cnt;

	if(__global_mem_areas==PTR_NULL)return 0;
	if(__global_mem_zones==PTR_NULL)return 0;

	if(!tree_node_find_child_by_type		(mem_area_list_node,NODE_GFX_CTRL_ITEM_LIST,&item_list,0))return 0;

	
	cnt=0;
	c_cnt=0;
	n=0;
	while(n<MAX_MEM_AREAS)
	{
		area_ptr		=	&__global_mem_areas[n];

		if(area_ptr->area_start!=0x00000000)
		{
			if(cnt>=first)
			{
				mem_zone_ref		item			={PTR_NULL};

				
				if(tree_find_child_node_by_id_name	(&item_list,NODE_GFX_CTRL_ITEM,"item_idx",c_cnt,&item))
				{
					unsigned int	_n;
					size_t		mem_size;
					int			n_zones;


					mem_size		=  0;
					n_zones			=  0;
					_n				=  0;
					while(_n<MAX_MEM_ZONES)
					{
						if(area_ptr->zones_buffer[_n].mem.ptr != PTR_NULL)
						{
							mem_size+=area_ptr->zones_buffer[_n].mem.size;
							n_zones++;
						}
						_n++;
					}

					tree_manager_set_child_value_i32 (&item,"area_id"	,area_ptr->area_id);
					tree_manager_set_child_value_i32 (&item,"type"		,area_ptr->type);

					switch(area_ptr->type)
					{
						case 1	:tree_manager_set_child_value_str	(&item,"type_str"		,"data");break;
						case 2	:tree_manager_set_child_value_str	(&item,"type_str"		,"nodes");break;
						default	:tree_manager_set_child_value_str	(&item,"type_str"		," X ");break;
					}
				

					tree_manager_set_child_value_i32 (&item,"start"		,mem_to_uint	(area_ptr->area_start));
					tree_manager_set_child_value_i32 (&item,"end"		,mem_to_uint	(area_ptr->area_end));

					tree_manager_set_child_value_i32 (&item,"used"		,mem_size/1024);
					tree_manager_set_child_value_i32 (&item,"n_zones"	,n_zones);
					release_zone_ref(&item);
					c_cnt++;
				}
			}
			if(c_cnt>=num)break;
			cnt++;
		}
		n++;
	}
	release_zone_ref(&item_list);
	return 1;
}
*/

mem_area	*get_area_by_zone_ptr	(const mem_zone *zone)
{
	int n;
	if(__global_mem_areas==PTR_NULL)return PTR_NULL;
	if(__global_mem_zones==PTR_NULL)return PTR_NULL;


	n=0;
	while(n<MAX_MEM_AREAS)
	{
		if(__global_mem_areas[n].area_start == 0x00000000)return PTR_NULL;
		if((zone>=__global_mem_areas[n].zones_buffer)&&(zone<&__global_mem_areas[n].zones_buffer[MAX_MEM_AREAS]))
			return &__global_mem_areas[n];
		n++;
	}


	return PTR_NULL;

}

//int check_zone			(const mem_zone_ref *ref)
int check_zone	(const mem_zone *zone)
{
	mem_area				*area_ptr;
	if(zone->area_id==0xFFFF)return 1;
	

	area_ptr			=	get_area(zone->area_id);
	if(area_ptr==PTR_NULL)
	{
		writestr("zone check area failed ");
		writeptr(zone);
		writestr(" not found \n");
		return 0;
	}
	if((zone<area_ptr->zones_buffer)||(zone>=&area_ptr->zones_buffer[MAX_MEM_ZONES]))
	{
		writestr("invalid zone ptr ");
		writeptr(zone);
		writestr(" ");
		writeint(zone->area_id,16);
		writestr(" ");
		writeptr(area_ptr->zones_buffer);
		writestr("->");
		writeptr(&area_ptr->zones_buffer[MAX_MEM_ZONES]);
		writestr("\n");

	}

	if((zone->mem.ptr==uint_to_mem(0xFFFFFFFF))&&(zone->mem.size==0))return 1;


	if((zone->mem.ptr==PTR_NULL)&&(zone->mem.size==0))
	{
		writestr("zone mem ptr null [");
		writeint(zone->area_id,16);
		writestr("] ");
		writeptr(zone);
		writestr("\n");
		return 0;
	}

	if((zone->mem.ptr<area_ptr->area_start)||(zone->mem.ptr>area_ptr->area_end))
	{
		writestr("check mem ptr fail zone:");
		writeptr(zone);
		writestr("-");
		writeint(zone->area_id,16);
		writestr(" addr:");
		writeptr(zone->mem.ptr);
		writestr("->");
		writesz(zone->mem.size,16);
		writestr(" ");
		writeint(zone->n_refs,10);
		writestr("\n");
		return 0;
	}

	return 1;

}



OS_API_C_FUNC(mem_ptr)	get_zone_ptr	(const mem_zone_ref *ref,mem_size ofset)
{
	if(ref==PTR_NULL) return PTR_INVALID;
	if(ref->zone==PTR_NULL)
	{
		writestr("zone reference null \n");

		//if(out_debug)
		{
			dump_task_infos_c();
			//snooze(1000000);
		}
		return PTR_INVALID;
	}

	if(!check_zone(ref->zone))
	{
		writestr("check zone failed ");
		writeptr(ref->zone);
		writestr("\n");
		//if(out_debug)
		{
			dump_task_infos_c();
			//snooze(4000000);
		}
		return PTR_INVALID;
	}

	if((((mem_zone *)(ref->zone))->mem.size)==0)
	{
		writestr("get zone ptr size 0 ");
		writeptr(ref->zone);
		writestr(" ");
		writeptr((((mem_zone *)(ref->zone))->mem.ptr));
		writestr("\n");
		//if(out_debug)
		{
			dump_task_infos_c();
			//snooze(4000000);
		}
		return PTR_INVALID;
	}

	if(ofset==0xFFFFFFFF)return mem_add( ((mem_zone *)(ref->zone))->mem.ptr,((mem_zone *)(ref->zone))->mem.size);
	if(ofset>=((mem_zone *)(ref->zone))->mem.size)
	{
		writestr("get_zone_ptr ofset out of boundary \n");
		//snooze	(1000000);
		return PTR_INVALID;
	}

	return mem_add( ((mem_zone *)(ref->zone))->mem.ptr,ofset);
}

/*
OS_API_C_FUNC(mem_ptr)	get_zone_const_ptr	(mem_zone_const_ref_ptr ref,mem_size ofset)
{
	mem_ptr ret;

	if(ref==PTR_NULL) return PTR_INVALID;
	if(ref->zone==PTR_NULL)
	{
		writestr("const zone null \n");
		dump_task_infos_c();
		snooze(1000000);

		return PTR_INVALID;
	}

	if(!check_zone(ref->zone))
	{
		writestr("const check zone failed \n");
		dump_task_infos_c();
		snooze(1000000);

		return PTR_INVALID;
	}

	if((ofset!=0xFFFFFFFF)&&(ofset>((mem_zone *)(ref->zone))->mem.size))
	{
		writestr("const get_zone_ptr ofset out of boundary \n");
		dump_task_infos_c();
		snooze(1000000);
		return PTR_INVALID;
	}
	
	if(ofset==0xFFFFFFFF)
		ret=mem_add(((mem_zone *)(ref->zone))->mem.ptr,((mem_zone *)(ref->zone))->mem.size);
	else
		ret=mem_add(((mem_zone *)(ref->zone))->mem.ptr,ofset);

	return ret;
}

OS_API_C_FUNC(mem_size) get_zone_const_size(mem_zone_const_ref_ptr ref)
{
	if(ref==PTR_NULL) return 0;
	if(ref->zone==PTR_NULL) return 0;

	return ((mem_zone *)(ref->zone))->mem.size;
}
*/
OS_API_C_FUNC(mem_size) get_zone_size(mem_zone_ref_const_ptr ref)
{
	if(ref==PTR_NULL) return 0;
	if(ref->zone==PTR_NULL) return 0;

	return ((mem_zone *)(ref->zone))->mem.size;
}


mem_size set_zone_free(mem_zone_ref_ptr ref,zone_free_func_ptr	free_func)
{
	if(ref==PTR_NULL) return 0;
	if(ref->zone==PTR_NULL) return 0;

	((mem_zone *)(ref->zone))->free_func=free_func;

	return 1;
}


void set_default_mem_area(unsigned int mem_area_id)
{
	default_mem_area_id		=	mem_area_id;
}


OS_API_C_FUNC(void) init_default_mem_area(unsigned int size)
{
	mem_ptr				start,end;
	if(default_mem_area_id	!= 0xCDCDCDCD)return ;

	
	start					=	get_next_aligned_ptr(kernel_memory_map_c(size+8));
	end						=	mem_add(start,size);
	default_mem_area_id		=	init_new_mem_area	(start,end,MEM_TYPE_DATA);

	memset_c(start,0,mem_sub(start,end));

	return;
}



OS_API_C_FUNC(void) init_mem_system()
{
	if(__global_mem_areas	!= PTR_INVALID)return;
	if(__global_mem_zones	!= PTR_INVALID)return;

	__global_mem_areas	=get_next_aligned_ptr(kernel_memory_map_c(MAX_MEM_AREAS*sizeof(mem_area)+8));
	__global_mem_zones	=get_next_aligned_ptr(kernel_memory_map_c(MAX_MEM_AREAS*MAX_MEM_ZONES*sizeof(mem_zone)+8));

	memset_c(__global_mem_areas,0,MAX_MEM_AREAS*sizeof(mem_area)	);
	memset_c(__global_mem_zones,0,MAX_MEM_AREAS*MAX_MEM_ZONES*sizeof(mem_zone) );

	n_mapped_zones			=	0;
	out_debug				=	0;

	writestr		("init mem base system ");
	writeptr		(__global_mem_areas);
	writestr		(" ");
	writeptr		(__global_mem_zones);
	writestr		("\n");

	
}
mem_ptr	get_next_aligned_ptr(mem_ptr ptr)
{
	unsigned int val_addr;

	val_addr=mem_to_uint(ptr);
	if((val_addr&0x0000000F)==0)return ptr;

	return uint_to_mem(((val_addr&0xFFFFFFF0)+16));
}


mem_ptr	get_next_seg_aligned_ptr(mem_ptr ptr)
{
	unsigned int val_addr;

	val_addr=mem_to_uint(ptr);
	if((val_addr&0x0000FFFF)==0)return ptr;

	return uint_to_mem(((val_addr&0xFFFF0000)+0x10000));
}
mem_size get_aligned_size(mem_ptr ptr,mem_ptr end)
{
	mem_ptr			start_align;
	mem_size		size_align;
	mem_size		size_av;
	
	start_align		=	get_next_aligned_ptr(ptr);
	if(start_align>=end)return 0;
	
	size_av			=	mem_sub(start_align,end);
	size_align		=	size_av&0xFFFFFFF0;

	return (size_align);
}

OS_API_C_FUNC(unsigned int) mem_area_enable_sem(unsigned int area_id)
{
	mem_area				*area_ptr;

	area_ptr			=	get_area(area_id);
	if(area_ptr==PTR_NULL)
	{
		writestr("area [");
		writeint(area_id,16);
		writestr("] not found \n");
		return 0;
	}
	area_ptr->lock_sema=task_manager_new_semaphore(1,32);

	if(area_ptr->lock_sema==PTR_NULL)return 0;

	writestr("created a semaphore on mem area [");
	writeint(area_id,16);
	writestr("] ");
	writeptr(area_ptr->lock_sema);
	writestr("\n");
	return 1;

}
/*
OS_API_C_FUNC(mem_ptr) mem_area_swap_sem(unsigned int area_id,mem_ptr sem)
{
	mem_area				*area_ptr;
	mem_ptr					ret;

	area_ptr			=	get_area(area_id);
	if(area_ptr==PTR_NULL)
	{
		writestr("area [");
		writeint(area_id,16);
		writestr("] not found \n");
		return PTR_NULL;
	}
	ret					=	area_ptr->lock_sema;
	area_ptr->lock_sema	=	sem;
	return ret;
}
*/


OS_API_C_FUNC(unsigned int) init_new_mem_area(mem_ptr phys_start,mem_ptr phys_end,mem_area_type_t type)
{
	int n;
	n=0;

	if(__global_mem_areas==PTR_NULL)return 0xFFFFFFFF;
	if(__global_mem_zones==PTR_NULL)return 0xFFFFFFFF;
	
	
	while(n<MAX_MEM_AREAS)
	{
		if(__global_mem_areas[n].area_start == 0x00000000)
		{
	
			__global_mem_areas[n].area_id			=n+1;
			__global_mem_areas[n].area_start		=phys_start;
			__global_mem_areas[n].area_end			=phys_end;
			__global_mem_areas[n].type				=type;
			__global_mem_areas[n].zones_buffer		=&__global_mem_zones[MAX_MEM_ZONES*n];

			__global_mem_areas[n].lock_sema			=	PTR_NULL;
			
			
			memset_c	(__global_mem_areas[n].zones_free		,0,MAX_MEM_ZONES*sizeof(mem_zone_desc));

			__global_mem_areas[n].zones_free[0].ptr		=	get_next_aligned_ptr(__global_mem_areas[n].area_start);
			__global_mem_areas[n].zones_free[0].size	=	get_aligned_size	( __global_mem_areas[n].area_start, __global_mem_areas[n].area_end);
			
			writestr		("init new memory area ");
			writeptr		(phys_start);
			writestr		(" ");
			writeptr		(phys_end);
			writestr		(" area id : ");
			writeint		(__global_mem_areas[n].area_id,16);
			writestr		("\n");
		
							
			return __global_mem_areas[n].area_id;
	
		}
		n++;
	}

	return 0xFFFFFFFF;
	
}






mem_area * find_area(mem_ptr ptr,mem_size size)
{
	int n;
	mem_ptr	 end_zone;
	
	if(__global_mem_areas==PTR_NULL)return PTR_NULL;
	if(__global_mem_zones==PTR_NULL)return PTR_NULL;

	end_zone	=	mem_add(ptr,size);

	n=0;
	while(n<MAX_MEM_AREAS)
	{
		if((ptr>=__global_mem_areas[n].area_start)&&(end_zone<__global_mem_areas[n].area_end))
			return &__global_mem_areas[n];
		n++;
	}


	return PTR_NULL;
}

void dump_mem_used_after	(unsigned int area_id,unsigned int time)
{
	int						n;
	size_t					mem_size;
	int						n_zones;
	mem_area				*area_ptr;

	area_ptr			=	get_area(area_id);
	if(area_ptr==PTR_NULL)
	{
		writestr("area [");
		writeint(area_id,16);
		writestr("] not found \n");
		return ;
	}

	writestr("+-+-+-+- zone dump +-+-+-+-\n");

	mem_size		=  0;
	n_zones			=  0;
	n				=  0;
	while(n<MAX_MEM_ZONES)
	{
		if(area_ptr->zones_buffer[n].mem.ptr != PTR_NULL)
		{
			if(area_ptr->zones_buffer[n].time>time)
			{
				writestr_fmt("zone[%d] [%d] %p %p %d %d\n",n_zones,area_ptr->zones_buffer[n].time,&area_ptr->zones_buffer[n],area_ptr->zones_buffer[n].mem.ptr,area_ptr->zones_buffer[n].mem.size,area_ptr->zones_buffer[n].n_refs);
				n_zones++;
				/*
				unsigned int	*data;
				int _n;

				if(area_ptr->zones_buffer[n].mem.size>0)
				{
					data=area_ptr->zones_buffer[n].mem.ptr;
					for(_n=0;_n<4;_n++)
					{
						writeint(data[_n],16);
						writestr(",");
					}
					writestr("\n");
				}
				*/
			}
		}
		n++;
	}

}

OS_API_C_FUNC(void) dump_mem_used	(unsigned int area_id)
{
	int						n;
	size_t					mem_size;
	int						n_zones;
	mem_area				*area_ptr;

	if(__global_mem_areas==PTR_NULL)return ;
	if(__global_mem_zones==PTR_NULL)return ;

	if(area_id==0xFFFFFFFF)
	{
		int n;

		n=0;
		while(n<MAX_MEM_AREAS)
		{
			unsigned int _n;


			area_ptr		=	&__global_mem_areas[n];

			if(area_ptr->area_start!=0x00000000)
			{
				mem_size		=  0;
				n_zones			=  0;
				_n				=  0;
				while(_n<MAX_MEM_ZONES)
				{
					if(area_ptr->zones_buffer[_n].mem.ptr != PTR_NULL)
					{
						mem_size+=area_ptr->zones_buffer[_n].mem.size;
						n_zones++;
					}
					_n++;
				}

				writestr("area [");
				writeint(area_ptr->area_id,16);
				writestr("]mem : ");
				writesz(mem_size/1024,10);
				writestr("kb/");
				writesz(mem_sub(area_ptr->area_start,area_ptr->area_end)/1024,10);
				writestr("kb in ");
				writeint(n_zones,10);
				writestr("/");
				writeint(MAX_MEM_ZONES,10);
				writestr(" zones\n");
			}
			n++;
		}
		return;
	}

	area_ptr			=	get_area(area_id);
	if(area_ptr==PTR_NULL)
	{
		writestr("area [");
		writeint(area_id,16);
		writestr("] not found \n");
		return ;
	}

	mem_size		=  0;
	n_zones			=  0;
	n				=  0;
	while(n<MAX_MEM_ZONES)
	{
		if(area_ptr->zones_buffer[n].mem.ptr != PTR_NULL)
		{
			mem_size+=area_ptr->zones_buffer[n].mem.size;
			n_zones++;
		}
		n++;
	}

	writestr("memory used : ");
	writesz(mem_size/1024,10);
	writestr("kb / ");
	writesz(mem_sub(area_ptr->area_start,area_ptr->area_end)/1024,10);
	writestr("kb in ");
	writeint(n_zones,10);
	writestr(" / ");
	writeint(MAX_MEM_ZONES,10);
	writestr(" zones\n");
	
}

/*
OS_API_C_FUNC(void) dump_free_zones	(unsigned int area_id)
{
	int						n;
	mem_area				*area_ptr;

	area_ptr			=	get_area(area_id);
	if(area_ptr==PTR_NULL)
	{
		writestr("area [");
		writeint(area_id,16);
		writestr("] not found \n");
		return ;
	}
	
	n	=	0;
	while(area_ptr->zones_free[n].ptr!= PTR_NULL)
	{
		mem_ptr	 start_free_zone;
		mem_ptr	 end_free_zone;

		start_free_zone			=	area_ptr->zones_free[n].ptr;
		end_free_zone			=	mem_add(area_ptr->zones_free[n].ptr,area_ptr->zones_free[n].size);

		writestr("free zone ");
		writeint(n,10);
		writestr(" ");
		writeptr(start_free_zone);
		writestr(" ");
		writeptr(end_free_zone);
		writestr("\n");

		n++;
	}
}
*/
int find_free_zone		(const mem_area *area,mem_size size,mem_zone_desc	*desc)
{
	int						n;
	n	=	0;
	
	while(area->zones_free[n].ptr!= PTR_NULL)
	{
		mem_ptr	 start_free_zone;
		mem_ptr	 end_free_zone;
		mem_ptr	 start_aligned_free_zone;
		mem_size size_aligned_free_zone;

		start_free_zone			=	area->zones_free[n].ptr;
		end_free_zone			=	mem_add(area->zones_free[n].ptr,area->zones_free[n].size);



		start_aligned_free_zone	=	get_next_aligned_ptr(start_free_zone);
		if(start_aligned_free_zone!=0x0)
		{
			size_aligned_free_zone	=	mem_sub	(start_aligned_free_zone,end_free_zone);

			if(size_aligned_free_zone>=size)
			{
				desc->ptr	=start_aligned_free_zone;
				desc->size	=size;
				return 1;
			}
		}
		n++;
		if(n>=MAX_MEM_ZONES)return 0;
	}
	desc->ptr	=	PTR_NULL;
	desc->size	=	0;

	return 0;
}


int allocate_zone(mem_area *area,const mem_zone_desc *desc)
{
	int						n;
	mem_zone_desc_ptr		ret;
	
	n	=	0;
	ret	=	PTR_NULL;

	while(area->zones_free[n].ptr!=	PTR_NULL)
	{
		mem_ptr		start_aligned_free_zone,end_free_zone;
		mem_size	size_aligned_free_zone;

		end_free_zone			=	mem_add(area->zones_free[n].ptr,area->zones_free[n].size);

		start_aligned_free_zone	=	get_next_aligned_ptr(area->zones_free[n].ptr);
		size_aligned_free_zone	=	mem_sub	(start_aligned_free_zone,end_free_zone);

		if(	(start_aligned_free_zone==desc->ptr)&&(size_aligned_free_zone>=desc->size))
		{
			mem_ptr					free_zone_end_ptr;
			mem_ptr					zone_end_ptr;
			mem_ptr					zone_end_aligned_ptr;

			free_zone_end_ptr		= 	mem_add	 (desc->ptr,area->zones_free[n].size);
			zone_end_ptr			=	mem_add	 (desc->ptr,desc->size);
			zone_end_aligned_ptr	=	get_next_aligned_ptr(zone_end_ptr);
			if(zone_end_aligned_ptr<free_zone_end_ptr)
			{
				area->zones_free[n].ptr		=  zone_end_aligned_ptr;
				area->zones_free[n].size	=  mem_sub	(zone_end_aligned_ptr,free_zone_end_ptr);
			}
			else
			{
				while(area->zones_free[n].ptr	!=	PTR_NULL)
				{
					area->zones_free[n].ptr		=area->zones_free[n+1].ptr;
					area->zones_free[n].size	=area->zones_free[n+1].size;
					n++;
				}
				area->zones_free[n].ptr		=PTR_NULL;
				area->zones_free[n].size	=0;
			}

			
			return 1;
		}
		n++;
	}

	return 0;
}


void	free_zone_area		(unsigned int area_id,mem_zone_desc *mem)
{
	unsigned int			n,cnt;
	mem_ptr					start_zone,end_zone;
	mem_ptr					start_free_zone,end_free_zone;
	mem_zone_desc			new_free_zone;
	mem_area				*area_ptr;

	if(area_id	==0xFFFF)return;
	if(mem->ptr	==PTR_INVALID)return;
	
	area_ptr			=	get_area(area_id);
	if(area_ptr==PTR_NULL)
	{
		writestr("area [");
		writeint(area_id,16);
		writestr("] not found \n");
		return ;
	}

	task_manager_aquire_semaphore(area_ptr->lock_sema,0);

	if(mem->size>0)
	{
		start_zone			=	mem->ptr;
		end_zone			=	mem_add(start_zone,mem->size);

		new_free_zone.ptr	=	start_zone;
		new_free_zone.size	=	mem_sub(start_zone,end_zone);

		n					=  0;
		while(area_ptr->zones_free[n].ptr!=PTR_NULL)
		{
			start_free_zone	=	area_ptr->zones_free[n].ptr;
			end_free_zone	=	mem_add(start_free_zone,area_ptr->zones_free[n].size);

			if(end_zone==start_free_zone)
			{
				new_free_zone.ptr	=start_zone;
				new_free_zone.size	=mem_sub(start_zone,end_free_zone);

				end_zone			=end_free_zone;

				cnt=n;
				while(area_ptr->zones_free[cnt].ptr!=PTR_NULL)
				{
					area_ptr->zones_free[cnt]=area_ptr->zones_free[cnt+1];
					cnt++;
				}
				area_ptr->zones_free[cnt].ptr=PTR_NULL;
				area_ptr->zones_free[cnt].size=0;
				
			}
			else if(start_zone==end_free_zone)
			{
				new_free_zone.ptr	=start_free_zone;
				new_free_zone.size	=mem_sub(new_free_zone.ptr,end_zone);
				start_zone			=start_free_zone;

				cnt=n;
				while(area_ptr->zones_free[cnt].ptr!=PTR_NULL)
				{
					area_ptr->zones_free[cnt]=area_ptr->zones_free[cnt+1];
					cnt++;
				}
				area_ptr->zones_free[cnt].ptr	=PTR_NULL;
				area_ptr->zones_free[cnt].size	=0;
			}
			else
			{
				n++;
			}
		}
		memset_32_c			(new_free_zone.ptr,0xDEF0DEF0,new_free_zone.size);

		n	=  0;
		while(n<MAX_MEM_ZONES)
		{
			if(area_ptr->zones_free[n].ptr==PTR_NULL)
			{
				area_ptr->zones_free[n].ptr		=new_free_zone.ptr;
				area_ptr->zones_free[n].size	=new_free_zone.size;
				break;
			}
			n++;
		}
	}

	mem->ptr	=PTR_NULL;
	mem->size	=0;


	task_manager_release_semaphore(area_ptr->lock_sema,0);
	
}

void	free_zone		(mem_zone_ref_ptr zone_ref)
{
	mem_zone *src_zone=zone_ref->zone;

	if(src_zone==PTR_NULL)return;
	

	if(src_zone->n_refs>0)
	{
		writestr("unable to free zone ");
		writeptr(src_zone->mem.ptr);
		writestr(" - ");
		writesz(src_zone->mem.size,16);
		writestr(" nref : ");
		writeint(src_zone->n_refs,16);
		writestr("\n");

		dump_task_infos_c	();	
		//snooze				(10000000);
		return;
	}

	free_zone_area			(src_zone->area_id,&src_zone->mem);

	src_zone->n_refs		=0;
	src_zone->free_func		=PTR_NULL;
	src_zone->area_id		=0;

//	zone_ref->zone			=PTR_NULL;

}
OS_API_C_FUNC(unsigned int) get_zone_numref(mem_zone_ref *zone_ref)
{
	if(zone_ref==PTR_NULL)return 0;
	if(zone_ref->	zone==PTR_NULL)return 0;
	return ((mem_zone *)(zone_ref->	zone))->n_refs;
}
OS_API_C_FUNC(void) dec_zone_ref(mem_zone_ref *zone_ref)
{
	mem_zone			*zone_ptr;
	if(zone_ref			==PTR_NULL)return;
	if(zone_ref			==uint_to_mem(0xDEF0DEF0))return;
	if(zone_ref->zone	==PTR_NULL)return;
	if(zone_ref->zone	==uint_to_mem(0xDEF0DEF0))return;
	zone_ptr=	zone_ref->	zone;
	if(zone_ptr->area_id==0xFFFF)return;


	if(fetch_add_c(&zone_ptr->n_refs,-1)==1)
	{
		if(zone_ptr->mem.ptr!=PTR_NULL)
		{
			if(zone_ptr->free_func!=PTR_NULL)
				zone_ptr->free_func(zone_ref);
			free_zone(zone_ref);
		}
	}

}

OS_API_C_FUNC(void) release_zone_ref(mem_zone_ref *zone_ref)
{
	dec_zone_ref(zone_ref);
	zone_ref->zone=PTR_NULL;
}

unsigned int inc_zone_ref(mem_zone_ref_ptr zone_ref)
{
	if(zone_ref->zone==PTR_NULL)return 0;
	if(zone_ref->zone==uint_to_mem(0xDEF0DEF0))return 0;
	if(fetch_add_c	(&((mem_zone *)(zone_ref->zone))->n_refs,1)==0)return 0;
	return 1;
	
}
OS_API_C_FUNC(void) copy_zone_ref(mem_zone_ref_ptr dest_zone_ref,const mem_zone_ref *zone_ref)
{
	if(zone_ref->zone==PTR_NULL)return;
	
	if(fetch_add_c				(&((mem_zone *)(zone_ref->zone))->n_refs,1)>=1)
	{
		release_zone_ref		(dest_zone_ref);
		dest_zone_ref->zone		=zone_ref->zone;
	}
	
}






void swap_zone_ref(mem_zone_ref_ptr dest_zone_ref,mem_zone_ref_ptr src_zone_ref)
{
	mem_zone *dst_zone	=	dest_zone_ref->zone;

	dest_zone_ref->zone	=  src_zone_ref->zone;
	src_zone_ref->zone	=  dst_zone;
	
	

	
}

OS_API_C_FUNC(unsigned int) create_zone_ref(mem_zone_ref *dest_zone_ref,mem_ptr ptr,mem_size size)
{
	mem_zone		*new_zone;

	if(n_mapped_zones>=32)
	{
		writestr("could not create more mapped zone \n");
	}

	new_zone			=&mapped_zones[n_mapped_zones];

	new_zone->area_id	=0xFFFF;
	new_zone->n_refs	=1;
	new_zone->mem.ptr	=ptr;
	new_zone->mem.size	=size;
	new_zone->free_func =PTR_NULL;
	dest_zone_ref->zone=new_zone;

	n_mapped_zones++;
	return 1;
}


OS_API_C_FUNC(unsigned int) allocate_new_empty_zone(unsigned int area_id,mem_zone_ref *zone_ref)
{
	unsigned int n,ret;
	mem_area	*area_ptr;

	area_ptr		=	get_area(area_id);
	if(area_ptr==PTR_NULL)
	{
		writestr("area [");
		writeint(area_id,16);
		writestr("] not found \n");
		return 0;
	}
	task_manager_aquire_semaphore	(area_ptr->lock_sema,0);
	
	release_zone_ref				(zone_ref);

	ret				=  0;
	n				=  0;
	while((n+1)<MAX_MEM_ZONES)
	{
		if(area_ptr->zones_buffer[n].mem.ptr == PTR_NULL)
		{
			mem_zone		*nzone;

			nzone			=	&area_ptr->zones_buffer[n];
			get_system_time_c	(&nzone->time);
			nzone->mem.ptr	=	uint_to_mem(0xFFFFFFFF);
			nzone->mem.size	=	0;
			nzone->area_id	=	area_ptr->area_id;
			nzone->n_refs	=	1;
			nzone->free_func=	PTR_NULL;
			zone_ref->zone	=	nzone;
			ret				=	1;
			break;
		}
		n++;
	}

	task_manager_release_semaphore(area_ptr->lock_sema,0);

	if(ret==0)
	{
		writestr("maximum zones allocated for this area (new empty zone) [");
		writeint(area_ptr->area_id,16);
		writestr("] (");
		writeint(MAX_MEM_ZONES,16);
		writestr(")\n");
		//dump_task_infos_c	();	
	}

		

	return ret;
}

extern unsigned int tree_output;
		

OS_API_C_FUNC(unsigned int) find_zones_used(unsigned int area_id)
{
	unsigned int n,nfree;
	mem_area	*area_ptr;

	area_ptr		=	get_area(area_id);
	if(area_ptr==PTR_NULL)
	{
		writestr("alloc area [");
		writeint(area_id,16);
		writestr("] not found \n");
		return 0;
	}

	nfree			=  0; 	
	n				=  0;
	while(n<MAX_MEM_ZONES)
	{
		if(area_ptr->zones_buffer[n].mem.ptr == PTR_NULL)
		{
			nfree++;
		}
		n++;
	}

	return nfree;
}


OS_API_C_FUNC(unsigned int) gfx_ctrl_new_row(struct obj_array_t *ctrl_data, const char *style, const char *cols)
{
	size_t pos, len;

	if (!tree_manager_create_obj_array(ctrl_data))return 0;


	tree_manager_add_obj(ctrl_data, "style_str", NODE_GFX_STR);
	tree_manager_add_obj_str_val(ctrl_data, "style", style);

	tree_manager_add_obj_array(ctrl_data, "column list", NODE_GFX_CTRL_DATA_COLUMN_LIST);

	pos = 0;
	len = strlen_c(cols);

	while (pos < len)
	{
		char				col_name[256];
		char				col_fld[256];
		char				col_style[256];
		size_t				next_pos, next_pos_x;



		tree_manager_add_obj(ctrl_data, "data_column", NODE_GFX_CTRL_DATA_COLUMN);

		next_pos = strlpos_c(cols, pos, ':');
		if (next_pos == INVALID_SIZE)break;

		strncpy_c(col_name, &cols[pos], next_pos - pos);
		pos = next_pos + 1;


		tree_manager_add_obj_str_val(ctrl_data, "label", col_name);

		next_pos = strlpos_c(cols, pos, ',');
		if (next_pos == INVALID_SIZE)next_pos = len;

		next_pos_x = strlpos_c(cols, pos, '{');

		if (next_pos_x == INVALID_SIZE)
			next_pos_x = next_pos;

		if (next_pos_x > next_pos)
			next_pos_x = next_pos;

		if (next_pos_x > pos)
		{
			strncpy_c(col_fld, &cols[pos], next_pos_x - pos);
		}

		if (next_pos_x < next_pos)
		{
			size_t next_pos_xx;

			next_pos_xx = strlpos_c(cols, pos, '}');
			if (next_pos_xx != INVALID_SIZE)
			{
				strcpy_c(col_style, "{(\"style\",0x04000020)");
				strncat_c(col_style, &cols[next_pos_x + 1], (next_pos_xx + 1) - (next_pos_x + 1));
				tree_manager_add_obj_str_val(ctrl_data, "style_str", col_style);
				next_pos = next_pos_xx + 1;
			}
		}

		pos = next_pos + 1;

		tree_manager_add_obj_str_val(ctrl_data, "field", col_fld);
	}

	tree_manager_end_obj_array(ctrl_data);
	tree_manager_add_obj_array(ctrl_data, "item_list", NODE_GFX_CTRL_ITEM_LIST);

	return 1;
}

OS_API_C_FUNC(unsigned int) gfx_ctrl_add_item(struct obj_array_t *ctrl_data, const char *name, const char *label, unsigned int item_id)
{
	tree_manager_add_obj(ctrl_data, name, NODE_GFX_CTRL_ITEM);
	tree_manager_add_obj_int_val(ctrl_data, "item_id", item_id);
	tree_manager_add_obj_str_val(ctrl_data, "label", label);

	return 1;
}


/*
int find_seg_free_zone		(const mem_area *area,mem_size size,mem_zone_desc	*desc)
{
	int						n;
	n	=	0;
	
	while(area->zones_free[n].ptr!= PTR_NULL)
	{
		mem_ptr	 start_free_zone;
		mem_ptr	 end_free_zone;
		mem_ptr	 start_aligned_free_zone;
		mem_size size_aligned_free_zone;

		start_free_zone			=	area->zones_free[n].ptr;
		end_free_zone			=	mem_add(area->zones_free[n].ptr,area->zones_free[n].size);
		start_aligned_free_zone	=	get_next_seg_aligned_ptr(start_free_zone);

		if((mem_add(start_aligned_free_zone,size)<end_free_zone)
		{
			size_aligned_free_zone	=	mem_sub	(start_aligned_free_zone,end_free_zone);
			desc->ptr				=	start_aligned_free_zone;
			desc->size				=	size;
			return 1;
		}
		n++;
		if(n>=MAX_MEM_ZONES)return 0;
	}
	desc->ptr	=	PTR_NULL;
	desc->size	=	0;

	return 0;
}
int allocate_seg_zone(mem_area *area,const mem_zone_desc *desc)
{
	int						n;
	mem_zone_desc_ptr		ret;
	
	n	=	0;
	ret	=	PTR_NULL;

	while(area->zones_free[n].ptr!=	PTR_NULL)
	{
		mem_ptr		start_aligned_free_zone,end_free_zone;
		mem_size	size_aligned_free_zone;

		end_free_zone			=	mem_add(area->zones_free[n].ptr,area->zones_free[n].size);

		start_aligned_free_zone	=	get_next_seg_aligned_ptr(area->zones_free[n].ptr);

		if((mem_add(start_aligned_free_zone,desc->size)<end_free_zone))
		{
			size_aligned_free_zone	=	mem_sub	(start_aligned_free_zone,end_free_zone);

			if(	(start_aligned_free_zone==desc->ptr)&&(size_aligned_free_zone>=desc->size))
			{
				mem_ptr					free_zone_end_ptr;
				mem_ptr					zone_end_ptr;
				mem_ptr					zone_end_aligned_ptr;

				free_zone_end_ptr		= 	mem_add	 (desc->ptr,area->zones_free[n].size);
				zone_end_ptr			=	mem_add	 (desc->ptr,desc->size);
				zone_end_aligned_ptr	=	get_next_aligned_ptr(zone_end_ptr);
				
				if(zone_end_aligned_ptr<free_zone_end_ptr)
				{
					area->zones_free[n].ptr		=  zone_end_aligned_ptr;
					area->zones_free[n].size	=  mem_sub	(zone_end_aligned_ptr,free_zone_end_ptr);
				}
				else
				{
					while(area->zones_free[n].ptr	!=	PTR_NULL)
					{
						area->zones_free[n].ptr		=area->zones_free[n+1].ptr;
						area->zones_free[n].size	=area->zones_free[n+1].size;
						n++;
					}
					area->zones_free[n].ptr		=PTR_NULL;
					area->zones_free[n].size	=0;
				}

				
				return 1;
			}
		}
		n++;
	}

	return 0;
}


OS_API_C_FUNC(unsigned int) allocate_new_zone_seg(unsigned int area_id,mem_size zone_size,mem_zone_ref *zone_ref)
{
	unsigned int n;
	mem_area	*area_ptr;

	area_ptr		=	get_area(area_id);
	if(area_ptr==PTR_NULL)
	{
		writestr("seg alloc area [");
		writeint(area_id,16);
		writestr("] not found \n");
		return 0;
	}

	task_manager_aquire_semaphore(area_ptr->lock_sema,0);

	release_zone_ref	(zone_ref);


	zone_size		=	((zone_size&0xFFFFFFF0)+16);
	n				=  0;
	while(n<MAX_MEM_ZONES)
	{
		if(area_ptr->zones_buffer[n].mem.ptr == PTR_NULL)
		{
			mem_zone		*nzone;

			nzone			=	&area_ptr->zones_buffer[n];
			if(find_seg_free_zone(area_ptr,zone_size,&nzone->mem)==1)
			{
				if(allocate_seg_zone(area_ptr,&nzone->mem)==1)
				{
					nzone->area_id	=	area_id;
					nzone->n_refs	=	0;
					nzone->free_func=	PTR_NULL;
					memset_c			(nzone->mem.ptr,0x00,nzone->mem.size);
					add_zone_ref	(nzone,zone_ref);

					task_manager_release_semaphore(area_ptr->lock_sema,0);
					return 1;

				}
				else
				{
					writestr("unable to seg allocate ");
					writesz(nzone->mem.size,16);
					writestr(" bytes at ");
					writeptr(nzone->mem.ptr);
					writestr(" \n");
					task_manager_release_semaphore(area_ptr->lock_sema,0);
					return 0;
				}
			}
			else
			{
				writestr("unable to get new seg free zone[");
				writeint(n,10);
				writestr("] ");
				writesz(zone_size,16);
				writestr(" bytes area ");
				writeint(area_id,10);
				writestr("\n");

				dump_task_infos_c	();
				snooze				(10000000);
				task_manager_release_semaphore(area_ptr->lock_sema,0);

				return 0;
			}
		}
		n++;
	}

	writestr("maximum zones allocated for this area seg [");
	writeint(area_ptr->area_id,16);
	writestr("] (");
	writeint(MAX_MEM_ZONES,16);
	writestr(")\n");
	

	task_manager_release_semaphore(area_ptr->lock_sema,0);
	return 0;
}
*/



OS_API_C_FUNC(unsigned int) allocate_new_zone(unsigned int area_id,mem_size zone_size,mem_zone_ref *zone_ref)
{
	unsigned int n;
	mem_area	*area_ptr;

	area_ptr		=	get_area(area_id);
	if(area_ptr==PTR_NULL)
	{
		writestr("alloc area [");
		writeint(area_id,16);
		writestr("] not found \n");
		return 0;
	}

	task_manager_aquire_semaphore(area_ptr->lock_sema,0);

	release_zone_ref	(zone_ref);


	zone_size		=	((zone_size&0xFFFFFFF0)+16);
	n				=  0;
	while(n<MAX_MEM_ZONES)
	{
		if(area_ptr->zones_buffer[n].mem.ptr == PTR_NULL)
		{
			mem_zone		*nzone;

			nzone			=	&area_ptr->zones_buffer[n];
			if(find_free_zone(area_ptr,zone_size,&nzone->mem)==1)
			{
				if(allocate_zone(area_ptr,&nzone->mem)==1)
				{
					get_system_time_c	(&nzone->time);
					nzone->area_id	=	area_ptr->area_id;
					nzone->n_refs	=	1;
					nzone->free_func=	PTR_NULL;
					memset_c			(nzone->mem.ptr,0x00,nzone->mem.size);
					zone_ref->	zone	=	nzone;	

					task_manager_release_semaphore(area_ptr->lock_sema,0);
					return 1;

				}
				else
				{
					task_manager_release_semaphore(area_ptr->lock_sema,0);

					writestr			("unable to allocate ");
					writesz				(nzone->mem.size,16);
					writestr			(" bytes at ");
					writeptr			(nzone->mem.ptr);
					writestr			(" \n");
					//dump_task_infos_c	();	
					

			
					return 0;
				}
			}
			else
			{

				task_manager_release_semaphore(area_ptr->lock_sema,0);


				writestr("unable to get new free zone[");
				writeint(n,10);
				writestr("] ");
				writesz(zone_size,16);
				writestr(" bytes area ");
				writeint(area_id,10);
				writestr("\n");

				/*
				dump_mem_used(area_ptr->area_id);

				dump_task_infos_c	();
				snooze				(10000000);
				*/
				

				return 0;
			}
		}
		n++;
	}

	task_manager_release_semaphore(area_ptr->lock_sema,0);

	writestr("maximum zones allocated for this area (new zone) [");
	writeint(area_ptr->area_id,16);
	writestr("] (");
	writeint(MAX_MEM_ZONES,16);
	writestr(")\n");
	

	
	return 0;
}




OS_API_C_FUNC(int) 	realloc_zone	(mem_zone_ref *zone_ref,mem_size new_size)
{
	unsigned int			n,cnt;
	mem_zone_desc			new_zone;
	mem_zone_desc			*mem;
	mem_area				*area_ptr;
	mem_zone				*src_zone;
	
	src_zone			=	zone_ref->zone;
	if(src_zone==PTR_NULL)return 0;

	area_ptr			=	get_area(src_zone->area_id);
	if(area_ptr==PTR_NULL)
	{
		writestr("realloc area [");
		writeint(src_zone->area_id,16);
		writestr("] not found \n");
		return 0;
	}

	task_manager_aquire_semaphore(area_ptr->lock_sema,0);

	if(new_size&0x0000000F)
		new_size	=	((new_size&0xFFFFFFF0)+16);
	
	mem					=	&src_zone->mem;


	if(mem->size>0)
	{
		mem_ptr					start_zone,end_zone,end_zone_aligned,new_end_zone;
		mem_ptr					new_end_zone_aligned;
		mem_zone_desc			new_free_zone;

		start_zone			=	mem->ptr;
		end_zone			=	mem_add(start_zone,mem->size);
		end_zone_aligned	=	get_next_aligned_ptr(end_zone);
		new_end_zone		=	mem_add(start_zone,new_size);
		new_end_zone_aligned=	get_next_aligned_ptr(new_end_zone);

		n					=  0;

		//try to find free zone contigous to the end of the current memory

		while(area_ptr->zones_free[n].ptr!=PTR_NULL)
		{
			mem_ptr		start_free_zone,end_free_zone;

			if(n>=MAX_MEM_ZONES)
			{
				writestr("error re alloc \n");

				task_manager_release_semaphore(area_ptr->lock_sema,0);
				return -1;
			}

			//current free zone to test
			start_free_zone	=	area_ptr->zones_free[n].ptr;
			end_free_zone	=	mem_add(start_free_zone,area_ptr->zones_free[n].size);
			

			if((end_zone_aligned==start_free_zone)&&(new_size<=area_ptr->zones_free[n].size))
			{
				//free zone big enought contigous to current memory block
				new_free_zone.ptr	=	new_end_zone_aligned;
				new_free_zone.size	=	mem_sub(new_free_zone.ptr,end_free_zone);

				if(new_free_zone.ptr>=end_free_zone)
				{
					//contigous free zone entirely consumed by the new alloc, remove it
					cnt=n;
					while(area_ptr->zones_free[cnt].ptr!=PTR_NULL)
					{
						area_ptr->zones_free[cnt]=area_ptr->zones_free[cnt+1];
						cnt++;
					}
				}
				else
				{
					//update the free zone to start at the end of the reallocated block
					area_ptr->zones_free[n]	= new_free_zone;
				}

				//reset newly allocated memory
				memset_c(end_zone,0,mem_sub(end_zone,new_end_zone));

				//return
				((mem_zone *)(zone_ref->zone))->mem.size = new_size;

				task_manager_release_semaphore(area_ptr->lock_sema,0);
				return 1;
			}
			n++;
		}
	}


	if(find_free_zone		(area_ptr,new_size,&new_zone)==0)
	{
		writestr("could not find new zone [");
		writeint(src_zone->area_id,16);
		writestr("]\n");

		task_manager_release_semaphore(area_ptr->lock_sema,0);
		return -1;
	}
	
	if(allocate_zone(area_ptr,&new_zone)==0)
	{
		writestr("could not allocate new zone [");
		writeint(src_zone->area_id,16);
		writestr("] not found \n");

		task_manager_release_semaphore(area_ptr->lock_sema,0);
		return -1;
	}
	
	if(new_zone.size>src_zone->mem.size)
	{
		mem_ptr		start_new;
		mem_size	size_new;
		
		start_new	=  mem_add(new_zone.ptr,src_zone->mem.size);
		size_new	= new_zone.size-src_zone->mem.size;
		memset_c	(start_new,0x00,size_new);
	}
	
	if(src_zone->mem.size>0)
		memcpy_c				(new_zone.ptr,get_zone_ptr(zone_ref,0),src_zone->mem.size);
	
	free_zone_area			(src_zone->area_id,&src_zone->mem);

	src_zone->mem.ptr		=new_zone.ptr;
	src_zone->mem.size		=new_zone.size;

	task_manager_release_semaphore(area_ptr->lock_sema,0);

	return 1;
}

OS_API_C_FUNC(int) expand_zone			(mem_zone_ref *ref,mem_size new_size)
{
	size_t ns;
	
	if(ref->zone==PTR_NULL)return 0;
	if(((mem_zone *)(ref->zone))->mem.size>=new_size)return 0;
	
	if(((mem_zone *)(ref->zone))->mem.size==0)
		ns		=	16;
	else
		ns		=	((mem_zone *)(ref->zone))->mem.size;

	while(ns<new_size)
		ns=ns*2;

	new_size=ns;

	return realloc_zone	(ref,new_size);
}


OS_API_C_FUNC(mem_ptr) malloc_c(mem_size sz)
{
	mem_zone_ref	ref;
	mem_ptr			m_ptr,ret_ptr;
/*
	writestr("malloc ");
	writeint(sz,10);
	writestr("\n");
*/
	ref.zone=PTR_NULL;
	
	if(allocate_new_zone(0x00,sz+16,&ref)!=1)
	{
		writestr("malloc error ");
		writesz(sz,10);
		writestr("\n");
		return PTR_NULL;
	}
	
	m_ptr	=	get_zone_ptr(&ref,0);

	*((unsigned int *)(m_ptr))=mem_to_uint(ref.zone);

	ret_ptr	=	mem_add(m_ptr,16);

	return ret_ptr;
}


OS_API_C_FUNC(mem_ptr) realloc_c(mem_ptr ptr,mem_size sz)
{
	mem_zone_ref	ref;
	mem_ptr			m_ptr,ret_ptr;

	m_ptr		=	mem_dec(ptr,16);
	ref.zone	=	uint_to_mem(*((unsigned int *)(m_ptr)));

	if(realloc_zone	(&ref,sz+16)!=1)
	{
		writestr("realloc error ");
		writesz(sz,10);
		writestr("\n");
		return PTR_NULL;
	}

	m_ptr		=	get_zone_ptr(&ref,0);

	*((unsigned int *)(m_ptr))=mem_to_uint(ref.zone);

	ret_ptr	=	mem_add(m_ptr,16);

	return ret_ptr;
}


OS_API_C_FUNC(void) free_c(mem_ptr ptr)
{
	
	mem_zone_ref	ref;
	mem_ptr			m_ptr;

	m_ptr		=	mem_dec(ptr,16);
	ref.zone	=	uint_to_mem(*((unsigned int *)(m_ptr)));
	release_zone_ref(&ref);
	
	

}

OS_API_C_FUNC(mem_ptr) calloc_c(mem_size sz,mem_size blk)
{
	return malloc_c(sz*blk);
}



