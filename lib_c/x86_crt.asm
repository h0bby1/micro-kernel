[BITS 32]

section .code

%ifdef PREFIX
	%macro declare_func 1
		GLOBAL %1
		export %1
		%1:
		
	%endmacro
%else
	%macro declare_func 1
	    %define %1 _%1
		GLOBAL _%1
		export _%1
		_%1:
		
	%endmacro
%endif

declare_func _allmul

;
;       AHI, BHI : upper 32 bits of A and B
;       ALO, BLO : lower 32 bits of A and B
;
;             ALO * BLO
;       ALO * BHI
; +     BLO * AHI
; ---------------------
;

        mov     eax,[esp + 8]
        mov     ecx,[esp + 16]
        or      ecx,eax         ;test for both hiwords zero.
        mov     ecx,[esp + 12]
        jnz     short hard      ;both are zero, just mult ALO and BLO

        mov     eax,[esp + 4]
        mul     ecx

        ret     16              ; callee restores the stack

hard:
        push    ebx

		; must redefine A and B since esp has been altered

        mul     ecx             ;eax has AHI, ecx has BLO, so AHI * BLO
        mov     ebx,eax         ;save result

        mov     eax,[esp + 8]
        mul     dword [esp + 20]  ;ALO * BHI
        add     ebx,eax         ;ebx = ((ALO * BHI) + (AHI * BLO))

        mov     eax,[esp + 8]  ;ecx = BLO
        mul     ecx             ;so edx:eax = ALO*BLO
        add     edx,ebx         ;now edx has all the LO*HI stuff

        pop     ebx

ret  16

declare_func _alldiv

        push    edi
        push    esi
        push    ebx

; Set up the local stack and save the index registers.  When this is done
; the stack frame will look as follows (assuming that the expression a/b will
; generate a call to lldiv(a, b)):
;
;               -----------------
;               |               |
;               |---------------|
;               |               |
;               |--divisor (b)--|
;               |               |
;               |---------------|
;               |               |
;               |--dividend (a)-|
;               |               |
;               |---------------|
;               | return addr** |
;               |---------------|
;               |      EDI      |
;               |---------------|
;               |      ESI      |
;               |---------------|
;       ESP---->|      EBX      |
;               -----------------
;

;DVND    equ     [esp + 16]      ; stack address of dividend (a)
;DVSR    equ     [esp + 24]      ; stack address of divisor (b)


; Determine sign of the result (edi = 0 if result is positive, non-zero
; otherwise) and make operands positive.

        xor     edi,edi         ; result sign assumed positive

        mov     eax,[esp + 20]  ; hi word of a
        or      eax,eax         ; test to see if signed
        jge     short L1        ; skip rest if a is already positive
        inc     edi             ; complement result sign flag
        mov     edx,[esp + 16]  ; lo word of a
        neg     eax             ; make a positive
        neg     edx
        sbb     eax,0
        mov     [esp + 20],eax ; save positive value
        mov     [esp + 16],edx
L1:
        mov     eax,[esp + 28] ; hi word of b
        or      eax,eax         ; test to see if signed
        jge     short L2        ; skip rest if b is already positive
        inc     edi             ; complement the result sign flag
        mov     edx,[esp + 24] ; lo word of a
        neg     eax             ; make b positive
        neg     edx
        sbb     eax,0
        mov     [esp + 28],eax ; save positive value
        mov     [esp + 24],edx
L2:

;
; Now do the divide.  First look to see if the divisor is less than 4194304K.
; If so, then we can use a simple algorithm with word divides, otherwise
; things get a little more complex.
;
; NOTE - eax currently contains the high order word of DVSR
;

        or      eax,eax         ; check to see if divisor < 4194304K
        jnz     short L3        ; nope, gotta do this the hard way
        mov     ecx,[esp + 24] ; load divisor
        mov     eax, [esp + 20] ; load high word of dividend
        xor     edx,edx
        div     ecx             ; eax <- high order bits of quotient
        mov     ebx,eax         ; save high bits of quotient
        mov     eax, [esp + 16] ; edx:eax <- remainder:lo word of dividend
        div     ecx             ; eax <- low order bits of quotient
        mov     edx,ebx         ; edx:eax <- quotient
        jmp     short L4        ; set sign, restore stack and return

;
; Here we do it the hard way.  Remember, eax contains the high word of DVSR
;

L3:
        mov     ebx,eax         ; ebx:ecx <- divisor
        mov     ecx,[esp + 24]
        mov     edx, [esp + 20] ; edx:eax <- dividend
        mov     eax, [esp + 16]
L5:
        shr     ebx,1           ; shift divisor right one bit
        rcr     ecx,1
        shr     edx,1           ; shift dividend right one bit
        rcr     eax,1
        or      ebx,ebx
        jnz     short L5        ; loop until divisor < 4194304K
        div     ecx             ; now divide, ignore remainder
        mov     esi,eax         ; save quotient

;
; We may be off by one, so to check, we will multiply the quotient
; by the divisor and check the result against the orignal dividend
; Note that we must also check for overflow, which can occur if the
; dividend is close to 2**64 and the quotient is off by 1.
;

        mul     dword [esp + 28] ; QUOT * HIWORD(DVSR)
        mov     ecx,eax
        mov     eax,[esp + 24]
        mul     esi             ; QUOT * LOWORD(DVSR)
        add     edx,ecx         ; EDX:EAX = QUOT * DVSR
        jc      short L6        ; carry means Quotient is off by 1

;
; do long compare here between original dividend and the result of the
; multiply in edx:eax.  If original is larger or equal, we are ok, otherwise
; subtract one (1) from the quotient.
;

        cmp     edx, [esp + 20] ; compare hi words of result and original
        ja      short L6        ; if result > original, do subtract
        jb      short L7        ; if result < original, we are ok
        cmp     eax, [esp + 16] ; hi words are equal, compare lo words
        jbe     short L7        ; if less or equal we are ok, else subtract
L6:
        dec     esi             ; subtract 1 from quotient
L7:
        xor     edx,edx         ; edx:eax <- quotient
        mov     eax,esi

;
; Just the cleanup left to do.  edx:eax contains the quotient.  Set the sign
; according to the save value, cleanup the stack, and return.
;

L4:
        dec     edi             ; check to see if result is negative
        jnz     short L8        ; if EDI == 0, result should be negative
        neg     edx             ; otherwise, negate the result
        neg     eax
        sbb     edx,0

;
; Restore the saved registers and return.
;

L8:
        pop     ebx
        pop     esi
        pop     edi

ret  16  



declare_func _allshr

;
; Handle shifts of 64 bits or more (if shifting 64 bits or more, the result
; depends only on the high order bit of edx).
;
        cmp     cl,64
        jae     short RETSIGN

;
; Handle shifts of between 0 and 31 bits
;
        cmp     cl, 32
        jae     short MORE32
        shrd    eax,edx,cl
        sar     edx,cl
        ret

;
; Handle shifts of between 32 and 63 bits
;
MORE32:
        mov     eax,edx
        sar     edx,31
        and     cl,31
        sar     eax,cl
        ret

;
; Return double precision 0 or -1, depending on the sign of edx
;
RETSIGN:
        sar     edx,31
        mov     eax,edx
ret 16


declare_func _allshl

;
; Handle shifts of 64 or more bits (all get 0)
;
        cmp     cl, 64
        jae     short RETZERO_l

;
; Handle shifts of between 0 and 31 bits
;
        cmp     cl, 32
        jae     short MORE32_l
        shld    edx,eax,cl
        shl     eax,cl
        ret

;
; Handle shifts of between 32 and 63 bits
;
MORE32_l:
        mov     edx,eax
        xor     eax,eax
        and     cl,31
        shl     edx,cl
        ret

;
; return 0 in edx:eax
;
RETZERO_l:
        xor     eax,eax
        xor     edx,edx
ret 16



  
declare_func _aullshr 

;
; Handle shifts of 64 bits or more (if shifting 64 bits or more, the result
; depends only on the high order bit of edx).
;
        cmp     cl,64
        jae     short RETZERO

;
; Handle shifts of between 0 and 31 bits
;
        cmp     cl, 32
        jae     short MORE32_2
        shrd    eax,edx,cl
        shr     edx,cl
        ret

;
; Handle shifts of between 32 and 63 bits
;
MORE32_2:
        mov     eax,edx
        xor     edx,edx
        and     cl,31
        shr     eax,cl
        ret

;
; return 0 in edx:eax
;
RETZERO:
        xor     eax,eax
        xor     edx,edx
        ret

%if 0
declare_func  _intel_fast_memcpy
  push        ebp
  mov         ebp,esp
  sub         esp,8
  mov         dword  [ebp-4],edi
  mov         dword  [ebp-8],esi
  mov         esi,dword  [ebp+0Ch]
  mov         edi,dword  [ebp+8]
  mov         ecx,dword  [ebp+10h]
  rep movsb    ;byte [edi],byte  [esi]                                                                        ;fast?
  mov         eax,dword  [ebp+8]
  mov         esi,dword  [ebp-8]
  mov         edi,dword  [ebp-4]
  mov         esp,ebp
  pop         ebp
ret
%endif
  

declare_func _ftol2_sse
declare_func _ftol2

push        ebp  
mov         ebp,esp 
sub         esp,20h 
and         esp,0xFFFFFFFFFFFFFFF0
fld         st0 
fst         dword [esp+18h] 
fistp       qword [esp+10h] 
fild        qword [esp+10h] 
mov         edx,dword [esp+18h] 
mov         eax,dword [esp+10h] 
test        eax,eax 
je          integer_QnaN_or_zero
arg_is_not_integer_QnaN:
fsubp       st1,st0 
test        edx,edx 
jns         positive
fstp        dword [esp] 
mov         ecx,dword [esp] 
xor         ecx,80000000h 
add         ecx,7FFFFFFFh 
adc         eax,0 
mov         edx,dword [esp+14h] 
adc         edx,0 
jmp         localexit
positive:
fstp        dword [esp] 
mov         ecx,dword [esp] 
add         ecx,7FFFFFFFh 
sbb         eax,0 
mov         edx,dword [esp+14h] 
sbb         edx,0 
jmp         localexit
integer_QnaN_or_zero:
mov         edx,dword [esp+14h] 
test        edx,7FFFFFFFh 
jne         arg_is_not_integer_QnaN
fstp        dword [esp+18h] 
fstp        dword [esp+18h] 
localexit:
leave            
ret              


declare_func mask_interupts
	
	test dword [esp+4],1
	jz not_mask_interupts
	cli
		
	jmp end_mask_interupts
	not_mask_interupts:
	sti
	end_mask_interupts:
ret 