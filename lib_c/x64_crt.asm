[BITS 64]

section .code


%macro declare_func 1
	GLOBAL %1
	export %1
	%1:
%endmacro


declare_func mask_interupts
	
	test dword[esp+4],1
	jz not_mask_interupts
	cli
		
	jmp end_mask_interupts
	not_mask_interupts:
	sti
	end_mask_interupts:
ret 