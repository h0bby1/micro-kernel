#define LIBC_API C_EXPORT
#include <std_def.h>
#include <kern.h>
#include <std_mem.h>
#include <mem_base.h>


#include "IceTypes.h"


	
udword*			mHistogram;					//!< Counters for each byte
udword*			mOffset;					//!< Offsets (nearly a cumulative distribution function)
udword			mCurrentSize;				//!< Current size of the indices list
udword			mPreviousSize;				//!< Size involved in previous call
udword*			mIndices;					//!< Two lists, swapped each pass
udword*			mIndices2;
// Stats
udword			mTotalCalls;
udword			mNbHits;


#define DELETEARRAY(x)	{ if (x != null) free_c(x);x = null; }
#define CHECKALLOC(x)	if(!x) return 0;

#define CREATE_HISTOGRAMS(type, buffer)															\ /* Clear counters */
	

#define CHECK_PASS_VALIDITY(pass)																\
	/* Shortcut to current counters */															\
	udword* CurCount = &mHistogram[pass<<8];													\
																								\
	/* Reset flag. The sorting pass is supposed to be performed. (default) */					\
	int PerformPass = 1;																	\
																								\
	/* Check pass validity */																	\
																								\
	/* If all values have the same byte, sorting is useless. */									\
	/* It may happen when sorting bytes or words instead of dwords. */							\
	/* This routine actually sorts words faster than dwords, and bytes */						\
	/* faster than words. Standard running time (O(4*n))is reduced to O(2*n) */					\
	/* for words and O(n) for bytes. Running time for floats depends on actual values... */		\
																								\
	/* Get first byte */																		\
	ubyte UniqueVal = *(((ubyte*)input)+pass);													\
																								\
	/* Check that byte's counter */																\
	if(CurCount[UniqueVal]==nb)	PerformPass=0;




void ResetIndices()
{
	udword i;
	for(i=0;i<mCurrentSize;i++)	mIndices[i] = i;
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 *	Resizes the inner lists.
 *	\param		nb				[in] new size (number of dwords)
 *	\return		true if success
 */
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int Resize(udword nb)
{
	// Free previously used ram
	DELETEARRAY(mIndices2);
	DELETEARRAY(mIndices);

	// Get some fresh one
	mIndices		= malloc_c(sizeof(udword)*nb);	CHECKALLOC(mIndices);
	mIndices2		= malloc_c(sizeof(udword)*nb);	CHECKALLOC(mIndices2);
	mCurrentSize	= nb;

	// Initialize indices so that the input buffer is read in sequential order
	ResetIndices();

	return 1;
}

#define CHECK_RESIZE(n)																			\
	if(n!=mPreviousSize)																		\
	{																							\
				if(n>mCurrentSize)	Resize(n);													\
		else						ResetIndices();												\
		mPreviousSize = n;																		\
	}


int ice_init()
{
	mCurrentSize	=0;
	mPreviousSize	=0;
	mTotalCalls		=0;
	mNbHits			=0;
	mHistogram		= malloc_c(sizeof(unsigned int)*256*4);
	mOffset			= malloc_c(sizeof(unsigned int)*256*4);
	ResetIndices		();

	return 1;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 *	Main sort routine.
 *	This one is for integer values. After the call, mIndices contains a list of indices in sorted order, i.e. in the order you may process your data.
 *	\param		input			[in] a list of integer values to sort
 *	\param		nb				[in] number of values to sort
 *	\param		signedvalues	[in] true to handle negative values, false if you know your input buffer only contains positive values
 *	\return		Self-Reference
 */
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int ice_sort(const udword* input, udword nb, int signedvalues)
{
	udword NbNegativeValues ;
	udword j;
	int AlreadySorted ;

	// Checkings
	if(!input || !nb)	return 0;

	// Stats
	mTotalCalls++;

	// Resize lists if needed
	CHECK_RESIZE(nb);

#ifdef RADIX_LOCAL_RAM
	// Allocate histograms & offsets on the stack
	udword mHistogram[256*4];
	udword mOffset[256];
#endif

	// Create histograms (counters). Counters for all passes are created in one run.
	// Pros:	read input buffer once instead of four times
	// Cons:	mHistogram is 4Kb instead of 1Kb
	// We must take care of signed/unsigned values for temporal coherence.... I just
	// have 2 code paths even if just a single opcode changes. Self-modifying code, someone?
	/*
	if(!signedvalues)	
	{ 
		udword PrevVal;
		CREATE_HISTOGRAMS(udword, input);	
	}
	else				
	{ 
		sdword PrevVal;
		CREATE_HISTOGRAMS(sdword, input);	
	}
	*/
	{
		ubyte* p;
		ubyte* pe; 
		udword* h0;
		udword* h1;
		udword* h2;
		udword* h3;
		udword* Indices;
		sdword PrevVal;

		memset_c(mHistogram, 0,256*4*sizeof(udword));										
																							
		/* Prepare for temporal coherence */												
		PrevVal			= (sdword)input[mIndices[0]];												
		AlreadySorted	= 1;	/* Optimism... */												
		Indices = mIndices;															
																							
		/* Prepare to count */																
		p = (ubyte*)input;																	
		pe = &p[nb*4];																		
		 h0= &mHistogram[0];		/* Histogram for first pass (LSB)	*/					
		 h1= &mHistogram[256];	/* Histogram for second pass		*/						
		 h2= &mHistogram[512];	/* Histogram for third pass			*/						
		 h3= &mHistogram[768];	/* Histogram for last pass (MSB)	*/						
																							
		while(p!=pe)																		
		{																					
			/* Read input buffer in previous sorted order */								
			sdword Val = (sdword)input[*Indices++];											
			/* Check whether already sorted or not */										
			if(Val<PrevVal)	{ AlreadySorted = 0; break; } /* Early out */				
			/* Update for next iteration */													
			PrevVal = Val;																	
																							
			/* Create histograms */															
			h0[*p++]++;	h1[*p++]++;	h2[*p++]++;	h3[*p++]++;									
		}																					
																							
		/* If all input values are already sorted, we just have to return and leave the */	
		/* previous list unchanged. That way the routine may take advantage of temporal */	
		/* coherence, for example when used to sort transparent faces.					*/	
		if(AlreadySorted)	{ mNbHits++; return 1;	}									
																							
		/* Else there has been an early out and we must finish computing the histograms */	
		while(p!=pe)																		
		{																					
			/* Create histograms without the previous overhead */							
			h0[*p++]++;	h1[*p++]++;	h2[*p++]++;	h3[*p++]++;									
		} 
	}








	// Compute #negative values involved if needed
	NbNegativeValues = 0;
	if(signedvalues)
	{
		// An efficient way to compute the number of negatives values we'll have to deal with is simply to sum the 128
		// last values of the last histogram. Last histogram because that's the one for the Most Significant Byte,
		// responsible for the sign. 128 last values because the 128 first ones are related to positive numbers.
		udword* h3= &mHistogram[768];
		udword i;
		for(i=128;i<256;i++)	NbNegativeValues += h3[i];	// 768 for last histogram, 128 for negative part
	}

	// Radix sort, j is the pass number (0=LSB, 3=MSB)
	for(j=0;j<4;j++)
	{

		ubyte* InputBytes;
		udword* Indices	;	
		udword*	IndicesEnd;

		CHECK_PASS_VALIDITY(j);

		// Sometimes the fourth (negative) pass is skipped because all numbers are negative and the MSB is 0xFF (for example). This is
		// not a problem, numbers are correctly sorted anyway.
		if(PerformPass)
		{
			udword* Tmp;
			// Should we care about negative values?
			if(j!=3 || !signedvalues)
			{
				udword i;
				// Here we deal with positive values only

				// Create offsets
				mOffset[0] = 0;
				for(i=1;i<256;i++)		mOffset[i] = mOffset[i-1] + CurCount[i-1];
			}
			else
			{
				udword i;
				// This is a special case to correctly handle negative integers. They're sorted in the right order but at the wrong place.

				// Create biased offsets, in order for negative numbers to be sorted as well
				mOffset[0] = NbNegativeValues;												// First positive number takes place after the negative ones
				for(i=1;i<128;i++)		mOffset[i] = mOffset[i-1] + CurCount[i-1];	// 1 to 128 for positive numbers

				// Fixing the wrong place for negative values
				mOffset[128] = 0;
				for(i=129;i<256;i++)			mOffset[i] = mOffset[i-1] + CurCount[i-1];
			}

			// Perform Radix Sort
			InputBytes	= (ubyte*)input;
			Indices		= mIndices;
			IndicesEnd	= &mIndices[nb];
			InputBytes += j;
			while(Indices!=IndicesEnd)
			{
				udword id = *Indices++;
				mIndices2[mOffset[InputBytes[id<<2]]++] = id;
			}

			// Swap pointers for next pass. Valid indices - the most recent ones - are in mIndices after the swap.
			Tmp	= mIndices;	mIndices = mIndices2; mIndices2 = Tmp;
		}
	}
	return 1;
}