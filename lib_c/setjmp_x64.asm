[BITS 64]

%ifdef PREFIX
	GLOBAL setjmp_c
	GLOBAL longjmp_c
%else

	GLOBAL _setjmp_c
	GLOBAL _longjmp_c

	export _setjmp_c
	export _longjmp_c
%endif

%define OFS_EBP   0
%define OFS_EBX   4
%define OFS_EDI   8
%define OFS_ESI   12
%define OFS_ESP   16
%define OFS_EIP   20

section .code

%ifdef PREFIX
setjmp_c:
%else
_setjmp_c:
%endif
    mov edx, [esp+4]         ; Get jmp_buf pointer
    mov eax, [esp]           ; Save EIP
    mov [edx+OFS_EIP], eax
    mov [edx+OFS_EBP], ebp    ; Save EBP, EBX, EDI, ESI, and ESP
    mov [edx+OFS_EBX], ebx
    mov [edx+OFS_EDI], edi
    mov [edx+OFS_ESI], esi
    mov [edx+OFS_ESP], esp
    xor eax, eax             ; Return 0
 ret

%ifdef PREFIX
	longjmp_c:
%else
	_longjmp_c:
%endif  

    mov edx, [esp+4]          ; Get jmp_buf pointer
    mov eax, [esp+8]          ; Get return value (eax)

    mov esp, [edx+OFS_ESP]    ; Switch to new stack position
    mov ebx, [edx+OFS_EIP]    ; Get new EIP value and set as return address
    mov [esp], ebx
    
    mov ebp, [edx+OFS_EBP]    ; Restore EBP, EBX, EDI, and ESI
    mov ebx, [edx+OFS_EBX]
    mov edi, [edx+OFS_EDI]
    mov esi, [edx+OFS_ESI]

ret
