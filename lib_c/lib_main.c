#define LIBC_API C_EXPORT
#include <std_def.h>
#include <kern.h>
#include <std_mem.h>
#include <mem_base.h>
#include <lib_c.h>
#include <tree.h>
#include <sys/task.h>


extern	void	init_kernel_log	();

#define INTERUPT_BUFFER_SIZE 4*1024*1024

C_EXPORT mod_name_decoration_t	 mod_name_deco_type	=	MOD_NAME_DECO;
C_EXPORT unsigned int __stdcall _DllMainCRTStartup(int *_a,int *_b,int *_c){return 1;}
C_EXPORT int					_fltused		=	1;

/*
unsigned int		interupt_area_id			=	0xFFFFFFFF;
unsigned int		saved_default_mem_area_id	=	0xFFFFFFFF;
*/



extern volatile unsigned int task_list_last_id;
extern unsigned int default_mem_area_id;
extern unsigned int	tree_mem_area_id;
extern mem_ptr		write_sema;
/*
C_EXPORT OS_INT_C_FUNC(unsigned int) interupt_start_callback(unsigned int int_id)
{
	writestr_fmt			("interupt %d called \n",int_id);
	snooze					(5000000);
	set_default_mem_area	(interupt_area_id);
	return default_mem_area_id;
}

C_EXPORT OS_INT_C_FUNC(unsigned int) interupt_end_callback(unsigned int int_id)
{
	writestr_fmt			("interupt %d end \n",int_id);
	set_default_mem_area	(saved_default_mem_area_id);
	return default_mem_area_id;
}
*/



OS_API_C_FUNC(unsigned int) libc_init()
{
	mem_zone_ref	text_output_buffer			=	{PTR_NULL};
	mem_zone_ref	text_output_len_buffer		=	{PTR_NULL};

	writestr					("init base system\n");
	init_mem_system				();
	init_default_mem_area		(1024*1024*32);

	allocate_new_zone			(0x00		,4*1024*1024,&text_output_buffer);
	allocate_new_zone			(0x00		,1024*1024,&text_output_len_buffer);
	set_output_buffer_addr_c	(get_zone_ptr(&text_output_buffer,0),get_zone_ptr(&text_output_len_buffer,0));

	init_kernel_log				();
	tree_manager_init			();
	init_task_manager			();

	//write_sema			=	task_manager_new_semaphore	(1,16);
	mem_area_enable_sem		(default_mem_area_id);
	mem_area_enable_sem		(tree_mem_area_id);

	
	return 0;
}
