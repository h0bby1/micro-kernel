#define LIBC_API C_EXPORT

#include <std_def.h>
#include <std_mem.h>
#include "mem_base.h"
#include "lib_c.h"

typedef union {
	unsigned short val;
	unsigned char bytes[2];
} un16;

typedef union {
	unsigned int val;
	unsigned char bytes[4];
} un32;


/* 16-bit */
/*
OS_API_C_FUNC(unsigned short) htobe16(unsigned short val)
{
	un16 un;

	un.bytes[1] = val & 0xff;
	un.bytes[0] = (val >> 8) & 0xff;

	return un.val;
}


OS_API_C_FUNC(unsigned short) htole16(unsigned short val)
{
	un16 un;

	un.bytes[0] = val & 0xff;
	un.bytes[1] = (val >> 8) & 0xff;

	return un.val;
}





OS_API_C_FUNC(unsigned short) le16toh(unsigned short val)
{
	un16 un;

	un.val = val;

	return ((un.bytes[1] << 8) |
	         un.bytes[0]);
}
*/

OS_API_C_FUNC(unsigned short) be16toh(unsigned short val)
{
	un16 un;

	un.val = val;

	return ((un.bytes[0] << 8) |
		un.bytes[1]);
}

/* 32-bit */


OS_API_C_FUNC(unsigned int) htobe32(unsigned int val)
{
	un32 un;

	un.bytes[3] = val & 0xff;
	un.bytes[2] = (val >> 8) & 0xff;
	un.bytes[1] = (val >> 16) & 0xff;
	un.bytes[0] = (val >> 24) & 0xff;

	return un.val;
}

OS_API_C_FUNC(void ) htobe24(unsigned int val,unsigned char *dst)
{
	dst[2] = val & 0xff;
	dst[1] = (val >> 8) & 0xff;
	dst[0] = (val >> 16) & 0xff;
}

OS_API_C_FUNC(unsigned int) htole32(unsigned int val)
{
	un32 un;

	un.bytes[0] = val & 0xff;
	un.bytes[1] = (val >> 8) & 0xff;
	un.bytes[2] = (val >> 16) & 0xff;
	un.bytes[3] = (val >> 24) & 0xff;

	return un.val;
}

/*
OS_API_C_FUNC(unsigned int) be32toh(unsigned int val)
{
	un32 un;

	un.val = val;

	return ((un.bytes[0] << 24) |
	        (un.bytes[1] << 16) |
	        (un.bytes[2] << 8) |
	         un.bytes[3]);
}
*/

OS_API_C_FUNC(unsigned int) le32toh(unsigned int val)
{
	un32 un;

	un.val = val;

	return ((un.bytes[3] << 24) |
	        (un.bytes[2] << 16) |
	        (un.bytes[1] << 8) |
	         un.bytes[0]);
}
