[BITS 32]

section .code


%ifdef PREFIX
	%macro declare_func 1

		GLOBAL %1
		%1:

	%endmacro

	%macro declare_efunc 1

		GLOBAL %1
		%1:

	%endmacro

%else

	%macro declare_func 1

		%define %1 _%1
		GLOBAL _%1
		_%1:
	%endmacro

	%macro declare_efunc 1
		%define %1 _%1
		EXPORT _%1
		GLOBAL _%1
		_%1:
	%endmacro


%endif



declare_func fabsf
	fld dword[esp+4]
	fabs
ret

declare_func fabsd
	fld qword[esp+4]
	fabs
ret

declare_func sqrtf
	fld dword [esp+4]
	fsqrt
ret

declare_func atanf
    fld1
	fld dword [esp+4]
	fatan 
ret

declare_func cosf
	fld dword [esp+4]
	fcos
ret

declare_func sinf
	fld dword [esp+4]
	fsin
ret



declare_efunc ftoi
	fld		DWORD [esp+4]
	fistp	dword [esp+4]
	mov     eax,[esp+4]
ret

declare_efunc isinf

	fld		DWORD [esp+4]
	fsin
	fmul	DWORD [esp+8]	
	fistp	dword [esp+4]
	mov     eax,[esp+4]
ret

declare_func sind
	fld qword [esp+4]
	fsin
ret


declare_func acosf
	fld dword [esp+4]
	facos
ret

declare_func asinf
	fld dword [esp+4]
	fasin
ret




declare_func exp_
   push    ebp
   mov     ebp,esp
   sub     esp,8                   ; Allocate temporary space
   fld     qword [ebp+8]       ; Load real from stack
   fldl2e                          ; Load log base 2(e)
   fmulp   st1,st0                ; Multiply x * log base 2(e)
   fst     st1                   ; Push result
   frndint                         ; Round to integer
   fsub    st1,st0                ; Subtract
   fxch                            ; Exchange st, st(1)
   f2xm1                           ; Compute 2 to the (x - 1)
   fld1                            ; Load real number 1
   fadd    st0,st1                 ; 2 to the x
   fscale                          ; Scale by power of 2
   fstp    st1                     ; Set new stack top and pop
   fst     qword [ebp-8]           ; Throw away scale factor
   mov     esp,ebp                 ; Deallocate temporary space
   pop     ebp
ret
  
declare_func ldexp
    push    ebp
	mov     ebp,esp
    sub     esp,8               ; Allocate temporary space
    fild    dword [ebp+16]      ; Load n as integer
    fld     qword [ebp+8]       ; Load real from stack
    fscale                      ; Compute 2 to the n
    fstp    st1                 ; Set new top of stack
    fst     qword [ebp-8]       ; Store result
    mov     esp,ebp             ; Deallocate temporary space
    pop     ebp
ret



;-----------------------------------------------------------------------------
declare_func ceild
                
	push    ebp
    mov     ebp,esp
    sub     esp,4                   ; Allocate temporary space
    fld     qword [ebp+8]		    ; Load real from stack
    fstcw   [ebp-2]                 ; Save control word
    fclex                           ; Clear exceptions
    mov     word [ebp-4],0b63h  ; Rounding control word
    fldcw   [ebp-4]                 ; Set new rounding control
    frndint                         ; Round to integer
    fclex                           ; Clear exceptions
    fldcw   [ebp-2]                 ; Restore control word
    mov     esp,ebp                 ; Deallocate temporary space
    pop     ebp
ret


;-----------------------------------------------------------------------------
declare_func frexp
	push    ebp
    mov     ebp,esp
    push    edi                     ; Save register edi
    fld     qword [ebp+8]       ; Load real from stack
    mov     edi,dword [ebp+16]  ; Put exponent address in edi
    ftst                            ; Test st for zero
    fstsw   ax                      ; Put test result in ax
    ; Set flags based on test
    jnz     __frexp1                ; Re-direct if not zero
    st                      ; Set exponent to zero
    jmp     __frexp2                ; End of case
__frexp1:       
	fxtract                         ; Get exponent and significand
    fld1                            ; Load constant 1
    fld1                            ; Load constant 1
    fadd st0,st1                           ; Constant 2
    fdiv st0,st1                           ; Significand / 2
    fxch                            ; Swap st, st(1)
    fld1                            ; Load constant 1
    fadd  st0,st1                          ; Increment exponent
    fistp   dword [edi]         ; Store result exponent and pop
__frexp2:   
    pop     edi                     ; Restore register edi
    mov     esp,ebp
    pop     ebp
ret


                
declare_func floord
    push    ebp
    mov     ebp,esp
    sub     esp,4                   ; Allocate temporary space
    fld     qword [ebp+8]           ; Load real from stack
    fstcw   [ebp-2]                 ; Save control word
    fclex                           ; Clear exceptions
    mov     word [ebp-4],0763h      ; Rounding control word
    fldcw   [ebp-4]                 ; Set new rounding control
    frndint                         ; Round to integer
    fclex                           ; Clear exceptions
    fldcw   [ebp-2]                 ; Restore control word
    mov     esp,ebp
    pop     ebp
ret

fzero:dd 0.0                   ; Floating point zero

declare_func powf
	push    ebp
    mov     ebp,esp
    sub     esp,12                  ; Allocate temporary space
    push    edi                     ; Save register edi
    push    eax                     ; Save register eax
    mov     dword [ebp-12],0    ; Set negation flag to zero
    fld     qword [ebp+16]      ; Load real from stack
    fld     qword [ebp+8]       ; Load real from stack
    mov     edi,fzero		; Point to real zero
    fcom    qword [edi]         ; Compare x with zero
    fstsw   ax                      ; Get the FPU status word
    mov     al,ah                   ; Move condition flags to AL
    lahf                            ; Load Flags into AH
    and     al,    01000101B        ; Isolate  C0, C2 and C3
    and     ah,   ~01000101B        ; Turn off CF, PF and ZF
    or      ah,al                   ; Set new  CF, PF and ZF
    sahf                            ; Store AH into Flags
    jb      __fpow1                 ; Re-direct if x < 0
    ja      __fpow3                 ; Re-direct if x > 0
    fxch                            ; Swap st, st(1)
    fcom    qword [edi]         ; Compare y with zero
    fxch                            ; Restore x as top of stack
    fstsw   ax                      ; Get the FPU status word
    mov     al,ah                   ; Move condition flags to AL
    lahf                            ; Load Flags into AH
    and     al,    01000101B        ; Isolate  C0, C2 and C3
    and     ah,   ~01000101B        ; Turn off CF, PF and ZF
    or      ah,al                   ; Set new  CF, PF and ZF
    sahf                            ; Store AH into Flags
    ja      __fpow3                 ; Re-direct if y > 0
    fstp    st1                   ; Set new stack top and pop
    mov     eax,1                   ; Set domain error (EDOM)
    jmp     __fpow5                 ; End of case
__fpow1:
    fxch                            ; Put y on top of stack
	fld    st0                       ; Duplicate y as st(1)
    frndint                         ; Round to integer
    fxch                            ; Put y on top of stack
    fcomp   st0,st1                  ; y = int(y) ?
    fstsw   ax                      ; Get the FPU status word
    mov     al,ah                   ; Move condition flags to AL
    lahf                            ; Load Flags into AH
    and     al,    01000101B        ; Isolate  C0, C2 and C3
    and     ah,   ~01000101B        ; Turn off CF, PF and ZF
    or      ah,al                   ; Set new  CF, PF and ZF
    sahf                            ; Store AH into Flags
    je      __fpow2                 ; Proceed if y = int(y)
    fstp    st1                   ; Set new stack top and pop
    fldz                            ; Set result to zero
	fstp    st1                   ; Set new stack top and pop
	mov     eax,1                   ; Set domain error (EDOM)
    jmp     __fpow5                 ; End of case
__fpow2:
    fist    dword [ebp-12]      ; Store y as integer
    and     dword [ebp-12],1    ; Set bit if y is odd
    fxch                            ; Put x on top of stack
    fabs                            ; x = |x|
__fpow3:        
	fldln2                          ; Load log base e of 2
    fxch    st1                    ; Exchange st, st(1)
    fyl2x                   ; Compute the natural log(x)
    fmul    st0,st1                   ; Compute y * ln(x)
    fldl2e                          ; Load log base 2(e)
    fmulp   st1,st0                ; Multiply x * log base 2(e)
    fst     st1                   ; Push result
	frndint                         ; Round to integer
    fsub    st1,st0                ; Subtract
	fxch                            ; Exchange st, st(1)
    f2xm1                           ; Compute 2 to the (x - 1)
    fld1                            ; Load real number 1
    fadd   st0,st1                  ; 2 to the x
    fscale                          ; Scale by power of 2
    fstp    st1                   ; Set new stack top and pop
	test    dword [ebp-12],1    ; Negation required ?
    jz      __fpow4                 ; No, re-direct
	fchs                            ; Negate the result
__fpow4:
    fstp    qword [ebp-8]       ; Save (double)pow(x, y)
    fld     qword [ebp-8]       ; Load (double)pow(x, y)
	fxam                            ; Examine st
    fstsw   ax                      ; Get the FPU status word
    cmp     ah,5                    ; Infinity ?
    jne     __fpow6                 ; No, end of case
    mov     eax,2                   ; Set range error (ERANGE)
                                                ; Get errno pointer offset
__fpow5:
    int     3
    mov     edi,0                   ; TODO: offset flat:__crt_errno
    mov     edi,[edi]               ; Get C errno variable pointer
    mov     dword [edi],eax     ; Set errno
__fpow6:        
	pop     eax                     ; Restore register eax
    pop     edi                     ; Restore register edi
    mov     esp,ebp                 ; Deallocate temporary space
    pop     ebp
 ret

declare_func __CIpow
	sub     esp,16                  ; Allocate stack space for args
    fstp    qword [esp+8]       ; Copy y onto stack
    fstp    qword [esp]         ; Copy x onto stack
    call    powf                    ; Call pow
    add     esp,16                  ; Remove args from stack
ret



declare_func log_
     push    ebp
     mov     ebp,esp
     fld     qword [ebp+8]       ; Load real from stack
     fldln2                          ; Load log base e of 2
     fxch    st1                   ; Exchange st, st(1)
	 fyl2x                           ; Compute the natural log(x)
     pop     ebp
ret

declare_func log10_
	push    ebp
    mov     ebp,esp
    fld     qword [ebp+8]       ; Load real from stack
    fldlg2                          ; Load log base 10 of 2
    fxch    st1                   ; Exchange st, st(1)
    fyl2x                           ; Compute the log base 10(x)
    pop     ebp
ret



  
  