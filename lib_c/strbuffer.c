/*
 * Copyright (c) 2009-2013 Petri Lehtinen <petri@digip.org>
 *
 * Jansson is free software; you can redistribute it and/or modify
 * it under the terms of the MIT license. See LICENSE for details.
 */

#define LIBC_API C_EXPORT

#include <std_def.h>
#include <std_mem.h>
#include <std_math.h>
#include <kern.h>
#include <mem_base.h>
#include <std_str.h>
#include <tree.h>

#include "strbuffer.h"
#include "utf.h"

#define STRBUFFER_MIN_SIZE  16
#define STRBUFFER_FACTOR    2
#define STRBUFFER_SIZE_MAX  ((size_t)-1)



#define EOF     (-1)
#define ERANGE      34

#define STREAM_STATE_OK        0
#define STREAM_STATE_EOF      -1
#define STREAM_STATE_ERROR    -2

#define TOKEN_INVALID         -1
#define TOKEN_EOF              0
#define TOKEN_STRING         256
#define TOKEN_INTEGER        257
#define TOKEN_REAL           258
#define TOKEN_TRUE           259
#define TOKEN_FALSE          260
#define TOKEN_NULL           261

/* Locale independent versions of isxxx() functions */
#define l_isupper(c)  ('A' <= (c) && (c) <= 'Z')
#define l_islower(c)  ('a' <= (c) && (c) <= 'z')
#define l_isalpha(c)  (l_isupper(c) || l_islower(c) || c == '_')
#define l_isdigit(c)  ('0' <= (c) && (c) <= '9')
#define l_isxdigit(c) \
    (l_isdigit(c) || ('A' <= (c) && (c) <= 'F') || ('a' <= (c) && (c) <= 'f'))





#define stream_to_lex(stream) container_of(stream, lex_t, stream)

extern unsigned int tree_mem_area_id;

int strbuffer_init(strbuffer_t *strbuff)
{
    strbuff->size	= 1024;
    strbuff->length = 0;
    strbuff->value_ptr[0] = '\0';
    return 0;
}

void strbuffer_close(strbuffer_t *strbuff)
{
    strbuff->size = 0;
    strbuff->length = 0;
}

void strbuffer_clear(strbuffer_t *strbuff)
{
    strbuff->length = 0;
    strbuff->value_ptr[0] = '\0';
}

const char *strbuffer_value(const strbuffer_t *strbuff)
{
    return strbuff->value_ptr;
}

char *strbuffer_steal_value(strbuffer_t *strbuff)
{
    char *result = strbuff->value_ptr;
    return result;
}

int strbuffer_append(strbuffer_t *strbuff, const char *string)
{
    return strbuffer_append_bytes(strbuff, string, strlen_c(string));
}

int strbuffer_append_byte(strbuffer_t *strbuff, char byte)
{
    return strbuffer_append_bytes(strbuff, &byte, 1);
}

int strbuffer_append_bytes(strbuffer_t *strbuff, const char *data, size_t size)
{

	if(size >= strbuff->size - strbuff->length)return 0;
/*
    if(size >= strbuff->size - strbuff->length)
    {
        size_t new_size;
//        char *new_value;

        // avoid integer overflow 
        if (strbuff->size > STRBUFFER_SIZE_MAX / STRBUFFER_FACTOR
            || size > STRBUFFER_SIZE_MAX - 1
            || strbuff->length > STRBUFFER_SIZE_MAX - 1 - size)
            return -1;

		if((strbuff->size * STRBUFFER_FACTOR)>(strbuff->length + size + 1))
			new_size = strbuff->size * STRBUFFER_FACTOR;
		else
			new_size = strbuff->length + size + 1;
                       
        //new_value = malloc_c(new_size);
        //if(!new_value)return -1;
        //memcpy_c(strbuff->value_ptr, strbuff->value_ptr, strbuff->length);

		//release_zone_ref(&strbuff->value_ref);
        
        //strbuff->value_ptr = new_value;
        strbuff->size = new_size;
    }
	*/

    memcpy_c(strbuff->value_ptr + strbuff->length, data, size);
    strbuff->length += size;
    strbuff->value_ptr[strbuff->length] = '\0';

    return 0;
}

char strbuffer_pop(strbuffer_t *strbuff)
{
    if(strbuff->length > 0) {
        char c = strbuff->value_ptr[--strbuff->length];
        strbuff->value_ptr[strbuff->length] = '\0';
        return c;
    }
    else
        return '\0';
}




#if 0
/*** error reporting ***/

static void error_set(json_error_t *error, const lex_t *lex,
                      const char *msg, ...)
{
    va_list ap;
    char msg_text[JSON_ERROR_TEXT_LENGTH];
    char msg_with_context[JSON_ERROR_TEXT_LENGTH];

    int line = -1, col = -1;
    size_t pos = 0;
    const char *result = msg_text;

    if(!error)
        return;

    va_start(ap, msg);
    vsnprintf(msg_text, JSON_ERROR_TEXT_LENGTH, msg, ap);
    msg_text[JSON_ERROR_TEXT_LENGTH - 1] = '\0';
    va_end(ap);

    if(lex)
    {
        const char *saved_text = strbuffer_value(&lex->saved_text);

        line = lex->stream.line;
        col = lex->stream.column;
        pos = lex->stream.position;

        if(saved_text && saved_text[0])
        {
            if(lex->saved_text.length <= 20) {
                snprintf(msg_with_context, JSON_ERROR_TEXT_LENGTH,
                         "%s near '%s'", msg_text, saved_text);
                msg_with_context[JSON_ERROR_TEXT_LENGTH - 1] = '\0';
                result = msg_with_context;
            }
        }
        else
        {
            if(lex->stream.state == STREAM_STATE_ERROR) {
                /* No context for UTF-8 decoding errors */
                result = msg_text;
            }
            else {
                snprintf(msg_with_context, JSON_ERROR_TEXT_LENGTH,
                         "%s near end of file", msg_text);
                msg_with_context[JSON_ERROR_TEXT_LENGTH - 1] = '\0';
                result = msg_with_context;
            }
        }
    }

    jsonp_error_set(error, line, col, pos, "%s", result);
}
#endif

/*** lexical analyzer ***/

static void
stream_init(stream_t *stream, get_func get, void *data)
{
    stream->get = get;
    stream->data = data;
    stream->buffer[0] = '\0';
    stream->buffer_pos = 0;

    stream->state = STREAM_STATE_OK;
    stream->line = 1;
    stream->column = 0;
    stream->position = 0;
}

static int stream_get(stream_t *stream)
{
    int c;

    if(stream->state != STREAM_STATE_OK)
        return stream->state;

    if(!stream->buffer[stream->buffer_pos])
    {
        c = stream->get(stream->data);
        if(c == EOF) {
            stream->state = STREAM_STATE_EOF;
            return STREAM_STATE_EOF;
        }

        stream->buffer[0] = c;
        stream->buffer_pos = 0;

        if(0x80 <= c && c <= 0xFF)
        {
            /* multi-byte UTF-8 sequence */
            int i, count;

            count = utf8_check_first(c);
            if(!count)
                goto out;

//            assert(count >= 2);

            for(i = 1; i < count; i++)
                stream->buffer[i] = stream->get(stream->data);

            if(!utf8_check_full(stream->buffer, count, NULL))
                goto out;

            stream->buffer[count] = '\0';
        }
        else
            stream->buffer[1] = '\0';
    }

    c = stream->buffer[stream->buffer_pos++];

    stream->position++;
    if(c == '\n') {
        stream->line++;
        stream->last_column = stream->column;
        stream->column = 0;
    }
    else if(utf8_check_first(c)) {
        /* track the Unicode character column, so increment only if
           this is the first character of a UTF-8 sequence */
        stream->column++;
    }

    return c;

out:
    stream->state = STREAM_STATE_ERROR;
//    error_set(error, stream_to_lex(stream), "unable to decode byte 0x%x", c);
    return STREAM_STATE_ERROR;
}

static void stream_unget(stream_t *stream, int c)
{
    if(c == STREAM_STATE_EOF || c == STREAM_STATE_ERROR)
        return;

    stream->position--;
    if(c == '\n') {
        stream->line--;
        stream->column = stream->last_column;
    }
    else if(utf8_check_first(c))
        stream->column--;

//    assert(stream->buffer_pos > 0);
    stream->buffer_pos--;
//    assert(stream->buffer[stream->buffer_pos] == c);
}


static int lex_get(lex_t *lex)
{
    return stream_get(&lex->stream);
}

static void lex_save(lex_t *lex, int c)
{
    strbuffer_append_byte(&lex->saved_text, c);
}

static int lex_get_save(lex_t *lex)
{
    int c = stream_get(&lex->stream);
    if(c != STREAM_STATE_EOF && c != STREAM_STATE_ERROR)
        lex_save(lex, c);
    return c;
}

static void lex_unget(lex_t *lex, int c)
{
    stream_unget(&lex->stream, c);
}

static void lex_unget_unsave(lex_t *lex, int c)
{
    if(c != STREAM_STATE_EOF && c != STREAM_STATE_ERROR) {
        /* Since we treat warnings as errors, when assertions are turned
         * off the "d" variable would be set but never used. Which is
         * treated as an error by GCC.
         */
        #ifndef NDEBUG
        char d;
        #endif
        stream_unget(&lex->stream, c);
        #ifndef NDEBUG
        d = 
        #endif
            strbuffer_pop(&lex->saved_text);
//        assert(c == d);
    }
}

static void lex_save_cached(lex_t *lex)
{
    while(lex->stream.buffer[lex->stream.buffer_pos] != '\0')
    {
        lex_save(lex, lex->stream.buffer[lex->stream.buffer_pos]);
        lex->stream.buffer_pos++;
        lex->stream.position++;
    }
}

/* assumes that str points to 'u' plus at least 4 valid hex digits */
static int32_t decode_unicode_escape(const char *str)
{
    int i;
    int32_t value = 0;

//    assert(str[0] == 'u');

    for(i = 1; i <= 4; i++) {
        char c = str[i];
        value <<= 4;
        if(l_isdigit(c))
            value += c - '0';
        else if(l_islower(c))
            value += c - 'a' + 10;
        else if(l_isupper(c))
            value += c - 'A' + 10;
		/*
        else
            assert(0);
		*/
    }

    return value;
}


/* assumes that str points to 'u' plus at least 4 valid hex digits */
static int32_t decode_escape(const char *str)
{
    int				i;
	char			c;
    int32_t			value;
	unsigned int	value_1,value_2;

//    assert(str[0] == 'u');

    	
	c = str[i];
    if(l_isdigit(c))
        value_1 = (c - '0');
    else if(l_islower(c))
        value_1 = ((c - 'a') + 10);
    else if(l_isupper(c))
        value_1 = ((c - 'A') + 10);

	
	c = str[i+1];

    if(l_isdigit(c))
        value_2 = (c - '0');
    else if(l_islower(c))
        value_2 = ((c - 'a') + 10);
    else if(l_isupper(c))
        value_2 = ((c - 'A') + 10);

	value = (value_1<<4)|(value_2);

    return value;
}


static void lex_scan_string(lex_t *lex)
{
    int c;
    const char *p;
    char *t;
    int i;

    lex->value.string	= NULL;
    lex->token			= TOKEN_INVALID;

    c = lex_get_save(lex);



    while(c != '"') {
        if(c == STREAM_STATE_ERROR)
			return;

        else if(c == STREAM_STATE_EOF) {
				writestr("premature end of input\n");
//            error_set(error, lex, "premature end of input");
            return;
        }

        else if(0 <= c && c <= 0x1F) {
            /* control character */
            lex_unget_unsave(lex, c);
			
			writestr_fmt("unexpected ctrl char 0x%2.2x\n",c);
			/*
            if(c == '\n')
                error_set(error, lex, "unexpected newline", c);
            else
                error_set(error, lex, "control character 0x%x", c);
			*/
           return;
        }

        else if(c == '\\') {
            c = lex_get_save(lex);
            if(c == 'u') {
                c = lex_get_save(lex);
                for(i = 0; i < 4; i++) {
                    if(!l_isxdigit(c)) 
					{
                        writestr("invalid u escape\n");
                       return;
                    }
                    c = lex_get_save(lex);
                }
            }
            else
			{
				 c = lex_get_save(lex);
				 c = lex_get_save(lex);
   			}
        }
        else
            c = lex_get_save(lex);
    }

    /* the actual value is at most of the same length as the source
       string, because:
         - shortcut escapes (e.g. "\t") (length 2) are converted to 1 byte
         - a single \uXXXX escape (length 6) is converted to at most 3 bytes
         - two \uXXXX escapes (length 12) forming an UTF-16 surrogate pair
           are converted to 4 bytes
    */

	lex->value.string = lex->value_buffer;

    /* the target */
    t = lex->value.string;


    /* + 1 to skip the " */
    p = strbuffer_value(&lex->saved_text) + 1;

    while(*p != '"') {
        if(*p == '\\') {
			p++;
            if( (*p) == 'u') {
                char buffer[4];
                int length;
                int32_t value;

				 

                value = decode_unicode_escape(p);
                p += 5;

                if(0xD800 <= value && value <= 0xDBFF) {
                    /* surrogate pair */
                    if(*p == '\\' && *(p + 1) == 'u') {
                        int32_t value2 = decode_unicode_escape(++p);
                        p += 5;

                        if(0xDC00 <= value2 && value2 <= 0xDFFF) {
                            /* valid second surrogate */
                            value =
                                ((value - 0xD800) << 10) +
                                (value2 - 0xDC00) +
                                0x10000;
                        }
                        else {
                            /* invalid second surrogate */
                            //error_set(error, lex,"invalid Unicode '\\u%04X\\u%04X'",value, value2);
                            return;
                        }
                    }
                    else {
                        /* no second surrogate */
                        //error_set(error, lex, "invalid Unicode '\\u%04X'",value);
                        return;
                    }
                }
                else if(0xDC00 <= value && value <= 0xDFFF) {
                    //error_set(error, lex, "invalid Unicode '\\u%04X'", value);
                    return;
                }
                else if(value == 0)
                {
                    //error_set(error, lex, "\\u0000 is not allowed");
					return;
                }

                utf8_encode(value, buffer, &length);

                memcpy_c(t, buffer, length);
                t += length;
            }
            else 
			{
			  //*(t++) = *(p++);

			  
              char buffer[4];
              int32_t value;

			  buffer[0]=p[0];
			  buffer[1]=p[1];
			  buffer[2]=0;


			  value	= strtol_c(buffer,PTR_NULL,16);


			  p		+= 2;
              *t	=(value&0xFF);
			  t++;
			  
				
			  /*
              switch(*p) {
                  case '"': case '\\': case '/':*t = *p; break;
                  case 'b': *t = '\b'; break;
                  case 'f': *t = '\f'; break;
                  case 'n': *t = '\n'; break;
                  case 'r': *t = '\r'; break;
                  case 't': *t = '\t'; break;
                  //default: assert(0);
              }
              t++;
              p++;
			  */
            }
        }
        else
            *(t++) = *(p++);
    }
    *t = '\0';


	
    lex->token = TOKEN_STRING;
    return;

}

#ifndef JANSSON_USING_CMAKE /* disabled if using cmake */
#if JSON_INTEGER_IS_LONG_LONG
#ifdef _MSC_VER  /* Microsoft Visual Studio */
#define json_strtoint     _strtoi64
#else
#define json_strtoint     strtoll
#endif
#else
#define json_strtoint     strtol
#endif
#endif

static int lex_scan_number(lex_t *lex, int c)
{
    const char *saved_text;
    char *end;
//    double value;

    lex->token = TOKEN_INVALID;

    if(c == '-')
        c = lex_get_save(lex);

    if(c == '0') {
        c = lex_get_save(lex);

        if(l_isdigit(c)) {
            lex_unget_unsave(lex, c);
            goto out;
        }
		if(c=='x')
		{
			 c = lex_get_save(lex);
			while(l_isxdigit(c))c = lex_get_save(lex);
		}
    }
    else if(l_isdigit(c)) {
        c = lex_get_save(lex);
        while(l_isdigit(c))
            c = lex_get_save(lex);
    }
    else {
        lex_unget_unsave(lex, c);
        goto out;
    }

    if(c != '.' && c != 'E' && c != 'e') {
        json_int_t value;

        lex_unget_unsave(lex, c);

        saved_text = strbuffer_value(&lex->saved_text);

        //errno = 0;

		if(!strncmp_c(saved_text,"0x",2))
			value = strtoul_c(&saved_text[2], &end, 16);
		else
			value = strtol_c(saved_text, &end, 10);

		/*
        if(errno == ERANGE) {
			
            if(value < 0)
                error_set(error, lex, "too big negative integer");
            else
                error_set(error, lex, "too big integer");
		
            goto out;
        }
			*/

        //assert(end == saved_text + lex->saved_text.length);

        lex->token = TOKEN_INTEGER;
        lex->value.integer = value;
        return 0;
    }

    if(c == '.') {
        c = lex_get(lex);
        if(!l_isdigit(c)) {
            lex_unget(lex, c);
            goto out;
        }
        lex_save(lex, c);

        c = lex_get_save(lex);
        while(l_isdigit(c))
            c = lex_get_save(lex);
    }

    if(c == 'E' || c == 'e') {
        c = lex_get_save(lex);
        if(c == '+' || c == '-')
            c = lex_get_save(lex);

        if(!l_isdigit(c)) {
            lex_unget_unsave(lex, c);
            goto out;
        }

        c = lex_get_save(lex);
        while(l_isdigit(c))
            c = lex_get_save(lex);
    }

    lex_unget_unsave(lex, c);

	/*
    if(jsonp_strtod(&lex->saved_text, &value)) 
	{
        error_set(error, lex, "real number overflow");
        goto out;
    }
    lex->value.real = value;
	*/

    lex->token = TOKEN_REAL;
	lex->value.real=0;

    return 0;

out:
    return -1;
}

 int lex_scan(lex_t *lex)
{
    int c;

    strbuffer_clear(&lex->saved_text);

    if(lex->token == TOKEN_STRING) {
        //free_c(lex->value.string);
        lex->value.string = NULL;
    }

    c = lex_get(lex);
    while(c == ' ' || c == '\t' || c == '\n' || c == '\r')
        c = lex_get(lex);

    if(c == STREAM_STATE_EOF) {
        lex->token = TOKEN_EOF;
        goto out;
    }

    if(c == STREAM_STATE_ERROR) {
        lex->token = TOKEN_INVALID;
        goto out;
    }

    lex_save(lex, c);

	

    if(c == '{' || c == '}' || c == '[' || c == ']' || c == ':' || c == ','|| c == '(' || c == ')')
        lex->token = c;

    else if(c == '"')
        lex_scan_string(lex);

    else if(l_isdigit(c) || c == '-') {
        if(lex_scan_number(lex, c))
            goto out;
    }

    else if(l_isalpha(c)) {
        /* eat up the whole identifier for clearer error messages */
        const char *saved_text;

        c = lex_get_save(lex);
        while(l_isalpha(c))
            c = lex_get_save(lex);
        lex_unget_unsave(lex, c);

        saved_text = strbuffer_value(&lex->saved_text);

        if(strcmp_c(saved_text, "true") == 0)
            lex->token = TOKEN_TRUE;
        else if(strcmp_c(saved_text, "false") == 0)
            lex->token = TOKEN_FALSE;
        else if(strcmp_c(saved_text, "null") == 0)
            lex->token = TOKEN_NULL;
        else
		{
			lex->value.string = lex->value_buffer;//malloc_c(strlen_c(saved_text)+1);
			strcpy_s(lex->value.string,1024,saved_text);
            lex->token = TOKEN_STRING;
		}
    }

    else {
        /* save the rest of the input UTF-8 sequence to get an error
           message of valid UTF-8 */
        lex_save_cached(lex);
        lex->token = TOKEN_INVALID;
    }

	

out:
    return lex->token;
}

void lex_steal_string(lex_t *lex,char *str,unsigned int str_len)
{
    if(lex->token == TOKEN_STRING)
    {
		strcpy_s(str,str_len,lex->value.string);
		lex->value.string = NULL;
		/*
        result = lex->value.string;
        lex->value.string = NULL;
		*/
    }
	
}

static int buffer_get(void *data)
{
    char c;
    buffer_data_t *stream = data;
    if(stream->pos >= stream->len)
      return EOF;

    c = stream->data[stream->pos];
    stream->pos++;
    return (unsigned char)c;
}

 int lex_init(lex_t *lex,  void *data)
{
    stream_init(&lex->stream, buffer_get, data);
    if(strbuffer_init(&lex->saved_text))
        return -1;

    lex->token = TOKEN_INVALID;
    return 0;
}

 void lex_close(lex_t *lex)
{
    //if(lex->token == TOKEN_STRING) free_c(lex->value.string);
    strbuffer_close(&lex->saved_text);
}


/*** parser ***/

int parse_value_to_array(mem_zone_ref_ptr new_node,lex_t *lex);

int parse_object(mem_zone_ref_ptr array_node,lex_t *lex)
{
	char			node_name[32];
	json_int_t		node_type;

    lex_scan(lex);
    if(lex->token == '}')
	{
		return tree_manager_create_node("empty",0,array_node);
	}

    if(lex->token == '(')
	{
		 lex_scan(lex);
		 if(lex->token != TOKEN_STRING) {
			  writestr_fmt(" error node name string or ')' expected %d \n",lex->token);
			  return 0;
		 }
		 lex_steal_string(lex,node_name,32);
		 lex_scan(lex);
         if(lex->token != ',') {
			  writestr_fmt(" error ',' expected %d \n",lex->token);
			  return 0;
		 }
		 lex_scan(lex);
		 if(lex->token != TOKEN_INTEGER) {
			  writestr_fmt(" error node type integer expected %d \n",lex->token);
			  return 0;
		 }
		 node_type=lex->value.integer;

		 lex_scan(lex);
         if(lex->token != ')') {
			  writestr_fmt(" error ')' expected %d \n",lex->token);
			  return 0;
		 }
		 lex_scan(lex);
	}
	else
	{
		node_type=NODE_GFX_OBJECT;
		strcpy_s(node_name,32,"object");
	}


	if(!tree_manager_create_node(node_name,(unsigned int)(node_type&0xFFFFFFFF),array_node))
	{
		writestr("could not create new obj node \n");
		return 0;
	}



    while(1) {
        char key[1024];
//        json_t *value;

        if(lex->token != TOKEN_STRING) {
            writestr_fmt(" error string or '}' expected %d \n",lex->token);
            return 0;
        }

        lex_steal_string(lex,key,1024);

		if(tree_node_find_child_by_name(array_node, key,PTR_NULL)) {
			writestr("duplicate object key\n");
			return 0;
		}
		

        lex_scan(lex);
        if(lex->token != ':') {
            //free_c(key);
			writestr("':' expected\n");
            return 0;
        }

        lex_scan(lex);
	
		switch(lex->token) {
			case TOKEN_STRING: 
				tree_manager_set_child_value_str	(array_node,key,lex->value.string);
			break;
			case TOKEN_INTEGER:
				if(lex->value.integer>=0)
					tree_manager_set_child_value_i64	(array_node,key,lex->value.integer);
				else
					tree_manager_set_child_value_si64	(array_node,key,lex->value.integer);
			break;
			case TOKEN_TRUE:
				tree_manager_set_child_value_bool	(array_node,key,1);
			break;
			case TOKEN_FALSE:
				tree_manager_set_child_value_bool	(array_node,key,0);
			break;
			case TOKEN_NULL:
				tree_manager_add_child_node			(array_node,key,NODE_GFX_NULL,PTR_NULL);
			break;
			case '[':

				lex_scan(lex);
				if(lex->token == ']') return 1;

				while(lex->token) 
				{
					mem_zone_ref	n_new_node={PTR_NULL};

					if(!parse_value_to_array(&n_new_node,lex))
					{
						writestr("could not parse object to array \n");
						return 0;
					}
					tree_manager_node_add_child	(array_node,&n_new_node);
					release_zone_ref			(&n_new_node);

					lex_scan(lex);
					if(lex->token != ',')
						break;
					lex_scan(lex);
				}

				if(lex->token != ']') {
					writestr("parse object ']' expected \n");
					writestr(&lex->stream.buffer[lex->stream.buffer_pos]);
					writestr("\n");
					return 0;
				}
			break;
		}

		lex_scan(lex);
        if(lex->token != ',')
            break;

		lex_scan(lex);
    }

    if(lex->token != '}') {
		writestr_fmt( "'}' expected %x '%s' '%s' \n",lex->token,lex->value.string?lex->value.string:"null",lex->saved_text.value_ptr?lex->saved_text.value_ptr:"null");
		writestr(lex->stream.buffer);
		writestr("\n");
    }

    return 1;


}





int parse_value_to_array(mem_zone_ref_ptr new_node,lex_t *lex)
{
	switch(lex->token) {
        case TOKEN_STRING: 

			if(!tree_manager_create_node	("array",NODE_GFX_STR,new_node))return 0;
			tree_manager_write_node_str	(new_node,0,lex->value.string);

            //json = json_string_nocheck(lex->value.string);
	    break;

		case TOKEN_INTEGER: 
			if(!tree_manager_create_node	("array",NODE_GFX_INT,new_node))return 0;
			tree_manager_write_node_qword	(new_node,0,lex->value.integer);
         break;
         case TOKEN_TRUE:
			if(!tree_manager_create_node	("array",NODE_GFX_BOOL,new_node))return 0;
			tree_manager_write_node_dword	(new_node,0,1);
            break;

        case TOKEN_FALSE:
			if(!tree_manager_create_node	("array",NODE_GFX_BOOL,new_node))return 0;
			tree_manager_write_node_dword	(new_node,0,0);
            break;

        case TOKEN_NULL:
            if(!tree_manager_create_node	("array",NODE_GFX_INT,new_node))return 0;
            break;

        case '{':
			if(!parse_object(new_node,lex))
			{
				writestr("parse object to array failed \n");
				return 0;
			}
        break;

			/*
        case '[':
            json = parse_array(lex, flags);
           break;
		*/
        case TOKEN_INVALID:
            //error_set(error, lex, "invalid token");
            return 0;

        default:
            //error_set(error, lex, "unexpected token");
            return 0;
    }

	if(new_node->zone==PTR_NULL)
        return 0;

    return 1;
}

#if 0
int parse_array(lex_t *lex,mem_zone_ref	 *array_ref,unsigned int n_child_cnt)
{
	unsigned int		idx;

	lex_scan(lex);
    if(lex->token == ']') return 1;

	idx=n_child_cnt;

    while(lex->token) {

		if(!parse_value_to_array(array_ref,idx,lex))
		{
			return 0;
		}

        lex_scan(lex);
        if(lex->token != ',')
            break;

		idx++;
        lex_scan(lex);
    }
	

    if(lex->token != ']') {
        writestr("']' expected \n");
		return 0;
    }

    return 1;
}
#endif


int parse_value(lex_t *lex,mem_zone_ref_ptr out)
{
	unsigned int idx;
	mem_zone_ref_ptr new_node;

    switch(lex->token) {
        case TOKEN_STRING: 
			if(!tree_manager_create_node	("STR",NODE_GFX_STR,out))return 0;
			tree_manager_write_node_str		(out,0,lex->value.string);
            break;

        case TOKEN_INTEGER:
			if(!tree_manager_create_node		("INT",NODE_GFX_INT,out))return 0;
			tree_manager_write_node_qword	(out,0,lex->value.integer);
           break;
        
/*
        case TOKEN_REAL: {
            json = json_real(lex->value.real);
            break;
        }
*/
        case TOKEN_TRUE:
			if(!tree_manager_create_node		("TRUE",NODE_GFX_INT,out))return 0;
			
			tree_manager_write_node_dword	(out,0,1);
        break;

        case TOKEN_FALSE:
			if(!tree_manager_create_node		("FALSE",NODE_GFX_INT,out))return 0;
			
			tree_manager_write_node_dword	(out,0,1);
			
        break;

        case TOKEN_NULL:
            if(!tree_manager_create_node		("NULL",NODE_GFX_INT,out))return 0;
        break;

        case '{':
			if(!parse_object(out,lex))
			{
				writestr("parse object failed \n");
				return 0;
			}
			if(lex->token != '}') {
				writestr("'}' expected \n");
				return 0;
			}
			return 1;
        break;
		
        case '[':

			lex_scan(lex);
			if(lex->token == ']') return 1;

			idx=0;

			while(lex->token) 
			{
				

				if(expand_zone		(out,(idx+2)*sizeof(mem_zone_ref))<0)
				{
					writestr_fmt("could not expand array to idx [%d] \n",idx);
					return 0;
				}
				new_node		=	get_zone_ptr(out,idx*sizeof(mem_zone_ref));

				if(new_node->zone!=PTR_NULL)
					release_zone_ref(new_node);

				if(!parse_value_to_array(new_node,lex))
				{
					writestr_fmt("could not parse object to array idx [%d] \n",idx);
					return 0;
				}

				idx++;

				lex_scan(lex);
				if(lex->token != ',')
					break;

				
				lex_scan(lex);
			}

			new_node		=	get_zone_ptr(out,idx*sizeof(mem_zone_ref));
			new_node->zone	=	PTR_NULL;

			if(lex->token != ']') {
				buffer_data_t *buf=(( buffer_data_t *)(lex->stream.data));

				writestr	("parse value ']' expected ");
				writeint	(lex->token,10);
				writestr	(" '");
				writenstr	(&buf->data[buf->pos],16);
				writestr	("'\n"); 
				return 1;
			}

			return 1;
        break;

        case TOKEN_INVALID:
            writestr("invalid token\n");
            return 0;

        default:
			writestr("unexpected token\n");
            //error_set(error, lex, "unexpected token");
            return 0;
    }


	writestr("json exit\n");
	return 0;

   
}


void cat_str	(mem_zone_ref_ptr	target,const char *str)
{
	size_t		src_n,dst_n;
	char		 *dst_str;
	char		 c;
	
	if(target->zone==PTR_NULL)return;

	dst_str		=	get_zone_ptr	(target,0);
	dst_n		=	strlen_c		(dst_str);
	src_n		=	strlen_c		(str);

	if(expand_zone		(target,dst_n+src_n+1)<0)
	{
		writestr("could not realloc string buffer\n");
		return;
	}
	dst_str		=	get_zone_ptr	(target,dst_n);
	while((c=(*str++))!=0)
	{
		(*dst_str++)=c;
	}
	*dst_str=0;
}



void cat_uint	(mem_zone_ref_ptr	target,size_t val)
{
	char		  buffer[32];
	size_t		  dst_n,src_n,n;
	char		  *dst_str;
	char		  c;

	if(target->zone==PTR_NULL)return;
	
	buffer[0]='0';
	buffer[1]='x';

	uitoa_s		(val,&buffer[2],30,16);
	src_n		=	strlen_c		(buffer);

	dst_str		=	get_zone_ptr	(target,0);
	dst_n		=	strlen_c		(dst_str);
	
	if(expand_zone		(target,dst_n+src_n+1)<0)
	{
		writestr("could not realloc string buffer\n");
		return;
	}

	dst_str		=	get_zone_ptr	(target,dst_n);
	n			=	0;
	while((c=(buffer[n++]))!=0)
	{
		(*dst_str++)=c;
	}
	*dst_str=0;
	
}



void cat_int	(mem_zone_ref_ptr	target,int val)
{
	char		  buffer[32];
	size_t		  dst_n,src_n,n;
	char		  *dst_str;
	char		  c;

	if(target->zone==PTR_NULL)return;
	
	itoa_s		(val,buffer,32,10);
	src_n		=	strlen_c		(buffer);

	dst_str		=	get_zone_ptr	(target,0);
	dst_n		=	strlen_c		(dst_str);
	
	if(expand_zone		(target,dst_n+src_n+1)<0)
	{
		writestr("could not realloc string buffer\n");
		return;
	}

	dst_str		=	get_zone_ptr	(target,dst_n);
	n			=	0;
	while((c=(buffer[n++]))!=0)
	{
		(*dst_str++)=c;
	}
	*dst_str=0;
	
}

void cat_str_escaped	(mem_zone_ref_ptr	target,const char *src_string)
{
	size_t			src_n,dst_n,dst_len;
	char			*dst_str;
	char			src;

	if(target->zone==PTR_NULL)return;

	src_n		=	0;
	dst_str		=	get_zone_ptr	(target,0);
	dst_n		=	strlen_c		(dst_str);
	dst_len		=	dst_n;
	
	while(((src=src_string[src_n])!=0))
	{
		if(isalpha_c(src)||isdigit_c(src)||(src=='_')||(src==':')||(src==' ')||(src=='=')||(src==','))
			dst_len++;
		else
			dst_len+=3;

		src_n++;
	}
	
	if(expand_zone		(target,dst_len+1)<0)
	{
		writestr("could not realloc string buffer\n");
		return;
	}

	dst_str		=	get_zone_ptr	(target,dst_n);
	src_n		=	0;

	while(((src=src_string[src_n])!=0))
	{
		if(isalpha_c(src)||isdigit_c(src)||(src=='_')||(src==':')||(src==' ')||(src=='=')||(src==','))
		{
			(*(dst_str++))=src;
		}
		else
		{
			(*(dst_str++))  ='\\';
			(*(dst_str++))=hex_chars[(src&0xF0)>>4];
			(*(dst_str++))=hex_chars[(src&0xF)];
		}
		src_n++;
	}
	(*(dst_str))=0;
}


void cat_char	(mem_zone_ref_ptr	target,char c)
{
	size_t		 dst_n;
	char		 *dst_str;

	if(target->zone==PTR_NULL)return;
	
	dst_str		=	get_zone_ptr	(target,0);
	dst_n		=	strlen_c		(dst_str);

	if(expand_zone		(target,dst_n+2)<0)
	{
		writestr("could not realloc string buffer\n");
		return;
	}
	dst_str			=	get_zone_ptr	(target,dst_n);
	(*dst_str)		=	c;
	(*(dst_str+1))	=	0;
}

OS_API_C_FUNC(int) tree_manager_create_obj(struct obj_array_t *obj_array)
{
	obj_array->char_buffer.zone	=PTR_NULL;
	if(!allocate_new_zone							(tree_mem_area_id,128,&obj_array->char_buffer))
	{
		writestr("could not allocate new obj array \n");
		return 0;
	}
	obj_array->depth_level	=	0;
	obj_array->n_objs[0]	=	0;
	obj_array->n_props		=	0;
	return 1;
}

OS_API_C_FUNC(int) tree_manager_create_obj_array(struct obj_array_t *obj_array)
{
	if(!tree_manager_create_obj	(obj_array))return 0;
	strcpy_c				(get_zone_ptr(&obj_array->char_buffer,0),"[");
	return 1;
}




OS_API_C_FUNC(void) tree_manager_add_obj(struct obj_array_t *obj_array,const char *name,unsigned int type)
{
	if(obj_array->n_objs[obj_array->depth_level]>0)
		cat_str(&obj_array->char_buffer,"},");

	obj_array->n_props	=	0;

	cat_str					(&obj_array->char_buffer	,"{");
	
	if(name!=PTR_NULL)
	{
		cat_str					(&obj_array->char_buffer	,"(\"");
		cat_str_escaped			(&obj_array->char_buffer	,name);
		cat_str					(&obj_array->char_buffer	,"\",");
		cat_int					(&obj_array->char_buffer	,type);
		cat_str					(&obj_array->char_buffer	,")");
	}
	obj_array->n_objs[obj_array->depth_level]++;
}

OS_API_C_FUNC(void) tree_manager_add_obj_array(struct obj_array_t *obj_array,const char *name,unsigned int type)
{
	if(obj_array->n_objs[obj_array->depth_level]>0)
		cat_str(&obj_array->char_buffer,"},");

	obj_array->n_props	=	0;

	obj_array->n_objs[obj_array->depth_level]++;

	cat_str					(&obj_array->char_buffer	,"{");
	
	if(name!=PTR_NULL)
	{
		cat_str					(&obj_array->char_buffer	,"(\"");
		cat_str_escaped			(&obj_array->char_buffer	,name);
		cat_str					(&obj_array->char_buffer	,"\",");
		cat_int					(&obj_array->char_buffer	,type);
		cat_str					(&obj_array->char_buffer	,")");
	}

	cat_str					(&obj_array->char_buffer	,"childs:[");

	obj_array->depth_level++;
	obj_array->n_objs[obj_array->depth_level]=0;
}


//"[{(style,NODE_STYLE}style='blabla'},{(colum_1,NODE_COLUMN}label='label'},{(colum_2,NODE_COLUMN}label='size'},{(item list,NODE_ITEM_LIST}[{(item_1,node_item}label=pouet,truc=much},{(item_2,node_item}label=pouet,truc=much}]}"
OS_API_C_FUNC(void) tree_manager_add_obj_int_val(struct obj_array_t *obj_array,const char *name,unsigned int value)
{
	if(obj_array->n_props>0)
		cat_char	(&obj_array->char_buffer,',');

	cat_str				(&obj_array->char_buffer,"\"");
	cat_str_escaped		(&obj_array->char_buffer,name);
	cat_str				(&obj_array->char_buffer,"\":");
	cat_uint			(&obj_array->char_buffer,value);
	obj_array->n_props	++;
}



OS_API_C_FUNC(void) tree_manager_add_obj_sint_val(struct obj_array_t *obj_array,const char *name,int value)
{

	if(obj_array->n_props>0)
		cat_char	(&obj_array->char_buffer,',');

	cat_str				(&obj_array->char_buffer,"\"");
	cat_str_escaped		(&obj_array->char_buffer,name);
	cat_str				(&obj_array->char_buffer,"\":");
	cat_int				(&obj_array->char_buffer,value);
	obj_array->n_props	++;


}


OS_API_C_FUNC(void) tree_manager_add_obj_str_val(struct obj_array_t *obj_array,const char *name,const char *str)
{
	if(obj_array->n_props>0)
		cat_char	(&obj_array->char_buffer,',');

	cat_str				(&obj_array->char_buffer,"\"");
	cat_str_escaped		(&obj_array->char_buffer,name);
	cat_str				(&obj_array->char_buffer,"\":");
	cat_str				(&obj_array->char_buffer,"\"");
	cat_str_escaped		(&obj_array->char_buffer,str);
	cat_str				(&obj_array->char_buffer,"\"");
	obj_array->n_props	++;
}

OS_API_C_FUNC(void) tree_manager_end_obj(struct obj_array_t *obj_array)
{

	if(obj_array->n_objs[obj_array->depth_level]>0)
		cat_char	(&obj_array->char_buffer,'}');

	if(obj_array->depth_level>0)
		obj_array->depth_level--;

	

}

OS_API_C_FUNC(void) tree_manager_end_obj_array(struct obj_array_t *obj_array)
{
	if(obj_array->char_buffer.zone==PTR_NULL)return;
	if(get_zone_size(&obj_array->char_buffer)==0)return;

	if(obj_array->n_objs[obj_array->depth_level]>0)
		cat_char	(&obj_array->char_buffer,'}');

	cat_char		(&obj_array->char_buffer,']');

	if(obj_array->depth_level>0)
	{
		obj_array->depth_level--;
		/*
		if(obj_array->n_objs[obj_array->depth_level]>0)
			cat_char	(&obj_array->char_buffer,'}');
		*/
	}

	
}

OS_API_C_FUNC(void) tree_manager_free_obj_array(struct obj_array_t *obj_array)
{
	if(obj_array->char_buffer.zone==PTR_NULL)return;
	if(get_zone_size(&obj_array->char_buffer)==0)return;

	release_zone_ref	(&obj_array->char_buffer);

}


