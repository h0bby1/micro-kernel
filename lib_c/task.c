#define LIBC_API C_EXPORT
#include <std_def.h>
#include <kern.h>
#include <std_mem.h>
#include <mem_base.h>
#include <lib_c.h>
#include <tree.h>
#include <sys/task.h>

struct work_regs_t
{
	unsigned int es;				//	0
	unsigned int ds;

	unsigned int edi;				//	8
	unsigned int esi;
	unsigned int ebp;
	unsigned int esp;
	unsigned int ebx;
	unsigned int edx;
	unsigned int ecx;
	unsigned int eax;

	unsigned int exe_addr;			//	40
	unsigned int code_seg;
	unsigned int cpu_flags;

	unsigned int final_ret;			//	52
	unsigned int stack_param;
	
};

mem_zone_ref			task_list		={PTR_INVALID};
mem_zone_ref			semaphore_list	={PTR_INVALID};

//mem_zone_ref			cur_task		={PTR_INVALID};


volatile unsigned int  task_list_last_id	=0xFFFFFFFF;

unsigned int			itr_mem_area		=0xFFFFFFFF;
//unsigned int			cur_task_id			=0xFFFFFFFF;

extern int			    out_debug;
extern unsigned int		default_mem_area_id;
extern unsigned int		tree_mem_area_id;

OS_API_C_FUNC(void) init_task_manager()
{
	mem_zone_ref		container_list_zone={PTR_NULL};
	mem_ptr				start,end;
	unsigned int def_tree;
	mem_zone_ref	main_task={PTR_NULL};

	writestr					("init task manager\n");


	

	start					=	get_next_aligned_ptr(kernel_memory_map_c(2000000));
	end						=	mem_add(start,2000000);
	itr_mem_area			=	init_new_mem_area	(start,end,MEM_TYPE_DATA);
	memset_c				(start,0,mem_sub(start,end));

	def_tree			=	tree_mem_area_id;
	tree_mem_area_id	=	itr_mem_area;

	task_list.zone=PTR_NULL;
	tree_manager_create_node("task_list",NODE_TASK_LIST,&task_list);

	semaphore_list.zone=PTR_NULL;
	tree_manager_create_node("semaphore_list",NODE_TASK_LIST,&semaphore_list);

	tree_manager_create_node						("main"		,NODE_TASK			,&main_task);

	
	tree_manager_set_child_value_i32				(&main_task	,"id"				,0x00);
	tree_manager_set_child_value_i32				(&main_task	,"active"			,0x00);

	tree_manager_allocate_child_data				(&main_task	,"work_regs"		,64);
	tree_manager_set_child_value_i32				(&main_task	,"prelude"			,0x00);

	tree_manager_set_child_value_i32				(&main_task	,"mem_area_id"		,default_mem_area_id);
	tree_manager_set_child_value_i32				(&main_task	,"tree_area_id"		,def_tree);


	allocate_new_empty_zone							(0,&container_list_zone);
	tree_manager_set_child_value_ptr				(&main_task,"container list"	,container_list_zone.zone);




	tree_manager_node_add_child						(&task_list	,&main_task);
	release_zone_ref								(&main_task);

	tree_manager_dump_node_rec						(&task_list	,0,4);

	cur_task_id				=	0xFFFFFFFF;
	task_list_last_id		=	1;
	while(cur_task_id==0xFFFFFFFF){snooze(1000000);}

	tree_mem_area_id	=def_tree;
	writestr_fmt		("init task manager end %d\n",cur_task_id);
	

}
void dump_work_regs(struct work_regs_t		*regs)
{
	writestr("\nes:");
	writeint(regs->es,16);
	writestr(" ds:");
	writeint(regs->ds,16);
	writestr("\n");


	writestr("eax:");
	writeint(regs->eax,16);
	writestr(" ecx:");
	writeint(regs->ecx,16);
	writestr(" edx:");
	writeint(regs->edx,16);
	writestr(" ebx:");
	writeint(regs->ebx,16);
	writestr("\nesp:");
	writeint(regs->esp,16);
	writestr(" ebp:");
	writeint(regs->ebp,16);
	writestr(" esi:");
	writeint(regs->esi,16);
	writestr(" edi:");
	writeint(regs->edi,16);
	writestr("\n");


	writestr("addr:");
	writeint(regs->exe_addr,16);
	writestr(" cs:");
	writeint(regs->code_seg,16);
	writestr(" flags:");
	writeint(regs->cpu_flags,16);
	writestr(" param:");
	writeint(regs->stack_param,16);
	writestr(" terminate:");
	writeint(regs->final_ret,16);
	writestr("\n");
}
/*
OS_API_C_FUNC(void) task_manager_set_current_task_preemptive(int preemptive)
{
	if(cur_task.zone==PTR_NULL)return;
	if(cur_task.zone==PTR_INVALID)return;
	if(cur_task.zone==PTR_FF)return;
	

	tree_manager_set_child_value_i32	(&cur_task	,"preemptive",preemptive);
}
*/

OS_API_C_FUNC(unsigned int)  task_manager_get_current_task_id()
{
	return 	cur_task_id;
}

OS_API_C_FUNC(unsigned int)  task_manager_get_current_task_name(char *name,int len)
{
	const char		*n_ptr;
	mem_zone_ref	cur_task={PTR_NULL};
	
	if(cur_task_id==0xFFFFFFFF)return 0;
	if(!tree_node_find_child_by_id			(&task_list,cur_task_id,&cur_task))return 0;

	n_ptr=tree_mamanger_get_node_name	(&cur_task);
	strcpy_s							(name,len,n_ptr);
	release_zone_ref					(&cur_task);

	return 1;
}


OS_API_C_FUNC(int) task_manager_get_container_list(mem_zone_ref_ptr list_zone)
{
	int				ret;
	mem_zone_ref	cur_task={PTR_NULL};
	
	if(cur_task_id==0xFFFFFFFF)return 0;
	if(!tree_node_find_child_by_id			(&task_list,cur_task_id,&cur_task))
	{
		writestr("could not find cur task (get cont list) task [");
		writeint(cur_task_id,16);
		writestr("]\n");
		return 0;
	}

	ret=tree_manager_get_child_value_ptr	(&cur_task,NODE_HASH("container list"),0,&list_zone->zone);
	release_zone_ref						(&cur_task);
	return ret;
}

OS_API_C_FUNC(int) task_manager_set_container_list(mem_zone_ref_ptr containter_list)
{
	int				ret;
	mem_zone_ref	cur_task={PTR_NULL};
	
	if(cur_task_id==0xFFFFFFFF)return 0;
	if(!tree_node_find_child_by_id			(&task_list,cur_task_id,&cur_task))return 0;

	ret=tree_manager_set_child_value_ptr	(&cur_task,"container list",containter_list->zone);
	release_zone_ref						(&cur_task);
	return ret;
}

OS_API_C_FUNC(int) task_manager_get_first_task(mem_zone_ref_ptr	task_list_out,mem_zone_ref_ptr *task)
{
	return tree_manager_get_first_child(&task_list,task_list_out,task);

}

OS_API_C_FUNC(int) task_manager_get_current_user_data(mem_zone_ref	*user_data)
{
	int				ret;
	mem_zone_ref	cur_task={PTR_NULL};
	
	if(cur_task_id==0xFFFFFFFF)return 0;
	if(!tree_node_find_child_by_id			(&task_list,cur_task_id,&cur_task))return 0;
	ret=tree_node_find_child_by_name		(&cur_task,"user_data"	,user_data);
	release_zone_ref						(&cur_task);


	return ret;

}

OS_API_C_FUNC(void) task_manager_pause()
{
	mem_zone_ref	cur_task={PTR_NULL};
	
	if(cur_task_id==0xFFFFFFFF)return ;
	if(!tree_node_find_child_by_id			(&task_list,cur_task_id,&cur_task))return ;
	tree_manager_set_child_value_i32		(&cur_task	,"active",0);
	release_zone_ref						(&cur_task);

	next_task_c();
}


OS_API_C_FUNC(void) task_manager_terminate()
{
	mem_zone_ref	cur_task={PTR_NULL};
	
	if(cur_task_id==0xFFFFFFFF)return ;
	if(!tree_node_find_child_by_id			(&task_list,cur_task_id,&cur_task))return ;
	tree_manager_set_child_value_i32		(&cur_task	,"active",0);
	release_zone_ref						(&cur_task);
	next_task_c();

}






OS_API_C_FUNC(void) task_manager_set_task_active(int task_id,int active)
{
	mem_zone_ref	dst_task={PTR_NULL};

	if(task_id==0)return ;

	if(tree_node_find_child_by_id	(&task_list,task_id,&dst_task))
	{
		tree_manager_set_child_value_i32	(&dst_task	,"active",active);
		release_zone_ref					(&dst_task);
	}
	else
	{
		writestr("task id not found [");
		writeint(task_id,16);
		writestr("]\n");

		tree_manager_dump_node_rec(&task_list,0,2);
	}

	

}



OS_API_C_FUNC(mem_ptr) task_manager_next_task(mem_ptr current_context_data,unsigned int *changed)
{
	size_t		num_task;
	
	mem_zone_ref  pp={PTR_NULL};
	
	*changed=0;
	
	if(task_list_last_id	==0xFFFFFFFF)return PTR_NULL;

	if(cur_task_id==0xFFFFFFFF)
	{
		mem_zone_ref cur_task	=	{PTR_NULL};
		mem_ptr				cur_context_ptr;

		if(out_debug)
		{
			writestr_fmt ("new task init \n");
			tree_manager_dump_node_rec			(&task_list,0,2);
			tree_manager_dump_node_rec			(&semaphore_list,0,2);
			snooze		(1000000);
		}

		cur_context_ptr=PTR_NULL;
		if(tree_node_find_child_by_name					(&task_list,"main",&cur_task))
		{
			cur_context_ptr	=	tree_manager_get_child_data	(&cur_task	,NODE_HASH("work_regs")	,0);


			memcpy_c			(cur_context_ptr,current_context_data,64);

			tree_manager_set_child_value_i32			(&cur_task	,"id"					,task_list_last_id);
			tree_manager_set_child_value_i32			(&cur_task	,"active"				,1);
			tree_manager_set_child_value_i32			(&cur_task	,"prelude"				,1);
			
			if(out_debug)
			{
				tree_manager_dump_node_rec		(&cur_task	,0,2);
				snooze		(1000000);
			}
			cur_task_id		=	task_list_last_id++;

			writestr_fmt("main task init %x\n",cur_task_id);
			tree_manager_dump_node_rec	(&cur_task,0,3);
			release_zone_ref			(&cur_task);
		}
		if(out_debug)
		{
			writestr_fmt	("first task init done %p\n",cur_context_ptr);
			snooze			(1000000);
		}
	
		return current_context_data;
	}
	else
	{	
		mem_ptr			cur_context_ptr;
		mem_zone_ref cur_task	=	{PTR_NULL};
		if(!tree_node_find_child_by_id			(&task_list,cur_task_id,&cur_task))
		{
			writestr_fmt("cur task id not found %x\n",cur_task_id);
			return 0;
		}
		cur_context_ptr	=	tree_manager_get_child_data	(&cur_task	,NODE_HASH("work_regs")	,0);

		if(cur_context_ptr	!=PTR_NULL)
			memcpy_c			(cur_context_ptr,current_context_data,64);
		else
			writestr_fmt("cur task id no reg data %x\n",cur_task_id);

		release_zone_ref(&cur_task);
	}
		
	/*
	tree_manager_get_child_value_i32	(&cur_task	,NODE_HASH("active"),&active);
	if(active==1)
	{
		tree_manager_get_child_value_i32	(&cur_task	,NODE_HASH("preemptive"),&preemptive);
		if(preemptive==0){pic_set_irq_mask_c			(0x0);return current_context_data;}
	}
	*/
	
	num_task	=	tree_manager_get_node_num_children				(&task_list);

	if(num_task>1)
	{
		unsigned int cur_idx,next_idx;


		cur_idx=0xFFFFFFFF;
		next_idx=0xFFFFFFFF;

		if(tree_find_child_node_idx_by_id				(&task_list,NODE_TASK,cur_task_id,&cur_idx))
		{
			next_idx=cur_idx;
		}
		if(out_debug)
		{
			writestr("next idx ");
			writeint(next_idx,16);
			writestr(" / ");
			writesz (num_task,16);
			writestr("\n");

			writestr_fmt("tl : %p %p\n",&task_list,task_list.zone);
		}
	


		while(next_idx<num_task)
		{
			mem_zone_ref	next_task={PTR_NULL};
			mem_ptr			next_context;
			unsigned int	active,task_id;

			

			next_idx=next_idx+1;
			if(next_idx>=num_task)next_idx=0;


			//copy_zone_ref		(&cur_task,&pp);
			
			active=0;
			task_id=0xFFFFFFFF;
			if(tree_manager_get_child_at(&task_list,next_idx,&next_task))
			{
				tree_manager_get_child_value_i32	(&next_task,NODE_HASH("active"),&active);
				tree_manager_get_child_value_i32	(&next_task,NODE_HASH("id"),&task_id);
			}

			if(out_debug)
				writestr_fmt("next task %d %d \n",active,task_id);


			if(active)
			{
				unsigned int prelude;

				next_context	=	tree_manager_get_child_data	(&next_task	,NODE_HASH("work_regs")	,0);

				prelude=0;
				tree_manager_get_child_value_i32	(&next_task,NODE_HASH("prelude")		,&prelude);
				tree_manager_get_child_value_i32	(&next_task,NODE_HASH("mem_area_id")	,&default_mem_area_id);
				tree_manager_get_child_value_i32	(&next_task,NODE_HASH("tree_area_id")	,&tree_mem_area_id);

				if(out_debug)writestr("4\n");

				if(prelude==0)
				{
					*changed |= 0x02;
					tree_manager_set_child_value_i32(&next_task,"prelude",1);
				}

				if(out_debug)
					writestr_fmt	("5 %x %x\n",cur_task_id,task_id);

				cur_task_id		=	task_id;


				if(out_debug)
				{
					writestr		("done\n");
					dump_work_regs	(current_context_data);
					snooze			(4000000);
				}
				release_zone_ref	(&next_task);


				

				*changed |= 0x01;

				return next_context;
			}
		}
	}

	return current_context_data;
}



OS_API_C_FUNC(int) task_manager_new_task(const char *task_name,mem_ptr code_addr,mem_zone_ref	*user_data, unsigned int param_stack)
{
	mem_zone_ref			new_task={PTR_NULL};
	mem_zone_ref			container_list_zone={PTR_NULL};
	mem_zone_ref			user_data_node={PTR_NULL};
	mem_ptr					start,end;
	struct work_regs_t		*regs;
	mem_ptr					new_stack_ptr;
	unsigned int			size;
	int						ret;
	unsigned int			task_mem_area_id,task_tree_area_id;
	unsigned int			def_tree;
	unsigned int			cpu_flags;

	cpu_flags			=	pic_enable_irq_c(0);
	
	def_tree			=	tree_mem_area_id;
	tree_mem_area_id	=	itr_mem_area;

	draw_car_c				("new task ");
	draw_car_c				(task_name);
	draw_car_c				("\n");
	
	size					=	1024*1024*8;
	start					=	get_next_aligned_ptr	(kernel_memory_map_c(size+8));
	end						=	mem_add					(start,size);
	task_tree_area_id		=	init_new_mem_area		(start,end,MEM_TYPE_TREE);

	size					=	1024*1024*16;
	start					=	get_next_aligned_ptr	(kernel_memory_map_c(size+8));
	end						=	mem_add					(start,size);
	task_mem_area_id		=	init_new_mem_area		(start,end,MEM_TYPE_DATA);
	



	allocate_new_empty_zone				(task_mem_area_id,&container_list_zone);


	tree_manager_create_node			(task_name,NODE_TASK,&new_task);
	tree_manager_set_child_value_i32	(&new_task,"id"				,task_list_last_id);
	tree_manager_set_child_value_i32	(&new_task,"prelude"		,0);
	tree_manager_set_child_value_i32	(&new_task,"mem_area_id"	,task_mem_area_id);
	tree_manager_set_child_value_i32	(&new_task,"tree_area_id"	,task_tree_area_id);
	tree_manager_allocate_child_data	(&new_task,"work_regs"		,64);
	tree_manager_allocate_child_data	(&new_task,"stack"			,64*1024);
	tree_manager_set_child_value_ptr	(&new_task,"container list"	,container_list_zone.zone);
	tree_manager_add_child_node			(&new_task,"user_data",NODE_TASK_DATA,&user_data_node);

	if(user_data!=PTR_NULL)
		tree_manager_copy_children		(&user_data_node,user_data);

	release_zone_ref					(&user_data_node);		

	regs			=	tree_manager_get_child_data			(&new_task,NODE_HASH("work_regs")	,0);
	new_stack_ptr	=	tree_manager_get_child_data			(&new_task,NODE_HASH("stack")		,64*1024-64);



	regs->final_ret									=mem_to_uint(task_manager_terminate);;
	regs->stack_param								=param_stack;


	regs->es		=	0x10;
	regs->ds		=	0x10;
	
	regs->edi		=	0x0;
	regs->esi		=	0x0;
	regs->ebp		=	mem_to_uint(new_stack_ptr);
	regs->esp		=	mem_to_uint(new_stack_ptr);
	regs->ebx		=	0;
	regs->edx		=	0;
	regs->ecx		=	0;
	regs->eax		=	0;
	
	regs->cpu_flags	=	0x200;
	regs->exe_addr	=	mem_to_uint(code_addr);
	regs->code_seg =	0x8;
	
	tree_manager_set_child_value_i32	(&new_task,"active",1);
	tree_manager_node_add_child			(&task_list,&new_task);
	

	

	release_zone_ref					(&new_task);
	ret=task_list_last_id;

	task_list_last_id++;
	
	tree_mem_area_id	=	def_tree;
	set_cpu_flags_c		(cpu_flags);

	return ret;


	//tree_manager_dump_node_rec(&task_list,0,4);
}


OS_API_C_FUNC(mem_ptr) task_manager_new_semaphore (unsigned int InitialUnits, unsigned int MaxUnits)
{
	mem_zone_ref	new_semaphore={PTR_NULL};
	mem_ptr			sem_ptr=PTR_NULL;
	unsigned int def_tree;
	unsigned int			cpu_flags;

	cpu_flags			=	pic_enable_irq_c(0);
	
	def_tree			=	tree_mem_area_id;
	tree_mem_area_id	=	itr_mem_area;

	writestr							("creating new semaphore \n");
	
	if(tree_manager_create_node			("semaphore",NODE_SEMAPHORE,&new_semaphore))
	{
		mem_zone_ref	new_task_list={PTR_NULL};


		allocate_new_zone					(itr_mem_area,MaxUnits*sizeof(unsigned int),&new_task_list);


		tree_manager_set_child_value_i32	(&new_semaphore,"lock_task",0);
		tree_manager_set_child_value_ptr	(&new_semaphore,"task_list",new_task_list.zone);


		tree_manager_node_add_child			(&semaphore_list,&new_semaphore);

		sem_ptr							=	new_semaphore.zone;

		release_zone_ref					(&new_semaphore);
	}

	tree_mem_area_id	=	def_tree;
	set_cpu_flags_c		(cpu_flags);	


	return sem_ptr;

}


OS_API_C_FUNC(unsigned int)	task_manager_aquire_semaphore (mem_ptr handle, unsigned int time_out)
{
	mem_zone_ref			semaphore={PTR_NULL};
	mem_zone_ref			task_list={PTR_NULL};
	unsigned int			*last_data_ptr;
	unsigned int			*data_ptr;
	unsigned int			*lock_task_ptr;
	unsigned int			cpu_flags;
	
	if(handle==PTR_NULL)return 0;
	if(handle==PTR_INVALID)return 0;
	if(cur_task_id==0xFFFFFFFF)return 0;
	
	semaphore.zone=handle;

	if(!tree_manager_get_child_value_ptr	(&semaphore ,NODE_HASH("task_list"),0,&task_list.zone))return 0;
	if(!tree_manager_get_child_data_ptr		(&semaphore	,NODE_HASH("lock_task")  ,&lock_task_ptr))return 0;
	if((*lock_task_ptr)==cur_task_id)return 1;

	cpu_flags	=	pic_enable_irq_c(0);

	if(!compare_z_exchange_c(lock_task_ptr,cur_task_id))
	{
		
			
		data_ptr		=	get_zone_ptr(&task_list,0);
		last_data_ptr	=	get_zone_ptr(&task_list,0xFFFFFFFF);
		
		while(data_ptr<last_data_ptr)
		{
			if(compare_z_exchange_c(data_ptr,cur_task_id))
			{
				task_manager_pause	();
				break;
			}
			data_ptr++;
		}
	}

	set_cpu_flags_c		(cpu_flags);
	return 1;
}


OS_API_C_FUNC(void) task_manager_release_semaphore (mem_ptr handle, unsigned int Units)
{
	mem_zone_ref		semaphore={PTR_NULL};
	mem_zone_ref		task_list={PTR_NULL};
	unsigned int		*lock_task_ptr;
	unsigned int		*last_data_ptr;
	unsigned int		*data_ptr;

	if(handle==PTR_NULL)return ;
	if(handle==PTR_INVALID)return ;
	if(cur_task_id==0xFFFFFFFF)return;

	semaphore.zone=handle;
	
	if(!tree_manager_get_child_value_ptr	(&semaphore	,NODE_HASH("task_list"),0	,&task_list.zone))	{writestr_fmt("error sem 1 %p\n",semaphore.zone);return ;}
	if(!tree_manager_get_child_data_ptr		(&semaphore	,NODE_HASH("lock_task")		,&lock_task_ptr))	{writestr_fmt("error sem 2 %p\n",semaphore.zone);return ;}
	
	data_ptr		=	get_zone_ptr(&task_list,0);
	last_data_ptr	=	get_zone_ptr(&task_list,0xFFFFFFFF);

	if(swap_nz_array_c(data_ptr,last_data_ptr,lock_task_ptr)!=0)
		task_manager_set_task_active	((*lock_task_ptr),1);
	else
		(*lock_task_ptr)=0;
	


return ;
}

OS_API_C_FUNC(int)	task_manager_delete_semaphore (mem_ptr handle)
{

return 1;
}


	

OS_API_C_FUNC(unsigned int)  system_time()
{
	unsigned int		time;
	get_system_time_c	(&time);
	return time;
}


OS_API_C_FUNC(void)  snooze(unsigned int micro_sec)
{
	unsigned int	milli_sec;
	unsigned int	cur_time,time;

	
	milli_sec			=	micro_sec / 1000;
	get_system_time_c	(&cur_time);
	time				=	cur_time+milli_sec;
	while(cur_time<time)
	{
		//writestr_fmt		(" %d:%d - %d:%d\n",cur_time.uint32.ints[1],cur_time.uint32.ints[0],time.uint32.ints[1],time.uint32.ints[0]);
		get_system_time_c	(&cur_time);
	}

	
}


OS_API_C_FUNC(void) task_manager_dump_tpo_mod_infos(unsigned int addr,char *out_name,int out_len)
{
	unsigned int				n,n_mod,n_sec;
	struct kern_mod_t			*tpo_mod;
	struct kern_mod_sec_t		*tpo_mod_sec;
	struct kern_mod_fn_t		*tpo_mod_fn;
	struct kern_mod_fn_t		*last_fnc;
	unsigned int				last_start;


	n_mod=0;
	while((tpo_mod	=	tpo_get_mod_entry_idx_c		(n_mod))!=PTR_NULL)
	{
		n_sec=0;

		while(tpo_mod_sec=tpo_get_mod_sec_idx_c(n_mod,n_sec))
		{

			if((addr>=tpo_mod_sec->addr)&&(addr<tpo_mod_sec->addr+tpo_mod_sec->size))
			{
				/*
				writestr	("'");
				writestr	((char *)(uint_to_mem(tpo_mod->string_table_addr)));
				writestr	("' sec ");
				writeint	(tpo_mod_sec->addr,16);
				writestr	("=>");
				writeint	(tpo_mod_sec->size,16);
				writestr	("\n");
				*/
				
				last_fnc	=PTR_FF;
				last_start	=0;
				n=0;
				while((tpo_mod_fn=tpo_get_fn_entry_idx_c(tpo_mod->mod_hash,n))!=PTR_FF)
				{
					//writestr_fmt(" fnc %d %s %p \n",n,(char *)(uint_to_mem(tpo_mod->string_table_addr+tpo_mod_fn->string_idx)),tpo_mod_fn->func_addr);

					if(addr>=tpo_mod_fn->func_addr)
					{
						if(tpo_mod_fn->func_addr>=last_start)
						{
							last_start	=	tpo_mod_fn->func_addr;
							last_fnc	=	tpo_mod_fn;
						}
					}
					n++;
				}

				if(last_fnc!=PTR_FF)
				{
					char *fnc_nme;

					fnc_nme=	((char *)(uint_to_mem(tpo_mod->string_table_addr+last_fnc->string_idx)));
					writestr	(fnc_nme);
					writeptr	(uint_to_mem(last_fnc->func_addr));
					writestr	("+");
					writesz		(mem_sub(uint_to_mem(last_fnc->func_addr),uint_to_mem(addr)),16);
					writestr	(" b in'");
					writestr	((char *)(uint_to_mem(tpo_mod->string_table_addr)));
					writestr	("'\n");

	

					if(out_name!=PTR_NULL)
						strcpy_s		(out_name,out_len,fnc_nme);
					
				}
				else
				{
					writestr("   ");
					writestr((char *)(uint_to_mem(tpo_mod->string_table_addr)));
					writestr("\n");
				}
				return;
			}
			n_sec++;
		}

		n_mod++;
	}
}

OS_API_C_FUNC(void) task_manager_dump()
{
	unsigned int			reg_ebp;
	unsigned int			cnt;

	mem_zone_ref		task_list_ref={PTR_NULL};
	mem_zone_ref_ptr	task;

	if(!tree_manager_get_first_child(&task_list,&task_list_ref,&task))return;

	writestr							("*");
	set_text_color_c					(0x02);
	writestr							("*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=");
	set_text_color_c					(0x07);
	writestr							("\n");

	while(task->zone!=PTR_NULL)
	{
		struct work_regs_t		*regs;
		unsigned int				code_addr;


		tree_manager_dump_node_rec	(task,0,2);

		regs	=	tree_manager_get_child_data	(task	,NODE_HASH("work_regs")	,0);

		code_addr=  regs->exe_addr;
		reg_ebp	 =	regs->ebp;
		cnt		 =	6;

		while((reg_ebp!=mem_to_uint(PTR_NULL))&&(reg_ebp!=0xFFFFFFFF)&&((cnt--)>0))
		{
			char fn_name[256];
			
			task_manager_dump_tpo_mod_infos(code_addr,fn_name,256);

			
			out_debug=0;

			
			if(!strcmp_c(fn_name,"_task_manager_aquire_semaphore@8"))
			{
				mem_zone_ref	parent_node;
				
				parent_node.zone	=	*((mem_ptr *)(uint_to_mem(reg_ebp+8)));

				writestr("==========================================================\n");
				writestr("params : ");
				writeptr(parent_node.zone);
				writestr(" ");
				writeptr(get_zone_ptr(&parent_node,0));
				writestr(" ");
				writesz(get_zone_size(&parent_node),10);
				writestr("\n");

				
				if((get_zone_ptr(&parent_node,0)!=PTR_NULL)&&(get_zone_size(&parent_node)>0))
					tree_manager_dump_node_rec(&parent_node,0,3);

				writestr("==========================================================\n");

			}
			if(!strcmp_c(fn_name,"_tree_manager_json_loadb@12"))
			{
				char			*buffer;
				mem_zone_ref	*parent_node;
				size_t			buffer_len;
				
				
				buffer		=	*((mem_ptr *)(uint_to_mem(reg_ebp+8)));
				buffer_len	=	*((size_t	*)(uint_to_mem(reg_ebp+12)));
				parent_node	=	*((mem_zone_ref_ptr *)(uint_to_mem(reg_ebp+16)));
				

				writestr("==========================================================\n");
				writestr("params : ");
				writeptr(((mem_ptr *)(buffer)));
				writestr(" '");
				writestr(buffer);
				writestr("' ");
				writesz(buffer_len,10);
				writestr(" ");
				writeptr(parent_node->zone);
				writestr(" ");
				writeptr(get_zone_ptr(parent_node,0));
				writestr(" ");
				writesz(get_zone_size(parent_node),10);
				writestr("\n");

				
				writestr("==========================================================\n");
			}

			
			if(!strcmp_c(fn_name,"_tree_manager_add_child_node@16"))
			{
				mem_zone_ref	*parent_node;
				
				parent_node	=	*((mem_zone_ref_ptr *)(uint_to_mem(reg_ebp+8)));

				

				writestr("==========================================================\n");
				writestr("params : ");
				writeptr(parent_node);
				writestr(" ");
				writeptr(parent_node->zone);
				writestr(" ");
				writeptr(get_zone_ptr(parent_node,0));
				writestr(" ");
				writesz(get_zone_size(parent_node),10);
				writestr("\n");

				
				if((get_zone_ptr(parent_node,0)!=PTR_NULL)&&(get_zone_size(parent_node)>0))
					tree_manager_dump_node_rec(parent_node,0,3);

				writestr("==========================================================\n");

			}

			if(!strcmp_c(fn_name,"_tree_manager_free_node@4"))
			{
				mem_zone_ref	*parent_node;
				
				parent_node	=	*((mem_zone_ref_ptr *)(uint_to_mem(reg_ebp+8)));

				writestr("==========================================================\n");
				writestr("params : ");
				writeptr(parent_node);
				writestr(" ");
				writeptr(parent_node->zone);
				writestr(" ");
				writeptr(get_zone_ptr(parent_node,0));
				writestr(" ");
				writesz(get_zone_size(parent_node),10);
				writestr("\n");

				if((get_zone_ptr(parent_node,0)!=PTR_NULL)&&(get_zone_size(parent_node)>0))
					tree_manager_dump_node_rec(parent_node,0,3);

				writestr("==========================================================\n");

			}

			if(!strcmp_c(fn_name,"_tree_remove_all_childs@4"))
			{
				mem_zone_ref	*parent_node;
				
				parent_node	=	*((mem_zone_ref_ptr *)(uint_to_mem(reg_ebp+8)));

				writestr("==========================================================\n");
				writestr("params : ");
				writeptr(parent_node);
				writestr(" ");
				writeptr(parent_node->zone);
				writestr(" ");
				writeptr(get_zone_ptr(parent_node,0));
				writestr(" ");
				writesz(get_zone_size(parent_node),10);
				writestr("\n");

				if((get_zone_ptr(parent_node,0)!=PTR_NULL)&&(get_zone_size(parent_node)>0))
					tree_manager_dump_node_rec(parent_node,0,3);

				writestr("==========================================================\n");

			}

			if(!strcmp_c(fn_name,"_release_zone_ref@4"))
			{
				mem_zone_ref	*parent_node;

				writestr("==========================================================\n");
				
				parent_node	=	*((mem_zone_ref_ptr *)(uint_to_mem(reg_ebp+8)));

				writestr("params : ");
				writeptr(parent_node);
				writestr(" ");
				writeptr(parent_node->zone);
				writestr(" ");
				writeptr(get_zone_ptr(parent_node,0));
				writestr(" ");
				writesz(get_zone_size(parent_node),10);
				writestr("\n");
				writestr("==========================================================\n");
			}

			//out_debug=1;

			code_addr = *((unsigned int *)(uint_to_mem(reg_ebp+4)));

			if(reg_ebp!=0xFFFFFFFF)
				reg_ebp	  = *((unsigned int *)(uint_to_mem(reg_ebp)));


			if(reg_ebp==mem_to_uint(PTR_NULL))break;
			
		}

		
		set_text_color_c			(0x03);
		writestr					("------------------------------------------------------------\n");
		set_text_color_c			(0x07);

		tree_manager_get_next_child	(&task_list_ref,&task);
	}
	
	
	//tree_manager_dump_node_rec(&task_list,0,2);
}


OS_API_C_FUNC(void) task_manager_dump_task_post(mem_ptr current_context_data)
{
	unsigned int			task_id;
	unsigned int			reg_ebp;
	unsigned int			cnt;
	char					name[32];
	struct work_regs_t		*regs;

	

	regs		=(struct work_regs_t		*)current_context_data;
	
	task_id		=task_manager_get_current_task_id	();
	if(task_id>0)
	{
		task_manager_get_current_task_name				(name,32);
		writestr("[");
		writeint(task_id,16);
		writestr("] '");
		writestr(name);
		writestr("'\n");
	}
	else
	{
		writestr("cannot dump current task \n");
	}

	reg_ebp	=	regs->ebp;
	cnt		=	16;

	while((reg_ebp!=mem_to_uint(PTR_NULL))&&(reg_ebp!=0xFFFFFFFF)&&((cnt--)>0))
	{
		char fn_name[256];
		
		writestr("stack frame : ");
		writeptr(uint_to_mem(reg_ebp));
		writeptr(*((mem_ptr *)(uint_to_mem(reg_ebp+4))));
		writestr("\n");
		task_manager_dump_tpo_mod_infos(*((unsigned int *)(uint_to_mem(reg_ebp+4))),fn_name,256);
		reg_ebp	=*((unsigned int *)(uint_to_mem(reg_ebp)));
		if(reg_ebp==mem_to_uint(PTR_NULL))break;
		if(reg_ebp==0xFFFFFFFF)break;

		
		out_debug=0;

		
		if(!strcmp_c(fn_name,"_task_manager_aquire_semaphore@8"))
		{
			mem_zone_ref	parent_node;
			
			parent_node.zone	=	*((mem_ptr *)(uint_to_mem(reg_ebp+8)));

			writestr("==========================================================\n");
			writestr("params : ");
			writeptr(parent_node.zone);
			writestr(" ");
			writeptr(get_zone_ptr(&parent_node,0));
			writestr(" ");
			writesz(get_zone_size(&parent_node),10);
			writestr("\n");

			
			if((get_zone_ptr(&parent_node,0)!=PTR_NULL)&&(get_zone_size(&parent_node)>0))
				tree_manager_dump_node_rec(&parent_node,0,4);

			writestr("==========================================================\n");

		}
		if(!strcmp_c(fn_name,"_tree_manager_json_loadb@12"))
		{
			char			*buffer;
			mem_zone_ref	*parent_node;
			size_t			buffer_len;
			
			
			buffer		=	*((mem_ptr *)(uint_to_mem(reg_ebp+8)));
			buffer_len	=	*((size_t	*)(uint_to_mem(reg_ebp+12)));
			parent_node	=	*((mem_zone_ref_ptr *)(uint_to_mem(reg_ebp+16)));
			

			writestr("==========================================================\n");
			writestr("params : ");
			writeptr(((mem_ptr *)(buffer)));
			writestr(" '");
			writestr(buffer);
			writestr("' ");
			writesz(buffer_len,10);
			writestr(" ");
			writeptr(parent_node->zone);
			writestr(" ");
			writeptr(get_zone_ptr(parent_node,0));
			writestr(" ");
			writesz(get_zone_size(parent_node),10);
			writestr("\n");

			
		writestr("==========================================================\n");
		}

		
		if(!strcmp_c(fn_name,"_tree_manager_add_child_node@16"))
		{
			mem_zone_ref	*parent_node;
			
			parent_node	=	*((mem_zone_ref_ptr *)(uint_to_mem(reg_ebp+8)));

			

			writestr("==========================================================\n");
			writestr("params : ");
			writeptr(parent_node);
			writestr(" ");
			writeptr(parent_node->zone);
			writestr(" ");
			writeptr(get_zone_ptr(parent_node,0));
			writestr(" ");
			writesz(get_zone_size(parent_node),10);
			writestr("\n");

			
			if((get_zone_ptr(parent_node,0)!=PTR_NULL)&&(get_zone_size(parent_node)>0))
				tree_manager_dump_node_rec(parent_node,0,3);

			writestr("==========================================================\n");

		}

		if(!strcmp_c(fn_name,"_tree_manager_free_node@4"))
		{
			mem_zone_ref	*parent_node;
			
			parent_node	=	*((mem_zone_ref_ptr *)(uint_to_mem(reg_ebp+8)));

			writestr("==========================================================\n");
			writestr("params : ");
			writeptr(parent_node);
			writestr(" ");
			writeptr(parent_node->zone);
			writestr(" ");
			writeptr(get_zone_ptr(parent_node,0));
			writestr(" ");
			writesz(get_zone_size(parent_node),10);
			writestr("\n");

			if((get_zone_ptr(parent_node,0)!=PTR_NULL)&&(get_zone_size(parent_node)>0))
				tree_manager_dump_node_rec(parent_node,0,3);

			writestr("==========================================================\n");

		}

		if(!strcmp_c(fn_name,"_tree_remove_all_childs@4"))
		{
			mem_zone_ref	*parent_node;
			
			parent_node	=	*((mem_zone_ref_ptr *)(uint_to_mem(reg_ebp+8)));

			writestr("==========================================================\n");
			writestr("params : ");
			writeptr(parent_node);
			writestr(" ");
			writeptr(parent_node->zone);
			writestr(" ");
			writeptr(get_zone_ptr(parent_node,0));
			writestr(" ");
			writesz(get_zone_size(parent_node),10);
			writestr("\n");

			if((get_zone_ptr(parent_node,0)!=PTR_NULL)&&(get_zone_size(parent_node)>0))
				tree_manager_dump_node_rec(parent_node,0,3);

			writestr("==========================================================\n");

		}

		if(!strcmp_c(fn_name,"_release_zone_ref@4"))
		{
			mem_zone_ref	*parent_node;

			writestr("==========================================================\n");
			
			parent_node	=	*((mem_zone_ref_ptr *)(uint_to_mem(reg_ebp+8)));

			writestr("params : ");
			writeptr(parent_node);
			writestr(" ");
			writeptr(parent_node->zone);
			writestr(" ");
			writeptr(get_zone_ptr(parent_node,0));
			writestr(" ");
			writesz(get_zone_size(parent_node),10);
			writestr("\n");

			writestr("==========================================================\n");

		}

		//out_debug=1;
			
		
		writestr("\n");
		
		
	}
	//pic_set_irq_mask_c	(0x01);
	//pic_enable_irq_c	(1);
	
	snooze				(10000000);
	
}


/*

_CreateSemaphore r�f�renc� dans la fonction _AcpiOsCreateSemaphore@12
_CloseHandle r�f�renc� dans la fonction _AcpiOsDeleteSemaphore@4
_WaitForSingleObject r�f�renc� dans la fonction _AcpiOsWaitSemaphore@12
_ReleaseSemaphore r�f�renc� dans la fonction _AcpiOsSignalSemaphore@8
_GetCurrentThreadId r�f�renc� dans la fonction _AcpiOsGetThreadId@0
__beginthread r�f�renc� dans la fonction _AcpiOsExecute@12

*/