#define STREAM_API C_EXPORT
#include <std_def.h>
#include <std_mem.h>
#include <kern.h>
#include <mem_base.h>
#include <lib_c.h>
#include <tree.h>
#include <sys/mem_stream.h>
#include <sys/tpo_mod.h>
#include <sys/file_system.h>
#include <sys/ctrl.h>
#include <sys/task.h>

extern unsigned int	kernel_log_id;

mem_zone_ref				ctrl_tpo_mod_list	=	{PTR_INVALID};
mem_ptr						ctrl_list_sema		=	PTR_INVALID;


OS_API_C_FUNC(unsigned int)	 gfx_ctrl_init()
{
	mem_zone_ref		*control_ptr,*control_ptr_last;

	ctrl_tpo_mod_list.zone	=	PTR_NULL;

	if(!allocate_new_zone	(0x00,sizeof(mem_zone_ref)*8	,&ctrl_tpo_mod_list))return 0;

	control_ptr			=	get_zone_ptr(&ctrl_tpo_mod_list,0);
	control_ptr_last	=	get_zone_ptr(&ctrl_tpo_mod_list,0xFFFFFFFF);

	kernel_log				(kernel_log_id,"gfx ctrl init \n");

	while(control_ptr<control_ptr_last)
	{
		control_ptr->zone=PTR_NULL;
		control_ptr++;
	}

	ctrl_list_sema	=	task_manager_new_semaphore	(32,32);

	return 1;
}



OS_API_C_FUNC(unsigned int) gfx_ctrl_new_control(const char *ctrl_name)
{
	unsigned int cnt;
	char		file_path[256];
	mem_stream  file_stream;
	mem_zone_ref		*control_ptr,*control_ptr_last;
	mem_zone_ref		*new_control;
	ctrl_t				*new_ctrl_ptr;
	ui_ctrl_init_func_ptr	init;
	control_ptr			=	get_zone_ptr(&ctrl_tpo_mod_list,0);
	control_ptr_last	=	get_zone_ptr(&ctrl_tpo_mod_list,0xFFFFFFFF);
	new_control			=	PTR_NULL;
	cnt					=	0;
	
	while(control_ptr<control_ptr_last)
	{
		ctrl_t				*ctrl_ptr;
		if(control_ptr->zone==PTR_NULL)	
		{
			new_control=control_ptr;
			break;
		}

		ctrl_ptr=get_zone_ptr(control_ptr,0);
		if(!strncmp_c(ctrl_ptr->tpo_mod.name,ctrl_name,strlen_c(ctrl_name)))
		{
			return 1;
		}
		control_ptr++;
		cnt++;
	}



	strcpy_s(file_path,256,"/system/libs/ui/");
	strcat_s(file_path,256,ctrl_name);
	strcat_s(file_path,256,".tpo");
	file_stream.data.zone=PTR_NULL;

	if(!file_system_open_file	("isodisk",file_path,&file_stream))
	{
		kernel_log(kernel_log_id,"could not find ctrl mod '");
		writestr  (file_path);
		writestr  ("'\n");
		return 0;
	}

	task_manager_aquire_semaphore	(ctrl_list_sema,0);

	if(new_control == PTR_NULL)
	{
		if(expand_zone(&ctrl_tpo_mod_list,(cnt+1)*sizeof(mem_zone_ref))<0){
			kernel_log(kernel_log_id,"unable to expand control list \n");
			return 0;
		}

		control_ptr				=	get_zone_ptr	(&ctrl_tpo_mod_list,0);
		control_ptr_last		=	get_zone_ptr	(&ctrl_tpo_mod_list,0xFFFFFFFF);

		new_control				=	&control_ptr[cnt];
		control_ptr				=	new_control;

		while(control_ptr<control_ptr_last)
		{
			control_ptr->zone				=	PTR_NULL;
			control_ptr++;
		}
	}
	allocate_new_zone		(0,sizeof(ctrl_t),new_control);

	new_ctrl_ptr	  =		get_zone_ptr(new_control,0);

	tpo_mod_init					(&new_ctrl_ptr->tpo_mod);
	tpo_mod_load_tpo				(&file_stream, &new_ctrl_ptr->tpo_mod, 0);


	init = get_tpo_mod_exp_addr_name(&new_ctrl_ptr->tpo_mod, "init");
	if (init != PTR_NULL)
		init(PTR_NULL);
	

	mem_stream_close				(&file_stream);
	task_manager_release_semaphore	(ctrl_list_sema,0);

	return 1;
}

OS_API_C_FUNC(ctrl_t *) gfx_ctrl_find_control(const char *ctrl_name)
{
	mem_zone_ref		*control_ptr,*control_ptr_last;
	ctrl_t				*ctrl_ptr;
	control_ptr			=	get_zone_ptr(&ctrl_tpo_mod_list,0);
	control_ptr_last	=	get_zone_ptr(&ctrl_tpo_mod_list,0xFFFFFFFF);

	while(control_ptr<control_ptr_last)
	{
		if(control_ptr->zone==PTR_NULL)return PTR_NULL;

		ctrl_ptr=get_zone_ptr(control_ptr,0);
		if(!strncmp_c(ctrl_ptr->tpo_mod.name,ctrl_name,strlen_c(ctrl_name)))break;
		control_ptr++;
	}

	return ctrl_ptr;
}






OS_API_C_FUNC(unsigned int) gfx_ctrl_render(const char *ctrl_name,mem_zone_ref_const_ptr ctrl_data_node)
{
	ctrl_get_render_objs_func_ptr				ctrl_get_render_objs;
	ctrl_t										*ctrl;
	


	ctrl	=	gfx_ctrl_find_control			(ctrl_name);
	if(ctrl	==	PTR_NULL)return 0;

	ctrl_get_render_objs	=	get_tpo_mod_exp_addr_name(&ctrl->tpo_mod,"ctrl_get_render_objs");

	if(ctrl_get_render_objs ==PTR_NULL)return 0;
		
	ctrl_get_render_objs	(ctrl_data_node);



	return 1;

}


OS_API_C_FUNC(unsigned int) gfx_ctrl_compute(const char *ctrl_name,mem_zone_ref_ptr ctrl_data_node)
{
	ctrl_t										*ctrl;
	ctrl	=	gfx_ctrl_find_control			(ctrl_name);
	if(ctrl	!=	PTR_NULL)
	{
		ctrl_compute_func_ptr	ctrl_compute;
		ctrl_compute	=	get_tpo_mod_exp_addr_name(&ctrl->tpo_mod,"ctrl_compute");
		if(ctrl_compute==PTR_NULL)
		{
			writestr("ctrl compute not found \n");
			return 0;
		}
		return ctrl_compute(ctrl_data_node);
	}
	else
	{
		writestr("ctrl menu not found \n");
		return 0;
	}
}

OS_API_C_FUNC(int) gfx_ctrl_drag	   (mem_zone_ref_ptr ctrl_data_node,unsigned int obj_id,vec_2s_t drag_ofset)
{
	ctrl_t					*ctrl;
	ctrl_drag_func_ptr		ctrl_drag;
	char					ctrl_class[32];

	if(!tree_manager_get_child_value_str	(ctrl_data_node,NODE_HASH("ctrl class"),ctrl_class,32,0))
	{
		writestr("ctrl class name not found \n");
		return 0;
	}

	ctrl	=	gfx_ctrl_find_control	(ctrl_class);
	if(ctrl	==	PTR_NULL)
	{
		writestr("ctrl ");
		writestr(ctrl_class);
		writestr("not found \n");
		return 0;
	}

	ctrl_drag	=	get_tpo_mod_exp_addr_name(&ctrl->tpo_mod,"ctrl_drag");
	if(ctrl_drag==PTR_NULL)
	{
		writestr("ctrl drag not found \n");
		return 0;
	}

	return ctrl_drag(ctrl_data_node,obj_id,drag_ofset);
}

OS_API_C_FUNC(int) gfx_ctrl_event	   (mem_zone_ref_ptr ctrl_data_node,mem_zone_ref_const_ptr event_node)
{
	ctrl_t					*ctrl;
	ctrl_event_func_ptr		ctrl_event;
	char					ctrl_class[32];

	if(!tree_manager_get_child_value_str	(ctrl_data_node,NODE_HASH("ctrl class"),ctrl_class,32,0))
	{
		writestr("ctrl class name not found \n");
		return 0;
	}

	ctrl	=	gfx_ctrl_find_control	(ctrl_class);
	if(ctrl	==	PTR_NULL)
	{
		writestr("ctrl ");
		writestr(ctrl_class);
		writestr("not found \n");
		return 0;
	}

	ctrl_event	=	get_tpo_mod_exp_addr_name(&ctrl->tpo_mod,"ctrl_event");

	if(ctrl_event==PTR_NULL)
	{
		writestr("ctrl event not found \n");
		return 0;
	}


	return ctrl_event(ctrl_data_node,event_node);

	
	
}



OS_API_C_FUNC(unsigned int) gfx_ctrl_new(struct obj_array_t *ctrl_data, const char *style)
{
	if (!tree_manager_create_obj_array(ctrl_data))return 0;


	tree_manager_add_obj(ctrl_data, "style_str", NODE_GFX_STR);
	tree_manager_add_obj_str_val(ctrl_data, "style", style);
	tree_manager_add_obj_array(ctrl_data, "item_list", NODE_GFX_CTRL_ITEM_LIST);
	return 1;
}


OS_API_C_FUNC(int) gfx_create_set_ctrl_event(mem_zone_ref_ptr ctrl_data_node, const char *type, const struct gfx_rect *rect, const char *prop, unsigned int prop_val)
{
	mem_zone_ref	 ev_lst = { PTR_NULL };
	mem_zone_ref	event_item = { PTR_NULL };
	mem_zone_ref	event_rect = { PTR_NULL };

	if (!tree_manager_find_child_node(ctrl_data_node, NODE_HASH("event list"), NODE_GFX_EVENT_LIST, &ev_lst))return 0;

	if (tree_find_child_node_by_id_name(&ev_lst, NODE_GFX_EVENT, prop, prop_val, &event_item))
	{
		if (rect != PTR_NULL)tree_manager_set_child_value_rect(&event_item, "rect", rect);
		release_zone_ref(&event_item);
		release_zone_ref(&ev_lst);
		return 1;
	}


	if (tree_manager_create_node("event", NODE_GFX_EVENT, &event_item))
	{
		tree_manager_set_child_value_i32(&event_item, prop, prop_val);
		tree_manager_set_child_value_str(&event_item, "event type", type);

		tree_manager_set_child_value_rect(&event_item, "rect", rect);

		tree_manager_node_add_child(&ev_lst, &event_item);

		release_zone_ref(&event_item);
	}
	release_zone_ref(&ev_lst);



	return 1;
}


OS_API_C_FUNC(unsigned int) gfx_ctrl_create_object(struct obj_array_t *ctrl_data, const char *class_name, const char *name, mem_zone_ref_ptr ctrl_data_node, unsigned int n_items)
{
	size_t				total_item;
	unsigned int		n_col;
	mem_zone_ref_ptr	col;
	mem_zone_ref		col_list = { PTR_NULL };
	mem_zone_ref		col_list_ref = { PTR_NULL };
	mem_zone_ref		item_list = { PTR_NULL };

	tree_manager_end_obj_array(ctrl_data);
	tree_manager_end_obj_array(ctrl_data);

	if (!tree_manager_create_node_childs(name, NODE_GFX_CTRL, ctrl_data_node, get_zone_ptr(&ctrl_data->char_buffer, 0)))return 0;

	tree_manager_set_child_value_str(ctrl_data_node, "ctrl class", class_name);


	if (tree_node_find_child_by_type(ctrl_data_node, NODE_GFX_CTRL_DATA_COLUMN_LIST, &col_list, 0))
	{
		n_col = 0;
		if (tree_manager_get_first_child(&col_list, &col_list_ref, &col))
		{
			while (col->zone != PTR_NULL)
			{
				char				*style_str;
				mem_zone_ref		style_node = { PTR_NULL };

				if (!tree_manager_get_child_data_ptr(col, NODE_HASH("style_str"), &style_str))
					style_str = PTR_NULL;

				if (tree_manager_create_node_params(&style_node, style_str))
				{
					tree_manager_node_add_child(col, &style_node);
					release_zone_ref(&style_node);
				}
				if (!tree_manager_get_next_child(&col_list_ref, &col))break;
			}
		}
		release_zone_ref(&col_list);
	}

	total_item = 0;
	if (tree_node_find_child_by_type(ctrl_data_node, NODE_GFX_CTRL_ITEM_LIST, &item_list, 0))
	{
		total_item = tree_manager_get_node_num_children(&item_list);
		release_zone_ref(&item_list);
	}

	tree_manager_set_child_value_i32(ctrl_data_node, "first_items", 0);
	tree_manager_set_child_value_i32(ctrl_data_node, "total_items", total_item);
	tree_manager_set_child_value_i32(ctrl_data_node, "n_items_disp", n_items);



	return 1;


}

OS_API_C_FUNC(unsigned int) gfx_ctrl_add_item_data(mem_zone_ref_ptr ctrl_node, unsigned int item_id, mem_zone_ref_ptr item_data)
{
	int ret;
	mem_zone_ref	item_node = { PTR_NULL };
	mem_zone_ref	item_list = { PTR_NULL };

	if (!tree_node_find_child_by_type(ctrl_node, NODE_GFX_CTRL_ITEM_LIST, &item_list, 0))return 0;

	ret = 0;

	if (tree_find_child_node_by_id_name(&item_list, NODE_GFX_CTRL_ITEM, "item_id", item_id, &item_node))
	{
		mem_zone_ref	item_data_node = { PTR_NULL };

		if (!tree_manager_find_child_node(&item_node, NODE_HASH("item_data"), NODE_GFX_CTRL_ITEM_DATA, &item_data_node))
			tree_manager_add_child_node(&item_node, "item_data", NODE_GFX_CTRL_ITEM_DATA, &item_data_node);
		else
			tree_remove_children(&item_data_node);

		ret = tree_manager_node_add_child(&item_data_node, item_data);
		release_zone_ref(&item_data_node);
		release_zone_ref(&item_node);
	}

	release_zone_ref(&item_list);

	return ret;
}




OS_API_C_FUNC(int) gfx_create_rect_style(struct obj_array_t *rect_style_ar, const struct gfx_rect *rect, const vec_4uc_t color)
{

	tree_manager_create_obj(rect_style_ar);
	tree_manager_add_obj(rect_style_ar, PTR_NULL, 0);
	tree_manager_add_obj_sint_val(rect_style_ar, "p_x", rect->pos[0]);
	tree_manager_add_obj_sint_val(rect_style_ar, "p_y", rect->pos[1]);
	tree_manager_add_obj_int_val(rect_style_ar, "width", rect->size[0]);
	tree_manager_add_obj_int_val(rect_style_ar, "height", rect->size[1]);
	tree_manager_add_obj_int_val(rect_style_ar, "color", *((unsigned int *)(color)));
	tree_manager_end_obj(rect_style_ar);

	return 1;
}



