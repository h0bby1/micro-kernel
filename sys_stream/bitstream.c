#define STREAM_API C_EXPORT
#include <std_def.h>
#include <std_mem.h>

#include "mem_base.h"
#include "lib_c.h"
#include "sys/bitstream.h"





OS_API_C_FUNC(void) bitstream_open		(bitstream *stream,mem_ptr start,mem_size size)
{
	stream->buffer_start	  =	start;
	stream->buffer_ptr		  = start;
	stream->buffer_length	  =	size;
	stream->current_ofset_bits=	0;
	stream->cache_32		  = 0;
}

OS_API_C_FUNC(void) bitstream_le_write	(bitstream *stream,unsigned int data,unsigned int bits)
{
	unsigned int	data_mask;
	

	if(bits>=24)return;

	stream->cache_32			|=	(data<<	stream->current_ofset_bits);
	stream->current_ofset_bits	+=	bits;
	data_mask					 =  0xFF;

	while(stream->current_ofset_bits>=8)
	{
		unsigned char current_byte;

		current_byte				 =	(stream->cache_32&data_mask);
		stream->cache_32			>>=	8;
		stream->current_ofset_bits	-=	8;

		*stream->buffer_ptr			 =	current_byte;
		stream->buffer_ptr++;
	}
}

OS_API_C_FUNC(void) bitstream_le_flush	(bitstream *stream)
{
	unsigned char		current_byte;
	
	current_byte				=	(stream->cache_32&0xFF);
	*stream->buffer_ptr			=	current_byte;
	stream->current_ofset_bits	=	0;
	stream->buffer_ptr++;
}