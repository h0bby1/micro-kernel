#define STREAM_API C_EXPORT
#include <std_def.h>
#include <std_mem.h>
#include "mem_base.h"
#include "lib_c.h"
#include "sys/async_stream.h"
#include "sys/mem_stream.h"
#include "sys/stream_dev.h"

stream_device	str_dev		=	{0xFF};

OS_API_C_FUNC(unsigned int) new_stream_device(unsigned int bus_id,unsigned int dev_id)
{
	unsigned int	n;
	dev_stream_t	*streams;

	str_dev.stream_dev_id	=	1;
	str_dev.bus_id			=	bus_id;
	str_dev.dev_id			=   dev_id;

	allocate_new_zone(0x0,16*sizeof(dev_stream_t),&str_dev.dev_streams);

	
	
	n=0;
	while(n<16)
	{
		streams				=	get_zone_ptr(&str_dev.dev_streams,n*sizeof(dev_stream_t));
		streams->id			=	0xFFFFFFFF;
		streams->infos.zone	=	PTR_NULL;
		n++;
	}
	return str_dev.stream_dev_id;
}


OS_API_C_FUNC(stream_device *) find_stream_device(unsigned int stream_dev_id)
{
	return &str_dev;
}

OS_API_C_FUNC(unsigned int) find_stream_device_bus		(unsigned int bus_id,unsigned int dev_id)
{
	return str_dev.stream_dev_id;
}

OS_API_C_FUNC(unsigned int) get_stream_device_stream_bus	(unsigned int bus_id,unsigned int dev_id,unsigned int idx)
{
	dev_stream_t	*strdev;

	if (idx >= 16)
		return 0xFFFFFFFF;

	strdev = (dev_stream_t	*)get_zone_ptr(&str_dev.dev_streams, idx * sizeof(dev_stream_t));

	if ((strdev == PTR_NULL)|| (strdev == PTR_INVALID))
		return 0xFFFFFFFF;

	return strdev->id;
}


unsigned int get_stream_device_stream	(unsigned int stream_dev_id,unsigned int idx)
{
	stream_device *str_dev;

	str_dev	=	find_stream_device(stream_dev_id);


	return ((dev_stream_t	*)(get_zone_ptr(&str_dev->dev_streams,idx*sizeof(dev_stream_t))))->id;

	
}

OS_API_C_FUNC(unsigned int) stream_dev_new_stream(stream_device *str_dev,mem_size dma_size)
{
	unsigned int	new_stream_idx;
	unsigned int	n;
	dev_stream_t	*streams;
	
	new_stream_idx			=	async_stream_manager_new_stream(dma_size);


	if(new_stream_idx	==0xFFFFFFFF)
	{
		writestr("could not create new stream \n");
		return 0xFFFFFFFF;
	}

	

	n=0;
	while(((dev_stream_t	*)(get_zone_ptr(&str_dev->dev_streams,n*sizeof(dev_stream_t))))->id!=0xFFFFFFFF)
	{
		n++;
		if(n>=16)
		{
			writestr("max streams for this device reached\n");
			return 0xFFFFFFFF;
		}
	}
	writestr("new stream added ");
	writeint(new_stream_idx,10);
	writestr(" strdev ");
	writeint(str_dev->stream_dev_id,10);
	writestr(" pcidev ");
	writeint(str_dev->dev_id,10);
	writestr(" str idx ");
	writeint(n,10);
	writestr("\n");
	

	streams		=	get_zone_ptr(&str_dev->dev_streams,n*sizeof(dev_stream_t));
	streams->id	=	new_stream_idx;
	
	return new_stream_idx;
}

double	PI					=3.1415926535897932384626433832795028841971693993751058209749445923078164062862089986280348253421170679;
float	PI_F				=3.141592653589793f;


void write_sinus(mem_ptr	data_ptr,mem_size data_size,mem_size total_data_read)
{
	unsigned int	i;
	mem_size		n_frame,total_frames;

	n_frame				=	(data_size)/4;
	total_frames		=	total_data_read/4;
	short *buff			=	(short *)data_ptr;

	for (i = 0; i < n_frame; i++) {
		
		float freq	=	450.0f;
		unsigned short s, c;
		float p = ((float)(total_frames) * freq * PI_F * 2.0f) / (float)(n_frame);

		s =  isinf(p, 8000.0f) + 8000.0f;
		c = 0;// isinf_c(p, 8000.0f) + 8000.0f;
		
		buff[i*2+0]	=	s;
		buff[i*2+1]	=	s;

		total_frames++;
	}
}

OS_API_C_FUNC(unsigned int) stream_dev_request_write_stream(unsigned int str_dev_idx,unsigned int str_idx,mem_size size_write)
{
	
	stream_device   *str_dev;
	dev_stream_t	*streams,*streams_end;

	str_dev	=	find_stream_device(str_dev_idx);
	if(str_dev	==PTR_NULL)return 0;

	
	streams		=	get_zone_ptr(&str_dev->dev_streams,0);
	streams_end	=	get_zone_ptr(&str_dev->dev_streams, 0xFFFFFFFF);
	
	while((streams->id!=0xFFFFFFFF)&&(streams<streams_end))
	{
		if(streams->id==str_idx)
		{
			mem_ptr						data_ptr;
			mem_size					total_data_read;
			
			
			if (async_stream_manager_get_dma_ptr(str_idx, &data_ptr, &total_data_read) != 0)
			{
				//writestr_fmt("get dma %p %d, %d \n", data_ptr, total_data_read, size_write);
				
				write_sinus						(data_ptr, size_write, total_data_read);
				async_stream_manager_wrote_dma	(str_idx, size_write);

				return 1;
			}
			else
			{
				writestr("unable to get dma pointer \n");
				return 0;
			}
			
				
			
		}
		streams++;
	}
	return 0;
}