#define STREAM_API C_EXPORT
#include <std_def.h>
#include <std_mem.h>

#include "mem_base.h"
#include "lib_c.h"
#include "sys/async_stream.h"
#include "sys/mem_stream.h"
#include "sys/tpo_mod.h"

mem_zone_ref	__global_streams={PTR_INVALID};


OS_API_C_FUNC(unsigned int) async_stream_manager_init()
{
	mem_size			zone_size;
	async_stream		*end_zone;
	async_stream		*streams;
	async_stream		*stream;

	allocate_new_zone(0,16*sizeof(async_stream),&__global_streams);


	streams		=	get_zone_ptr	(&__global_streams,0);
	zone_size	=	get_zone_size	(&__global_streams);
	end_zone	=	mem_add			(streams,zone_size);
	
	stream		=	streams;
	
	while(stream<end_zone)
	{
		stream->dma_buffer.zone	=	NULL;
		stream->stream_idx		=	0xFFFFFFFF;
		stream++;
	}

	return 1;
}


unsigned int get_last_idx()
{
	mem_size			zone_size;
	async_stream		*end_zone;
	async_stream		*stream;
	unsigned int		last_idx;

	stream		=	get_zone_ptr	(&__global_streams,0);
	zone_size	=	get_zone_size	(&__global_streams);
	end_zone	=	mem_add			(stream,zone_size);
	last_idx	=	0;

	while(stream<end_zone)
	{
		if(stream->stream_idx==0xFFFFFFFF)return last_idx;
		if(stream->stream_idx>last_idx)
			last_idx=stream->stream_idx;

		stream++;
	}

	return last_idx;
}

OS_API_C_FUNC(unsigned int) async_stream_manager_new_stream(mem_size buffer_size)
{
	mem_size			zone_size;
	async_stream		*end_zone;
	async_stream		*streams;
	async_stream		*stream;
	unsigned int		n;
	unsigned int		new_stream_idx;


	streams		=	get_zone_ptr	(&__global_streams,0);
	zone_size	=	get_zone_size	(&__global_streams);
	end_zone	=	mem_add			(streams,zone_size);
	
	stream		=	streams;
	n			=	0;
	while(stream->stream_idx!=0xFFFFFFFF)
	{
		stream++;
		if(stream<end_zone)
		{
			n++;
		}
		else
		{
			if(expand_zone (&__global_streams,(n+1)*sizeof(async_stream))<0)
			{
				writestr("unable to allocate new stream [");
				writeint(n,10);
				writestr("] \n");
				return 0xFFFFFFFF;
			}
			else
			{
				
				streams		=	get_zone_ptr	(&__global_streams,0);
				zone_size	=	get_zone_size	(&__global_streams);
				end_zone	=	mem_add			(streams,zone_size);
				stream		=	&streams[n];
				while(stream<end_zone)
				{
					stream->dma_buffer.zone	=	NULL;
					stream->stream_idx		=	0xFFFFFFFF;
					stream++;
				}
				stream		=	&streams[n];
			}
		}
	}



	if(allocate_new_zone(0,buffer_size,&stream->dma_buffer)!=1)
	{
		stream->stream_idx		=	0xFFFFFFFF;
		return 0xFFFFFFFF;
	}
	new_stream_idx			= n;
	stream->stream_idx		= new_stream_idx;
	stream->total_data_read	= 0;


	return new_stream_idx;
}

OS_API_C_FUNC(int) async_stream_manager_set_dma_pos(unsigned int stream_idx,mem_size data_pos)
{
	async_stream	*stream;

	stream					=	get_zone_ptr	(&__global_streams,stream_idx*sizeof(async_stream));
	stream->current_ofset	=	data_pos;
	return 1;
}

OS_API_C_FUNC(int) async_stream_manager_get_dma_ptr(unsigned int stream_idx,mem_ptr *data_ptr,mem_size *total_read)
{
	async_stream	*stream;

	stream			=	get_zone_ptr	(&__global_streams,stream_idx*sizeof(async_stream));
	if ((stream == PTR_INVALID) || (stream == PTR_NULL))
		return 0;

	(*data_ptr)		=	get_zone_ptr	(&stream->dma_buffer,stream->current_ofset);
	if (((*data_ptr) == PTR_INVALID) || ((*data_ptr) == PTR_NULL))
		return 0;

	(*total_read)	=	stream->total_data_read;
	
	return 1;
}

OS_API_C_FUNC(int) async_stream_manager_wrote_dma	(unsigned int stream_idx,mem_size size)
{
	async_stream	*stream;

	stream		=	get_zone_ptr	(&__global_streams,stream_idx*sizeof(async_stream));
	
	if ((stream == PTR_INVALID) || (stream == PTR_NULL))
		return 0;

	stream->total_data_read+=size;

	return 1;
}

OS_API_C_FUNC(mem_ptr)	async_stream_manager_get_dma_zone(unsigned int stream_idx)
{
	async_stream	*stream;

	stream	= get_zone_ptr	(&__global_streams,stream_idx*sizeof(async_stream));
	return get_zone_ptr(&stream->dma_buffer,0);

}