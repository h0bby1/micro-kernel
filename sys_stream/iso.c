#define STREAM_API C_EXPORT
#include <std_def.h>
#include <std_mem.h>
#include "kern.h"
#include "mem_base.h"
#include "lib_c.h"
#include "tree.h"
#include "sys/mem_stream.h"
#include "sys/file_system.h"

extern enum iso_enum1_s {
  ISO_PVD_SECTOR      =   16, 
  ISO_EVD_SECTOR      =   17, 
  LEN_ISONAME         =   31, 
  ISO_MAX_SYSTEM_ID   =   32, 
  MAX_ISONAME         =   37, 
  ISO_MAX_PREPARER_ID =  128, 
  MAX_ISOPATHNAME     =  255, 
  ISO_BLOCKSIZE       = 2048  
} iso_enums1;

typedef unsigned char   iso711_t; 
typedef char		    iso712_t; 
typedef unsigned short  iso721_t; 
typedef unsigned short  iso722_t; 
typedef unsigned int	iso723_t; 
typedef unsigned int	iso731_t; 
typedef unsigned int	iso732_t; 
//typedef __int64			iso733_t; 
typedef char			achar_t;  
typedef char			dchar_t;  

/* This part borrowed from the bsd386 isofs */
#define ISODCL(from, to)        ((to) - (from) + 1)

#define ISO_MAX_PUBLISHER_ID 128

#define ISO_MAX_APPLICATION_ID 128

#define ISO_MAX_VOLUME_ID 32

#define ISO_MAX_VOLUMESET_ID 128
extern unsigned int kernel_log_id;

typedef struct   {
  iso711_t      dt_year;   
  iso711_t      dt_month;  
  iso711_t      dt_day;    
  iso711_t      dt_hour;   
  iso711_t      dt_minute; 
  iso711_t      dt_second; 
  iso712_t      dt_gmtoff; 
} iso9660_dtime_t;



typedef struct   {
  char   lt_year        [ISODCL(   1,   4)];   
  char   lt_month       [ISODCL(   5,   6)];   
  char   lt_day         [ISODCL(   7,   8)];   
  char   lt_hour        [ISODCL(   9,   10)];  
  char   lt_minute      [ISODCL(  11,   12)];  
  char   lt_second      [ISODCL(  13,   14)];  
  char   lt_hsecond     [ISODCL(  15,   16)];  
  iso712_t lt_gmtoff;  
} iso9660_ltime_t;


typedef struct  {
  iso711_t         length;            
  iso711_t         xa_length;         
  iso723_t         extent_le;
  iso723_t         extent_be;
  iso723_t         size_le;
  iso723_t         size_be;
  iso9660_dtime_t  recording_time;    
  unsigned char    file_flags;        
  iso711_t         file_unit_size;    
  iso711_t         interleave_gap;    
  iso723_t		   volume_sequence_number;    
  iso711_t         filename_len;      
} iso9660_dir_t;

typedef struct  
{
  iso711_t         type;                         
  char             id[5];                        
  iso711_t         version;                      
  char             flags;                        
  achar_t          system_id[ISO_MAX_SYSTEM_ID]; 
  dchar_t          volume_id[ISO_MAX_VOLUME_ID]; 
  char             unused2[8];
  unsigned int     volume_space_size_le;
  unsigned int     volume_space_size_be;
  char             escape_sequences[32];         
  iso723_t         volume_set_size;              
  iso723_t         volume_sequence_number;       
  iso721_t         logical_block_size_le;
  iso721_t         logical_block_size_be;

  iso723_t         path_table_size_le; 
  iso723_t         path_table_size_be; 

  iso731_t         type_l_path_table;            
  iso731_t         opt_type_l_path_table;        
  iso732_t         type_m_path_table;            
  iso732_t         opt_type_m_path_table;        
  iso9660_dir_t    root_directory_record;        
  char             root_directory_filename;      
  dchar_t          volume_set_id[ISO_MAX_VOLUMESET_ID];    
  achar_t          publisher_id[ISO_MAX_PUBLISHER_ID]; 
  achar_t          preparer_id[ISO_MAX_PREPARER_ID]; 
  achar_t          application_id[ISO_MAX_APPLICATION_ID]; 
  dchar_t          copyright_file_id[37];     
  dchar_t          abstract_file_id[37];      
  dchar_t          bibliographic_file_id[37]; 
  iso9660_ltime_t  creation_date;             
  iso9660_ltime_t  modification_date;         
  iso9660_ltime_t  expiration_date;           
  iso9660_ltime_t  effective_date;            
  iso711_t         file_structure_version;    
  unsigned char    unused4[1];                
  char             application_data[512];     
  unsigned char    unused5[653];              
}iso9660_svd_t;

typedef struct  
{
	unsigned char	name_length;
	unsigned char	ext_attr;
	unsigned int 	first_sector;
	unsigned short	parent_record;
}path_table_entry;
/*
 length
     in bytes  contents
     --------  ---------------------------------------------------------
        1      N, the name length (or 1 for the root directory)
        1      0 [number of sectors in extended attribute record]
        4      number of the first sector in the directory, as a
                 double word
        2      number of record for parent directory (or 1 for the root
                 directory), as a word; the first record is number 1,
                 the second record is number 2, etc.
        N      name (or 0 for the root directory)
      0 or 1   padding byte: if N is odd, this field contains a zero; if
                 N is even, this field is omitted
*/


typedef struct
{
	unsigned int	crc_name;
	unsigned int	type;
	unsigned int	image_position;
	mem_stream		*image_stream;
}file_node;

#define sector_size 2048

unsigned int read_table_entry(mem_stream *img_stream,unsigned int *start_position,unsigned int mode,unsigned int *name_crc)
{
	char				dir_name[256];
	short				dir_name_s[256];

	path_table_entry			path_entry;

	mem_stream_skip_to			(img_stream,*start_position);
			

	path_entry.name_length	=	mem_stream_read_8	(img_stream);
	path_entry.ext_attr		=	mem_stream_read_8	(img_stream);
	path_entry.first_sector	=	mem_stream_read_32	(img_stream);
	path_entry.parent_record=	mem_stream_read_16	(img_stream);

	if(mode==0)
	{
		mem_stream_read		(img_stream,dir_name,path_entry.name_length);
		
		if(dir_name[0]==0)
			strcpy_s(dir_name,256,"/");
		else
			dir_name[path_entry.name_length]=0;
	}
	else
	{
		mem_stream_read		(img_stream,(char *)dir_name_s,path_entry.name_length);
	   

		if(dir_name_s[0]==0)
			strcpy_s(dir_name,256,"/");
		else
		{
			int strl;
			int nchar;

			nchar	=path_entry.name_length/2;
			strl	=0;
			while(strl<nchar)
			{
				dir_name[strl]=(dir_name_s[strl]>>8);
				strl++;
			}
			dir_name[nchar]=0;
		}
	}

	(*start_position)    =	8+path_entry.name_length;
	
	if(path_entry.name_length&0x01)
	{
		mem_stream_skip		(img_stream,1);
		(*start_position)++;
	}

	*name_crc			=	calc_crc32_c(dir_name,256);

	return path_entry.first_sector;


}

unsigned int read_dir_entry	(mem_stream *img_stream,unsigned int *start_position,unsigned int mode,mem_zone_ref *file_node)
{
	char			file_name[256];
	short			file_name_s[256];
	unsigned int	start_dir;
	unsigned int	end_dir;
	iso9660_dir_t	dir_entry;
	
	start_dir				=(*start_position);

	mem_stream_skip_to	(img_stream,start_dir);

	if(mem_stream_peek_8(img_stream)==0)
	{
		
		kernel_log	(kernel_log_id,"iso zero entry ");
		writeptr	(get_zone_ptr(&img_stream->data,0));
		writestr	("\n");
		
		return 0;
	}

	dir_entry.length				=	mem_stream_read_8	(img_stream);
	
	end_dir							=	start_dir+dir_entry.length;
	(*start_position)				=	dir_entry.length;

	dir_entry.xa_length				=	mem_stream_read_8	(img_stream);
	dir_entry.extent_le				=	mem_stream_read_32	(img_stream);
	dir_entry.extent_be				=	mem_stream_read_32	(img_stream);
	dir_entry.size_le				=	mem_stream_read_32	(img_stream);
	dir_entry.size_be				=	mem_stream_read_32	(img_stream);
	
	mem_stream_read		(img_stream,(char *)&dir_entry.recording_time,7);

	dir_entry.file_flags			=	mem_stream_read_8	(img_stream); 
	dir_entry.file_unit_size		=	mem_stream_read_8	(img_stream);
	dir_entry.interleave_gap		=	mem_stream_read_8	(img_stream);
	dir_entry.volume_sequence_number=	mem_stream_read_32	(img_stream);
	dir_entry.filename_len			=	mem_stream_read_8	(img_stream);

	if(mode==0)
	{
		mem_stream_read		(img_stream,file_name,dir_entry.filename_len);
			
		if(file_name[0]!=0)
			file_name[dir_entry.filename_len]=0;
	}
	else
	{
		mem_stream_read		(img_stream,(char *)file_name_s,dir_entry.filename_len);
		   
		if(file_name_s[0]!=0)
		{
			int strl;
			int nchar;

			nchar	=dir_entry.filename_len/2;
			strl	=0;
			while(strl<nchar)
			{
				file_name[strl]=(file_name_s[strl]>>8);
				strl++;
			}
			file_name[nchar]=0;
		}
		else
			file_name[0]=0;
	}
			
	if(file_name[0]!=0)
	{
		file_name	[dir_entry.filename_len]=0;
		if(file_node!=PTR_NULL)
		{
			unsigned int	node_type;
			
			if(dir_entry.file_flags&0x02)
				node_type = NODE_FILE_SYSTEM_DIR;
			else
				node_type = NODE_FILE_SYSTEM_FILE;
			
			tree_manager_create_node		(file_name,node_type,file_node);
			
			if(node_type == NODE_FILE_SYSTEM_DIR)
				tree_manager_set_node_image_info	(file_node,dir_entry.extent_le*2048,0);
			else
				tree_manager_set_node_image_info	(file_node,dir_entry.extent_le*2048,dir_entry.size_le);
		}
	}


	return 1;
}


int get_iso_entries	(mem_stream *img_stream,mem_zone_ref *file_node,int read_files)
{
	mem_zone_ref	 new_node;
	size_t			 entry_start;
	size_t			 length;
	int				 n;

	if(!and_node_type(tree_mamanger_get_node_type(file_node),NODE_FILE_SYSTEM_DIR))
	{
		kernel_log(kernel_log_id,"iso get entries from not dir \n");
		return 0;
	}
	//if(!(((tree_mamanger_get_node_type(file_node)&0x00FFFFFF)&(NODE_FILE_SYSTEM_DIR&0x00FFFFFF))))return 0;

	new_node.zone			=	PTR_NULL;
	entry_start				=	tree_manager_get_node_image_pos	(file_node);
	length					=	entry_start;
	n						=	0;
	
	while(read_dir_entry(img_stream,&length,1,&new_node)>0)
	{
		writestr("read iso dir 001\n");
		tree_manager_node_add_child		(file_node,&new_node);
		if(read_files==1)
		{
			if(((tree_mamanger_get_node_type(&new_node)&0x00FFFFFF)==NODE_FILE_SYSTEM_FILE))
			{
				size_t file_pos,file_size;
				mem_ptr		 tree_node_data_ptr;
				file_pos	=	tree_manager_get_node_image_pos		(&new_node);
				file_size	=	tree_manager_get_node_image_size	(&new_node);

				tree_manager_allocate_node_data(&new_node,file_size);

				tree_node_data_ptr=tree_mamanger_get_node_data_ptr(&new_node,0);
				
				mem_stream_skip_to	(img_stream,file_pos*2048);
				mem_stream_read		(img_stream,tree_node_data_ptr,file_size);
			}
		}
		writestr("read iso dir 002\n");
		release_zone_ref				(&new_node);
		
		entry_start	=	entry_start+length;
		length		=	entry_start;

		n++;
	}
	return 0;

}

int find_iso_entry		(file_system *system,mem_zone_ref *file_node,const char *name,mem_zone_ref *out_node)
{
	mem_stream			*img_stream;
	const char			*node_name;
	mem_zone_ref	 	new_node;
	size_t			 	entry_start;
	size_t			 	length;
	unsigned int	 	name_crc;
	int				 	n;

	if((and_node_type(tree_mamanger_get_node_type(file_node),NODE_FILE_SYSTEM_DIR))==0)
	{
		kernel_log(kernel_log_id,"iso find from not directory ");
		writestr(tree_mamanger_get_node_name(&system->root));
		writestr(" ");
		writeint(tree_mamanger_get_node_type(file_node),16);
		writestr(" ");
		writestr(tree_mamanger_get_node_name(file_node));
		writestr("\n");
		return 0;
	}
	

	node_name				=	tree_mamanger_get_node_name(file_node);
	img_stream				=	&system->image_stream;

	name_crc				=	calc_crc32_c(name,32);
	entry_start				=	tree_manager_get_node_image_pos	(file_node);
	length					=	entry_start;
	n						=	0;
	new_node.zone			=	PTR_NULL;

	
	while(read_dir_entry (img_stream,&length,1,&new_node)>0)
	{
		if(new_node.zone!=PTR_NULL)
		{
			if(tree_manager_compare_node_crc(&new_node,name_crc))
			{
				copy_zone_ref					(out_node,&new_node);
				release_zone_ref				(&new_node);

				return 1;
			}
		}
			
		release_zone_ref	(&new_node);
		entry_start			=	entry_start+length;
		length				=	entry_start;
		n++;
	}

	
	//writestr("iso entry not found\n");
	return 0;
}



int get_iso_entry		(file_system *system,mem_zone_ref *file_node,unsigned int type,mem_zone_ref *out_node,unsigned int index)
{
	mem_stream			*img_stream;
	mem_zone_ref		new_node;
	size_t				entry_start;
	size_t				length;
	unsigned int		nfound;
	int					n;

	if(!and_node_type(tree_mamanger_get_node_type(file_node),NODE_FILE_SYSTEM_DIR))
	{
		kernel_log(kernel_log_id,"iso get entry from not dir \n");
		return 0;
	}
	
	img_stream				=	&system->image_stream;
	entry_start				=	tree_manager_get_node_image_pos	(file_node);
	length					=	entry_start;
	n						=	0;
	nfound					=	0;
	new_node.zone			=	PTR_NULL;

	while(read_dir_entry (img_stream,&length,1,&new_node)>0)
	{
		if(and_node_type(tree_mamanger_get_node_type(&new_node),type)!=0)
		{
			if(nfound==index)
			{
				copy_zone_ref					(out_node,&new_node);
				release_zone_ref				(&new_node);
				return 1;
			}
			nfound++;
		}
		release_zone_ref	(&new_node);
		entry_start			=	entry_start+length;
		length				=	entry_start;
		n++;
	}
	return 0;
}

void parse_iso_rec	 (mem_stream *img_stream,mem_zone_ref	*cur_dir)
{
	unsigned int	n;
	mem_zone_ref	child_dir;

	
	child_dir.zone	=	PTR_NULL;
	n				=	0;
	get_iso_entries				(img_stream,cur_dir,0);
	while(tree_node_find_child_by_type	(cur_dir,NODE_FILE_SYSTEM_DIR,&child_dir,n)==1)
	{
		parse_iso_rec					(img_stream,&child_dir);
		release_zone_ref				(&child_dir);
		n++;
	}
}
void parse_iso	 (file_system *files)
{
	parse_iso_rec(&files->image_stream,&files->root);
}
unsigned int read_primary_desc		(mem_stream *img_stream,iso9660_svd_t  *primary_desc)
{
		size_t				 start_pos;
	  
	  start_pos								=	img_stream->current_ptr;
	  primary_desc->type					=	mem_stream_read_8(img_stream); 
	  mem_stream_read(img_stream,primary_desc->id,5);
	  primary_desc->version					=	mem_stream_read_8(img_stream); 
	  primary_desc->flags					=	mem_stream_read_8(img_stream); 
	  mem_stream_read(img_stream,primary_desc->system_id,ISO_MAX_SYSTEM_ID);
	  mem_stream_read(img_stream,primary_desc->volume_id,ISO_MAX_SYSTEM_ID);
	  mem_stream_skip(img_stream,8);
	  primary_desc->volume_space_size_le	=	mem_stream_read_32(img_stream);		//80
	  primary_desc->volume_space_size_be	=	mem_stream_read_32(img_stream);
	  mem_stream_read(img_stream,primary_desc->escape_sequences,32);
	  primary_desc->volume_set_size			=	mem_stream_read_32(img_stream);
	  primary_desc->volume_sequence_number	=	mem_stream_read_32(img_stream);

	  primary_desc->logical_block_size_le	=	mem_stream_read_16(img_stream);		//128
	  primary_desc->logical_block_size_be	=	mem_stream_read_16(img_stream);
	  primary_desc->path_table_size_le		=	mem_stream_read_32(img_stream);
	  primary_desc->path_table_size_be		=	mem_stream_read_32(img_stream);
  	  primary_desc->type_l_path_table       =	mem_stream_read_32(img_stream);
	  primary_desc->opt_type_l_path_table   =	mem_stream_read_32(img_stream);      
  	  primary_desc->type_m_path_table       =	mem_stream_read_32(img_stream);
	  primary_desc->opt_type_m_path_table   =	mem_stream_read_32(img_stream);      
	  
	  primary_desc->root_directory_record.length					= mem_stream_read_8(img_stream);
	  primary_desc->root_directory_record.xa_length					= mem_stream_read_8(img_stream);
	  
//	  writeint(img_stream->current_ptr-start_pos,10);
//	  snooze(10000000000);

	  primary_desc->root_directory_record.extent_le					= mem_stream_read_32(img_stream);	//158
	  primary_desc->root_directory_record.extent_be					= mem_stream_read_32(img_stream);
   
	  primary_desc->root_directory_record.size_le					= mem_stream_read_32(img_stream);
	  primary_desc->root_directory_record.size_be					= mem_stream_read_32(img_stream);
	  mem_stream_skip(img_stream,8);//iso9660_dtime_t  recording_time;    

	  primary_desc->root_directory_record.file_flags				= mem_stream_read_8(img_stream);
	  primary_desc->root_directory_record.file_unit_size			= mem_stream_read_8(img_stream);
	  primary_desc->root_directory_record.interleave_gap			= mem_stream_read_8(img_stream);

	  primary_desc->root_directory_record.volume_sequence_number	= mem_stream_read_32(img_stream);
	  primary_desc->root_directory_record.filename_len				= mem_stream_read_8(img_stream);
	  mem_stream_skip_to(img_stream,start_pos+2048);


	  return 1;
}

OS_API_C_FUNC(int) open_iso_image			(mem_stream *img_stream,const char *volume_name,unsigned int root_type)
{
	
	iso9660_svd_t	 primary_desc;
	unsigned int	 n_desc;
	int				 fn_ret;

	kernel_log(kernel_log_id,"open iso image\n");

	mem_stream_skip(img_stream,sector_size*16);


	//ret = mem_stream_read(img_stream,sector_buffer,2048);
	//primary_desc	=	sector_buffer;
	  
	  
     

	n_desc			=	0;
	fn_ret			=	0;

	
	
	while(	(read_primary_desc (img_stream,&primary_desc)>0)&&
			(primary_desc.type!=0xFF))
	{
		if(memcmp_c(primary_desc.id,"CD001",5)!=0)
		{
			return 0;
		}

		kernel_log	(kernel_log_id,"type ");
		writeint	(primary_desc.type,10);
		writestr	(" root directory ");
		writeint	(primary_desc.root_directory_record.extent_le,10);
		writestr	("\n");

		kernel_log	(kernel_log_id,"path table le sector ");
		writeint	(primary_desc.type_l_path_table,10);
		writestr	(" size ");
		writeint	(primary_desc.path_table_size_le,10);
		writestr	("\n");
		
		
		/*
		if(n_desc==0)
		{
			memcpy_c	(volume_id,primary_desc.abstract_file_id,32);
			volume_id[31]=0;
		}
		else
		{
			ptr		=primary_desc.abstract_file_id;
			nchar	=16;
			strl	=0;
			while(strl<nchar)
			{
				volume_id[strl]=(ptr[strl])>>8;
				strl++;
			}
			volume_id[nchar]=0;
		}
		*/
		
		if(root_type==primary_desc.type)
		{
			mem_zone_ref		*root_node;

			
			file_system_new		(volume_name,1,img_stream);
			

			root_node	=	file_system_get_root_node	(volume_name);

			
			tree_manager_set_node_image_info			(root_node,primary_desc.root_directory_record.extent_le*primary_desc.logical_block_size_le,0);

			
			fn_ret		=	1;

		}

		/*
		mem_zone_ref	 child_node={PTR_NULL};
		tree_manager_add_child_node			(&files->base,volume_name,NODE_FILE_SYSTEM_ROOT|NODE_FILE_SYSTEM_DIR,&child_node);
		tree_manager_set_node_image_info	(&child_node,primary_desc.root_directory_record.extent_le*primary_desc.logical_block_size_le,0);
		if(root_index==n_desc)
		{
			copy_zone_ref(&files->root,&child_node);			
			fn_ret=1;
		}
		release_zone_ref					(&child_node);
		*/
		
		
		
		n_desc++;
	}

	writestr(" fs #5 \n");
	return fn_ret;

	
}