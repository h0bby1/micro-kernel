#define STREAM_API C_EXPORT
#include <std_def.h>
#include <std_mem.h>
#include "kern.h"
#include "mem_base.h"
#include "lib_c.h"
#include "tree.h"
#include "sys/mem_stream.h"
#include "sys/file_system.h"

mem_zone_ref		file_system_array		=	{PTR_INVALID};
mem_zone_ref		file_system_const_array	=	{PTR_INVALID};


extern unsigned int	kernel_log_id;
void reset_file_system(file_system *system)
{
	system->base.zone				=  PTR_NULL;
	system->root.zone				=  PTR_NULL;
	system->image_stream.data.zone	=  PTR_NULL;
	system->fs_type					=  0xFFFFFFFF;
	system->delim					=  0;
}
OS_API_C_FUNC(void) init_file_system()
{
	unsigned int		n;
	file_system			*system_ptr;
	
	file_system_array.zone	=	PTR_NULL;
	allocate_new_zone			(0,64*sizeof(file_system),&file_system_array);

	system_ptr				=	get_zone_ptr(&file_system_array,0);

	n=0;
	while(n<64)
	{
		reset_file_system(&system_ptr[n]);
		n++;
	}
}



OS_API_C_FUNC(unsigned int) file_system_new	(const char *fs_name,unsigned int fs_type,mem_stream *img_str)
{
	unsigned int		n;
	file_system			*system_ptr;
	file_system			*end_zone;
	mem_size			size;


	size			=	get_zone_size	(&file_system_array);
	system_ptr		=	get_zone_ptr	(&file_system_array,0);
	end_zone		=	mem_add			(system_ptr,size);
	n				=	0;
	while(system_ptr->fs_type!=0xFFFFFFFF)
	{
		if((system_ptr+1)>=end_zone)
		{
			if(expand_zone(&file_system_array,(n+1)*sizeof(file_system))<0)
			{
				kernel_log(kernel_log_id,"could not allocate new file system \n");
				return 0;
			}
			else
			{
				file_system		*sys_ptr;
				size			=	get_zone_size	(&file_system_array);
				system_ptr		=	get_zone_ptr	(&file_system_array,n*sizeof(file_system));
				end_zone		=	mem_add			(system_ptr,size);
				sys_ptr			=	system_ptr;

				while(sys_ptr<end_zone){reset_file_system(sys_ptr);sys_ptr++;}
			}
		}
		else
		{
			system_ptr++;
			n++;
		}
	}

	mem_stream_init(&system_ptr->image_stream,&img_str->data,img_str->buf_ofs);

	system_ptr->delim		=	'/';
	system_ptr->fs_type		=	fs_type;
	system_ptr->root.zone	=	PTR_NULL;

	tree_manager_create_node(fs_name,NODE_FILE_SYSTEM_DIR,&system_ptr->root);

	return 1;
}

file_system *find_file_system(const char *root_name)
{
	unsigned int	crc_name;
	mem_ptr			end_zone;
	mem_size		size;
	file_system		*system_ptr;
	
	crc_name		=	calc_crc32_c(root_name,32);

	size			=	get_zone_size	(&file_system_array);
	system_ptr		=	get_zone_ptr	(&file_system_array,0);
	end_zone		=	mem_add			(system_ptr,size);

	while(system_ptr->fs_type!=0xFFFFFFFF)
	{
		if(tree_manager_compare_node_crc(&system_ptr->root,crc_name))return system_ptr;
		system_ptr++;
	}
	return PTR_NULL;
}


OS_API_C_FUNC(file_system *)file_system_get_at(unsigned int idx)
{
	mem_ptr			end_zone;
	mem_size		size;
	file_system		*system_ptr;
	unsigned int	n;
	
	
	size			=	get_zone_size	(&file_system_array);
	system_ptr		=	get_zone_ptr	(&file_system_array,0);
	end_zone		=	mem_add			(system_ptr,size);
	n				=	0;

	while(system_ptr->fs_type!=0xFFFFFFFF)
	{
		if(n==idx)return system_ptr;
		n++;
		system_ptr++;
	}
	return PTR_NULL;

	
}

OS_API_C_FUNC(mem_zone_ref *) file_system_get_root_node	(const char *fs_name)
{
	file_system *files;
	files	=	find_file_system	(fs_name);
	if(files == PTR_NULL)return PTR_NULL;
	return &files->root;
}

OS_API_C_FUNC(void) file_system_dump(const char *fs_name)
{
	file_system *files;

	files	=	find_file_system	(fs_name);
	tree_manager_dump_node_rec		(&files->root,0,0xFF);
}

OS_API_C_FUNC(size_t) file_system_create_directory(const char *fs_name,const char *path)
{
	char		 cur_entry[64];	
	mem_zone_ref cur_node,next_node,new_node;
	size_t		 ofset;
	size_t		 ret;
	size_t		 size_cur;
	size_t		 pos_next;
	file_system *files;

	files	=	find_file_system(fs_name);
	if(files	==PTR_NULL)
		return 0;

	if(path[0]=='/')
	{
		ofset	=	1;
	}
	else
	{
		ofset	=	0;
	}

	cur_node.zone	=PTR_NULL;
	next_node.zone	=PTR_NULL;
	new_node.zone	=PTR_NULL;

	memset_c		(cur_entry,0,64);

	copy_zone_ref	(&cur_node,&files->root);

	strcpy_s		(cur_entry,64,tree_mamanger_get_node_name(&cur_node));
	
	while((ret=strlpos_c(path,ofset,files->delim))!=INVALID_SIZE)
	{
		pos_next	=	ret+1;



		if(path[ofset]!=files->delim)
		{
			size_cur			=	(ret-ofset);
			memcpy_c				(cur_entry,&path[ofset],size_cur);
			cur_entry[size_cur]	=	0;

			if(!tree_node_find_child_by_name(&cur_node,cur_entry,&next_node))break;
			copy_zone_ref		(&cur_node,&next_node);
			release_zone_ref	(&next_node);
		}
		ofset		=	pos_next;
	}
	/*
	if(ofset>=total_len)
	{
		writestr("already exist\n");
		release_zone_ref			(&cur_node);
		return 0;
	}
	*/
	if(!(tree_mamanger_get_node_type(&cur_node)&NODE_FILE_SYSTEM_DIR))
	{
		kernel_log					(kernel_log_id,"already exist\n");
		release_zone_ref			(&cur_node);
		return 0;
	}
	
	if(ret<0)
	{
		pos_next	=	ofset;
		//size_cur	=	(total_len-pos_next);
		size_cur	=	pos_next;
		while(path[size_cur]!=0)size_cur++;

		memcpy_c			(cur_entry,&path[ofset],size_cur);
		cur_entry[size_cur]	=	0;
	}
	
	kernel_log	(kernel_log_id,"create dir ");
	writestr	(cur_entry);
	writestr	(" at : ");
	writestr	(tree_mamanger_get_node_name(&cur_node));
	writestr	(" (");
	writeint	(tree_mamanger_get_node_type(&cur_node),16);
	writestr	(")\n");
	
	ret=tree_manager_add_child_node	(&cur_node,cur_entry,NODE_FILE_SYSTEM_DIR,&new_node);

	release_zone_ref			(&new_node);
	release_zone_ref			(&cur_node);

	
	return ret;
}


OS_API_C_FUNC(int) file_system_create_file(const char *fs_name,const char *path)
{
	
	char		 cur_entry[64];
	mem_zone_ref cur_node,next_node,new_node;
	size_t		 ofset;
	size_t		 ret;
	size_t		 size_cur;
	size_t		 pos_next;
	file_system *files;

	files	=	find_file_system(fs_name);
	if(files	==PTR_NULL)
		return 0;

	cur_node.zone	=PTR_NULL;
	next_node.zone	=PTR_NULL;
	new_node.zone	=PTR_NULL;
	if(path[0]=='/')
	{
		ofset	=	1;
	}
	else
	{
		ofset	=	0;
	}
	
	//total_len	=	strlen_c(path);


	copy_zone_ref	(&cur_node,&files->root);
	strcpy_s		(cur_entry,64,tree_mamanger_get_node_name(&cur_node));
	
	
	
	while((ret=strlpos_c(path,ofset,files->delim))!=INVALID_SIZE)
	{
		pos_next	=	ret+1;
				
		if(path[ofset]!=files->delim)
		{
			size_cur			=	(ret-ofset);
			memcpy_c				(cur_entry,&path[ofset],size_cur);
			cur_entry[size_cur]	=	0;


			if(!tree_node_find_child_by_name(&cur_node,cur_entry,&next_node))
			{
				release_zone_ref	(&cur_node);
				kernel_log			(kernel_log_id,"unable to create file \n");
				return 0;
			}
			copy_zone_ref		(&cur_node,&next_node);
			release_zone_ref	(&next_node);
		}
		ofset		=	pos_next;
	}


	if((tree_mamanger_get_node_type(&cur_node)&NODE_FILE_SYSTEM_DIR)==0)
	{
		release_zone_ref			(&cur_node);
		return 0;
	}
	
	if(ret<0)
	{
		pos_next	=	ofset;
		size_cur	=	pos_next;
		while(path[size_cur]!=0)size_cur++;
		memcpy_c			(cur_entry,&path[ofset],size_cur);
		cur_entry[size_cur]	=	0;
	}

	kernel_log	(kernel_log_id,"create file ");
	writestr	(cur_entry);
	writestr	(" at : ");
	writestr	(tree_mamanger_get_node_name(&cur_node));
	writestr	("\n");

	tree_manager_add_child_node	(&cur_node,cur_entry,NODE_FILE_SYSTEM_FILE,&new_node);

	
	release_zone_ref			(&cur_node);
	release_zone_ref			(&new_node);
	

	return 1;
}


OS_API_C_FUNC(int) file_system_open_file(const char *fs_name,const char *path,mem_stream *file_stream)
{
	
	char		 cur_entry[64];
	mem_zone_ref cur_node,next_node,new_node,data_ref;
	size_t		 ofset;
	size_t		 ret,size_cur,pos_next;
	unsigned int total_len;
	unsigned int debug;
	file_system *files;

	files	=	find_file_system(fs_name);
	if(files	==PTR_NULL)
	{
		kernel_log	(kernel_log_id,"cannot open file system \n");
		return 0;
	}

	/*
	kernel_log	(kernel_log_id,"opening file on fs ");
	writestr	(tree_mamanger_get_node_name(&files->root));
	writestr	("\n");
	*/

	debug			=0;	

	cur_node.zone	=PTR_NULL;
	next_node.zone	=PTR_NULL;
	new_node.zone	=PTR_NULL;
	data_ref.zone	=PTR_NULL;
	if(path[0]=='/')
	{
		ofset	=	1;
	}
	else
	{
		ofset	=	0;
	}
	
	//total_len	=	strlen_c(path);
	total_len	=	0;
	while(path[total_len]!=0)total_len++;

	copy_zone_ref	(&cur_node,&files->root);
	strcpy_s		(cur_entry,64,tree_mamanger_get_node_name(&cur_node));

	/*
	if(!strcmp_c(path,"/system/imgs/image.png"))
		debug=1;
	*/

	while(ofset<total_len)
	{
		ret			=	strlpos_c(path,ofset,files->delim);
		
		if(path[ofset]!=files->delim)
		{
			int ret_find_child;

			if(ret!=INVALID_SIZE)
				pos_next	=	ret;
			else
				pos_next	=	total_len;

			size_cur			=	(pos_next-ofset);
			if(size_cur>63)size_cur=63;

			memcpy_c				(cur_entry,&path[ofset],size_cur);
			cur_entry[size_cur]	=	0;
			ret_find_child		=	0;

			if(files->image_stream.data.zone!=PTR_NULL)
			{
				ret_find_child	=	find_iso_entry					(files,&cur_node,cur_entry,&next_node);
			}
			else
			{
				ret_find_child	=	tree_node_find_child_by_name	(&cur_node,cur_entry,&next_node);
			}
				
			if(ret_find_child == 0)
			{
				kernel_log	(kernel_log_id,"could not open file ");
				writestr	(cur_entry);
				writestr	("\n");

				release_zone_ref	(&cur_node);
				release_zone_ref	(&next_node);
				return 0;
			}

			copy_zone_ref		(&cur_node,&next_node);
			release_zone_ref	(&next_node);
		}
		ofset		=	pos_next+1;
	}
 
	if(ofset==total_len)
	{
		kernel_log	(kernel_log_id,"file not found ");
		writestr	(cur_entry);
		writestr	("\n");

		return 0;
	}
/*
	kernel_log	(kernel_log_id,"file opened ");
	writestr	(cur_entry);
	writestr	("\n");
*/


	if(files->image_stream.data.zone!=PTR_NULL)
	/*
	{
		tree_mamanger_get_node_data_ref		(&cur_node,&data_ref);
		mem_stream_init						(file_stream,&data_ref,0);
		release_zone_ref					(&data_ref);
	}
	else
	*/
	{	//0x38800 0x4578
		size_t			file_pos;
		size_t			file_size;
		
		//mem_stream_init		(file_stream,&files->image_stream->data);
		//mem_stream_skip_to	(file_stream,tree_manager_get_node_image_pos(&cur_node));
		
		file_pos				=	tree_manager_get_node_image_pos	(&cur_node);
		file_size				=	tree_manager_get_node_image_size(&cur_node);

		mem_stream_init			(file_stream,&files->image_stream.data,file_pos+files->image_stream.buf_ofs);

		/*
		mem_zone_ref	file_ref;
		size_t			file_pos;
		size_t			file_size;
		mem_ptr			file_ptr;
		
		file_ref.zone=PTR_NULL;

		file_pos				=	tree_manager_get_node_image_pos	(&cur_node);
		file_ptr				=   get_zone_ptr					(&files->image_stream.data,file_pos+files->image_stream.buf_ofs);
		file_size				=	tree_manager_get_node_image_size(&cur_node);

		if(create_zone_ref			(&file_ref,file_ptr,file_size)>0)
		{
			mem_stream_init			(file_stream,&file_ref,0);
			release_zone_ref		(&file_ref);
		}
		*/
	}
	else
	{
		writestr("pb \n");
	}


	release_zone_ref		(&cur_node);
	return 1;
}

int file_system_read_file(const char *fs_name,const char *path,mem_stream *file_stream)
{
	
}

OS_API_C_FUNC(unsigned int) file_system_get_dir_entry(const char *fs_name,const char *path,mem_zone_ref *file_out,unsigned int type,unsigned int index)
{
    char		 cur_entry[64];
	mem_zone_ref cur_node,next_node,new_node;
	size_t		 ofset,str_len;
	size_t		 ret;
	size_t		 size_cur;
	int			 ret_find_child;
	size_t		 pos_next;
	file_system *files;

	files	=	find_file_system(fs_name);
	if(files	==PTR_NULL)
		return 0;

	cur_node.zone	=PTR_NULL;
	next_node.zone	=PTR_NULL;
	new_node.zone	=PTR_NULL;
	if(path[0]=='/')
	{
		ofset	=	1;
	}
	else
	{
		ofset	=	0;
	}
	
	copy_zone_ref	(&cur_node,&files->root);
	strcpy_s		(cur_entry,64,tree_mamanger_get_node_name(&cur_node));

	str_len			=	0;
	while(path[str_len]!=0)str_len++;
	
	while(ofset<str_len)
	{
		ret			=	strlpos_c(path,ofset,files->delim);
		if(ret!=INVALID_SIZE)
			pos_next	=	ret;
		else
			pos_next	=	str_len;
				
		if(path[ofset]!=files->delim)
		{
			size_cur			=	(pos_next-ofset);
			memcpy_c				(cur_entry,&path[ofset],size_cur);
			cur_entry[size_cur]	=	0;


			if(files->image_stream.data.zone!=PTR_NULL)
			{
				ret_find_child	=	find_iso_entry					(files,&cur_node,cur_entry,&next_node);
			}
			else
			{
				ret_find_child	=	tree_node_find_child_by_name	(&cur_node,cur_entry,&next_node);
			}

			if(ret_find_child==0)
			{
				release_zone_ref	(&cur_node);
				kernel_log			(kernel_log_id,"unable to find directory \n");
				return 0;
			}
			copy_zone_ref		(&cur_node,&next_node);
			release_zone_ref	(&next_node);
		}
		ofset		=	pos_next+1;
	}

	if(files->image_stream.data.zone!=PTR_NULL)
	{
		ret_find_child	=	get_iso_entry					(files,&cur_node,type,file_out,index);
	}
	else
	{
		ret_find_child	=	tree_node_list_child_by_type	(&cur_node,type,file_out,index);
	}

	release_zone_ref	(&cur_node);

	return ret_find_child;
	
	//tree_node_get_childs_by_type(cur_node,type);
}



void file_system_mods_manager_load_mod()
{


}