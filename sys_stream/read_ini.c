#define STREAM_API C_EXPORT
#include <std_def.h>
#include <std_mem.h>
#include "kern.h"
#include "mem_base.h"
#include "lib_c.h"
#include "tree.h"
#include "sys/read_ini.h"

extern unsigned int		kernel_log_id;



OS_API_C_FUNC(void) copy_without_space	(char *dest,unsigned int dest_len,const char *src,size_t		 start,size_t		 end)
{
	size_t n,cnt;

	n		=	start;
	cnt		=	0;
		
	cnt=0;
	while((n<end)&&((cnt+1)<dest_len)&&(src[n]!='\n'))
	{
		if((src[n]!=' ')&&(src[n]!='\r')&&(src[n]!='"'))
		{
			dest[cnt]=src[n];
			cnt++;
		}
		n++;
	}
	dest[cnt]=0;
}

size_t strlpos_lim_c(const char *string,size_t		 ofset,size_t		 size,char c)
{
	size_t		n;
	size_t		end_read;
	char		sc;

	n=ofset;
	end_read=ofset+size;

	while((sc=string[n])!=0)
	{ 
		if(n>=end_read)return INVALID_SIZE;
		if(sc==c)return n;
		n++;
	}

	return INVALID_SIZE;

}


OS_API_C_FUNC(void) read_ini_file(mem_zone_ref	*ini_datas,mem_zone_ref *tree_out,const char *ini_name)
{
	char		section_name[64];
	unsigned char	*data_ptr;
	mem_zone_ref	root_node;
	mem_zone_ref	current_section_node;
	size_t			cnt,len;
	size_t				pos;

	root_node.zone				=	PTR_NULL;
	current_section_node.zone	=	PTR_NULL;
	len							=	get_zone_size(ini_datas);

	tree_manager_create_node	(ini_name,NODE_INI_ROOT_NODE,&root_node);
	data_ptr	=	get_zone_ptr(ini_datas,0);
	cnt			=	0;

	kernel_log(kernel_log_id,"read ini file \n");
	
	while((pos=strlpos_c(data_ptr,cnt,'\n'))!=INVALID_SIZE)
	{
		size_t		n,len_line,end_line;
		size_t		section_name_start;

		len_line	=pos-cnt;
		end_line	=pos;
		n			=cnt;
		section_name_start=0xFFFFFFFF;

		if(first_char(&data_ptr[n])==';')
			goto next_line;

		while(n<end_line)
		{
			if(data_ptr[n]==';'){break;}
			if(data_ptr[n]=='[')
			{
				if(current_section_node.zone!=PTR_NULL)
				{
					release_zone_ref	(&current_section_node);

					/*
					if( (!strncmp_c(section_name,"PDoLL.reg",9))||
						(!strncmp_c(section_name,"TnT.reg",7))
						)
					{
						tree_manager_dump_node_rec(&current_section_node,0);
					}
					*/
				}

				section_name_start	=n+1;
			}
			if(data_ptr[n]==']')
			{
				size_t					section_name_len;
				


				section_name_len				=	n-section_name_start;
				
				strncpy_c	(section_name,&data_ptr[section_name_start],section_name_len);
				section_name[section_name_len]	=	0;

/*
				kernel_log(kernel_log_id,"new ini section '");
				writestr(section_name);
				writestr("'\n");
				snooze(100000);
				*/
				/*
				if( (!strncmp_c(section_name,"PDoLL.reg",9))||
					(!strncmp_c(section_name,"TnT.reg",7))
					)
				{
					tree_manager_set_output(1);
				}
				else
				{
					tree_manager_set_output(0);
				}
				*/

				tree_manager_add_child_node			(&root_node,section_name,NODE_INI_SECTION_NODE,&current_section_node);


				

				section_name_start				=	0xFFFFFFFF;
			}
			n++;
		}

		
		if((len_line>0)&&(current_section_node.zone!=PTR_NULL))
		{
			size_t		pos_eq;
			size_t		pos_coma;
			
			n		=	cnt;
			pos_eq	=	strlpos_lim_c(data_ptr,n,len_line,'=');
			if(pos_eq!=INVALID_SIZE)
			{
				char				var_name[64];
				char				var_val[1024];
				mem_zone_ref		current_value_node;


				copy_without_space	(var_name,64	,data_ptr,n,pos_eq);
				n=pos_eq+1;
				copy_without_space	(var_val,1024	,data_ptr,n,end_line);

				current_value_node.zone=PTR_NULL;
				
				tree_manager_add_child_node			(&current_section_node,var_name,NODE_INI_VALUE_NODE,&current_value_node);
				tree_manager_write_node_data		(&current_value_node,var_val,0,strlen_c(var_val)+1);

				release_zone_ref(&current_value_node);
			}
			
			else
			{
				mem_zone_ref		current_value_node;
				mem_zone_ref		current_value_data_node;
				char  				r_key[64];
				char  				s_key[64];
				char  				entry[64];
				char  				flags[64];
				char  				val[64];
				
				int   				kidx;

				
				memset_c(r_key,0,64);
				memset_c(s_key,0,64);
				memset_c(entry,0,64);
				memset_c(flags,0,64);
				memset_c(val,0,64);

				
				kidx		=0;
				pos_coma	=0;
				while(n<end_line)
				{
					pos_coma	=	strlpos_lim_c(data_ptr,n,len_line,',');
					
					if(pos_coma==INVALID_SIZE)pos_coma=end_line;

					if(pos_coma>0)
					{
						switch (kidx)
						{
							case 0:
								copy_without_space(r_key,64,data_ptr,n,pos_coma);
							break;
							case 1:
								copy_without_space(s_key,64,data_ptr,n,pos_coma);
							break;
							case 2:
								copy_without_space(entry,64,data_ptr,n,pos_coma);
							break;
							case 3:
								copy_without_space(flags,64,data_ptr,n,pos_coma);
							break;
							case 4:
								copy_without_space(val,64,data_ptr,n,pos_coma);
							break;
						}
					}
					kidx++;
					n=pos_coma+1;
				}

				

				
				if(strlen_c(s_key)>0)
				{
					size_t	 base_key_name;
					unsigned int crc_name;
					
					base_key_name					=	strrpos_c(s_key,'\\');

					/*
					if( (!strncmp_c(section_name,"PDoLL.reg",9))||
						(!strncmp_c(section_name,"TnT.reg",7))
						)
					{
						writeptr(s_key);
						writestr("'");
						writestr(&s_key[base_key_name]);
						writestr("' '");
						writestr(entry);
						writestr("' '");
						writestr(val);
						writestr("'--");
					}
					*/


					current_value_node.zone			=	PTR_NULL;
					current_value_data_node.zone	=	PTR_NULL;

					//writestr(" data 03.01 ");
					//writestr(s_key);

					crc_name=calc_crc32_c(&s_key[base_key_name],32);

					if(tree_manager_find_child_node	(&current_section_node,crc_name,NODE_INI_REGKEY_NODE,&current_value_node)==0)
						tree_manager_add_child_node	(&current_section_node,&s_key[base_key_name],NODE_INI_REGKEY_NODE,&current_value_node);

					if(strlen_c(entry)>0)
					{
						tree_manager_add_child_node			(&current_value_node,entry,NODE_INI_REGVALUE_NODE,&current_value_data_node);
						tree_manager_write_node_data		(&current_value_data_node,val,0,strlen_c(val)+1);
						release_zone_ref					(&current_value_data_node);
					}
					
					//writestr(" data 03.03 ");
					release_zone_ref(&current_value_node);
				}
			}
			
		}
		

		next_line:
			cnt=pos+1;
	}



	kernel_log					(kernel_log_id,"ini end\n");
	copy_zone_ref				(tree_out,&root_node);
	release_zone_ref			(&current_section_node);
	release_zone_ref			(&root_node);

	//tree_manager_dump_node_rec(tree_out,0);

}

OS_API_C_FUNC(int) read_ini_vendor_value(mem_zone_ref	*manuf,unsigned int idx,unsigned short *vid,unsigned short *did,unsigned short *sub_sys_vendor,unsigned short *sub_sys,char *conf_sec_name,unsigned int len_name)
{
	mem_zone_ref		val_node;
	char				*node_dats;
	size_t				node_size,section_name_end;
	size_t				n;

	val_node.zone=PTR_NULL;

	if(tree_manager_get_child_at(manuf,idx,&val_node)!=1)
		return 0;



	node_dats			=	tree_mamanger_get_node_data_ptr	(&val_node,0);
	node_size			=	strlen_c(node_dats);

	*vid				=	0xFFFF;
	*did				=	0xFFFF;

	section_name_end	=strlpos_c(node_dats,0,',');
	
	copy_without_space	(conf_sec_name,len_name,node_dats,0,section_name_end);

	n					=	0;
	while(n<node_size)
	{
		if(!strncmp_c(&node_dats[n],"PCI\\",4))
		{
			size_t		n_pos;
			int			done;

			done=0;
			n		+=	4;
			while(n<node_size)
			{
				n_pos	=	strlpos_c(node_dats,n,'&');

				if(n_pos==INVALID_SIZE)
				{
					n_pos	=node_size;
					done	=1;
				}
					
				if(!strncmp_c(&node_dats[n],"VEN_",4))
					*vid	=	strtos_c(&node_dats[n+4],PTR_NULL,16);
		
				if(!strncmp_c(&node_dats[n],"DEV_",4))
					*did	=	strtos_c(&node_dats[n+4],PTR_NULL,16);

				if(!strncmp_c(&node_dats[n],"SUBSYS_",7))
				{
					unsigned int	subsys_code;
					subsys_code		=strtol_c(&node_dats[n+7],PTR_NULL,16);

					(*sub_sys_vendor)	=subsys_code&0xFFFF;
					(*sub_sys)			=(subsys_code>>16)&0xFFFF;
				}
				n=n_pos+1;
			}
		}
		n++;
	}
	release_zone_ref(&val_node);


	return 1;

}


