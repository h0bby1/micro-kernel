#define STREAM_API C_EXPORT
#include <std_def.h>
#include <std_mem.h>
#include <std_math.h>
#include <kern.h>
#include <mem_base.h>
#include <lib_c.h>
#include <tree.h>
#include <sys/async_stream.h>
#include <sys/stream_dev.h>
#include <sys/mem_stream.h>
#include <sys/file_system.h>
#include <sys/tpo_mod.h>
#include <sys/acpi/acpi.h>
#include <sys/task.h>



unsigned int					kernel_log_id						=	0xFFFFFFFF;

#define get_cyl(a)		(a/36)
#define get_hd(a)		((a%36)/18)
#define get_sec(a)		((a%18)+1)




unsigned int read_ram_disk()
{
	mem_zone_ref			iso_mem;
	int						volume_size;
	mem_ptr					iso_ptr;
	mem_stream				iso_file;
	unsigned int			ret;

	ret						=	0;
	iso_file.data.zone		=	PTR_NULL;
	
	iso_ptr					=	uint_to_mem(0x70000);
	volume_size				=	*((unsigned int *)(mem_add(iso_ptr,8)));

	if(volume_size<=0)
	{
		kernel_log				(kernel_log_id,"bad ram disk volume size ");
		writeint				(volume_size,10);
		writestr				("\n");
		return 0;
	}

	
	create_zone_ref				(&iso_mem,iso_ptr,volume_size);

	
	
	kernel_log					(kernel_log_id,"reading ram disk image ");
	writeint					(volume_size/1024,10);
	writestr					("kb => ");
	writeptr					(get_zone_ptr(&iso_mem,0));
	writestr					("\n");
	

	kernel_log					(kernel_log_id,"mem stream init ");
	writeint					(mem_to_uint(mem_stream_init),16);
	writestr					("\n");

	mem_stream_init			(&iso_file,&iso_mem,0);



	if(open_iso_image		(&iso_file,"ramdisk",2)!=1)
	{
		kernel_log	(kernel_log_id,"could not read iso file ");

		writeptr	(get_zone_ptr(&iso_mem,0));
		writestr	(" ");
		writeptr	(get_zone_ptr(&iso_file.data,0));
		writestr	(get_zone_ptr(&iso_file.data,2048*16));
		writestr	("\n");

		ret = 0;
	}
	else
	{
		ret = 1;
	}

	
	mem_stream_close	(&iso_file);
	

	return ret;
}
/*
called_func	call_funcs[64]									=	{PTR_INVALID};
void reset_callfunc()
{
	int n;
	n=0;
	while(n<64)
	{
		call_funcs[n].func=PTR_INVALID;
		call_funcs[n].data=PTR_INVALID;
		call_funcs[n].time=0x00000000;
		n++;
	}
}


OS_API_C_FUNC(void) add_timer_func		(timer_func_ptr func,void *param,unsigned long time)
{
	int n;

	n=0;
	while(n<64)
	{
		if(call_funcs[n].func==PTR_INVALID)
		{
			call_funcs[n].func=func;
			call_funcs[n].data=param;
			if(time>0)
				call_funcs[n].time=system_time()+time;
			else
				call_funcs[n].time=0;
			return;
		}
		n++;
	}
}


void check_func_calls()
{
	int n;
	unsigned int time;


	time=system_time();

	

	n=0;
	while(n<64)
	{
		if(call_funcs[n].func!=PTR_INVALID)
		{
			timer_func_ptr	func;
			void			*p;

			kernel_log(kernel_log_id," \ncalling function \n");

			if((call_funcs[n].time==0)||(time>=call_funcs[n].time))
			{
				func				=call_funcs[n].func;
				p					=call_funcs[n].data;
				call_funcs[n].func	=PTR_INVALID;
				call_funcs[n].data	=PTR_INVALID;
				func(p);

			}
		}
		n++;
	}
}

OS_INT_C_FUNC(unsigned int) timer_interupt(void *t)
{
	check_func_calls();

	return 0;
}
*/

OS_API_C_FUNC(int) load_tpo_dll(const char *file_system,const char *file_name,tpo_mod_file *mod)
{

	mem_stream								file;
	file.data.zone=PTR_NULL;
	
	
	if(file_system_open_file	(file_system,file_name,&file)!=1)
	{
		kernel_log	(kernel_log_id,"cannot open driver file "); 
		writestr	(file_name);
		writestr	("\n"); 
		return 0;
	}

	tpo_mod_init			(mod);
	tpo_mod_load_tpo		(&file,mod,0);
	mem_stream_close		(&file);
	register_tpo_exports	(mod,PTR_NULL);
	
	return 1;
}



OS_API_C_FUNC(int) run_tpo(const char *file_system,const char *file_name,tpo_mod_file *mod)
{
	mem_stream						file;
	run_func_fn_ptr					run_func;
	int								res;
	file.data.zone=PTR_NULL;
	if(file_system_open_file	(file_system,file_name,&file)!=1)
	{
		kernel_log	(kernel_log_id,"cannot open driver file "); 
		writestr	(file_name);
		writestr	("\n"); 
		return 0xFFFFFFFF;
	}

	tpo_mod_init			(mod);
	tpo_mod_load_tpo		(&file,mod,0);
	mem_stream_close		(&file);

	register_tpo_exports	(mod,PTR_NULL);

	run_func		= get_tpo_mod_exp_addr_name		(mod,"run_func");
	if(run_func==PTR_NULL)
	{
		kernel_log	(kernel_log_id,"cannot find run_func "); 
		writestr	(file_name);
		writestr	("\n"); 
		return 0xFFFFFFFF;
	}

	res=run_func();

	return res;
}



void dump_tpo_mod(const char *mod_name)
{
	unsigned int				n;
	struct kern_mod_t			*lib_c_mod;
	struct kern_mod_fn_t		*lib_c_mod_fn;

	lib_c_mod	=	tpo_get_mod_entry_hash_c		(MOD_HASH("lib_c"));

	draw_car_c	("lib c module  \n");
	draw_car_c	("mod hash		: ");
	draw_dword_c(lib_c_mod->mod_hash);
	draw_car_c	("\n");
	draw_car_c	("num funcs		: ");
	draw_dword_c(lib_c_mod->n_funcs);
	draw_car_c	("\n");
	draw_car_c	("base addr		: ");
	draw_dword_c(lib_c_mod->mod_base_addr);
	draw_car_c	("\n");
	draw_car_c	("string table  : ");
	draw_dword_c(lib_c_mod->string_table_addr);
	draw_car_c	("\n");
	
	n=0;
	while(n<lib_c_mod->n_funcs)
	{
		lib_c_mod_fn	=	tpo_get_fn_entry_idx_c			(MOD_HASH("lib_c"),n);
		if(mem_to_uint(lib_c_mod_fn)!=0xFFFFFFFF)
		{
			draw_car_c	("name [");
			draw_dword_c(lib_c_mod_fn->string_idx);
			draw_car_c	("] '");
			draw_car_c	(((char *)uint_to_mem(lib_c_mod->string_table_addr+lib_c_mod_fn->string_idx)));
			draw_car_c	("'");

			draw_car_c	("0x");
			draw_dword_c(lib_c_mod_fn->section_start_addr);
			
			draw_car_c	(" 0x");
			draw_dword_c(lib_c_mod_fn->func_addr-lib_c_mod_fn->section_start_addr);
			draw_car_c	("\n");
		}
		n++;
	}
}

typedef enum 
{
	STACK			=	0x01,
	MEM32			=	0x02,
	GREG			=	0x03,
	GREG_REF		=	0x04,
	GREG_REF_DISP8	=	0x05,
	GREG_REF_DISP32	=	0x06,
	SREG			=	0x07,
	IMM8			=	0x08,
	IMM32			=	0x09,
}operand_type;



struct op_code_t
{
	unsigned short	prefix;
	unsigned char	size;
	unsigned char	op_code;
	unsigned char	mod_rm;
	operand_type	src_type;
	operand_type	dst_type;
	unsigned char	mod;
	unsigned char	src_reg;
	unsigned char	dst_reg;
	unsigned int	src_imm;
};


void disam_func (unsigned int mod_hash,unsigned int func_hash,unsigned int bytes)
{
	struct kern_mod_fn_t		*fn_infos;
	struct kern_mod_t			*mod_infos;
	mem_ptr						code_addr;
	unsigned int				ofset;
	struct op_code_t			op_codes[64];
	unsigned int				n_opcode;

	mod_infos	=	tpo_get_mod_entry_hash_c			(mod_hash);
	if(mem_to_uint(mod_infos)	==	0xFFFFFFFF)
	{
		draw_car_c("module not found \n");
		return;
	}
	

	fn_infos	=	tpo_get_fn_entry_hash_c			(mod_hash,func_hash);
	if(mem_to_uint(fn_infos)	==	0xFFFFFFFF)
	{
		draw_car_c("function not found \n");
		return;
	}
	


	draw_car_c(" function name : '");
	draw_car_c((char *)(uint_to_mem(mod_infos->string_table_addr+ fn_infos->string_idx)));
	draw_car_c("'\n");

	draw_car_c	("module base    :");
	draw_dword_c(mod_infos->mod_base_addr);
	draw_car_c	("\n");
	draw_car_c	("section addr   :");
	draw_dword_c(fn_infos->section_start_addr);
	draw_car_c	("\n");
	draw_car_c	("function addr  :");
	draw_dword_c(fn_infos->func_addr);
	draw_car_c	("\n");
	draw_car_c	("function ofset :");
	draw_dword_c(fn_infos->func_addr-fn_infos->section_start_addr);
	draw_car_c	("\n");


	ofset		=	0;
	n_opcode	=	0;

	while(ofset<bytes)
	{
		struct op_code_t	  *cur_opcode;

		cur_opcode	=	&op_codes[n_opcode];

		code_addr	=	mem_add(uint_to_mem(fn_infos->func_addr),ofset);

		cur_opcode->op_code= *((unsigned char *)(code_addr));

		draw_byte_c	(cur_opcode->op_code);
		draw_car_c	(" ");

		switch(cur_opcode->op_code)
		{
			default:
				ofset	++;
			break;
			//push ebp
			case 0x55:
				cur_opcode->mod_rm	=0;
				cur_opcode->prefix	=0;
				cur_opcode->size	=1;
				cur_opcode->dst_type=STACK;
				cur_opcode->src_type=GREG;
				cur_opcode->src_reg =5;
				n_opcode++;
				ofset	+=	cur_opcode->size;
			break;
			//mov 
			case 0x8B:
				cur_opcode->mod_rm	=*((unsigned char *)(mem_add(code_addr,1)));
				cur_opcode->prefix	=0;
				cur_opcode->size	=2;

				cur_opcode->mod		=((cur_opcode->mod_rm >> 6) & 0x3);
				
				cur_opcode->dst_type=GREG;
				cur_opcode->dst_reg =((cur_opcode->mod_rm) & 0x7);
				cur_opcode->src_reg	=((cur_opcode->mod_rm >> 3) & 0x7);

				switch(cur_opcode->mod)
				{
					case 0:
						cur_opcode->src_type=GREG_REF;
					break;
					case 1:
						cur_opcode->src_type=GREG_REF_DISP8;
					break;
					case 3:
						cur_opcode->src_type=GREG;
					break;
				}
				n_opcode++;
				ofset	+=	cur_opcode->size;
			break;
			//add r/m16/32	imm8
			case 0x83:
				cur_opcode->mod_rm	=*((unsigned char *)(mem_add(code_addr,1)));
				cur_opcode->prefix	=0;
				cur_opcode->size	=3;

				cur_opcode->mod		=((cur_opcode->mod_rm >> 6) & 0x3);
				cur_opcode->dst_reg	=((cur_opcode->mod_rm >> 3) & 0x7);				

				switch(cur_opcode->mod)
				{
					case 0:
						cur_opcode->dst_type	=GREG_REF;
					break;
					case 3:
						cur_opcode->dst_type	=GREG;
					break;
				}
				cur_opcode->src_type=	IMM8;
				cur_opcode->src_imm =	*((unsigned char *)(mem_add(code_addr,2)));
				n_opcode++;
				ofset	+=	cur_opcode->size;
			break;
		}
		draw_car_c	(" ");
		
	}
	draw_car_c	("\n");

}





/*
OS_API_C_FUNC(void)  my_task()
{
	mem_ptr		semaphore;
	mem_ptr		semaphore_2;


	task_manager_aquire_semaphore	(semaphore_2,0);
	snooze							(1000000);

	while(1)
	{
		task_manager_release_semaphore	(semaphore_2,0);

		task_manager_aquire_semaphore	(semaphore,0);
		writestr						("task aquired sem 1 ! \n");
		snooze							(1000000);
		task_manager_release_semaphore	(semaphore,0);
		writestr						("task released sem 1! \n");

		
		task_manager_aquire_semaphore	(semaphore_2,0);
		}

}
*/





OS_API_C_FUNC(void) sys_init()
{
	tpo_mod_file	ndis_tpo;
	tpo_mod_file	bus_manager_tpo;
	tpo_mod_file	disk_init_tpo;
	struct gdt_t	new_gdt;
	unsigned int i, cpu_freq;
	unsigned int int_mode=1;

	
	writestr("init 1\n");
	

	if (int_mode == 1)
	{
		pic_init_c();

		if (init_apic_c())
		{
			if (init_lapic_c())
			{
				pic_set_irq_mask_c(0xFF);
				
				apic_enable_irq_c(2, APIC_BASE + 0x0, 0);

				
				kernel_log(kernel_log_id, "APIC grab cpu frequency \n");
				cpu_freq = get_cpu_frequency_c();
				kernel_log(kernel_log_id, "cpu frequency : ");
				writeint(cpu_freq, 10);
				kernel_log(kernel_log_id, "MHz\n");
				
				apic_enable_irq_c(1, APIC_BASE + 0x01, 0);
			}
			else
				int_mode = 0;
		}
		else
			int_mode = 0;
	}

	if (int_mode == 0)
	{
		disable_apic_c();

		pic_init_c();
		pic_enable_irq_c(1);
		pic_set_irq_mask_c(~0x01);
		kernel_log(kernel_log_id, "PIC grab cpu frequency \n");
		cpu_freq = get_cpu_frequency_c();
		kernel_log(kernel_log_id, "cpu frequency : ");
		writeint(cpu_freq, 10);
		kernel_log(kernel_log_id, "MHz\n");
		pic_set_irq_mask_c(0x00);
	}

	setup_interupt_c(0x00, PTR_NULL, 0);

	writestr("init 2\n");
	libc_init					();
	writestr("init 3\n");
	

	kernel_log_id			=	get_new_kern_log_id("sysbase:",0x0B);
	
	do_gdt_real_mode			(&new_gdt);

	

/*
	semaphore					=	task_manager_new_semaphore	(1,8);
	semaphore_2					=	task_manager_new_semaphore	(1,8);

	task_manager_aquire_semaphore		(semaphore,0);
	task_manager_new_task				("pouet",my_task,PTR_NULL,0);
	snooze								(10000000);

	while(1)
	{
		task_manager_release_semaphore		(semaphore,0);
		writestr							("main released sem 1\n");

		task_manager_aquire_semaphore		(semaphore_2,0);
		writestr							("main aquired sem 2\n");
		snooze								(10000000);
		task_manager_release_semaphore		(semaphore_2,0);

		task_manager_aquire_semaphore		(semaphore,0);
		writestr							("main aquired sem 1\n");

		snooze								(10000000);

	}*/

	kernel_log					(kernel_log_id, "file system \n");
	init_file_system			();


	kernel_log					(kernel_log_id, "stream system \n");
	async_stream_manager_init	();
	
	

	bus_manager_tpo.data_sections.zone	= PTR_NULL;
	ndis_tpo.data_sections.zone			= PTR_NULL;
	disk_init_tpo.data_sections.zone	= PTR_NULL;

	kernel_log(kernel_log_id, "init ramdisk \n");

	if(read_ram_disk				()==1)
	{
		kernel_log					(kernel_log_id,"ram disk opened \n");

		load_tpo_dll				("ramdisk","/bus_manager.tpo"	,&bus_manager_tpo);
		load_tpo_dll				("ramdisk","/ndis.tpo"			,&ndis_tpo);
		run_tpo						("ramdisk","/disk_init.tpo"		,&disk_init_tpo);
	}
	else
	{
		kernel_log(kernel_log_id,"FATAL COULD NOT OPEN RAM DISK !!!! \n");
	}


}
