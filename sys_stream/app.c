#define STREAM_API C_EXPORT
#include <std_def.h>
#include <std_mem.h>
#include <kern.h>
#include <mem_base.h>
#include <lib_c.h>
#include <sys/mem_stream.h>
#include <sys/tpo_mod.h>
#include <sys/file_system.h>
#include <sys/task.h>

extern unsigned int kernel_log_id;

OS_API_C_FUNC(int) run_app(const char *file_system,const char *file_name,tpo_mod_file *mod,mem_zone_ref_ptr app_data)
{
	mem_stream						file;
	init_func_fn_ptr				init_func;

	file.data.zone=PTR_NULL;
	if(file_system_open_file	(file_system,file_name,&file)!=1)
	{
		kernel_log	(kernel_log_id,"cannot open driver file "); 
		writestr	(file_name);
		writestr	("\n"); 
		return 0xFFFFFFFF;
	}

	tpo_mod_init			(mod);
	tpo_mod_load_tpo		(&file,mod,0);
	mem_stream_close		(&file);


	init_func		= get_tpo_mod_exp_addr_name		(mod,"init_app");
	if(init_func!=PTR_NULL)
	{
		if(!task_manager_new_task(mod->name,init_func,app_data,0))
		{
			kernel_log(kernel_log_id,"init app failed \n");
			return 0;
		}
	}

	return 1;
}