;In the 8086 processor, the IDT resides at a fixed location in memory from address 0x0000 to 0x03ff, 
;and consists of 256 four-byte real mode pointers (256 � 4 = 1024 bytes of memory). In the 80286 and later, 
;the size and locations of the IDT can be changed in the same way as it is done in protected mode, though it 
;does not change the format of it. A real mode pointer is defined as a 16-bit segment address and a 16-bit offset 
;into that segment. A segment address is expanded internally by the processor to 20 bits thus limiting real mode 
;interrupt handlers to the first 1 megabyte of addressable memory. The first 32 vectors are reserved for the processor's 
;internal exceptions, and hardware interrupts may be mapped to any of the vectors by way of a programmable interrupt 
;controller.
;A commonly used x86 real mode interrupt is ?INT 10?, the Video BIOS code to handle primitive screen drawing functions 
;such as pixel drawing and changing the screen resolution.


;8e + [11 | 000 dest_reg |000 source_reg ]
;8e + [11 | 000 dest_reg |000 source_reg ]
;8e + [11 | 000 dest_reg es|011 source_reg bx]
;8e + [11 | 000 dest_reg es|000 source_reg ax]
;8e + [11 | 011 dest_reg ds|011 source_reg bx]
;8e + [11 | 011 dest_reg ds|000 source_reg ax]
;The /r indicates that the following byte is a "Mod R/M" byte. The description of the instruction indicates 
;that we should interpret the Reg/Opcode part as a segment register which will be the destination and the the Mod 
;and R/M parts will indicate the source. Seperating out the bits, Mod is the top two bits (11b), Reg is the next 
;three (000b) and R/M the bottom three bits (000b).	

cpu_eflags				:	dd 0
safe_esp				:	dd 0
flags_save				:	dd 0
ret_ax					:	dw 0
ret_val					:	dd 0
shared_data				:   times 512 dd 0x00


dap:
dap_size				:	db 18h
dap_xxxx				:	db 0
dap_num					:	dw 0
dap_ofs					:	dw 0
dap_seg					:	dw 0
dap_start				:	dq 0
dap_edd					:	dq 0


;In the 8086 processor, the IDT resides at a fixed location in memory from address 0x0000 to 0x03ff, 
;and consists of 256 four-byte real mode pointers (256 � 4 = 1024 bytes of memory). In the 80286 and later, 
;the size and locations of the IDT can be changed in the same way as it is done in protected mode, though it 
;does not change the format of it. A real mode pointer is defined as a 16-bit segment address and a 16-bit offset 
;into that segment. A segment address is expanded internally by the processor to 20 bits thus limiting real mode 
;interrupt handlers to the first 1 megabyte of addressable memory. The first 32 vectors are reserved for the processor's 
;internal exceptions, and hardware interrupts may be mapped to any of the vectors by way of a programmable interrupt 
;controller.
;A commonly used x86 real mode interrupt is ?INT 10?, the Video BIOS code to handle primitive screen drawing functions 
;such as pixel drawing and changing the screen resolution.

addr_real_mode_interupt	:dd 0
addr_interupt			:dd 0
addr_real_mode_int		:dd 0
text_load_i				:db 'interupt vector :',0

[BITS 16]
save_eax:dd 0

test_fn_16bits:

	
	push eax
		mov		eax,cr0
		and		ax ,~1
		mov		cr0,eax		; PE mis a 1 (CR0)

		mov		ax ,	0x1000
		mov		ds ,	ax
		mov		es ,	ax	
	pop eax
	
	mov	[save_eax real_mode_to_addr],	eax	
	
	xor     ax ,	ax
	mov		ss ,	ax
	mov		esp ,	0xFFFF
	
	mov eax	,	[save_eax real_mode_to_addr]
	
	jmp word 0x1000:(here_bios_call real_mode_to_addr)
	here_bios_call:
	
		pushf
		push	word 0x1000
		push	word (addr_ret real_mode_to_addr)
		jmp far [addr_real_mode_interupt real_mode_to_addr]
		
		
		
		
	
	addr_ret:

	pushfd
	pop	dword [cpu_eflags real_mode_to_addr]
	mov [ret_ax real_mode_to_addr],ax	
	
	mov eax,cr0
	or  ax ,1
	mov cr0,eax		; PE mis a 1 (CR0)
		
	mov ax	,	0x10		; segment de donne
	mov es	,	ax
	mov ds	,	ax

			
	jmp	dword 0x08:(fn_here)
ret







[BITS 32]

call_real_mode_interupt_vector_c:
	
	;pushf
	;pop dword[flags_save]
  	
  	cli
  	
  	pusha
		call	 _init_pic_real_mode
		lidt	[orig_idt]
    popa
 
	jmp 0x08:testttt
    testttt:    
    
	mov [safe_esp],esp
	mov esp,0xFFFF
	
	jmp dword 0x30:(test_fn_16bits real_mode_to_addr )
	fn_here:

	mov	ax	,	0x18
	mov	ss	,	ax
	mov esp	,	[safe_esp]		
			
	
	lidt [idtptr]
	jmp 0x08:testtttt
    testtttt:
      
    pusha
		call  _init_pic
	popa
	
	push	dword [cpu_eflags]
	popfd
	
	;test word[flags_save],(1<<9)
	;jz bios_call_no_sti
	;	sti
	;bios_call_no_sti:
ret

load_real_mode_interupt_vector_c:
	
	mov	ax							,0x10
	mov	ds							,ax
	mov eax							,[esp+4]		;interupt number
	shl eax							,2
	add eax							,[orig_idt+2]			; (start of vector list)

	mov	ebx							,dword [eax]
	;shl ebx						,4	
	;and ebx						,0x0FFFFFFF
	
	mov	ax							,0x10
	mov	ds							,ax	
	
	mov	[addr_real_mode_interupt]	,ebx
	mov  [addr_real_mode_int]		,ebx
	
	;mov esi		,text_load_i			
	;call _draw_cars
	
	;mov ecx,[addr_real_mode_interupt]
	;call _draw_dword_hex

	;mov edx		,' '
	;call _draw_car
	
	;mov ecx,[addr_real_mode_int]
	;call _draw_dword_hex
	
	;mov edx		,10
	;call _draw_car
	
	
ret

saved_esp_pppp:dd 0

_do_bios_interupt_c:
	cli
	pusha
	mov  ebp,esp
	
	mov [saved_esp_pppp]	,	esp
	
	mov ax					,	0x48
	mov ss					,	ax
	mov esp					,	0x8000
	
	mov eax,0x13
	int 10h
	
	
			
	;addr_ret_bios_call:
	
	mov ax					,	0x10
	mov ds					,	ax
	mov es					,	ax
	
	mov ax					,	0x18
	mov ss					,	ax	
    mov esp					,	[saved_esp_pppp]
    popa

	sti
ret

