text_lapic									:db 'lapic ',0
text_lapic_id								:db ' id ',0
text_lapic_ver								:db ' ver ',0
text_ioapic									:db 'ioapic ',0
text_ioapic_id								:db ' id ',0
text_ioapic_intbase							:db ' gsi ',0
text_ioapic_ver								:db ' v ',0
text_madt_cpuid								:db 'cpu id ',0
text_madt_lapicid							:db ' lapic id ',0
text_init_apic								:db 'init apic',10, 0
text_madt_done								:db 'parse MADT done',10, 0
text_apic_redir								:db 'src ovrd ', 0
text_apic_redir_gsi							:db ' gsi ', 0
text_apic_redir_Flags						:db ' Flags ', 0

lapic_ptr									:dd 0x0
ioapic_ptr									:dd 0x0
lapic64_ptr									:dq 0x0
numcore										:dd 0x0



%define ARCH_INTERRUPT_BASE		0x20


%define LAPIC_ID 020h
%define LAPIC_VER 030h
%define LAPIC_TPR 080h
%define LAPIC_APR 090h
%define LAPIC_EOI 0B0h
%define LAPIC_SIV 0F0h

%define IOAPIC_ID 0x00	;Get/set the IO APIC's id in bits 24-27. All other bits are reserved.
%define IOAPIC_VER 0x01	;Get the version in bits 0-7. Get the maximum amount of redirection entries in bits 16-23. All other bits are reserved. Read only.
%define IOAPIC_APR 0x02 ;Get the arbitration priority in bits 24-27. All other bits are reserved. Read only.
%define IOAPIC_TBL 0x10 ;redirection table

%define IA32_APIC_BASE_MSR  0x1B
%define IA32_APIC_BASE_MSR_ENABLE 0x800


_detect_apic:

	push ebp
	mov ebp,esp

	mov esi, [rsd_ptr]
	
	mov ecx, [esi+4]
	lea ecx, [esi+ ecx]

	lea esi, [esi+0x24] ; first entry

	loop_detect_cores:

		mov ebx,[esi]

		push DWORD text_rsdt
		call	_draw_car_c
		add esp,4

		push dword 4
		push ebx
		call _draw_ncar_c
		add esp,8

		push DWORD text_nl
		call	_draw_car_c
		add esp,4

		
		cmp DWORD [ebx], 0x43495041 ; "APIC = MADT"
		jne detect_cores_next

			mov eax,[ebx + 0x24]
			mov [lapic_ptr] , eax

			push DWORD text_lapic
			call	_draw_car_c
			add esp,4

			push DWORD [lapic_ptr]    
			call _draw_dword_c
			add esp,4

			push DWORD text_lapic_id
			call _draw_car_c
			add esp,4

			push DWORD LAPIC_ID
			call read_lapic

			push eax
			call _draw_dword_c
			add esp,4

			push DWORD text_lapic_ver
			call	_draw_car_c
			add esp, 4

			push DWORD LAPIC_VER
			call read_lapic

			push eax
			call _draw_dword_c
			add esp,4

			push DWORD text_nl
			call	_draw_car_c
			add esp,4
			
			mov edx,[ebx + 0x4] ; total length
			lea edx,[ebx + edx]

			add ebx, 44		   ; first entry
			
			loop_detect_core_madt:

				cmp byte[ebx],0
				jne loop_detect_core_madt_not_0
					test byte[ebx+4],1
					jz loop_detect_core_madt_next

					push DWORD text_madt_cpuid
					call	_draw_car_c
					add esp,4

					movzx eax, byte[ebx+2]

					push	eax
					call	_draw_byte_c
					add		esp,4

					push DWORD text_madt_lapicid  
					call	_draw_car_c
					add esp,4

					movzx eax, byte[ebx+3]

					push	eax
					call	_draw_byte_c
					add		esp,4
					
					push DWORD text_nl
					call	_draw_car_c
					add esp,4
					
				jmp loop_detect_core_madt_next
				loop_detect_core_madt_not_0:
				
				cmp byte[ebx],1
				jne loop_detect_core_madt_not_1

					mov eax, [ebx + 4]
					mov [ioapic_ptr], eax

					push DWORD text_ioapic
					call	_draw_car_c
					add esp,4

					push dword [ioapic_ptr]
					call _draw_dword_c
					add esp,4

					push DWORD text_ioapic_intbase
					call	_draw_car_c
					add esp,4

					push DWORD [ebx + 8]
					call _draw_dword_c
					add esp,4

					push DWORD text_ioapic_id
					call	_draw_car_c
					add esp,4

					push dword IOAPIC_ID
					call read_ioapic

					push eax
					call _draw_dword_c
					add esp,4

					push DWORD text_ioapic_id
					call	_draw_car_c
					add esp,4

					movzx eax, byte[ebx]
					push eax
					call _draw_byte_c
					add esp,4

					push DWORD text_ioapic_ver
					call	_draw_car_c
					add esp,4

					push dword IOAPIC_VER
					call read_ioapic

					push eax
					call _draw_dword_c
					add esp,4

					push DWORD text_nl
					call	_draw_car_c
					add esp,4

				jmp loop_detect_core_madt_next
				loop_detect_core_madt_not_1:

				cmp byte[ebx],2
				jne loop_detect_core_madt_not_2

					push dword text_apic_redir
					call	_draw_car_c
					add esp,4

					movzx eax, byte [ebx + 2] ;Bus Source
					push eax
					call _draw_byte_c
					add esp,4

					movzx eax, byte [ebx + 3] ;IRQ Source
					push eax
					call _draw_byte_c
					add esp,4
					
					push dword text_apic_redir_gsi 
					call	_draw_car_c
					add esp,4

					mov   eax, [ebx + 4] ;Global System Interrupt
					push eax
					call _draw_dword_c
					add esp,4

							
					push dword text_apic_redir_Flags
					call	_draw_car_c
					add esp,4

					movzx eax, word [ebx + 8] ;Flags (see below)
					push eax
					call _draw_dword_c
					add esp,4
					
					push DWORD text_nl
					call	_draw_car_c
					add esp,4

				jmp loop_detect_core_madt_next
				loop_detect_core_madt_not_2:


				cmp byte[ebx],5
				jne loop_detect_core_madt_not_5

					mov eax, [ebx + 4]
					mov [lapic64_ptr], eax

					mov eax, [ebx + 8]
					mov [lapic64_ptr+4], eax

				jmp loop_detect_core_madt_next
				loop_detect_core_madt_not_5:

				loop_detect_core_madt_next:


			movzx eax, byte[ebx+1]
			add ebx, eax
			cmp ebx, edx
			jl loop_detect_core_madt

			jmp detect_cores_end

		detect_cores_next:

		cmp byte[rsd_ptr] , 'X'
		jne detect_cores_not_x
			add esi, 8
		jmp detect_cores_x_ok
		detect_cores_not_x:
			add esi, 4
		detect_cores_x_ok:

	cmp esi, ecx
	jle loop_detect_cores

	detect_cores_end:

	mov esp,ebp
	pop ebp


	push DWORD text_madt_done
	call _draw_car_c
	add esp,4


ret

read_ioapic:
	mov eax,[esp+4]
	and eax,0xFF
	mov edi, [ioapic_ptr]
	mov [edi],eax ; select the register
	mov eax,[edi+0x10]
ret 4

write_ioapic:
	mov eax,[esp+4]
	and eax,0xFF
	mov edi, [ioapic_ptr]
	mov [edi],eax ; select the register

	mov eax,[esp+8]
	mov [edi+0x10],eax
ret 8

read_lapic:
	mov edi, [lapic_ptr]
	add edi, [esp+4]
	mov eax, [edi]
ret 4

write_lapic:
	mov edi, [lapic_ptr]
	add edi, [esp+4]
	mov [edi],eax
ret 4

_apic_enable_irq_c:
	
	push ebp
	mov ebp,esp

	push edx
	push ecx
	push edi
	
	mov edx,[ebp+8]
	shl edx,1
	add edx,IOAPIC_TBL
	add edx,1

	mov eax,[ebp+16]
	shl eax,24

	push eax
	push edx
	call write_ioapic

	dec edx

	push dword [ebp+12]
	push edx
	call write_ioapic

	pop edi
	pop ecx
	pop edx


	mov esp,ebp
	pop ebp
	
	mov eax,1

ret


;--------------------------
apic_end_of_interrupt:

	xor eax,eax
	push LAPIC_EOI
	call write_lapic
ret


_imcr_to_pic:
	;select IMCR register 

	mov dx,0x22
	mov al,0x70
	out dx,al
	
	mov dx ,0x23
	mov al,0x00
	out dx,al
ret


_imcr_to_apic:
	;select IMCR register 

	mov dx,0x22
	mov al,0x70
	out dx,al
	
	mov dx ,0x23
	mov al,0x01
	out dx,al
ret

_init_lapic_c:
	
	cmp dword [lapic_ptr],0
	jne _init_lapic_ptr_ok
		xor eax,eax
		ret
	_init_lapic_ptr_ok:

	push ebp
	mov ebp,esp
	
	push edi

	push LAPIC_SIV
	call read_lapic
	
	and eax , ~0xFF
	or eax , 0x100	; enabled
	or eax , 0x200  ; SPIV_FOCUS_DISABLED
	or eax , 0xFF

	push LAPIC_SIV
	call write_lapic

	xor eax,eax
	push LAPIC_TPR
	call write_lapic
	
	pop edi
	
	mov esp,ebp
	pop ebp

	mov eax,1

ret

_disable_apic_c:
	cmp dword[ioapic_ptr],0
	jne _disable_apic_c_ptr_ok
		xor eax,eax
		ret
	_disable_apic_c_ptr_ok:


	push ebp
	mov ebp,esp

	push ecx
	push edx
	push ebx

	cli

	call _imcr_to_pic

	mov ecx,IA32_APIC_BASE_MSR
	RDMSR
	and eax , ~IA32_APIC_BASE_MSR_ENABLE
	WRMSR

	mov dword[interupt_mode],0

	sti

	pop ebx
	pop edx
	pop ecx

	mov esp,ebp
	pop ebp

	mov eax,1
ret

_init_apic_c:

	cmp dword[ioapic_ptr],0
	jne _init_ioapic_ptr_ok
		xor eax,eax
		ret
	_init_ioapic_ptr_ok:

	push ebp
	mov ebp,esp

	
	push esi
	push ecx
	push edx
	push ebx

	mov esi, text_init_apic
	call _draw_cars

	cli

	call _imcr_to_apic
	
    mov ecx,IA32_APIC_BASE_MSR
	RDMSR

	mov ebx,eax

		push edx
		call _draw_dword_c
		add esp,4

		mov eax,ebx
		and eax, 0xffff1000
		push eax
		call _draw_dword_c
		add esp,4

		push DWORD text_nl
		call _draw_car_c
		add esp,4

	mov eax,ebx

	and eax,  0xffff1000
	or eax , IA32_APIC_BASE_MSR_ENABLE

	WRMSR

	mov dword[interupt_mode],1

	sti

	pop ebx
	pop edx
	pop ecx
	pop esi

	mov esp,ebp
	pop ebp
	
	mov eax,1

	;byte* ptr = (byte*)(p + 0xf0);
	;(*((uint32_t*)ptr)) = 39 | 0x100;
   
ret


set_apic_interupt_mask:
ret


reset_apic_interupt_mask:
ret


_apic_set_irq_mask_c:
ret

