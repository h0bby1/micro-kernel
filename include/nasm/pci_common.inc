
_read_dword_pci:

   cmp dword [pci_drv_type],1
   jne rdtype2
	  call _read_dword_pci_type1
	  jmp read_dword_pci_end
   rdtype2:

   cmp dword [pci_drv_type],2
   jne read_dword_pci_end
   
     call _read_dword_pci_type2

read_dword_pci_end:

ret

_read_word_pci:


   cmp dword [pci_drv_type],1
   jne rwtype2
	  call _read_word_pci_type1
	  jmp read_word_pci_end
   rwtype2:

   cmp dword [pci_drv_type],2
   jne read_word_pci_end
   
     call _read_word_pci_type2

read_word_pci_end:

ret

_read_byte_pci:


   cmp dword [pci_drv_type],1
   jne rbtype2
	  call _read_byte_pci_type1
	  jmp read_byte_pci_end
   rbtype2:

   cmp dword [pci_drv_type],2
   jne read_byte_pci_end
   
     call _read_byte_pci_type2

read_byte_pci_end:

ret


_write_dword_pci:

   cmp dword [pci_drv_type],1
   jne wdtype2
	  call _write_dword_pci_type1
	  jmp write_dword_pci_end
   wdtype2:

   cmp dword [pci_drv_type],2
   jne write_dword_pci_end
   
     call _write_dword_pci_type2

write_dword_pci_end:

ret


_write_word_pci:

   cmp dword [pci_drv_type],1
   jne wwtype2
	  call _write_word_pci_type1
	  jmp write_word_pci_end
   wwtype2:

   cmp dword [pci_drv_type],2
   jne write_word_pci_end
   
     call _write_word_pci_type2

write_word_pci_end:

ret

_write_byte_pci:

   cmp dword [pci_drv_type],1
   jne wbtype2
	  call _write_byte_pci_type1
	  jmp write_byte_pci_end
   wbtype2:

   cmp dword [pci_drv_type],2
   jne write_byte_pci_end
   
     call _write_byte_pci_type2

write_byte_pci_end:

ret


_calc_pci_config_addr:

   cmp dword [pci_drv_type],1
   jne catype2
	  call _calc_pci_config_addr_type1
	  jmp ca_pci_end
   catype2:

   cmp dword [pci_drv_type],2
   jne ca_pci_end
   
     call _calc_pci_config_addr_type2

ca_pci_end:


ret