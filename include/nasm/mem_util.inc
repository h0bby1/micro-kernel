;Figure 30-3: Example Data Segment Descriptor
;7   6   5   4   3   2   1   0   
;Upper Byte of Base Address
;0   0   0   0   0   0   0   0		: Byte 7
;G   B   0   AVL Upper Digit of Limit
;0   1   0   0   0   0   0   0		: Byte 6
;P   [DPL]   S   D/C E   W   A
;1   1   0   1   0   0   1   1		: Byte 5
;3rd Byte of Base Address
;0   0   0   0   1   0   0   0		: Byte 4
;2nd Byte of Base Address
;0   0   1   1   1   1   1   0		: Byte 3
;1st Byte of Base Address
;1   0   1   0   0   0   0   0 		: Byte 2
;2nd Byte of Limit
;0   1   1   1   1   0   1   1  		: Byte 1
;1st Byte of Limit
;0   0   1   1   1   1   1   0		: Byte 0
;------------------------------------------------
;G Bit: Granularity bit defines meaning of limit value (0 = length of segment in bytes)
;B Bit: In stack segment, the B Bit defines the SP Size. It also identifies the upper boundary of an expanded down stack segment. 1 = 32-bit SP (ESP) and upper limit of FFFFFFFFh.
;AVL Bit: Available for use by system software.
;P Bit: Segment present bit. (Must be 1 if the data segment is present in memory)
;DPL field: Descriptor Privilege Level = 2
;S Bit: System Bit. When 0, indicates system segment. Must be 1 in a data segment descriptor.
;D/C : This could be called the data / code bit. 0 Indicates a data segment.
;E Bit: Expand Down Bit. Will be 0 in a data segment descriptor.
;W Bit: Write-enable bit. A 1 indicates a read/write enabled segment.
;A Bit: Accessed bit. Set to 1 by the processor when a data segment is accessed.
;Segment Base Address: Base address of the segment is 00083EA0h.
;Segment Size: The size of the segment is 07B3Eh, or 31,550d.


;P   [DPL]   S   D/C E   W   A
%define CS_ACCES    10011010b
%define DS_ACCES    10010010b
%define SS_ACCES	10010010b
%define HZ			100



%define VGA_BIOS_START	0xC000
%define VGA_BIOS_SIZE	0x8000


;--------------------------------------------------------------------
; descInit
;--------------------------------------------------------------------
%macro descInit	5	; base(32),limite(20/32),acces(8),flags(4/8),adresse(32)
	
; base :
    mov eax			,%1
	mov word [%5+2] ,ax
	shr eax			,16
	mov byte [%5+4] ,al
	mov byte [%5+7] ,ah
; limite :
	mov eax        ,%2
	and eax        ,0x000FFFFF
	mov word [%5]  ,ax		; ecrit (0..15)
	shr eax        ,16		; place (16..19) sur le nibble inferieur
	mov byte [%5+6],0		; initialise flags+lim(16..19) a 0
	or [%5+6]      ,al	    ; ecrit (16..19)
; flags :
	mov al         ,%4
	and al         ,0x0F
	shl al         ,4
	or [%5+6]      ,al
; acces :
	mov byte [%5+5],%3
	
%endmacro
;--------------------------------------------------------------------

%define kern_base 0x10000
%define kern_org  0x10000
%define start_seg 0x00000
%define REAL_MODE_STACK_START 0x1000

%define addr_to_real_mode	+start_seg
%define real_mode_to_addr  -kern_org

