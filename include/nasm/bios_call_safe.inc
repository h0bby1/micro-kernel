
safe_ret:dd 0


call_real_mode_interupt_vector_c_safe:
		
	mov [safe_esp],esp
	jmp dword 0x30:(test_fn_16bits_safe addr_to_real_mode)
	fn_here_safe:
	
	mov esp,[safe_esp]
		
	call init_pmode_data_segs
	
	push	dword [cpu_eflags]
	popfd
	
	jmp dword [safe_ret]

ret


;[sec_num]
;[cyl_num]
;[hd_num]	
bios_read_sector:

	
	retry_sec:
	
		mov ax,0x38
		mov es,ax
		mov ds,ax	
	
        mov ah,2				;read sector

        mov cl,[sec_num addr_to_real_mode]		;sector number
        mov ch,[cyl_num addr_to_real_mode]		;cyl number
        mov dh,[hd_num- addr_to_real_mode]		;head number
        mov dl,[drv_num addr_to_real_mode]		;drive number
	    mov al,1						;read 1 sector
    
        lea	bx	,[sector_data addr_to_real_mode]
        mov di	,bx
        
		mov ax,0x10
		mov es,ax
		mov ds,ax	
        
        jmp dword	0x08:call_real_mode_interupt_vector_c_safe

        jnc no_error
			jmp retry_sec
 	    no_error:
   

ret


test_fn_16bits_2:

	mov ax,0x38
	mov es,ax
	mov ds,ax
	mov ss,ax
	

	;mov word[save_esp_dat addr_to_real_mode],0xCCCC
	;mov sp,0x2000
	;lea di,[save_esp_dat]
	;mov word[save_esp_dat],0xAABB
	;call dword 0x30:(testtt addr_to_real_mode)
	

	
	
	vv:
		
	;cli
		mov ax,01H

		pushfd
		;push	dword (addr_ret_ addr_to_real_mode)

		call dword 0x40:0x3E
		;int 10h
		
				
		;pushf
		;push	cs
		;mov [save_esp],esp
		;mov		sp,0x100
		;call testtt
		;mov [save_esp],esp
			
	addr_ret_:
	;sti
			
	mov ax,0x10
	mov es,ax
	mov ds,ax
	
	
	mov ax,0x18
	mov ss,ax	
		
	jmp	dword 0x08:(fn_here)
ret


test_fn_16bits_safe:

push eax	
	mov		eax,cr0
	and		ax ,~1
	mov		cr0,eax		; PE mis a 1 (CR0)

	mov		ax ,	0x1000
	mov		ds ,	ax
	mov		es ,	ax	
	pop eax
	
	
	mov		[save_eax addr_to_real_mode],	eax
	mov     ax					,	0x0
	mov		ss					,	ax
	mov		sp					,	0x1000
	mov eax						,	[save_eax addr_to_real_mode]
	
	
	jmp word 0x1000:(here_bios_call_safe addr_to_real_mode)
	here_bios_call_safe:
	

	;cli
		%if 1
		pushf
		push	word 0x1000
		push	word (addr_ret_safe addr_to_real_mode)
		
		les bx,[0x10000]
			
		jmp far [addr_real_mode_interupt addr_to_real_mode]
		;int 13h
		%endif		
		
	addr_ret_safe:

	;pushfd
	;pop	dword [cpu_eflags addr_to_real_mode]
	
	;sti
	
	
	push eax
		mov eax,cr0
		or  ax ,1
		mov cr0,eax		; PE mis a 1 (CR0)
		
		mov		ax ,	0x10
		mov		ds ,	ax
		mov		es ,	ax			
		mov		fs ,	ax	
		mov		gs ,	ax
		
		mov		ax ,	0x18
		mov		ss	,	ax
	pop eax
	
	jmp	dword 0x08:(fn_here_safe)
ret


%if 0
check_vga_bios:
	
	lea esi		,[vga_bios]
	xor cx,cx
	loop_cpy:
		
	
		%if 1
		;mov to segment register opcode
		cmp byte [esi],0x8E		;	
		jne not_mov_seg
			mov word [esi]	,0x9090		;fill the next two byte with nop opcode
			add esi			,2
			jmp end_loop_cpy
		not_mov_seg:
		
		cmp byte [esi],0xC4		;	les
		jne not_mov_segx
			mov word [esi]	,0x9090		;fill the next two byte with nop opcode
			add esi			,2
			jmp end_loop_cpy
		not_mov_segx:
				
		cmp byte [esi],0xC5		;	lds
		jne not_mov_segxx
			mov word [esi]	,0x9090		;fill the next two byte with nop opcode
			add esi			,2
			jmp end_loop_cpy
		not_mov_segxx:
		%endif

		%if 1
		cmp word [esi],0xC000
		jne not_vga_data_00
			mov word [esi]	,0x0000	
			add esi			,2
			jmp end_loop_cpy
		not_vga_data_00:
		%endif
		
		inc esi
		
		end_loop_cpy:
		inc cx
		cmp cx,0x8000
	jl loop_cpy

ret



verif:
	mov esi,0xC0000
	mov edi,vga_bios
	mov ecx,0
	
	loop_ver:
		mov eax		 ,[esi]
		cmp [edi]	 ,eax
		mov [err_fnd],ecx
		jne error_fnd
		
		
		add edi,4
		add esi,4
		
	inc ecx
	cmp ecx,VGA_BIOS_SIZE
	shr ecx,2
	jl loop_ver
	ret
		
	error_fnd:
	
	mov esi,text_kern_imp_dll
	call _draw_cars

	mov edx,' '
	call _draw_car	

	mov ecx,[err_fnd]
	call _draw_dword_hex
	
	mov edx,10
	call _draw_car
		
ret	

_bios_reset_drives_c:
	pusha
	
    push  dword 0x13
    call  load_real_mode_interupt_vector_c  
    add   esp,4
   	
	lea eax	,[bios_reset_drives_ret]
    mov dword [safe_ret],eax
    
	jmp		dword 0x30:(bios_reset_disk addr_to_real_mode)
	
	bios_reset_drives_ret:
	
	popa

ret

%endif

