
text_cdc_ready:db 'codec ready ',0
text_cdc_eready:db 'codec ready err',0

text_cdc_ok:db 'codec ok ',0
text_cdc_init:db 'codec init ',0
text_cdc_err:db 'codec error ',0
text_cdc_warn:db 'codec warning ',0
text_cdc_busy:db 'codec busy ',0
text_cdc_inv:db 'codec invalid ',0

text_cdc_cant_read:db 'codec cant read ',0

text_cdc_rst_fail:db 'codec reset failed',0
text_cdc_rst_ok:db 10,'codec reset ok ',10,0

text_cdc_rate_fail:db 'codec rate test failed',0
text_cdc_rate_ok:db 'codec rate test ok',0




%define VIA_REG_AC97        0x80
%define VIA_REG_AC97_BUSY	0x1000000

%define VIA_ACLINK_STAT		0x40
%define  VIA_ACLINK_C00_READY	0x01 ; primary codec ready

%define VIA_ACLINK_CTRL		0x41

%define VIA_FUNC_ENABLE		0x42
%define  VIA_FUNC_ENABLE_SB	0x01
%define  VIA_FUNC_ENABLE_FM	0x04
%define VIA_PNP_CONTROL		0x43

%define VIA_FUNC_INIT VIA_FUNC_ENABLE_SB|VIA_FUNC_ENABLE_FM


%define  VIA_ACLINK_CTRL_PCM	   0x04 ; 0: disable PCM, 1: enable PCM 
%define  VIA_ACLINK_CTRL_VRA	   0x08 ; 0: disable VRA, 1: enable VRA
%define  VIA_ACLINK_CTRL_SYNC	   0x20 ; 0: release SYNC, 1: force SYNC hi
%define  VIA_ACLINK_CTRL_RESET	   0x40 ; 0: assert, 1: de-assert 
%define  VIA_ACLINK_CTRL_ENABLE	   0x80 ; 0: disable, 1: enable 
%define   VIA_REG_OFFSET_TABLE_PTR   0x04 ; dword - channel table pointer */


%define VIA_REG_OFFSET_STATUS		0x00	; byte - channel status */
%define   VIA_REG_STAT_ACTIVE		0x80	; RO */
%define   VIA_REG_STAT_PAUSED		0x40	; RO */
%define   VIA_REG_STAT_TRIGGER_QUEUED	0x08	; RO */
%define   VIA_REG_STAT_STOPPED		0x04	; RWC */
%define   VIA_REG_STAT_EOL		0x02	; RWC */
%define   VIA_REG_STAT_FLAG		0x01	; RWC */


%define VIA_REG_OFFSET_CONTROL		0x01	; byte - channel control */
%define   VIA_REG_CTRL_START		0x80	; WO */
%define   VIA_REG_CTRL_TERMINATE	0x40	; WO */
%define   VIA_REG_CTRL_AUTOSTART	0x20
%define   VIA_REG_CTRL_PAUSE		0x08	; RW */
%define   VIA_REG_CTRL_INT_STOP		0x04		
%define   VIA_REG_CTRL_INT_EOL		0x02
%define   VIA_REG_CTRL_INT_FLAG		0x01
%define   VIA_REG_CTRL_RESET		0x01	; RW - probably reset? undocumented */
%define   VIA_REG_CTRL_INT (VIA_REG_CTRL_INT_FLAG | VIA_REG_CTRL_INT_EOL | VIA_REG_CTRL_AUTOSTART)
%define  VIA_ACLINK_CTRL_INIT	VIA_ACLINK_CTRL_ENABLE | VIA_ACLINK_CTRL_RESET | VIA_ACLINK_CTRL_PCM | VIA_ACLINK_CTRL_VRA

%define VIA_REG_OFS_PLAYBACK_VOLUME_L	0x02	; byte */
%define VIA_REG_OFS_PLAYBACK_VOLUME_R	0x03	; byte */
%define VIA_REG_OFS_MULTPLAY_FORMAT	0x02	    ; byte - format and channels */
%define   VIA_REG_MULTPLAY_FMT_8BIT	0x00      
%define   VIA_REG_MULTPLAY_FMT_16BIT	0x80
%define   VIA_REG_MULTPLAY_FMT_CH_MASK	0x70	; # channels << 4 (valid = 1,2,4,6) */
%define VIA_REG_OFS_CAPTURE_FIFO	0x02	; byte - bit 6 = fifo  enable */
%define   VIA_REG_CAPTURE_FIFO_ENABLE	0x40


old_legacy:db 0
old_legacy_cfg:db 0




%define AC97_PCM_LFE_DAC_RATE	0x30	; PCM LFE DAC Rate */
%define AC97_PCM_FRONT_DAC_RATE 0x2c	; PCM Front DAC Rate */
%define AC97_REC_GAIN		0x1c	; Record Gain */
%define AC97_VENDOR_ID1		0x7c	; Vendor ID1 
%define AC97_VENDOR_ID2		0x7e	; Vendor ID2 / revision 
%define AC97_EXTENDED_ID	0x28	; Extended Audio ID */

%define AC97_RESET	 	0x00	; Reset */
%define AC97_MASTER		0x02	; Master Volume */
%define AC97_HEADPHONE		0x04	; Headphone Volume (optional) */
%define AC97_MASTER_MONO	0x06	; Master Volume Mono (optional) */
%define AC97_POWERDOWN		0x26	; Powerdown control / status */
%define AC97_PCM		0x18	    ; PCM Volume 
%define AC97_CENTER_LFE_MASTER	0x36	; Center + LFE Master Volume 
%define AC97_GENERAL_PURPOSE	0x20	; General Purpose (optional) */
%define AC97_EXTENDED_STATUS	0x2a	; Extended Audio Status and Control */
        

%define   VIA_REG_AC97			0x80	; dword */
%define   VIA_REG_AC97_CODEC_ID_MASK	(3<<30)
%define   VIA_REG_AC97_CODEC_ID_SHIFT	30
%define   VIA_REG_AC97_CODEC_ID_PRIMARY	0x00
%define   VIA_REG_AC97_CODEC_ID_SECONDARY 0x01
%define   VIA_REG_AC97_SECONDARY_VALID	(1<<27)
%define   VIA_REG_AC97_PRIMARY_VALID	(1<<25)
%define   VIA_REG_AC97_BUSY		(1<<24)
%define   VIA_REG_AC97_READ		(1<<23)
%define   VIA_REG_AC97_CMD_SHIFT	16
%define   VIA_REG_AC97_CMD_MASK		0x7e
%define   VIA_REG_AC97_DATA_SHIFT	0
%define   VIA_REG_AC97_DATA_MASK	0xffff

%define   VIA_REG_OFFSET_STOP_IDX	0x08	    ; dword - stop index, channel type, sample rate */
  %define   VIA8233_REG_TYPE_16BIT	0x00200000	; RW */
  %define   VIA8233_REG_TYPE_STEREO	0x00100000	; RW */


;static int snd_via82xx_codec_valid(via82xx_t *chip, int secondary)
;{
;	unsigned int timeout = 1000;	/* 1ms */
;	unsigned int val, val1;
;	unsigned int stat = !secondary ? VIA_REG_AC97_PRIMARY_VALID :
;					 VIA_REG_AC97_SECONDARY_VALID;
	
;	while (timeout-- > 0) {
;		val = snd_via82xx_codec_xread(chip);
;		val1 = val & (VIA_REG_AC97_BUSY | stat);
;		if (val1 == stat)
;			return val & 0xffff;
;		udelay(1);
;	}
;	return -EIO;
;}



via_codec_valid:

    mov  dword [counter3],0
    loop_via_codec_valid:
      
        call _nop

        mov  ecx,VIA_REG_AC97_PRIMARY_VALID
        or   ecx,VIA_REG_AC97_BUSY
  
        mov dx,[pci_config+16]  
        and dx,0xFFFC	        ; grab address start from pci config
        add dx,VIA_REG_AC97	    ; add via ac97 reg offset 
        in  eax,dx
       
        and ecx,eax
        cmp ecx,VIA_REG_AC97_PRIMARY_VALID
        je vald_ok
        inc dword [counter3]
        cmp dword [counter3],10
        jl loop_via_codec_valid

        mov esi,text_cdc_inv
        call _draw_cars
        mov eax,0xFFFF
ret
      vald_ok:
      and eax,0xFFFF
ret



;static void snd_via82xx_codec_write(ac97_t *ac97,
;				    unsigned short reg,
;				    unsigned short val)
;{
;	via82xx_t *chip = ac97->private_data;
;	unsigned int xval;
;	xval = !ac97->num ? VIA_REG_AC97_CODEC_ID_PRIMARY : VIA_REG_AC97_CODEC_ID_SECONDARY;
;	xval <<= VIA_REG_AC97_CODEC_ID_SHIFT;
;	xval |= reg << VIA_REG_AC97_CMD_SHIFT;
;	xval |= val << VIA_REG_AC97_DATA_SHIFT;
;	snd_via82xx_codec_xwrite(chip, xval);
;	snd_via82xx_codec_ready(chip, ac97->num);
;}


; in ecx     => reg to access
; in [value] => value to write

via_codec_write:

   mov ebx, VIA_REG_AC97_CODEC_ID_PRIMARY 
   shl ebx, VIA_REG_AC97_CODEC_ID_SHIFT;

   mov eax,ecx
   shl eax,VIA_REG_AC97_CMD_SHIFT;

   or ebx,eax

   mov eax,[value]
   shl eax,VIA_REG_AC97_DATA_SHIFT

   or ebx,eax

   mov dx,[pci_config+16]  
   and dx,0xFFFC	    ; grab address start from pci config
   add dx,VIA_REG_AC97	    ; add via ac97 reg offset 
   mov eax,ebx
   out dx,eax


   call via_codec_ready
 


ret



;static unsigned short snd_via82xx_codec_read(ac97_t *ac97, unsigned short reg)
;{
;	via82xx_t *chip = ac97->private_data;
;	unsigned int xval, val = 0xffff;
;	int again = 0;

;	xval  = ac97->num << VIA_REG_AC97_CODEC_ID_SHIFT;
;	xval |= ac97->num ? VIA_REG_AC97_SECONDARY_VALID : VIA_REG_AC97_PRIMARY_VALID;
;	xval |= VIA_REG_AC97_READ;
;	xval |= (reg & 0x7f) << VIA_REG_AC97_CMD_SHIFT;
;     	while (1) {
;      		if (again++ > 3) {
;			snd_printk(KERN_ERR "codec_read: codec %i is not valid [0x%x]\n", ac97->num, 			;snd_via82xx_codec_xread(chip));
;		      	return 0xffff;
;		}
;		snd_via82xx_codec_xwrite(chip, xval);
;		udelay (20);
;		if (snd_via82xx_codec_valid(chip, ac97->num) >= 0) {
;			udelay(25);
;			val = snd_via82xx_codec_xread(chip);
;			break;
;		}
;	}
;	return val & 0xffff;
;}

;in ecx register to read
;in ebx ac97 num


via_codec_read:


   mov ebx,0
   shl ebx,VIA_REG_AC97_CODEC_ID_SHIFT
   or  ebx,VIA_REG_AC97_PRIMARY_VALID;
   or  ebx,VIA_REG_AC97_READ

   mov eax,ecx
   and eax,0x7f
   shl eax,VIA_REG_AC97_CMD_SHIFT

   or ebx,eax

   mov [value],ebx


   mov dword [counter2],0

   loop_via_codec_read:
        

       mov dx,[pci_config+16]  
       and dx,0xFFFC	    ; grab address start from pci config
       add dx,VIA_REG_AC97	    ; add via ac97 reg offset 
       mov eax,[value]
       out dx,eax
       call _nop
       
       call via_codec_valid
       cmp eax,0xFFFF
       jne codec_valid
       
       inc dword [counter2]
       cmp dword [counter2],3
    jle loop_via_codec_read

    mov esi,text_cdc_cant_read
    call _draw_cars

    mov eax,0xFFFF

ret


codec_valid:

    call _nop

    mov dx ,[pci_config+16]  
    and dx ,0xFFFC				; grab address start from pci config
    add dx ,VIA_REG_AC97	    ; add via ac97 reg offset 
    in eax ,dx
    and eax,0xFFFF

ret

;static int snd_via82xx_codec_ready(via82xx_t *chip, int secondary)
;{
;	unsigned int timeout = 1000;	/* 1ms */
;	unsigned int val;
	
;	while (timeout-- > 0) {
;		udelay(1);
;		if (!((val = snd_via82xx_codec_xread(chip)) & VIA_REG_AC97_BUSY))
;			return val & 0xffff;
;	}
;	snd_printk(KERN_ERR "codec_ready: codec %i is not ready [0x%x]\n", secondary, snd_via82xx_codec_xread(chip));
;	return -EIO;
;}

via_codec_ready:


    mov dword[counter2],0

    loop_cdc_ready:

     mov dx,[pci_config+16]  
     and dx,0xFFFC		     ; grab address start from pci config
     add dx,VIA_REG_AC97	 ; add via ac97 reg offset 
     in eax ,dx              ; get value from the register
         
     mov [value],eax
         
     test eax,VIA_REG_AC97_BUSY
     jz cdc_ready
     
     call _nop

     inc dword[counter2]
     cmp dword[counter2],10

   jl loop_cdc_ready

   mov esi,text_cdc_eready
   call _draw_cars

   mov eax,0
   ret

   cdc_ready:
   

;   mov esi,text_cdc_ready
;   call _draw_cars


    mov eax,1
ret

;static void snd_via82xx_channel_reset(via82xx_t *chip, viadev_t *viadev)
;{
;	outb(VIA_REG_CTRL_PAUSE | VIA_REG_CTRL_TERMINATE | VIA_REG_CTRL_RESET,
;	     VIADEV_REG(viadev, OFFSET_CONTROL));
;	inb(VIADEV_REG(viadev, OFFSET_CONTROL));
;	udelay(50);
;	/* disable interrupts */
;	outb(0x00, VIADEV_REG(viadev, OFFSET_CONTROL));
;	/* clear interrupts */
;	outb(0x03, VIADEV_REG(viadev, OFFSET_STATUS));
;	outb(0x00, VIADEV_REG(viadev, OFFSET_TYPE)); /* for via686 */
;	// outl(0, VIADEV_REG(viadev, OFFSET_CURR_PTR));
;	viadev->lastpos = 0;
;}


via_channel_reset:

   mov al,VIA_REG_CTRL_PAUSE
   or  al,VIA_REG_CTRL_TERMINATE
   or  al,VIA_REG_CTRL_RESET
   
   mov dx,[pci_config+16]
   and dx,0xFFFC
   add dx,VIA_REG_OFFSET_CONTROL
   out dx,al
   
   in al,dx
  
   xor al,al
   out dx,al
   
   mov al,3
   mov dx,[pci_config+16]
   and dx,0xFFFC
   add dx,VIA_REG_OFFSET_STATUS
   out dx,al
ret


via_channel_go:

   mov al,VIA_REG_CTRL_START
   
   mov dx,[pci_config+16]
   and dx,0xFFFC
   add dx,VIA_REG_OFFSET_CONTROL
   out dx,al
   
   in al,dx
  
   xor al,al
   out dx,al
   
   mov al,3
   mov dx,[pci_config+16]
   and dx,0xFFFC
   add dx,VIA_REG_OFFSET_STATUS
   out dx,al
ret


%if 0
static int ac97_reset_wait(ac97_t *ac97, int timeout, int with_modem)
{
	unsigned long end_time;
	unsigned short val;

	end_time = jiffies + timeout;
	do {
		
		/* use preliminary reads to settle the communication */
		snd_ac97_read(ac97, AC97_RESET);
		snd_ac97_read(ac97, AC97_VENDOR_ID1);
		snd_ac97_read(ac97, AC97_VENDOR_ID2);
		/* modem? */
		if (with_modem) {
			val = snd_ac97_read(ac97, AC97_EXTENDED_MID);
			if (val != 0xffff && (val & 1) != 0)
				return 0;
		}
		if (ac97->scaps & AC97_SCAP_DETECT_BY_VENDOR) {
			/* probably only Xbox issue - all registers are read as zero */
			val = snd_ac97_read(ac97, AC97_VENDOR_ID1);
			if (val != 0 && val != 0xffff)
				return 0;
		} else {
			/* because the PCM or MASTER volume registers can be modified,
			 * the REC_GAIN register is used for tests
			 */
			/* test if we can write to the record gain volume register */
			snd_ac97_write_cache(ac97, AC97_REC_GAIN, 0x8a05);
			if ((snd_ac97_read(ac97, AC97_REC_GAIN) & 0x7fff) == 0x0a05)
				return 0;
		}
		set_current_state(TASK_UNINTERRUPTIBLE);
		schedule_timeout(1);
	} while (time_after_eq(end_time, jiffies));
	return -ENODEV;
}
%endif

via_ac97_reset_wait:

    mov dword [counter3],0

    via_act97_loop_reset_wait:
    
      mov  ecx,AC97_RESET
      call via_codec_read

      mov  ecx,AC97_VENDOR_ID1
      call via_codec_read

      mov  ecx,AC97_VENDOR_ID2
      call via_codec_read

      call _nop
     
      mov  dword [value],0x8a05
      mov  ecx,AC97_REC_GAIN
      call via_codec_write

      call _nop

      xor eax,eax

      mov  ecx,AC97_REC_GAIN
      call via_codec_read
      and  eax,0x7fff
      cmp  eax,0x0a05
      je   via_codec_ok

     inc dword [counter3]
     cmp dword [counter3],10
   jle via_act97_loop_reset_wait

     mov esi,text_cdc_rst_fail
     call _draw_cars
   ret

via_codec_ok:

     mov esi,text_cdc_rst_ok
     call _draw_cars

ret

rate: dd 00

;in [rate] rate to test

via_ac97_test_rate:

   mov ecx     ,AC97_PCM_FRONT_DAC_RATE
   mov edx     ,[rate]
   and edx     ,0xFFFF          
   mov [value] ,edx  
   
   call   via_codec_write

   mov ecx     ,AC97_PCM_FRONT_DAC_RATE
   call via_codec_read

   and eax   ,0xFFFF

   cmp eax,[rate]
   je rate_ok
      mov esi,text_cdc_rate_fail
     call _draw_cars
   ret
   
   rate_ok:
   
     mov esi,text_cdc_rate_ok
     call _draw_cars


     mov ecx     ,AC97_PCM_LFE_DAC_RATE
     mov edx     ,[rate]
     and edx     ,0xFFFF          
     mov [value] ,edx  
     call   via_codec_write
   
ret

;static int snd_ac97_test_rate(ac97_t *ac97, int reg, int shadow_reg, int rate)
;{
;	unsigned short val;
;	unsigned int tmp;

;	tmp = ((unsigned int)rate * ac97->bus->clock) / 48000;
;	snd_ac97_write_cache(ac97, reg, tmp & 0xffff);
;	if (shadow_reg)
;		snd_ac97_write_cache(ac97, shadow_reg, tmp & 0xffff);
;   val = snd_ac97_read(ac97, reg);
;	return val == (tmp & 0xffff);
;}




%if 0
static int snd_ac97_try_volume_mix(ac97_t * ac97, int reg)
{
	unsigned short val, mask = 0x8000;

	if (! snd_ac97_valid_reg(ac97, reg))
		return 0;

	switch (reg) {
	case AC97_MASTER_TONE:
		return ac97->caps & 0x04 ? 1 : 0;

	case AC97_CENTER_LFE_MASTER:	/* center */
		if ((ac97->ext_id & AC97_EI_CDAC) == 0)
			return 0;
		break;
	case AC97_CENTER_LFE_MASTER+1:	/* lfe */
		if ((ac97->ext_id & AC97_EI_LDAC) == 0)
			return 0;
		reg = AC97_CENTER_LFE_MASTER;
		mask = 0x0080;
		break;
	case AC97_SURROUND_MASTER:
		if ((ac97->ext_id & AC97_EI_SDAC) == 0)
			return 0;
		break;
	}

	val = snd_ac97_read(ac97, reg);

	if (!(val & mask)) 
    {
		/* nothing seems to be here - mute flag is not set */
		/* try another test */
		snd_ac97_write_cache(ac97, reg, val | mask);
		val = snd_ac97_read(ac97, reg);
	
        if (!(val & mask))
			return 0;	/* nothing here */
	}
	return 1;		/* success, useable */
}
%endif




ac97id: dd 0
via_mixer_init:

    mov  ecx,AC97_VENDOR_ID1
    call via_codec_read

    mov dword [ac97id],eax
    shl dword [ac97id],16

    mov  ecx,AC97_VENDOR_ID2
    call via_codec_read
    or dword [ac97id],eax

	mov  dword [value],0
    mov  ecx          ,AC97_RESET
    call via_codec_write
    
    call via_ac97_reset_wait
	
    mov  ecx,AC97_VENDOR_ID1
    call via_codec_read

    mov  ecx,AC97_VENDOR_ID2
    call via_codec_read

    mov  ecx, AC97_EXTENDED_ID
    call via_codec_read
    
   
;   snd_ac97_update(ac97, reg, tmp & 0xffff);
;	snd_ac97_read(ac97, reg);

    


ret

;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
; in [pci_bus]
; in [pci_dev]
; in [pci_fnc]
;
;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

_init_via_sound:     ;(int bus, int device, int func)

;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*


;mov edi,dma_sound_buffer
;mov esi,sound_buffer_aera
;mov ecx,48000
;rep movsb


mov  dword [regs_cnt],VIA_FUNC_ENABLE
call _read_byte_pci
mov  [old_legacy],al

mov dword [regs_cnt],VIA_PNP_CONTROL
call _read_byte_pci
mov [old_legacy_cfg],al

mov al, [old_legacy]
and al, ~VIA_FUNC_INIT 

mov dword [regs_cnt],VIA_FUNC_ENABLE
mov [value]         ,al
call _write_byte_pci
call _nops


;get status bit from pci config

   mov dword [regs_cnt],VIA_ACLINK_STAT
   call _read_byte_pci

;test if codec is ready

   test al,VIA_ACLINK_C00_READY
   jnz codec_ready

    ;codec is not ready

		;mov esi,text_cdc_init
		;call _draw_cars

		;send enable/init command

         mov dword [regs_cnt],VIA_ACLINK_CTRL
         mov byte [value]    ,VIA_ACLINK_CTRL_ENABLE
         or  byte [value]    ,VIA_ACLINK_CTRL_RESET
         or  byte [value]    ,VIA_ACLINK_CTRL_SYNC

         call _write_byte_pci
         call _nops

         mov esi,text_cdc_init
         call _draw_cars

	     ;reset the ctrl register
         mov dword [regs_cnt],VIA_ACLINK_CTRL
         mov byte [value],0

         call _write_byte_pci
         call _nops

         mov esi,text_cdc_init
         call _draw_cars

	     ;send enable/init command

         mov dword [regs_cnt],VIA_ACLINK_CTRL
         mov byte [value],VIA_ACLINK_CTRL_INIT

         call _write_byte_pci
         call _nops

         mov esi,text_cdc_init
         call _draw_cars

   codec_ready:

      mov dword [regs_cnt],VIA_ACLINK_CTRL
      call _read_byte_pci

      mov cl,al
      call _draw_byte_hex

      and al,VIA_ACLINK_CTRL_INIT
      cmp al,VIA_ACLINK_CTRL_INIT
      je no_init

           mov esi,text_cdc_warn
           call _draw_cars

	       mov dword [regs_cnt],VIA_ACLINK_CTRL	  
       	   mov byte [value],VIA_ACLINK_CTRL_INIT

           call _write_byte_pci
           call _nops
     
     no_init:

        mov dword [counter],0

        loop_wait_via_ready:

	         mov dword [regs_cnt],VIA_ACLINK_STAT
             call _read_byte_pci
   
             test al,VIA_ACLINK_C00_READY
             jnz ok

             inc dword[counter]
  	         cmp dword[counter],100
             jle loop_wait_via_ready
            ret 

            mov esi,text_cdc_err
            call _draw_cars
         ret
         ok:
         
         mov dx,[pci_config+16]  
         and dx,0xFFFC		     ; grab address start from pci config
         add dx,VIA_REG_AC97	 ; add via ac97 reg offset 
         in eax ,dx              ; get value from the register
         
         mov [value],eax
         
         test eax,VIA_REG_AC97_BUSY
         jz no_error
           mov esi,text_cdc_busy
           call _draw_cars
         no_error:
        

         mov cx,[value+2]
         call _draw_word_hex

         mov cx,[value]
         call _draw_word_hex
         
         mov esi,text_cdc_ok
         call _draw_cars

	 ;====================================================
 	 ;end of chipset warming up AC97 should be there
	 ;====================================================

          mov  dword [value],0
          mov  ecx, AC97_RESET
          call via_codec_write

          mov  ecx, AC97_RESET
          call via_codec_read


          mov  dword [value],0
          mov  ecx, AC97_RESET
          call via_codec_write

          mov  ecx, AC97_VENDOR_ID1
          call via_codec_read

          mov  ecx, AC97_VENDOR_ID2
          call via_codec_read

       	  mov  dword [value]  , 0x8a06
          mov  ecx            , AC97_REC_GAIN
          call via_codec_write

          mov  ecx            , AC97_REC_GAIN
          call via_codec_read

          mov  ecx, AC97_RESET
          call via_codec_read


          mov  ecx, AC97_EXTENDED_ID
          call via_codec_read

          mov  dword [value],0
          mov  ecx    , AC97_POWERDOWN
          call via_codec_write

          mov  dword [value],0
          mov  ecx, AC97_RESET
          call via_codec_write

          mov  dword [value],0
          mov  ecx    , AC97_POWERDOWN
          call via_codec_write

          mov  dword [value],0
          mov  ecx    , AC97_GENERAL_PURPOSE
          call via_codec_write

          mov  ecx    , AC97_POWERDOWN
          call via_codec_read

          mov  dword [value],0x181
          mov  ecx  , AC97_EXTENDED_STATUS
          call via_codec_write


	;rate testing/setting

          mov dword [rate],22050
          call via_ac97_test_rate

          mov dword [rate],44100
          call via_ac97_test_rate

          mov dword [rate],48000
          call via_ac97_test_rate
   
	;volume testing/setting

          mov  dword [value],0x1F1F
          mov  ecx, AC97_MASTER
          call via_codec_write

          mov  ecx, AC97_MASTER
          call via_codec_read
         
          mov cx,ax
          call _draw_word_hex


          mov  dword [value],0x1F1F
          mov  ecx, AC97_HEADPHONE
          call via_codec_write

          mov  ecx, AC97_HEADPHONE
          call via_codec_read

          mov cx,ax
          call _draw_word_hex


          mov  dword [value],0x1F1F
          mov  ecx, AC97_PCM
          call via_codec_write

          mov  ecx, AC97_PCM	
          call via_codec_read

          mov cx,ax
          call _draw_word_hex


        ;
          mov  ecx    , AC97_POWERDOWN
          call via_codec_read

          mov  dword [value],0x800f
          mov  ecx    , AC97_POWERDOWN
          call via_codec_write

          mov  ecx    , AC97_POWERDOWN
          call via_codec_read

          mov  dword [value],0xf
          mov  ecx    , AC97_POWERDOWN
          call via_codec_write

	;

          mov  ecx    , AC97_EXTENDED_STATUS
          call via_codec_read

          mov  dword [value],0x5c1
          mov  ecx    , AC97_EXTENDED_STATUS
          call via_codec_write


          mov  dword [value],0x5f1
          mov  ecx    , AC97_EXTENDED_STATUS
          call via_codec_write


          mov  dword [value],0x5f5
          mov  ecx    , AC97_EXTENDED_STATUS
          call via_codec_write



         mov al,0x1f
         mov dx,[pci_config+16]  
         and dx,0xFFFC		                 ; grab address start from pci config
         add dx,VIA_REG_OFS_PLAYBACK_VOLUME_L    ; add via ac97 reg offset 
         out dx,al
         
         mov al,0x1f
         mov dx,[pci_config+16]  
         and dx,0xFFFC		                 ; grab address start from pci config
         add dx,VIA_REG_OFS_PLAYBACK_VOLUME_R    ; add via ac97 reg offset 
         out dx,al

         mov eax,VIA8233_REG_TYPE_16BIT
 ;       or  eax,VIA8233_REG_TYPE_STEREO
         or  eax,0xfffff
         or  eax,0xff000000
         
         mov dx,[pci_config+16]
         and dx,0xFFFC
         add dx,VIA_REG_OFFSET_STOP_IDX
         out dx,eax

         call via_channel_reset
         call via_codec_ready
         
         mov dx,[pci_config+16]  
         and dx,0xFFFC		                 ; grab address start from pci config
         add dx,VIA_REG_OFFSET_TABLE_PTR     ; add via ac97 reg offset 
         mov eax,dma_sound_buffer
         out dx ,eax                         ; get value from the register

%if 0         
         call via_codec_ready                  


         mov dx,[pci_config+16]  
         and dx,0xFFFC		             ; grab address start from pci config
         add dx,VIA_REG_OFFSET_TABLE_PTR     ; add via ac97 reg offset 
         in eax,dx
         mov [value],eax

         mov cx,[value+2]
         call _draw_word_hex

         mov cx,[value]
         call _draw_word_hex

%if 0
         outl((runtime->format == SNDRV_PCM_FORMAT_S16_LE ? VIA8233_REG_TYPE_16BIT : 0) | /* format */
	     (runtime->channels > 1 ? VIA8233_REG_TYPE_STEREO : 0) | /* stereo */
	     rbits | /* rate */
	     0xff000000,    /* STOP index is never reached */
	     VIADEV_REG(viadev, OFFSET_STOP_IDX));
%endif
%endif         
         mov al,VIA_REG_CTRL_START
         mov dx,[pci_config+16]
         and dx,0xFFFC
         add dx,VIA_REG_OFFSET_CONTROL
         out dx,al

;         mov edi,dma_sound_buffer
;         mov esi, sound_buffer_aera
;         mov ecx,48000
; 	  rep movsb


ret
         

         mov al,2
         shl al,4
         or  al,VIA_REG_MULTPLAY_FMT_16BIT
         
         mov dx,[pci_config+16]
         and dx,0xFFFC
         add dx,VIA_REG_OFS_MULTPLAY_FORMAT
         out dx,al
         
         call via_codec_ready                  


         call via_channel_go

          mov  dword [value],0
          mov  ecx, AC97_POWERDOWN
          call via_codec_write

         call via_mixer_init

         call via_channel_reset
         
         call via_codec_ready
         
         mov dx,[pci_config+16]  
         and dx,0xFFFC		             ; grab address start from pci config
         add dx,VIA_REG_OFFSET_TABLE_PTR ; add via ac97 reg offset 
         mov eax,dma_sound_buffer
         out dx ,eax                     ; get value from the register
         call _nops
         
         call via_codec_ready                  
       
         mov al,2
         shl al,4
         or  al,VIA_REG_MULTPLAY_FMT_16BIT
         
         mov dx,[pci_config+16]
         and dx,0xFFFC
         add dx,VIA_REG_OFS_MULTPLAY_FORMAT
         out dx,al
         
         call via_codec_ready                  


                                                                    
;	fmt = (runtime->format == SNDRV_PCM_FORMAT_S16_LE) ? VIA_REG_MULTPLAY_FMT_16BIT : VIA_REG_MULTPLAY_FMT_8BIT;
;	fmt |= runtime->channels << 4;
;	outb(fmt, VIADEV_REG(viadev, OFS_MULTPLAY_FORMAT));
; 	outl((u32)viadev->table.addr, VIADEV_REG(viadev, OFFSET_TABLE_PTR));
;      outb(chip->playback_volume[i], port + VIA_REG_OFS_PLAYBACK_VOLUME_L + i);
        
             
        
       mov  dword [value],0x7F7F
       mov  ecx, AC97_MASTER
       call via_codec_write

       mov  ecx, AC97_MASTER
       call via_codec_read
         
       mov cx,ax
       call _draw_word_hex


       mov  dword [value],0x1F1F
       mov  ecx, AC97_CENTER_LFE_MASTER
       add  ecx,1
       call via_codec_write

       mov  dword [value],0x1F1F
       mov  ecx, AC97_CENTER_LFE_MASTER
       call via_codec_write

       mov  ecx, AC97_CENTER_LFE_MASTER
       call via_codec_read
         
       mov cx,ax
       call _draw_word_hex




       mov  dword [value],0x1F1F
       mov  ecx, AC97_HEADPHONE
       call via_codec_write

       mov  ecx, AC97_HEADPHONE
       call via_codec_read

       mov cx,ax
       call _draw_word_hex



       mov  dword [value],0x1F1F
       mov  ecx, AC97_MASTER_MONO
       call via_codec_write

       mov  ecx, AC97_MASTER_MONO
       call via_codec_read

       mov cx,ax
       call _draw_word_hex
         

       mov  dword [value],0x1F1F
       mov  ecx, AC97_PCM
       call via_codec_write


       mov  ecx, AC97_PCM
       call via_codec_read

       mov cx,ax
       call _draw_word_hex
   
         mov eax,0x1f1f
         mov dx,[pci_config+16]  
         and dx,0xFFFC		                 ; grab address start from pci config
         add dx,VIA_REG_OFS_PLAYBACK_VOLUME_L    ; add via ac97 reg offset 
         out dx,eax
         

         mov eax,0x1f1f
         mov dx,[pci_config+16]  
         and dx,0xFFFC		                 ; grab address start from pci config
         add dx,VIA_REG_OFS_PLAYBACK_VOLUME_R    ; add via ac97 reg offset 
         out dx,eax

;		reg = AC97_CENTER_LFE_MASTER;
;		mask = 0x0080;
;		break;
;	    case AC97_SURROUND_MASTER:
;		if ((ac97->ext_id & AC97_EI_SDAC) == 0)
;			return 0;
;		break;
;	}
;	val = snd_ac97_read(ac97, reg);

 


      call via_channel_go


;     mov al,VIA_REG_CTRL_INT
;     or al,VIA_REG_CTRL_START

;     mov dx,[pci_config+16]  
;     and dx,0xFFFC		              ; grab address start from pci config
;     add dx,VIA_REG_OFFSET_CONTROL    ; add via ac97 reg offset 
;     out dx ,al                        ; get value from the register
        

;       VIA_REG_CTRL_START
;	outb(val, VIADEV_REG(viadev, OFFSET_CONTROL));

ret

%if 0
	pci_read_config_byte(chip->pci, VIA_ACLINK_STAT, &pval);
	if (! (pval & VIA_ACLINK_C00_READY)) { /* codec not ready? */
		/* deassert ACLink reset, force SYNC */
		pci_write_config_byte(chip->pci, VIA_ACLINK_CTRL,
				      VIA_ACLINK_CTRL_ENABLE |
				      VIA_ACLINK_CTRL_RESET |
				      VIA_ACLINK_CTRL_SYNC);
		udelay(100);
		pci_write_config_byte(chip->pci, VIA_ACLINK_CTRL, 0x00);
		udelay(100);

		/* ACLink on, deassert ACLink reset, VSR, SGD data out */
		/* note - FM data out has trouble with non VRA codecs !! */
		pci_write_config_byte(chip->pci, VIA_ACLINK_CTRL, VIA_ACLINK_CTRL_INIT);
		udelay(100);
	}
	
	/* Make sure VRA is enabled, in case we didn't do a
	 * complete codec reset, above */
	pci_read_config_byte(chip->pci, VIA_ACLINK_CTRL, &pval);
	if ((pval & VIA_ACLINK_CTRL_INIT) != VIA_ACLINK_CTRL_INIT) {
		/* ACLink on, deassert ACLink reset, VSR, SGD data out */
		/* note - FM data out has trouble with non VRA codecs !! */
		pci_write_config_byte(chip->pci, VIA_ACLINK_CTRL, VIA_ACLINK_CTRL_INIT);
		udelay(100);
	}

	/* wait until codec ready */
	max_count = ((3 * HZ) / 4) + 1;
	do {
		pci_read_config_byte(chip->pci, VIA_ACLINK_STAT, &pval);
		if (pval & VIA_ACLINK_C00_READY) /* primary codec ready */
			break;
		set_current_state(TASK_UNINTERRUPTIBLE);
		schedule_timeout(1);
	} while (--max_count > 0);

%endif

