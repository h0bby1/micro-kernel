
%define BIT0  1
%define BIT1  2

ac97_mixer_addr   :dd 0
ac97_bus_addr     :dd 0

ac97_buffer_list  :dd 0,0, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0, 0,0

%define ac97_BUP  (1<<14) 
%define ac97_IOC  (1<<15)

%define ac97_IO_ADDR_MASK			0FFFEh   ; mask off bit 0 for reading BARs
%define ac97_reg_command			0x04
%define ac97_reg_mixer				0x10
%define ac97_reg_bus				0x14

%define ac97_reg_pcm_front_dacrate  0x2c     ; PCM out sample rate
%define ac97_reg_pcm_out_CR         0x1b	 ; PCM out Status register
%define ac97_reg_pcm_out_LVI		0x15	 ; PCM out Last Valid Index
%define ac97_reg_pcm_out_BDBAR      0x10     ; PCM out buffer descriptor BAR


%define ac97_bus_master  4
%define ac97_bus_io_en   1
%define ac97_reset		 BIT1    ; reset registers.  Nukes all regs
                                 ; except bits 4:2 of this register.
                                 ; Only set this bit if BIT 0 is 0

%define ac97_run_pause	 BIT0    ; Run/Pause
                                 ; set this bit to start the codec!
;------------------------------------------------------------------
;in : edx  samples buffer address
;in : eax  buffer length
;in : ecx  buffer index
;------------------------------------------------------------------
init_buffer_list:
   
   mov        edi	, ac97_buffer_list
   mov		  ebx	, ecx
   shl        ebx	, 3
   add		  edi	, ebx

   mov dword [edi]  , edx
   mov word  [edi+4], ax
   mov word  [edi+6], (ac97_BUP)|(ac97_IOC)

ret

orig :dd 0
new  :dd 0


init_ac97:

	mov dword [pci_bus]    ,0
    mov dword [pci_dev]    ,0x11
    mov dword [pci_fnc]    ,5
	mov dword [regs_cnt]   ,ac97_reg_command
	mov dword [multi_card] ,0
	
	call _calc_pci_config_addr

	;--- first read ---	
	mov eax , ecx
	and eax , ~(PCI16+PCI32)
	or	eax , PCI16
	mov dx  , 0x0CF8			    ;put ecx  (the address) in addr register
	out dx  , ax

	mov dx  , 0x0CFC				;put the value in cmd register
	in  ax  , dx

	;--- put value ---	

	mov eax , ecx
	and eax , ~(PCI16+PCI32)
	or	eax , PCI16
	mov dx  , 0x0CF8			    ; put ecx  (the address) in addr register
	out dx  , ax
	or	al  , ac97_bus_master
	or	al  , ac97_bus_io_en
	out dx  , ax
	mov [orig]	,ax

	;--- second read ---	
	mov eax , ecx
	and eax , ~(PCI16+PCI32)
	or	eax , PCI16
	mov dx  , 0x0CF8			    ; put ecx  (the address) in addr register
	out dx  , ax

	in  eax		,dx
	mov [new]	,ax

	mov cx		,[orig]
	call _draw_word_hex
	
	mov dl		,32
	call _draw_car
		
	mov cx		,[new]
	call _draw_word_hex
	
	;---------------------------------
	;read config
    
    call _read_pci_config

	;---------------------------------
	;dump 
	

	;---------------------------------
	;draw ac97 command 	
	 	
 		mov esi				  ,text_apic_command
		call _draw_cars

		mov cx				  ,[pci_config+ac97_reg_command]
		call _draw_word_hex
	 	
		mov dl				  ,10
		call _draw_car

	;---------------------------------
	;draw ac97 mixer address
	 	
 		mov esi				  ,text_apic_mixer_base
		call _draw_cars

		mov eax				  ,[pci_config+ac97_reg_mixer]
		and eax				  ,ac97_IO_ADDR_MASK
		
		mov [ac97_mixer_addr] ,eax
		mov ecx				  ,eax
		call _draw_dword_hex
	    
		mov dl,10
		call _draw_car

	;---------------------------------
	;draw ac97 bus address

 		mov esi,text_apic_bus_base
		call _draw_cars
	    
   		mov eax				  ,[pci_config+ac97_reg_bus]
   		and eax				  ,ac97_IO_ADDR_MASK
   		mov [ac97_bus_addr]   ,eax
		mov ecx				  ,eax
		call _draw_dword_hex

		mov dl,10
		call _draw_car
		
	;---------------------------------
	;draw ac97 interupt line

 		mov esi,text_i_line
		call _draw_cars
	    
		mov cl				  ,[pci_config+0x3C]
		call _draw_byte_hex
	    
		mov dl,10
		call _draw_car

	;---------------------------------
	;enable ac97

;		lea  edx	, [pci_config+ac97_reg_command] 
;		mov  al		, [edx]
;		or	 al     , ac97_bus_master
;		or	 al     , ac97_bus_io_en
;		mov  [edx]  ,al
;    
;    call _write_pci_config
           
	
	;---------------------------------
	;reread config
	
	mov dword [regs_cnt]   ,0
    call _read_pci_config

 	call _dump_pci_config 

	;---------------------------------
	;draw ac97 command 	
 	mov esi				  ,text_apic_command
    call _draw_cars

    mov cx				  ,[pci_config+ac97_reg_command]
    call _draw_word_hex
 	
    mov dl				  ,10
    call _draw_car

ret    







ac97_kickit:

	mov     dx, [ac97_bus_addr]
    add     dx, ac97_reg_pcm_out_CR         ; DCM out control register
    mov     al, ac97_run_pause
    out     dx, al                          ; set start!
ret

ac97_setbufferslist:

    mov edx				  ,sound_buffer_aera
    and edx				  ,0xFFFFFFFC
    mov eax				  ,65536
    mov ecx				  ,0
    call				  init_buffer_list

	;
	; tell the DMA engine where to find our list of Buffer 
	; Descriptors.
	; this 32bit value is a flat mode memory offset 
	; (ie no segment:offset)
	; write NABMBAR+10h with offset of buffer descriptor list
	;
    
    mov	    eax, ac97_buffer_list
    mov     dx, [ac97_bus_addr]
    add     dx, ac97_reg_pcm_out_BDBAR      ; set pointer to BDL
    out     dx, eax                         ; write to AC97 controller

ret


ac97_setLastValidIndex:

	mov	dx, [ac97_bus_addr]
	add	dx, ac97_reg_pcm_out_LVI
    out     dx, al

ret

ac97_reset_dma:
    mov     dx, [ac97_bus_addr]
    add     dx, ac97_reg_pcm_out_CR        ; set pointer to Cntl reg
    mov     al, ac97_reset                 ; set reset
    out     dx, al                         ; self clearing bit
ret

ac97_config:
    
    mov     dx, [ac97_mixer_addr]           ; get mixer base address
    add     dx, ac97_reg_pcm_front_dacrate  ; register 2ch
    out     dx, ax                          ; out sample rate

    call    delay1_4ms
    call    delay1_4ms
    call    delay1_4ms
    call    delay1_4ms

	;
	;
	; This stuff that's commented out sets the volume to MAXIMUM.
	; be careful if you use it this way.
	;


	;      mov     dx, ds:[NAMBAR]
	;      add     dx, CODEC_MASTER_VOL_REG        ;2 
	;      xor     ax, ax
	;      out     dx, ax
	;
	;      call    delay1_4ms                      ; delays cuz codecs are slow
	;      call    delay1_4ms
	;      call    delay1_4ms
	;      call    delay1_4ms
	;
	;
	;      mov     dx, ds:[NAMBAR]
	;      add     dx, CODEC_PCM_OUT_REG           ; 18
	;      out     dx, ax
	;
	;      call    delay1_4ms
	;      call    delay1_4ms
	;      call    delay1_4ms
	;      call    delay1_4ms
ret

