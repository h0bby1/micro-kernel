
txt_prefill_begin:db 'prefilling dma buffer',10,0
txt_chan_start:db 'starting playback',10,0
txt_via_driver_init:db 'via driver init : ',0
txt_via_driver_a2:db 'via driver addr 2 : ',0

txt_via_driver_set_buffer:db 'via set buffer from ',0
txt_via_driver_set_to	 :db ' too ',0
txt_via_driver_set_size	 :db ' size ',0

txt_via_driver_set_table :db ' via set buffer table ',0
txt_via_driver_set_table2 :db ' buffers of each ',0

text_cdc_ready:db 'codec ready ',0
text_cdc_eready:db 'codec is not ready',10,0
text_cdc_enable:db 'codec enabled ',10,0
text_cdc_not_enable:db 'codec not enabled ',10,0

text_cdc_ok:db 'codec ok ',0
text_cdc_init:db 'codec init ',0
text_cdc_err:db 'codec error ',0
text_cdc_warn:db 'codec warning ',0
text_cdc_busy:db 'codec busy ',0
text_cdc_inv:db 'codec invalid ',0

text_cdc_cant_read:db 'codec cant read ',0

text_cdc_rst_fail:db 'codec reset failed',0
text_cdc_rst_ok:db 10,'codec reset ok ',10,0

text_cdc_rate_fail:db 'codec rate test failed',0
text_cdc_rate_ok:db 'codec rate test ok',0

%define VIA_DXS_MAX_VOLUME		31		;/* max. volume (attenuation) of reg 0x32/33 */


%define AUVIA_PCICONF_JUNK	0x40
%define		AUVIA_PCICONF_ENABLES	 0x00ff0000	;/* reg 42 mask */
%define		AUVIA_PCICONF_ACLINKENAB 0x00008000	;/* ac link enab */
%define		AUVIA_PCICONF_ACNOTRST	 0x00004000	;/* ~(ac reset) */
%define		AUVIA_PCICONF_ACSYNC	 0x00002000	;/* ac sync */
%define		AUVIA_PCICONF_ACVSR	 0x00000800	;/* var. samp. rate */
%define		AUVIA_PCICONF_ACSGD	 0x00000400	;/* SGD enab */
%define		AUVIA_PCICONF_ACFM	 0x00000200	;/* FM enab */
%define		AUVIA_PCICONF_ACSB	 0x00000100	;/* SB enab */


%define VIA_REG_AC97					0x80
%define VIA_CODEC_CTL					0x80

%define VIA_ACLINK_STAT					0x40
%define VIA_ACLINK_C00_READY			0x01 ; primary codec ready
%define VIA_ACLINK_CTRL					0x41

%define VIA_FUNC_ENABLE					0x42
%define VIA_FUNC_ENABLE_SB				0x01
%define VIA_FUNC_ENABLE_FM				0x04
%define VIA_PNP_CONTROL					0x43
%define VIA_FUNC_INIT					(VIA_FUNC_ENABLE_SB|VIA_FUNC_ENABLE_FM)
%define  VIA_ACLINK_CTRL_PCM			0x04 ; 0: disable PCM, 1: enable PCM 
%define  VIA_ACLINK_CTRL_VRA			0x08 ; 0: disable VRA, 1: enable VRA
%define  VIA_ACLINK_CTRL_SYNC			0x20 ; 0: release SYNC, 1: force SYNC hi
%define  VIA_ACLINK_CTRL_RESET			0x40 ; 0: assert, 1: de-assert 
%define  VIA_ACLINK_CTRL_ENABLE			0x80 ; 0: disable, 1: enable 
%define   VIA_REG_OFFSET_TABLE_PTR		0x04 ; dword - channel table pointer */


%define VIA_REG_OFFSET_STATUS			0x00	; byte - channel status */
%define   VIA_REG_STAT_ACTIVE			0x80	; RO */
%define   VIA_REG_STAT_PAUSED			0x40	; RO */
%define   VIA_REG_STAT_TRIGGER_QUEUED	0x08	; RO */
%define   VIA_REG_STAT_STOPPED			0x04	; RWC */
%define   VIA_REG_STAT_EOL				0x02	; RWC */
%define   VIA_REG_STAT_FLAG				0x01	; RWC */
%define   VIA_8233_MP_BASE				0x40

%define   VIA_REG_OFFSET_CONTROL		0x01	; byte - channel control */
%define   VIA_REG_CTRL_START			0x80	; WO */
%define   VIA_REG_CTRL_TERMINATE		0x40	; WO */
%define   VIA_REG_CTRL_AUTOSTART		0x20
%define   VIA_REG_CTRL_PAUSE			0x08	; RW */
%define   VIA_REG_CTRL_INT_STOP			0x04		
%define   VIA_REG_CTRL_INT_EOL			0x02
%define   VIA_REG_CTRL_INT_FLAG			0x01


%define	  AUVIA_RP_CONTROL				0x01
%define	  AUVIA_RPCTRL_START			0x80	
%define   AUVIA_RPCTRL_AUTOSTART		0x20
%define	  AUVIA_RPCTRL_STOP				0x04
%define	  AUVIA_RPCTRL_EOL				0x02
%define	  AUVIA_RPCTRL_FLAG				0x01






%define   VIA_REG_CTRL_RESET			0x01	; RW - probably reset? undocumented */

%define   VIA_REG_CTRL_INT		(VIA_REG_CTRL_INT_FLAG | VIA_REG_CTRL_INT_EOL | VIA_REG_CTRL_AUTOSTART)
%define   VIA_ACLINK_CTRL_INIT	(VIA_ACLINK_CTRL_ENABLE | VIA_ACLINK_CTRL_RESET | VIA_ACLINK_CTRL_PCM | VIA_ACLINK_CTRL_VRA)

%define VIA_REG_OFS_PLAYBACK_VOLUME_L	0x02	; byte */
%define VIA_REG_OFS_PLAYBACK_VOLUME_R	0x03	; byte */
%define VIA_REG_OFS_MULTPLAY_FORMAT		0x02	; byte - format and channels */
%define VIA_REG_MULTPLAY_FMT_8BIT		0x00      
%define VIA_REG_MULTPLAY_FMT_16BIT		0x80
%define VIA_REG_MULTPLAY_FMT_CH_MASK	0x70	; # channels << 4 (valid = 1,2,4,6) */
%define VIA_REG_OFS_CAPTURE_FIFO		0x02	; byte - bit 6 = fifo  enable */
%define VIA_REG_CAPTURE_FIFO_ENABLE		0x40

%define AC97_PCM_LFE_DAC_RATE		0x30	; PCM LFE DAC Rate */
%define AC97_PCM_FRONT_DAC_RATE		0x2c	; PCM Front DAC Rate */
%define AC97_REC_GAIN				0x1c	; Record Gain */
%define AC97_VENDOR_ID1				0x7c	; Vendor ID1 
%define AC97_VENDOR_ID2				0x7e	; Vendor ID2 / revision 
%define AC97_EXTENDED_ID			0x28	; Extended Audio ID */

%define AC97_RESET	 				0x00	; Reset */
%define AC97_MASTER					0x02	; Master Volume */
%define AC97_HEADPHONE				0x04	; Headphone Volume (optional) */
%define AC97_MASTER_MONO			0x06	; Master Volume Mono (optional) */
%define AC97_POWERDOWN				0x26	; Powerdown control / status */
%define AC97_PCM					0x18	    ; PCM Volume 
%define AC97_CENTER_LFE_MASTER		0x36	; Center + LFE Master Volume 
%define AC97_GENERAL_PURPOSE		0x20	; General Purpose (optional) */
%define AC97_EXTENDED_STATUS		0x2a	; Extended Audio Status and Control */
        

%define   VIA_REG_AC97						0x80	; dword */
%define   VIA_REG_AC97_CODEC_ID_MASK		(3<<30)
%define   VIA_REG_AC97_CODEC_ID_SHIFT		30
%define   VIA_REG_AC97_CODEC_ID_PRIMARY		0x00
%define   VIA_REG_AC97_CODEC_ID_SECONDARY	0x01
%define   VIA_REG_AC97_SECONDARY_VALID		(1<<27)

%define   VIA_CODEC_READ					0x00800000
%define   VIA_REG_AC97_BUSY				    0x01000000
%define   VIA_REG_AC97_PRIMARY_VALID		0x02000000

%define   VIA_REG_AC97_CMD_SHIFT			16
%define   VIA_REG_AC97_CMD_MASK				0x7e
%define   VIA_REG_AC97_DATA_SHIFT			0
%define   VIA_REG_AC97_DATA_MASK			0xffff

%define   VIA_REG_OFFSET_STOP_IDX			0x08	    ; dword - stop index, channel type, sample rate */
%define   VIA8233_REG_TYPE_16BIT			0x00200000	; RW */
%define   VIA8233_REG_TYPE_STEREO			0x00100000	; RW */

%define   AUVIA_DMAOP_EOL		0x80000000
%define   AUVIA_DMAOP_FLAG		0x40000000
%define   AUVIA_DMAOP_STOP		0x20000000

%define		AUVIA_RP_DMAOPS_BASE		0x04
%define		VIA_RP_DMAOPS_COUNT			0x0c

%define AUVIA_8233_MP_BASE				0x40	;/* STAT, CONTROL, DMAOPS_BASE, DMAOPS_COUNT are valid */
%define AUVIA_8233_OFF_MP_FORMAT		0x02
%define	AUVIA_8233_MP_FORMAT_8BIT		0x00
%define	AUVIA_8233_MP_FORMAT_16BIT		0x80
%define	AUVIA_8233_MP_FORMAT_CHANNEL_MASK	0x70 ;/* 1, 2, 4, 6 */
%define AUVIA_8233_OFF_MP_SCRATCH		0x03

%define AUVIA_8233_OFF_MP_STOP		0x08
%define AUVIA_8233_CUR_COUNT		0x0C


%define AC97_MASTER_VOLUME		0x02
%define AC97_AUX_OUT_VOLUME		0x04
%define AC97_PCM_OUT_VOLUME		0x18
%define AC97_RECORD_SELECT		0x1A


via_slottab:dd 0x000000, 0x000011, 0x000021, 0x000521,0x004321, 0x054321, 0x654321


%macro GET_VIA_REG_L 2 
mov dx,%1
add dx,VIA_REG_%2
in eax,dx
%endmacro

via_port			  :dd 0
via_nabmbar			  :dd 0
via_nabbbar			  :dd 0
via_regno			  :dd 0
via_old_legacy		  :db 0
via_old_legacy_cfg	  :db 0
via_currrent_buffer   :dd 0
via_ac97_current_addr :dd 0
via_ac97_buffer_end	  :dd 0
via_temp			  :dd 0
via_temp2			  :dd 0
via_ac97_buffers	  :times 256 dd 0,0
via_ac97_buffers_a	  :dd 0
chan_select			  :dd 0

%define blk_size			0x0400
%define nblks 				0x04
%define buff_size			blk_size*nblks
%define via_dma_buffer_zone	0x01000000


auvia_codec_waitready:

	mov ecx,200
	loop_wait_via_ready:
	  
	  mov  dx ,[via_nabmbar]
	  add  dx ,VIA_CODEC_CTL
	  in   eax,dx
	  
	  test eax,VIA_REG_AC97_BUSY
	  jnz via_codec_not_ready
		mov eax,1
		ret
	  via_codec_not_ready:
	  
	  call delay1_4ms
	  
	  dec ecx
	  cmp ecx,0
	jg loop_wait_via_ready

	xor eax,eax 

ret


auvia_codec_waitvalid:

	mov ecx,2000
	loop_wait_via_valid:
	
	  mov  dx ,[via_nabmbar]
	  add  dx ,VIA_CODEC_CTL
	  in   eax,dx
	  
	  test eax,VIA_REG_AC97_PRIMARY_VALID
	  jz via_codec_not_valid
		  mov eax,1
		  ret
	  via_codec_not_valid:

	  call delay1_4ms
	  dec ecx
	  cmp ecx,0
	jg loop_wait_via_valid

	xor eax,eax 
ret




via_reg_write_8:
	
	mov [via_regno] ,edx
	mov al			,[value]
	
	mov dx			,[via_nabmbar]
	add dx			,[via_regno]
	out dx			,al
ret	

via_reg_write_16:
	
	mov [via_regno] ,edx
	mov ax			,[value]
	
	mov dx			,[via_nabmbar]
	add dx			,[via_regno]
	out dx			,ax
ret	

via_reg_write_32:
	
	mov [via_regno] ,edx
	mov eax			,[value]
	
	mov dx			,[via_nabmbar]
	add dx			,[via_regno]
	out dx			,eax
ret	

via_reg_read_8:
	
	mov [via_regno] ,edx
	mov dx			,[via_nabmbar]
	add dx			,[via_regno]
	in  al			,dx
ret

via_reg_read_16:
	
	mov [via_regno] ,edx
	mov dx			,[via_nabmbar]
	add dx			,[via_regno]
	in  ax			,dx
ret

via_reg_read_32:
	
	mov [via_regno] ,edx
	mov dx			,[via_nabmbar]
	add dx			,[via_regno]
	in  eax			,dx
ret

via_codec_read:

	mov [via_regno],edx

	call auvia_codec_waitready
	test eax,1
	jnz read_cdc_rdy1
	    mov esi,text_cdc_eready
	    call _draw_cars
	    xor eax,eax
		ret
	read_cdc_rdy1:
	
	mov eax	,[via_regno]
	and eax , 0x0000007F
	shl eax	,16
	or  eax	,VIA_CODEC_READ

	mov dx,[via_nabmbar]
	add dx,VIA_CODEC_CTL
	out dx,eax
	
	call auvia_codec_waitready
	test eax,1
	jnz read_cdc_rdy2
	    mov esi,text_cdc_eready
	    call _draw_cars
	    xor eax,eax
		ret
	read_cdc_rdy2:

	call auvia_codec_waitvalid
	test eax,1
	jnz read_cdc_vld2
	    mov esi,text_cdc_inv
	    call _draw_cars
	    xor eax,eax
		ret
	read_cdc_vld2:
	
	mov dx,[via_nabmbar]
	add dx,VIA_CODEC_CTL
	in  ax,dx
ret

via_codec_write:

	mov [via_regno]  ,edx
	and dword [value],0x0000FFFF

	call auvia_codec_waitready
	test eax,1
	jnz write_cdc_rdy1
	    mov esi,text_cdc_eready
	    call _draw_cars
	    xor eax,eax
		ret
	write_cdc_rdy1:

	mov eax	,[via_regno]
	and eax ,0x0000007F
	shl eax	,16
	or  eax ,[value]

	mov dx	,[via_nabmbar]
	add dx	,VIA_CODEC_CTL
	out dx	,eax
ret



_via82xx_amp_enable:

	mov edx	   ,	AC97_POWERDOWN
	call via_codec_read
	and ax	   ,	~0x8000
	
	mov [value],	ax
	mov edx	   ,	AC97_POWERDOWN
	call via_codec_write
	
	call auvia_codec_waitready
	
	; enable master output 
	mov edx				,AC97_MASTER_VOLUME
	mov dword[value]	,0x0F0F
	call via_codec_write
	
	; enable aux output 
	mov edx				,AC97_AUX_OUT_VOLUME
	mov dword[value]	,0x0F0F
	call via_codec_write
	
	; enable pcm output 
	mov edx				,AC97_PCM_OUT_VOLUME
	mov dword [value]	,0x0808
	call via_codec_write

	;set record line in 
	mov edx				,AC97_RECORD_SELECT
	mov dword [value]	,0x0404
	call via_codec_write
		
    mov  dword [value]  , 0x8a06
    mov  edx            , AC97_REC_GAIN
    call via_codec_write		
    
    mov  dword [value]	,0x0F0F
    mov  edx			,AC97_MASTER_VOLUME
    call via_codec_write
   
    mov  dword [value]	,0x0F0F
    mov  edx			,AC97_HEADPHONE
    call via_codec_write
    
    mov  dword [value]	,0x0F0F
    mov  edx			,AC97_PCM
    call via_codec_write    
    

ret



_create_buffer_table:

    mov dword [via_ac97_current_addr]		, via_dma_buffer_zone
	mov dword [via_ac97_buffer_end]			, via_dma_buffer_zone+buff_size

    mov esi							, txt_via_driver_set_buffer
    call _draw_cars
   
    mov ecx							, [via_ac97_current_addr]
    call _draw_dword_hex

    mov esi							, txt_via_driver_set_to
    call _draw_cars

    mov ecx							, [via_ac97_buffer_end]
    call _draw_dword_hex

    mov esi							,txt_via_driver_set_size
    call _draw_cars

    mov ecx							,[via_ac97_buffer_end]
    sub ecx							,[via_ac97_current_addr]
    call _draw_dword_hex
   
    mov dl							,10
    call _draw_car
    
	mov dword [via_currrent_buffer ],  0 
  
	lea eax							,  [via_ac97_buffers+15]
	and eax							,  ~0x0F
	mov [via_ac97_buffers_a]		,  eax

   via_loop_create_buffers:
	
		mov edi						,  [via_currrent_buffer]
		shl edi						,  3
		add edi						,  [via_ac97_buffers_a]
		
		
		mov  eax					,  [via_ac97_current_addr]
		mov [edi]					,  eax
		
		mov  eax					,  blk_size
		or	 eax					,  AUVIA_DMAOP_FLAG
		mov [edi+4]					,  eax
		
		inc dword [via_currrent_buffer]
		
		mov eax						,  dword [via_ac97_current_addr]
		add eax						,  blk_size
		mov	[via_ac97_current_addr]	,  eax
		
		add eax						,  blk_size
		
		cmp eax,[via_ac97_buffer_end]
	jl  via_loop_create_buffers
    
    ;--- set the last buffer ---
	
	mov edi				,  [via_currrent_buffer]
	shl edi				,  3
	add edi				,  [via_ac97_buffers_a]
		
	mov  eax			,  [via_ac97_current_addr]
	mov  [edi]			,  eax
	
	mov	eax				,  blk_size
	or  eax				,  AUVIA_DMAOP_EOL ; | AUVIA_DMAOP_FLAG
	mov dword [edi+4]	,  eax

	inc dword [via_currrent_buffer]
	
	;--- dump the result ---

	mov esi,txt_via_driver_set_table 
	call _draw_cars
			
	mov ecx,[via_ac97_buffers_a]
	call _draw_dword_hex

	mov dl,22
	call _draw_car

	mov ecx,[via_currrent_buffer]
	call _draw_byte_hex

	mov esi,txt_via_driver_set_table2
	call _draw_cars

	mov ecx,blk_size
	call _draw_dword_hex
			
	mov dl,10
	call _draw_car	
ret



_via82xx_setup_channel:
	
	mov [chan_select]		, edx

	;--- set the table ptr in via registers ---
	
	mov eax					, [via_ac97_buffers_a]
	mov dword[value]		, eax
		
	mov edx					, [chan_select]
	add edx					, AUVIA_RP_DMAOPS_BASE
	call via_reg_write_32

	;--- set the ac97 rate ---
	mov edx				   ,AC97_PCM_FRONT_DAC_RATE
	mov dword [value]	   ,22050
	call via_codec_write	
	
	cmp dword [chan_select] , 0x40
	jne not_main_chan_setup
	jmp main_chan_selected
	
	not_main_chan_setup:

		;--- set format infos for chan 0 ---
		mov eax					, [via_currrent_buffer]
		shl eax					, 24
		and eax					, 0xFF000000

	    or  eax					, VIA8233_REG_TYPE_STEREO
		or  eax					, VIA8233_REG_TYPE_16BIT
		or  eax					, 22050
				
		mov dword[value]		, eax
		mov edx					, [chan_select]
		add edx					, 0x08
		call via_reg_write_32
		
		mov dword  [value]		, 0x0F0F
		mov edx					, [chan_select]
		add edx					, VIA_REG_OFS_PLAYBACK_VOLUME_L
		call via_reg_write_16
			
		mov dword  [value]		, 0x0F0F
		mov edx					, [chan_select]
		add edx					, VIA_REG_OFS_PLAYBACK_VOLUME_R
		call via_reg_write_16

		jmp all_set
	main_chan_selected:

		;--- set SGD  the ac97 format--- 
		
		mov eax					  ,2
		shl eax					  ,4
		or  eax					  ,AUVIA_8233_MP_FORMAT_16BIT
		mov dword [value]		  ,eax
		
		mov edx					  ,(VIA_8233_MP_BASE+AUVIA_8233_OFF_MP_FORMAT) ; 0x42
		call via_reg_write_8
		
		;--- set the ac97 SGD buffer count and channel slot --- 
	
		;lea edx				   , [via_slottab+1*4]
		;mov eax				   , [edx]
		;and eax				   , 0x00FFFFFF
		;mov [value+0]		  	   , eax
	
		mov eax				       , [via_currrent_buffer]
		shl eax				       , 24
		or  eax					   , 0x21
		mov	[value]				   , eax

		mov edx				       ,(VIA_8233_MP_BASE + AUVIA_8233_OFF_MP_STOP)
		call via_reg_write_32
	
	all_set:

ret



_via82xx_start_channel:

	mov [chan_select], edx
	
	mov esi			 , txt_chan_start
	call _draw_cars
	
	;| AUVIA_RPCTRL_FLAG
	
	mov byte[value]  , (AUVIA_RPCTRL_START | AUVIA_RPCTRL_AUTOSTART | AUVIA_RPCTRL_STOP | AUVIA_RPCTRL_EOL)	
	mov edx          , [chan_select]
	add edx			 , AUVIA_RP_CONTROL
	call via_reg_write_8
		
ret


_via_prefill:

	mov esi,txt_prefill_begin
	call _draw_cars

%if 0
	mov edx,sound_buffer_aera
	mov ecx,sound_buffer_aera_end
	call stream_init
%endif	

	mov edi				, via_dma_buffer_zone
	mov dword [counter]	, 0
	loop_prefill:
	
		mov edx,blk_size
		call stream_read
	    
	    inc dword [counter]
	    cmp dword [counter],nblks
	jle loop_prefill

ret

_via_get_dma_count:

	add edx			,AUVIA_8233_CUR_COUNT
	call via_reg_read_32
	
	shr eax			,24
	and eax			,0x000000FF

ret

_via_pci_enable:
	
	; get codec base io port
	
	mov dword [regs_cnt] ,PCI_BASE_ADDRESS_0
	call _read_dword_pci
	
	and	eax			  ,0xFFFFFFF0
	mov [via_nabmbar] ,eax

;get status bit from pci config

   mov dword [regs_cnt],VIA_ACLINK_STAT
   call _read_byte_pci

;test if codec is ready

   test al,VIA_ACLINK_C00_READY
   jz codec_readynnnnn
   jmp codec_ready
   codec_readynnnnn:

	mov esi,text_cdc_init
	call _draw_cars

    ;codec is not ready

		 ;send enable/init command
		 mov byte [value]		, (VIA_ACLINK_CTRL_ENABLE|VIA_ACLINK_CTRL_RESET|VIA_ACLINK_CTRL_SYNC)
         mov dword [regs_cnt]	, VIA_ACLINK_CTRL
         call _write_byte_pci
         
         call delay1_4ms

	     ;reset the ctrl register
         mov dword [regs_cnt]	, VIA_ACLINK_CTRL
         mov byte [value]		, 0
         call _write_byte_pci
         call delay1_4ms

	     ;send enable/init command

         mov dword [regs_cnt] ,  VIA_ACLINK_CTRL
         mov byte  [value]    ,  (VIA_ACLINK_CTRL_ENABLE | VIA_ACLINK_CTRL_RESET | VIA_ACLINK_CTRL_PCM | VIA_ACLINK_CTRL_VRA)
         call _write_byte_pci
         call delay1_4ms

   codec_ready:
	
   call	auvia_codec_waitready
   call	auvia_codec_waitvalid

ret

_via82xx_create:
	
	call _pci_enable_device
	call _via_pci_enable

	mov esi	,txt_via_driver_init
	call   _draw_cars
	
	mov ecx	,[via_nabmbar]
	call   _draw_dword_hex
	
	mov dl	,10
	call   _draw_car
	
	mov esi,text_cdc_enable
	call _draw_cars

	call _via82xx_amp_enable
	
	call _pci_set_master

	call _create_buffer_table
	call _via_prefill
	
	mov dword [via_temp],0x00
	loop_via_channel_setup:

		mov edx,[via_temp]
		call _via82xx_setup_channel
		
		add dword [via_temp],0x10
		cmp dword [via_temp],0x40
	jle	loop_via_channel_setup


ret

via_disp_addr:

	mov [via_temp2]	, edx
	add edx			,	AUVIA_RP_DMAOPS_BASE
	call via_reg_read_32
	mov [via_temp]	, eax
	
	mov eax			, [via_temp2]
	shr eax			, 4
	add eax			, 20
	mul dword [char_screen_width]
	add eax			, 60
	shl eax			, 1
	mov edx			, eax	

	mov eax			,[via_temp]
	mov [hex_value]	,eax
	call _put_dword_hex
ret



via_cur_cnt:

	mov [via_temp2]	,edx
	add edx			,AUVIA_8233_CUR_COUNT
	call via_reg_read_32

	mov [via_temp]	, eax
	
	mov eax			, [via_temp2]
	shr eax			, 4
	add eax			, 20
	mul dword [char_screen_width]
	add eax			, 51
	shl eax			, 1

	mov edx			, eax	

	mov eax			,[via_temp]
	mov [hex_value]	,eax
	call _put_dword_hex
	


ret




%if 0
	mov dword [regs_cnt],VIA_FUNC_ENABLE
	call _read_byte_pci
	mov [via_old_legacy],al
	
	mov dword [regs_cnt],VIA_PNP_CONTROL
	call _read_byte_pci
	mov [via_old_legacy_cfg],al

	mov al		  , [via_old_legacy]
	and al		  , ~(VIA_FUNC_ENABLE_SB|VIA_FUNC_ENABLE_FM)
	mov [value]	  , al

	mov dword [regs_cnt],VIA_FUNC_ENABLE
	call _write_byte_pci
	
	mov dword [regs_cnt],VIA_ACLINK_STAT
	call _read_byte_pci
	
	test al,VIA_ACLINK_C00_READY
	jnz codec_ready01
	
		mov byte [value]	,	(VIA_ACLINK_CTRL_ENABLE |VIA_ACLINK_CTRL_RESET |VIA_ACLINK_CTRL_SYNC)
		mov dword [regs_cnt],	VIA_ACLINK_CTRL
		call _write_byte_pci
		
		mov ecx,100
		call _delay_x_ms

		mov byte [value]	,	0
		mov dword [regs_cnt],	VIA_ACLINK_CTRL
		call _write_byte_pci
		
		mov ecx,100
		call _delay_x_ms

		mov byte  [value]	,	VIA_ACLINK_CTRL_INIT
		mov dword [regs_cnt],	VIA_ACLINK_CTRL
		call _write_byte_pci

		mov ecx,100
		call _delay_x_ms

	codec_ready01:


	mov dword [regs_cnt],	VIA_ACLINK_CTRL
	call _read_byte_pci
	
	and al,VIA_ACLINK_CTRL_INIT
	cmp al,VIA_ACLINK_CTRL_INIT
	je vra_enabled
		
		mov esi,text_cdc_not_enable
		call _draw_cars
		
		mov byte  [value]   ,   VIA_ACLINK_CTRL_INIT
		mov dword [regs_cnt],	VIA_ACLINK_CTRL
		call _write_byte_pci

		mov ecx,100
		call _delay_x_ms
		
	vra_enabled:
	

	mov dword [counter], (3 * HZ )/4 +1

	loop_via_ready02:
	
		mov dword [regs_cnt],	VIA_ACLINK_STAT
		call _read_byte_pci
		
		test al,VIA_ACLINK_C00_READY
		jnz codec_ready02
		
		call delay1_4ms
	
	dec dword [counter]
	cmp dword [counter],0
	jg loop_via_ready02

	codec_ready02:
	
	mov esi,text_cdc_ready
	call _draw_cars
	
	mov dword [regs_cnt],PCI_BASE_ADDRESS_0
	call _read_dword_pci
	mov [via_port],eax
	
	mov dl,32
	call _draw_car
	
	mov ecx,[via_port]
	call _draw_dword_hex
	
	mov dl,10
	call _draw_car
	
	GET_VIA_REG_L [via_port],AC97
	
	test eax,VIA_REG_AC97_BUSY
	jz codec_ready03
		mov esi,text_cdc_eready
		call _draw_cars
	codec_ready03:
	
	mov dword [counter],0
	loop_via_volume:
	
		mov ax,[counter]
		mov bx,0x10
		mul bx
		
		mov bx,ax
		add bx,[via_port]
		
		mov dx,bx
		add dx,VIA_REG_OFS_PLAYBACK_VOLUME_L
		mov al,VIA_DXS_MAX_VOLUME
		out dx,al
		
		mov dx,bx
		add dx,VIA_REG_OFS_PLAYBACK_VOLUME_L+1
		mov al,VIA_DXS_MAX_VOLUME
		out dx,al
			
	inc dword [counter]
	cmp dword [counter],4
	jl loop_via_volume
%endif	

