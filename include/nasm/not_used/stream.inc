stream_read_size :dd 0
stream_write_size:dd 0

stream_mode_loop :dd 0
stream_nsample	 :dd 0
stream_frek		 :dd 440.0
stream_rate		 :dd 22050
volume			 :dd 0x7FFF



%if 0
stream_addr_start:dd 0
stream_addr_end  :dd 0
stream_cur_ptr   :dd 0

stream_init:

	mov [stream_addr_start]	,edx
	mov [stream_cur_ptr]	,edx
	mov [stream_addr_end  ]	,ecx

ret
%endif

stream_read:

	mov [stream_read_size]			,edx
	mov dword [stream_write_size]	,0
	
	loop_create_sin:
		fldpi	
		fmul	dword [f_two]
		fmul	dword [stream_frek]			;(osc*2pi/sec)
		fimul	dword [stream_nsample]		;sample * (osc/sec)
		fidiv	dword [stream_rate]			;((sample * osc)/sec)/(sample/sec)
											;((sample * osc)/sec)*(sec/sample)
											;((sample * osc*sec)/(sec*sample))
											; osc
		fsin
		fimul	dword [volume]
		fist	word  [edi]
		fistp	word  [edi+2]

		inc	dword [stream_nsample]
		add dword [stream_write_size],0x04
		add edi						 ,0x04
		
		mov eax,[stream_write_size]
		cmp eax,dword[stream_read_size]
	jl loop_create_sin
			
		
	
%if 0	
	mov eax					,[stream_addr_end]
	sub eax					,[stream_cur_ptr]		;eax remaining byte in the stream buffer
	cmp [stream_read_size] ,eax							
	jle buffer_ok
		;stream read size > remaing byte
		
		;copy remaining bytes
		mov esi					,[stream_cur_ptr]
		mov ecx					,eax	
		rep movsb
		
		;reset cur_ptr
		mov ebx					,[stream_addr_start]
		mov [stream_cur_ptr]	,ebx
		
		;fill the rest from begining
		mov esi					,[stream_cur_ptr]
		mov ecx					,[stream_read_size]
		sub ecx					,eax
		rep movsb
		
		;update stream_cur_ptr
		mov [stream_cur_ptr]	,esi
	
	jmp end_read
	buffer_ok:
	
		;stream read size <= remaing byte
		mov esi,[stream_cur_ptr]
		mov ecx,[stream_read_size]
		rep movsb
		
		;update stream_cur_ptr
		mov [stream_cur_ptr],esi

	end_read:
%endif	


ret