




;------------------------------------
%define IA32_APIC_BASE_MSR 0x1B
%define IA32_APIC_BASE_MSR_ENABLE 0x800

;redirect first DWORD
%define IA32_APIC_REDIR_IVEC 0x000000FF ;interuption vector
%define IA32_APIC_REDIR_DVMD 0x00000700 
%define IA32_APIC_REDIR_LOGC 0x00000800 ;0 => physique - 1 => logical
%define IA32_APIC_REDIR_DVRY 0x00001000 ;0 = IDLE - 1 = Send Pending 
%define IA32_APIC_REDIR_IPOL 0x00002000 ;0 = low polarity - 1 = high polarity
%define IA32_APIC_REDIR_RIRR 0x00004000 ;
%define IA32_APIC_REDIR_TRIG 0x00008000 ;1= level - 0 = edge
%define IA32_APIC_REDIR_INTM 0x00010000 ;1=masked 
%define IA32_APIC_REDIR_RSV1 0xFFFE0000 ;reserved

;redirect second DWORD
%define IA32_APIC_REDIR_RSV2  0x00FFFFFF ;reserved
%define IA32_APIC_REDIR_LDEST 0xFF000000 ;destination field for logical mode  (set of cpu)
%define IA32_APIC_REDIR_PDEST 0x0F000000 ;destination field for physical mode (apic id)


;delivery mode bit 8:10
;000 fixed - 001 low prio - 010 SMI - 011 res - 100 NMI - 101 INIT - 110 Reserv - 111 ExtINT

%define IA32_APIC_FIXED        0x00000000
%define IA32_APIC_LOPRIO       0x00000100
%define IA32_APIC_SMI          0x00000200
%define IA32_APIC_RES          0x00000300
%define IA32_APIC_NMI          0x00000400
%define IA32_APIC_INIT         0x00000500
%define IA32_APIC_RES2         0x00000600
%define IA32_APIC_EINT         0x00000700

;destination mode bit 11 1=logical(cpu id) 0=physical(apic id)
%define IA32_APIC_DEST_LOG     0x00000800
%define IA32_APIC_DEST_PHYS    0x00000000

;signal polarity  bit 13
%define IA32_APIC_DEST_POLLOW  0x00002000
%define IA32_APIC_DEST_POLHI   0x00000000

;signal triger mode  bit 15
%define IA32_APIC_TRIG_LEVEL   0x00008000
%define IA32_APIC_TRIG_EDGE    0x00000000

;interuption masked
%define IA32_APIC_INT_MASK_OFF 0x00010000
%define IA32_APIC_INT_MASK_ON  0x00000000

;destination field bit 56:59 APIC ID
%define IA32_APIC_DEST_ID0 0x00000000
%define IA32_APIC_DEST_ID1 0x01000000
%define IA32_APIC_DEST_ID2 0x02000000
%define IA32_APIC_DEST_ID3 0x03000000
%define IA32_APIC_DEST_ID4 0x04000000
%define IA32_APIC_DEST_ID5 0x05000000
%define IA32_APIC_DEST_ID6 0x06000000
%define IA32_APIC_DEST_ID7 0x07000000

%define IA32_INIT_FLAFS IA32_APIC_LOPRIO|IA32_APIC_DEST_PHYS|IA32_APIC_DEST_POLLOW|IA32_APIC_TRIG_LEVEL|IA32_APIC_INT_MASK_ON 

%macro read_apic_redir_table 2

    lea edi          ,[IO_APIC_BASE]            ; get APIC base address
    mov dword [edi] , 0x10+%1*2                             ; select 1st 32b of 1st IO register
    mov eax         , [edi+0x04]                             
    mov [%2]        , eax                                 ; read value in redirec_table

%endmacro

%macro write_apic_redir_table 2

   lea edi         , [IO_APIC_BASE]            ; get APIC base address
   
   mov dword [edi] , 0x010+%1*2                ; select 1st 32b of 1st IO register
   mov eax         , [%2]                             
   mov [edi+0x04]  , eax                       ; read value in redirec_table
 
   mov dword [edi] , 0x011+%1*2                ; select 2nd 32b of 1st IO register
   mov eax         , [%2+4]                             
   mov [edi+0x10]  , eax                       ; read value in redirec_table

%endmacro

_write_apic_reg:

    lea edi		         , [IO_APIC_BASE]   ; get APIC base address
    mov dword[edi+ecx]   , eax     		    ;enable apic controler
    
 ;  lea edi		         , [IO_APIC_BASE]    ; get APIC base address
 ;  mov dword[edi]       , ecx     		    ;enable apic controler    
 ;  mov dword[edi+0x04]  , eax     		    ;enable apic controler    
 ;  mov dword[edi+ecx+4] , edx     		    ;enable apic controler

ret

_read_apic_reg:

    lea edi  , [IO_APIC_BASE]         
    mov eax  , dword[edi+ecx]         

;   lea edi		      , [IO_APIC_BASE]    ; get APIC base address
;   mov dword[edi]    , ecx     		    ;enable apic controler    
;   mov eax           , dword[edi+0x04]   ;enable apic controler    
;   mov edx			  , dword[edi+ecx+4]       

ret

IO_APIC_BASE		: dd 0
IO_APIC_ID			: dd 0
IO_APIC_MAX			: dd 0
IO_APIC_VER			: dd 0

redir_tbl			: dd 2
text_apicbase		: db 'APIC base: ',0
text_apic_dis		: db 'APIC disabled: ',0
text_enable_apic	: db 'enable APIC ',10,0
text_get_apic_info	: db 'Get APIC info: ',10,0
text_apic_done		: db 'APIC configuration done: ',10,0


%if 0
	/* Local APIC registers */ 
	LapicID		= 0x0020,	/* ID */ 
	LapicVER	= 0x0030,	/* Version */ 
	LapicTPR	= 0x0080,	/* Task Priority */ 
	LapicAPR	= 0x0090,	/* Arbitration Priority */ 
	LapicPPR	= 0x00A0,	/* Processor Priority */ 
	LapicEOI	= 0x00B0,	/* EOI */ 
	LapicLDR	= 0x00D0,	/* Logical Destination */ 
	LapicDFR	= 0x00E0,	/* Destination Format */ 
	LapicSVR	= 0x00F0,	/* Spurious Interrupt Vector */ 
	LapicISR	= 0x0100,	/* Interrupt Status (8 registers) */ 
	LapicTMR	= 0x0180,	/* Trigger Mode (8 registers) */ 
	LapicIRR	= 0x0200,	/* Interrupt Request (8 registers) */ 
	LapicESR	= 0x0280,	/* Error Status */ 
	LapicICRLO	= 0x0300,	/* Interrupt Command */ 
	LapicICRHI	= 0x0310,	/* Interrupt Command [63:32] */ 
	LapicTIMER	= 0x0320,	/* Local Vector Table 0 (TIMER) */ 
	LapicPCINT	= 0x0340,	/* Performance COunter LVT */ 
	LapicLINT0	= 0x0350,	/* Local Vector Table 1 (LINT0) */ 
	LapicLINT1	= 0x0360,	/* Local Vector Table 2 (LINT1) */ 
	LapicERROR	= 0x0370,	/* Local Vector Table 3 (ERROR) */ 
	LapicTICR	= 0x0380,	/* Timer Initial Count */ 
	LapicTCCR	= 0x0390,	/* Timer Current Count */ 
	LapicTDCR	= 0x03E0,	/* Timer Divide Configuration */ 
%endif

%define APIC_ESR					0x280
%define APIC_ID						0x20
%define APIC_LVR					0x30
%define APIC_SPIV					0xF0

%define APIC_VECTOR_MASK		    0x000FF
%define APIC_SPIV_APIC_ENABLED		(1<<8)
%define APIC_SPIV_FOCUS_DISABLED	(1<<9)
%define SPURIOUS_APIC_VECTOR    	0xff


%define APIC_ADDR_MASK				0xFFFFF000


_init_apic:
  
   cli 

   mov ecx,IA32_APIC_BASE_MSR
   rdmsr                        ; get intel model specific model register

   test eax,IA32_APIC_BASE_MSR_ENABLE
   jnz apic_ok
     or   eax,IA32_APIC_BASE_MSR_ENABLE   ;or the enable field
     mov  ecx,IA32_APIC_BASE_MSR          ;set the register
     wrmsr                                ;write back the model specific register
   apic_ok:

   and eax			   ,APIC_ADDR_MASK
   shr eax			   ,12
   mov [IO_APIC_BASE]  ,eax	   ; decode APIC base address
   
   mov esi,text_apicbase
   call _draw_cars
   
   mov cx,[IO_APIC_BASE+2]
   call _draw_word_hex

   mov cx,[IO_APIC_BASE]
   call _draw_word_hex

   mov dl,10
   call _draw_car

   ;---------------------------   
   ;enable stuff
   ;---------------------------
   
   mov esi,text_enable_apic
   call _draw_cars

   ;leaving PIC mode,enabling APIC mode

   mov al,0x70
   out 0x22,al

   mov al,0x01
   out 0x23,al
   
   ;Pound the ESR really hard over the head with a big hammer - mbligh 

   xor eax ,eax
   mov ecx ,APIC_ESR
   call _write_apic_reg

   xor eax ,eax
   mov ecx ,APIC_ESR
   call _write_apic_reg

   xor eax ,eax
   mov ecx ,APIC_ESR
   call _write_apic_reg

   xor eax ,eax
   mov ecx ,APIC_ESR
   call _write_apic_reg
   
   mov esi,text_get_apic_info
   call _draw_cars

  ;---------------------------   
  ;get apic id
  ;---------------------------

  mov  ecx			,APIC_ID
  call _read_apic_reg

  shr eax			,24
  mov [IO_APIC_ID]	,eax

  ;---------------------------   
  ;get apic version
  ;---------------------------

  mov  ecx			,APIC_LVR
  call _read_apic_reg
 
  mov dword [IO_APIC_MAX] , eax
  shr dword [IO_APIC_MAX] , 16				;decode max interupt
  and dword [IO_APIC_MAX] , 0x000000FF                            

  mov dword [IO_APIC_VER] , eax
  and dword [IO_APIC_VER] , 0x000000FF		;decode version

  mov cx				 ,[APIC_ID]
  call _draw_byte_hex

  mov dl				 ,32
  call _draw_car

  mov cl				  ,[IO_APIC_MAX]
  call _draw_byte_hex

  mov dl                  ,32
  call _draw_car

  mov cl                  ,[IO_APIC_VER]
  call _draw_byte_hex

  mov dl                  ,10
  call _draw_car
  
  
   ;---------------------------
   ;init redirection table   
   ;---------------------------

; fill redirection registers 16-23 to vector 16-23
%assign i 0
%rep    8

     read_apic_redir_table i,redir_tbl

      mov  dword  [redir_tbl]  , i+0x20 | IA32_INIT_FLAFS

     write_apic_redir_table i,redir_tbl
%assign i i+1 
%endrep


%assign i 16
%rep    8
    ;read_apic_redir_table i,redir_tbl
    ;mov eax               ,IA32_APIC_REDIR_IVEC
    ;not eax
    ;and dword [redir_tbl],eax
    ; or  byte  [redir_tbl],i+0x70
    ;write_apic_redir_table i,redir_tbl
%assign i i+1 
%endrep

sti     

ret
