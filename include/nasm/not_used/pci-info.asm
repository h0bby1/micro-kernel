

txt_name_unknown: db 'device name unknown',0

pci_class_names:
db 'reserved       ', 0 ,    ; 00
db 'disk           ', 0 ,    ;01
db 'network        ', 0 ,    ;02
db 'display        ', 0 ,    ;03
db 'multimedia     ', 0 ,    ;04
db 'memory         ', 0 ,    ;05
db 'bridge         ', 0 ,    ;06
db 'communication  ', 0 ,    ;07
db 'system         ', 0 ,    ;08
db 'input          ', 0 ,    ;09
db 'docking station', 0 ,    ;0A
db 'CPU            ', 0 ,    ;0B
db 'serial bus     ', 0 ,    ;0C


media_subclass:
db 'video  ',0
db 'audio  ',0
db 'other  ',0

display_subclass:
db 'VGA    ', 0 , 
db 'SVGA   ', 0 ,
db 'XGA    ', 0 ,
db 'other  ', 0 

;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
;
;
;
;
;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

_dump_vendor_list:

;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*


   mov cx,[pci_vendors]
   call _draw_word_hex
  
   mov dl ,10
   call _draw_car

   lea esi,[pci_vendors+4]
   call _draw_cars

   mov dx,[pci_vendors+2]
   add dx,2
   
ret

;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
;
; in dx vendor_id
;
;
;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

_dump_vendor_id:

;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

    lea esi,[pci_vendors]
    vdr_loop:
       
       cmp dx ,word [esi]
       je print

	xor ebx,ebx
	mov bx,[esi+2]
        add esi,ebx

    jmp vdr_loop

print:  

  lea  esi,[esi+4]
  call _draw_cars
     
ret

;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
;
; in ax vendor_id
; in dx device_id
;
;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

_dump_device_id:

;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

    lea esi,[pci_vendors]
    vdr_dev_loop:
       
       cmp ax ,word [esi]
       je vendor_found

		xor ebx,ebx
		mov bx,[esi+2]
        add esi,ebx

    jmp vdr_dev_loop
 
    ret

vendor_found:
    
    add esi,2
    
    devstl:
	lodsb
        cmp al,0
        je devstf
    jmp devstl

   devstf:
	
	mov ecx		,[esi]
	test ecx	,ecx
	jz no_pci_dev_name_found
	add esi		,4
    
	loop_devices:
		cmp dx,[esi]
        je device_found
		add esi,32
	loopnz loop_devices
	
	no_pci_dev_name_found:
	
	mov esi,txt_name_unknown
	call _draw_cars
	
ret
	
device_found:    
		lea esi,[esi+2]
		call _draw_cars
ret

;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
;
; in edx addr index 0-6
;
;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
 _dump_address:
;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
    

    mov eax,[pci_config+16+edx*4]
    mov [org_addr],eax


    test dword [org_addr],1
    jz not_io_addr

       mov esi,text_io
       call _draw_cars
       
       mov cx ,[org_addr]   
       and cx ,0xFFFC
       call _draw_word_hex
    
       mov dl,32
       call _draw_car
 

       mov cx ,[size]   
       call _draw_word_hex

       mov dl,10
       call _draw_car

    jmp end_dump_addr

    not_io_addr:		        ;addr & 0 mean address
      
      mov  eax,[org_addr]
      and  eax,6
      shr  eax,1			;2-3rd bit addr = 0 mean 32b address

       cmp  eax,0
       jne not_type_0
         mov eax,[org_addr]
         and eax,0xFFFFFFF0
         jmp draw_it
       not_type_0:

       cmp  eax,1			;2-3rd bit addr = 1 mean 24b address
       jne not_type_1
         mov eax,[org_addr]
	 and eax,0x00FFFFF0
	 jmp draw_it
       not_type_1:

       jmp end_dump_addr
  
       draw_it:

         mov [rl_addr],eax

         mov  cx ,[rl_addr+2]
         call _draw_word_hex
 
         mov cx ,[rl_addr]   
         call _draw_word_hex

         mov dl,32
         call _draw_car
 
        mov byte[txt_size],'b'

         mov ecx ,[size]   
         cmp ecx,1024
         jl no_div
           shr ecx ,10      ;kilo
             mov byte[txt_size],'k'
             cmp ecx,1024
             jl no_div
                shr ecx ,10      ;Mega
                 mov byte[txt_size],'m'
	 no_div:

         call _draw_word_dec

         mov dl,[txt_size]
         call _draw_car
  
          mov dl,10
          call _draw_car

     end_dump_addr:


ret
%macro draw_pci_addr 1

    mov  ecx ,[pci_config+%1]
    call _draw_dword_hex

    mov dl,32
    call _draw_car
%endmacro

;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
; in [pci_bus]
; in [pci_dev]
; in [pci_fnc]
; in [regs_cnt]
;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

_dump_pci_config:     ;(int bus, int device, int func)

;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

  ;------------  draw bus number  ------------
  
    mov esi,text_bus
    call _draw_cars

    mov cx,[pci_bus]
    call _draw_byte_hex

    mov dl,32
    call _draw_car

  ;------------ draw device number ------------
    mov esi,text_dev
    call _draw_cars

    mov cx,[pci_dev]
    call _draw_byte_hex

  ;------------  draw function number  ------------

    mov esi,text_fnc
    call _draw_cars

    mov cx,[pci_fnc]
    call _draw_byte_hex

    mov dl,10
    call _draw_car


  ;------------  draw class info  ------------

    movzx esi,byte [pci_config+11]
    shl   esi,4
    add   esi,pci_class_names
    call _draw_cars

    mov dl,32
    call _draw_car

  ;------------  draw subclass info  ------------

     cmp  byte [pci_config+10],0x80
     jne known
       lea   esi,[media_subclass+16]
       call _draw_cars
       jmp   hdr_typ
     
     known:

     cmp byte [pci_config+11],  4
     jne not_media
       movzx esi,byte [pci_config+10]
       shl   esi,3
       add   esi,media_subclass
       call _draw_cars
       jmp   hdr_typ
     
     not_media:

     cmp byte [pci_config+11],  3
     jne not_dispt


       movzx esi,byte [pci_config+10]
       shl   esi,3
       add   esi,display_subclass
       call _draw_cars
       jmp   hdr_typ

     not_dispt:

       mov  cl,byte [pci_config+10]
       call _draw_byte_hex

     hdr_typ:

    mov dl,32
    call _draw_car
  ;------------  draw headertype info  ------------

    mov cl,[pci_config+14]
    call _draw_byte_hex

    mov dl,32
    call _draw_car

  ;------------  draw venderinfo  ------------
    mov  dx,[pci_config]
    call _dump_vendor_id

    mov dl,10
    call _draw_car

   ;------------  draw device name  ------------
    mov  cx ,[pci_config+2]
    call _draw_word_hex

    mov esi,text_rev
    call _draw_cars

    mov  cl ,[pci_config+8]
    call _draw_byte_hex

    mov dl,32
    call _draw_car

    mov  ax,[pci_config]
    mov  dx,[pci_config+2]
    call _dump_device_id

    mov dl,32
    call _draw_car

   ;------------  draw cmd reg  ------------
    mov  cx ,[pci_config+4]
    call _draw_word_hex
 
    mov dl,32
    call _draw_car

  ;------------  draw stat reg ------------
    mov  cx ,[pci_config+6]
    call _draw_word_hex

    mov dl,32
    call _draw_car
 

  ;------------ draw interupt line ------------
    mov  cl ,[pci_config+60]
    call _draw_byte_hex

    mov dl,32
    call _draw_car

  ;------------ draw interupt pin ------------

    mov  cl ,[pci_config+61]
    call _draw_byte_hex

    mov dl,32
    call _draw_car

    mov dl,10
    call _draw_car
    
    draw_pci_addr 0x10
    draw_pci_addr 0x14
    draw_pci_addr 0x18    
    draw_pci_addr 0x1C
    draw_pci_addr 0x20
    draw_pci_addr 0x24
      
    mov dl,10
    call _draw_car
     
   ;------------  draw device addr0 ------------

    mov  edx,0
    call _get_pci_address_size

    mov  edx,1
    call _get_pci_address_size


ret

