intel_ac97_cold:db 'intel ac97 cold',10,0
intel_ac97_warm:db 'intel ac97 warm',10,0
intel_ac97_notok:db 'intel ac97 not ok',10,0
intel_ac971_ok:db 'intel ac97 primary ok',0
intel_ac972_ok:db 'intel ac97 secondary ok',0

sub_vendor:dw 0
sub_device:dw 0



%define   ICH_RCS		0x00008000	; read completion status 
%define   ICH_MCINT		0x00000080	; MIC capture interrupt */
%define   ICH_MOINT		0x00000004	; modem playback interrupt 
%define   ICH_MIINT		0x00000002	; modem capture interrupt 
%define   ICH_POINT		0x00000040	; playback interrupt */
%define   ICH_PIINT		0x00000020	; capture interrupt */


%define   ICH_REG_GLOB_STA	0x30	; dword - global status 
%define   ICH_REG_GLOB_CNT	0x2c	; dword - global control
%define   ICH_REG_ACC_SEMA	0x34	; byte - codec write semaphore */
%define   ICH_CAS		0x01		; codec access semaphore */


%define   ICH_STATUS             ICH_RCS | ICH_MCINT | ICH_POINT | ICH_PIINT;

%define   ICH_CTRL_INIT 	ICH_RCS | ICH_MIINT | ICH_MOINT
%define   ICH_CTRL_TEST 	ICH_PCR | ICH_SCR | ICH_TCR

%define   ICH_RCS		0x00008000	; read completion status */

%define   ICH_SCR		0x00000200	; secondary (AC_SDIN1) codec ready */
%define   ICH_PCR		0x00000100	; primary (AC_SDIN0) codec ready */
%define   ICH_TCR		0x10000000	; ICH4: tertiary (AC_SDIN2) codec ready */

%define   ICH_ACLINK		0x00000008	; AClink shut off 

%define   ICH_AC97WARM		0x00000004	; AC'97 warm reset */
%define   ICH_AC97COLD		0x00000002	; AC'97 cold reset */

%define   ICH_SIS_PCM_246_MASK	0x000000c0	; 6 channels (SIS7012)
%define   ICH_PCM_246_MASK	0x00300000	    ; 6 channels (not all chips) 

%define ICH_REG_OFF_BDBAR	 0x0       ; dword - buffer descriptor list base address 
%define ICH_REG_OFF_CR	     0x0b      ; byte - control register 
%define ICH_RESETREGS		 0x02      ; reset busmaster registers

text_read:db 'read ',0
text_write:db 'write ',0

;in cx reg to read

intel_read_reg:


;        mov edx,[pci_config+20]
;        and edx,0xFFFFFFF0
;        add edx,ICH_REG_GLOB_STA   ;dx = pci_addr2 + ICH_REG_GLOB_STA
;        mov eax,[edx]		   ;get 32bit status

        mov dx,[pci_config+16]
        and dx,0xFFFC
        add dx,cx                  ;dx = pci_addr2 + cx
        in eax,dx		   ;get 32bit status
 
         mov [value],eax		   ;store in [value]


        mov cx,dx		  ;print port
        call _draw_word_hex

        mov dl,32
        call _draw_car

        mov esi,text_read
        call _draw_cars

        mov dl,32
        call _draw_car

        mov cx,[value+2]	  ;print value
        call _draw_word_hex
      
        mov cx,[value]
        call _draw_word_hex

        mov dl,10
        call _draw_car

ret


;in cx reg to read

intel_read_regw:


;       mov edx,[pci_config+20]
;       and edx,0xFFFFFFF0
;       add dx ,cx  	   ;dx = pci_addr2 + cx
;       mov ax ,[edx]	   ;get 16bit status

       mov dx,[pci_config+16]
       and dx,0xFFFC
       add dx,cx                  ;dx = pci_addr1 + cx
       in  ax,dx		           ;get 32bit status

        mov [value],ax	  ;store in [value]

        mov cx,dx		  ;print port
        call _draw_word_hex

        mov dl,32
        call _draw_car

        mov esi,text_read
        call _draw_cars

        mov dl,32
        call _draw_car

        mov cx,[value]
        call _draw_word_hex

        mov dl,10
        call _draw_car

ret

intel_read_regb:


;      mov edx,[pci_config+20]
;      and edx,0xFFFFFFF0
;      add dx ,cx  	   ;dx = pci_addr2 + cx
;      mov al ,[edx]	   ;get 16bit status

       mov dx,[pci_config+16]
       and dx,0xFFFC
       add dx,cx                  ;dx = pci_addr1 + cx
       in  al,dx		           ;get 32bit status

        mov [value],al	  ;store in [value]

        mov cx,dx		  ;print port
        call _draw_word_hex

        mov dl,32
        call _draw_car

        mov esi,text_read
        call _draw_cars

        mov dl,32
        call _draw_car

        mov cl,[value]
        call _draw_byte_hex

        mov dl,10
        call _draw_car

ret

intel_write_reg:


;       mov edx,[pci_config+20]
;       and edx,0xFFFFFFF0
;       add edx,ICH_REG_GLOB_STA   ;dx = pci_addr2 + ICH_REG_GLOB_STA
;       mov [edx],eax		  ;put eax = value |ICH_CTRL_INIT

       mov dx,[pci_config+16]
       and dx,0xFFFC
       add dx,cx  		   ;dx = pci_addr2 + ICH_REG_GLOB_STA
       out dx,eax		   ;set 32bit status

         mov [value],eax		   ;store in [value]

         mov cx,dx		  ;print port
         call _draw_word_hex

         mov dl,32
         call _draw_car

         mov esi,text_write
         call _draw_cars

         mov dl,32
         call _draw_car

         mov cx,[value+2]	  ;print value
         call _draw_word_hex
      
         mov cx,[value]
         call _draw_word_hex

        mov dl,10
        call _draw_car

ret


delay:

mov ecx,100000

loop_nopy:
  nop
loopnz loop_nopy

ret
  


intel_write_regw:


;      mov edx,[pci_config+20]
;      and edx,0xFFFFFFF0
;      add dx,cx  		  ;dx = pci_addr2 + ICH_REG_GLOB_STA
;      mov [edx],ax		  ;put eax = value |ICH_CTRL_INIT

       mov dx,[pci_config+16]
       and dx,0xFFFC
       add dx,cx  		   ;dx = pci_addr2 + ICH_REG_GLOB_STA
       out dx,ax		   ;set 32bit status

         mov [value],ax		   ;store in [value]
       
         mov cx,dx		  ;print port
         call _draw_word_hex

         mov dl,32
         call _draw_car

         mov esi,text_write
         call _draw_cars

         mov dl,32
         call _draw_car

         mov cx,[value]
         call _draw_word_hex

         mov dl,10
         call _draw_car
ret


ac97_mixer_init:

  mov dword [regs_cnt],44
  call _read_dword_pci

  mov [sub_vendor],ax
  shr eax,16
  mov [sub_device],ax

  mov cx,[sub_vendor]
  call _draw_word_hex

  mov dl,32
  call _draw_car

  mov cx,[sub_device]
  call _draw_word_hex

  mov dl,10
  call _draw_car
  
  
  ;real init start here

  mov dword [counter],0
  loop_sema:

     mov cx,ICH_REG_ACC_SEMA
     call intel_read_regb
     
     test byte [value],ICH_CAS
     jz sema_ok
     call delay
     
     mov  cx,0
   	 call intel_read_regw

  inc dword [counter]
  cmp dword [counter],3
  jl loop_sema

  ret
  
  sema_ok:
  
%if 0
  mov cx,AC97_VENDOR_ID1
; add cx,0x80
  call intel_read_regw

  mov cx,AC97_VENDOR_ID2
; add cx,0x80
  call intel_read_regw

%endif

  mov ax,0
  mov cx,AC97_RESET
; add cx,0x80  
  call intel_write_regw
  
  mov dword [counter],0
  ac97_loop_wait:
  
     call delay
     
     
     mov ax,0x8a05
     mov cx,AC97_REC_GAIN
;    add cx,0x80
     call intel_write_regw

     call delay
     
     mov cx,ICH_REG_ACC_SEMA
     call intel_read_regb

     call delay
     
     mov cx,AC97_REC_GAIN
;     add cx,0x80
     call intel_read_regw
     mov  ax,[value]
     and  ax,0x7fff
  
     cmp ax,0x0a05
     je end_wait_loop
  inc dword [counter]
  cmp dword [counter],2
  jle ac97_loop_wait

  ret

end_wait_loop:

  mov cx,AC97_VENDOR_ID1
;  add cx,0x80
  call intel_read_regw

  mov cx,AC97_VENDOR_ID2
;  add cx,0x80
  call intel_read_regw


  mov cx,AC97_EXTENDED_ID
;  add cx,0x80
  call intel_read_regw
  
  

  ;ac97->ext_id = snd_ac97_read(ac97, AC97_EXTENDED_ID);       
 
  
  
  ;snd_ac97_write_cache(ac97, AC97_REC_GAIN, 0x8a05);
  ;if ((snd_ac97_read(ac97, AC97_REC_GAIN) & 0x7fff) == 0x0a05)	return 0;
  ; snd_ac97_write(ac97, AC97_RESET, 0);


ret


init_intel_snd:

        mov  cx,ICH_REG_GLOB_STA
        call intel_read_reg

        mov eax,[value]
	    and eax,ICH_STATUS

        mov  cx,ICH_REG_GLOB_STA
        call intel_write_reg

        mov  cx,ICH_REG_GLOB_CNT
        call intel_read_reg
       
        mov ebx,ICH_ACLINK
	    or  ebx,ICH_PCM_246_MASK
        not ebx

        mov eax,[value]
        and eax,ebx		 ; eax = [value] &  ~ (ICH_ACLINK|ICH_PCM_246_MASK)

	
        test eax,ICH_AC97COLD	 ; test ICH_AC97COLD
        jnz ac97_not_cold	 ;cold is set
          or eax,ICH_AC97COLD
      
        jmp ac97_ok1

	     ac97_not_cold:		 ;cold is not set
            or eax,ICH_AC97WARM
            jmp ac97_ok1

    	ac97_ok1:

        mov dword [counter],0

        mov  cx,ICH_REG_GLOB_CNT
        call intel_write_reg

        loop_int_ac97:

           mov  cx,ICH_REG_GLOB_CNT
           call intel_read_reg
 
           test eax,ICH_AC97WARM
           jz warm
 
       inc dword[counter]
       cmp dword[counter],100
       jle loop_int_ac97
  
       mov esi,intel_ac97_cold
       call _draw_cars
  ret
        
warm:

       mov esi,intel_ac97_warm
       call _draw_cars

        mov dword [counter],0

        loop_init_ok:

            mov  cx,ICH_REG_GLOB_STA
            call intel_read_reg

            test dword [value],ICH_CTRL_TEST
            jnz init_ok
	        
           inc dword[counter]
           cmp dword[counter],100
        jle loop_init_ok

        mov  esi,intel_ac97_notok
        call _draw_cars

     ret

init_ok:

      test dword [value],ICH_PCR
      jz not_primary
        ;mov esi,intel_ac971_ok
        ;call _draw_cars
      not_primary:

      test dword [value],ICH_SCR
      jz not_secondary
        ;mov esi,intel_ac972_ok
        ;call _draw_cars
      not_secondary:
       
       
       xor cx,cx
       loop_zer:

          mov dx,ICH_REG_OFF_CR         ;ICH_REG_OFF_CR + chip->ichd[i].reg_offset
          add dx,cx
          mov al,0                      ;
          out dx,al                     ;put byte 0

          add cx,0x10
          cmp cx,0x30
       jl loop_zer

       xor cx,cx
       loop_rst:

          mov dx,ICH_REG_OFF_CR         ;ICH_REG_OFF_CR + chip->ichd[i].reg_offset
          add dx,cx
          mov al,ICH_RESETREGS          
          out dx,al                     ;put byte ICH_RESETREGS

          add cx,0x10
          cmp cx,0x30
       jl loop_rst

       xor cx,cx
       loop_adr:

          mov dx ,ICH_REG_OFF_BDBAR
          add dx,cx
          lea eax,[sound_buffer_aera]     
          out dx ,eax                     ;put byte 0
 
          add cx,0x10
          cmp cx,0x30

       jl loop_adr


       call ac97_mixer_init
          
%if  0
  	   /* disable interrupts */
	   for (i = 0; i < chip->bdbars_count; i++)
	  	  iputbyte(chip, ICH_REG_OFF_CR + chip->ichd[i].reg_offset, 0x00);
 	   /* reset channels */
	    for (i = 0; i < chip->bdbars_count; i++)
		  iputbyte(chip, ICH_REG_OFF_CR + chip->ichd[i].reg_offset, ICH_RESETREGS);
 	   /* initialize Buffer Descriptor Lists */
 	   for (i = 0; i < chip->bdbars_count; i++)
		  iputdword(chip, ICH_REG_OFF_BDBAR + chip->ichd[i].reg_offset, chip->ichd[i].bdbar_addr);
%endif

ret


%if 0

	/* put logic to right state */
	/* first clear status bits */
	status = ICH_RCS | ICH_MIINT | ICH_MOINT;
	cnt = igetdword(chip, ICHREG(GLOB_STA));
	iputdword(chip, ICHREG(GLOB_STA), cnt & status);

	/* ACLink on, 2 channels */
	cnt = igetdword(chip, ICHREG(GLOB_CNT));
	cnt &= ~(ICH_ACLINK);
	/* finish cold or do warm reset */
	cnt |= (cnt & ICH_AC97COLD) == 0 ? ICH_AC97COLD : ICH_AC97WARM;
	iputdword(chip, ICHREG(GLOB_CNT), cnt);
	end_time = (jiffies + (HZ / 4)) + 1;
	do {
		if ((igetdword(chip, ICHREG(GLOB_CNT)) & ICH_AC97WARM) == 0)
			goto __ok;
		do_delay(chip);
	} while (time_after_eq(end_time, jiffies));
	snd_printk("AC'97 warm reset still in progress? [0x%x]\n", igetdword(chip, ICHREG(GLOB_CNT)));
	return -EIO;

      __ok:
	if (probing) {
		/* wait for any codec ready status.
		 * Once it becomes ready it should remain ready
		 * as long as we do not disable the ac97 link.
		 */
		end_time = jiffies + HZ;
		do {
			status = igetdword(chip, ICHREG(GLOB_STA)) & (ICH_PCR | ICH_SCR | ICH_TCR);
			if (status)
				break;
			do_delay(chip);
		} while (time_after_eq(end_time, jiffies));
		if (! status) {
			/* no codec is found */
			snd_printk(KERN_ERR "codec_ready: codec is not ready [0x%x]\n", igetdword(chip, ICHREG(GLOB_STA)));
			return -EIO;
		}

		/* up to two codecs (modem cannot be tertiary with ICH4) */
		nstatus = ICH_PCR | ICH_SCR;

		/* wait for other codecs ready status. */
		end_time = jiffies + HZ / 4;
		while (status != nstatus && time_after_eq(end_time, jiffies)) {
			do_delay(chip);
			status |= igetdword(chip, ICHREG(GLOB_STA)) & nstatus;
		}

%endif
