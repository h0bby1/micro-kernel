
;#define IS_R300_VARIANT ((info->ChipFamily == CHIP_FAMILY_R300)  ||  \
;        (info->ChipFamily == CHIP_FAMILY_RV350) ||  \
;        (info->ChipFamily == CHIP_FAMILY_R350)  ||  \
;        (info->ChipFamily == CHIP_FAMILY_RV380) ||  \
;        (info->ChipFamily == CHIP_FAMILY_R420))

_radeon_is_r300	 :dd 0
_radeon_format   :dd 0
_radeon_chip_type:dd 0
_radeon_membase  :dd 0
_radeon_mmmiobase:dd 0
_radeon_biosaddr :dd 0
_radeon_videoram :dd 0
_radeon_memcntl	 :dd 0
_radeon_buscntl	 :dd 0
_radeon_vram_type:dd 0

%define ofset_ovr_clr					0
%define ofset_ovr_wid_left_right		4
%define ofset_ovr_wid_top_bottom		8
%define ofset_ov0_scale_cntl			12
%define ofset_mpp_tb_config				16
%define ofset_mpp_gb_config				20
%define ofset_subpic_cntl				24
%define ofset_viph_control				28
%define ofset_i2c_cntl_1				32
%define ofset_gen_int_cntl				36
%define ofset_cap0_trig_cntl			40
%define ofset_cap1_trig_cntl			44
%define ofset_bus_cntl					48
%define ofset_surface_cntl				52
%define ofset_bios_4_scratch			56
%define ofset_bios_5_scratch			60
%define ofset_bios_6_scratch			64


%define ofset_crtc_gen_cntl					0
%define ofset_crtc_ext_cntl					4
%define ofset_dac_cntl						8
%define ofset_crtc_h_total_disp				12
%define ofset_crtc_h_sync_strt_wid			16
%define ofset_crtc_v_total_disp				20
%define ofset_crtc_v_sync_strt_wid			24
%define ofset_crtc_offset					28
%define ofset_crtc_offset_cntl				32
%define ofset_crtc_pitch					36
%define ofset_disp_merge_cntl				40
%define ofset_grph_buffer_cntl				44
%define ofset_crtc_more_cntl				48


%macro MMIO_IN32 2 
	
	mov ebx,[_radeon_mmmiobase]
	add ebx,%1
	mov %0,[ebx]
	
%endmacro

#  define MMIO_OUT32(base, offset, val) \
	*(volatile CARD32 *)(void *)(((CARD8*)(base)) + (offset)) = (val)

_radeon_registers:

    dd 0,             ;            ovr_clr;
    dd 0,             ;            ovr_wid_left_right;
    dd 0,             ;            ovr_wid_top_bottom;
    dd 0,             ;            ov0_scale_cntl;
    dd 0,             ;            mpp_tb_config;
    dd 0,             ;            mpp_gp_config;
    dd 0,             ;            subpic_cntl;
    dd 0,             ;            viph_control;
    dd 0,             ;            i2c_cntl_1;
    dd 0,             ;            gen_int_cntl;
    dd 0,             ;            cap0_trig_cntl;
    dd 0,             ;            cap1_trig_cntl;
    dd 0,             ;            bus_cntl;
    dd 0,             ;            surface_cntl;
    dd 0,             ;            bios_4_scratch;
    dd 0,             ;            bios_5_scratch;
    dd 0,             ;            bios_6_scratch;

	;---- Other registers to save for VT switches ;----  

    dd 0,             ;            dp_datatype;
    dd 0,             ;            rbbm_soft_reset;
    dd 0,             ;            clock_cntl_index;
    dd 0,             ;            amcgpio_en_reg;
    dd 0,             ;            amcgpio_mask;
    dd 0,             ;            surfaces[8][3];
	
	;---- CRTC registers ---- 
_radeon_CRTC_registers:

    dd 0,             ;            crtc_gen_cntl;
    dd 0,             ;            crtc_ext_cntl;
    dd 0,             ;            dac_cntl;
    dd 0,             ;            crtc_h_total_disp;
    dd 0,             ;            crtc_h_sync_strt_wid;
    dd 0,             ;            crtc_v_total_disp;
    dd 0,             ;            crtc_v_sync_strt_wid;
    dd 0,             ;            crtc_offset;
    dd 0,             ;            crtc_offset_cntl;
    dd 0,             ;            crtc_pitch;
    dd 0,             ;            disp_merge_cntl;
    dd 0,             ;            grph_buffer_cntl;
    dd 0,             ;            crtc_more_cntl;

	; ---- CRTC2 registers ---- 
_radeon_CRTC2_registers:	
    dd 0,             ;            crtc2_gen_cntl;
	dd 0,             ;            dac2_cntl;
    dd 0,             ;            disp_output_cntl;
    dd 0,             ;            disp_hw_debug;
    dd 0,             ;            disp2_merge_cntl;
    dd 0,             ;            grph2_buffer_cntl;
    dd 0,             ;            crtc2_h_total_disp;
    dd 0,             ;            crtc2_h_sync_strt_wid;
    dd 0,             ;            crtc2_v_total_disp;
    dd 0,             ;            crtc2_v_sync_strt_wid;
    dd 0,             ;            crtc2_offset;
    dd 0,             ;            crtc2_offset_cntl;
    dd 0,             ;            crtc2_pitch;
	
	;---- Flat panel registers ----
_radeon_FPANEL_registers:	
    dd 0,             ;            fp_crtc_h_total_disp;
    dd 0,             ;            fp_crtc_v_total_disp;
    dd 0,             ;            fp_gen_cntl;
    dd 0,             ;            fp2_gen_cntl;
    dd 0,             ;            fp_h_sync_strt_wid;
    dd 0,             ;            fp2_h_sync_strt_wid;
    dd 0,             ;            fp_horz_stretch;
    dd 0,             ;            fp_panel_cntl;
    dd 0,             ;            fp_v_sync_strt_wid;
    dd 0,             ;            fp2_v_sync_strt_wid;
    dd 0,             ;            fp_vert_stretch;
    dd 0,             ;            lvds_gen_cntl;
    dd 0,             ;            lvds_pll_cntl;
    dd 0,             ;            tmds_pll_cntl;
    dd 0,             ;            tmds_transmitter_cntl;

	; ---- Computed values for PLL ----
_radeon_PPL_values:	
    dd 0,             ;           dot_clock_freq;
    dd 0,             ;           pll_output_freq;
    dd 0,             ;           feedback_div;
    dd 0,             ;           post_div;

	; ---- PLL registers ----
_radeon_PPL_registers:		
    dd 0,             ;			  ppll_ref_div;
    dd 0,             ;			  ppll_div_3;
    dd 0,             ;           htotal_cntl;

	;---- Computed values for PLL2 ---
_radeon_PPL2_values:	
    dd 0,             ;            dot_clock_freq_2;
    dd 0,             ;            pll_output_freq_2;
    dd 0,             ;            feedback_div_2;
    dd 0,             ;            post_div_2;

	;----	PLL2 registers ----
_radeon_PPL2_registers:	
    dd 0,             ;            p2pll_ref_div;
    dd 0,             ;            p2pll_div_0;
    dd 0,             ;            htotal_cntl2;



_radeon_init_common_registers:
   
    lea edi								, [_radeon_registers]
    
    mov [edi+ofset_ovr_clr]				, 0x00000000
    mov [edi+ofset_ovr_wid_left_right]	, 0x00000000
    mov [edi+ofset_ovr_wid_top_bottom]	, 0x00000000
    mov [edi+ofset_ov0_scale_cntl]		, 0x00000000
    mov [edi+ofset_subpic_cntl]			, 0x00000000
    mov [edi+ofset_viph_control]		, 0x00000000    
    mov [edi+ofset_i2c_cntl_1]			, 0x00000000    
    mov [edi+ofset_rbbm_soft_reset]		, 0x00000000    
    mov [edi+ofset_cap0_trig_cntl]		, 0x00000000    
    mov [edi+ofset_cap1_trig_cntl]		, 0x00000000    
    
    mov eax							    , [_radeon_buscntl]
    mov [edi+bus_cntl]					, eax        
   
   
    ; If bursts are enabled, turn on discards
    ; Radeon doesn't have write bursts

    test  [edi+bus_cntl],RADEON_BUS_READ_BURST
    
    jz no_radeon_bus_read_burst
		or [edi+bus_cntl],RADEON_BUS_RD_DISCARD_EN
	no_radeon_bus_read_burst:
	
ret

_radeon_init_crtc_registers:
	
	lea edi		,[_radeon_CRTC_registers]
	
	;---- crtc_gen_cntl -----
	
	mov eax				,RADEON_CRTC_EXT_DISP_EN
	or  eax				,RADEON_CRTC_EN
	
	mov ebx				,[_radeon_format]
	shl ebx				,8

	or  eax				,ebx
	
	test [mode_flags]	,V_DBLSCAN
	
	jz no_radeon_dbl_scan
		or  eax     ,RADEON_CRTC_DBL_SCAN_EN
	no_radeon_dbl_scan:
	
	test [mode_flags]	,V_CSYNC
	
	jz no_radeon_dbl_vsync
		or  eax     ,RADEON_CRTC_CSYNC_EN
	no_radeon_dbl_vsync:
	
	test [mode_flags]	,V_INTERLACE
	
	jz no_radeon_interlace
		or eax		,RADEON_CRTC_INTERLACE_EN
	no_radeon_interlace:
	
	mov [edi+ofset_crtc_gen_cntl]	,eax


	

	;(if (info->DisplayType == MT_DFP) | (info->DisplayType == MT_LCD))

		;---- crtc_ext_cntl -----
		mov [edi+ofset_crtc_ext_cntl]	,RADEON_VGA_ATI_LINEAR|RADEON_XCRT_CNT_EN

		;---- crtc_ext_cntl -----
	
		mov eax							,[edi+ofset_crtc_gen_cntl]
		and eax							,~(RADEON_CRTC_DBL_SCAN_EN |RADEON_CRTC_CSYNC_EN |RADEON_CRTC_INTERLACE_EN)
		mov [edi+ofset_crtc_gen_cntl]	,eax
		

	;---- ofset_dac_cntl -----
	mov [edi+ofset_dac_cntl]		,RADEON_DAC_MASK_ALL|RADEON_DAC_VGA_ADR_EN|RADEON_DAC_8BIT_EN

	;---- ofset_crtc_h_total_disp -----
	mov eax,[mode_CrtcHTotal]
	shr eax,3
	dec eax
	and eax,0x3ff
	
	mov ebx,[mode_CrtcHDisplay]
	shr ebx,3
	dec ebx
	and ebx,0x1ff
	shl ebx,16
	
	or  eax,ebx
	
	mov [edi+ofset_crtc_h_total_disp],eax
	
	
	;---- ofset_crtc_h_sync_strt_wid -----
	
	mov eax							 ,[mode_CrtcHSyncEnd]
	sub eax							 ,[mode_CrtcHSyncStart]
	shr eax							 ,3
	mov [hsync_wid]					 ,eax
	
	test eax						 ,eax
	jnz radeon_hsync_wid_ok
		mov [hsync_wid]	,1
	radeon_hsync_wid_ok:
	
	mov eax							,[mode_CrtcHSyncStart]
	sub eax							,8
	mov [hsync_start]				,eax
	
	mov eax							,[hsync_start]
	and eax							,0x1fff
	
	mov ebx							,[hsync_wid]
	and ebx							,0x3f
	shl ebx							,16
	
	or eax							,ebx

	test [mode_flags],V_NHSYNC
	jz radeon_no_vnhsync
		or eax,RADEON_CRTC_H_SYNC_POL
	radeon_no_vnhsync:

	mov [edi+ofset_crtc_h_sync_strt_wid],eax
	
	;---- ofset_crtc_v_total_disp -----
	
	mov eax	,  [mode_CrtcVTotal]
	dec eax
	and eax ,	0xFFFF
	
	mov ebx		[mode_CrtcVDisplay]
	dec ebx
	shl ebx ,	16
	or eax	,	ebx
	
	mov [edi+ofset_crtc_v_total_disp],eax
	

	;---- ofset_vsync_wid -----
	
	mov eax						, [mode_CrtcVSyncEnd]
	sub eax					    , [mode_CrtcVSyncStart]
	mov [edi+ofset_vsync_wid]	, eax
		
	test eax,eax
	jnz radeon_vsync_wid_not_zero
		mov [edi+ofset_vsync_wid]	,1
	radeon_vsync_wid_not_zero:
		
	;---- ofset_crtc_v_sync_strt_wid -----	

	mov eax			,[mode_CrtcVSyncStart]
	dec eax
	and eax			,0xfff
	
	mov ebx			,[vsync_wid]
	and ebx			,0x1f
	shl ebx			,16
	
	and eax			,ebx
	
	test [mode_flags],V_NVSYNC
	jz radeon_no_vnvsync2
		or eax,RADEON_CRTC_V_SYNC_POL
	radeon_no_vnvsync2:
	
	mov [edi+ofset_crtc_v_sync_strt_wid]	,eax
	
	;---- ofset_crtc_offset -----	
	mov dword [edi+ofset_crtc_offset]		, 0
   
   	;---- ofset_crtc_offset_cntl -----	
    
    MMIO_IN32 eax,RADEON_CRTC_OFFSET_CNTL 
   
    test [_radeon_is_r300],1
    jz radeon_tile_no_r300
		or eax,( R300_CRTC_X_Y_MODE_EN | R300_CRTC_MICRO_TILE_BUFFER_DIS |  R300_CRTC_MACRO_TILE_EN)
    jmp radeon_tile_done
    radeon_tile_no_r300:
		or eax,RADEON_CRTC_TILE_EN
    radeon_tile_done:
    
    mov dword [edi+ofset_crtc_offset_cntl], eax

	;---- ofset_crtc_pitch -----	
	
	mov eax		,[displayWidth]
	mul			[bitsPerPixel]
	
	mov ebx		,[bitsPerPixel]
	shl ebx		,3
	dec ebx
	
	add eax		,ebx
	
	; eax = eax / (pScrn->bitsPerPixel * 8)
	shr eax		,8

	mov ebx		,eax
	shl ebx		,16
	or  eax		,ebx
	
	mov [edi+ofset_crtc_pitch],eax
	
	;---- ofset_crtc_more_cntl -----
	
	xor eax,eax
	
	cmp dword [ChipFamily],CHIP_FAMILY_RS100
	je is_rs100_200

	cmp dword [ChipFamily],CHIP_FAMILY_RS200
	je is_rs100_200
	
	jmp not_100_200
		
	is_rs100_200:		
        ; This is to workaround the asic bug for RMX, some versions
        ;   of BIOS dosen't have this register initialized correctly.
		or eax,RADEON_CRTC_H_CUTOFF_ACTIVE_EN
	not_100_200:
	
	mov [edi+ofset_crtc_more_cntl],eax

 
	;---- ofset_surface_cntl -----
 
	mov [edi+ofset_surface_cntl],0
	
	;---- ofset_disp_merge_cntl -----
	
	mov eax							,[saved_disp_merge_cntl]
	and eax							,~RADEON_DISP_RGB_OFFSET_EN
	mov [edi+ofset_disp_merge_cntl]	,eax
    
    
%if 0
	#if X_BYTE_ORDER == X_BIG_ENDIAN
		/* Alhought we current onlu use aperture 0, also setting aperture 1 should not harm -ReneR */
		switch (pScrn->bitsPerPixel) {
		case 16:
		save->surface_cntl |= RADEON_NONSURF_AP0_SWP_16BPP;
		save->surface_cntl |= RADEON_NONSURF_AP1_SWP_16BPP;
		break;

		case 32:
		save->surface_cntl |= RADEON_NONSURF_AP0_SWP_32BPP;
		save->surface_cntl |= RADEON_NONSURF_AP1_SWP_32BPP;
		break;
		}
	#endif
%endif

ret




_radeon_get_clock_info:

    RADEONInfoPtr info = RADEONPTR (pScrn);
    RADEONPLLPtr pll = &info->pll;
    double min_dotclock;

    if (RADEONGetClockInfoFromBIOS(pScrn)) 
    {
		if (pll->reference_div < 2) 
		{
			;retrive it from register setting for fitting into current PLL algorithm.
			;We'll probably need a new routine to calculate the best ref_div from BIOS 
			;provided min_input_pll and max_input_pll 
			CARD32 tmp;
			tmp = INPLL(pScrn, RADEON_PPLL_REF_DIV);
			if (IS_R300_VARIANT || (info->ChipFamily == CHIP_FAMILY_RS300)) 
			{
				pll->reference_div = (tmp & R300_PPLL_REF_DIV_ACC_MASK) >> R300_PPLL_REF_DIV_ACC_SHIFT;
			} 
			else 
			{
				pll->reference_div = tmp & RADEON_PPLL_REF_DIV_MASK;
			}
			if (pll->reference_div < 2) pll->reference_div = 12;
		}
	} 
	else 
	{
		xf86DrvMsg (pScrn->scrnIndex, X_WARNING,  "Video BIOS not detected, using default clock settings!\n");
       /* Default min/max PLL values */
       if (info->ChipFamily == CHIP_FAMILY_R420) {
           pll->min_pll_freq = 20000;
           pll->max_pll_freq = 50000;
       } else {
           pll->min_pll_freq = 12500;
           pll->max_pll_freq = 35000;
       }

       if (RADEONProbePLLParameters(pScrn))
            return;

	if (info->IsIGP)
	    pll->reference_freq = 1432;
	else
	    pll->reference_freq = 2700;

	pll->reference_div = 12;
	pll->xclk = 10300;

        info->sclk = 200.00;
        info->mclk = 200.00;
    }

    xf86DrvMsg (pScrn->scrnIndex, X_INFO,
		"PLL parameters: rf=%d rd=%d min=%ld max=%ld; xclk=%d\n",
		pll->reference_freq,
		pll->reference_div,
		pll->min_pll_freq, pll->max_pll_freq, pll->xclk);

    /* (Some?) Radeon BIOSes seem too lie about their minimum dot
     * clocks.  Allow users to override the detected minimum dot clock
     * value (e.g., and allow it to be suitable for TV sets).
     */
    if (xf86GetOptValFreq(info->Options, OPTION_MIN_DOTCLOCK,
			  OPTUNITS_MHZ, &min_dotclock)) {
	if (min_dotclock < 12 || min_dotclock*100 >= pll->max_pll_freq) {
	    xf86DrvMsg(pScrn->scrnIndex, X_INFO,
		       "Illegal minimum dotclock specified %.2f MHz "
		       "(option ignored)\n",
		       min_dotclock);
	} else {
	    xf86DrvMsg(pScrn->scrnIndex, X_INFO,
		       "Forced minimum dotclock to %.2f MHz "
		       "(instead of detected %.2f MHz)\n",
		       min_dotclock, ((double)pll->min_pll_freq/1000));
	    pll->min_pll_freq = min_dotclock * 1000;
	}
    }
}


_radeon_init:



ret

_radeon_preinit:


ret