%define TEXT_SEG 0x28

;cursor_str_x: dd 0	   ;initial X position on line feed
;cursor_pos_x: dd 0	   ;current X position
;cursor_pos_y: dd 0	   ;current Y position

hex_chars   : db       '0123456789ABCDEF'

char_screen_width			:  dd 80
char_screen_height			:  dd 25

char_buffer_offset			:  dd 0
char_buffer_start_line		:  dd 0
char_buffer_loop_line		:  dd 0
char_buffer_loop_line_src	:  dd 0

text_color					:  db 0x07




char_buffer					:  times 1024*2 dw 0
char_buffer_lines_infos		:  times 64 dd 0

proutprout						:times 256*4 dd 0


char_buffer_nlines			:  dd 0

char_buffer_memory			:  dd char_buffer
char_buffer_lines_infos_ptr	:  dd char_buffer_lines_infos



char_buffer_txt_ofset		:  dd 0
char_buffer_line_cpy		:  dd 0

output_buffer_active:dd 1




bin_mask			:dd 0

_toggle_output_buffer_c:
	mov eax,[esp+4]
	mov dword [output_buffer_active],eax
	xor eax,eax
ret

;-------------------------------------------------------------------------------------
;const unsigned int set_output_buffer_addr_c(void *char_buffer,unsigned int *line_infos)
;-------------------------------------------------------------------------------------

_set_output_buffer_addr_c:
	mov eax							 ,[esp+4]
	mov [char_buffer_memory]		 ,eax
	
	mov eax							 ,[esp+8]
	mov [char_buffer_lines_infos_ptr],eax
	
	mov dword [char_buffer_offset],0
	mov dword [char_buffer_nlines]	,0
	mov eax,1
	ret 

ret
;-------------------------------------------------------------------------------------
;const short *get_output_buffer_line_c(unsigned int line_idx,unsigned int *line_len)
;-------------------------------------------------------------------------------------

_get_output_buffer_line_c:
	
	mov eax,[esp+4]
	cmp eax,[char_buffer_nlines]
	jl get_output_buffer_line_c_ok
		xor eax,eax
		ret
	get_output_buffer_line_c_ok:

	shl eax			,3
	add eax			,[char_buffer_lines_infos_ptr]
	
	push edi
	push edx
	mov edi			,[esp+16]
	
	mov edx			, [eax+4]
	mov dword [edi]	, edx
	
	pop edx
	pop edi
	
	
	mov eax,[eax]
	add eax,[char_buffer_memory]
ret


;-------------------------------------------------------------------------------------
;unsigned int get_num_output_buffer_line_c()
;-------------------------------------------------------------------------------------
_get_num_output_buffer_line_c:

  mov eax,[char_buffer_nlines]

ret

;-------------------------------------------------------------------------------------
;unsigned int _output_buffer()
;-------------------------------------------------------------------------------------

_output_buffer:

	cmp dword [output_buffer_active],1
	jne output_buffer_text_end
	
		pushfd
		pusha

        xor edi,edi
        
		mov dword [char_buffer_loop_line],0
		mov dword [char_buffer_loop_line_src],0
		mov dword [char_buffer_line_cpy],0
		
       
        loop_line_output_char:
        
			mov eax			,	[char_buffer_loop_line]
			cmp eax			,	[char_screen_height]
			jge output_buffer_text_end_loop
			
			cmp eax,[char_buffer_nlines]
			jge output_buffer_text_end_loop        
        
	        mov eax			,	[char_buffer_loop_line]
		    mul dword			[char_screen_width]
			shl eax			,	1
        	mov edi			,	eax
        	add edi			,	0xB8000

	        mov eax			,	[char_buffer_offset]
	        add eax			,	[char_buffer_loop_line_src]
			shl eax			,   3
			add eax			,	[char_buffer_lines_infos_ptr];
			
        	mov esi			,	[char_buffer_line_cpy]
        	shl esi			,	1
			add esi			,	[char_buffer_memory]
			add esi			,	[eax]
			
						
			mov	ecx			,	[eax+4]
			sub ecx			,	[char_buffer_line_cpy]
					
			cmp	ecx			,	65
			jle _output_buffer_ok
				mov ecx,65
				add [char_buffer_line_cpy]			 ,ecx
				rep movsw
								
				jmp _output_buffer_do_copy
			_output_buffer_ok:
				add dword [char_buffer_loop_line_src]	 ,1
				mov dword [char_buffer_line_cpy]		 ,0
				
				mov edx,65
				sub edx,ecx
				rep movsw
				
				mov ecx,edx
				xor eax,eax
				rep stosw
			_output_buffer_do_copy:
       
			
			
			inc dword [char_buffer_loop_line]

		jmp loop_line_output_char
			

   output_buffer_text_end_loop:
   
	    mov eax			,	[char_buffer_loop_line]
	    
	    cmp eax			,[char_screen_height]
	    jge no_end_copy_text_buf
			mul dword			[char_screen_width]
			shl eax			,	1
			mov edi			,	eax            
			add edi			,	0xB8000
	        
			
			mov eax,[char_screen_height]
			sub eax,[char_buffer_loop_line]
			mul dword	[char_screen_width]
			mov ecx,eax
			xor eax,eax
			rep stosw
       
		no_end_copy_text_buf:
           
	   call delay1_4ms
	   
	   popa
	   popfd
	   
	   output_buffer_text_end:
ret

next_line:

	mov eax							,	[char_buffer_nlines]
	shl eax							,   3
	add eax							,	[char_buffer_lines_infos_ptr]

	mov edx							,	[char_buffer_start_line]
	mov	[eax]						,	edx

	mov edx							,   [char_buffer_txt_ofset]
	sub edx							,	[char_buffer_start_line]
	shr edx							,	1
	mov	[eax+4]						,	edx


	add dword[char_buffer_nlines]	,	1

	mov eax							,	[char_buffer_txt_ofset]
	mov [char_buffer_start_line]	,	eax


	 cmp dword[char_buffer_nlines],25
	jl no_char_vscroll
	   mov eax,[char_buffer_nlines]
	   sub eax,25
	   mov [char_buffer_offset],eax
	no_char_vscroll:


ret

;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
; in esi text ptr
;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
_draw_cars:
;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

loop_print:

   ;--- grab next char in al	---
   lodsb
   
   ;------ exit loop on char 0 ------ 
   cmp al,0
   je  end_print
   
   cmp  al,10
   jne no_rtrl
		
	   ;--- line feed on car 10	---
	   call next_line
	   call _output_buffer
	   jmp loop_print

   no_rtrl:

   ;------ calc buffer offset ------ 
   
   mov edi  , [char_buffer_memory]
   add edi  , [char_buffer_txt_ofset]

   mov ah	,	[text_color]
   stosw

   skip_car:


   add dword [char_buffer_txt_ofset], 2
   
   
   mov eax							, [char_buffer_start_line]
   add eax							, 120
   cmp dword [char_buffer_txt_ofset], eax
   jl no_rtrl2
	   call next_line
	   call _output_buffer
   no_rtrl2:
   
jmp loop_print

end_print:



ret



;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
; in esi text ptr
;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
_draw_n_cars:
;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

sub esp,4

mov dword [esp],0

loop_print_n:

   ;--- grab next char in al	---
   
   cmp dword [esp],1
   je no_load_char_n
		lodsb
     
		cmp  al,10
		jne no_next_line_n
		
			;--- line feed on car 10	---
			call next_line
			call _output_buffer
			
			dec  ecx
			test ecx,ecx
			jz end_print_n
			jmp loop_print_n
	   
		no_next_line_n:
		
		cmp al,0
		jne end_load_char_print_n
			mov dword [esp],1
	
	no_load_char_n:
		
		mov al,'0'
   end_load_char_print_n:
   

   ;------ calc buffer offset ------ 
   
   mov edi  , [char_buffer_memory]
   add edi  , [char_buffer_txt_ofset]

   mov ah	, [text_color]
   stosw

   
   add dword [char_buffer_txt_ofset], 2
   
   
   mov eax							, [char_buffer_start_line]
   add eax							, 120
   cmp dword [char_buffer_txt_ofset], eax
   jl no_rtrl4
	   call next_line
	   call _output_buffer
   no_rtrl4:   
   
   dec  ecx
   cmp  ecx,0
   jg loop_print_n

end_print_n:

add esp,4

ret


;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
; in dl car to draw
;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
_draw_car:	
;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

   mov  bl,dl
   
   cmp  bl,10
   jne no_rtrl1
		call next_line
		call _output_buffer
		ret
   no_rtrl1:

   mov edi  , [char_buffer_memory]
   add edi  , [char_buffer_txt_ofset]

   mov ah	,0x07
   mov al	,bl
   stosw
   
   add dword [char_buffer_txt_ofset] , 2
   
   mov eax							, [char_buffer_start_line]
   add eax							, 120
   cmp dword [char_buffer_txt_ofset], eax
   jl no_rtrl5
	   call next_line
	   call _output_buffer
   no_rtrl5:      
   
   mov  dl,bl
   
ret


current_char: dd 0
;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
; in cl : byte to draw
;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

_draw_byte_hex:   

;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

 ;1rd hex char
 
    mov [current_char],cl
    
    xor ebx,ebx
    mov bl,[current_char]
    and bl,0xF0
    shr bl,4
    add ebx,hex_chars

    mov dl ,[ebx]
    call _draw_car

 ;2nd hex char

    xor ebx,ebx
    mov bl,[current_char]
    and bl,0x0F
    add ebx,hex_chars

    mov dl ,[ebx]
    call _draw_car

ret

;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
; in cx : word to draw
;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
_draw_word_hex:   
;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

 ;first hex char
 
	mov [current_char],cx

    xor ebx,ebx
    mov bx,[current_char]
    and bx,0xF000
    shr bx,12
    add ebx,hex_chars

    mov dl ,[ebx]
    call _draw_car

 ;2nd hex char
    
    xor ebx,ebx
    mov bx,[current_char]
    and bx,0x0F00
    shr bx,8
    add ebx,hex_chars

    mov dl ,[ebx]
    call _draw_car

 ;3rd hex char
    
    xor ebx,ebx
    mov bx,[current_char]
    and bx,0x00F0
    shr bx,4
    add ebx,hex_chars

    mov dl ,[ebx]
    call _draw_car


 ;4st hex char

    xor  ebx,ebx
    mov  bx,[current_char]
    and  bx,0x000F
    add  ebx,hex_chars

    mov dl ,[ebx]
    call _draw_car

ret

;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
; in ecx : dword to draw
;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
_draw_dword_hex:   
;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

	mov [current_char],ecx
 ;7th hex char

    xor ebx,ebx
    mov ebx,[current_char]
    and ebx,0xF0000000
    shr ebx,28
    add ebx,hex_chars

    mov dl ,[ebx]
    call _draw_car

 ;6th hex char
    
    xor ebx,ebx
    mov ebx,[current_char]
    and ebx,0x0F000000
    shr ebx,24
    add ebx,hex_chars

    mov dl ,[ebx]
    call _draw_car

 ;5th hex char
    
    xor ebx,ebx
    mov ebx,[current_char]
    and ebx,0x00F00000
    shr ebx,20
    add ebx,hex_chars

    mov dl ,[ebx]
    call _draw_car


 ;4st hex char

    xor  ebx,ebx
    mov  ebx,[current_char]
    and  ebx,0x000F0000
    shr  ebx,16
    add  ebx,hex_chars

    mov dl ,[ebx]
    call _draw_car


 ;3th hex char

    xor ebx,ebx
    mov ebx,[current_char]
    and ebx,0x0000F000
    shr ebx,12
    add ebx,hex_chars

    mov dl ,[ebx]
    call _draw_car

 ;2nd hex char
    
    xor ebx,ebx
    mov ebx,[current_char]
    and ebx,0x000F00
    shr ebx,8
    add ebx,hex_chars

    mov dl ,[ebx]
    call _draw_car

 ;1st hex char
    
    xor ebx,ebx
    mov ebx,[current_char]
    and ebx,0x00F0
    shr bx,4
    add ebx,hex_chars

    mov dl ,[ebx]
    call _draw_car


 ;0st hex char

    xor  ebx,ebx
    mov  ebx,[current_char]
    and  ebx,0x000F
    add  ebx,hex_chars

    mov dl ,[ebx]
    call _draw_car

ret



%include "print_c_funcs.inc"


