
%include "bios_call.inc"
%include "bios_call_int13.inc"
%include "bios_call_int10.inc"
%include "bios_call_int33.inc"

real_mode_seg			:	dd 0
real_mode_ofset			:	dd 0
protect_mmx_regs		:	dd 0
mmx_regs				:	times 8 dq 0
gen_regs				:	times 8 dq 0
seg_regs				:	times 8 dq 0
cpu_flag				:	dd 0
added_gdt_txt			:	db 'added new gdt entry ',0
gdt_entry_idx			:   dd 0
gdt_entry_addr			:   dd 0
cpu_flags				:	dd 0
num_int_called			:	times 1024 dd 0
num_exp_called			:	times 256 dd 0
save_esp				:	dd 0
save_ss					:	dd 0
cpu_eflags_real			:	dd 0
switch_esi				:	dd 0

save_mmx_regs:
	movq [mmx_regs+0] ,mm0
	movq [mmx_regs+8] ,mm1
	movq [mmx_regs+16],mm2
	movq [mmx_regs+24],mm3
	movq [mmx_regs+32],mm4
	movq [mmx_regs+40],mm5
	movq [mmx_regs+48],mm6
	movq [mmx_regs+56],mm7
ret

restore_mmx_regs:
	movq mm0,[mmx_regs+0] 
	movq mm1,[mmx_regs+8] 
	movq mm2,[mmx_regs+16]
	movq mm3,[mmx_regs+24]
	movq mm4,[mmx_regs+32]
	movq mm5,[mmx_regs+40]
	movq mm6,[mmx_regs+48]
	movq mm7,[mmx_regs+56]
ret

_set_mmx_protect_c:
	mov eax,[esp+4]
	mov dword[protect_mmx_regs],eax
	xor eax,eax
ret


setup_registers_from_stack:
	
	mov eax,[ebp+28]
	mov ecx,[ebp+24]
	mov edx,[ebp+20]
	mov ebx,[ebp+16]
   ;mov esp,[ebp+12]
   ;mov ebp,[ebp+8]
	mov esi,[ebp+4]
	mov edi,[ebp+0]
ret


setup_stack_from_registers:
	mov [ebp+28],eax
	mov [ebp+24],ecx
	mov [ebp+20],edx
	mov [ebp+16],ebx
   ;mov [ebp+12],esp
   ;mov [ebp+08],ebp
	mov [ebp+04],esi	
	mov [ebp+00],edi	
ret

;--------------------------
%macro real_mode_interupt_hndl 1

real_mode_interupt_%1:

	pushfd								;	push cpu flags from caller
	pushad								;	push registers from caller
	mov ebp , esp						;	save stack frame to saved registers location
	push ds								;	push segment registers
	push es	
	
	mov			ax,0x10					;	set segments selector to segment starting at memory 0x000
	mov			es,ax
	mov			ds,ax
	
	%if 0
		mov eax		,( %1 % 20)				;	set text output location
		mul dword	[char_screen_width]
		add eax		,66
		shl eax		,1
		mov edx     ,eax

		mov byte [value+0]		, 'I'		;	output some stuff
		mov byte [value+1]		, 0x06
		mov ax					,[value]
		mov [0xB8000+edx]		, ax
		add edx					, 2
	   
	   

		mov byte [value+0]		, ' '
		mov ax					,[value]
		mov [0xB8000+edx]		, ax
		add edx					, 2

		mov byte [hex_value]	 , %1
		call _put_byte_hex

		mov byte [value+0]		 ,' '
		mov ax					,[value]
		mov [0xB8000+edx]		, ax
		add edx					, 2
	%endif

	lea edi					 , [num_int_called+%1*4]
	inc						   dword [edi]
	
	%if 0
		mov al					 , [edi+1]
		mov byte [hex_value]	 , al
		call _put_byte_hex		
		
		lea edi					 , [num_int_called+%1*4]
		mov al					 , [edi]
		mov byte [hex_value]	 , al
		call _put_byte_hex
	%endif	

	;---------------------------------------------
	;start of the interupt routing
	;---------------------------------------------
	movzx	eax				,	word [0x0+(%1)*4]				;	get real mode interupt vector ofset from ivt
	movzx	ebx				,	word [0x0+(%1)*4+2]				;	get real mode interupt vector segment from ivt

	;shl		ebx			,	4
	;add		ebx			,	eax
	
	;mov		eax			,	ebx
	;and		eax			,	0xFFF
	;and		ebx			,	~0xFFF
	;shr		ebx			,	4
	
	shr			ebx			,	4
	
	mov		[real_mode_ofset],  eax
	mov		[real_mode_seg]  ,  ebx
	
	call  setup_registers_from_stack							;	get caller registers from the stack, but keep the current stack
	
	push dword [ebp+32]											;	set caller cpu flags from the stack
	popfd
	
	;setup the stack for 16 bit iret 
	
	push  word [ebp+32]											;	16 bit cpu flags
	push  word 0x50												;	return address segment selector starting at kern base
	push  word (end_of_real_interupt_%1-kern_base)				;	return address ofset from kern base
	
	;setup caller stack
	push  dword [ebp+32]										;   push caller flags on the stack (32 bit cpu flags)
	push  dword [real_mode_seg]									;	push real mode interupt vector address on the stack
	push  dword [real_mode_ofset]								
	
	push eax													;	restore caller segment selectors from the stack
		mov ax			,	[ebp-8]
		mov es			,	ax
		mov ax			,	[ebp-4]
		mov ds			,	ax
	pop eax
	
	db		0xCF												;	opcode for 32 bit iretd, set cp:eip and cpu flags from the stack
	
	end_of_real_interupt_%1:
	
		
	pushfd														;   store cpu flags from the interupt on the stack
	pop dword[ebp+32]
	
	pushfd														;   store cpu flags from the interupt on the iret return stack
	pop dword[ebp+32+12]
	
	jmp 0x08:end_of_real_interupt_2_%1							;	restore code segment selector to 0x08
	end_of_real_interupt_2_%1:
	
	
	call setup_stack_from_registers								;	store registers returned from the interupt on the stack
	
	mov	ax,0x10											;	restore segment selector to 0x10
	mov	es,ax
	mov	ds,ax
		
	
	lea esp				,	[ebp-8]								;	setup the stack to last push location
		
	pop es														;	restore segment registers to caller state
	pop ds
	
	popad														;	restore registers from the stack to state saved from interupt return
	popfd														;	restore cpu flags from state saved from interupt return

iret															;	return from interupt

%endmacro

real_mode_interupt_hndl 0x10
real_mode_interupt_hndl 0x15
real_mode_interupt_hndl 0x13


