pci_enable_device:db 'pci enable device',10,0
pci_cmd_reg:db 'pci command reg',10,0
text_pci_conf1:db 'dectected type 1 pci',10,0
text_pci_conf2:db 'dectected type 2 pci',10,0
text_pci_noconf:db 'pci type not dectected',10,0
text_pci_nocaplist:db 'capacity list not supported',10,0
text_pci_nocap:db 'capacity not supported',10,0
text_pci_capfound:db 'capacity found ',0
text_pci_setmaster:db 'setting pci master ',10,0
regs_cnt	: dd 0
pci_config	: times 64 dd 0
pci_bus		: dd 0
pci_dev		: dd 0
pci_fnc		: dd 0
pci_temp	: dd 0
rl_addr		: dd 0
org_addr	: dd 0
value		: dd 0
size		: dd 0
txt_size	: db 0
pci_drv_type: dd 0


%define pcibios_max_latency  255

%define BIT30 0x40000000
%define BIT31 0x80000000
%define PCI32 BIT31           ;		bitflag to signal 32bit access
%define PCI16 BIT30           ;		bitflag for 16bit access
%define PCI_STATUS		0x06  ;		/* 16 bits */

%define PCI_COMMAND		0x04	;/* 16 bits */
%define  PCI_COMMAND_IO		0x1	;/* Enable response in I/O space */
%define  PCI_COMMAND_MEMORY	0x2	;/* Enable response in Memory space */
%define  PCI_COMMAND_MASTER	0x4	;/* Enable bus mastering */

%define PCI_LATENCY_TIMER	0x0d;/* 8 bits */

%define PCI_CAP_LIST_NEXT	1 ;		/* Next capability in the list */
%define PCI_CAP_LIST_ID		0 ;		/* Capability ID */
%define PCI_STATUS_CAP_LIST	0x10 ;	/* Support Capability List */
%define PCI_CAPABILITY_LIST	0x34 ;	/* Offset of first capability list entry */

%define  PCI_CAP_ID_PM		0x01
%define  PCI_PM_CTRL		4	;			/* PM control and status register */
%define  PCI_PM_CTRL_STATE_MASK	0x0003	;	/* Current power state (D0 to D3) */

%define PCI_BASE_ADDRESS_SPACE	  0x01	 ;   /* 0 = memory, 1 = I/O */

%define PCI_BASE_ADDRESS_SPACE_IO 0x01
%define PCI_BASE_ADDRESS_SPACE_MEMORY 0x00
%define PCI_BASE_ADDRESS_0	0x10		;	/* 32 bits */
%define PCI_BASE_ADDRESS_1	0x14		;	/* 32 bits */


;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
;
; in [pci_config]
; in edx : addresse index 0-6
; p 224 pci ref
;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
_get_pci_address_size:
;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*


   lea eax,[0x10+edx*4]    ; eax = pci_config.address0+edx
   mov [regs_cnt],eax


; read original addresse register in [org_addr]

   call _read_dword_pci  
   mov  [org_addr],eax


; write 0xFFFFFFFF preserving the original first bit

   and  eax,1
   or   eax,0xFFFFFFFC   

   mov  [value],eax
   call _write_dword_pci


; read new value in eax
   call _read_dword_pci  

;clear good bits depending on i/o or mem type

   test dword [org_addr],1
   jz not_io
      and eax,0x0000FFFC
   jmp s_ok
   not_io:
      and eax,0xFFFFFFF0
   s_ok:

   not eax
   inc eax

   mov [size],eax

  ;put back [org_addr] in the register
   mov  eax,[org_addr]   
   mov  [value],eax 
   call _write_dword_pci

ret


pci_cap_pos	:dd 0
pci_cap		:dd 0
pci_pm_pos  :dd 0
pci_cmd     :dd 0
pci_cur_addr:dd 0
pci_lat_timer:dd 0

;------------------------------------------------------------------------------------------------
; in ebx CAP_ID
;------------------------------------------------------------------------------------------------
_pci_find_capability:
;------------------------------------------------------------------------------------------------

    mov [pci_cap] ,ebx
    
	mov dword [regs_cnt],PCI_STATUS
	call _read_word_pci
	
	test ax,PCI_STATUS_CAP_LIST

	jnz status_cap_list_ok
	
	    mov esi,text_pci_nocaplist
	    call _draw_cars
			
		xor eax,eax
		ret

	status_cap_list_ok:
	
	mov dword [regs_cnt],PCI_CAPABILITY_LIST
	call _read_byte_pci
	
	mov [pci_cap_pos],al
		
	mov dword [counter],0
	loop_get_cap_list:
	
		cmp dword [counter],48
		jge end_loop_get_cap_list
		
		inc dword [counter]
		
		cmp byte [pci_cap_pos],0x40
		jl end_loop_get_cap_list
			
		and byte [pci_cap_pos]	,~3

		xor eax				,eax
		mov al				,[pci_cap_pos]
		add al				,PCI_CAP_LIST_ID
		mov [regs_cnt]		,eax
		call _read_byte_pci
		
		cmp al,0xFF
		je end_loop_get_cap_list
		
		cmp al,[pci_cap]
		jne not_this_cap

			and eax,0x000000FF
			ret
		not_this_cap:
		
		mov eax			,[pci_cap_pos]
		add eax			,PCI_CAP_LIST_NEXT
		mov [regs_cnt]	,eax
		call _read_byte_pci
		
		mov [pci_cap_pos],al
	
	jmp loop_get_cap_list
		
	end_loop_get_cap_list:
	xor eax,eax
ret


_pci_set_power_state:

	mov  ebx,PCI_CAP_ID_PM
	call _pci_find_capability
	
	cmp eax,0
	jne capacity_found
	
	    mov esi,text_pci_nocap
		call _draw_cars
		ret
		
	capacity_found:
	
	mov [pci_pm_pos]	,eax
    
    mov esi				,text_pci_capfound
    call _draw_cars
    
    mov ecx,[pci_pm_pos]
    call _draw_dword_hex
    
    mov dl,10
    call _draw_car
    
	mov eax				,[pci_pm_pos]
	add eax				,PCI_PM_CTRL
	mov [regs_cnt]		,eax
	call _read_word_pci
	
	and ax				,~PCI_PM_CTRL_STATE_MASK
	mov [value]			,ax
	
	mov eax				, [pci_pm_pos]
	add eax				, PCI_PM_CTRL
	mov [regs_cnt]		, eax
	call _write_word_pci
	
	mov ecx,500
	call _delay_x_ms

ret

_pci_bios_enable:

	mov esi,pci_enable_device
	call _draw_cars

	mov  dword [regs_cnt],PCI_COMMAND
	call _read_word_pci
	mov  [pci_cmd],ax
	
	mov dword [pci_cur_addr],0
	loop_pci_ressources:
	
		mov eax			,[pci_cur_addr]
		shl eax			,2
		add eax			,PCI_BASE_ADDRESS_0
		mov [regs_cnt]	,eax
		call _read_dword_pci
		
		cmp eax,0xFFFFFFFF
		jne address_is_valid
			xor eax,eax
			jmp end_addr_conf
		address_is_valid:
		
		cmp eax,0x00000000
		jne address_is_valid2
			xor eax,eax
			jmp end_addr_conf
		address_is_valid2:

		and eax,PCI_BASE_ADDRESS_SPACE
		cmp eax,PCI_BASE_ADDRESS_SPACE_MEMORY
		jne io_address
			or word [pci_cmd],PCI_COMMAND_MEMORY
			jmp end_addr_conf
		io_address:
			or word [pci_cmd],PCI_COMMAND_IO
		end_addr_conf:
		
	inc dword [pci_cur_addr]
	cmp dword [pci_cur_addr],6
	jl loop_pci_ressources
	
	
	;output the value
	
	mov  ax		,[pci_cmd]
	mov  [value],ax
	or   ax		,4

	mov  dword [regs_cnt],PCI_COMMAND
	call _write_word_pci


	mov  dword [regs_cnt],PCI_COMMAND
	call _read_word_pci
	
	mov [pci_cmd],ax

ret

_pci_enable_device:

;call _pci_set_power_state
;call _pci_bios_enable

call _pci_set_master

ret

_pci_set_master:

	mov esi,text_pci_setmaster
	call _draw_cars

	mov dword [regs_cnt],PCI_COMMAND
	call _read_word_pci
	
	test ax,PCI_COMMAND_MASTER
	jnz pci_master_ok
		or ax		  ,PCI_COMMAND_MASTER
		mov [value]   ,ax
		mov dword [regs_cnt],PCI_COMMAND
		call _write_word_pci
		call delay1_4ms
	
	pci_master_ok:
	
	mov dword [regs_cnt],PCI_LATENCY_TIMER
	call _read_byte_pci
	
	cmp al,16
	jge latency_ge_16
		mov byte [pci_lat_timer],64
		jmp pci_latency_set
	latency_ge_16:
	
	cmp al,pcibios_max_latency
	jle latency_le_max
		mov byte [pci_lat_timer],pcibios_max_latency
		jmp pci_latency_set
	latency_le_max:
	
	mov [pci_lat_timer],al
	
	pci_latency_set:
	
	mov al		,[pci_lat_timer]
	mov [value]	,al
	
	mov dword [regs_cnt],PCI_LATENCY_TIMER
	call _write_byte_pci

ret



;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
; in [pci_bus]
; in [pci_dev]
; in [pci_fnc]

;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

_read_pci_config:     ;(int bus, int device, int func)

;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

  
  ;read 32bit from pci configuration registers
  mov dword [regs_cnt],0
  call _read_dword_pci
  
  ;store the 32 bits into pci_config
  mov [pci_config] , eax

  cmp word [pci_config],0xFFFF
  jne devok
      mov eax,0xFFFFFFFF
      ret
  devok:

  add  dword [regs_cnt],4

  loop_regs:

      ;read 32bit from pci configuration registers 
       call _read_dword_pci
      ;store the 32 bits into pci_config

       lea edi   , [pci_config]
       add edi   , dword [regs_cnt]
       mov [edi] , eax

       add  dword [regs_cnt],4
       cmp  dword [regs_cnt],64
   jl loop_regs

   xor eax,eax


ret



;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
; in [pci_bus]
; in [pci_dev]
; in [pci_fnc]
; in [regs_cnt]
;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

_write_pci_config:     ;(int bus, int device, int func)

;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

  ;store the 32 bits into pci_config
  mov  dword [regs_cnt],0

  loop_wregs:

      ;load the 32 bits from pci_config

      lea edi       , [pci_config]
      add edi       , dword [regs_cnt]
      mov eax       , [edi] 
      mov  [value]	, eax
      
      ;write 32bit from pci configuration registers 
      call _write_dword_pci
      
      add  dword [regs_cnt],4
      cmp  dword [regs_cnt],64
   jl loop_wregs

   xor eax,eax

ret



_pci_detect_conf_type:

	mov dx  ,0xCFB
	mov al  ,1
	out dx  ,al
	
	mov dx  ,0xCF8
	in  eax ,dx
	
	mov ecx ,eax
	
	mov eax,0x80000000
	out dx,eax

	xor eax ,eax
	in  eax ,dx
		
	cmp eax,0x80000000
	jne type1_chk1_failed
	
		mov eax,ecx
		out dx ,eax
		mov dword [pci_drv_type],1
		mov eax,dword [pci_drv_type]
		ret
	type1_chk1_failed:
		
	mov eax,ecx
	out dx,eax
	
	xor eax,eax
	
	mov dx ,0xCFB
	out dx ,al

	mov dx ,0xCF8
	out dx ,al

	mov dx ,0xCFA
	out dx ,al
	
	mov dx,0xCF8
	in  al,dx
	
	test al,al
	jnz type2_error
	
	mov dx,0xCFA
	in  al,dx
	
	test al,al
	jnz type2_error
	
	mov dword [pci_drv_type],2
	mov eax,dword [pci_drv_type]
	ret

type2_error:
	mov dword [pci_drv_type],0xFFFFFFFF
	mov eax,dword [pci_drv_type]
ret


_pci_display_conf_type:

	cmp dword [pci_drv_type],1
	jne pci_conf_nottype1
	
		mov esi,text_pci_conf1
		call _draw_cars
		ret
	pci_conf_nottype1:
	
	cmp dword [pci_drv_type],2
	jne pci_conf_nottype2

		mov esi,text_pci_conf2
		call _draw_cars
		ret
	pci_conf_nottype2:
	
	mov esi,text_pci_noconf
	call _draw_cars
ret




%include "pci_c_funcs.inc"



%if 0

int pci_find_capability(struct pci_dev *dev, int cap)
{
	u16 status;
	u8 pos, id;
	int ttl = 48;

	pci_read_config_word(dev, PCI_STATUS, &status);
	if (!(status & PCI_STATUS_CAP_LIST))
		return 0;
	switch (dev->hdr_type) {
	case PCI_HEADER_TYPE_NORMAL:
	case PCI_HEADER_TYPE_BRIDGE:
		pci_read_config_byte(dev, PCI_CAPABILITY_LIST, &pos);
		break;
	case PCI_HEADER_TYPE_CARDBUS:
		pci_read_config_byte(dev, PCI_CB_CAPABILITY_LIST, &pos);
		break;
	default:
		return 0;
	}
	while (ttl-- && pos >= 0x40) {
		pos &= ~3;
		pci_read_config_byte(dev, pos + PCI_CAP_LIST_ID, &id);
		if (id == 0xff)
			break;
		if (id == cap)
			return pos;
		pci_read_config_byte(dev, pos + PCI_CAP_LIST_NEXT, &pos);
	}
	return 0;
}
%endif




%if 0
int
pci_set_power_state(struct pci_dev *dev, int state)
{
	int pm;
	u16 pmcsr;

	/* bound the state we're entering */
	if (state > 3) state = 3;

	/* Validate current state:
	 * Can enter D0 from any state, but if we can only go deeper 
	 * to sleep if we're already in a low power state
	 */
	if (state > 0 && dev->current_state > state)
		return -EINVAL;
	else if (dev->current_state == state) 
		return 0;        /* we're already there */

	/* find PCI PM capability in list */
	pm = pci_find_capability(dev, PCI_CAP_ID_PM);
	
	/* abort if the device doesn't support PM capabilities */
	if (!pm) return -EIO; 

	/* check if this device supports the desired state */
	if (state == 1 || state == 2) {
		u16 pmc;
		pci_read_config_word(dev,pm + PCI_PM_PMC,&pmc);
		if (state == 1 && !(pmc & PCI_PM_CAP_D1)) return -EIO;
		else if (state == 2 && !(pmc & PCI_PM_CAP_D2)) return -EIO;
	}

	/* If we're in D3, force entire word to 0.
	 * This doesn't affect PME_Status, disables PME_En, and
	 * sets PowerState to 0.
	 */
	if (dev->current_state >= 3)
		pmcsr = 0;
	else {
		pci_read_config_word(dev, pm + PCI_PM_CTRL, &pmcsr);
		pmcsr &= ~PCI_PM_CTRL_STATE_MASK;
		pmcsr |= state;
	}

	/* enter specified state */
	pci_write_config_word(dev, pm + PCI_PM_CTRL, pmcsr);

	/* Mandatory power management transition delays */
	/* see PCI PM 1.1 5.6.1 table 18 */
	if(state == 3 || dev->current_state == 3)
		msleep(10);
	else if(state == 2 || dev->current_state == 2)
		udelay(200);
	dev->current_state = state;

	return 0;
}
%endif



