

;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
;
; in [pci_bus]
; in [pci_dev]
; in [pci_fnc]
; in [regs_cnt]
;
; out eax : addr
;
;
;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

_calc_pci_config_addr_type1:  ;  (int bus, int device, int func, int reg)

;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

; DWORD addr = 0x80000000L | 
;		(((DWORD)(bus & 0xFF)) << 16) |
;	      	((((unsigned)device) & 0x1F) << 11) |
;	     	 ((((unsigned)func) & 0x07) << 8) | 
;		(reg & 0xFC) ;

mov eax,0x80000000

mov edx,[pci_bus]
and edx,0xFF
shl edx,16

or  eax,edx

mov edx,[pci_dev]
and edx,0x1f
shl edx,11

or  eax,edx

mov edx,[pci_fnc]
and edx,0x07
shl edx,8

or  eax,edx

mov edx,[regs_cnt]
and edx,~3	
or  eax,edx	;address value in eax

ret


;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
;
; in [pci_bus]
; in [pci_dev]
; in [pci_fnc]
; in [regs_cnt]
;
;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

_read_dword_pci_type1:  ;  (int bus, int device, int func, int reg)

;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

call _calc_pci_config_addr_type1


mov dx	, 0x0CF8
out dx	, eax					;set our addr value eax

mov dx	,0x0CFC
in  eax	,dx						;eax dword config data

ret

;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
;
; in [pci_bus]
; in [pci_dev]
; in [pci_fnc]
; in [regs_cnt]
;
;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

_read_word_pci_type1:  ;  (int bus, int device, int func, int reg)

;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

call _calc_pci_config_addr_type1


mov dx	, 0x0CF8
out dx	, eax					;set our addr value eax

mov dx	,[regs_cnt]
and dx	,2
add dx	,0x0CFC
in  ax	,dx						;eax word config data

ret

;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
;
; in [pci_bus]
; in [pci_dev]
; in [pci_fnc]
; in [regs_cnt]
;
;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

_read_byte_pci_type1:  ;  (int bus, int device, int func, int reg)

;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*


call _calc_pci_config_addr_type1


mov dx	,0x0CF8
out dx	,eax			;set our addr value eax

mov dx	,[regs_cnt]
and dx	,3
add dx	,0x0CFC
in  al	,dx	

ret

;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
;
; in [pci_bus]
; in [pci_dev]
; in [pci_fnc]
; in [regs_cnt]
;
; in [value] the dword value
;
;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

_write_dword_pci_type1:  ;  (int bus, int device, int func, int reg)

;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*


call _calc_pci_config_addr_type1

mov dx ,0x0CF8			    ; put eax  (the address) in addr register
out dx ,eax

mov eax,[value]

mov dx ,0x0CFC				;put the value in cmd register
out dx ,eax

ret



;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
;
; in [pci_bus]
; in [pci_dev]
; in [pci_fnc]
; in [regs_cnt]
;
; in [value] the byte value
;
;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

_write_word_pci_type1:  ;  (int bus, int device, int func, int reg)

;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

	call _calc_pci_config_addr_type1

	mov dx	,0x0CF8		;put eax  (the address) in addr register
	out dx	,eax

	mov ax ,[value]

	mov dx ,[regs_cnt]
	and dx ,2
	add dx ,0x0CFC
	out dx ,ax
ret

;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
;
; in [pci_bus]
; in [pci_dev]
; in [pci_fnc]
; in [regs_cnt]
;
; in [value] the byte value
;
;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

_write_byte_pci_type1:  ;  (int bus, int device, int func, int reg)

;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

	call _calc_pci_config_addr_type1

	mov dx	,0x0CF8		;put eax  (the address) in addr register
	out dx	,eax

	mov al ,[value]

	mov dx ,[regs_cnt]
	and dx ,3
	add dx ,0x0CFC
	out dx ,al

ret
