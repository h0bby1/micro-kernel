pci_cur_vendor_addr: dd 0
pci_cur_vendor_device_addr: dd 0
pci_str_vendor_addr: dd 0

pci_dev_not_found_str:db 'device not found ',0

pci_str_vendor_addr_dst: dd 0
pci_str_vendor_dst_len : dd 0

pci_str_device_addr_dst: dd 0
pci_str_device_dst_len : dd 0

n_pci_vendor_devices:dd 0
cnt_pci_vendor_devices:dd 0

pci_vendor_id: dw 0
pci_device_id: dw 0

;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
;
; in [pci_config]
; in edx : addresse index 0-6
; p 224 pci ref
;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
_get_pci_address_size_c:
;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

ret


;------------------------------------------------------------------------------------------------
; in ebx CAP_ID
;------------------------------------------------------------------------------------------------
_pci_find_capability_c:
;------------------------------------------------------------------------------------------------

ret


_pci_set_power_state_c:

	
ret

_pci_bios_enable_c:



ret


_pci_set_master_c:


ret

target_ptr:dd 0


;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
_pci_enable_device_c:		;(int bus, int device, int func)
;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
	
	mov eax					,[esp+4];
	mov dword [pci_bus]		,eax
	
	mov eax					,[esp+8];
	mov dword [pci_dev]		,eax
	
	mov eax					,[esp+12];
	mov dword [pci_fnc]		,eax
	
	pusha
	
		call _pci_enable_device
	
	popa

ret


;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
_read_pci_config_c:  ;(int bus, int device, int func,unsigned int *pci_config(16 int) ) 
;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
;ret unsigned int : 0 = success
;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
		
	mov eax					,[esp+4];
	mov dword [pci_bus]		,eax
	
	mov eax					,[esp+8];
	mov dword [pci_dev]		,eax
	
	mov eax					,[esp+12];
	mov dword [pci_fnc]		,eax
	
	mov eax					,[esp+16];
	mov dword[target_ptr]	,eax
	
	pusha
	
	call _read_pci_config
	
	cmp  eax,0xFFFFFFFF
	jne	  dev_found
		popa
		mov eax,0xFFFFFFFF
		ret
	dev_found:
		
	mov edi		,dword[target_ptr]
	
	cmp edi		,0x00000000
	je no_cpy_rd_pci_conf
	
	lea esi				,[pci_config]
	
	xor ecx,ecx
	cpy_rd_pci_conf:
	
		mov eax			,[esi]
		mov dword[edi]	,eax
		
		lea edi,[edi+4]
		lea esi,[esi+4]
	inc ecx
	cmp ecx,16
	jl cpy_rd_pci_conf
	
	no_cpy_rd_pci_conf:
	
	popa
	xor eax,eax
ret



;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
;ret unsigned int 
_read_pci_dword_c:  ;(int bus, int device, int func,in reg ) 

;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
;ret unsigned int : 0 = success
;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
		
	mov eax					,[esp+4];
	mov dword [pci_bus]		,eax
	
	mov eax					,[esp+8];
	mov dword [pci_dev]		,eax
	
	mov eax					,[esp+12];
	mov dword [pci_fnc]		,eax

	mov eax					,[esp+16];
	mov dword [regs_cnt]	,eax
	
	push ebx
	
	call _read_dword_pci
	
	
	pop ebx
	
ret


;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
;ret unsigned int 
_write_pci_dword_c:  ;(int bus, int device, int func, int reg , int value)

;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
;ret unsigned int : 0 = success
;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
		
	mov eax					,[esp+4];
	mov dword [pci_bus]		,eax
	
	mov eax					,[esp+8];
	mov dword [pci_dev]		,eax
	
	mov eax					,[esp+12];
	mov dword [pci_fnc]		,eax

	mov eax					,[esp+16];
	mov dword [regs_cnt]	,eax
	
	mov eax					,[esp+20];
	mov dword [value]		,eax

	push ebx
	
	call _write_dword_pci
		
	pop ebx
	
ret



;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
;ret unsigned short
_read_pci_word_c:  ;(int bus, int device, int func,in reg ) 
;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
		
	mov eax					,[esp+4];
	mov dword [pci_bus]		,eax
	
	mov eax					,[esp+8];
	mov dword [pci_dev]		,eax
	
	mov eax					,[esp+12];
	mov dword [pci_fnc]		,eax

	mov eax					,[esp+16];
	mov dword [regs_cnt]	,eax
	
	push ebx
	
	call _read_word_pci
		
	pop ebx
	
ret


;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
;ret unsigned int 
_write_pci_word_c:  ;(int bus, int device, int func, int reg , int value)

;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
;ret unsigned int : 0 = success
;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
		
	mov eax					,[esp+4];
	mov dword [pci_bus]		,eax
	
	mov eax					,[esp+8];
	mov dword [pci_dev]		,eax
	
	mov eax					,[esp+12];
	mov dword [pci_fnc]		,eax

	mov eax					,[esp+16];
	mov dword [regs_cnt]	,eax
	
	mov ax					,[esp+20];
	mov word [value]		,ax

	push ebx
	
	call _write_word_pci
		
	pop ebx
	
ret



;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
;ret unsigned short
_read_pci_byte_c:  ;(int bus, int device, int func,in reg ) 
;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
		
	mov eax					,[esp+4];
	mov dword [pci_bus]		,eax
	
	mov eax					,[esp+8];
	mov dword [pci_dev]		,eax
	
	mov eax					,[esp+12];
	mov dword [pci_fnc]		,eax

	mov eax					,[esp+16];
	mov dword [regs_cnt]	,eax
	
	push ebx
	
	call _read_byte_pci
		
	pop ebx
	
ret


;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
;ret unsigned int 
_write_pci_byte_c:  ;(int bus, int device, int func, int reg , int value)
;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
;ret unsigned int : 0 = success
;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
		
	mov eax					,[esp+4];
	mov dword [pci_bus]		,eax
	
	mov eax					,[esp+8];
	mov dword [pci_dev]		,eax
	
	mov eax					,[esp+12];
	mov dword [pci_fnc]		,eax

	mov eax					,[esp+16];
	mov dword [regs_cnt]	,eax
	
	mov ax					,[esp+20];
	mov word [value]		,ax

	push ebx
	
	call _write_byte_pci
		
	pop ebx
	
ret

;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
_write_pci_config_c:     ;(int bus, int device, int func)
;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*


ret

;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
_pci_detect_conf_type_c:
;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
;ret: pci_config_type
;ret: 0xFFFFFFFF = error
;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
	call _pci_detect_conf_type
ret

%if 0

;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
;in (unsigned short vendor_id, char *dst_string,unsigned int len)
;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
_pci_get_vendor_string_c:
;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
;ret: 1 = success
;ret: 0 = error
;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
	mov ax						,	[esp+4]	
	mov word [pci_vendor_id]	,   ax
	
	mov eax						  ,	[esp+6]
	mov [pci_str_vendor_addr_dst] , eax
	
	mov eax						  ,	[esp+10]
	mov [pci_str_vendor_dst_len]  , eax

	lea eax							,	[pci_vendors]
	mov dword [pci_cur_vendor_addr]	,	eax
	
	get_vendor_string_c_loop:
		
		lea eax		, [pci_vendors_end]
		cmp eax		, [pci_cur_vendor_addr]
		jge			get_vendor_string_c_end

		mov	 eax	,	[pci_cur_vendor_addr]
		mov  ax		,	word  [eax]
		cmp  ax		,	word  [pci_vendor_id]
		jne get_vendor_string_c_next
			mov eax						,[pci_cur_vendor_addr]
			lea eax						,[eax+4]
			mov [pci_str_vendor_addr]	,eax
			
			push dword [pci_str_vendor_addr]
			push dword [pci_str_vendor_dst_len]
			push dword [pci_str_vendor_addr_dst]
			call dword [lib_c_strcpy_addr]
			add esp,12
			
			mov  eax,1
			ret
			
		get_vendor_string_c_next:
		
		mov		eax						,[pci_cur_vendor_addr]
		movzx	eax						,word [eax+2]
		add [pci_cur_vendor_addr]		,eax

		
	jmp get_vendor_string_c_loop
	
	get_vendor_string_c_end:
	
	xor  eax,eax

ret



;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
;in (unsigned short vendor_id, unsigned short device_id,
;	char *dst_vendor_string		,unsigned int vendor_str_len, 
;	char *dst_device_string		,unsigned int device_str_len)
;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
_pci_get_vendor_device_string_c:
;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
;ret: 1 = success
;ret: 0 = error
;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
	mov ax							,	[esp+4]	
	mov word [pci_vendor_id]		,   ax
	
	mov ax							,	[esp+8]	
	mov word [pci_device_id]		,   ax
	
	mov eax							,	[esp+12]
	mov [pci_str_vendor_addr_dst]	,	 eax
	
	mov eax							,	[esp+16]
	mov [pci_str_vendor_dst_len]	,	eax

	mov eax							,	[esp+20]
	mov [pci_str_device_addr_dst]	,	eax
	
	mov eax							,	[esp+24]
	mov [pci_str_device_dst_len]	,	eax
	
	
	lea eax							,	[pci_vendors]
	mov dword [pci_cur_vendor_addr]	,	eax
	
		
	get_vendor_device_string_c_loop:
		
		cmp dword [pci_cur_vendor_addr]		, pci_vendors_end
		jge	get_vendor_device_string_c_end
		
		mov	 eax	,	[pci_cur_vendor_addr]
		mov  ax		,	word  [eax]
		cmp  ax		,	word  [pci_vendor_id]
		jne get_vendor_device_string_c_next
			mov eax							,[pci_cur_vendor_addr]
			lea eax							,[eax+4]
			mov [pci_str_vendor_addr]		,eax
			mov [pci_cur_vendor_device_addr],eax
			
			
			push dword [pci_str_vendor_addr]
			push dword [pci_str_vendor_dst_len]
			push dword [pci_str_vendor_addr_dst]
			call dword [lib_c_strcpy_addr]
			add esp,12
			
			;start of vendor name
						
			get_vendor_device_string_c_skip_str_loop:
					mov		eax		,[pci_cur_vendor_device_addr]		
					inc		dword	[pci_cur_vendor_device_addr]
					
					mov	al	, byte [eax]
					test al	, al
					jz get_vendor_device_string_c_skip_str_end
					
			jmp get_vendor_device_string_c_skip_str_loop
			get_vendor_device_string_c_skip_str_end:
			;end of vendor name
			
			;number of device
			mov	eax									,[pci_cur_vendor_device_addr]			
			mov eax									,[eax]
			mov dword [n_pci_vendor_devices]		,eax
			add dword [pci_cur_vendor_device_addr]	,4
			

			
			; start of device list
			mov dword [cnt_pci_vendor_devices]	, 0
			
			get_vendor_device_string_c_find_device_loop:
			
				mov eax,[cnt_pci_vendor_devices]
				cmp eax,[n_pci_vendor_devices]
				jge get_vendor_device_string_c_find_device_loop_end
								
					mov eax,[pci_cur_vendor_device_addr]
					mov ax,	[eax]
					cmp ax, [pci_device_id]
					jne get_vendor_device_string_c_find_device_loop_next
						
						mov eax,[pci_cur_vendor_device_addr]
						lea eax,[eax+2]
					
						
						push dword eax
						push dword [pci_str_device_dst_len]
						push dword [pci_str_device_addr_dst]
						call dword [lib_c_strcpy_addr]
						add esp,12
												
						mov  eax,1
						ret
			
					get_vendor_device_string_c_find_device_loop_next:

					add dword [pci_cur_vendor_device_addr], 32
					inc dword [cnt_pci_vendor_devices]
					
			jmp get_vendor_device_string_c_find_device_loop
			get_vendor_device_string_c_find_device_loop_end:
			
			
			push dword pci_dev_not_found_str
			push dword [pci_str_device_dst_len]
			push dword [pci_str_device_addr_dst]
			call dword [lib_c_strcpy_addr]
			add esp,12
												
			mov  eax,1
			ret
			
		get_vendor_device_string_c_next:
		
		mov		eax						,[pci_cur_vendor_addr]
		movzx	eax						,word [eax+2]
		test    eax						,eax
		jz get_vendor_device_string_c_end
		
		add [pci_cur_vendor_addr]		,eax

		
	jmp get_vendor_device_string_c_loop
	
	get_vendor_device_string_c_end:
	
	xor  eax,eax

ret



%endif
