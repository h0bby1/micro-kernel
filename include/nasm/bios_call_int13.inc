%define BIOS_13H_No_error										00h	
%define BIOS_13H_Invalid_command								01h	
%define BIOS_13H_Address_mark_not_found							02h	
%define BIOS_13H_Disk_write_protected							03h				;(floppy)
%define BIOS_13H_Request_sector_not_found						04h	
%define BIOS_13H_Reset_failed									05h				;(hard disk)	
%define BIOS_13H_Floppy_disk_removed_Disk_changeline			06h				;(floppy)
%define BIOS_13H_Bad_parameter_table							07h				;(hard disk)/Initialization failed	
%define BIOS_13H_DMA_overrun									08h				;(floppy)
%define BIOS_13H_DMA_crossed_64K_boundary						09h	
%define BIOS_13H_Bas_sector_flag								0Ah				; (hard disk)
%define BIOS_13H_Bad_track_flag 								0Bh				;(hard disk)	
%define BIOS_13H_Media_type_not_found							0Ch				;(floppy)
%define BIOS_13H_Invalid_number_of_sectors_on_format			0Dh				;(hard disk)
%define BIOS_13H_Control_data_address_mark_detected				0Eh				;(hard disk)
%define BIOS_13H_DMA_arbitration_level_out_of_range				0Fh				;(hard error - retry failed)
%define BIOS_13H_Uncorrectable_CRC_or_ECC_data_error			10h				;(hard error - retry failed)
%define BIOS_13H_ECC_corrected_data_error						11h				;(soft error - retried OK ) (hard disk)	
%define BIOS_13H_Controller_failure								20h	
%define BIOS_13H_Seek_failure									40h				;(failed to respond)
%define BIOS_13H_Disk_timout 									80h	
%define BIOS_13H_Drive_not_ready 								AAh				;(hard disk)
%define BIOS_13H_Undefined_error								BBh				;(hard disk)
%define BIOS_13H_Write_fault									CCh				;(hard disk)
%define BIOS_13H_Statur_register error							E0h				;(hard disk)
%define BIOS_13H_Sense_operation failed							FFh				;(hard disk)	


sec_num					:	db 0
cyl_num					:	db 0
hd_num					:	db 0 
drv_num					:	db 0 
sec_idx					:	dd 0


disk_infos_ptr			:	dd 0 
;has_lba				:	db 0
;disk_ncyls				:	db 0
;disk_nhds				:	db 0
;disk_nsecs				:	dw 0
;disk_hdsec				:	dw 0
;lba_infos				:	times 64 db 0





;offset range 	size 	description
;00h 	1 byte 	size of DAP = 16 = 10h
;01h 	1 byte 	unused, should be zero
;02h..03h 	2 bytes 	number of sectors to be read, (some Phoenix BIOSes are limited to a maximum of 127 sectors)
;04h..07h 	4 bytes 	segment:offset pointer to the memory buffer to which sectors will be transferred (note that x86 is little-endian: if declaring the segment and offset separately, the offset must be declared before the segment)
;08h..0Fh 	8 bytes 	absolute number of the start of the sectors to be read (1st sector of drive has number 0)





;Offset  Size    Description     (Table 00273)
;00h    WORD    (call) size of buffer
;(001Ah for v1.x, 001Eh for v2.x, 42h for v3.0)
;(ret) size of returned data
;02h    WORD    information flags (see #00274)
;04h    DWORD   number of physical cylinders on drive
;08h    DWORD   number of physical heads on drive
;0Ch    DWORD   number of physical sectors per track
;10h    QWORD   total number of sectors on drive
;18h    WORD    bytes per sector
;---v2.0+ ---
;1Ah    DWORD   -> EDD configuration parameters (see #00278)
;FFFFh:FFFFh if not available
;---v3.0 ---
;1Eh    WORD    signature BEDDh to indicate presence of Device Path info
;20h    BYTE    length of Device Path information, including signature and this
;byte (24h for v3.0)
;21h  3 BYTEs   reserved (0)
;24h  4 BYTEs   ASCIZ name of host bus ("ISA" or "PCI")
;28h  8 BYTEs   ASCIZ name of interface type
;"ATA"
;"ATAPI"
;"SCSI"
;"USB"
;"1394" IEEE 1394 (FireWire)
;"FIBRE" Fibre Channel
;30h  8 BYTEs   Interface Path (see #00275)
;38h  8 BYTEs   Device Path (see #00276)
;40h    BYTE    reserved (0)
;41h    BYTE    checksum of bytes 1Eh-40h (two's complement of sum, which makes
;the 8-bit sum of bytes 1Eh-41h equal 00h)

;struct bios_drive_t
;{
;	unsigned char   bios_id;
;	unsigned char   is_lba;
;	unsigned short  sector_size;
;	unsigned short  cylinders;
;	unsigned char	sectors;
;	unsigned char	heads;
;	unsigned word	hdXsecs;
;};


_bios_reset_drives_c:

ret



_read_bios_disk_geom_c:
	
    mov eax				,[esp+4]					;drive number
    mov [drv_num]		,al
    
    mov eax				,[esp+8]					;target addr
    mov [disk_infos_ptr],eax

	
	pusha
	mov  ebp,esp
	
	cli

	mov eax					,	0x0100				;setup the stack for 16 bit use (usable with 16 bit stack pointer)
	mov ss					,	eax
	mov esp					,	0x1000
	
	mov eax					,	0x00				;reset 32 bit register to zero
	mov edx					,	0x00
	mov ebx					,	0x00
	mov ecx					,	0x00
	mov esi					,	0x00
	mov edi					,	0x00
	
		
	
    xor ax		,ax									;call interupt 13h ah=0 (reset drives)
    mov dl		,[drv_num]
    int 13h
    jc bios_disk_geom_error
	
		        
	;-----------------------------------------
	;test for lba device
	;-----------------------------------------
	mov ah,41h									;call interupt 13h ah=41 (lba extension present)
	mov dl,[drv_num]
	mov bx,0x55AA
	int 13h
	jc no_ext_lba
	cmp bx,0xAA55
	jne no_ext_lba
	test cx,1
	jz no_ext_lba
		
		;-----------------------------------------
		;get lba sector size
		;-----------------------------------------
		
			
		
		mov ah			,	0x48									;call interupt 13h ah=48 (get extended infos)
		mov dl			,	[drv_num]
		lea esi			,	[shared_data real_mode_to_addr]			;setup buffer ofset from start of the kernel
		
		mov	ebx			,	0x1000									;setup segment selector to start of the kernel
		mov es			,	ebx
		mov ds			,	ebx
		xor ebx			,	ebx
		
		mov word [esi]	,	0x1A									;set the size of the output buffer (1A = extension informations version 1)
		
		int 13h														;call the interupt
		jc no_ext_lba
		
		mov	ax					,0x10								;set segment selector to default (starting at address 0x000)
		mov ds					,ax
		mov es					,ax		
		
		mov edi					,[disk_infos_ptr]					;set pointer to buffer where the infos need to be copied
		lea esi					,[shared_data]						;set pointer to the information returned from interupt
		
		mov al					,[drv_num]
		mov byte [edi+0]		,al	
		mov byte [edi+1]		,1
		
		mov ax					,[esi+18h]
		mov word [edi+2]		,ax					;sector size
		
		mov eax					,[esi+04h]			;cylinders
		mov dword [edi+4]		,eax
		
		mov eax					,[esi+0Ch]
		mov dword [edi+8]		,eax				;sectors
		
		mov eax					,[esi+08h]
		mov dword [edi+12]		,eax				;heads
		
		
		cmp dword [edi+4],0
		jle no_heads
			mov	ax, [edi+8]
			mul word [edi+12]
			mov word [edi+16]		,ax					;heads x sectors
			mov word [edi+18]		,dx					;heads x sectors
		no_heads:
		
		
		
		
	jmp short bios_disk_geom_done
	no_ext_lba:
		;-----------------------------------------
		;chs device
		;-----------------------------------------
			
		
		;-----------------------------------------
		;get chs disk geometry
		;-----------------------------------------			
		;xor ax,ax
		;mov es,ax
		;xor di,di
		
		mov ah,0x08
		mov dl,[drv_num]
		;call call_real_mode_interupt_vector_c
		int 13h
		jc	bios_disk_geom_error
		
		test cl,cl
		jz bios_disk_geom_error
		
		;-----------------------------------------
		;store result
		;-----------------------------------------		
		
		mov edi					,[disk_infos_ptr]
		
		mov al					,[drv_num]
		mov byte [edi+0]		,al
		mov byte [edi+1]		,0
		mov word [edi+2]		,512
				
		mov al,cl	
		
		mov byte [edi+4]		,ch					;cylinders
		shr cl					,6
		mov byte [edi+5]		,cl					
		inc word [edi+4]
		mov word [edi+6]		,0
					
		inc dh
		mov dword[edi+12]		,0
		mov byte [edi+12]		,dh					;heads
				
		and al					,0x3F
		mov dword[edi+8]		,0
		mov byte [edi+8]		,al					;sectors
	
		movzx ax,dh
		mul	word [edi+8]	
		mov [edi+16],ax
		mov [edi+18],dx
		
	bios_disk_geom_done:
	
	
		mov ax					,	0x18
		mov ss					,	ax
		mov esp					,	ebp
		
		mov	ax					,	0x10
		mov es					,	ax
		mov ds					,	ax
		
		
			
		popa
		mov eax,1
	ret
	bios_disk_geom_error:
	
		mov ax					,	0x18
		mov ss					,	ax
		mov esp					,	ebp

		mov	ax					,	0x10
		mov es					,	ax
		mov ds					,	ax		
	
		popa
		xor eax,eax
	ret


_bios_get_sector_data_ptr_c:
	lea eax,[shared_data]
ret





_bios_read_sector_c:

   mov eax				,[esp+4]		;in infos
   mov [disk_infos_ptr]	,eax
   
   mov eax				,[esp+8]		;sector number     
   mov [sec_idx]		,eax
   
   pusha
   mov ebp,esp
   
   mov ax					,	0x0100
   mov ss					,	ax
   mov esp					,	0x1000
   
   xor edi					,	edi
   xor edx					,	edx
   
   
   
  
   mov edi		,[disk_infos_ptr]
   mov dl		,[edi+0]
   
   test byte [edi+1],1
   jz no_lba_ext
				
		xor edi					,edi	
		lea ebx					,[shared_data real_mode_to_addr]
		
		mov ecx					,[sec_idx]		


   		
		lea esi					,[dap real_mode_to_addr]
		
		mov	ax					,0x1000
		mov ds					,ax
		mov es					,ax		
		
		mov byte [esi+0]	,0x18
		mov byte [esi+1]	,0x00
		mov word [esi+2]	,1
		mov word [esi+4]	,bx
		mov word [esi+6]	,0x1000
		mov dword [esi+8]	,ecx
		mov dword [esi+12]	,0
		mov dword [esi+16]	,0
		mov dword [esi+20]	,0
		
		xor eax					,eax
		
		mov ah					,0x42			;read sector
		int 13h
		
		

		mov	ax					,0x10
		mov ds					,ax		
		
		
		
   
   jmp bios_read_sector_lba_test   
   no_lba_ext:
            
		mov eax				,[sec_idx]
		xor	dx				,dx

		div word [edi+16]
		mov [cyl_num]		,	ax			;cylinder

		mov ax				,	dx	
		div byte	[edi+8]
		mov [hd_num]		,	al			;head
		
		inc ah
		mov [sec_num]		,	ah			;sector
		
		
		
		;- the parameters in CX change depending on the number of cylinders;
		;  the track/cylinder number is a 10 bit value taken from the 2 high
		;  order bits of CL and the 8 bits in CH (low order 8 bits of track):

		;  |F|E|D|C|B|A|9|8|7|6|5-0|  CX
		;   | | | | | | | | | |	`-----	sector number
		;   | | | | | | | | `---------  high order 2 bits of track/cylinder
		;   `------------------------  low order 8 bits of track/cyl number		
		
		
		lea bx,[shared_data real_mode_to_addr]
		mov ah,2				;read sector
		mov ch,[cyl_num]		;cyl number
		mov dh,[hd_num]			;head number
		mov cl,[sec_num]		;sector number
		mov dl,[edi+0]			;drive number
		mov al,1				;read 1 sector
		int 13h
		;call call_real_mode_interupt_vector_c
		
		
    
    bios_read_sector_lba_test:
    
   jnc bios_read_sector_lba_no_error
		mov dword [ret_val],0
		jmp bios_read_sector_lba_end
	   bios_read_sector_lba_no_error:
		mov dword [ret_val],1
	 bios_read_sector_lba_end:
 
    
   mov ax					,	0x18
   mov ss					,	ax
   mov esp					,	ebp
   
   mov ax					,	0x10
   mov es					,	ax
   mov ds					,	ax
   popa
   
   mov eax,[ret_val]
   

ret

