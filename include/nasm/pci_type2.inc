

;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
;
; in [pci_bus]
; in [pci_dev]
; in [pci_fnc]
; in [regs_cnt]
;
; out ecx : addr
;
;
;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

_calc_pci_config_addr_type2:  ;  (int bus, int device, int func, int reg)

;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

;PCI_CONF2_ADDRESS(dev, reg)	(u16)(0xC000 | (dev << 8) | reg)

mov cx,0xC000

mov dx,[pci_dev]
shl dx,8

or  cx,dx
or  cx,[regs_cnt]	;address value in ecx

ret


;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
;
; in [pci_bus]
; in [pci_dev]
; in [pci_fnc]
; in [regs_cnt]
;
;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

_read_dword_pci_type2:  ;  (int bus, int device, int func, int reg)

;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

call _calc_pci_config_addr_type2

mov al,[pci_fnc]
shl al,1
or  al,0xF0

mov dx,0xCF8
out dx,al

mov al,[pci_bus]
mov dx,0xCFA
out dx,al


mov dx,cx
in  eax,dx

mov ecx,eax

xor al,al
mov dx,0xCF8
out dx,al

mov eax,ecx

ret

;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
;
; in [pci_bus]
; in [pci_dev]
; in [pci_fnc]
; in [regs_cnt]
;
;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

_read_word_pci_type2:  ;  (int bus, int device, int func, int reg)

;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

call _calc_pci_config_addr_type2

mov al,[pci_fnc]
shl al,1
or  al,0xF0

mov dx,0xCF8
out dx,al

mov al,[pci_bus]
mov dx,0xCFA
out dx,al

mov dx,cx
in  ax,dx				;eax word config data

mov cx,ax

xor al,al
mov dx,0xCF8
out dx,al

mov ax,cx

ret

;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
;
; in [pci_bus]
; in [pci_dev]
; in [pci_fnc]
; in [regs_cnt]
;
;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

_read_byte_pci_type2:  ;  (int bus, int device, int func, int reg)

;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*


call _calc_pci_config_addr_type2

mov al,[pci_fnc]
shl al,1
or  al,0xF0

mov dx,0xCF8
out dx,al

mov al,[pci_bus]
mov dx,0xCFA
out dx,al

mov dx,cx
in  al,dx				;eax word config data

mov bl,al

xor al,al
mov dx,0xCF8
out dx,al

mov al,bl

ret

;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
;
; in [pci_bus]
; in [pci_dev]
; in [pci_fnc]
; in [regs_cnt]
;
; in [value] the dword value
;
;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

_write_dword_pci_type2:  ;  (int bus, int device, int func, int reg)

;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*


call _calc_pci_config_addr_type2

mov al,[pci_fnc]
shl al,1
or  al,0xF0

mov dx,0xCF8
out dx,al

mov al,[pci_bus]
mov dx,0xCFA
out dx,al

mov eax,[value]
mov dx,cx
out dx,eax				;eax word config data

xor al,al
mov dx,0xCF8
out dx,al

ret
;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
;
; in [pci_bus]
; in [pci_dev]
; in [pci_fnc]
; in [regs_cnt]
;
; in [value] the byte value
;
;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

_write_word_pci_type2:  ;  (int bus, int device, int func, int reg)

;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

call _calc_pci_config_addr_type2

mov al,[pci_fnc]
shl al,1
or  al,0xF0

mov dx,0xCF8
out dx,al

mov al,[pci_bus]

mov dx,0xCFA
out dx,al

mov ax,[value]

mov dx,cx
out dx,ax				;eax word config data

xor al,al
mov dx,0xCF8
out dx,al

ret

;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
;
; in [pci_bus]
; in [pci_dev]
; in [pci_fnc]
; in [regs_cnt]
;
; in [value] the byte value
;
;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

_write_byte_pci_type2:  ;  (int bus, int device, int func, int reg)

;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

call _calc_pci_config_addr_type2

mov al,[pci_fnc]
shl al,1
or  al,0xF0

mov dx,0xCF8
out dx,al

mov al,[pci_bus]

mov dx,0xCFA
out dx,al

mov al,[value]

mov dx,cx
out dx,al				;eax word config data

xor al,al
mov dx,0xCF8
out dx,al

ret
