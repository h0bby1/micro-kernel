%define BIOS_MODE 0
vesa_mode_idx	:	dw 0x00

_bios_get_vesa_info_ptr_c:
	lea eax,[shared_data]
ret

_bios_get_vesa_info_c:


%if BIOS_MODE == 1
   pushfd
   cli
   pusha 
   push es
   push ds

   push ebp
   mov  ebp,esp
      
   mov ax	,	0x00100
   mov ss	,	ax
   mov esp	,	0x1FFF
  
   mov ax	,0x1000
   mov ds	,ax
   mov es	,ax	  
  
   xor eax,eax
   xor ebx,ebx
   xor ecx,ecx
   xor edx,edx
   xor esi,esi
   xor edi,edi

   mov ah	,0x4F			;Super VGA support
   mov al	,0				;Return Super VGA information
   lea edi	,[shared_data real_mode_to_addr]
   int 10h

   mov [ret_ax real_mode_to_addr],ax
   
   mov ax	,	0x18
   mov ss	,	ax
   
   mov esp ,	ebp
   pop ebp
         
   pop ds
   pop es
   
   popad
   popfd
   
   cmp word [ret_ax],0x004F
   je get_vga_info_no_error
	  mov eax,0
      jmp  bios_get_vga_info_end
   get_vga_info_no_error:
	  mov eax,1
    bios_get_vga_info_end:
%else
	pusha
    push  dword 0x10
    call  load_real_mode_interupt_vector_c  
    add   esp,4
 
     
	 mov ah	,0x4F			;Super VGA support
 	 mov al	,0				;Return Super VGA information
     lea di	,[shared_data real_mode_to_addr]
             
     call call_real_mode_interupt_vector_c
     
     mov ax,[ret_ax]
	 cmp ax,0x004F
     je get_vga_info_no_error
	   mov dword [ret_val],0
      jmp  bios_get_vga_info_end
   get_vga_info_no_error:
	  mov dword [ret_val],1
    bios_get_vga_info_end:

   popa
   sti
   mov eax,[ret_val]
%endif

ret


_bios_get_vesa_mode_info_ptr_c:
	lea eax,[shared_data]
ret



_bios_get_vesa_mode_info_c:

%if BIOS_MODE == 1
   pushfd
   cli
   pusha 
   push es
   push ds

   push ebp
   mov  ebp,esp
   
   mov	ax				,[ebp+52]
   mov [vesa_mode_idx]	,ax   
      
   mov ax	,	0x00100
   mov ss	,	ax
   mov esp	,	0x1FFF
  
   mov ax	,0x1000
   mov ds	,ax
   mov es	,ax	   

   mov ah	,0x4F			;Super VGA support
   mov al	,0x01			;Return Super VGA mode information
   mov cx   ,[vesa_mode_idx real_mode_to_addr]
   lea di	,[shared_data real_mode_to_addr]
   int 10h

   mov [ret_ax real_mode_to_addr],ax
   
   mov ax	,	0x18
   mov ss	,	ax
   
   mov esp ,	ebp
   pop ebp
         
   pop ds
   pop es
   
   popad
   popfd

   cmp word [ret_ax],0x004F
   je get_vga_info_no_error_1
	  mov eax,0
      jmp  bios_get_vga_info_end_1
   get_vga_info_no_error_1:
	  mov eax,1
    bios_get_vga_info_end_1:

%else

	mov	ax				,[esp+4]
	mov [vesa_mode_idx]	,ax

	pusha
    push  dword 0x10
    call  load_real_mode_interupt_vector_c  
    add   esp,4
 
     
	 mov ah	,0x4F			;Super VGA support
 	 mov al	,0x01			;Return Super VGA mode information
 	 mov cx ,[vesa_mode_idx]
     lea di	,[shared_data real_mode_to_addr]
             
     call call_real_mode_interupt_vector_c
     
     mov ax,[ret_ax]
	 cmp ax,0x004F
     je get_vga_mode_info_no_error
	   mov dword [ret_val],0
      jmp  bios_get_mode_vga_info_end
   get_vga_mode_info_no_error:
	  mov dword [ret_val],1
    bios_get_mode_vga_info_end:

	   popa
	   sti
	   mov eax,[ret_val]
%endif   
ret


_bios_set_vesa_mode_c:

%if BIOS_MODE == 1
   pushfd
   cli
   pusha 
   push es
   push ds

   push ebp
   mov  ebp,esp
   
   mov	eax				,[ebp+52]
   mov [vesa_mode_idx]	,eax   
      
   mov ax	,	0x0100
   mov ss	,	ax
   mov esp	,	0x1FFF
  
   mov ax	,0x1000
   mov ds	,ax
   mov es	,ax	   

   mov ah	,0x4F			;Super VGA support
   mov al	,0x02			;Set Super VGA video mode
   mov bx  ,[vesa_mode_idx real_mode_to_addr]
   and	bx	,~0x8000
   int 10h

   mov [ret_ax real_mode_to_addr],ax
   
   mov ax	,	0x18
   mov ss	,	ax
   
   mov esp ,	ebp
   pop ebp
         
   pop ds
   pop es
   
   popad
   popfd

   cmp word [ret_ax],0x004F
   je get_vesa_info_no_error_2
	  mov eax,0
      jmp  bios_get_vesa_info_end_2
   get_vesa_info_no_error_2:
	  mov eax,1
    bios_get_vesa_info_end_2:

%else
	
	mov	ax				,[esp+4]
	mov [vesa_mode_idx]	,ax
	
pusha
    push  dword 0x10
    call  load_real_mode_interupt_vector_c  
    add   esp,4	

	mov ah	,0x4F			;Super VGA support
 	mov al	,0x02			;Set Super VGA video mode
 	mov bx  ,[vesa_mode_idx]
 	and	bx	,~0x8000
	call	call_real_mode_interupt_vector_c
	
	mov ax,[ret_ax]
	 cmp ax,0x004F
     je set_vga_mode_no_error
	   mov dword [ret_val],0
      jmp  bios_set_mode_vga_end
   set_vga_mode_no_error:
	  mov dword [ret_val],1
    bios_set_mode_vga_end:

   popa
   sti
   mov eax,[ret_val]
%endif

ret






_bios_get_vga_info_c:

	pusha
    push  dword 0x10
    call  load_real_mode_interupt_vector_c  
    add   esp,4
 
     
	 mov ah	,0x1B			;VGA Dynamic State Table
 	 mov al	,0				;VGA Dynamic State Table
     lea di	,[shared_data real_mode_to_addr]
             
     call call_real_mode_interupt_vector_c
     mov dword [ret_val],1
     
	 popa
	 sti
	 mov eax,[ret_val]
ret


_bios_set_vga_mode_c:
	
	
%if BIOS_MODE == 1
   pushfd
   cli
   pusha 
   push es
   push ds

   push ebp
   mov  ebp,esp
   
  
   mov	ax				,[ebp+52]
   mov [vesa_mode_idx]	,ax   
      
   mov ax	,	0x00100
   mov ss	,	ax
   mov esp	,	0x1FFF
  
   mov ax	,0x1000
   mov ds	,ax
   mov es	,ax	   

    mov	ax	,[vesa_mode_idx real_mode_to_addr]
	int 10h

   mov [ret_ax real_mode_to_addr],ax
   
   mov ax	,	0x18
   mov ss	,	ax
   
   mov esp ,	ebp
   pop ebp
         
   pop ds
   pop es
   
   popad
   popfd

   cmp word [ret_ax],0x004F
   je get_vga_info_no_error_2
	  mov eax,0
      jmp  bios_get_vga_info_end_2
   get_vga_info_no_error_2:
	  mov eax,1
    bios_get_vga_info_end_2:

%else
	
	mov	ax				,[esp+4]
	mov [vesa_mode_idx]	,ax
	
	pusha
    push  dword 0x10
    call  load_real_mode_interupt_vector_c  
    add   esp,4	

	
 	mov ax	,[vesa_mode_idx]

	call	call_real_mode_interupt_vector_c
	
    mov dword [ret_val],1
  
    popa
    sti
   mov eax,[ret_val]
%endif
	
ret

