counter							: dd 0
math_tmp						: dd 0
crc_res							: dd 0
len_crc_str						: dd 0
addr_crc_str					: dd 0
last_addr_mapped				: dq 0

_code_stub_space	 			: times 1024*2 db 0
_code_stub_space_end 			: dd 0xFFFFFFFF	
_code_stub_space_cur 			: dd 0

_code_stub_space_nex 			: dd 0
_code_stub_ret		  			: dd 0
_code_stub_dat		  			: dd 0
_code_stub_fnc		  			: dd 0


kernel_map_start				: dd 0x01000000
ret_val_d						: dd 0

%define PORTB			0x061
%define REFRESH_STATUS	0x010		; Refresh signal status

delay1_4ms:
    mov     dx, 16			; close enough.
	
	in	al,PORTB
	and	al,REFRESH_STATUS
	mov	ah,al			; Start toggle state
	or	dx, dx
	jz	end
	inc	dx			; Throwaway first toggle
b:	
	in	al,PORTB			; Read system control port
	and	al,REFRESH_STATUS	; Refresh toggles 15.085 microseconds
	cmp	ah,al
	je	b					; Wait for state change

	mov	ah,al				; Update with new state
	dec	dx
	jnz	b

end:
ret


;--------------------------------
; in : ecx ms to delay
;--------------------------------
_delay_x_ms:
	
	loop_delay_ms:
	
		call delay1_4ms
		dec dword [counter]
		cmp dword [counter],0
	jg loop_delay_ms

ret


%define LDT_FLAG	0x2		; ldt
%define TASK_FLAG	0x5		; task gate
%define TSS_FLAG	0x9		; TSS
%define CALL_FLAG	0xC		; call gate
%define INT_FLAG	0xE		; interrupt gate
%define TRAP_FLAG	0xF		; trap gate

%define PRESENT_F	0x80	; present flag
%define	DPL0	0x00
%define	DPL1	0x20
%define	DPL2	0x40
%define	DPL3	0x60




;--------------------------------
; in : ecx ms to delay
;--------------------------------
_delay1_4ms_c:
	call delay1_4ms
ret

_delay_x_ms_c:
	
	mov		eax			,[esp+4]
	push	edx
	
	cmp		eax,0
	jle		end_delay_x_ms_c
	
		mov DWORD [counter]	,eax
		shr DWORD [counter]	,2

		call	_delay_x_ms
	
	end_delay_x_ms_c:
	
	pop	edx
	xor eax,eax
ret


time_val:dq 0
time_ret:dd 0

_get_system_time_c:
	
	push ebp
	mov  ebp,esp
	pusha
	
	mov	edi	,	[ebp+8]

	xor eax,eax
	CPUID
	RDTSC					;edx:eax = timer counter

	sub eax					, [start_cpu_tick]					; Calculate TSC
	sbb edx					, [start_cpu_tick+4]	
	
	mov ebx					, [cpu_frequency]
	div ebx
	
	mov [edi]				, eax

	popa
	mov esp,ebp
	pop ebp

ret




_out_8_c:
	mov dx	,[esp+4]
	mov al	,[esp+8]
	out dx	,al
ret

_out_16_c:
	mov dx	,[esp+4]
	mov ax	,[esp+8]
	out dx	,ax
ret

_out_32_c:
	mov dx	,[esp+4]
	mov eax	,[esp+8]
	out dx	,eax
ret







_rep_in_byte:
   pushad
   
   
   mov   edi ,[esp+32+ 8]
   mov   ecx ,[esp+32+ 12]
   mov   edx ,[esp+32+ 4]

   cld

   rep   insb

   popad
ret

_rep_in_word:
    
   pushad
   
   mov   edi ,[esp+32+ 8]
   mov   ecx ,[esp+32+ 12]
   mov   edx ,[esp+32+ 4]

   cld

   rep   insw

   popad
ret

_rep_in_dword:
    
   pushad
   
   
   mov   edi ,[esp+32+ 8]
   mov   ecx ,[esp+32+ 12]
   mov   edx ,[esp+32+ 4]

   cld

   rep   insd

   popad
ret


_rep_out_byte:
  
   pushad
   
   
   mov   esi ,[esp+32+ 8]
   mov   ecx ,[esp+32+ 12]
   mov   edx ,[esp+32+ 4]

   cld

   rep   outsb

   popad
ret


_rep_out_word:
    
   pushad
   
   
   mov   esi ,[esp+32+ 8]
   mov   ecx ,[esp+32+ 12]
   mov   edx ,[esp+32+ 4]

   cld

   rep   outsw

   popad
ret

_rep_out_dword:
    
   pushad
   
   
   mov   esi ,[esp+32+ 8]
   mov   ecx ,[esp+32+ 12]
   mov   edx ,[esp+32+ 4]

   cld

   rep   outsd

   popad
ret

_in_8_c:
	mov dx	,[esp+4]
	in  al	,dx
ret


_in_16_c:
	mov dx	,[esp+4]
	in  ax	,dx
ret


_in_32_c:
	mov dx	,[esp+4]
	in	eax ,dx
ret


%if 0
_sinf:
	fld DWORD [esp+4]
	fsin
ret

_cosf:
	fld DWORD [esp+4]
	fcos
ret


_isinf:
	fld		DWORD [esp+4]
	fsin
	fmul	DWORD [esp+8]	
	fistp	dword [math_tmp]
	mov     eax,[math_tmp]
ret

_icosf:
	fld		DWORD [esp+4]
	fcos
	fmul	DWORD [esp+8]		
	fistp	dword [math_tmp]
	mov     eax,[math_tmp]
ret
%endif

_kernel_memory_map_c:

	mov eax							,	[kernel_map_start]
	add eax							,	[last_addr_mapped]
	mov [ret_val_d]					,	eax

	mov eax							,	[esp+4]
	add [last_addr_mapped]			,	eax
	
	
	mov eax							,	[ret_val_d]
	
ret


write_bits_64_mmx:

ret

_code_stub:
 
	mov  eax,0xDEADB00F
	push eax
	mov  eax,0xDEADBEEF
	call eax
	
	xor eax,eax
ret
_end_code_stub					: 
_end_code_stub_pad				: times 8 db 0xAA

_create_code_stub:

	mov eax					,[esp+4]
	mov [_code_stub_dat]	,eax

	mov eax					,[esp+8]
	mov [_code_stub_fnc]	,eax
	
	pusha
	
	lea edi	, [_code_stub_space]
	add edi	, [_code_stub_space_cur]
	
	lea esi	, [_code_stub]
	
	mov ecx	,	_end_code_stub
	sub ecx		,_code_stub

	mov [_code_stub_ret]		,edi
	mov [_code_stub_space_nex]	,edi
	
	add [_code_stub_space_cur]	,ecx
	add [_code_stub_space_nex]	,ecx
		
	loop_create_code_stub:
		
		cmp dword[esi],0xDEADBEEF
		jne not_fnc
			
			mov eax		,	[_code_stub_fnc]
			stosd
			
			lea esi		,	[esi+4]
				
			jmp next_loop
		not_fnc:
		
		cmp dword[esi],0xDEADB00F
		jne not_data
			
			mov eax	,	[_code_stub_dat]
			stosd
			
			lea esi	,	[esi+4]
				
			jmp next_loop
		not_data:		
		
		movsb
		
		next_loop:
		
	cmp edi,[_code_stub_space_nex]
	jle loop_create_code_stub

	popa
	
	mov eax,[_code_stub_ret]
ret


wait_key_pressed:
pushfd
cli
	wait_key_buffer_1:
		in	 al,0x64
		test al,1
	jz  wait_key_buffer_1
	
	
	in  al	,  0x60	
	mov bl  ,  al
	and bl  ,  0x80
	
	__wait_key_press:
	
		wait_key_buffer_2:
			in	 al,0x64
			test al,1
		jz  wait_key_buffer_2
		
		in  al	,  0x60	
		and al  ,  0x80
	cmp bl,al
	je __wait_key_press
	
	
	wait_key_buffer_3:
		in	 al,0x64
		test al,1
	jz  wait_key_buffer_3
			
	;mov dword [counter],10000
	;call _delay_x_ms
	popfd
ret

_dump_memory:
pusha
	mov dword [counter],0
	loop_dump_memory:
	
		push ecx
		push esi
			mov eax	,[counter]
			mov cl	,[esi+eax]
			
			call _draw_byte_hex	
			
			mov dl,' '
			call _draw_car
			
			add dword [counter],1
		pop esi			
		pop ecx
	
	cmp dword [counter],ecx
	jle loop_dump_memory
popa
ret

;   Push(EAX)
;   Push(ECX)
;   Push(EDX)
;   Push(EBX)
;   Push(ESP)
;   Push(EBP)
;   Push(ESI)
;   Push(EDI)
dump_reg_stats:

	push ds
	
	push eax
		mov eax,0x10
		mov ds,eax
	pop eax

	mov dword [gen_regs+00],edi
	mov dword [gen_regs+04],esi
	mov dword [gen_regs+08],ebp
	mov dword [gen_regs+12],esp
	mov dword [gen_regs+16],ebx
	mov dword [gen_regs+20],edx
	mov dword [gen_regs+24],ecx
	mov dword [gen_regs+28],eax
	
	push eax
		mov word [seg_regs+00] ,es
		mov ax				   ,[esp+4]
		mov word [seg_regs+02] ,ax
		mov word [seg_regs+04] ,gs
		mov word [seg_regs+06] ,ss
		mov word [seg_regs+08] ,cs
		mov word [seg_regs+10] ,fs
	pop eax

	
	pushfd
	pop dword [cpu_flags]
	
	pusha
	pushfd
	
	mov eax		,12
	mul dword	[char_screen_width]
	add eax		,0
	shl eax		,1
	mov edx     ,eax
		
	mov ecx,6
	lea esi,[seg_regs]
	dump_reg_stats_seg:
		
		mov byte [value+0]		, ' '
		mov byte [value+1]		, 0x07
		_put_car
		
		mov eax					,	[esi]
		mov [hex_value]			,	eax
		call _put_word_hex
		
		add esi					,	2
	loopnz dump_reg_stats_seg	 
			
	mov eax		,13
	mul dword	[char_screen_width]
	add eax		,0
	shl eax		,1
	mov edx     ,eax

	mov ecx,8
	lea esi,[gen_regs+32]
	dump_reg_stats_gen:
		sub esi					,	4
		
		mov byte [value+0]		, ' '
		mov byte [value+1]		, 0x07
		_put_car
		
		mov eax					,	[esi]
		mov [hex_value]			,	eax
		call _put_dword_hex
		
		
	loopnz dump_reg_stats_gen
	
	mov eax					,	14
	mul dword				[char_screen_width]
	add eax					,	0
	shl eax					,	1
	mov edx     			,	eax
	
	mov eax					,	[cpu_flags]
	mov [hex_value]			,	eax
	call _put_dword_hex	
	
	call wait_key_pressed
		
	popfd
	popa
	pop ds
	
	end_dump_reg:
	


ret


_swap_nz_array_c:
	pushfd
	cli 
	push esi
	mov  esi,[esp+12]
	
	xor eax,eax
	loop_swap_zero_array:
	cmp esi,[esp+16]
	jge end_swap_zero_array
		cmp [esi]	,eax
		jne end_loop_zero_array
		lea  esi	,[esi+4]
	jmp loop_swap_zero_array
	
	end_loop_zero_array:
	
	XCHG [esi]	,eax
	mov  esi	,[esp+20]
	mov  [esi]	,eax
	
	end_swap_zero_array:
	pop esi
	popfd
ret


_compare_z_exchange_c:

	push edi
	push ebx
	
	mov  edi,	[esp+12]
	mov  ebx,	[esp+16]
	
	sfence
	
	;Compare EAX with r/m32. If equal, ZF is set and r32 is	 loaded into r/m32. Else, clear ZF and load r/m32 into AL
	
	xor	eax				,	eax
	lock CMPXCHG [edi]	,	ebx
	jnz _compare_z_exchange_c_not_changed
		mov eax,1
		jmp _compare_z_exchange_c_done
	_compare_z_exchange_c_not_changed:
		xor eax,eax
	
	_compare_z_exchange_c_done:
	pop ebx
	pop edi
ret


_fetch_add_c:
	push edi

	mov  edi,	[esp+8]
	mov  eax,	[esp+12]
	
	lock xadd [edi]	, eax

	pop edi
re