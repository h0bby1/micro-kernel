writestr_str:dd 0
original_itr_flgs:dd 0
itr_cli:dd 0

_draw_car_c:

	mov	eax	,	[esp+4]
	

	pusha
	mov		esi,eax
	call	_draw_cars
	popa
		
	xor eax,eax
ret


_draw_ncar_c:

	push ebp
	mov  ebp,esp

	pusha
	mov		esi,[ebp+8]
	mov		ecx,[ebp+12]
	call   _draw_n_cars
	popa
	
	mov esp,ebp
	pop ebp
	
	xor eax,eax
ret


_draw_dword_c:

	mov	eax	,	[esp+4]
	

	pusha
	mov	  ecx,eax
	call _draw_dword_hex
	popa
	

ret

_draw_byte_c:

	mov	eax	,	[esp+4]
	

	pusha
	mov	  ecx,eax
	call _draw_byte_hex
	popa
	

ret

_set_text_color_c:

	mov al			,[esp+4]
	mov [text_color],al

ret


_get_text_color_c:

	mov al			,[text_color]
ret

_get_char_buffer_ofset_c:
	mov eax,[char_buffer_offset]
ret

_set_char_buffer_ofset_c:
	mov eax,[esp+4]

	pushfd	
	cli	
	pusha
	
    mov [char_buffer_offset],eax
    
    call _output_buffer
    
    popa
    popfd
ret
