
%define ARCH_INTERRUPT_BASE		0x20
%define PIC_INT_BASE			ARCH_INTERRUPT_BASE
%define PIC_SLAVE_INT_BASE		(ARCH_INTERRUPT_BASE + 8)
%define PIC_NUM_INTS			0x0f

%define PIC_MASTER_CONTROL		0x20
%define PIC_SLAVE_CONTROL		0xa0

%define PIC_NON_SPECIFIC_EOI	0x20

%define PIC_INIT1				0x10
%define PIC_INIT1_SEND_INIT4	0x01
%define PIC_INIT3_IR2_IS_SLAVE	0x04
%define PIC_INIT3_SLAVE_ID2		0x02
%define PIC_INIT4_x86_MODE		0x01


%define PICU1					PIC_MASTER_CONTROL
%define PICU2					PIC_SLAVE_CONTROL

%define PIC_MASTER_MASK			0x21
%define PIC_SLAVE_MASK			0xa1

%define PIC_MASTER_INIT1		PIC_MASTER_CONTROL
%define PIC_MASTER_INIT2		PIC_MASTER_MASK
%define PIC_MASTER_INIT3		PIC_MASTER_MASK
%define PIC_MASTER_INIT4		PIC_MASTER_MASK
%define PIC_SLAVE_INIT1			PIC_SLAVE_CONTROL
%define PIC_SLAVE_INIT2			PIC_SLAVE_MASK
%define PIC_SLAVE_INIT3			PIC_SLAVE_MASK
%define PIC_SLAVE_INIT4			PIC_SLAVE_MASK



;--------------------------
pic_end_of_interrupt:

	cmp eax,PIC_INT_BASE
	jl end_eoi

	cmp eax,(PIC_INT_BASE+PIC_NUM_INTS)
	jg end_eoi

	;PIC 8259 controlled interrupt
	cmp eax,PIC_SLAVE_INT_BASE
	jl no_slave_eoi
		mov al					,PIC_NON_SPECIFIC_EOI
		out PIC_SLAVE_CONTROL	,al
	no_slave_eoi:
	
	;we always need to acknowledge the master PIC
	mov al					,PIC_NON_SPECIFIC_EOI
	out PIC_MASTER_CONTROL	,al

	end_eoi:
ret


_init_pic:

;   Start initialization sequence for the master and slave PICs

	cli
	
    MOV AL	,(PIC_INIT1 | PIC_INIT1_SEND_INIT4)
    mov dx	,PIC_MASTER_CONTROL
    out dx	,AL          	;ICW1 - MASTER
		
    MOV AL	,(PIC_INIT1 | PIC_INIT1_SEND_INIT4)
    mov dx	,PIC_SLAVE_CONTROL
    out dx	,AL          	;ICW1 - SLAVE


	; Set start of interrupts to 0x20 for master, 0x28 for slave
    MOV AL	,PIC_INT_BASE
    mov dx	,PIC_MASTER_INIT2
    out dx	,AL          	;ICW1 - MASTER
	
    MOV AL	,PIC_SLAVE_INT_BASE
    mov dx	,PIC_SLAVE_INIT2
    out dx	,AL          	;ICW1 - MASTER
    

	; Specify cascading through interrupt 2

    MOV AL	,PIC_INIT3_IR2_IS_SLAVE
    mov dx	,PIC_MASTER_INIT3
    out dx	,AL          	;ICW1 - MASTER
    
    MOV AL	,PIC_INIT3_SLAVE_ID2
    mov dx	,PIC_SLAVE_INIT3
    out dx	,AL          	;ICW1 - MASTER

	; Set both to operate in 8086 mode
    MOV AL	,PIC_INIT4_x86_MODE
    mov dx	,PIC_MASTER_INIT4
    out dx	,AL          	;ICW1 - MASTER

    MOV AL	,PIC_INIT4_x86_MODE
    mov dx	,PIC_SLAVE_INIT4
    out dx	,AL          	;ICW1 - SLAVE
    

    ;set interupt mask

	MOV AL	,0x00
	mov dx	,PIC_MASTER_MASK
	out dx	,AL          	;ICW1 - MASTER

	MOV AL	,0x00
	mov dx	,PIC_SLAVE_MASK
	out dx	,AL          	;ICW1 - SLAVE
	
	call  delay1_4ms
	;sti	
ret

_init_pic_real_mode:

	mov al,0x11 ; Initialisation command
	out 0x20,al
	
	mov al,0x08 ; Beginning of the remapping 0x8-0xF
	;(interrupt 8-15)
	out 0x21,al
	mov al,0x04
	out 0x21,al
	mov al,0x01
	out 0x21,al

	; We initialise the secondary PIC (Secondary 8259)
	mov al,0x11	  ; Initialisation command
	out 0xA0,al
	
	mov al,0x70		; Beginning of the remapping 0x70-0x77
	;(interrupt 112-119)
	out 0xA1,al
	mov al,0x02
	out 0xA1,al
	mov al,0x01
	out 0xA1,al

	; We unmask all interrupts on the primary and secondary PIC
	mov al	,0x0
	out 0x21,al
	mov al	,0x0
	out 0xA1,al
	
	call  delay1_4ms

ret

set_pic_interupt_mask:
	cli
	MOV AL	,0x00
	mov dx	,PIC_MASTER_MASK
	out dx	,AL          	;ICW1 - MASTER

	MOV AL	,0x00
	mov dx	,PIC_SLAVE_MASK
	out dx	,AL          	;ICW1 - SLAVE
	sti
ret


reset_pic_interupt_mask:
	MOV AL	,0xFF
	mov dx	,PIC_MASTER_MASK
	out dx	,AL          	;ICW1 - MASTER

	MOV AL	,0xFF
	mov dx	,PIC_SLAVE_MASK
	out dx	,AL          	;ICW1 - SLAVE
ret
_set_cpu_flags_c:
	
	push dword[esp+4]
	popfd
ret

_pic_enable_irq_c:
	pushfd
	pop		eax
		
	cmp dword [esp+4],1
	je enable_irq
		cli
	jmp end_enable_irq
	enable_irq:
		sti
	end_enable_irq:

		
ret

_pic_set_irq_mask_c:
	mov al				,[esp+4]
	out PIC_MASTER_MASK	,AL          	;ICW1 - MASTER
	mov al				,[esp+5]
	out PIC_SLAVE_MASK	,AL          	;ICW1 - SLAVE
ret

_pic_init_c:
	push edx
	call _init_pic
	pop edx
ret