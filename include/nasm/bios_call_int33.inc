


_bios_reset_mouse_c:
	pusha
    push  dword 0x33
    call  load_real_mode_interupt_vector_c  
    add   esp,4
      
	 mov AX,0x00 ;- increments the cursor flag;  the cursor is displayed if flag is zero;  default flag value is -1
     call call_real_mode_interupt_vector_c
    
     movzx eax				,word [ret_ax] 
	 mov	[ret_val]		,eax
     popa
     sti
     mov eax,[ret_val]
ret

_bios_enable_mouse_c:

	pusha
    push  dword 0x33
    call  load_real_mode_interupt_vector_c  
    add   esp,4
      
	 mov AX,0x01 ;- increments the cursor flag;  the cursor is displayed if flag is zero;  default flag value is -1
     call call_real_mode_interupt_vector_c
     
     popa
     sti
     mov ax,[ret_ax]
     
ret

mouse_status:times 8 dw 0

_bios_get_mouse_status_c:
	
	 
	 pusha
     push  dword 0x33
     call  load_real_mode_interupt_vector_c  
     add   esp,4
      
	  mov AX,0x03 ;- increments the cursor flag;  the cursor is displayed if flag is zero;  default flag value is -1
      call call_real_mode_interupt_vector_c
      
	  ;Return: BX = button status (see #03168)
	  ;CX = column
	  ;DX = row
	  ;Note:	in text modes, all coordinates are specified as multiples of the cell
	  
      mov word [mouse_status+0],BX 
      mov word [mouse_status+2],CX  
      mov word [mouse_status+4],DX  
     
     popa
     sti
     mov eax,mouse_status
ret