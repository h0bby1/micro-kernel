hex_value	: dd 0
hex_value_t	: dd 0


addr_code_ret: ret

%if 0
;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
; in dl car to draw
;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
_put_car:	
;=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
   mov ax				,[value]
   mov [0xB8000+edx]	,ax
   add edx	 , 2
   
   _put_car_text_end:
ret
%endif

%macro _put_car 0
   mov ax				,[value]
   mov [0xB8000+edx]	,ax
   add edx				, 2
%endmacro

_put_str:	
	
	loop_str:
		mov al				,	[esi]
		cmp al				,   0
		je end_put_str
		
		mov [0xB8000+edx]	,ax
		add edx				, 2		
		
		inc esi
	jmp loop_str
		

	end_put_str:
ret

_put_byte_hex:	
	
   cmp dword [output_buffer_active],1
   jne addr_code_ret
   	
	;1rd hex char

    xor ebx,ebx
    mov bl,[hex_value]
    and bl,0xF0
    shr bl,4
    add ebx,hex_chars

	mov bl			  ,[ebx]
    mov byte [value]  ,bl
    _put_car

 ;2nd hex char

    xor ebx,ebx
    mov bl,[hex_value]
    and bl,0x0F
    add ebx,hex_chars

	mov bl			  ,[ebx]
    mov byte [value]  ,bl
    _put_car
ret

_put_byte_bin:	

   cmp dword [output_buffer_active],1
   jne addr_code_ret
	
    xor ebx,ebx
    mov bl,[hex_value]
    
    mov al,1
    shl al,cl
    shr al,1
    
    mov byte [bin_mask],al
        	
	put_byte_bin_loop:
	
		test bl,byte [bin_mask]
		jz put_byte_bin_not_one
			mov byte [value+0],'1'
			_put_car
		jmp put_byte_bin_next_loop
		put_byte_bin_not_one:
			mov byte [value+0],'0'
			_put_car		
		put_byte_bin_next_loop:
		
		shr byte [bin_mask],1
	loopnz put_byte_bin_loop
	
ret

_put_word_hex:	
   cmp dword [output_buffer_active],1
   jne addr_code_ret

	mov eax				,[hex_value]
	mov [hex_value_t]	,eax
	

	mov eax				,[hex_value_t]
	shr eax				,8
	and eax				,0x000000FF
	mov [hex_value]		,al
	call _put_byte_hex

	mov eax				,[hex_value_t]
	and eax				,0x000000FF
	mov [hex_value]		,al
	call _put_byte_hex
ret

_put_word_hex_be:	
   cmp dword [output_buffer_active],1
   jne addr_code_ret

	mov eax				,[hex_value]
	mov [hex_value_t]	,eax
	
	mov eax				,[hex_value_t]
	and eax				,0x000000FF
	mov [hex_value]		,al
	call _put_byte_hex
	
	mov eax				,[hex_value_t]
	shr eax				,8
	and eax				,0x000000FF
	mov [hex_value]		,al
	call _put_byte_hex
ret

_put_dword_hex:	

   cmp dword [output_buffer_active],1
   jne addr_code_ret


	mov eax				,[hex_value]
	mov [hex_value_t]	,eax
	
	mov eax				,[hex_value_t]
	shr eax				,24
	and eax				,0x000000FF
	mov [hex_value]		,al
	call _put_byte_hex
	
	mov eax				,[hex_value_t]
	shr eax				,16
	and eax				,0x000000FF
	mov [hex_value]		,al
	call _put_byte_hex


	mov eax				,[hex_value_t]
	shr eax				,8
	and eax				,0x000000FF
	mov [hex_value]		,al
	call _put_byte_hex

	mov eax				,[hex_value_t]
	and eax				,0x000000FF
	mov [hex_value]		,al
	call _put_byte_hex

ret

_put_dword_hex_be:	

   cmp dword [output_buffer_active],1
   jne addr_code_ret


	mov eax				,[hex_value]
	mov [hex_value_t]	,eax
	
	mov eax				,[hex_value_t]
	and eax				,0x000000FF
	mov [hex_value]		,al
	call _put_byte_hex
	
	mov eax				,[hex_value_t]
	shr eax				,8
	and eax				,0x000000FF
	mov [hex_value]		,al
	call _put_byte_hex
	
	mov eax				,[hex_value_t]
	shr eax				,16
	and eax				,0x000000FF
	mov [hex_value]		,al
	call _put_byte_hex
	
			
	mov eax				,[hex_value_t]
	shr eax				,24
	and eax				,0x000000FF
	mov [hex_value]		,al
	call _put_byte_hex
ret
