%define ARCH_INTERRUPT_BASE		0x20
%define PIC_INT_BASE			ARCH_INTERRUPT_BASE
%define PIC_SLAVE_INT_BASE		(ARCH_INTERRUPT_BASE + 8)




user_int_func	:	times 2*512 dd 0



interupt_start_callback_func:dd 0xFFFFFFFF
interupt_end_callback_func	:dd 0xFFFFFFFF

;-------------------------------------------
;					AX 		CX 		DX 		BX 		SP		BP 		SI 		DI
;-------------------------------------------
;(In binary) REG = 	000 	001 	010 	011 	100 	101 	110 	111
;-------------------------------------------
;   Push(EAX)
;   Push(ECX)
;   Push(EDX)
;   Push(EBX)
;   Push(ESP)
;   Push(EBP)
;   Push(ESI)
;   Push(EDI)






;--------------------------
_wait_key_buffer:

	in	 al,0x64
	
	test byte al,1
	jnz  no_key_buffer
		mov	 eax,1
	    jmp end_ke_buffer
	no_key_buffer:
		xor	 eax,eax

end_ke_buffer:		
ret

set_user_ihdl:
	push edi
	lea edi		,[user_int_func+eax*8]
	mov [edi]	,edx
	mov [edi+4]	,ebx
	pop edi
ret



;--------------------------
%macro interupt_hndl 1


text_kern_itr_fnc_%1				 :db 'interupt_handler ',%1+48,0

interupt_%1:

	cli

	pushad
	push ds
	push es	
	
	mov			ax,0x10
	mov			es,ax
	mov			ds,ax
	
	cmp dword [protect_mmx_regs],0
	je irq_hndl_no_mmx_%1
		call save_mmx_regs
		emms
	irq_hndl_no_mmx_%1:		
	
	%if %1 == 0x00
		
			lea edi		,[task_data]
			lea esi		,[esp]
			mov ecx		,13
			rep movsd	
			
			add dword [task_data+20],12
				
			mov dword [task_data_ptr],0 
			cmp dword [task_manager_next_task_func]	,0
			je ret_itr_switch

				push dword task_switched
				push dword task_data
				call [task_manager_next_task_func]
				mov [task_data_ptr],eax
				
			ret_itr_switch:
	%endif

	
%if 1
	
	mov eax		,%1
	and eax		,0x0F
	mul dword	[char_screen_width]
	add eax		,66
	shl eax		,1
	mov edx     ,eax


	mov byte [value+0]		, 'I'
	mov byte [value+1]		, 0x07
	_put_car

	mov byte [value+0]		, ' '
	mov byte [value+1]		, 0x07
	_put_car

	mov byte [hex_value]	 , %1+ARCH_INTERRUPT_BASE
	call _put_byte_hex

	mov byte [value+0]		 ,' '
	mov byte [value+1]		 , 0x07
	_put_car

	lea edi					 , [num_int_called+%1*4]
	inc						   dword [edi]
	
	mov al					 , [edi+1]
	mov byte [hex_value]	 , al
	call _put_byte_hex		
	
	lea edi					 , [num_int_called+%1*4]
	mov al					 , [edi]
	mov byte [hex_value]	 , al
	call _put_byte_hex
%endif

		
	lea esi ,[user_int_func+%1*8]
	mov ebx ,[esi]
	test ebx,ebx
	jz no_user_defined_idl_%1
		push dword [esi+4]
		call ebx
		add esp,4
	no_user_defined_idl_%1:

	cmp dword [interupt_mode],1
	jne int_mode_0_%1
	
		call	apic_end_of_interrupt
		jmp		int_mode_done_%1
	int_mode_0_%1:
	
		mov eax	,	%1+PIC_INT_BASE
		call	pic_end_of_interrupt

	int_mode_done_%1:

	cmp dword [protect_mmx_regs],0
	je irq_hndl_no_mmx2_%1
		call restore_mmx_regs
	irq_hndl_no_mmx2_%1:
		
	%if %1 == 0x00
			
			cmp dword [task_data_ptr],0
			je ret_itr_switch_2
			
			mov esi				,	[task_data_ptr]
			
			sub dword [esi+20]	,	12
			mov esp				,	[esi+20]
			sub esp				,	40

			mov edi				,	esp
			mov ecx				,	10
			rep movsd		
			
					
			mov esi				,	[task_data_ptr]
			
			mov eax				,	[esi+40]
			mov [edi+0]			,	eax
			
			mov eax				,	[esi+44]
			mov [edi+4]			,	eax

			mov eax				,	[esi+48]
			mov [edi+8]			,	eax
			
			
			test dword [task_switched],	0x02
			jz ret_itr_switch_2
			
				
			mov eax				,	[esi+52]
			mov [edi+12]		,	eax

			mov eax				,	[esi+56]
			mov [edi+16]		,	eax

			ret_itr_switch_2:

	%endif

	
	pop es	
	pop ds
	
	popad
		
iret

%endmacro


;--------------------------
%macro system_hndl 1

syshandle_%1:

	cli
	pusha

	mov eax		,66
	shl eax		,1
	mov edx     ,eax

	mov byte [value+0],'S'
	mov byte [value+1], 0x07
	_put_car

	mov byte [value+0],' '
	mov byte [value+1], 0x07
	_put_car

	mov byte [hex_value] ,%1
	add byte [hex_value] ,0x20
	call _put_byte_hex

	mov byte [value+0],' '
	mov byte [value+1], 0x07
	_put_car

	
	mov byte [value+0],'e'
	mov byte [value+1], 0x07
	_put_car
	
	popa
	sti

iret

%endmacro



;--------------------------
%macro gatehandle 1

gatehandle_%1:

	cli
	pusha

	mov eax				, [char_screen_width]
	add eax				, 66
	shl eax				, 1
	mov edx				, eax

	mov byte [value+0]	, 'G'
	mov byte [value+1]	, 0x07
	_put_car

	mov byte [value+0]	, ' '
	mov byte [value+1]	, 0x07
	_put_car

	mov byte [hex_value] ,%1
	add byte [hex_value] ,0x20
	call _put_byte_hex

	mov byte [value+0]   ,' '
	mov byte [value+1]   , 0x07
	_put_car

	mov byte [value+0]	,'e'
	mov byte [value+1]	, 0x07
	_put_car
	
	popa	
	sti

iret

%endmacro


_setup_interupt_c:
  
  push ebp
  mov  ebp,esp
  
  pusha
  
  mov  eax,[ebp+8]
  mov  edx,[ebp+12]
  mov  ebx,[ebp+16]
  call set_user_ihdl
  
  popa
  
  pop ebp
ret

irq_hdl_start:
interupt_hndl 0x00
interupt_hndl 0x01
interupt_hndl 0x02
interupt_hndl 0x03
interupt_hndl 0x04
interupt_hndl 0x05
interupt_hndl 0x06
interupt_hndl 0x07
interupt_hndl 0x08
interupt_hndl 0x09
interupt_hndl 0x0A
interupt_hndl 0x0B
interupt_hndl 0x0C
interupt_hndl 0x0D
interupt_hndl 0x0E
interupt_hndl 0x0F

interupt_hndl 0x10
interupt_hndl 0x11
interupt_hndl 0x12
interupt_hndl 0x13
interupt_hndl 0x14
interupt_hndl 0x15
interupt_hndl 0x16
interupt_hndl 0x17
interupt_hndl 0x18
interupt_hndl 0x19
interupt_hndl 0x1A
interupt_hndl 0x1B
interupt_hndl 0x1C
interupt_hndl 0x1D
interupt_hndl 0x1E
interupt_hndl 0x1F

irq_hdl_end:

system_hndl	  0x63

gatehandle    251
gatehandle    252
gatehandle    253
gatehandle    254
gatehandle    255


%define FLAG_PRESENT  0x8000
%define FLAG_INT_CALL 0x0E00

%macro idt_init_real_int 2

lea eax   			,[real_mode_interupt_%1]
mov [%2+0]  	    ,ax

mov word [%2+2]		,0x0008
mov word [%2+4]		,(FLAG_PRESENT|FLAG_INT_CALL)
shr eax   			,16
mov word [%2+6]		,ax

%endmacro

%macro idt_init_int 2

lea eax   			,[interupt_%1]
mov [%2+0]  	    ,ax

mov word [%2+2]		,0x0008
mov word [%2+4]		,(FLAG_PRESENT|FLAG_INT_CALL)
shr eax   			,16
mov word [%2+6]		,ax

%endmacro

%macro ts_init_int 2

lea eax   			,[task_switch_handler]
mov [%2+0]  	    ,ax

mov word [%2+2]		,0x0008
mov word [%2+4]		,(FLAG_PRESENT|FLAG_INT_CALL)
shr eax   			,16
mov word [%2+6]		,ax

%endmacro



%macro idt_init_sys 2

	lea eax   			,[syshandle_%1]
	mov [%2+0]  	    ,ax

	mov word [%2+2]		,0x0008
	mov word [%2+4]		,(FLAG_PRESENT|FLAG_INT_CALL)
	shr eax   			,16
	mov word [%2+6]		,ax

%endmacro


%macro idt_init_gate 2

	lea eax   			,[gatehandle_%1]
	mov [%2+0]  	    ,ax

	mov word [%2+2]		,0x0008
	mov word [%2+4]		,(FLAG_PRESENT|FLAG_INT_CALL)
	shr eax   			,16
	mov word [%2+6]		,ax

%endmacro
