

task_data		:times 256 dd 0
task_switched	:dd 0
task_data_ptr	:dd 0


task_manager_next_task_func:dd 0
task_manager_dump_task_post_func:dd 0
task_manager_dump_tpo_mod_infos_func:dd 0

_dump_task_infos_c:

	cmp dword [task_manager_dump_task_post_func],0
	je no_task_dump_fnc

    push ebp
    mov  ebp,esp
	pushad
	push ds
	push es	

	mov			ax,0x10
	mov			es,ax
	mov			ds,ax
	
	
	mov eax,esp
	push dword eax
	call [task_manager_dump_task_post_func]
	
	pop es
	pop ds
	popa	
	
	mov esp,ebp
	pop ebp
	
	no_task_dump_fnc:

ret

_next_task_c:

	pushfd
	pop dword [task_data+48]
	
	
	cli
	pushad
	push ds
	push es	

	mov			ax,0x10
	mov			es,ax
	mov			ds,ax
	
	lea edi		,[task_data]
	lea esi		,[esp]
	mov ecx		,11
	rep movsd	
	
	add dword [task_data+20],4
	mov dword [task_data+44],0x08
	or	dword [task_data+48],0x200
		
	mov dword [task_data_ptr],0 
	cmp dword [task_manager_next_task_func]	,0
	je ret_next_task

	push dword task_switched
	push dword task_data
	call [task_manager_next_task_func]
	
	
	test eax,eax
	jz ret_next_task
		
	mov esi				,	eax
	
	sub dword [esi+20]	,	4
	mov esp				,	[esi+20]
	sub esp				,	40

	mov edi				,	esp
	mov ecx				,	10
	rep movsd		
				
	mov esi				,	eax
	mov eax				,	[esi+40]
	mov [edi+0]			,	eax

	test dword [task_switched],	0x02
	jz ret_next_task
	
		;mov eax			,	[esi+52]
		;mov [edi+4]		,	eax

		;mov eax			,	[esi+56]
		;mov [edi+8]		,	eax

	ret_next_task:
	

	
	
	
	push dword [task_data+48]
	popfd
	
	pop es
	pop ds
	popa
	
ret


