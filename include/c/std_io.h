#ifndef LIBC_API	
#define LIBC_API	C_IMPORT
#endif
#define SEEK_SET 0
#define SEEK_CUR 1
#define SEEK_END 2
typedef struct
{
	unsigned int id;
}FILE;

LIBC_API FILE *			C_API_FUNC fopen_c		(const char *file,const char * mode);
LIBC_API unsigned int	C_API_FUNC fread_c		(mem_ptr out,mem_size n_blk,mem_size blk,FILE *f);
LIBC_API unsigned int	C_API_FUNC fwrite_c		(mem_ptr out,mem_size n_blk,mem_size blk,FILE *f);
LIBC_API int			C_API_FUNC fclose_c		(FILE *f);
LIBC_API int			C_API_FUNC ftell_c		(FILE *f);
LIBC_API int			C_API_FUNC fseek_c		(FILE *f,mem_size pos,unsigned int mode);

