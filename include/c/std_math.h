#ifndef LIBC_API	
#define LIBC_API	DLL_IMPORT
#endif





#ifdef __cplusplus
	extern "C" {
#endif

LIBC_API unsigned int	C_API_FUNC iabs_c	(int v);
LIBC_API float			C_API_FUNC fabsf_c	(float f);
LIBC_API double			C_API_FUNC fabs_c	(double f);
LIBC_API double			C_API_FUNC sin_c	(double f);
LIBC_API float			C_API_FUNC powf_c	(float x,float y);
LIBC_API float			C_API_FUNC sinf_c	(float x);
LIBC_API unsigned long	C_API_FUNC isqrt	(unsigned long x);
LIBC_API double			C_API_FUNC floor_c	(double f);
LIBC_API double			C_API_FUNC ceil_c	(double f);
LIBC_API int			C_API_FUNC icosf	(float f,float m);
LIBC_API int			ASM_API_FUNC ftoi	(float f);
LIBC_API int			ASM_API_FUNC isinf  (float f, float m);




#ifdef __cplusplus
	}
#endif
