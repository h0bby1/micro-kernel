#ifndef __STD_DEF__

#define __STD_DEF__

#ifdef __GNUC__
	#define C_EXPORT
	#define C_IMPORT

	
	#if defined(_M_X64) || defined(__amd64__)

	#else
		#define C_INT_FUNC			__attribute__((__cdecl))
		#define KERN_API_FUNC		__attribute__((__stdcall))

		#define	MOD_NAME_DECO		GCC_STDCALL_32
	#endif

	#define struct_packed			__attribute__((packed))
	typedef long long				int64_t;
	typedef unsigned long long		uint64_t;
#endif

#ifdef _MSC_VER
	#define C_EXPORT		__declspec(dllexport)
	#define C_IMPORT		__declspec(dllimport)
	

	#if defined(_M_X64) || defined(__amd64__)
		
	#else
		#define C_API_FUNC			__stdcall
		#define ASM_API_FUNC		__cdecl
		#define C_INT_FUNC			__cdecl

		#define	MOD_NAME_DECO		MSVC_STDCALL_32
	#endif	

	#define struct_packed 
	typedef __int64					int64_t;
	typedef unsigned __int64		uint64_t;
#endif


#ifndef MOD_NAME_DECO
	#error compiler not supported !
#endif
	
#if defined(_M_X64) || defined(__amd64__)
	typedef int64_t			ptrdiff_t;
	typedef uint64_t		size_t;
	
	#define UINT_MAX	 0xffffffffUL
	#define ULONG_MAX	 0xffffffffUL
	#define CHAR_BIT	 8						/* number of bits in a char */
	#define USHRT_MAX    0xffff					/* maximum unsigned short value */
	#define SHRT_MIN    (-32768)				/* minimum (signed) short value */
	#define SHRT_MAX      32767					/* maximum (signed) short value */
	#define INT_MIN     (-2147483647 - 1)		/* minimum (signed) int value */
	#define INT_MAX       2147483647			/* maximum (signed) int value */
	#define offsetof(s,m)   (size_t)( (ptrdiff_t)&(((s *)0)->m) )

#else
	
	typedef unsigned int	size_t;

	typedef size_t			ptrdiff_t;
	#define offsetof(s,m)   (size_t)&(((s *)0)->m)


	#define INVALID_SIZE	0xffffffffUL

	#define LONG_MAX		0x7FFFFFFFL
	#define LONG_MIN		((long) 0x80000000L)

	#define UINT_MAX		0xffffffffUL
	#define ULONG_MAX		0xffffffffUL
	#define ULONG_MIN		0x00000000L
	#define CHAR_BIT		8						/* number of bits in a char */
	#define USHRT_MAX		0xffff					/* maximum unsigned short value */
	#define SHRT_MIN		(-32768)				/* minimum (signed) short value */
	#define SHRT_MAX		32767					/* maximum (signed) short value */
	#define INT_MIN			(-2147483647 - 1)		/* minimum (signed) int value */
	#define INT_MAX			2147483647			/* maximum (signed) int value */

	


	

#endif

#define OS_API_C_FUNC(return_type) C_EXPORT return_type C_API_FUNC 
#define OS_INT_C_FUNC(return_type) C_EXPORT return_type C_INT_FUNC 
#define OS_API_XTRN_ASM_FUNC(return_type) extern return_type ASM_API_FUNC 


typedef		unsigned int		C_INT_FUNC interupt_func(void *data);
typedef		interupt_func		*interupt_func_ptr	;


typedef enum
{
	NO_DECORATION	=	0,
	MSVC_STDCALL_32	=	1,
	MSVC_CDECL_32	=	2,
	GCC_STDCALL_32	=	3,
	GCC_CDECL_32	=	4
}mod_name_decoration_t;

typedef union
{
	struct {
		uint64_t	val;
	}uint64;
	struct {
		unsigned int ints[2];
	}uint32;
}large_uint_t;

#endif
