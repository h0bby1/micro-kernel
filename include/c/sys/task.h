#ifndef LIBC_API
#define LIBC_API C_IMPORT
#endif

LIBC_API void			C_API_FUNC init_task_manager						();

LIBC_API int			C_API_FUNC task_manager_get_first_task				(mem_zone_ref_ptr	task_list_out,mem_zone_ref_ptr *task);
LIBC_API mem_ptr		C_API_FUNC task_manager_next_task					(mem_ptr current_context_data, unsigned int *changed);

LIBC_API void			C_API_FUNC task_manager_pause						();

LIBC_API int			C_API_FUNC task_manager_get_current_user_data		(mem_zone_ref	*user_data);
LIBC_API int			C_API_FUNC task_manager_set_container_list			(mem_zone_ref_ptr list_zone);
LIBC_API int			C_API_FUNC task_manager_get_container_list			(mem_zone_ref_ptr list_zone);

LIBC_API unsigned int   C_API_FUNC task_manager_get_current_task_id			();
LIBC_API unsigned int	C_API_FUNC task_manager_get_current_task_name		(char *name,int len);

LIBC_API void			C_API_FUNC task_manager_set_task_active				(int task_id,int active);
LIBC_API void			C_API_FUNC task_manager_dump						();
//LIBC_API void			C_API_FUNC task_manager_set_current_task_preemptive	(int preemptive);
LIBC_API int			C_API_FUNC task_manager_new_task					(const char *task_name,mem_ptr code_addr,mem_zone_ref	*user_data, unsigned int param_stack);

LIBC_API mem_ptr		C_API_FUNC task_manager_new_semaphore		(unsigned int InitialUnits, unsigned int MaxUnits);
LIBC_API unsigned int	C_API_FUNC task_manager_aquire_semaphore	(mem_ptr handle, unsigned int time_out);
LIBC_API void			C_API_FUNC task_manager_release_semaphore	(mem_ptr handle, unsigned int Units);
LIBC_API int			C_API_FUNC task_manager_delete_semaphore	(mem_ptr handle);
LIBC_API void			C_API_FUNC task_manager_dump				();
LIBC_API void			C_API_FUNC task_manager_dump_task_post		(mem_ptr current_context_data);
LIBC_API void			C_API_FUNC task_manager_dump_tpo_mod_infos	(unsigned int addr, char *out_name, int out_len);


typedef void task_func();
typedef task_func *task_func_ptr;