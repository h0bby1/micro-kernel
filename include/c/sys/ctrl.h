#ifndef STREAM_API
#define STREAM_API C_IMPORT
#endif

typedef int C_API_FUNC ctrl_get_render_objs_func(mem_zone_ref_const_ptr ctrl_data_node);
typedef int C_API_FUNC ctrl_compute_func		(mem_zone_ref_const_ptr ctrl_data_node);
typedef int C_API_FUNC ctrl_event_func			(mem_zone_ref_ptr ctrl_data_node,mem_zone_ref_const_ptr event_node);
typedef int C_API_FUNC ctrl_drag_func			(mem_zone_ref_ptr ctrl_data_node,unsigned int obj_id,vec_2s_t drag_ofset);


typedef  ctrl_get_render_objs_func *ctrl_get_render_objs_func_ptr;
typedef  ctrl_compute_func			*ctrl_compute_func_ptr;
typedef  ctrl_event_func			*ctrl_event_func_ptr;
typedef  ctrl_drag_func				*ctrl_drag_func_ptr;


typedef struct
{
	tpo_mod_file					tpo_mod;
	

}ctrl_t;

typedef int ui_ctrl_init_func(mem_zone_ref_ptr p);
typedef ui_ctrl_init_func *ui_ctrl_init_func_ptr;

STREAM_API unsigned int  C_API_FUNC gfx_ctrl_init						();
STREAM_API unsigned int  C_API_FUNC gfx_ctrl_new_control				(const char *ctrl_name);
STREAM_API ctrl_t*		 C_API_FUNC gfx_ctrl_find_control				(const char *ctrl_name);
STREAM_API unsigned int  C_API_FUNC gfx_ctrl_compute					(const char *ctrl_name,mem_zone_ref_ptr ctrl_data_node);
STREAM_API unsigned int  C_API_FUNC gfx_ctrl_render						(const char *ctrl_name,mem_zone_ref_const_ptr ctrl_data_node);
STREAM_API int			 C_API_FUNC gfx_ctrl_event						(mem_zone_ref_ptr ctrl_data_node,mem_zone_ref_const_ptr event_node);
STREAM_API int			 C_API_FUNC gfx_ctrl_drag						(mem_zone_ref_ptr ctrl_data_node,unsigned int obj_id,vec_2s_t drag_ofset);
STREAM_API unsigned int	 C_API_FUNC gfx_ctrl_new						(struct obj_array_t *ctrl_data, const char *style);
STREAM_API unsigned int	 C_API_FUNC gfx_ctrl_create_object				(struct obj_array_t *ctrl_data, const char *class_name, const char *name, mem_zone_ref_ptr ctrl_data_node, unsigned int n_items);
STREAM_API int			 C_API_FUNC gfx_create_rect_style				(struct obj_array_t *rect_style_ar, const struct gfx_rect *rect, const vec_4uc_t color);
STREAM_API unsigned int	 C_API_FUNC gfx_ctrl_add_item_data				(mem_zone_ref_ptr ctrl_data_node, unsigned int item_id, mem_zone_ref_ptr item_data);
STREAM_API int			 C_API_FUNC gfx_create_set_ctrl_event			(mem_zone_ref_ptr ctrl_data_node, const char *type, const struct gfx_rect *rect, const char *prop, unsigned int prop_val);
