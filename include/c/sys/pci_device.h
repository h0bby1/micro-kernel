
typedef struct
{
	unsigned int irq_line;
	unsigned int irq_data;
}self_config_irq;

typedef struct
{
	unsigned int				dev_id;
	unsigned int				bus_id;
	unsigned int				device_type;
	unsigned int				device_sub_type;
	unsigned int				device_pi;
	char						driver_name[128];
	char						name[128];
	unsigned int				is_ndis;
	mem_zone_ref				driver_conf_node;
	tpo_mod_file				driver_mod;
	pci_dev_config				config;
	mem_zone_ref				pci_node;
	unsigned short				pci_code;
	self_config_irq				*sci;
	

}pci_device;

typedef unsigned int C_API_FUNC pci_get_cmd_flag_func			(unsigned short vendor_id ,unsigned short device_id);
typedef int			 C_API_FUNC pci_probe_device_func			(pci_device *pci_device);
typedef unsigned int C_API_FUNC pci_init_driver_func			();
typedef void		 C_API_FUNC pci_start_stream_func			(unsigned int stream_idx);
typedef int			 C_API_FUNC pci_get_device_format_func		(unsigned int idx,unsigned int *fmt_ptr);
typedef int			 C_API_FUNC pci_set_device_format_func		(unsigned int idx);
typedef int			 C_API_FUNC bus_drv_get_stream_infos_func	(unsigned int str_idx,mem_zone_ref *infos);
typedef int			 C_API_FUNC bus_drv_set_stream_infos_func	(unsigned int str_idx,large_uint_t sec_pos);
typedef int			 C_API_FUNC bus_drv_read_stream_func		(unsigned int str_idx,mem_zone_ref *req);

typedef unsigned int C_API_FUNC get_bus_device_irq_func			(unsigned int dev_id);



typedef pci_get_cmd_flag_func						*pci_get_cmd_flag_func_ptr;
typedef pci_probe_device_func						*pci_probe_device_func_ptr;
typedef pci_init_driver_func						*pci_init_driver_func_ptr;
typedef pci_start_stream_func						*pci_start_stream_func_ptr;
typedef pci_get_device_format_func	  				*pci_get_device_format_func_ptr;
typedef pci_set_device_format_func	  				*pci_set_device_format_func_ptr;
typedef bus_drv_get_stream_infos_func				*bus_drv_get_stream_infos_func_ptr;
typedef bus_drv_set_stream_infos_func				*bus_drv_set_stream_infos_func_ptr;
typedef bus_drv_read_stream_func					*bus_drv_read_stream_func_ptr;
typedef get_bus_device_irq_func						*get_bus_device_irq_func_ptr;





#define PCI_PARAM_NAMBAR	1
#define PCI_PARAM_NABMBAR   2
