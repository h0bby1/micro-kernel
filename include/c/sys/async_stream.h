#ifndef STREAM_API
	#define STREAM_API C_IMPORT
#endif

typedef struct
{
	unsigned int				stream_idx;
	size_t						total_data_read;
	size_t						current_ofset;
	mem_zone_ref				dma_buffer;
	


}async_stream;

STREAM_API	unsigned int C_API_FUNC async_stream_manager_init			();
STREAM_API	unsigned int C_API_FUNC async_stream_manager_new_stream		(mem_size buffer_size);
STREAM_API	mem_ptr		 C_API_FUNC async_stream_manager_get_dma_zone	(unsigned int stream_idx);
STREAM_API	int			 C_API_FUNC async_stream_manager_set_dma_pos	(unsigned int stream_idx,mem_size data_pos);
STREAM_API	int			 C_API_FUNC async_stream_manager_get_dma_ptr	(unsigned int stream_idx,mem_ptr *data_ptr, mem_size *total_read);
STREAM_API	int			 C_API_FUNC async_stream_manager_wrote_dma		(unsigned int stream_idx,mem_size size);
STREAM_API void			 C_API_FUNC add_timer_func						(timer_func_ptr func,void *param,unsigned long time);