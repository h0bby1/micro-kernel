#ifndef STREAM_API
	#define STREAM_API C_IMPORT
#endif

typedef struct
{
	unsigned int	id;
	mem_zone_ref	infos;
}dev_stream_t;


typedef struct
{
	unsigned int	stream_dev_id;
	unsigned int	bus_id;
	unsigned int	dev_id;
	mem_zone_ref	dev_streams;
	//unsigned int	streams[16];
	
}stream_device;	


STREAM_API unsigned int    C_API_FUNC new_stream_device					(unsigned int bus_id,unsigned int dev_id);
STREAM_API unsigned int    C_API_FUNC stream_dev_new_stream				(stream_device *str_dev,mem_size dma_size);
STREAM_API unsigned int    C_API_FUNC stream_dev_request_write_stream	(unsigned int str_dev_idx,unsigned int str_idx,mem_size size_read);
STREAM_API unsigned int    C_API_FUNC find_stream_device_bus			(unsigned int bus_id,unsigned int dev_id);
STREAM_API unsigned int    C_API_FUNC get_stream_device_stream_bus		(unsigned int bus_id,unsigned int dev_id,unsigned int idx);
STREAM_API stream_device * C_API_FUNC find_stream_device				(unsigned int stream_dev_id);

