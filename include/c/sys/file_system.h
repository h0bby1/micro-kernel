#ifndef STREAM_API
	#define STREAM_API C_IMPORT
#endif

typedef struct
{
	char					delim;
	mem_stream				image_stream;
	unsigned int			fs_type;
	mem_zone_ref			root;
	mem_zone_ref			base;
}file_system;


 
STREAM_API void				C_API_FUNC init_file_system				();
STREAM_API mem_zone_ref *	C_API_FUNC file_system_get_root_node	(const char *fs_name);
STREAM_API file_system *	C_API_FUNC file_system_get_at			(unsigned int idx);
STREAM_API void				C_API_FUNC file_system_dump				(const char *fs_name);
STREAM_API unsigned int		C_API_FUNC file_system_new				(const char *fs_name,unsigned int fs_type,mem_stream *img_str);
STREAM_API unsigned int		C_API_FUNC file_system_new_const		(const char *fs_name,unsigned int fs_type,const_mem_stream *img_str);
STREAM_API size_t			C_API_FUNC file_system_create_directory(const char *fs_name,const char *path);
STREAM_API int				C_API_FUNC file_system_create_file		(const char *fs_name,const char *path);
STREAM_API int				C_API_FUNC file_system_open_file		(const char *fs_name,const char *path,mem_stream *file_stream);
STREAM_API unsigned int		C_API_FUNC file_system_get_dir_entry	(const char *fs_name,const char *path,mem_zone_ref *file_out,unsigned int type,unsigned int index);
STREAM_API int				C_API_FUNC open_iso_image				(mem_stream *img_stream,const char *volume_name,unsigned int root_type);

extern		int				find_iso_entry				(file_system *system,mem_zone_ref *file_node,const char *name,mem_zone_ref *out_node);
extern		int				get_iso_entries				(mem_stream	*img_stream,mem_zone_ref * file_node,int read_files);
extern		int				get_iso_entry				(file_system *system,mem_zone_ref *file_node,unsigned int type,mem_zone_ref *out_node,unsigned int index);
extern		void			parse_iso					(file_system *files);
