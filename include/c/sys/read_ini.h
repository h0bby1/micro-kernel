#ifndef STREAM_API
	#define STREAM_API C_IMPORT
#endif

STREAM_API void				C_API_FUNC read_ini_file			(mem_zone_ref	*ini_datas,mem_zone_ref *tree_out,const char *ini_name);
STREAM_API int				C_API_FUNC read_ini_vendor_value	(mem_zone_ref	*manuf,unsigned int idx,unsigned short *vid,unsigned short *did,unsigned short *sub_sys_vendor,unsigned short *sub_sys,char *conf_sec_name,unsigned int len_name);
STREAM_API void				C_API_FUNC copy_without_space		(char *dest,unsigned int dest_len,const char *src,size_t		 start,size_t		 end);