#ifndef STREAM_API
	#define STREAM_API C_IMPORT
#endif

typedef struct
{
	mem_ptr			buffer_start;
	unsigned char	*buffer_ptr;
	mem_size		buffer_length;
	unsigned int	current_ofset_bits;
	unsigned int	cache_32;
}bitstream;


STREAM_API	void		C_API_FUNC bitstream_open		(bitstream *stream,mem_ptr start,mem_size size);
STREAM_API	void		C_API_FUNC bitstream_le_write	(bitstream *stream,unsigned int data,unsigned int bits);
STREAM_API	void		C_API_FUNC bitstream_le_flush	(bitstream *stream);
