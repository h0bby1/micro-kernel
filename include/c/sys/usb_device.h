typedef enum
{
    USB_SPEED_NONE,
	USB_SPEED_LOW,
    USB_SPEED_FULL,
    USB_SPEED_HIGH
} usb_speed_t;

typedef enum 
{
	usb_device_not_connected	= 0	,
	usb_device_reset			= 1	,
	usb_device_addressed		= 2 ,
	usb_device_configureed		= 3
}usb_device_state_t;



enum usb_transfer_direction
{
USB_TRANSFER_DIRECTION_HOST_TO_DEVICE=0,
USB_TRANSFER_DIRECTION_DEVICE_TO_HOST=1
};

enum usb_transfer_type
{
USB_TRANSFER_TYPE_STD		= 0,
USB_TRANSFER_TYPE_CLASS		= 1,
USB_TRANSFER_TYPE_VENDOR	= 2,
USB_TRANSFER_TYPE_RESERVED	= 3
};

enum usb_transfer_recipient
{

USB_TRANSFER_RECIPIENT_DEVICE	=	0,  
USB_TRANSFER_RECIPIENT_INTERFACE=	1,
USB_TRANSFER_RECIPIENT_ENDPOINT	=	2,
USB_TRANSFER_RECIPIENT_OTHER	=	3  
};

enum usb_transfer_request
{
USB_TRANSFER_REQ_GET_STATUS			=0x00,
USB_TRANSFER_REQ_CLEAR_FEATURE  	=0x01,
USB_TRANSFER_REQ_SET_FEATURE		=0x03,
USB_TRANSFER_REQ_SET_ADDRESS		=0x05,
USB_TRANSFER_REQ_GET_DESCRIPTOR 	=0x06,
USB_TRANSFER_REQ_SET_DESCRIPTOR 	=0x07,
USB_TRANSFER_REQ_GET_CONFIGURATION  =0x08,
USB_TRANSFER_REQ_SET_CONFIGURATION  =0x09
};

enum usb_hid_request
{
	USB_HID_REQ_GET_REPORT		=0x01 ,
	USB_HID_REQ_GET_IDLE		=0x02 ,
	USB_HID_REQ_GET_PROTOCOL	=0x03 ,
	USB_HID_REQ_SET_REPORT		=0x09 ,
	USB_HID_REQ_SET_IDLE		=0x0A ,
	USB_HID_REQ_SET_PROTOCOL	=0x0B 
};

enum usb_hid_report_type
{
	USB_HID_REPORT_INPUT	= 0x01 ,
	USB_HID_REPORT_OUTPUT	= 0x02 ,
	USB_HID_REPORT_FEATURE	= 0x03 
};
 
enum usb_transfer_descriptor_type
{
USB_TRANSFER_DESC_TYPE_DEVICE_DESCRIPTOR	= 0x01,
USB_TRANSFER_DESC_TYPE_CONFIG_DESCRIPTOR	= 0x02,
USB_TRANSFER_DESC_TYPE_STRING_DESCRIPTOR	= 0x03,
USB_TRANSFER_DESC_TYPE_IFACE_DESCRIPTOR		= 0x04,
USB_TRANSFER_DESC_TYPE_ENDPT_DESCRIPTOR		= 0x05,
USB_TRANSFER_DESC_TYPE_HUB_DESCRIPTOR		= 0x29
};



// Control and Status
#define TD_CONTROL_SPD				(1 << 29)
#define TD_CONTROL_3_ERRORS			(3 << 27)
#define TD_CONTROL_LOWSPEED			(1 << 26)
#define TD_CONTROL_ISOCHRONOUS		(1 << 25)
#define TD_CONTROL_IOC				(1 << 24)

#define TD_STATUS_ACTIVE			(1 << 23)
#define TD_STATUS_ERROR_STALLED		(1 << 22)
#define TD_STATUS_ERROR_BUFFER		(1 << 21)
#define TD_STATUS_ERROR_BABBLE		(1 << 20)
#define TD_STATUS_ERROR_NAK			(1 << 19)
#define TD_STATUS_ERROR_CRC			(1 << 18)
#define TD_STATUS_ERROR_TIMEOUT		(1 << 18)
#define TD_STATUS_ERROR_BITSTUFF	(1 << 17)

#define TD_STATUS_ACTLEN_MASK		0x07ff
#define TD_STATUS_ACTLEN_NULL		0x07ff

// Token
/*
#define TD_TOKEN_MAXLEN_SHIFT		21
#define TD_TOKEN_NULL_DATA			(0x07ff << TD_TOKEN_MAXLEN_SHIFT)
#define TD_TOKEN_DATA_TOGGLE_SHIFT	19
#define TD_TOKEN_DATA1				(1 << TD_TOKEN_DATA_TOGGLE_SHIFT)
*/
/*
#define TD_TOKEN_SETUP				0x0D
#define TD_TOKEN_IN					0x09
#define TD_TOKEN_OUT				0x01
*/

// ED
#define OHCI_ED_TD     0
#define OHCI_ED_OUT    1
#define OHCI_ED_IN     2

// TD
#define TD_TOKEN_SETUP  0
#define TD_TOKEN_OUT    1
#define TD_TOKEN_IN     2
//#define OHCI_TD_NOINT  7
//#define OHCI_TD_NOCC  15


#define TD_TOKEN_ENDPTADDR_SHIFT	15
#define TD_TOKEN_DEVADDR_SHIFT		8

#define TD_DEPTH_FIRST				0x04
#define TD_TERMINATE				0x01
#define TD_ERROR_MASK				0x440000
#define TD_ERROR_COUNT_SHIFT		27
#define TD_ERROR_COUNT_MASK			0x03
#define TD_LINK_MASK				0xfffffff0


#define USB_HUB_STATUS_CONNECTED	(1 << 0)
#define USB_HUB_STATUS_LOWSPEED		(1 << 9)
#define USB_HUB_STATUS_HIGHSPEED	(1 << 10)

struct usb_desc_header
{
unsigned char bLength;			//Number Size of this descriptor in bytes
unsigned char bDescriptorType;	//Constant ENDPOINT descriptor type (= 5)
};

/*
Configurations, Interfaces, and Endpoints.

The device contains a number of descriptors (as shown to the right) which help to define what the device is capable of. We will examine these descriptors further down the page. For the moment we need to have an idea what the configurations, interfaces and endpoints are and how they fit together.
A device can have more than one configuration, though only one at a time, and to change configuration the whole device would have to stop functioning. Different configurations might be used, for example, to specify different current requirements, as the current required is defined in the configuration descriptor.
However it is not common to have more than one configuration. Windows standard drivers will always select the first configuration so there is not a lot of point.
A device can have one or more interfaces. Each interface can have a number of endpoints and represents a functional unit belonging to a particular class.
Each endpoint is a source or sink of data.
For example a VOIP phone might have one audio class interface with 2 endpoints for transferring audio in each direction, plus a HID interface with a single IN interrupt endpoint, for a built in keypad.
It is also possible to have alternative versions of an interface, and this is more common than multiple configurations. In the VOIP phone example, the audio class interface might offer an alternative with a different audio rate. It is possible to switch an interface to an alternate while the device remains configured.
*/

struct usb_endpoint_desc
{
unsigned char bLength;			//Number Size of this descriptor in bytes
unsigned char bDescriptorType;	//Constant ENDPOINT descriptor type (= 5)
unsigned char bEndpointAddress;	//Endpoint The address of this endpoint within the device. 
/*
D7: Direction 0 = OUT, 1 = IN 
D6-D4: Set to 0 
D3-D0: Endpoint number
*/

unsigned char  bmAttributes;
/*Bitmap
D1:0 Transfer Type
00 = Control
01 = Isochronous
10 = Bulk
11 = Interrupt
The following only apply to isochronous endpoints. Else set to 0.
D3:2 Synchronisation Type
00 = No Synchronisation
01 = Asynchronous
10 = Adaptive
11 = Synchronous
D5:4 Usage Type
00 = Data endpoint
01 = Feedback endpoint
10 = Implicit feedback Data endpoint
11 = Reserved
D7:6 Reserved
Set to 0
*/
unsigned short wMaxPacketSize;	//Number Maximum packet size this endpoint can send or receive when this configuration is selected
unsigned char  bInterval;		// Number Interval for polling endpoint for data transfers. Expressed in frames (ms) for low/full speed or microframes (125us) for high speed.
};

/*
Interface Descriptor

The interface descriptor format is shown to the right.
bAlternateSetting needs some explanation. An interface can have more than one variant, and these variants can be switched between, while other interfaces are still in operation.
For the first (and default) alternative bAlternateSetting is always 0.
To have a second interface variant, the default interface descriptor would be followed by its endpoint descriptors, which would then be followed by the alternative interface descriptor and then its endpoint descriptors.
bInterfaceClass, bInterfaceSubClass and bInterfaceProtocol
By defining the class, subclass and protocol in the interface, it is possible to have interfaces with different classes in the same device. This is referred to as a composite device.
*/

struct usb_interface_desc
{
unsigned char bLength ; //Number Size of this descriptor in bytes
unsigned char bDescriptorType;//Constant INTERFACE descriptor type (= 4)
unsigned char bInterfaceNumber; //Number Number identifying this interface. Zero-based value.
unsigned char bAlternateSetting;//Number Value used to select this alternate setting for this interface.
unsigned char bNumEndpoints;//Number Number of endpoints used by this interface. Doesn't include control endpoint 0.
unsigned char bInterfaceClass;//Class Class code assigned by USB-IF 00h is a reserved value FFh means vendor-defined class Any other value must be a class code
unsigned char bInterfaceSubClass;//SubClass SubClass Code assigned by USB-IF
unsigned char bInterfaceProtocol;//Protocol Protocol Code assigned by USB-IF
unsigned char iInterface;//Index Index of string descriptor describing interface - set to 0 if no string
};



/*
Configuration Descriptors
A USB device can have several different configurations although the majority of devices are simple and only have one. 
The configuration descriptor specifies how the device is powered, what the maximum power consumption is, the number of interfaces it has. 
Therefore it is possible to have two configurations, one for when the device is bus powered and another when it is mains powered. 
As this is a "header" to the Interface descriptors, its also feasible to have one configuration 
using a different transfer mode to that of another configuration.

Once all the configurations have been examined by the host, the host will send a SetConfiguration command 
with a non zero value which matches the bConfigurationValue of one of the configurations. 
This is used to select the desired configuration.

When the configuration descriptor is read, it returns the entire configuration hierarchy which includes 
all related interface and endpoint descriptors. 
*/
								//Offset   Size	 Value	 Description
struct usb_configuration_desc
{
unsigned char  bLength				;//0	    1	 Number		Size of Descriptor in Bytes
unsigned char  bDescriptorType		;//1	    1	 Constant	Configuration Descriptor (0x02)

//The wTotalLength field reflects the number of bytes in the hierarchy.
unsigned short wTotalLength			;//2	    2	 Number		Total length in bytes of data returned
//bNumInterfaces specifies the number of interfaces present for this configuration.
unsigned char  bNumInterfaces		;//4	    1	 Number		Number of Interfaces
//bConfigurationValue is used by the SetConfiguration request to select this configuration.
unsigned char  bConfigurationValue	;//5	    1	 Number		Value to use as an argument to select this configuration
//iConfiguration is a index to a string descriptor describing the configuration in human readable form.
unsigned char  iConfiguration		;//6	    1	 Index		Index of String Descriptor describing this configuration
//bmAttributes specify power parameters for the configuration. If a device is self powered, it sets D6. Bit D7 was used in USB 1.0 to indicate a bus powered device, but this is now done by bMaxPower. If a device uses any power from the bus, whether it be as a bus powered device or as a self powered device, it must report its power consumption in bMaxPower. Devices can also support remote wakeup which allows the device to wake up the host when the host is in suspend.
//D7 Reserved, set to 1. (USB 1.0 Bus Powered)
//D6 Self Powered
//D5 Remote Wakeup
//D4..0 Reserved, set to 0.
unsigned char  bmAttributes			;//7	    1	 bitmap
//bMaxPower defines the maximum power the device will drain from the bus. This is in 2mA units, thus a maximum of approximately 500mA can be specified. The specification allows a high powered bus powered device to drain no more than 500mA from Vbus. If a device loses external power, then it must not drain more than indicated in bMaxPower. It should fail any operation it cannot perform without external power.
unsigned char  bMaxPower			;//8	    1	 mA			Maximum Power Consumption in 2mA units
};

struct usb_hid_class_desc
{
unsigned char bDescriptorType;		//	Nom du type du descripteur de classe
unsigned short wDescriptorLength;	//	Taille totale du Report Descriptor
}packed_struct;

struct usb_hid_desc
{
unsigned char  bLength;				// Taille totale du descripteur HID
unsigned char  bDescriptorType;		// Nom sp�cifiant le type du descripteur HID
unsigned short bcdHID;				// Version de la sp�cification HID utilis�e
unsigned char  bCountryCode;		// Num�ro du pays
unsigned char  bNumDescriptors;		// Nombre de descripteurs de classe
};

struct hid_report_desc
{
	mem_zone_ref			root_node;
	mem_zone_ref			input_list;
	mem_zone_ref			open_collection_node;
	unsigned int			has_report_id;
	unsigned int			report_total_size;
};

struct usb_hid
{
	struct usb_hid_desc			hid_desc;
	struct usb_hid_class_desc	classes[8];
	struct hid_report_desc		report[8];
};

struct usb_endpoint
{
	struct usb_endpoint_desc	ep_desc;
	unsigned int				data_tog;
};

struct usb_interface
{
	struct usb_interface_desc	iface_desc;
	mem_zone_ref				endpoints_ref;
	struct usb_hid				hid;
	unsigned int				drv_initialized;
	tpo_mod_file				driver_mod;

};

struct usb_configuration
{
	struct usb_configuration_desc	conf_desc;
	mem_zone_ref					interfaces_ref;
};

/*
Device Descriptors
The device descriptor of a USB device represents the entire device. 
As a result a USB device can only have one device descriptor. 

It specifies some basic, yet important information about the device such as 
the supported USB version, maximum packet size, vendor and product IDs and the number of possible configurations
the device can have. The format of the device descriptor is shown below.
*/

struct usb_device_desc
{
								     	//Offset	Size	 Value	 Description
unsigned char  bLength				;	//0		 	 1		Number		Size of the Descriptor in Bytes (18 bytes)
unsigned char  bDescriptorType		;	//1		 	 1		Constant	Device Descriptor (0x01)
//The bcdUSB field reports the highest version of USB the device supports. The value is in binary coded decimal with a format of 0xJJMN where JJ is the major version number, M is the minor version number and N is the sub minor version number. e.g. USB 2.0 is reported as 0x0200, USB 1.1 as 0x0110 and USB 1.0 as 0x0100.
unsigned short bcdUSB				;	//2	 	 	 2		BCD			USB Specification Number which device complies too.
//The bDeviceClass, bDeviceSubClass and bDeviceProtocol are used by the operating system to find a class driver for your device. 
//Typically only the bDeviceClass is set at the device level. 
//Most class specifications choose to identify itself at the interface level and as a result set the bDeviceClass as 0x00.

//This allows for the one device to support multiple classes.
//If equal to Zero, each interface specifies it�s own class code
//If equal to 0xFF, the class code is vendor specified.
//Otherwise field is valid Class Code.
unsigned char  bDeviceClass			;	//4		 	 1	 	Class		Class Code (Assigned by USB Org)
unsigned char  bDeviceSubClass		;	//5		 	 1	 	SubClass	Subclass Code (Assigned by USB Org)
unsigned char  bDeviceProtocol		;	//6		 	 1	 	Protocol	Protocol Code (Assigned by USB Org)
//The bMaxPacketSize field reports the maximum packet size for endpoint zero. All devices must support endpoint zero.
unsigned char  bMaxPacketSize		;	//7		 	 1	 	Number		Maximum Packet Size for Zero Endpoint. Valid Sizes are 8, 16, 32, 64
//The idVendor and idProduct are used by the operating system to find a driver for your device. The Vendor ID is assigned by the USB-IF.
unsigned short idVendor				;	//8		 	 2	 	ID			Vendor ID (Assigned by USB Org)
unsigned short idProduct			;	//10		 2	 	ID			Product ID (Assigned by Manufacturer)
//The bcdDevice has the same format than the bcdUSB and is used to provide a device version number. This value is assigned by the developer.
unsigned short bcdDevice			;	//12		 2	 	BCD			Device Release Number
//Three string descriptors exist to provide details of the manufacturer, product and serial number. 
//There is no requirement to have string descriptors. 
//If no string descriptor is present, a index of zero should be used.
unsigned char  iManufacturer		;	//14		 1	 	Index		Index of Manufacturer String Descriptor
unsigned char  iProduct				;	//15		 1	 	Index		Index of Product String Descriptor
unsigned char  iSerialNumber		;	//16		 1	 	Index		Index of Serial Number String Descriptor
//bNumConfigurations defines the number of configurations the device supports at its current speed.
unsigned char  bNumConfigurations	;	//17		 1	 	Integer		Number of Possible Configurations
}struct_packed ;


/*
String Descriptors
String descriptors provide human readable information and are optional. 
If they are not used, any string index fields of descriptors must be set to zero indicating there is no string descriptor available.
The strings are encoded in the Unicode format and products can be made to support multiple languages. 
String Index 0 should return a list of supported languages. 
A list of USB Language IDs can be found in Universal Serial Bus Language Identifiers (LANGIDs) version 1.0
*/


struct usb_string_desc_zero
{
								//Offset	Size	 Value	 Description
unsigned char  bLength			;//0		1		Number		Size of Descriptor in Bytes
unsigned char  bDescriptorType	;//1		1		Constant	String Descriptor (0x03)
unsigned short *wLANGID			;//2		2		number		Supported Language Code Zero (e.g. 0x0409 English - United States)
};

//The above String Descriptor shows the format of String Descriptor Zero. 
//The host should read this descriptor to determine what languages are available. 
//If a language is supported, it can then be referenced by sending the language ID in the wIndex field of a Get Descriptor(String) request.
//All subsequent strings take on the format below,

struct usb_string_desc
{
									 //Offset	Size	 Value		Description
unsigned char bLength				;// 0		 1		 Number		Size of Descriptor in Bytes
unsigned char bDescriptorType		;// 1		 1		 Constant	String Descriptor (0x03)
unsigned char *bString				;// 2		 n		 Unicode	Unicode Encoded String
};


struct usb_hubdesc
{
  unsigned char  length;
  unsigned char  type;
  unsigned char  portcnt;
  unsigned short characteristics;
  unsigned char  pwdgood;
  unsigned char  current;
  /* Removable and power control bits follow.  */
};

/*
typedef struct
{
unsigned char bLength; //Number Size of this descriptor in bytes
unsigned char bDescriptorType; //Constant DEVICE descriptor type (= 1)
unsigned short bcdUSB; //BCD USB Spec release number
unsigned char bDeviceClass; //1 Class Class code assigned by USB-IF 00h means each interface defines its own class FFh means vendor-defined class Any other value must be a class code
unsigned char bDeviceSubClass; // SubClass SubClass Code assigned by USB-IF 6 bDeviceProtocol 1 Protocol Protocol Code assigned by USB-IF
unsigned char bMaxPacketSize0; //Number Max packet size for endpoint 0. Must be 8, 16, 32 or 64
unsigned short idVendor; //ID Vendor ID - must be obtained from USB-IF
unsigned short idProduct;//Product ID - assigned by the manufacturer
unsigned short bcdDevice;//BCD Device release number in binary coded decimal
unsigned char iManufacturer;//Index Index of string descriptor describing manufacturer - set to 0 if no string
unsigned char iProduct; // Index Index of string descriptor describing product - set to 0 if no string
unsigned char iSerialNumber; //Index Index of string descriptor describing device serial number - set to 0 if no string
unsigned char bNumConfigurations;//Number Number of possible configurations
}usb_device_desc;
*/



// Represents a Transfer Descriptor (TD)
struct usb_td_t
{
	// Hardware part
	volatile unsigned int		token;			// Contains the packet header (where it needs to be sent)
	volatile mem_ptr			buffer_phy;		// A pointer to the buffer with the actual packet
	volatile mem_ptr			link_phy;		// Link to the next TD/QH
	volatile mem_ptr			end_buffer_phy;	// A pointer to the buffer with the actual packet
} ;

typedef	 struct usb_td_t *usb_td_ptr_t;



struct ohci_ed {
        volatile unsigned int ed_flags;
#define OHCI_ED_GET_FA(s)       ((s) & 0x7f)
#define OHCI_ED_ADDRMASK        0x0000007f
#define OHCI_ED_SET_FA(s)       (s)
#define OHCI_ED_GET_EN(s)       (((s) >> 7) & 0xf)
#define OHCI_ED_SET_EN(s)       ((s) << 7)
#define OHCI_ED_DIR_MASK        0x00001800
#define OHCI_ED_DIR_TD          0x00000000
#define OHCI_ED_DIR_OUT         0x00000800
#define OHCI_ED_DIR_IN          0x00001000
#define OHCI_ED_SPEED           0x00002000
#define OHCI_ED_SKIP            0x00004000
#define OHCI_ED_FORMAT_GEN      0x00000000
#define OHCI_ED_FORMAT_ISO      0x00008000
#define OHCI_ED_GET_MAXP(s)     (((s) >> 16) & 0x07ff)
#define OHCI_ED_SET_MAXP(s)     ((s) << 16)
#define OHCI_ED_MAXPMASK        (0x7ff << 16)
        volatile unsigned int ed_tailp;
        volatile unsigned int ed_headp;
#define OHCI_HALTED             0x00000001
#define OHCI_TOGGLECARRY        0x00000002
#define OHCI_HEADMASK           0xfffffffc
        volatile unsigned int ed_next;
};
typedef struct ohci_ed ohci_ed_t;



struct usb_device
{
	char						name[255];
	unsigned int				dev_id;
	unsigned int				bus_id;
	unsigned int				port_id;
	unsigned int				used;
	unsigned int				addr;
	unsigned int				toggle_control;
	unsigned int				cur_configuration;
	usb_speed_t					speed;
	usb_device_state_t			state;
	
	struct usb_device_desc		device_desc;
	mem_zone_ref				configs_ref;
	char						driver_name[128];
	mem_zone_ref				usb_node;
}struct_packed;


struct td_chain_t
{
usb_td_ptr_t		td_chain[64];
unsigned int		n_td;
};

struct usb_transfer_t
{
struct usb_td_t						*head;
struct usb_td_t						*tail;
mem_zone_ref						data;
size_t								data_size;
struct usb_device					*dev;
unsigned int						iface_idx;
unsigned int						endp_idx;
unsigned int						iface_id;
unsigned int						endp_id;
unsigned int						packet_size;
unsigned int						done;
struct td_chain_t					chain;
};


typedef struct
{
							//Ofset Size	 Value		Description
//0 = Host to Device
//1 = Device to Host
//D6..5 Type
//0 = Standard
//1 = Class
//2 = Vendor
//3 = Reserved
//D4..0 Recipient
//0 = Device
//1 = Interface
//2 = Endpoint
//3 = Other
//4..31 = Reserved
unsigned char  bmRequestType;	//0  1	 Bit-Map			D7 Data Phase Transfer Direction
unsigned char  bRequest;			//1	 1	 Value				Request
unsigned short wValue;			//2	 2	 Value				Value
unsigned short wIndex;			//4	 2	 Index or Offset	Index
unsigned short wLength;			//6	 2	 Count				Number of bytes to transfer if there is a data phase
}usb_setup_packet_t;


typedef unsigned int C_API_FUNC usb_get_cmd_flag_func			(unsigned short vendor_id ,unsigned short device_id);
typedef int			 C_API_FUNC usb_probe_device_func			(struct usb_device *usb_device);
typedef unsigned int C_API_FUNC usb_init_driver_func			();
typedef void		 C_API_FUNC usb_start_stream_func			(unsigned int stream_idx);
typedef int			 C_API_FUNC usb_get_device_format_func		(unsigned int idx,unsigned int *fmt_ptr);
typedef int			 C_API_FUNC usb_set_device_format_func		(unsigned int idx);



typedef usb_get_cmd_flag_func						*usb_get_cmd_flag_func_ptr;
typedef usb_probe_device_func						*usb_probe_device_func_ptr;
typedef usb_init_driver_func						*usb_init_driver_func_ptr;
typedef usb_start_stream_func						*usb_start_stream_func_ptr;
typedef usb_get_device_format_func	  				*usb_get_device_format_func_ptr;
typedef usb_set_device_format_func	  				*usb_set_device_format_func_ptr;



typedef  int			 C_API_FUNC usb_hubports_func			();
typedef  int			 C_API_FUNC usb_detect_dev_func		(int port);
typedef  int			 C_API_FUNC usb_portstatus_func		( unsigned int port, unsigned int enable);
typedef  int			 C_API_FUNC usb_ctrl_transfer_func		(unsigned int dev_addr,usb_speed_t speed,unsigned int max_frame_size,struct usb_td_t *first_td,struct usb_td_t *last_td);
								
typedef  int			 C_API_FUNC usb_intr_transfer_func		(struct usb_device *dev,unsigned int endp_id,unsigned int max_frame_size,struct usb_td_t *first_td,struct usb_td_t *last_td);

typedef  void			 C_API_FUNC root_hub_change_func		(unsigned int port_id,unsigned int port_status);
typedef  void			 C_API_FUNC transfer_done_func			(usb_td_ptr_t	td_done);

typedef usb_hubports_func							*usb_hubports_func_ptr;
typedef usb_detect_dev_func							*usb_detect_dev_func_ptr;
typedef usb_portstatus_func							*usb_portstatus_func_ptr;
typedef usb_ctrl_transfer_func						*usb_ctrl_transfer_func_ptr;
typedef usb_intr_transfer_func						*usb_intr_transfer_func_ptr;
typedef root_hub_change_func						*root_hub_change_func_ptr;
typedef transfer_done_func							*transfer_done_func_ptr;


