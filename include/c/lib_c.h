
#include "std_str.h"
#include "std_math.h"


typedef void C_API_FUNC timer_func	(void *pp);
typedef timer_func		*timer_func_ptr;



typedef struct {
  unsigned long ebp;
  unsigned long ebx;
  unsigned long edi;
  unsigned long esi;
  unsigned long esp;
  unsigned long eip;
} jmp_buf[1];

typedef struct
{
	timer_func_ptr	func;
	void			*data;
	unsigned long	time;
}called_func;



LIBC_API char				C_API_FUNC first_char				(const char *str);





LIBC_API unsigned int		C_API_FUNC	libc_init				();
LIBC_API unsigned int		C_API_FUNC	libc_init_cpu			();
LIBC_API unsigned int		C_API_FUNC system_time				();

LIBC_API unsigned int		C_API_FUNC htole32					(unsigned int in);
LIBC_API void				C_API_FUNC htobe24					(unsigned int val,unsigned char *dst);

LIBC_API unsigned short 	C_API_FUNC htobe16					(unsigned short val);
LIBC_API unsigned short 	C_API_FUNC htole16					(unsigned short val);
LIBC_API unsigned short 	C_API_FUNC be16toh					(unsigned short val);
LIBC_API unsigned short 	C_API_FUNC le16toh					(unsigned short val);
LIBC_API unsigned int 		C_API_FUNC htobe32					(unsigned int val);
LIBC_API unsigned int 		C_API_FUNC htole32					(unsigned int val);
LIBC_API unsigned int 		C_API_FUNC be32toh					(unsigned int val);
LIBC_API unsigned int 		C_API_FUNC le32toh					(unsigned int val);

LIBC_API unsigned int		C_API_FUNC set_bit					(unsigned int data_orig,unsigned int value,unsigned int ofset);
LIBC_API unsigned int		C_API_FUNC write_bits				(unsigned int data_orig,unsigned int data_bits,unsigned int ofset,unsigned int n_bits);
LIBC_API unsigned char		C_API_FUNC write_bits_8				(unsigned char data_orig,unsigned char data_bits,unsigned char ofset,unsigned char n_bits);
LIBC_API int				C_API_FUNC ice_init					();
LIBC_API int				C_API_FUNC ice_sort					(const int* input, size_t nb, int signedvalues);


LIBC_API void				ASM_API_FUNC longjmp_c				(jmp_buf env, int value);
LIBC_API int				ASM_API_FUNC setjmp_c				(jmp_buf env);


#define _ADDRESSOF(v)   ( &(v) )
#define _INTSIZEOF(n)   ( (sizeof(n) + sizeof(int) - 1) & ~(sizeof(int) - 1) )

#define _crt_va_start(ap,v)  ( ap = (va_list)_ADDRESSOF(v) + _INTSIZEOF(v) )
#define _crt_va_arg(ap,t)    ( *(t *)((ap += _INTSIZEOF(t)) - _INTSIZEOF(t)) )
#define _crt_va_end(ap)      ( ap = (va_list)0 )

#define va_start _crt_va_start
#define va_arg _crt_va_arg
#define va_end _crt_va_end

extern unsigned int	errno;