
#ifndef KERNEL_API	
#define KERNEL_API	C_IMPORT
#define KERN_API_FUNC	ASM_API_FUNC
#endif

KERNEL_API unsigned int		KERN_API_FUNC   read_pci_config_c			(unsigned int bus,unsigned int dev,unsigned int fn,unsigned int *config_out);

KERNEL_API unsigned int		KERN_API_FUNC	read_pci_dword_c			(int bus, int device, int func,int reg ) ;
KERNEL_API void				KERN_API_FUNC	write_pci_dword_c			(int bus, int device, int func, int reg , unsigned int value);

KERNEL_API unsigned short	KERN_API_FUNC	read_pci_word_c				(int bus, int device, int func,int reg ) ;
KERNEL_API void				KERN_API_FUNC	write_pci_word_c			(int bus, int device, int func, int reg , unsigned short value);

KERNEL_API unsigned char	KERN_API_FUNC	read_pci_byte_c				(int bus, int device, int func,int reg ) ;
KERNEL_API void				KERN_API_FUNC	write_pci_byte_c			(int bus, int device, int func, int reg , unsigned char value);
KERNEL_API void				KERN_API_FUNC	pci_enable_device_c			(int bus, int device, int func);


//DLL_IMPORT unsigned int KERN_API_FUNC pci_get_vendor_string_c			(unsigned short vendor_id, char *dst_string,unsigned int len);
//DLL_IMPORT unsigned int KERN_API_FUNC pci_get_vendor_device_string_c	(unsigned short vendor_id,unsigned short device_id, char *dst_vendor_string,unsigned int dst_vendor_len, char *dst_device_string,unsigned int dst_device_len);


typedef struct
{
unsigned short vendor_id;					//	0
unsigned short device_id;					//  2
unsigned short command;						//  4
unsigned short status;						//	6
unsigned char  rev_id;						//	8
unsigned char  class_code[3];				//  9
unsigned char  line;						//	12
unsigned char  timer;						//  13
unsigned char  header_type;					//  14
unsigned char  bist;						//  15
unsigned char  base_addr_regs[24];			//  16
unsigned int   cardbus_ptr;					//	40
unsigned short subsys_vendor_id;			//	44
unsigned short subsys_id;					//	46
unsigned int   rom_addr;					//	48
unsigned char  cap_ptr;						//	52
unsigned char  reserved[3];					//	53
unsigned int   reserved2;					//	56
unsigned char  int_line;					//	60
unsigned char  int_pin;						//	61
unsigned char  min_gnt;						//  62
unsigned char  max_lat;						//  63
}pci_dev_config;
//64 - 3C

#define PCI_PCICMD		0x04
#define PCI_PCICMD_IOS	0x01
#define PCI_PCICMD_MSE	0x02
#define PCI_PCICMD_BME	0x04

#define PCI_CFG			0x41
#define PCI_CFG_IOSE	0x01

#define	PCIM_CMD_PORTEN			0x0001
#define	PCIM_CMD_MEMEN			0x0002
#define	PCIM_CMD_BUSMASTEREN	0x0004

#define PCI_address_memory_32_mask	0xFFFFFFF0	/* mask to get 32bit memory space base address */
#define PCI_address_io_mask		0xFFFFFFFC	/* mask to get i/o space base address */

