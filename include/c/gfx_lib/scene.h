
unsigned int		C_API_FUNC gfx_scene_clear					(mem_zone_ref_ptr scene_root);
unsigned int		C_API_FUNC gfx_scene_add_text				(mem_zone_ref_ptr scene_root,const char *text,const char *style);
unsigned int		C_API_FUNC gfx_scene_add_rect				(mem_zone_ref_ptr scene_root,const char *style);
unsigned int		C_API_FUNC gfx_scene_add_text_list			(mem_zone_ref_ptr scene_root,const char *style);
unsigned int		C_API_FUNC gfx_scene_add_text_to_list		(mem_zone_ref_ptr scene_root,unsigned int text_list_id,const char *text,const char *style);
unsigned int		C_API_FUNC gfx_scene_get_obj				(mem_zone_ref_ptr scene_root,unsigned int obj_id,mem_zone_ref_ptr dst_node);
unsigned int		C_API_FUNC gfx_scene_swap_ctrl				(mem_zone_ref_ptr scene_root,unsigned int obj_id,mem_zone_ref_ptr src_node);
unsigned int		C_API_FUNC gfx_set_scene_obj_val_ui			(mem_zone_ref_ptr scene_root,unsigned int obj_id,const char *name,unsigned int val);
unsigned int		C_API_FUNC gfx_scene_add_image				(mem_zone_ref_ptr scene_root,int p_x,int p_y,const char *fs_name,const char *image_path);

unsigned int		gfx_scene_add_obj_style						(mem_zone_ref_ptr scene_root,unsigned int obj_id,const char *style);
unsigned int		gfx_scene_rem_obj							(mem_zone_ref_ptr scene_root,unsigned int obj_id);
unsigned int		gfx_scene_add_ctrl							(mem_zone_ref_ptr scene_root,mem_zone_ref_ptr src_node);

unsigned int		gfx_scene_obj_cpy_childs					(mem_zone_ref_ptr scene_root,unsigned int obj_id,mem_zone_ref_const_ptr node);
unsigned int		gfx_scene_get_ctrl_scene_itr				(mem_zone_ref_const_ptr ctrl_node,scene_itr_t		*scene_itr);
unsigned int		gfx_scene_get_extent						(mem_zone_ref_ptr scene_root,struct gfx_rect *out_rect);
unsigned int		gfx_scene_event_test						(mem_zone_ref_ptr scene_root,mem_zone_ref_ptr container_event,vec_2s_t trans,mem_zone_ref_ptr ctrl_event_node);


unsigned int		gfx_get_scene_obj_val_ui					(mem_zone_ref_ptr scene_root,unsigned int obj_id,const char *name,unsigned int *val);
unsigned int		gfx_get_scene_obj_type						(mem_zone_ref_ptr scene_root,unsigned int obj_id);
unsigned int		gfx_get_scene_obj_val_str					(mem_zone_ref_ptr scene_root,unsigned int obj_id,const char *name,char *val,unsigned int str_len);
unsigned int		gfx_get_scene_obj_prop_by_type				(mem_zone_ref_ptr scene_root,unsigned int obj_id,unsigned int type,mem_zone_ref_ptr out);
void				gfx_get_scene_dump_obj						(mem_zone_ref_ptr scene_root,unsigned int obj_id);


