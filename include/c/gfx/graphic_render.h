#ifndef GFX_API
#define GFX_API	C_IMPORT
#endif

typedef struct
{
	unsigned int	str_len;
	mem_zone_ref	str;
}gfx_str_t;


typedef void		 C_API_FUNC set_surface_attr_ui_func	(unsigned int surface_id,unsigned int attr,unsigned int attr_val);
typedef void		 C_API_FUNC free_surface_2D_func		(unsigned int surf_id);
typedef void		 C_API_FUNC set_color_4uc_func			(unsigned char r,unsigned char g,unsigned char b,unsigned char a);
typedef void		 C_API_FUNC draw_rect_func				(const gfx_rect_t *rect);
typedef void		 C_API_FUNC swap_buffers_func			(unsigned int flags);
typedef void		 C_API_FUNC clear_buffers_func			(unsigned int flags);
typedef void		 C_API_FUNC blit_surf_func				(unsigned int surface_id,int x_pos,int y_pos,gfx_rect_t *src_rect);
typedef void		 C_API_FUNC blit_surf_resize_func		(unsigned int surface_id, gfx_rect_t *dst_rect,gfx_rect_t *src_rect);
typedef void		 C_API_FUNC set_clear_color_4uc_func	(unsigned char r,unsigned char g,unsigned char b,unsigned char a);
typedef void		 C_API_FUNC set_clip_rect_func			(int x,int y,unsigned int width,unsigned int height);
typedef void		 C_API_FUNC draw_img_func  				(int x_pos, int y_pos,gfx_image_t *img,gfx_rect_t *src_rect);
typedef void		 C_API_FUNC draw_line_func			    (const vec_2s_t	p1,const vec_2s_t p2);
typedef void		 C_API_FUNC draw_hspan_list_func		(struct gfx_hspan_list_t *list,int x,int y);
typedef void		 C_API_FUNC draw_point_func				(const vec_2s_t	p1);
typedef void		 C_API_FUNC draw_text_func				(const char *text_txt,const vec_2s_t trans,const vec_2s_t zoom,vec_2s_t out_ext);
typedef void		 C_API_FUNC draw_image_resize_func		(gfx_image_t *img, gfx_rect_t *dst_rect,gfx_rect_t *src_rect);
typedef void		 C_API_FUNC get_text_vec_func			(const char *text,struct gfx_hspan_list_t *list,struct gfx_rect *text_box);
typedef void		 C_API_FUNC draw_cursor_func			(int x,int y);

typedef unsigned int C_API_FUNC create_surface_2D_func		(unsigned int surf_type	,unsigned int pix_type,unsigned int d_type,unsigned int width,unsigned int height,unsigned int d_pix_type,unsigned char *data);
typedef unsigned int C_API_FUNC load_surface_data_func		(unsigned int surface_id,int x,int y,gfx_rect_t *src_rect,unsigned int src_scan_line,unsigned int pix_type,mem_ptr pix_data);
typedef unsigned int C_API_FUNC select_font_func			(const char *font_name,unsigned int font_size_x,unsigned int font_size_y,unsigned int *new_font,struct gfx_font_glyph_list_t **out_list);
typedef unsigned int C_API_FUNC load_font_surface_data_func (gfx_rect_t *src_rect,unsigned int glyph_idx,unsigned int src_scan_line,unsigned int pix_type,mem_ptr pix_data);
typedef unsigned int C_API_FUNC load_cursor_func			(gfx_image_t	*cursor_img);

typedef create_surface_2D_func		*create_surface_2D_func_ptr; 
typedef load_surface_data_func		*load_surface_data_func_ptr;
typedef load_font_surface_data_func *load_font_surface_data_func_ptr;
typedef set_surface_attr_ui_func	*sset_surface_attr_ui_func_ptr;
typedef free_surface_2D_func		*free_surface_2D_func_ptr;
typedef swap_buffers_func			*swap_buffers_func_ptr;
typedef clear_buffers_func			*clear_buffers_func_ptr;
typedef set_color_4uc_func			*set_color_4uc_func_ptr;
typedef draw_rect_func				*draw_rect_func_ptr;
typedef blit_surf_func				*blit_surf_func_ptr;
typedef blit_surf_resize_func		*blit_surf_resize_func_ptr;
typedef set_clear_color_4uc_func	*set_clear_color_4uc_func_ptr;
typedef set_clip_rect_func			*set_clip_rect_func_ptr;
typedef draw_img_func				*draw_img_func_ptr;
typedef draw_line_func				*draw_line_func_ptr;
typedef draw_hspan_list_func		*draw_hspan_list_func_ptr;
typedef draw_point_func				*draw_point_func_ptr;
typedef draw_image_resize_func		*draw_image_resize_func_ptr;
typedef select_font_func			*select_font_func_ptr;
typedef draw_text_func				*draw_text_func_ptr;
typedef get_text_vec_func			*get_text_vec_func_ptr;
typedef load_cursor_func			*load_cursor_func_ptr;
typedef draw_cursor_func			*draw_cursor_func_ptr;


typedef struct
{
	unsigned int					id;
	cursor_imgs_idx					cursor_img_id;

	create_surface_2D_func_ptr		create_surface_2D;
	load_surface_data_func_ptr		load_surface_data;
	load_font_surface_data_func_ptr load_font_surface_data;

	free_surface_2D_func_ptr		free_surface_2D;
	set_color_4uc_func_ptr			set_color_4uc;
	draw_rect_func_ptr				draw_rect;
	blit_surf_func_ptr				blit_surf;
	blit_surf_resize_func_ptr		blit_surf_resize;
	draw_img_func_ptr				draw_image;
	draw_line_func_ptr				draw_line;
	draw_hspan_list_func_ptr		draw_hspan_list;
	draw_point_func_ptr				draw_point;
	draw_image_resize_func_ptr		draw_image_resize;
	swap_buffers_func_ptr			swap_buffers;
	clear_buffers_func_ptr			clear_buffers;
	set_clear_color_4uc_func_ptr	set_clear_color_4uc;
	set_clip_rect_func_ptr			set_clip_rect;
	select_font_func_ptr			select_font;
	draw_text_func_ptr				draw_text;
	get_text_vec_func_ptr			get_text_vec;
	load_cursor_func_ptr			load_cursor;
	draw_cursor_func_ptr			draw_cursor;
}gfx_renderer_device_t;


GFX_API int		  C_API_FUNC gfx_font_init			(struct gfx_font_glyph_list_t *glyph_list,const char *font_name,unsigned int font_size_x,unsigned int font_size_y,unsigned int first_glyph,unsigned int last_glyph);
