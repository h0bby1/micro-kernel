#ifndef GFX_API 
#define GFX_API	C_IMPORT
#endif

/*
#define PIX_NONE		0x00000000
#define PIX_RGB			0x00000001
#define PIX_RGBA		0x00000002
#define PIX_INDEXED		0x00000003
#define PIX_MONO		0x00000004
#define PIX_GRAY		0x00000005
*/




typedef struct gfx_rect gfx_rect_t;



typedef enum
{
	GFX_PIX_FORMAT_NONE		=	0,
	GFX_PIX_FORMAT_RGB		=	1,
	GFX_PIX_FORMAT_RGBA		=	2,
	GFX_PIX_FORMAT_INDEXED	=	3,
	GFX_PIX_FORMAT_MONO		=	4,
	GFX_PIX_FORMAT_GRAY		=	5
	
}gfx_pix_fmt_t;

typedef enum  
{
	GFX_CURSOR_DEFAULT		=	1,
	GFX_CURSOR_SELECTED		=	2,
	GFX_CURSOR_MOVE_RIGHT	=	3,
	GFX_CURSOR_MOVE_BOTTOM  =	4
}cursor_imgs_idx;



typedef struct
{
	struct VBitmap				OrigImage;
	struct VBitmap				DrawArea;
	struct ResampleFilterData	fd;

	unsigned int				image_id;
	unsigned int				width;
	unsigned int				height;
	unsigned int				line_size;
	unsigned int				pix_stride;
	gfx_pix_fmt_t				pixfmt;
	mem_zone_ref				line_table;
	mem_zone_ref				image_data;
	mem_ptr						line_ptr;
}gfx_image_t;

struct gfx_hspan_t
{
	short			pos[2];
    unsigned int	len;
	unsigned int	coverage;
};

struct gfx_hspan_list_t
{

	mem_zone_ref	span_list;
	unsigned int	num_spans;
	unsigned int	num_span_alloc;
};


struct gfx_glyph_metrics_t
{
	int	box_top;
	int	box_left;

	int	adv_x;
	int	adv_y;

};

struct gfx_glyph_span_idx_t
{
	unsigned int	glyph_span_last;
	unsigned int	glyph_span_first;
};

struct gfx_font_glyph_list_t
{
	unsigned int						max_char_w;
	unsigned int						max_char_h;
	unsigned int						size_x;
	unsigned int						size_y;
	struct  gfx_glyph_span_idx_t		glyph_infos[256];
	struct	gfx_glyph_metrics_t			glyph_metrics[256];
	struct	gfx_rect					glyph_box[256];
	struct  gfx_hspan_list_t			span_list;
};



typedef struct
{
	unsigned int	id;
	unsigned int	task_id;


	mem_zone_ref	scene_ref;
	mem_zone_ref	ctrl_scene_ref;
	/*
	unsigned int	scene_id;
	unsigned int	ctrl_scene_id;
	*/

	unsigned int	z_order;

	gfx_rect_t		rect;
	vec_2s_t		zoom_fac;
	vec_2s_t		trans;

	mem_zone_ref	event_array;
	mem_ptr			event_list_sem;

	gfx_rect_t		ctrl_rect[16];
}gfx_render_container_t;

typedef gfx_render_container_t	*gfx_render_container_ptr_t;

GFX_API unsigned int	C_API_FUNC gfx_load_font				(const char *font_file,char *font_name,int str_len);
GFX_API int				C_API_FUNC gfx_init_render_device		(unsigned int bus_drv_id,unsigned int dev_id);
GFX_API unsigned int	C_API_FUNC gfx_compute_container		(unsigned int container_id,unsigned int render_id);
GFX_API unsigned int	C_API_FUNC gfx_render_all_containers	(unsigned int render_id);
GFX_API unsigned int	C_API_FUNC gfx_compute_all_containers	(unsigned int render_id);


extern const vec_4uc_t	white;
extern const vec_4uc_t	black;

void init_image();