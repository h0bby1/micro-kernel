#ifndef GFX_LIB_API  
#define GFX_LIB_API C_IMPORT
#endif

#ifndef GFX_API  
#define GFX_API C_IMPORT
#endif


typedef unsigned int	C_API_FUNC itr_scene_get_obj_fnc			(mem_zone_ref_ptr scene_root,unsigned int obj_id,mem_zone_ref_ptr dst_node);
typedef unsigned int	C_API_FUNC itr_scene_add_text_fnc			(mem_zone_ref_ptr scene_root,const char *text,const char *style);
typedef unsigned int	C_API_FUNC itr_scene_add_rect_fnc			(mem_zone_ref_ptr scene_root,const char *style);
typedef unsigned int	C_API_FUNC itr_scene_swap_ctrl_fnc			(mem_zone_ref_ptr scene_root,unsigned int obj_id,mem_zone_ref_ptr src_node);
typedef unsigned int	C_API_FUNC itr_scene_add_image_fnc			(mem_zone_ref_ptr scene_root, int p_x, int p_y, const char *fs_name, const char *image_path);


typedef unsigned int	C_API_FUNC itr_scene_clear_fnc				(mem_zone_ref_ptr scene_root);
typedef unsigned int	C_API_FUNC itr_set_scene_obj_val_ui_fnc		(mem_zone_ref_ptr scene_root,unsigned int obj_id,const char *name,unsigned int val);
typedef unsigned int	C_API_FUNC itr_set_scene_obj_val_str_fnc	(mem_zone_ref_ptr scene_root,unsigned int obj_id,const char *name,const char *val);



typedef		itr_scene_add_text_fnc 		 	*itr_scene_add_text_fnc_ptr;
typedef		itr_scene_add_rect_fnc 		 	*itr_scene_add_rect_fnc_ptr;
typedef		itr_scene_add_rect_fnc 		 	*itr_scene_add_rect_fnc_ptr;
typedef		itr_scene_clear_fnc	   		 	*itr_scene_clear_fnc_ptr;
typedef		itr_scene_swap_ctrl_fnc			*itr_scene_swap_ctrl_fnc_ptr;
typedef		itr_scene_get_obj_fnc			*itr_scene_get_obj_fnc_ptr;
typedef		itr_set_scene_obj_val_ui_fnc	*itr_set_scene_obj_val_ui_fnc_ptr;
typedef		itr_set_scene_obj_val_str_fnc	*itr_set_scene_obj_val_str_fnc_ptr;
typedef		itr_scene_add_image_fnc			*itr_scene_add_image_fnc_ptr;


typedef	struct
{
	itr_scene_add_image_fnc_ptr			add_image;
	itr_scene_add_text_fnc_ptr			add_text;
	itr_scene_add_rect_fnc_ptr			add_rect;
	itr_scene_swap_ctrl_fnc_ptr 		swap_ctrl;
	itr_scene_clear_fnc_ptr				clear;
	itr_scene_get_obj_fnc_ptr			get_obj;
	itr_set_scene_obj_val_ui_fnc_ptr	set_obj_val_ui;
	itr_set_scene_obj_val_str_fnc_ptr	set_obj_val_str;
}scene_itr_t;


unsigned int	gfx_init_containers							();



GFX_LIB_API unsigned int	C_API_FUNC gfx_create_render_container					(const char *name,int x,int y,unsigned int width,unsigned int height);
GFX_LIB_API unsigned int	C_API_FUNC gfx_process_events							();
GFX_LIB_API unsigned int	C_API_FUNC gfx_itr_get_scene							(scene_itr_t	*scene_itr);
GFX_LIB_API unsigned int	C_API_FUNC gfx_container_add_ctrl						(unsigned int container_id,mem_zone_ref_ptr ctrl_data_node);

GFX_LIB_API unsigned int	C_API_FUNC gfx_container_add_text						(unsigned int container_id,const char *text,const char *style);
GFX_LIB_API unsigned int	C_API_FUNC gfx_container_add_text_to_list				(unsigned int container_id,unsigned int list_id,const char *text,const char *style);
GFX_LIB_API unsigned int	C_API_FUNC gfx_container_add_text_list					(unsigned int container_id,const char *style);
GFX_LIB_API unsigned int	C_API_FUNC gfx_container_add_obj_style					(unsigned int container_id,unsigned int obj_id,const char *style);
GFX_LIB_API unsigned int	C_API_FUNC gfx_container_rem_obj						(unsigned int container_id,unsigned int obj_id);
GFX_LIB_API unsigned int	C_API_FUNC gfx_container_add_menu						(unsigned int container_id,mem_zone_ref_ptr menu);
GFX_LIB_API unsigned int	C_API_FUNC gfx_container_add_image						(unsigned int container_id,unsigned int p_x,unsigned int p_y,const char *fs_name,const char *image_path);



unsigned int	gfx_container_remove_all_text				(unsigned int container_id);
int				gfx_pop_container_event						(unsigned int container_id,mem_zone_ref *event_node);
unsigned int	gfx_dump_container							(unsigned int container_id);
void			gfx_dump_all_containers						();
void			gfx_render_containers_set_scroll_y			(unsigned int container_id,int scroll_x,int scroll_y);
int				gfx_render_containers_get_pos				(unsigned int container_id,unsigned int *pos_x,unsigned int *pos_y);
int				gfx_render_containers_get_size				(unsigned int container_id,unsigned int *size_x,unsigned int *size_y);
int				gfx_render_containers_get_cursor_abs		(unsigned int container_id,const vec_2s_t cursor,vec_2s_t out);
int				gfx_render_containers_set_pos				(unsigned int container_id,unsigned int pos_x,unsigned int pos_y);

unsigned int	gfx_render_container_dump					(unsigned int container_id);
void			gfx_render_containers_set_zoom				(unsigned int container_id,vec_2s_t zoom);
unsigned int	gfx_render_containers_get_selected			(unsigned int container_id,vec_2s_t					cursor,mem_zone_ref_ptr event_node);
unsigned int	gfx_container_get_obj_node					(unsigned int container_id,unsigned int obj_id,mem_zone_ref_ptr	node);
unsigned int	gfx_render_containers_set_obj_prop			(unsigned int container_id,unsigned int obj_id,const char *name,unsigned int val);
unsigned int	gfx_render_containers_get_obj_prop			(unsigned int container_id,unsigned int obj_id,const char *name,unsigned int *val);
unsigned int	gfx_render_containers_get_obj_type			(unsigned int container_id,unsigned int obj_id);
unsigned int	gfx_render_containers_get_obj_prop_str		(unsigned int container_id,unsigned int obj_id,const char *name,char *val,unsigned int str_len);
unsigned int	gfx_render_containers_get_obj_prop_by_type	(unsigned int container_id,unsigned int obj_id,unsigned int type,mem_zone_ref_ptr out);
unsigned int 	gfx_render_containers_get_extent			(unsigned int container_id,struct gfx_rect *rect);


GFX_API unsigned int C_API_FUNC gfx_init					();
GFX_API void		 C_API_FUNC gfx_set_dragged_id			(unsigned int ctrl_id,unsigned int obj_id);



