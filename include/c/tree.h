#ifndef LIBC_API
#define LIBC_API C_IMPORT
#endif

#define		NODE_GFX_STR			 		0x00000001
#define		NODE_GFX_INT			 		0x00000002
#define		NODE_GFX_BOOL			 		0x00000004
#define		NODE_GFX_SIGNED_INT			 	0x00000008
#define		NODE_GFX_RECT			 		0x00000010
#define		NODE_GFX_DATA			 		0x00000020
#define		NODE_GFX_PTR			 		0x00000040
#define		NODE_GFX_4UC			 		0x00000080
#define		NODE_GFX_INT64			 		0x00000100
#define		NODE_GFX_NULL			 		0x00000200
#define		NODE_GFX_OBJECT			 		0x00000400
#define		NODE_GFX_SIGNED_INT64		 	0x00000800



#define		NODE_PCI_BUS			 		0x01000002
#define		NODE_PCI_BUS_DEV		 		0x01000004
#define		NODE_BUS_DEVICE			 		0x01000008
#define		NODE_BUS_DEVICE_STREAM_INFOS	0x01000010
#define		NODE_BUS_DEVICE_STREAM_TYPE		0x01000020
#define		NODE_BUS_DEVICE_IRQ_INFOS		0x01000040

#define		NODE_BUS_DEV_ROOT			 	0x01000001
#define		NODE_BUS_DRIVER_ROOT	 		0x01000040
#define		NODE_BUS_DRIVER_VENDOR	 		0x01000080
#define		NODE_BUS_DRIVER_DEVICE	 		0x01000100
#define		NODE_BUS_DRIVER_SUBSYS	 		0x01000200
#define		NODE_BUS_DRIVER_V_SUBSYS	 	0x01000400
#define		NODE_BUS_DRIVER_CLASS	 		0x01000800
#define		NODE_BUS_DRIVER_SUBCLASS 		0x01001000
#define		NODE_BUS_DRIVER_PI		 		0x01002000

#define		NODE_BUS_DRIVER			 		0x01004000
#define		NODE_BUS_NDIS_DRIVER	 		0x01008000

#define		NODE_FILE_SYSTEM_DEV	 		0x02000001
//#define	NODE_FILE_SYSTEM_ROOT	 		0x02000002
#define		NODE_FILE_SYSTEM_DIR	 		0x02000004
#define		NODE_FILE_SYSTEM_FILE	 		0x02000008
#define		NODE_FILE_SYSTEM_LIST			0x02000010
#define		NODE_FILE_SYSTEM_PATH	 		0x02000020

#define		NODE_INI_ROOT_NODE		 		0x03000001
#define		NODE_INI_SECTION_NODE	 		0x03000002
#define		NODE_INI_VALUE_NODE		 		0x03000004
#define		NODE_INI_REGCONF_NODE	 		0x03000008
#define		NODE_INI_REGKEY_NODE	 		0x03000010
#define		NODE_INI_REGVALUE_NODE	 		0x03000020

#define		NODE_GFX_SCENE			 		0x04000001
#define		NODE_GFX_TEXT			 		0x04000002
#define		NODE_GFX_IMAGE_OBJ		 		0x04000004
#define		NODE_GFX_CTRL_DATA_COLUMN 		0x04000008
#define		NODE_GFX_CTRL_DATA_COLUMN_LIST	0x04000010
#define		NODE_GFX_STYLE			 		0x04000020
#define		NODE_GFX_CTRL			 		0x04000040
#define		NODE_GFX_CTRL_ITEM		 		0x04000080
#define		NODE_GFX_TEXT_LIST		 		0x04000100
#define		NODE_GFX_TEXT_LIST_ENTRY 		0x04000200
#define		NODE_GFX_CTRL_ITEM_DATA			0x04000400
#define		NODE_GFX_CTRL_ITEM_LIST	 		0x04000800
#define		NODE_GFX_EVENT_LIST		 		0x04001000
#define		NODE_GFX_EVENT			 		0x04002000
#define		NODE_GFX_RECT_OBJ		 		0x04004000
#define		NODE_GFX_IMAGE					0x04008000

//#define		NODE_USB_ROOT_HUB				0x05000001
#define		NODE_USB_HUB					0x05000002
#define		NODE_USB_CONFIGURATION			0x05000004
#define		NODE_USB_INTERFACE				0x05000008
#define		NODE_USB_ENDPOINT				0x05000010

#define		NODE_HID_REPORT					0x06000001
#define		NODE_HID_COLLECTION_PHYSICAL	0x06000002
#define		NODE_HID_COLLECTION_APPLICATION	0x06000004
#define		NODE_HID_COLLECTION_LOGICAL		0x06000008
#define		NODE_HID_INPUT					0x06000010
#define		NODE_HID_USAGE					0x06000020
#define		NODE_HID_INFOS					0x06000040
#define		NODE_HID_INPUT_TYPE				0x06000080
#define		NODE_HID_INPUT_LIST				0x06000090

#define		NODE_REQUEST					0x07000001
#define		NODE_MEM_AREA_LIST				0x07000002
#define		NODE_MEM_AREA					0x07000004
#define		NODE_MEM_AREA_DESC				0x07000008
#define		NODE_TREE_AREA_DESC				0x07000010
#define		NODE_TREE_NODE_DESC				0x07000020

#define		NODE_TASK_LIST					0x08000001
#define		NODE_TASK						0x08000002
#define		NODE_TASK_DATA					0x08000003
#define		NODE_SEMAPHORE					0x08000004

#define		NODE_JSON_ARRAY					0x09000001

struct node_hash_val_t
{
	unsigned int		crc;
	mem_ptr				data;
};




LIBC_API  int  			C_API_FUNC	tree_manager_create_node				(const char *name,unsigned int type,mem_zone_ref *ref_ptr);
LIBC_API  int			C_API_FUNC	tree_manager_create_node_childs			(const char *name,unsigned int type,mem_zone_ref *ref_ptr,const char *params);
LIBC_API  int			C_API_FUNC	tree_manager_create_node_params			(mem_zone_ref_ptr ref_ptr,const char *params);
LIBC_API  int			C_API_FUNC	tree_manager_add_node_childs			(mem_zone_ref_ptr p_node_ref,const char *params,unsigned int merge);
LIBC_API  void			C_API_FUNC	tree_manager_sort_childs				(mem_zone_ref_ptr parent_ref_ptr,const char *name,unsigned int dir);

LIBC_API  int			C_API_FUNC	tree_manager_get_first_child			(mem_zone_ref_const_ptr p_node_ref,	mem_zone_ref_ptr child_list,	mem_zone_ref_ptr *p_node_out_ref);
LIBC_API  int			C_API_FUNC	tree_manager_get_next_child				(									mem_zone_ref_ptr child_list,	mem_zone_ref_ptr *p_node_out_ref);

LIBC_API  int			C_API_FUNC	tree_manager_get_last_child				(mem_zone_ref_const_ptr p_node_ref,mem_zone_ref_ptr child_list, mem_zone_ref_ptr *p_node_out_ref);
LIBC_API  int			C_API_FUNC	tree_manager_get_prev_child				(mem_zone_ref_ptr child_list,mem_zone_ref_ptr *p_node_out_ref);


LIBC_API  unsigned int	C_API_FUNC	and_node_type							(unsigned int type1,unsigned int type2);
LIBC_API  const char *	C_API_FUNC	tree_mamanger_get_node_name				(mem_zone_ref_const_ptr node_ref);
LIBC_API  unsigned int	C_API_FUNC	tree_mamanger_get_node_type				(mem_zone_ref_const_ptr node_ref);
LIBC_API  int			C_API_FUNC  tree_mamanger_get_parent				(mem_zone_ref_const_ptr node_ref,mem_zone_ref_ptr p_ref);
LIBC_API  int			C_API_FUNC  tree_manager_get_ancestor_by_type		(mem_zone_ref_const_ptr node_ref,unsigned int node_type,mem_zone_ref_ptr  p_ref);
LIBC_API  size_t		C_API_FUNC	tree_manager_get_node_num_children		(mem_zone_ref_ptr p_node_ref);

LIBC_API  void			C_API_FUNC	tree_manager_set_node_name				(mem_zone_ref_ptr node_ref,const char *name);

LIBC_API  int  			C_API_FUNC	tree_manager_add_child_node				(mem_zone_ref_ptr parent_ref_ptr,const char *name,unsigned int type,mem_zone_ref *ref_ptr);
LIBC_API  unsigned int	C_API_FUNC	tree_manager_node_add_child				(mem_zone_ref_ptr parent_ref_ptr,mem_zone_ref_const_ptr child_ref_ptr);
LIBC_API  int			C_API_FUNC	tree_manager_node_dup					(mem_zone_ref_ptr new_parent,mem_zone_ref_ptr src_ref_ptr,mem_zone_ref_ptr new_ref_ptr);
LIBC_API  int			C_API_FUNC	tree_manager_node_dup_one				(mem_zone_ref_ptr src_ref_ptr,mem_zone_ref_ptr new_ref_ptr);
LIBC_API  int			C_API_FUNC	tree_manager_copy_children				(mem_zone_ref_ptr dest_ref_ptr,mem_zone_ref_const_ptr src_ref_ptr);

LIBC_API  int			C_API_FUNC	tree_manager_get_child_at				(mem_zone_ref_const_ptr parent_ref_ptr	,unsigned int index,mem_zone_ref_ptr ref_ptr);
LIBC_API  int			C_API_FUNC	tree_manager_find_child_node			(mem_zone_ref_const_ptr parent_ref_ptr	,unsigned int crc_name,unsigned int type,mem_zone_ref_ptr ref_ptr);

LIBC_API  int			C_API_FUNC	 tree_node_read_childs					(mem_zone_ref_const_ptr p_node_ref,struct node_hash_val_t *list);
LIBC_API  int			C_API_FUNC	tree_node_find_child_by_name			(mem_zone_ref_const_ptr p_node_ref		,const char *name						,mem_zone_ref_ptr p_node_out_ref);
LIBC_API  unsigned int	C_API_FUNC	tree_node_list_child_by_type			(mem_zone_ref_const_ptr p_node_ref		,unsigned int type						,mem_zone_ref_ptr p_node_out_ref,unsigned int index);
LIBC_API  int			C_API_FUNC	tree_node_find_child_by_type_value		(mem_zone_ref_const_ptr node,unsigned int type,unsigned int value,mem_zone_ref_ptr p_node_out_ref);
LIBC_API  int			C_API_FUNC	tree_node_find_child_by_type			(mem_zone_ref_const_ptr p_node_ref		,unsigned int node_type					,mem_zone_ref_ptr p_node_out_ref,unsigned int index);
LIBC_API  int			C_API_FUNC	tree_node_find_child_by_id				(mem_zone_ref_const_ptr p_node_ref		,unsigned int node_id,mem_zone_ref_ptr p_node_out_ref);
LIBC_API  int			C_API_FUNC	tree_find_child_node_by_id_name			(mem_zone_ref_const_ptr  p_node_ref,unsigned int child_type,const char *id_name,unsigned int id_val,mem_zone_ref_ptr out_node);
//LIBC_API  int			C_API_FUNC	tree_find_child_node_by_value			(mem_zone_ref_const_ptr  p_node_ref,unsigned int child_type,const char *id_name,unsigned int id_val,mem_zone_ref_ptr out_node);
LIBC_API  int			C_API_FUNC  tree_find_child_node_idx_by_id			(mem_zone_ref *p_node_ref,unsigned int child_type,unsigned int child_id,unsigned int *out_idx);
LIBC_API  int			C_API_FUNC	tree_find_child_node_by_member_name		(mem_zone_ref_const_ptr p_node_ref,unsigned int child_type,unsigned int child_member_type,const char *child_member_name,mem_zone_ref_ptr out_node);
LIBC_API  int			C_API_FUNC	tree_swap_child_node_by_id				(mem_zone_ref_ptr p_node_ref,unsigned int id_val,mem_zone_ref_ptr node);

LIBC_API  int			C_API_FUNC	tree_remove_children					(mem_zone_ref_ptr p_node_ref);
LIBC_API  int			C_API_FUNC	tree_remove_child_by_type				(mem_zone_ref_ptr p_node_ref,unsigned int child_type);
LIBC_API  int			C_API_FUNC	tree_remove_child_by_id					(mem_zone_ref_ptr p_node_ref,unsigned int child_id);

LIBC_API  int			C_API_FUNC	tree_remove_child_by_value_dword		(mem_zone_ref_ptr p_node_ref,unsigned int value);
LIBC_API  int			C_API_FUNC	tree_remove_child_by_member_value_dword	(mem_zone_ref_ptr p_node_ref,unsigned int child_type,const char *member_name,unsigned int value);





LIBC_API  int  			C_API_FUNC	tree_manager_allocate_node_data			(mem_zone_ref_ptr node_ref,mem_size data_size);
LIBC_API  int	 		C_API_FUNC	tree_manager_write_node_data			(mem_zone_ref_ptr node_ref,const_mem_ptr data,mem_size ofset,mem_size size);
LIBC_API  int			C_API_FUNC	tree_manager_copy_node_data				(mem_zone_ref_ptr dst_node,mem_zone_ref_const_ptr src_node);
LIBC_API  int	 		C_API_FUNC	tree_manager_write_node_dword			(mem_zone_ref_ptr node_ref,mem_size ofset,unsigned int value);
LIBC_API  int	 		C_API_FUNC	tree_manager_cmp_z_xchg_node_dword		(mem_zone_ref_ptr node_ref,mem_size ofset,unsigned int value);
LIBC_API  int			C_API_FUNC	tree_manager_write_node_qword			(mem_zone_ref *node_ref,mem_size ofset,uint64_t value);
LIBC_API  int			C_API_FUNC	tree_manager_write_node_4uc				(mem_zone_ref_ptr node_ref,mem_size ofset,const vec_4uc_t val);
LIBC_API  int			C_API_FUNC	tree_manager_write_node_word			(mem_zone_ref_ptr node_ref,mem_size ofset,unsigned short value);
LIBC_API  int			C_API_FUNC	tree_manager_write_node_byte			(mem_zone_ref_ptr node_ref,mem_size ofset,unsigned char value);
LIBC_API  int			C_API_FUNC	tree_manager_write_node_signed_dword	(mem_zone_ref_ptr node_ref,mem_size ofset,int value);
LIBC_API  int			C_API_FUNC	tree_manager_write_node_signed_word		(mem_zone_ref_ptr node_ref,mem_size ofset,short value);
LIBC_API  int		    C_API_FUNC	tree_manager_write_node_str				(mem_zone_ref_ptr node_ref,mem_size ofset,const char *str);
LIBC_API  void			C_API_FUNC	tree_manager_set_node_image_info		(mem_zone_ref_ptr node_ref,mem_size position,mem_size size);

LIBC_API  int			C_API_FUNC	tree_manager_get_node_str				(mem_zone_ref_const_ptr node_ref,mem_size ofset,char *str,unsigned int str_len,unsigned int base);
LIBC_API  int			C_API_FUNC	tree_manager_get_node_4uc				(mem_zone_ref_const_ptr node_ref,mem_size ofset,vec_4uc_t val);
LIBC_API  int		    C_API_FUNC	tree_manager_get_child_value_str		(mem_zone_ref_const_ptr p_node_ref,unsigned int crc_name,char *str,unsigned int str_len,unsigned int base);
LIBC_API  size_t		C_API_FUNC	tree_manager_get_node_image_size		(mem_zone_ref_const_ptr node_ref);
LIBC_API  size_t		C_API_FUNC	tree_manager_get_node_image_pos			(mem_zone_ref_const_ptr node_ref);
LIBC_API  int			C_API_FUNC	tree_mamanger_get_node_dword			(mem_zone_ref_const_ptr node_ref,mem_size ofset,unsigned int *val);
LIBC_API  int			C_API_FUNC	tree_mamanger_get_node_signed_dword		(mem_zone_ref_const_ptr node_ref,mem_size ofset,int *val);
LIBC_API  int			C_API_FUNC	tree_mamanger_get_node_word				(mem_zone_ref_const_ptr node_ref,mem_size ofset,unsigned short *val);
LIBC_API  int			C_API_FUNC	tree_mamanger_get_node_signed_word		(mem_zone_ref_const_ptr node_ref,mem_size ofset,short *val);
LIBC_API  int			C_API_FUNC	tree_mamanger_get_node_byte				(mem_zone_ref_const_ptr node_ref,mem_size ofset,unsigned char *val);
LIBC_API  mem_ptr		C_API_FUNC	tree_mamanger_get_node_data_ptr			(mem_zone_ref_const_ptr node_ref,mem_size ofset);
//LIBC_API  void			C_API_FUNC	tree_mamanger_get_node_data_ref			(mem_zone_ref_const_ptr node_ref,mem_zone_ref_ptr out);
LIBC_API  unsigned int	C_API_FUNC	tree_manager_compare_node_crc			(mem_zone_ref_ptr node_ref,unsigned int crc);
LIBC_API  int			C_API_FUNC	tree_mamanger_compare_node_dword		(mem_zone_ref_ptr node_ref,mem_size ofset,unsigned int val);

LIBC_API  int			C_API_FUNC	tree_manager_get_child_value_i32		(mem_zone_ref_const_ptr parent_ref_ptr,unsigned int crc_name,unsigned int *value);
LIBC_API  int			C_API_FUNC	tree_manager_get_child_data_ptr			(mem_zone_ref_const_ptr p_node_ref,unsigned int crc_name,mem_ptr *data_ptr);
LIBC_API  int			C_API_FUNC	tree_manager_get_child_value_4uc		(mem_zone_ref_const_ptr p_node_ref,unsigned int crc_name,vec_4uc_t value);
LIBC_API  int			C_API_FUNC	tree_manager_get_child_value_rect		(const mem_zone_ref	*p_node_ref,unsigned int crc_name,struct gfx_rect *rect);
LIBC_API  int		    C_API_FUNC	tree_manager_get_child_type				(mem_zone_ref_const_ptr p_node_ref,unsigned int crc_name);
LIBC_API  int			C_API_FUNC	tree_manager_get_child_value_si32		(mem_zone_ref_const_ptr	p_node_ref,unsigned int crc_name,int *value);
LIBC_API  mem_ptr		C_API_FUNC	tree_manager_get_child_data				(mem_zone_ref_const_ptr p_node_ref,unsigned int crc_name,unsigned int ofset);
LIBC_API  int			C_API_FUNC	tree_manager_get_child_value_ptr		(mem_zone_ref_const_ptr	p_node_ref,unsigned int node_hash,unsigned int ofset,mem_ptr *value);

LIBC_API  int			C_API_FUNC	tree_manager_allocate_child_data		(mem_zone_ref_ptr parent_ref_ptr,char *name,unsigned int size);
LIBC_API  int			C_API_FUNC	tree_manager_set_child_value_i32		(mem_zone_ref_ptr	p_node_ref,const char *name,unsigned int value);
LIBC_API  int			C_API_FUNC	tree_manager_set_child_value_4uc		(mem_zone_ref_ptr	p_node_ref,const char *name,const vec_4uc_t value);
LIBC_API  int			C_API_FUNC	tree_manager_set_child_value_rect		(mem_zone_ref	*p_node_ref,const char *name,const struct gfx_rect *rect);
LIBC_API  int			C_API_FUNC	tree_manager_set_child_value_bool		(mem_zone_ref_ptr	p_node_ref,const char *name,unsigned int value);
LIBC_API  int			C_API_FUNC	tree_manager_set_child_value_si32		(mem_zone_ref_ptr	p_node_ref,const char *name,int value);
LIBC_API  int		    C_API_FUNC	tree_manager_set_child_value_ptr		(mem_zone_ref_ptr	p_node_ref,const char *name,mem_ptr value);
LIBC_API  int			C_API_FUNC	tree_manager_set_child_value_str		(mem_zone_ref_ptr	p_node_ref,const char *name,const char *str);
LIBC_API  int			C_API_FUNC	tree_manager_set_child_value_i64		(mem_zone_ref_ptr	p_node_ref,char *name,uint64_t value);
LIBC_API  int			C_API_FUNC	tree_manager_set_child_value_si64		(mem_zone_ref_ptr p_node_ref,char *name,int64_t value);

LIBC_API  void			C_API_FUNC	tree_manager_set_output					(int output);
LIBC_API  void 			C_API_FUNC	tree_manager_dump_node_rec				(mem_zone_ref_const_ptr node_ref,unsigned int rec_level,unsigned int max_rec);

LIBC_API  unsigned int	C_API_FUNC	 node_array_pop							(mem_zone_ref_ptr node_array,mem_zone_ref_ptr	node);
LIBC_API  unsigned int	C_API_FUNC	 node_array_get_free_element			(mem_zone_ref_ptr node_array,mem_zone_ref_ptr	node);
LIBC_API  void			C_API_FUNC	 init_node_array						(mem_zone_ref_ptr node_array,unsigned int n_elems,const char *name,unsigned int type,unsigned int size_alloc);

LIBC_API  int			C_API_FUNC tree_manager_create_obj					(struct obj_array_t *obj_array);
LIBC_API  int			C_API_FUNC tree_manager_create_obj_array			(struct obj_array_t *obj_array);
LIBC_API  void			C_API_FUNC tree_manager_add_obj						(struct obj_array_t *obj_array,const char *name,unsigned int type);
LIBC_API  void			C_API_FUNC tree_manager_add_obj_array				(struct obj_array_t *obj_array,const char *name,unsigned int type);
LIBC_API  void			C_API_FUNC tree_manager_add_obj_str_val				(struct obj_array_t *obj_array,const char *name,const char *str);
LIBC_API  void			C_API_FUNC tree_manager_add_obj_int_val				(struct obj_array_t *obj_array,const char *name,unsigned int value);
LIBC_API  void			C_API_FUNC tree_manager_add_obj_sint_val			(struct obj_array_t *obj_array,const char *name,int value);
LIBC_API  void			C_API_FUNC tree_manager_end_obj_array				(struct obj_array_t *obj_array);
LIBC_API  void			C_API_FUNC tree_manager_end_obj						(struct obj_array_t *obj_array);
LIBC_API  void			C_API_FUNC tree_manager_free_obj_array				(struct obj_array_t *obj_array);



void 			tree_manager_init						();
int				tree_manager_json_loadb					(const char *buffer, size_t buflen,mem_zone_ref_ptr result);
int	C_API_FUNC	tree_manager_free_node_array			(mem_zone_ref_ptr childs_ref_ptr);

#define NODE_HASH(name) calc_crc32_c(name,32)

