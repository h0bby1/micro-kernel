
#ifndef KERNEL_API	
#define KERNEL_API	C_IMPORT
#endif


#define KERN_API_FUNC	ASM_API_FUNC



struct bios_drive_t
{
	unsigned char   bios_id;
	unsigned char   is_lba;
	unsigned short  sector_size;
	unsigned int	cylinders;
	unsigned int	sectors;
	unsigned int	heads;
	unsigned int	hdXsecs;
};

struct bios_memory_map_t
{
	unsigned int mem_base_low;
	unsigned int mem_base_hi;

	unsigned int mem_size_low;
	unsigned int mem_size_hi;

	unsigned int type;
};

typedef unsigned char gdt_entry_t	[8];
typedef gdt_entry_t *gdt_entry_ptr_t;

struct gdt_t
{
	unsigned short		limit;
	gdt_entry_ptr_t		addr;
};

typedef struct gdt_t *gdt_ptr;

struct gdt_entry_desc_t
{
	unsigned int  base;
	unsigned int  limit;
	unsigned char access;
	unsigned char flags;
	unsigned int ofset;
};

struct kern_mod_sec_t
{
	unsigned int	addr;
	unsigned int	size;


};

struct kern_mod_t
{
	unsigned int	mod_hash;
	unsigned char	n_secs;
	unsigned char	n_funcs;
	unsigned short	func_ofs;
	unsigned int	mod_base_addr;
	unsigned int	string_table_addr;
};

struct kern_mod_fn_t
{
	unsigned int	func_hash;
	unsigned int	func_addr;
	unsigned int	section_start_addr;
	unsigned int	string_idx;
};


KERNEL_API int					KERN_API_FUNC draw_car_c						(const char *string);
KERNEL_API int					KERN_API_FUNC draw_ncar_c						(const char *string,unsigned int n);
KERNEL_API int					KERN_API_FUNC draw_dword_c						(unsigned int dword);
KERNEL_API int					KERN_API_FUNC draw_byte_c						(unsigned int byte);
KERNEL_API void					KERN_API_FUNC set_text_color_c					(unsigned char color);
KERNEL_API unsigned char		KERN_API_FUNC get_text_color_c					();
KERNEL_API void					KERN_API_FUNC delay1_4ms_c						();
KERNEL_API void					KERN_API_FUNC delay_x_ms_c						(unsigned int ms);
KERNEL_API void					KERN_API_FUNC get_system_time_c					(unsigned int *time);

KERNEL_API void			 		KERN_API_FUNC dump_ivt_c						();

KERNEL_API void			 		KERN_API_FUNC out_32_c							(unsigned short port,unsigned int);
KERNEL_API void					KERN_API_FUNC out_16_c							(unsigned short port,unsigned short);
KERNEL_API void		 			KERN_API_FUNC out_8_c							(unsigned short port,unsigned char);

KERNEL_API unsigned int 		KERN_API_FUNC in_32_c							(unsigned short port);
KERNEL_API unsigned short		KERN_API_FUNC in_16_c							(unsigned short port);
KERNEL_API unsigned char 		KERN_API_FUNC in_8_c							(unsigned short port);

KERNEL_API unsigned int 		KERN_API_FUNC 	rep_in_byte						(unsigned int port,void *buffer,unsigned int byte_cnt);
KERNEL_API unsigned int			KERN_API_FUNC 	rep_in_word						(unsigned int port,void *buffer,unsigned int word_cnt);
KERNEL_API unsigned int 		KERN_API_FUNC 	rep_in_dword					(unsigned int port,void *buffer,unsigned int dword_cnt);


KERNEL_API unsigned int 		KERN_API_FUNC 	rep_out_byte					(unsigned int port,void *buffer,unsigned int byte_cnt);
KERNEL_API unsigned int			KERN_API_FUNC 	rep_out_word					(unsigned int port,void *buffer,unsigned int word_cnt);
KERNEL_API unsigned int 		KERN_API_FUNC 	rep_out_dword					(unsigned int port,void *buffer,unsigned int dword_cnt);

KERNEL_API void	*				KERN_API_FUNC 	kernel_memory_map_c				(unsigned int size);

KERNEL_API unsigned int 		KERN_API_FUNC 	tpo_mod_imp_func_addr_c			(unsigned int mod_hash,unsigned int crc_func);
KERNEL_API unsigned int 		KERN_API_FUNC 	tpo_mod_add_func_addr_c			(unsigned int mod_hash,unsigned int crc_func,unsigned int func_addr);

KERNEL_API unsigned int 		 KERN_API_FUNC 	tpo_add_mod_c					(unsigned int mod_hash,unsigned int deco_type	,unsigned int string_table_addr);
KERNEL_API unsigned int 		 KERN_API_FUNC 	tpo_mod_add_section_c			(unsigned int mod_idx ,unsigned int section_addr,unsigned int section_size);
KERNEL_API struct kern_mod_fn_t	*KERN_API_FUNC 	tpo_mod_add_func_c				(unsigned int mod_idx ,unsigned int func_addr	,unsigned int func_type,unsigned int string_id);



KERNEL_API struct kern_mod_t		*KERN_API_FUNC 	tpo_get_mod_entry_hash_c		(unsigned int mod_hash);
KERNEL_API struct kern_mod_t		*KERN_API_FUNC 	tpo_get_mod_entry_idx_c			(unsigned int idx);
KERNEL_API struct kern_mod_sec_t	*KERN_API_FUNC 	tpo_get_mod_sec_idx_c			(unsigned int mod_idx,unsigned int sec_idx);

KERNEL_API struct kern_mod_fn_t	*KERN_API_FUNC 	tpo_get_fn_entry_idx_c			(unsigned int mod_hash,unsigned int idx_func); 
KERNEL_API struct kern_mod_fn_t *KERN_API_FUNC 	tpo_get_fn_entry_hash_c			(unsigned int mod_hash,unsigned int crc_func);
KERNEL_API struct kern_mod_fn_t *KERN_API_FUNC 	tpo_get_fn_entry_name_c			(unsigned int mod_idx,unsigned int mod_hash,unsigned int str_idx,unsigned int deco_type);
KERNEL_API unsigned int 		 KERN_API_FUNC	tpo_calc_exp_func_hash_c		(unsigned int mod_idx ,unsigned int string_id);
KERNEL_API unsigned int 		 KERN_API_FUNC	tpo_calc_exp_func_hash_name_c	(const char *func_name ,unsigned int deco_type);
KERNEL_API unsigned int 		 KERN_API_FUNC	tpo_calc_imp_func_hash_name_c	(const char *func_name ,unsigned int src_deco_type,unsigned int deco_type);

KERNEL_API int					KERN_API_FUNC 	setup_interupt_c				(unsigned int interupt, interupt_func_ptr func_ptr,unsigned int data);
KERNEL_API int					KERN_API_FUNC 	apic_enable_irq_c				(unsigned int input, unsigned int vector, unsigned int cpunum);
KERNEL_API int					KERN_API_FUNC 	init_lapic_c					();
KERNEL_API int					KERN_API_FUNC 	init_apic_c						();
KERNEL_API int					KERN_API_FUNC 	disable_apic_c					();
KERNEL_API int					KERN_API_FUNC 	get_interupt_mode_c				();

KERNEL_API unsigned char *  	KERN_API_FUNC 	create_code_stub				(unsigned int data,unsigned int addr);
KERNEL_API unsigned int			KERN_API_FUNC 	calc_crc32_c					(const char *string,unsigned int len);

KERNEL_API unsigned int			KERN_API_FUNC	bios_reset_drives_c				();
KERNEL_API unsigned int			KERN_API_FUNC	bios_get_boot_device_c			();
KERNEL_API unsigned int			KERN_API_FUNC	read_bios_disk_geom_c			(unsigned int drv_id,struct bios_drive_t *infos);

KERNEL_API unsigned int			KERN_API_FUNC	bios_read_sector_c				(struct bios_drive_t *infos,unsigned int sector_id);
KERNEL_API unsigned int			KERN_API_FUNC	do_bios_interupt_c			    (unsigned int itr);


KERNEL_API unsigned char *		KERN_API_FUNC	bios_get_sector_data_ptr_c		();

KERNEL_API unsigned int			KERN_API_FUNC	bios_get_vesa_info_c			();
KERNEL_API unsigned int			KERN_API_FUNC	bios_get_vga_info_c				();
KERNEL_API unsigned char *		KERN_API_FUNC	bios_get_vesa_info_ptr_c		();

KERNEL_API unsigned int			KERN_API_FUNC	bios_get_vesa_mode_info_c		(unsigned int idx);
KERNEL_API unsigned char *		KERN_API_FUNC	bios_get_vesa_mode_info_ptr_c	();

KERNEL_API unsigned int			KERN_API_FUNC	bios_set_vesa_mode_c			(unsigned int idx);
KERNEL_API unsigned int			KERN_API_FUNC	bios_set_vga_mode_c				(unsigned int idx);

KERNEL_API	void				KERN_API_FUNC	toggle_output_buffer_c			(unsigned int on);

KERNEL_API	void				KERN_API_FUNC	set_char_buffer_ofset_c			(unsigned int ofset);
KERNEL_API	unsigned int		KERN_API_FUNC	get_char_buffer_ofset_c			();



KERNEL_API unsigned int			KERN_API_FUNC	set_output_buffer_addr_c		(char *char_buffer,unsigned int *line_infos);
KERNEL_API const short   *		KERN_API_FUNC	get_output_buffer_line_c		(unsigned int line_idx,unsigned int *line_len);
KERNEL_API unsigned int			KERN_API_FUNC	get_num_output_buffer_line_c	();

KERNEL_API unsigned int			KERN_API_FUNC	bios_reset_mouse_c				();
KERNEL_API unsigned int			KERN_API_FUNC	bios_enable_mouse_c				();
KERNEL_API short *				KERN_API_FUNC	bios_get_mouse_status_c			();
KERNEL_API unsigned int			KERN_API_FUNC	set_mmx_protect_c				(unsigned int on);
KERNEL_API unsigned int			KERN_API_FUNC	bios_get_mem_map_c				(struct bios_memory_map_t **out_data);
KERNEL_API void*				KERN_API_FUNC	bios_get_edba_c					();
KERNEL_API void*				KERN_API_FUNC	acpi_get_rdsp_c					();




KERNEL_API	unsigned int		KERN_API_FUNC	get_gdt_ptr_c					(gdt_ptr out_gdt);
KERNEL_API	unsigned int		KERN_API_FUNC	set_gdt_ptr_c					(unsigned int size,void *entries);

KERNEL_API	void				KERN_API_FUNC	mask_interupts					(unsigned int on);
KERNEL_API	unsigned int		KERN_API_FUNC	pic_enable_irq_c				(unsigned int on);


KERNEL_API void			 		KERN_API_FUNC	next_task_c						();

KERNEL_API void			 		KERN_API_FUNC	dump_task_infos_c				();


KERNEL_API void			 		KERN_API_FUNC	pic_init_c						();
KERNEL_API unsigned int			KERN_API_FUNC	get_cpu_frequency_c				();
KERNEL_API void			 		KERN_API_FUNC	pic_set_irq_mask_c				(unsigned short mask);
KERNEL_API unsigned int			KERN_API_FUNC	compare_z_exchange_c			(unsigned int *data,unsigned int new_value);
KERNEL_API unsigned int			KERN_API_FUNC	fetch_add_c						(unsigned int *data,int new_value);
KERNEL_API unsigned int			KERN_API_FUNC	swap_nz_array_c					(unsigned int *first,unsigned int * last,unsigned int *value);

KERNEL_API unsigned int			KERN_API_FUNC	set_cpu_flags_c				(unsigned int flags);

KERNEL_API volatile unsigned int			cur_task_id;


typedef struct
{
	unsigned char step_rate_time;			//  specify byte 1; step-rate time, head unload time
	unsigned char head_load_time;			//  specify byte 2; head load time, DMA mode
	unsigned char disk_motor_shutoff_time;	//byte  timer ticks to wait before disk motor shutoff
	unsigned char bytes_per_sector;
	//0 - 128 bytes	2 - 512 bytes
	//1 - 256 bytes	3 - 1024 bytes

	unsigned char sectors_per_track;			//sectors per track (last sector number)
	unsigned char inter_block_gap_length;		//inter-block gap length/gap between sectors
	unsigned char data_length;					//, if sector length not specified
	unsigned char gap_length_between_sectors;	//for format
	unsigned char fill_byte;					//for formatted sectors
	unsigned char head_settle_time;				// in milliseconds
	unsigned char motor_startup_time;			// in eighths of a second
}bios_disk_base_table;

#define APIC_BASE 0x20
#define APIC_POLARITY_LOW (1<<13)
#define APIC_LEVEL_TRIGERING (1<<15)

