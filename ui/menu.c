#include <std_def.h>
#include <std_mem.h>
#include <kern.h>
#include <mem_base.h>
#include <lib_c.h>
#include <tree.h>
#include <sys/mem_stream.h>
#include <sys/tpo_mod.h>
#include <sys/file_system.h>
#include <sys/ctrl.h>

#include "../kernel/bus_manager/bus_drv.h"
#include "../graphic_base/filters/filters.h"
#include "gfx/graphic_object.h"
#include "gfx/graphic_base.h"


struct menu_style_t
{
int							 ctrl_p_x;
int							 ctrl_p_y;
unsigned int				 ctrl_width;
unsigned int				 ctrl_height;
unsigned int				 border;
unsigned int				 item_height;
unsigned int				 font_size_x,font_size_y;
char						 font_name[32];
vec_4uc_t					 txt_col;
vec_4uc_t					 bk_col;
};
extern unsigned int menu_kernel_log_id;

void compute_scroll(mem_zone_ref_ptr ctrl_data_node)
{
	mem_zone_ref				 item_list			={PTR_NULL};
	mem_zone_ref				 col_list			={PTR_NULL};
	mem_zone_ref				 style_node			={PTR_NULL};
	mem_zone_ref				 prop_node			={PTR_NULL};
	mem_zone_ref				 col				={PTR_NULL};
	const char					*prop_name;
	unsigned int				 x_ofs;
	unsigned int				 first_item;
	unsigned int				 total_item;
	unsigned int				 num_cols;
	unsigned int				 n_item_disp;
	unsigned int				 cnt;
	unsigned int				 ctrl_width,ctrl_height,item_height;
	unsigned int				 border;
	int							 ctrl_p_x,ctrl_p_y;
	gfx_rect_t					 scrl_rect;
	int							 ofs_y;
	unsigned int				 scroll_height;

	if(!tree_manager_get_child_value_i32	(ctrl_data_node,NODE_HASH("first_items")	,&first_item))return;
	if(!tree_manager_get_child_value_i32	(ctrl_data_node,NODE_HASH("total_items")	,&total_item))return;
	if(!tree_manager_get_child_value_i32	(ctrl_data_node,NODE_HASH("n_items_disp")	,&n_item_disp))return;
	if(total_item<=n_item_disp)return;

	if(!tree_node_find_child_by_name			(ctrl_data_node,"style",&style_node))return;
	

	ctrl_width=0;
	ctrl_height=0;
	item_height=0;
	border=0;
	ctrl_p_x=0;
	ctrl_p_y=0;

	
	cnt	=0;
	while(tree_manager_get_child_at(&style_node,cnt,&prop_node))
	{
		prop_name	=	tree_mamanger_get_node_name				(&prop_node);
		if(!strcmp_c(prop_name,"width"))
		{	
			tree_mamanger_get_node_dword(&prop_node,0,&ctrl_width);
		}
		if(!strcmp_c(prop_name,"height"))
		{	
			tree_mamanger_get_node_dword(&prop_node,0,&ctrl_height);
		}
		if(!strcmp_c(prop_name,"item_height"))
		{	
			tree_mamanger_get_node_dword(&prop_node,0,&item_height);
		}
		if(!strcmp_c(prop_name,"border"))
		{	
			tree_mamanger_get_node_dword(&prop_node,0,&border);
		}
		if(!strcmp_c(prop_name,"p_x"))
		{
			tree_mamanger_get_node_signed_dword(&prop_node,0,&ctrl_p_x);
		}
		if(!strcmp_c(prop_name,"p_y"))
		{
			tree_mamanger_get_node_signed_dword(&prop_node,0,&ctrl_p_y);
		}
		release_zone_ref(&prop_node);
		cnt++;
	}
	release_zone_ref(&style_node);



	num_cols	=	0;

	if(tree_node_find_child_by_type(ctrl_data_node,NODE_GFX_CTRL_DATA_COLUMN_LIST,&col_list,0))
	{
		x_ofs		=	0;
		
		while(tree_manager_get_child_at(&col_list,num_cols,&col))
		{
			unsigned int col_w;

			if(tree_node_find_child_by_type		(&col,NODE_GFX_STYLE,&style_node,0))
			{
				tree_manager_get_child_value_i32(&style_node,NODE_HASH("width")			,&col_w);
				release_zone_ref				(&style_node);

				x_ofs			+=	col_w+4;
			}
			
			num_cols++;
			release_zone_ref(&col);
		}
		release_zone_ref				(&col_list);
	}
	else
		x_ofs=ctrl_width;
	
	ofs_y				=	16;
	if(num_cols>0)ofs_y	+=	item_height+border*2+4;

	if(tree_manager_find_child_node					(ctrl_data_node,NODE_HASH("head_item"),NODE_GFX_CTRL_ITEM,PTR_NULL))
		ofs_y+=item_height+border*2;

	scroll_height=0;

	if(tree_node_find_child_by_type(ctrl_data_node,NODE_GFX_CTRL_ITEM_LIST,&item_list,0))
	{
		size_t	num_items;

		num_items	=	tree_manager_get_node_num_children	(&item_list);
		release_zone_ref									(&item_list);

		scroll_height		=	num_items*(item_height+border*2);
	}
	scroll_height = ctrl_height - ofs_y;

	scrl_rect.pos[0]	=	ctrl_p_x+x_ofs;
	scrl_rect.pos[1]	=	ctrl_p_y+ofs_y;

	scrl_rect.size[0]	=	10;
	scrl_rect.size[1]	=	scroll_height;


	tree_manager_set_child_value_rect(ctrl_data_node,"scroll rect",&scrl_rect);

	scrl_rect.size[0]	=	8;
	scrl_rect.size[1]	=	(n_item_disp*scroll_height)/total_item;
	if(scrl_rect.size[1]<4)scrl_rect.size[1]=4;

	scrl_rect.pos[0]	=	ctrl_p_x+x_ofs+1;
	scrl_rect.pos[1]	=	ctrl_p_y+ofs_y+(first_item*(scroll_height-scrl_rect.size[1]))/total_item;


	tree_manager_set_child_value_rect(ctrl_data_node,"scroll button",&scrl_rect);

	
}

OS_API_C_FUNC(int) ctrl_compute	(mem_zone_ref_ptr ctrl_data_node)
{
	mem_zone_ref				 style_node={PTR_NULL};
	mem_zone_ref				 history={PTR_NULL};
	mem_zone_ref				 entry={PTR_NULL};
	mem_zone_ref				 col={PTR_NULL};
	mem_zone_ref				 item_list={PTR_NULL};
	mem_zone_ref				 rect_node={PTR_NULL};
	mem_zone_ref				 col_list={PTR_NULL};
	char						*font_name;
	unsigned int				 first_item;
	unsigned int 				 total_item,n_item_disp;
	unsigned int				 cnt,x_ofs;
	unsigned int				 border;
	unsigned int				 ctrl_width,ctrl_height,item_height;

	unsigned int				 num_cols,need_recomp;
	unsigned int				 font_size_x,font_size_y,rect_width;
	vec_4uc_t					 txt_col,bk_col;
	int							 p_y,ret;
	int							 ctrl_p_x;
	int							 ctrl_p_y;


	need_recomp=0;
	ret=tree_manager_get_child_value_i32(ctrl_data_node,NODE_HASH("need_recompute"),&need_recomp);
	if((ret==1)&&(need_recomp==0))return 1;


	ctrl_width	=	0;
	ctrl_height	=	0;
	item_height	=	0;
	border		=	0;
	font_size_x	=	0;
	font_size_y	=	0;
	ctrl_p_x	=	0;
	ctrl_p_y	=	0;
	txt_col[0]	=	255;
	txt_col[1]	=	255;
	txt_col[2]	=	255;
	txt_col[3]	=	255;

	bk_col[0]	=	255;
	bk_col[1]	=	255;
	bk_col[2]	=	255;
	bk_col[3]	=	255;
	if(tree_node_find_child_by_name	(ctrl_data_node,"style",&style_node))
	{
		struct node_hash_val_t		  node_hash_val_list[16];

		node_hash_val_list[0].crc	=NODE_HASH("border");
		node_hash_val_list[0].data	=&border;
		node_hash_val_list[1].crc	=NODE_HASH("p_x");
		node_hash_val_list[1].data	=&ctrl_p_x;
		node_hash_val_list[2].crc	=NODE_HASH("p_y");
		node_hash_val_list[2].data	=&ctrl_p_y;
		node_hash_val_list[3].crc	=NODE_HASH("width");
		node_hash_val_list[3].data	=&ctrl_width;
		node_hash_val_list[4].crc	=NODE_HASH("height");
		node_hash_val_list[4].data	=&ctrl_height;
		node_hash_val_list[5].crc	=NODE_HASH("text_color");
		node_hash_val_list[5].data	=txt_col;
		node_hash_val_list[6].crc	=NODE_HASH("bk_color");
		node_hash_val_list[6].data	=bk_col;
		node_hash_val_list[7].crc	=NODE_HASH("font_size_x");
		node_hash_val_list[7].data	=&font_size_x;
		node_hash_val_list[8].crc	=NODE_HASH("font_size_y");
		node_hash_val_list[8].data	=&font_size_y;
		node_hash_val_list[9].crc	=NODE_HASH("font_name");
		node_hash_val_list[9].data	=&font_name;
		node_hash_val_list[10].crc	=NODE_HASH("item_height");
		node_hash_val_list[10].data	=&item_height;
		node_hash_val_list[11].crc	=0;
		node_hash_val_list[11].data	=PTR_NULL;

		tree_node_read_childs			(&style_node,node_hash_val_list);
		release_zone_ref				(&style_node);
	}

	p_y	=	ctrl_p_y;
	

	if(!tree_manager_find_child_node	(ctrl_data_node,NODE_HASH("event list"),NODE_GFX_EVENT_LIST,PTR_NULL))
		tree_manager_add_child_node		(ctrl_data_node,"event list"		,NODE_GFX_EVENT_LIST,PTR_NULL);
	
	copy_zone_ref					(&history,ctrl_data_node);

	while(history.zone!=PTR_NULL)
	{	
		unsigned int ctrl_id;
		mem_zone_ref	previous_item={PTR_NULL};

		tree_manager_get_child_value_i32	(&history,NODE_HASH("id"),&ctrl_id);
		gfx_create_set_ctrl_event			(ctrl_data_node,"mouse_1_release"	,PTR_NULL,"history_id",ctrl_id);

		if(!tree_manager_find_child_node	(&history,NODE_HASH("previous item"),0,&previous_item))break;
		tree_manager_get_child_at			(&previous_item,0,&history);
		release_zone_ref					(&previous_item);
	}
	release_zone_ref					(&history);
	p_y+=16;






	num_cols	=	0;

	if(tree_node_find_child_by_type(ctrl_data_node,NODE_GFX_CTRL_DATA_COLUMN_LIST,&col_list,0))
	{
		x_ofs		=	0;
		
		while(tree_manager_get_child_at(&col_list,num_cols,&col))
		{
			gfx_rect_t	rect;

			rect.pos[0]		=	ctrl_p_x+x_ofs;
			rect.pos[1]		=	p_y;

			rect.size[0]	=	ctrl_width-x_ofs;
			rect.size[1]	=	item_height;


			if(tree_node_find_child_by_type		(&col,NODE_GFX_STYLE,&style_node,0))
			{
				tree_manager_get_child_value_i32(&style_node,NODE_HASH("width")			,&rect.size[0]);
				tree_manager_get_child_value_i32(&style_node,NODE_HASH("height")		,&rect.size[1]);
				release_zone_ref				(&style_node);
			}


			gfx_create_set_ctrl_event			(ctrl_data_node,"mouse_1_release"	,&rect,"column_id",num_cols);

			x_ofs			+=	rect.size[0]+4;

			num_cols++;
			release_zone_ref(&col);
		}

		rect_width	=	x_ofs;

		p_y+=item_height+border*2+4;
		release_zone_ref				(&col_list);
	}
	else
		rect_width	=	ctrl_width;
	

	
	if(tree_manager_find_child_node					(ctrl_data_node,NODE_HASH("head_item"),NODE_GFX_CTRL_ITEM,PTR_NULL))
		p_y+=item_height+border*2;
	
	if(tree_node_find_child_by_type(ctrl_data_node,NODE_GFX_CTRL_ITEM_LIST,&item_list,0))
	{
		cnt=0;
		while(tree_manager_get_child_at(&item_list,cnt,&entry)==1)
		{
			gfx_rect_t		rect;
			unsigned int	item_id=0;

			rect.pos[0]		=	ctrl_p_x;
			rect.pos[1]		=	p_y;

			rect.size[0]	=	rect_width;
			rect.size[1]	=	item_height;

			tree_manager_get_child_value_i32	(&entry,NODE_HASH("item_id")		,&item_id);
			gfx_create_set_ctrl_event			(ctrl_data_node,"mouse_1_release"	,&rect,"item_id",item_id);
			
     		p_y				+=	item_height+border*2;
			
			release_zone_ref				(&entry);
			cnt++;
		}
		release_zone_ref				(&item_list);
	}
	




	tree_manager_get_child_value_i32	(ctrl_data_node,NODE_HASH("first_items")	,&first_item);
	tree_manager_get_child_value_i32	(ctrl_data_node,NODE_HASH("total_items")	,&total_item);
	tree_manager_get_child_value_i32	(ctrl_data_node,NODE_HASH("n_items_disp")	,&n_item_disp);

	kernel_log(menu_kernel_log_id,"ctrl compute first : ");
	writestr_fmt(" %d, total : %d, disp : %d\n",first_item,total_item,n_item_disp);


	compute_scroll							 (ctrl_data_node);
	
	if(total_item>n_item_disp)
	{
		gfx_rect_t				scrl_rect;

		tree_manager_get_child_value_rect	(ctrl_data_node,NODE_HASH("scroll rect"),&scrl_rect);
		gfx_create_set_ctrl_event			(ctrl_data_node,"mouse_1_click"	,&scrl_rect,"scroll_in",0);

		ctrl_width			+=	10;
	}

	if(tree_node_find_child_by_name		(ctrl_data_node,"rect",&rect_node))
	{
		tree_manager_write_node_signed_dword	(&rect_node,0	,ctrl_p_x);
		tree_manager_write_node_signed_dword	(&rect_node,4	,ctrl_p_y);
		tree_manager_write_node_dword			(&rect_node,8	,ctrl_width+border*2);
		tree_manager_write_node_dword			(&rect_node,12	,ctrl_height+border*2);
		release_zone_ref						(&rect_node);
	}

	tree_manager_set_child_value_i32(ctrl_data_node,"need_recompute",0);
	tree_manager_set_child_value_i32(ctrl_data_node,"initialized"	,1);
	tree_manager_set_child_value_i32(ctrl_data_node,"need_rebuild"	,1);

	

	return 1;

}

unsigned int create_ctrl_node_from_data(mem_zone_ref_ptr data_node,mem_zone_ref_ptr new_ctrl)
{
	char				path[256];
	char				m_name[128];
	char				fs_name[32];
	char				bus_name[32];
	struct obj_array_t	ctrl_items;
	mem_zone_ref		file_out		={PTR_NULL};
	mem_zone_ref		item_data		={PTR_NULL};
	mem_zone_ref		my_dev			={PTR_NULL};
	mem_zone_ref		item_list		={PTR_NULL};
	mem_zone_ref		header_node		={PTR_NULL};
	mem_zone_ref		tree_node		={PTR_NULL};
	mem_zone_ref		tree_node_child_list={PTR_NULL};
	mem_zone_ref_ptr	tree_node_child;

	size_t				num_childs;
	unsigned int 		bus_id,bus_idx,node_type;
	unsigned int 		new_ctrl_id,type,area_id,total_items;
	unsigned int		n,n_tot;
	unsigned int		n_childs;
	int					ret;
	new_ctrl_id		=0;

	if(data_node->zone==PTR_NULL)return 0;

	type		=	tree_mamanger_get_node_type(data_node);

	switch(type)
	{
		case NODE_GFX_CTRL:
			copy_zone_ref						(new_ctrl,data_node);
		break;
		/*
		case NODE_TREE_NODE_DESC:
			tree_manager_get_child_value_ptr		(data_node,NODE_HASH("zone_ptr"),0,&tree_node.zone);
			
			gfx_ctrl_new_row						(&ctrl_items,"{p_x:4,p_y:4,width:380,height:150,border:1}","name:label{width:64,bk_color:0xFF00FF00},type:type{width:64,bk_color:0xFF00FF00},value:value{width:64,bk_color:0xFF00FF00},childs:childs{width:64,bk_color:0xFF00FF00},nref:nref{width:28,bk_color:0xFF00FF00}");
		
			n_childs=0;
			if(tree_manager_get_first_child		(&tree_node,&tree_node_child_list,&tree_node_child))
			{
				while(tree_node_child->zone!=PTR_NULL)
				{
					if(n_childs<3)
					{
						node_type							=tree_mamanger_get_node_type				(tree_node_child);
						strcpy_s								(m_name,32,tree_mamanger_get_node_name		(tree_node_child));

						tree_manager_get_node_str				(tree_node_child,0,path,256,16);
						num_childs	=	tree_manager_get_node_num_children		(tree_node_child);


						gfx_ctrl_add_item						(&ctrl_items,m_name,m_name,n_childs+1);
						
						tree_manager_add_obj_int_val			(&ctrl_items,"item_idx"	,n_childs);
						tree_manager_add_obj_int_val			(&ctrl_items,"type"		,node_type);
						tree_manager_add_obj_str_val			(&ctrl_items,"value"	,path);
						tree_manager_add_obj_int_val			(&ctrl_items,"childs"	,num_childs);
						tree_manager_add_obj_int_val			(&ctrl_items,"nref"		,get_zone_numref(tree_node_child));

						
					}
					if(!tree_manager_get_next_child(&tree_node_child_list,&tree_node_child))break;

					n_childs++;
				}

			}
			if(gfx_ctrl_create_object					(&ctrl_items,"menu"		,"ctrl node desc",new_ctrl,3))
			{
				node_type								=tree_mamanger_get_node_type				(&tree_node);
				num_childs	=	tree_manager_get_node_num_children		(&tree_node);
				strcpy_s								(m_name,32,tree_mamanger_get_node_name		(&tree_node));
				tree_manager_get_node_str				(&tree_node,0,path,256,16);


				tree_manager_add_child_node				(new_ctrl,"head_item",NODE_GFX_CTRL_ITEM,&header_node);
				tree_manager_set_child_value_str		(&header_node,"label"	,m_name);
				tree_manager_set_child_value_i32		(&header_node,"type"	,node_type);
				tree_manager_set_child_value_str		(&header_node,"value"	,path);
				tree_manager_set_child_value_i32		(&header_node,"childs"	,num_childs);
				tree_manager_set_child_value_i32		(&header_node,"nref"	,get_zone_numref(&tree_node));
				release_zone_ref						(&header_node);

				tree_manager_set_child_value_ptr		(new_ctrl,"zone_ptr"	,tree_node.zone);
				tree_manager_set_child_value_i32		(new_ctrl,"data_type"	,type);
				tree_manager_set_child_value_i32		(new_ctrl,"first_items"	,0);
				tree_manager_set_child_value_i32		(new_ctrl,"total_items"	,num_childs);
			}
				
		break;

		case NODE_TREE_AREA_DESC:
			tree_manager_get_child_value_i32		(data_node,NODE_HASH("area_id"),&area_id);
			
			total_items	= get_tree_area_zone_list	(area_id,&ctrl_items	,0,4,"{p_x:4,p_y:4,width:380,height:150,border:1}");

			if(gfx_ctrl_create_object					(&ctrl_items,"menu"		,"memory nodes",new_ctrl,4))
			{
				if(tree_node_find_child_by_type		(new_ctrl,NODE_GFX_CTRL_ITEM_LIST,&item_list,0))
				{
					mem_zone_ref		item_list_ref			={PTR_NULL};
					mem_zone_ref_ptr	item;
					mem_ptr				node_zone;
					

					if(tree_manager_get_first_child(&item_list,&item_list_ref,&item))
					{
						while(item->zone!=PTR_NULL)
						{
							mem_zone_ref	node_desc_node={PTR_NULL};
							unsigned int	item_id=0,addr_val;

							tree_manager_get_child_value_i32	(item,NODE_HASH("zone_ptr")	,&addr_val);
							tree_manager_get_child_value_i32	(item,NODE_HASH("item_id")	,&item_id);

							node_zone	=	uint_to_mem			(addr_val);

							tree_manager_create_node			("node",NODE_TREE_NODE_DESC ,&node_desc_node);
							tree_manager_set_child_value_ptr	(&node_desc_node,"zone_ptr",node_zone);

							gfx_ctrl_add_item_data				(new_ctrl,item_id,&node_desc_node);

							release_zone_ref					(&node_desc_node);

							if(!tree_manager_get_next_child(&item_list_ref,&item))break;
						}
					}

					release_zone_ref					(&item_list);
				}


				tree_manager_set_child_value_i32		(new_ctrl,"area_id"		,area_id);
				tree_manager_set_child_value_i32		(new_ctrl,"data_type"	,type);
				tree_manager_set_child_value_i32		(new_ctrl,"first_items"	,0);
				tree_manager_set_child_value_i32		(new_ctrl,"total_items"	,total_items);
			}


		break;


		case NODE_MEM_AREA_DESC:
			
			tree_manager_get_child_value_i32		(data_node,NODE_HASH("area_id"),&area_id);
			
			total_items	= get_mem_area_zone_list	(area_id,&ctrl_items	,0,4,"{p_x:4,p_y:4,width:380,height:150,border:1}");

			if(gfx_ctrl_create_object					(&ctrl_items,"menu"		,"memory zones",new_ctrl,4))
			{
				tree_manager_set_child_value_i32		(new_ctrl,"area_id"		,area_id);
				tree_manager_set_child_value_i32		(new_ctrl,"data_type"	,type);
				tree_manager_set_child_value_i32		(new_ctrl,"first_items"	,0);
				tree_manager_set_child_value_i32		(new_ctrl,"total_items"	,total_items);
			}

		break;
		
		case NODE_MEM_AREA_LIST:

			total_items	= get_mem_area_list		(&ctrl_items,0,4,"{p_x:4,p_y:4,width:380,height:150,border:1}");
			if(gfx_ctrl_create_object				(&ctrl_items,"menu","memory areas",new_ctrl,4))
			{
				if(tree_node_find_child_by_type		(new_ctrl,NODE_GFX_CTRL_ITEM_LIST,&item_list,0))
				{
					mem_zone_ref		entry			={PTR_NULL};
					n=0;
					while(tree_manager_get_child_at(&item_list,n,&entry)==1)
					{
						mem_zone_ref	mem_area={PTR_NULL};
						unsigned int	area_id	=0xFFFFFFFF,item_id=0,type=0;

						tree_manager_get_child_value_i32	(&entry,NODE_HASH("area_id"),&area_id);
						tree_manager_get_child_value_i32	(&entry,NODE_HASH("type"),&type);
						tree_manager_get_child_value_i32	(&entry,NODE_HASH("item_id"),&item_id);

						if(type==2)
							tree_manager_create_node			("area",NODE_TREE_AREA_DESC ,&mem_area);
						else
							tree_manager_create_node			("area",NODE_MEM_AREA_DESC	,&mem_area);


						tree_manager_set_child_value_i32	(&mem_area,"area_id",area_id);
						gfx_ctrl_add_item_data				(new_ctrl,item_id,&mem_area);

						release_zone_ref					(&entry);
						release_zone_ref					(&mem_area);
						n++;
					}
				}

				tree_manager_set_child_value_i32		(new_ctrl,"first_items"	,0);
				tree_manager_set_child_value_i32		(new_ctrl,"total_items"	,total_items);
				tree_manager_set_child_value_i32		(new_ctrl,"data_type"	,type);
			}
		break;	
		*/
		case NODE_FILE_SYSTEM_PATH:

			
			gfx_ctrl_new_row					(&ctrl_items,"{p_x:4,p_y:4,width:380, item_height:16,height:150,border:1}","file name:label{width:64,bk_color:0xFF00FF00},file size:size{width:64,bk_color:0xFF00FF00},type:type{width:64,bk_color:0xFF00FF00}");
			tree_manager_get_child_value_str	(data_node,NODE_HASH("fs name"),fs_name,32,0);
			tree_manager_get_child_value_str	(data_node,NODE_HASH("path")   ,path,256,0);

			n_tot=1;
			n=0;
			while(file_system_get_dir_entry(fs_name,path,&file_out,0x02FFFFFF,n))
			{
				const char *f_name;
				
				f_name=tree_mamanger_get_node_name	(&file_out);

				

				gfx_ctrl_add_item					(&ctrl_items,f_name,f_name,n_tot);

				if(tree_mamanger_get_node_type		(&file_out)==NODE_FILE_SYSTEM_FILE)
				{
					tree_manager_add_obj_int_val		(&ctrl_items,"size",tree_manager_get_node_image_size(&file_out));
					tree_manager_add_obj_str_val		(&ctrl_items,"type","file");
				}
				else
				{
					tree_manager_add_obj_str_val		(&ctrl_items,"type","directory");
				}

				release_zone_ref					(&file_out);
				n++;
				n_tot++;
			}

			if(!strcmp_c(path,"/"))
				ret=gfx_ctrl_create_object		(&ctrl_items,"menu",fs_name,new_ctrl,4);
			else
				ret=gfx_ctrl_create_object		(&ctrl_items,"menu",strrchr_c(path,'/')+1,new_ctrl,4);

			if(ret)
			{
				n=0;
				while(file_system_get_dir_entry(fs_name,path,&file_out,0x02FFFFFF,n))
				{
					
					char m_path[256];

					strcpy_s		(m_path,256,path);
					strcat_s		(m_path,256,"/");
					strcat_s		(m_path,256,tree_mamanger_get_node_name(&file_out));
					

					tree_manager_create_node			("fs entry",NODE_FILE_SYSTEM_PATH,&item_data);
					tree_manager_set_child_value_str	(&item_data,"fs name",fs_name);
					tree_manager_set_child_value_str	(&item_data,"path" 	 ,m_path);
					gfx_ctrl_add_item_data				(new_ctrl,n+1,&item_data);
					
					release_zone_ref					(&item_data);
					release_zone_ref					(&file_out);
					n++;
				}

				tree_manager_set_child_value_i32	(new_ctrl,"first_items"	,0);
				tree_manager_set_child_value_i32	(new_ctrl,"total_items"	,n);
				tree_manager_set_child_value_i32	(new_ctrl,"data_type"	,type);
			}
		break;
		case NODE_FILE_SYSTEM_LIST:
			{
				file_system	*fs;

				
				gfx_ctrl_new								(&ctrl_items,"{p_x:4,p_y:4,width:380, item_height:16,height:150,border:1}");


				n=0;
				while((fs=file_system_get_at(n))!=PTR_NULL)
				{
					const char *name;
					name	=	tree_mamanger_get_node_name(&fs->root);
					gfx_ctrl_add_item						(&ctrl_items,name,name,n+1);
					n++;
				}
				
				if(gfx_ctrl_create_object					(&ctrl_items,"menu","fs list",new_ctrl,4))
				{
					n=0;
					while((fs=file_system_get_at(n))!=PTR_NULL)
					{
						
				
						tree_manager_create_node			("fs entry",NODE_FILE_SYSTEM_PATH,&item_data);
						tree_manager_set_child_value_str	(&item_data,"fs name",tree_mamanger_get_node_name(&fs->root));
						tree_manager_set_child_value_str	(&item_data,"path" 	 ,"/");
						gfx_ctrl_add_item_data				(new_ctrl,n+1,&item_data);
						release_zone_ref					(&item_data);

						n++;
					}
					tree_manager_set_child_value_i32	(new_ctrl,"first_items"	,0);
					tree_manager_set_child_value_i32	(new_ctrl,"total_items"	,n);
					tree_manager_set_child_value_i32	(new_ctrl,"data_type"	,type);
				}
			}

		break;
		case NODE_BUS_DEV_ROOT:
		
			tree_mamanger_get_node_dword				(data_node,0,&bus_id);
			bus_idx=bus_manager_get_bus_driver_idx		(bus_id);
			bus_manager_get_bus_driver_name				(bus_idx,bus_name,32);
			
			gfx_ctrl_new								(&ctrl_items,"{p_x:4,p_y:4,width:380,height:200, item_height:64,border:1}");
			
			n=0;
			while(tree_node_list_child_by_type	(data_node,NODE_BUS_DEVICE,&my_dev,n))
			{
				char				dev_name[32];
				char				v_name[32];
				char				p_name[64];
			
				unsigned int		dev_id;
				unsigned int 		vendor;
				unsigned int 		product;

				tree_manager_get_child_value_i32	(&my_dev,NODE_HASH("id") ,&dev_id);
				tree_manager_get_child_value_i32	(&my_dev,NODE_HASH("id vendor") ,&vendor);
				tree_manager_get_child_value_i32	(&my_dev,NODE_HASH("id product"),&product);


				if(bus_manager_read_bus_dev_string				(bus_id,vendor,product,v_name,32,p_name,64))
				{
					strcpy_s(dev_name	 ,32,v_name);
					strcat_s(dev_name	 ,32," ");
					strcat_s(dev_name	 ,32,p_name);
				}
				else
				{
					tree_manager_get_child_value_str		(&my_dev,NODE_HASH("name")		,dev_name,32,0);
				}
				gfx_ctrl_add_item				(&ctrl_items,"bus dev"		,dev_name,n+1);
				release_zone_ref				(&my_dev);

				n++;
			}

			strcpy_s							(m_name,128,"devices");

			if(gfx_ctrl_create_object				(&ctrl_items,"menu",m_name,new_ctrl,2))
			{
				tree_manager_set_child_value_i32	(new_ctrl,"first_items"	,0);
				tree_manager_set_child_value_i32	(new_ctrl,"total_items"	,n);
				tree_manager_set_child_value_i32	(new_ctrl,"data_type",type);
			}

			
			n = 0;
			while (tree_node_list_child_by_type(data_node, NODE_BUS_DEVICE, &my_dev, n))
			{
				unsigned int		dev_id;
				mem_zone_ref		icon = { PTR_NULL };

				tree_manager_get_child_value_i32(&my_dev, NODE_HASH("id"), &dev_id);

				if (bus_manager_get_bus_dev_icon(bus_id, dev_id, &icon))
				{
					gfx_ctrl_add_item_data(new_ctrl, n + 1, &icon);
					release_zone_ref(&icon);
				}

				release_zone_ref(&my_dev);

				n++;
			}

		break;
		default:
			return 0;
		break;

	}

	tree_manager_free_obj_array		(&ctrl_items);

	if(new_ctrl->zone==PTR_NULL)return 0;

	return 1;
	
}
OS_API_C_FUNC(int) ctrl_drag	   (mem_zone_ref_ptr ctrl_data_node,unsigned int obj_id,vec_2s_t drag_ofset)
{
	mem_zone_ref	style_node={PTR_NULL};
	unsigned int	scroll_start,item_height,total_item,n_item_disp;
	int				new_first,start_y;
	gfx_rect_t		scrl_rect;
	gfx_rect_t		but_rect;

	tree_manager_get_child_value_rect	(ctrl_data_node,NODE_HASH("scroll rect")	,&scrl_rect);
	tree_manager_get_child_value_rect	(ctrl_data_node,NODE_HASH("scroll button")	,&but_rect);
	tree_manager_get_child_value_i32	(ctrl_data_node,NODE_HASH("scroll_start")	,&scroll_start);
	tree_manager_get_child_value_i32	(ctrl_data_node,NODE_HASH("total_items")	,&total_item);
	tree_manager_get_child_value_i32	(ctrl_data_node,NODE_HASH("n_items_disp")	,&n_item_disp);

	item_height=16;

	if(tree_node_find_child_by_name			(ctrl_data_node,"style",&style_node)>0)
	{
		tree_manager_get_child_value_i32	(&style_node,NODE_HASH("item_height"),&item_height);
		release_zone_ref					(&style_node);
	}

	start_y								=		scroll_start*item_height;	
	new_first							=		((int)((start_y+drag_ofset[1])*total_item))/((int)(scrl_rect.size[1]-but_rect.size[1]));
	if(new_first<0)new_first=0;
	if((new_first+n_item_disp)>=total_item)new_first=total_item-n_item_disp;



	tree_manager_set_child_value_i32	(ctrl_data_node,"first_items"				,new_first);

	kernel_log(menu_kernel_log_id, "ctrl dragged ");
	writestr_fmt("[%d] %d->%d %d %d,%d \n",obj_id,scroll_start,new_first,item_height,drag_ofset[0],drag_ofset[1]);

	tree_manager_set_child_value_i32(ctrl_data_node, "need_recompute", 1);

	return 1;
}

OS_API_C_FUNC(int) ctrl_event	   (mem_zone_ref_ptr ctrl_data_node,mem_zone_ref_const_ptr event_node)
{
	
	scene_itr_t			scene_itr;
	mem_zone_ref		new_ctrl		={PTR_NULL};
	mem_zone_ref		data_node		={PTR_NULL};
	mem_zone_ref		main_scene		={PTR_NULL};
	unsigned int		ctrl_id;
	unsigned int 		col_id;
	unsigned int		hist_id;
	unsigned int		drag_id;
	unsigned int		item_id;
	
	gfx_itr_get_scene						(&scene_itr);

	hist_id=0;
	item_id=0;
	col_id=0;
	

	tree_manager_get_child_value_i32			(ctrl_data_node,NODE_HASH("id"),&ctrl_id);

	if(tree_manager_get_child_value_i32			(event_node,NODE_HASH("item_id")	,&item_id))
	{
		mem_zone_ref		item_list		={PTR_NULL};
		mem_zone_ref		item_node		={PTR_NULL};
		mem_zone_ref		item_data_node	={PTR_NULL};
		
		if(!tree_node_find_child_by_type		(ctrl_data_node,NODE_GFX_CTRL_ITEM_LIST,&item_list,0))
		{

			kernel_log(menu_kernel_log_id, "no ctrl item list \n");
			return 0;
		}
		if(!tree_find_child_node_by_id_name		(&item_list,NODE_GFX_CTRL_ITEM,"item_id"	,item_id,&item_node))
		{
			kernel_log(menu_kernel_log_id, "ctrl item [");
			writeint			(item_id,16);
			writestr			("] not found \n");
			release_zone_ref(&item_list);
			return 0;
		}
		if(!tree_manager_find_child_node		(&item_node,NODE_HASH("item_data"),NODE_GFX_CTRL_ITEM_DATA,&item_data_node))
		{
			kernel_log(menu_kernel_log_id, "ctrl item [");
			writeint			(item_id,16);
			writestr			("] data not found \n");

			release_zone_ref(&item_list);
			release_zone_ref(&item_node);
			return 0;
		}
		if(!tree_manager_get_child_at			(&item_data_node,0,&data_node))
		{
			kernel_log(menu_kernel_log_id, "ctrl item [");
			writeint			(item_id,16);
			writestr			("] data node not found \n");

			release_zone_ref(&item_list);
			release_zone_ref(&item_node);
			release_zone_ref(&item_data_node); 
			return 0;
		}

		release_zone_ref(&item_list);
		release_zone_ref(&item_node);
		release_zone_ref(&item_data_node); 
		
		//writestr_fmt							("item click %d %d \n",ctrl_id,item_id);
	}
	else if(tree_manager_get_child_value_i32	(event_node,NODE_HASH("history_id")	,&hist_id))
	{
		mem_zone_ref							item_data_node	={PTR_NULL};

		
		//writestr_fmt							("hist click %d %d \n",ctrl_id,hist_id);

		if(ctrl_id==hist_id)return 0;
		if(!tree_manager_find_child_node		(event_node,NODE_HASH("item_data"),NODE_GFX_CTRL_ITEM_DATA,&item_data_node)){return 0;}
		if(!tree_manager_get_child_at			(&item_data_node,0,&data_node)){release_zone_ref(&item_data_node); return 0;}
		release_zone_ref						(&item_data_node); 
	}
	else if(tree_manager_get_child_value_i32	(event_node,NODE_HASH("column_id")	,&col_id))
	{
		char									col_fld[32]={0};
		mem_zone_ref							item_list		={PTR_NULL};
		mem_zone_ref							col_list		={PTR_NULL};
		mem_zone_ref							col				={PTR_NULL};
		unsigned int							sort_dir=0;
		

		if(!tree_node_find_child_by_type		(ctrl_data_node,NODE_GFX_CTRL_ITEM_LIST,&item_list,0))return 0;
		if(!tree_node_find_child_by_type		(ctrl_data_node,NODE_GFX_CTRL_DATA_COLUMN_LIST,&col_list,0)){release_zone_ref(&item_list);return 0;}

		tree_manager_get_child_at				(&col_list,col_id,&col);

		tree_manager_get_child_value_str		(&col,NODE_HASH("field"),col_fld,32,0);
		tree_manager_get_child_value_i32		(&col,NODE_HASH("sort_dir"),&sort_dir);

		//writestr_fmt							("col click %d %d '%s' \n",ctrl_id,col_id,col_fld);

		tree_manager_sort_childs				(&item_list,col_fld,sort_dir);

		tree_manager_set_child_value_i32		(&col,"sort_dir",(sort_dir^1));

		release_zone_ref						(&col);
		release_zone_ref						(&col_list);
		release_zone_ref						(&item_list);

		tree_manager_set_child_value_i32		(ctrl_data_node,"sort_col",col_id);
		tree_manager_set_child_value_i32		(ctrl_data_node,"need_recompute",1);

		return 1;
	}
	else if(tree_manager_get_child_value_i32	(event_node,NODE_HASH("scroll_in")	,&drag_id))
	{
		unsigned int						first_item;

		kernel_log(menu_kernel_log_id, " scroll click \n");

		tree_manager_get_child_value_i32	(ctrl_data_node,NODE_HASH("first_items")	,&first_item);
		tree_manager_set_child_value_i32	(ctrl_data_node,"scroll_start"				,first_item);
		gfx_set_dragged_id					(ctrl_id,drag_id);
		gfx_set_dragged_id					(ctrl_id,drag_id);
		return 1;
	}
	
	if(!tree_manager_get_ancestor_by_type		(ctrl_data_node	,NODE_GFX_SCENE			,&main_scene))
	{
		kernel_log(menu_kernel_log_id, "could not find root scene \n");
		return 0;
	}

	if(create_ctrl_node_from_data			(&data_node,&new_ctrl))
	{
		
		unsigned int	new_ctrl_id;
				
		new_ctrl_id	=	scene_itr.swap_ctrl	(&main_scene,ctrl_id,&new_ctrl);

		if(new_ctrl_id)
		{
			
			mem_zone_ref		previous_item	={PTR_NULL};
			kernel_log(menu_kernel_log_id, "new ctrl added ");
			writestr_fmt("%d \n",new_ctrl_id);
		
			if(hist_id==0)
			{
				mem_zone_ref		new_obj			={PTR_NULL};

				scene_itr.get_obj				(&main_scene,new_ctrl_id,&new_obj);
				

				if(tree_manager_find_child_node	(&new_obj,NODE_HASH("previous item"),0,&previous_item))
					tree_remove_children		(&previous_item);
				else
					tree_manager_add_child_node	(&new_obj,"previous item",0,&previous_item);

				tree_manager_node_add_child		(&previous_item,&new_ctrl);
				release_zone_ref				(&previous_item);
				
				tree_manager_set_child_value_i32(&new_obj,"need_recompute",1);
				release_zone_ref				(&new_obj);
			}
			else
			{
				/*
				mem_zone_ref		history		={PTR_NULL};
				copy_zone_ref	(&history,&new_ctrl);

				while(history.zone!=PTR_NULL)
				{	
					unsigned int	hist_ctrl_id;
					mem_zone_ref	previous_item={PTR_NULL},previous_item_data={PTR_NULL},prev_history={PTR_NULL};

					tree_manager_get_child_value_i32	(&history,NODE_HASH("id"),&hist_ctrl_id);
					if(hist_ctrl_id==hist_id)break;

					if(!tree_manager_find_child_node	(&history,NODE_HASH("previous item"),0,&previous_item))break;
					tree_manager_get_child_at			(&previous_item,0,&prev_history);
					release_zone_ref					(&previous_item);
					

					copy_zone_ref						(&history,&prev_history);
					release_zone_ref					(&prev_history);
				}
				*/
			}
		}
		release_zone_ref	(&new_ctrl);
		release_zone_ref	(&data_node);
	}

	release_zone_ref	(&main_scene);

	return 1;
}


void render_ctrl_item	(scene_itr_t	*scene_itr, mem_zone_ref_ptr	ctrl_data_node,mem_zone_ref_ptr	ctrl_item,const struct menu_style_t *style,int p_y)
{
	struct obj_array_t			 obj_ar;
	mem_zone_ref		ctrl_scene			={PTR_NULL};
	mem_zone_ref		col_list			={PTR_NULL};	
	mem_zone_ref		col_list_ref={PTR_NULL};
	mem_zone_ref_ptr	col;
	gfx_rect_t			rect;
	char				ctrl_label[32];
	

	tree_node_find_child_by_type	(ctrl_data_node,NODE_GFX_CTRL_DATA_COLUMN_LIST	,&col_list	,0);
	tree_node_find_child_by_type	(ctrl_data_node,NODE_GFX_SCENE					,&ctrl_scene,0);


	if(tree_manager_get_first_child(&col_list,&col_list_ref,&col))
	{
		unsigned int		x_ofs;

		x_ofs			=	0;

		while(col->zone!=PTR_NULL)
		{
			char ctrl_field[32];
			char data_str[256];
			unsigned int int_base;
			mem_zone_ref	style_node={PTR_NULL};
			vec_4uc_t		bk_col;

			int_base=16;

			rect.pos[0]		=	style->ctrl_p_x+x_ofs;
			rect.pos[1]		=	p_y;

			rect.size[0]	=	style->ctrl_width-x_ofs;
			rect.size[1]	=	style->item_height;

			if(tree_node_find_child_by_type		(col,NODE_GFX_STYLE,&style_node,0))
			{
				tree_manager_get_child_value_i32	(&style_node,NODE_HASH("width")		,&rect.size[0]);
				tree_manager_get_child_value_i32	(&style_node,NODE_HASH("height")	,&rect.size[1]);

				tree_manager_get_child_value_4uc	(&style_node,NODE_HASH("bk_color")	,bk_col);
				tree_manager_get_child_value_i32	(&style_node,NODE_HASH("int_base")	,&int_base);

				release_zone_ref					(&style_node);
			}

			gfx_create_rect_style				(&obj_ar,&rect,bk_col);

			if(obj_ar.char_buffer.zone!=PTR_NULL)
			{
				scene_itr->add_rect					(&ctrl_scene,get_zone_ptr(&obj_ar.char_buffer,0));
				tree_manager_free_obj_array			(&obj_ar);
			}

			tree_manager_get_child_value_str	(col,NODE_HASH("field")	,ctrl_field,32,0);

			if(tree_manager_get_child_value_str	(ctrl_item,NODE_HASH(ctrl_field),data_str,256,int_base))
			{
				tree_manager_create_obj				(&obj_ar);
				tree_manager_add_obj				(&obj_ar				,PTR_NULL,0);

				tree_manager_add_obj_int_val		(&obj_ar,"pos"			,2);
				tree_manager_add_obj_sint_val		(&obj_ar,"p_x"			,4);
				tree_manager_add_obj_sint_val		(&obj_ar,"p_y"			,0);
				
				tree_manager_add_obj_int_val		(&obj_ar,"width"		,rect.size[0]);
				tree_manager_add_obj_int_val		(&obj_ar,"height"		,rect.size[1]);

				tree_manager_add_obj_str_val		(&obj_ar,"font_name"	,"Flama");
				tree_manager_add_obj_int_val		(&obj_ar,"font_size_x"	,style->font_size_x);
				tree_manager_add_obj_int_val		(&obj_ar,"font_size_y"	,style->font_size_y);
				tree_manager_add_obj_int_val		(&obj_ar,"text_color"	,*((unsigned int *)(style->txt_col)));
				tree_manager_end_obj				(&obj_ar);
				if(obj_ar.char_buffer.zone!=PTR_NULL)
				{
					unsigned int new_text_id;
					mem_zone_ref field_node={PTR_NULL};
					new_text_id		=	scene_itr->add_text					(&ctrl_scene,data_str,get_zone_ptr(&obj_ar.char_buffer,0));

					tree_node_find_child_by_name							(ctrl_item,ctrl_field,&field_node);
					tree_manager_set_child_value_i32						(&field_node,"ctrl_id",new_text_id);
					release_zone_ref										(&field_node);
					tree_manager_free_obj_array								(&obj_ar);
				}	
			}
			x_ofs	+=	rect.size[0]+4;
			if(!tree_manager_get_next_child(&col_list_ref,&col))break;
		}
	}
	else
	{
		mem_zone_ref item_data_node = { PTR_NULL };

		tree_manager_get_child_value_str	(ctrl_item,NODE_HASH("label")	,ctrl_label,32,0);


		rect.pos[0]		=	style->ctrl_p_x;
		rect.pos[1]		=	p_y;

		rect.size[0]	=	style->ctrl_width;
		rect.size[1]	=	style->item_height;


		gfx_create_rect_style				(&obj_ar,&rect,style->bk_col);
		if(obj_ar.char_buffer.zone!=PTR_NULL)
		{
			scene_itr->add_rect					(&ctrl_scene,get_zone_ptr(&obj_ar.char_buffer,0));
			tree_manager_free_obj_array			(&obj_ar);
		}

		
		if (tree_manager_find_child_node(ctrl_item, NODE_HASH("item_data"), NODE_GFX_CTRL_ITEM_DATA, &item_data_node))
		{
			mem_zone_ref data_node = { PTR_NULL };
			if (tree_manager_get_child_at(&item_data_node, 0, &data_node))
			{
				char icon_path[128];

				if (tree_manager_get_child_value_str(&data_node, NODE_HASH("iconpath"), icon_path, 128, 0))
				{
					scene_itr->add_image(&ctrl_scene, rect.pos[0], rect.pos[1], "isodisk", icon_path);
				}
				release_zone_ref(&data_node);
			}
			release_zone_ref(&item_data_node);
		}
		

		tree_manager_create_obj				(&obj_ar);
		tree_manager_add_obj				(&obj_ar				,PTR_NULL,0);
		tree_manager_add_obj_sint_val		(&obj_ar,"p_x"			,rect.pos[0]);
		tree_manager_add_obj_sint_val		(&obj_ar,"p_y"			,rect.pos[1]);
		tree_manager_add_obj_int_val		(&obj_ar,"width"		,rect.size[0]);
		tree_manager_add_obj_int_val		(&obj_ar,"height"		,rect.size[1]);
		tree_manager_add_obj_str_val		(&obj_ar,"font_name"	,"Flama");
		tree_manager_add_obj_int_val		(&obj_ar,"font_size_x"	,style->font_size_x);
		tree_manager_add_obj_int_val		(&obj_ar,"font_size_y"	,style->font_size_y);
		tree_manager_add_obj_int_val		(&obj_ar,"text_color"	,*((unsigned int *)(style->txt_col)));
		tree_manager_end_obj				(&obj_ar);

	
		//writestr_fmt("add text %x '%s' \n",obj_ar.char_buffer.zone,get_zone_ptr(&obj_ar.char_buffer,0));
		if(obj_ar.char_buffer.zone!=PTR_NULL)
		{
			scene_itr->add_text			(&ctrl_scene,ctrl_label,get_zone_ptr(&obj_ar.char_buffer,0));
			tree_manager_free_obj_array(&obj_ar);
		}
	}
    

	release_zone_ref	(&col_list);
	release_zone_ref	(&ctrl_scene);
}

OS_API_C_FUNC(int) init(mem_zone_ref_ptr p)
{
	menu_kernel_log_id = get_new_kern_log_id("ui menu", 0x7);

	kernel_log(menu_kernel_log_id, " init menu \n");

	return 1;
}

OS_API_C_FUNC(struct obj_array_t *)create_text_obj(const struct menu_style_t *style_s,unsigned int first)
{
	struct obj_array_t *obj_ar;

	obj_ar = malloc_c(sizeof(struct obj_array_t));

	tree_manager_create_obj(obj_ar);
	tree_manager_add_obj(obj_ar, PTR_NULL, 0);

	tree_manager_add_obj_str_val(obj_ar, "font_name", style_s->font_name);
	tree_manager_add_obj_int_val(obj_ar, "font_size_x", style_s->font_size_x);
	tree_manager_add_obj_int_val(obj_ar, "font_size_y", style_s->font_size_y);
	tree_manager_add_obj_int_val(obj_ar, "text_color", *((unsigned int *)(style_s->txt_col)));
	if (first)
	{
		tree_manager_add_obj_int_val(obj_ar, "pos", 0);
		tree_manager_add_obj_sint_val(obj_ar, "p_x", style_s->ctrl_p_x);
		tree_manager_add_obj_sint_val(obj_ar, "p_y", style_s->ctrl_p_y);
	
	}
	else
	{
		tree_manager_add_obj_int_val(obj_ar, "pos", 1);
		tree_manager_add_obj_sint_val(obj_ar, "p_x", 4);
		tree_manager_add_obj_sint_val(obj_ar, "p_y", 0);
	}
	tree_manager_end_obj(obj_ar);

	return obj_ar;
}


OS_API_C_FUNC(int) ctrl_get_render_objs(mem_zone_ref_ptr ctrl_data_node)
{
	scene_itr_t					 scene_itr;
	struct obj_array_t			 obj_ar;
	mem_zone_ref				 style_node			={PTR_NULL};
	mem_zone_ref				 ctrl_item_list		={PTR_NULL};
	mem_zone_ref				 history			={PTR_NULL};
	mem_zone_ref				 ctrl_scene			={PTR_NULL};
	mem_zone_ref				 header_node		={PTR_NULL};
	mem_zone_ref				 col_list			={PTR_NULL};	
	mem_zone_ref				 ev_lst				={PTR_NULL};
	mem_zone_ref				 h_list[32]			={PTR_NULL};
	mem_zone_ref				 item_list			={PTR_NULL};
	mem_zone_ref				 tree_node			={PTR_NULL};
	mem_zone_ref				 tree_node_child_list={PTR_NULL};
	mem_zone_ref_ptr			 tree_node_child;
	mem_zone_ref_ptr			 ctrl_item;
	int							 ret;
	unsigned int				 num_cols,need_rebuild,need_recomp;
	unsigned int				 l_x,n_h,first,init,n;
	unsigned int				 first_item;
	unsigned int				 total_item;
	unsigned int				 n_item_disp;
	unsigned int				 r_mode;
	unsigned int				scrl_but_id;
	struct menu_style_t			style_s;
	int							 p_y;

	ret=tree_manager_get_child_value_i32(ctrl_data_node,NODE_HASH("need_recompute"),&need_recomp);
	if((!ret)||(need_recomp==1))return 1;

	init=0;
	tree_manager_get_child_value_i32		(ctrl_data_node,NODE_HASH("initialized")	,&init);
	if(init!=1)return 1;

	r_mode=0;
	tree_manager_get_child_value_i32		(ctrl_data_node,NODE_HASH("render_mode")	,&r_mode);

	

	if(!tree_node_find_child_by_type	(ctrl_data_node,NODE_GFX_CTRL_ITEM_LIST			,&item_list,0))	return 0;
	tree_node_find_child_by_type		(ctrl_data_node,NODE_GFX_CTRL_DATA_COLUMN_LIST	,&col_list,0);


	if(!tree_node_find_child_by_type		(ctrl_data_node,NODE_GFX_SCENE,&ctrl_scene,0))
		tree_manager_add_child_node			(ctrl_data_node,"scene"	,NODE_GFX_SCENE,&ctrl_scene);

	gfx_itr_get_scene					(&scene_itr);

	first_item	=	0;
	n_item_disp	=	4;

	
	style_s.ctrl_width	=	0;
	style_s.ctrl_height	=	0;
	style_s.item_height	=	0;
	style_s.border		=	0;
	style_s.font_size_x	=	0;
	style_s.font_size_y	=	0;
	style_s.ctrl_p_x	=	0;
	style_s.ctrl_p_y	=	0;
	style_s.txt_col[0]	=	255;
	style_s.txt_col[1]	=	255;
	style_s.txt_col[2]	=	255;
	style_s.txt_col[3]	=	255;
	strcpy_s(style_s.font_name, 32, "Flama");

	style_s.txt_col[0]	=	0;
	style_s.txt_col[1]	=	0;
	style_s.txt_col[2]	=	0;
	style_s.txt_col[3]	=	255;

	if(tree_node_find_child_by_name	(ctrl_data_node,"style",&style_node))
	{
		struct node_hash_val_t		  node_hash_val_list[16];

		node_hash_val_list[0].crc	=NODE_HASH("border");
		node_hash_val_list[0].data	=&style_s.border;
		node_hash_val_list[1].crc	=NODE_HASH("p_x");
		node_hash_val_list[1].data	=&style_s.ctrl_p_x;
		node_hash_val_list[2].crc	=NODE_HASH("p_y");
		node_hash_val_list[2].data	=&style_s.ctrl_p_y;
		node_hash_val_list[3].crc	=NODE_HASH("width");
		node_hash_val_list[3].data	=&style_s.ctrl_width;
		node_hash_val_list[4].crc	=NODE_HASH("height");
		node_hash_val_list[4].data	=&style_s.ctrl_height;
		node_hash_val_list[5].crc	=NODE_HASH("text_color");
		node_hash_val_list[5].data	=style_s.txt_col;
		node_hash_val_list[6].crc	=NODE_HASH("bk_color");
		node_hash_val_list[6].data	=style_s.bk_col;
		node_hash_val_list[7].crc	=NODE_HASH("font_size_x");
		node_hash_val_list[7].data	=&style_s.font_size_x;
		node_hash_val_list[8].crc	=NODE_HASH("font_size_y");
		node_hash_val_list[8].data	=&style_s.font_size_y;
		/*
		node_hash_val_list[9].crc	=NODE_HASH("font_name");
		node_hash_val_list[9].data	=&style_s.font_name;
		*/
		node_hash_val_list[10].crc	=NODE_HASH("item_height");
		node_hash_val_list[10].data	=&style_s.item_height;
		node_hash_val_list[11].crc	=0;
		node_hash_val_list[11].data	=PTR_NULL;

		tree_node_read_childs			(&style_node,node_hash_val_list);
		release_zone_ref				(&style_node);
	}

	tree_manager_get_child_value_i32	(ctrl_data_node,NODE_HASH("first_items")	,&first_item);
	tree_manager_get_child_value_i32	(ctrl_data_node,NODE_HASH("total_items")	,&total_item);
	tree_manager_get_child_value_i32	(ctrl_data_node,NODE_HASH("n_items_disp")	,&n_item_disp);

	need_rebuild=0;
	ret=tree_manager_get_child_value_i32	(ctrl_data_node,NODE_HASH("need_rebuild"),&need_rebuild);
	if ((ret == 1) && (need_rebuild == 0))
		return 1;

	scene_itr.clear				(&ctrl_scene);

	p_y	=	style_s.ctrl_p_y;
	l_x	=	0;
	n_h	=	0;

	tree_manager_node_dup_one	(ctrl_data_node,&history);

	while(history.zone!=PTR_NULL)
	{	
		mem_zone_ref	previous_item		={PTR_NULL};
		copy_zone_ref						(&h_list[n_h++],&history);

		if(!tree_manager_find_child_node	(&history,NODE_HASH("previous item"),0,&previous_item))break;
		tree_manager_get_child_at			(&previous_item,0,&history);
		release_zone_ref					(&previous_item);
	}
	release_zone_ref					(&history);

	first=1;
	
	//display history breadcrumb
	
	while(n_h--)
	{
		mem_zone_ref		event_item={PTR_NULL};
		mem_zone_ref		new_text={PTR_NULL};
		unsigned int		ctrl_hist_id,ctrl_id;
		const char			*name;
		struct obj_array_t *obj_ar_ptr;
		struct menu_style_t ms = style_s;

		if (!first)
		{
			struct obj_array_t *obj_ar_ptr2;
			unsigned int ctrl_id2, f = 0;
			

			obj_ar_ptr2 = create_text_obj(&ms, 0);
			ctrl_id2 = scene_itr.add_text(&ctrl_scene, " > ", get_zone_ptr(&obj_ar_ptr2->char_buffer, 0));
			tree_manager_free_obj_array(obj_ar_ptr2);
			free_c(obj_ar_ptr2);
		}

		ms = style_s;

		ms.txt_col[0] = 0;
		ms.txt_col[1] = 0;
		ms.txt_col[2] = 255;
		ms.txt_col[3] = 255;

		
		obj_ar_ptr = create_text_obj(&ms, first);
		name = tree_mamanger_get_node_name(&h_list[n_h]);

		if (obj_ar_ptr->char_buffer.zone != PTR_NULL)
		{
			ctrl_id = scene_itr.add_text(&ctrl_scene, name, get_zone_ptr(&obj_ar_ptr->char_buffer, 0));
			tree_manager_free_obj_array(obj_ar_ptr);
		}
		else
			ctrl_id	=	0;
		
		free_c(obj_ar_ptr);

		first = 0;

		//copy history text label rect to history click event entry
		tree_manager_get_child_value_i32		(&h_list[n_h]	,NODE_HASH("id"),&ctrl_hist_id);

		tree_manager_find_child_node			(ctrl_data_node, NODE_HASH("event list"), NODE_GFX_EVENT_LIST, &ev_lst);

		if(tree_find_child_node_by_id_name		(&ev_lst		,NODE_GFX_EVENT,"history_id",ctrl_hist_id,&event_item))
		{
			mem_zone_ref						t_rect={PTR_NULL};
			mem_zone_ref						event_data={PTR_NULL};

			
			tree_remove_child_by_type			(&event_item,NODE_GFX_RECT);
			
			if(scene_itr.get_obj					(&ctrl_scene,ctrl_id,&new_text))
			{
				if(tree_manager_find_child_node		(&new_text,NODE_HASH("rect"),NODE_GFX_RECT,&t_rect))
				{
					tree_manager_node_add_child			(&event_item,&t_rect);
					release_zone_ref					(&t_rect);
				}
				release_zone_ref					(&new_text);
			}

			if(!tree_manager_find_child_node		(&event_item,NODE_HASH("item_data"),NODE_GFX_CTRL_ITEM_DATA,&event_data))
				tree_manager_add_child_node		(&event_item,"item_data",NODE_GFX_CTRL_ITEM_DATA,&event_data);
			else
				tree_remove_children				(&event_data);

			tree_manager_node_add_child			(&event_data,&h_list[n_h]);

			release_zone_ref					(&event_data);
			release_zone_ref					(&event_item);
		}
		release_zone_ref						(&h_list[n_h]);
	}
	
	release_zone_ref						(&ev_lst);
	
		
	p_y			+=	16;
	num_cols	=	0;
		
	if(tree_node_find_child_by_type(ctrl_data_node,NODE_GFX_CTRL_DATA_COLUMN_LIST,&col_list,0))
	{
		unsigned int	sort_col_id;
		unsigned int	x_ofs;
		mem_zone_ref	col	={PTR_NULL};

		sort_col_id =   0xFFFFFFFF;
		x_ofs		=	0;
		

		tree_manager_get_child_value_i32		(ctrl_data_node,NODE_HASH("sort_col"),&sort_col_id);

		while(tree_node_find_child_by_type(&col_list,NODE_GFX_CTRL_DATA_COLUMN,&col,num_cols))
		{
			char			col_label[32];
			gfx_rect_t		rect;
			tree_manager_get_child_value_str	(&col,NODE_HASH("label")	,col_label,32,0);


			if(sort_col_id==num_cols)
			{
				unsigned int sort_dir;
				tree_manager_get_child_value_i32	(&col,NODE_HASH("sort_dir")	,&sort_dir);

				if(sort_dir)
					strcat_s(col_label,32," ^");
				else
					strcat_s(col_label,32," v");
									
			}

			rect.pos[0]		=	style_s.ctrl_p_x+x_ofs;
			rect.pos[1]		=	p_y;

			rect.size[0]	=	style_s.ctrl_width-x_ofs;
			rect.size[1]	=	style_s.item_height;

			if(tree_node_find_child_by_type		(&col,NODE_GFX_STYLE,&style_node,0))
			{
				tree_manager_get_child_value_i32(&style_node,NODE_HASH("width")	,&rect.size[0]);
				tree_manager_get_child_value_i32(&style_node,NODE_HASH("height"),&rect.size[1]);
				release_zone_ref				(&style_node);
			}

			gfx_create_rect_style				(&obj_ar,&rect,style_s.bk_col);

			if(obj_ar.char_buffer.zone!=PTR_NULL)
			{
				scene_itr.add_rect					(&ctrl_scene,get_zone_ptr(&obj_ar.char_buffer,0));
				tree_manager_free_obj_array			(&obj_ar);
			}

			tree_manager_create_obj				(&obj_ar);
			tree_manager_add_obj				(&obj_ar				,PTR_NULL,0);

			tree_manager_add_obj_int_val		(&obj_ar,"pos"			,2);
			tree_manager_add_obj_sint_val		(&obj_ar,"p_x"			,4);
			tree_manager_add_obj_sint_val		(&obj_ar,"p_y"			,0);
			
			tree_manager_add_obj_int_val		(&obj_ar,"width"		,rect.size[0]);
			tree_manager_add_obj_int_val		(&obj_ar,"height"		,rect.size[1]);

			tree_manager_add_obj_str_val		(&obj_ar,"font_name"	,"Flama");
			tree_manager_add_obj_int_val		(&obj_ar,"font_size_x"	,style_s.font_size_x);
			tree_manager_add_obj_int_val		(&obj_ar,"font_size_y"	,style_s.font_size_y);
			tree_manager_add_obj_int_val		(&obj_ar,"text_color"	,*((unsigned int *)(style_s.txt_col)));
			tree_manager_end_obj				(&obj_ar);

			
			if(obj_ar.char_buffer.zone!=PTR_NULL)
			{
				scene_itr.add_text					(&ctrl_scene,col_label,get_zone_ptr(&obj_ar.char_buffer,0));
				tree_manager_free_obj_array		(&obj_ar);
			}
			x_ofs	+=	rect.size[0]+4;
			
			num_cols++;
			release_zone_ref(&col);
		}
	
		p_y+=style_s.item_height+style_s.border*2+4;
	}

	if(tree_manager_find_child_node					(ctrl_data_node,NODE_HASH("head_item"),NODE_GFX_CTRL_ITEM,&header_node))
	{
		render_ctrl_item(&scene_itr,ctrl_data_node,&header_node,&style_s,p_y);
		release_zone_ref(&header_node);

		p_y+=style_s.item_height+style_s.border*2;
	}

	if (!tree_manager_get_child_value_i32(ctrl_data_node, NODE_HASH("first_items"), &first_item))
		first_item = 0;

	n = 0;
	
	if(tree_manager_get_first_child		(&item_list,&ctrl_item_list,&ctrl_item))
	{
		while(ctrl_item->zone!=PTR_NULL)
		{
			if (n >= first_item)
			{
				render_ctrl_item(&scene_itr, ctrl_data_node, ctrl_item, &style_s, p_y);
				p_y += style_s.item_height + style_s.border * 2;
			}
			if(!tree_manager_get_next_child(&ctrl_item_list,&ctrl_item))break;

			n++;
		}
	}
	
	if(total_item>n_item_disp)
	{
		vec_4uc_t				c1={0x66,0x66,0x66,0xFF},c2={0xAA,0xAA,0xAA,0xFF};
		unsigned int			scrl_but_id;
		gfx_rect_t				scrl_rect;


		tree_manager_get_child_value_rect(ctrl_data_node,NODE_HASH("scroll rect"),&scrl_rect);

		gfx_create_rect_style	(&obj_ar,&scrl_rect,c1);
		if(obj_ar.char_buffer.zone!=PTR_NULL)
		{
			scene_itr.add_rect			(&ctrl_scene,get_zone_ptr(&obj_ar.char_buffer,0));
			tree_manager_free_obj_array	(&obj_ar);
		}
		tree_manager_get_child_value_rect(ctrl_data_node,NODE_HASH("scroll button"),&scrl_rect);

		gfx_create_rect_style	(&obj_ar,&scrl_rect,c2);
		if(obj_ar.char_buffer.zone!=PTR_NULL)
		{
			scrl_but_id	=	scene_itr.add_rect			(&ctrl_scene,get_zone_ptr(&obj_ar.char_buffer,0));
			tree_manager_free_obj_array					(&obj_ar);

			tree_manager_set_child_value_i32			(ctrl_data_node,"scroll ctrl id",scrl_but_id);
		}
	}
	tree_manager_set_child_value_i32(ctrl_data_node,"need_rebuild",0);
	

	release_zone_ref	(&item_list);
	release_zone_ref	(&col_list);
	release_zone_ref	(&ctrl_scene);
	
	

	return 1;
}


