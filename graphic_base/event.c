#define GFX_API C_EXPORT
#include <std_def.h>
#include <std_mem.h>
#include <kern.h>
#include <mem_base.h>

#include <lib_c.h>
#include <tree.h>
#include <sys/mem_stream.h>
#include <sys/file_system.h>
#include <sys/tpo_mod.h>
#include <sys/task.h>
#include <sys/ctrl.h>

#include "filters/filters.h"
#include "gfx/graphic_object.h"
#include "gfx/graphic_base.h"
#include "gfx/graphic_render.h"
#include "../kernel/bus_manager/bus_drv.h"
#include <ft2build.h>
#include FT_FREETYPE_H

#include "gfx_lib/scene.h"

#include "cursor.h"
#include "font.h"
#include "render.h"
#include "image.h"

gfx_render_container_ptr_t				get_container_by_id(unsigned int container_id);


extern unsigned int						max_z_order;
extern gfx_render_container_ptr_t		container_list_zone[1024];
extern unsigned int						container_list_count;





OS_API_C_FUNC(void)	gfx_set_dragged_id(unsigned int ctrl_id,unsigned int obj_id)
{
	struct	gfx_cursor_t	*cursor;

	cursor							=	get_zone_ptr(&cursor_ref,0);

	cursor->drag_start_pos[0]		=	cursor->cursor_pos[0];
	cursor->drag_start_pos[1]		=	cursor->cursor_pos[1];
	cursor->dragged_cont_id			=	cursor->mouse_over_container_id;
	cursor->dragged_ctrl_id			=	ctrl_id;
	cursor->dragged_ctrl_obj_id		=	obj_id;
}

int get_input_value(mem_zone_ref_const_ptr input_node,int *out_value)
{

	mem_zone_ref	dtype		={PTR_NULL};
	int				is_rel		=0;
	unsigned int	value;
	int				signed_value;

	if(tree_node_find_child_by_name			(input_node	,"DATA TYPE"	,&dtype))
	{
		is_rel=tree_node_find_child_by_name		(&dtype		,"REL",PTR_NULL);
		release_zone_ref						(&dtype);
	}

	switch(tree_manager_get_child_type(input_node,NODE_HASH("last value")))
	{
		case NODE_GFX_INT:
			if(tree_manager_get_child_value_i32		(input_node,NODE_HASH("last value"),&value))
			{
				if(is_rel)
					(*out_value)+=value;
				else
					(*out_value)=value;
			}

		break;
		
		case NODE_GFX_SIGNED_INT:		
			if(tree_manager_get_child_value_si32		(input_node,NODE_HASH("last value"),&signed_value))
			{
				if(is_rel)
					(*out_value)+=signed_value;
				else
					(*out_value)=signed_value;
			}
		break;
	}

	return 1;
}

unsigned int gfx_add_container_event (gfx_render_container_t	*container,const char *event_name,mem_ptr event_data_ptr,unsigned int data_size)
{
	mem_zone_ref			new_event={PTR_NULL};

	
	if(!node_array_get_free_element(&container->event_array,&new_event))return 0;
	tree_manager_set_node_name		(&new_event,event_name);
	tree_manager_write_node_data	(&new_event,event_data_ptr,sizeof(unsigned int),data_size);
	release_zone_ref				(&new_event);
	return 1;
}
gfx_render_container_ptr_t get_container_by_id(unsigned int container_id)
{
	unsigned int n;

	if(container_id==0xFFFFFFFF)return PTR_NULL;


	for(n=0;n<container_list_count;n++)
	{
		if( ((gfx_render_container_ptr_t )(container_list_zone[n]))->id==container_id)return container_list_zone[n];
	}

	return PTR_NULL;
}
gfx_render_container_ptr_t gfx_find_container_pos	(const vec_2s_t	pos,vec_2s_t ofset)
{
	gfx_render_container_ptr_t	container_found;
	unsigned int				n;
	int							size[2];

	container_found	=	PTR_NULL;

	for(n=0;n<container_list_count;n++)
	{
		gfx_render_container_ptr_t	container=container_list_zone[n];

		size[0]=container->rect.size[0];
		size[1]=container->rect.size[1];

		if( (pos[0]>=container->rect.pos[0])&&
			(pos[1]>=container->rect.pos[1])&&
			(pos[0]<(container->rect.pos[0]+size[0]))&&
			(pos[1]<(container->rect.pos[1]+size[1]))
		  )
		{
			ofset[0]		=	pos[0]-container->rect.pos[0];
			ofset[1]		=	pos[1]-container->rect.pos[1];
			container_found	=	container;
		}
	}
	return container_found;
}
unsigned int gfx_container_top	(const vec_2s_t	pos,vec_2s_t ofset)
{
	gfx_render_container_ptr_t	container_found;
	unsigned int				n;
	int							size[2];

	container_found	=	PTR_NULL;

	for(n=0;n<container_list_count;n++)
	{
		gfx_render_container_ptr_t	container=container_list_zone[n];

		if(container->id!=0xFFFFFFFF)
		{
			size[0]=container->ctrl_rect[0].size[0];
			size[1]=container->ctrl_rect[0].size[1];
			//writestr_fmt("rect id [%d,%d] [%d,%d] [%d,%d]->[%d,%d] \n",pos[0],pos[1],container->rect.pos[0],container->rect.pos[1],rect.pos[0],rect.pos[1],rect.size[0],rect.size[1]);
			if( (pos[0]>= (container->rect.pos[0]+container->ctrl_rect[0].pos[0]))&&
				(pos[1]>= (container->rect.pos[1]+container->ctrl_rect[0].pos[1]))&&
				(pos[0]< ((container->rect.pos[0]+container->ctrl_rect[0].pos[0])+size[0]))&&
				(pos[1]< ((container->rect.pos[1]+container->ctrl_rect[0].pos[1])+size[1]))
			  )
			{
				ofset[0]	=	pos[0]-(container->rect.pos[0]);
				ofset[1]	=	pos[1]-(container->rect.pos[1]);
				return container->id;
			}

		}
	}
	return 0xFFFFFFFF;

/*

	gfx_render_container_t	*container,*container_last;
	//mem_zone_ref			 container_list={PTR_NULL};
	
	//if(!task_manager_get_container_list(&container_list))return 0;

	//container		=	get_zone_ptr(&container_list,0);
	//container_last	=	get_zone_ptr(&container_list,0xFFFFFFFF);

	while(container<container_last)
	{
		if(container->id!=0xFFFFFFFF)
		{
			//writestr_fmt("rect id [%d,%d] [%d,%d] [%d,%d]->[%d,%d] \n",pos[0],pos[1],container->rect.pos[0],container->rect.pos[1],rect.pos[0],rect.pos[1],rect.size[0],rect.size[1]);
			if( (pos[0]>= (container->rect.pos[0]+container->ctrl_rect[0].pos[0]))&&
				(pos[1]>= (container->rect.pos[1]+container->ctrl_rect[0].pos[1]))&&
				(pos[0]< ((container->rect.pos[0]+container->ctrl_rect[0].pos[0])+container->ctrl_rect[0].size[0]))&&
				(pos[1]< ((container->rect.pos[1]+container->ctrl_rect[0].pos[1])+container->ctrl_rect[0].size[1]))
			  )
			{
				ofset[0]	=	pos[0]-(container->rect.pos[0]);
				ofset[1]	=	pos[1]-(container->rect.pos[1]);
				return container->id;
			}

		}
		container++;
	}
	return 0xFFFFFFFF;
	*/
}


int	C_API_FUNC gfx_mouse_event			(device_type_t	dev_type,const mem_zone_ref	*hid_infos)
{
	mem_zone_ref			x_node		={PTR_NULL};
	mem_zone_ref			y_node		={PTR_NULL};
	mem_zone_ref			w_node		={PTR_NULL};
	mem_zone_ref			b1_node		={PTR_NULL};
	mem_zone_ref			b2_node		={PTR_NULL};
	gfx_render_container_t		*selected_container;
	unsigned int			button[4];
	int						pos[2];
	struct	gfx_cursor_t	*cursor;
	gfx_render_container_t	*cursor_mouse_over_container;
	gfx_render_container_t	*last_cursor_mouse_over_container;
	int						changed;
	

	
	tree_find_child_node_by_member_name(hid_infos,NODE_HID_INPUT,NODE_HID_USAGE,"X"			,	&x_node);
	tree_find_child_node_by_member_name(hid_infos,NODE_HID_INPUT,NODE_HID_USAGE,"Y"			,	&y_node);
	tree_find_child_node_by_member_name(hid_infos,NODE_HID_INPUT,NODE_HID_USAGE,"Wheel"		,	&w_node);
	tree_find_child_node_by_member_name(hid_infos,NODE_HID_INPUT,NODE_HID_USAGE,"button 1"	,	&b1_node);
	tree_find_child_node_by_member_name(hid_infos,NODE_HID_INPUT,NODE_HID_USAGE,"button 2"	,	&b2_node);

	


	//tree_manager_dump_node_rec(hid_infos, 0, 3);

	
	

	cursor					=	get_zone_ptr(&cursor_ref,0);
	cursor->last_acvity		=	system_time();
	cursor->visible			=	1;

	pos[0]					=	cursor->cursor_pos[0];
	pos[1]					=	cursor->cursor_pos[1];
	button[0]				=	cursor->buttons[0];
	button[1]				=	cursor->buttons[1];

	
	

	get_input_value(&x_node,&pos[0]);
	get_input_value(&y_node,&pos[1]);

	get_input_value(&b1_node,&button[0]);
	get_input_value(&b2_node,&button[1]);

	//writestr_fmt("buttons %d %d \n", button[0], button[1]);

	release_zone_ref(&x_node);
	release_zone_ref(&y_node);
	release_zone_ref(&w_node);
	release_zone_ref(&b1_node);
	release_zone_ref(&b2_node);



	changed=0;
	last_cursor_mouse_over_container=PTR_NULL;
	selected_container				=PTR_NULL;

	if(	
		(pos[0]!=cursor->cursor_pos[0])||
		(pos[1]!=cursor->cursor_pos[1])
		)
	{
		
		

		if(cursor->top_clicked_container_id!=0xFFFFFFFF)
		{
			gfx_render_container_t		*cursor_top_clicked_container;

			cursor_top_clicked_container	=   get_container_by_id		(cursor->top_clicked_container_id);
			if(cursor_top_clicked_container)
			{
				cursor_top_clicked_container->rect.pos[0]=cursor->cursor_pos[0]-cursor->top_sel_ofs[0];
				cursor_top_clicked_container->rect.pos[1]=cursor->cursor_pos[1]-cursor->top_sel_ofs[1];
			}
		}

		if(cursor->dragged_cont_id!=0xFFFFFFFF)
		{
			
			gfx_render_container_t		*drag_container;

			



			drag_container				=	get_container_by_id		(cursor->dragged_cont_id);

			
			if(drag_container!=PTR_NULL)
			{
				int			data[4];

				data[0]=cursor->cursor_pos[0]-cursor->drag_start_pos[0];
				data[1]=cursor->cursor_pos[1]-cursor->drag_start_pos[1];
				data[2]=cursor->dragged_ctrl_id;
				data[3]=cursor->dragged_ctrl_obj_id;

				gfx_add_container_event	(drag_container			,"item_drag",data,sizeof(int)*4);

				/*
				if(tree_node_find_child_by_id		(&drag_container->scene_ref,cursor->dragged_ctrl_id,&drag_ctrl))
				{
					gfx_ctrl_drag					(&drag_ctrl,cursor->dragged_ctrl_obj_id,drag_delta);
					release_zone_ref				(&drag_ctrl);
				}*/
			}

	
		}





		selected_container					=   gfx_find_container_pos	(pos,cursor->selected_ofset);

		if(selected_container!=PTR_NULL)
		{
			cursor->selected_ofset_ext[0]=cursor->selected_ofset[0];
			cursor->selected_ofset_ext[1]=cursor->selected_ofset[1];
		}

		cursor_mouse_over_container			=   get_container_by_id		(cursor->mouse_over_container_id);
		last_cursor_mouse_over_container	=	cursor_mouse_over_container;

		if(selected_container	!=	cursor_mouse_over_container)
		{

			if(cursor_mouse_over_container!=PTR_NULL)
				gfx_add_container_event(cursor_mouse_over_container	,"mouse_out",PTR_NULL,0);

			if(selected_container!=PTR_NULL)
			{
				gfx_add_container_event	(selected_container			,"mouse_over",cursor->selected_ofset_ext,sizeof(cursor->selected_ofset_ext));
				cursor->mouse_over_container_id=selected_container->id;
			}
			else
				cursor->mouse_over_container_id=0xFFFFFFFF;

		}
		else if(selected_container!=PTR_NULL)
		{
			gfx_add_container_event	(selected_container,"mouse_moved",cursor->selected_ofset_ext,sizeof(cursor->selected_ofset_ext));
		}



		changed=1;
		cursor->cursor_pos[0]		=	pos[0];
		cursor->cursor_pos[1]		=	pos[1];


	}

	
	cursor_mouse_over_container			=   get_container_by_id(cursor->mouse_over_container_id);
	if(button[0]!=cursor->buttons[0])
	{
		if(button[0])
		{
			cursor->top_clicked_container_id=gfx_container_top	(cursor->cursor_pos,cursor->top_sel_ofs);
			if(cursor->top_clicked_container_id!=0xFFFFFFFF)
			{
				 get_container_by_id(cursor->top_clicked_container_id)->z_order	=	max_z_order+1;
				 max_z_order													=	get_container_by_id(cursor->top_clicked_container_id)->z_order;
			}

			if(cursor_mouse_over_container!=PTR_NULL)
			{
				cursor_mouse_over_container	->z_order	=	max_z_order+1;
				max_z_order								=	cursor_mouse_over_container	->z_order;

				gfx_add_container_event(cursor_mouse_over_container	,"mouse_1_click"	,cursor->selected_ofset_ext,sizeof(cursor->selected_ofset_ext));
			}

		
		}
		else
		{
			cursor->dragged_cont_id				=	0xFFFFFFFF;
			cursor->dragged_ctrl_id				=	0xFFFFFFFF;
			cursor->dragged_ctrl_obj_id			=	0xFFFFFFFF;
			cursor->top_clicked_container_id	=	0xFFFFFFFF;

			if(cursor_mouse_over_container!=PTR_NULL)gfx_add_container_event(cursor_mouse_over_container	,"mouse_1_release"	,cursor->selected_ofset_ext,sizeof(cursor->selected_ofset_ext));

		
		}
		changed=1;
		cursor->buttons[0]=button[0];
		
	}
	
	if(selected_container!=PTR_NULL)
	{
		if((iabs_c(cursor->selected_ofset_ext[0]-selected_container->rect.size[0])<16))
		{
			cursor->image_id=GFX_CURSOR_MOVE_RIGHT;
		}
		else if(button[0])
			cursor->image_id	=	GFX_CURSOR_SELECTED;
		else
			cursor->image_id	=	GFX_CURSOR_DEFAULT;
			
	}
	else if(button[0])
		cursor->image_id	=	GFX_CURSOR_SELECTED;
	else
		cursor->image_id	=	GFX_CURSOR_DEFAULT;

	//writestr_fmt("cursor %d,%d\n",cursor->cursor_pos[0],cursor->cursor_pos[1]);

	

	return 1;

}


int	C_API_FUNC gfx_joystick_event(device_type_t	dev_type, const mem_zone_ref	*hid_infos)
{
	gfx_render_container_t  *selected_container= PTR_NULL;
	struct	gfx_cursor_t	*cursor;
	const char				*event_name;
	unsigned int			node_type;
	mem_zone_ref			val_node = { PTR_NULL }, input = { PTR_NULL };
	
	int						pos[2];
	mem_zone_ref			x_node = { PTR_NULL };
	mem_zone_ref			y_node = { PTR_NULL };
	
	
	tree_find_child_node_by_member_name(hid_infos, NODE_HID_INPUT, NODE_HID_USAGE, "X", &x_node);
	tree_find_child_node_by_member_name(hid_infos, NODE_HID_INPUT, NODE_HID_USAGE, "Y", &y_node);
	
#if 1
	

	val_node.zone = PTR_NULL;

	//if (tree_node_find_child_by_type(&input, NODE_HID_USAGE, &val_node, 0) == 0)return 0;

	//tree_manager_dump_node_rec(&val_node, 0, 3);

	cursor = get_zone_ptr(&cursor_ref, 0 * sizeof(struct gfx_cursor_t));


	get_system_time_c(&cursor->last_acvity);
	cursor->visible = 1;

	pos[0] = cursor->cursor_pos[0];
	pos[1] = cursor->cursor_pos[1];

	get_input_value(&x_node, &pos[0]);
	get_input_value(&y_node, &pos[1]);


	release_zone_ref(&x_node);
	release_zone_ref(&y_node);

	cursor->cursor_pos[0] = pos[0];
	cursor->cursor_pos[1] = pos[1];

	//selected_container = get_container_by_id(cursor->selected_container_id);


	//event_name = tree_mamanger_get_node_name(&val_node);
	//release_zone_ref(&val_node);
	/*
	if (tree_node_find_child_by_name(&input, "last value", &val_node) != 0)
	{
		int		value;

		node_type = tree_mamanger_get_node_type(&val_node);

		value = 0;

		if (node_type == NODE_GFX_INT)
		{
			tree_mamanger_get_node_dword(&val_node, 0, &value);
		}
		if (node_type == NODE_GFX_SIGNED_INT)
		{
			tree_mamanger_get_node_signed_dword(&val_node, 0, &value);
		}

		if (!strcmp_c(event_name, "button 1"))
		{

			cursor->buttons[0] = value;

			if (value == 0)
			{
				cursor->selected_container_id = 0xFFFFFFFF;
				selected_container = PTR_NULL;
			}
			else
			{
				selected_container = gfx_find_container_pos(cursor->cursor_pos, cursor->selected_ofset);

				if (selected_container != PTR_NULL)
				{
					struct gfx_rect rect;

					gfx_scene_get_extent(selected_container->scene_id, &rect);

					cursor->selected_ofset_ext[0] = cursor->cursor_pos[0] - rect.pos[0];
					cursor->selected_ofset_ext[1] = cursor->cursor_pos[1] - rect.pos[1];

					cursor->selected_container_id = selected_container->id;
				}
				else
				{
					selected_container = PTR_NULL;
					cursor->selected_container_id = 0xFFFFFFFF;
				}
			}

		}
		if (!strcmp_c(event_name, "button 2"))
		{
			cursor->buttons[1] = value;
		}


		if (!strcmp_c(event_name, "X"))
		{

			cursor->cursor_pos[0] = value;

			if (selected_container != PTR_NULL)
				selected_container->rect.pos[0] = (cursor->cursor_pos[0] - cursor->selected_ofset[0]);

		}

		if (!strcmp_c(event_name, "Y"))
		{

			cursor->cursor_pos[1] = value;

			if (selected_container != PTR_NULL)
				selected_container->rect.pos[1] = (cursor->cursor_pos[1] - cursor->selected_ofset[1]);
		}

		release_zone_ref(&val_node);
	}
	*/

	//writestr_fmt("cursor %d,%d\n", cursor->cursor_pos[0], cursor->cursor_pos[1]);

#endif

	return 1;

}
