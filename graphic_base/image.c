
#define GFX_DLL_FUNC DLL_EXPORT

#include <std_def.h>
#include "libpng-1.2.18/png.h"
#include <mem_base.h>
#include <tree.h>
#include <sys/mem_stream.h>
#include <sys/tpo_mod.h>
#include <sys/file_system.h>

#include "filters/filters.h"
#include <gfx/graphic_object.h>

#include "image.h"


extern unsigned int		kernel_log_id;

mem_zone_ref		images_list			={PTR_INVALID};
unsigned int		n_image_alloc		=0xFFFFFFFF;
unsigned int		n_images			=0xFFFFFFFF;
unsigned int		next_img_id			=0xFFFFFFFF;

int  init_images()
{

	next_img_id			=1;

	images_list.zone	=PTR_NULL;
	n_image_alloc		=16;
	n_images			=0;
	return allocate_new_zone		(0,sizeof(gfx_image_t)*n_image_alloc,&images_list);
}



	
gfx_image_t			*gfx_get_new_image(const char *fs_name,const char *image_path)
{
	mem_stream			file;
	gfx_image_t			*new_image;

	if((n_images+1)>=n_image_alloc)
	{
		n_image_alloc*=2;
		if(realloc_zone	(&images_list,n_image_alloc*sizeof(gfx_image_t))<0)
		{
			kernel_log(kernel_log_id,"could not realoc image list \n");
			return PTR_NULL;
		}
	}


	file.data.zone=PTR_NULL;
	
	if(file_system_open_file	(fs_name,image_path,&file)!=1)
	{
		kernel_log	(kernel_log_id,"cannot open image file '");
		writestr	(fs_name);
		writestr	(":");
		writestr	(image_path);
		writestr	("'\n");
		return PTR_NULL;
	}

	new_image					=	get_zone_ptr(&images_list,n_images*sizeof(gfx_image_t));
	new_image->image_id			=	++next_img_id;
	new_image->image_data.zone	=	PTR_NULL;
	new_image->line_table.zone	=	PTR_NULL;
	new_image->fd.resampler_ref.zone=PTR_NULL;

	if(gfx_load_png_image				(new_image,&file)==0)
	{
		kernel_log	(kernel_log_id,"could not load png image ");
		writestr	(image_path);
		writestr	("\n");
		mem_stream_close(&file);
		return PTR_NULL;
	}
	mem_stream_close				(&file);

	kernel_log	(kernel_log_id,"loaded image ");
	writeint	(new_image->image_id,16);
	writestr	(" w:");
	writeint	(new_image->width,10);
	writestr	(" h:");
	writeint	(new_image->height,10);
	writestr	("\n");

	n_images++;
	return new_image;
}

gfx_image_t	 *gfx_find_image	(unsigned int image_id)
{
	gfx_image_t			*image,*image_last;

	image			=	get_zone_ptr(&images_list,0);
	image_last		=	get_zone_ptr(&images_list,n_images*sizeof(gfx_image_t));

	while(image<image_last)
	{
		if(image->image_id==image_id)
			return image;

		image++;
	}
	return PTR_NULL;

}



png_voidp png_malloc_c(png_structp png_ptr, png_size_t size)
{
   if (png_ptr == NULL || size == 0)
      return (NULL);
   return calloc_c(size,1);
}

void png_free_c(png_structp png_ptr, png_voidp ptr)
{
	if (png_ptr == NULL || ptr == NULL)
      return ;
	
	free_c(ptr);
	return ;
}

void C_API_FUNC user_read_data(png_structp png_ptr,png_bytep data, png_size_t length)
{
	mem_stream  *file = (mem_stream  *)png_get_io_ptr(png_ptr);
	
	mem_stream_read	(file,data,length);
}




int gfx_allocate_image(gfx_image_t *img,unsigned int width,unsigned int height,gfx_pix_fmt_t pixfmt)
{
	unsigned int image_size;
	unsigned int a_height;
	unsigned int n;
	mem_ptr		 line_ptr;
	mem_ptr		 *line_table;
	
	

	switch(pixfmt)
	{
		default :img->line_size=0;break;
		case GFX_PIX_FORMAT_RGBA:
			img->pix_stride	=	32;
			img->line_size	=	(width*4);
		break;
	}
	if(img->line_size==0)return 0;

	if((img->line_size&0xF)!=0)
	{
		img->line_size=(img->line_size&0xFFFFFFF0)+16;
	}

	kernel_log	(kernel_log_id,"allocating image w:");
	writeint	(width,10);
	writestr	(" h:");
	writeint	(height,10);
	writestr	(" stride:");
	writeint	(img->line_size,10);
	writestr	("\n");

	img->pixfmt	=	pixfmt;
	img->width	=	width;
	img->height	=	height;
	img->fd.resampler_ref.zone=PTR_NULL;
	init_resampler	(&img->fd.resampler_ref);

	
	

	if((img->height&0xF)!=0)
	{
		a_height	=	((img->height&0xFFFFFFF0)+16);
	}
	else
	{
		a_height	=	img->height;
	}

	image_size		=	img->line_size*a_height;

	

	allocate_new_zone	(0,image_size						,&img->image_data);
	allocate_new_zone	(0,a_height*sizeof(mem_ptr *)	,&img->line_table);

	

	line_ptr	=	get_zone_ptr(&img->image_data,0);
	line_table	=	get_zone_ptr(&img->line_table,0);

	n=0;
	while(n<a_height)
	{
		line_table[n]	=	line_ptr;
		line_ptr		=	mem_add(line_ptr,img->line_size);
		n++;
	}

	return 1;
}


mem_ptr image_get_row			(gfx_image_t	*img,unsigned int row_y)
{
	mem_ptr		 *line_table;
	if(row_y>=img->height)return NULL;

	line_table	=	get_zone_ptr(&img->line_table,0);

	return line_table[row_y];
}





int gfx_load_png_image(gfx_image_t	*img,mem_stream *file)
{
	unsigned int	is_alpha;
	int				iy,ix;
	int				width, height;
	png_byte		color_type;
	png_byte		bit_depth;
	png_structp		 png_ptr;
	png_infop		info_ptr;
	int				number_of_passes;
	png_color		*palette=NULL;
	int				num=255;
	png_color_16    *color=NULL;
	unsigned char   *trnsp=NULL;
	int				ntr;
	png_bytep		*row_pointers;
	unsigned char	*file_data;

	file_data=get_zone_ptr(&file->data,file->buf_ofs);



	if (png_sig_cmp((unsigned char *)file_data, 0, 8))return 0;

	/* initialize stuff */
	png_ptr			= png_create_read_struct_2(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL,NULL,png_malloc_c,png_free_c);
	if (!png_ptr){return 0;}


	info_ptr = png_create_info_struct(png_ptr);
	if (!info_ptr){return 0;}
	/*if (setjmp(png_jmpbuf(png_ptr))){return false;}*/

	png_set_read_fn		(png_ptr, file, user_read_data);
	png_read_info		(png_ptr, info_ptr);

	width				= info_ptr->width;
	height				= info_ptr->height;
	color_type			= info_ptr->color_type;
	bit_depth			= info_ptr->bit_depth;

	gfx_allocate_image		(img,width,height,GFX_PIX_FORMAT_RGBA);

	number_of_passes	= png_set_interlace_handling(png_ptr);
	
	png_read_update_info(png_ptr, info_ptr);


	/* read file */
	/*if (setjmp(png_jmpbuf(png_ptr))){return false;}*/

	row_pointers = (png_bytep*) calloc_c(sizeof(png_bytep) * height,1);
	
	for (iy=0; iy<height; iy++)
		row_pointers[iy] = (png_byte*) calloc_c(info_ptr->rowbytes,1);

	png_read_image(png_ptr, row_pointers);
	ntr=0;

	/*
	kernel_log	(kernel_log_id,"png num channels ");
	writeint	(info_ptr->channels,10);
	writestr	("\n");
	*/


	switch(info_ptr->channels)
	{
		case 1:
			
			png_get_PLTE(png_ptr, info_ptr, &palette,&num);
			png_get_tRNS(png_ptr, info_ptr, &trnsp ,&ntr,&color);

			if(ntr>0)
			{
				is_alpha=1;
			}

			for (iy=0; iy<height; iy++)
			{
				unsigned char *cptr=image_get_row(img,iy);
				for (ix=0; ix<width; ix++)
				{
					int		istran=0;
					int		n;
					unsigned char c=row_pointers[iy][ix];
					cptr[ix*4+0]=palette[c].blue;
					cptr[ix*4+1]=palette[c].green;
					cptr[ix*4+2]=palette[c].red;

					for(n=0;n<ntr;n++)
					{
						if(trnsp[n]==c)istran=1;
					}

					if(istran)
						cptr[ix*4+3]=0;
					else
						cptr[ix*4+3]=255;
				}
			}
		break;
		case 3:
			for (iy=0; iy<height; iy++)
			{
				unsigned char *cptr=image_get_row(img,iy);
				for (ix=0; ix<width; ix++)
				{
					int		istran=0;
					int		n;

					cptr[ix*4+0]=row_pointers[iy][ix*3+2];
					cptr[ix*4+1]=row_pointers[iy][ix*3+1];
					cptr[ix*4+2]=row_pointers[iy][ix*3+0];

					for(n=0;n<ntr;n++)
					{
						if( (color[n].red   == row_pointers[iy][ix*3+0])&&
							(color[n].green == row_pointers[iy][ix*3+1])&&
							(color[n].blue  == row_pointers[iy][ix*3+2]))
								istran=1;
					}

					if(istran)
						cptr[ix*4+3]=0;
					else
						cptr[ix*4+3]=255;
				}
			}
		break;
		case 4:
			for (iy=0; iy<height; iy++)
			{
				unsigned char *cptr=image_get_row(img,iy);
				for (ix=0; ix<width; ix++)
				{
					cptr[ix*4+0]=row_pointers[iy][ix*4+2];
					cptr[ix*4+1]=row_pointers[iy][ix*4+1];
					cptr[ix*4+2]=row_pointers[iy][ix*4+0];
					cptr[ix*4+3]=row_pointers[iy][ix*4+3];
				}
			}
			is_alpha=1;
		break;
	}
	return 1;
}