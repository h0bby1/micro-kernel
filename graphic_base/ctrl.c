#define GFX_API C_EXPORT
#include <std_def.h>
#include <std_mem.h>
#include <kern.h>
#include <mem_base.h>
#include <lib_c.h>
#include <tree.h>
#include <sys/mem_stream.h>
#include <sys/file_system.h>
#include <sys/tpo_mod.h>
#include <sys/task.h>

#include "filters/filters.h"
#include "gfx/graphic_object.h"
#include "gfx/graphic_base.h"
#include "../kernel/bus_manager/bus_drv.h"

#include "ctrl.h"

extern unsigned int kernel_log_id;
mem_zone_ref		ctrl_tpo_mod_list	=	{PTR_INVALID};



unsigned int gfx_ctrl_compute(mem_zone_ref_ptr	ctrl_data)
{
	ctrl_t					*ctrl;
	ctrl_compute_func_ptr	ctrl_compute;
	char					ctrl_class[32];

	tree_manager_get_child_value_str(ctrl_data,NODE_HASH("ctrl class"),ctrl_class,32);
	
	ctrl	=	gfx_ctrl_find_control			(ctrl_class);
	if(ctrl	==	PTR_NULL)
	{
		writestr("ctrl menu not found \n");
		return 0;
	}
	ctrl_compute		=	get_tpo_mod_exp_addr_name(&ctrl->tpo_mod,"ctrl_compute");
	if(ctrl_compute==PTR_NULL)return 0;

	return ctrl_compute(ctrl_data);
}
int gfx_ctrl_event	   (mem_zone_ref_ptr ctrl_data_node,mem_zone_ref_const_ptr event_node)
{
	ctrl_t					*ctrl;
	ctrl_event_func_ptr		ctrl_event;
	char					ctrl_class[32];

	if(!tree_manager_get_child_value_str	(ctrl_data_node,NODE_HASH("ctrl class"),ctrl_class,32))
	{
		writestr("ctrl class name not found \n");
		return 0;
	}


	
	ctrl	=	gfx_ctrl_find_control	(ctrl_class);
	if(ctrl	==	PTR_NULL)
	{
		writestr("ctrl ");
		writestr(ctrl_class);
		writestr("not found \n");
		return 0;
	}

	ctrl_event	=	get_tpo_mod_exp_addr_name(&ctrl->tpo_mod,"ctrl_event");

	if(ctrl_event==PTR_NULL)
	{
		writestr("ctrl event not found \n");
		return 0;
	}


	return ctrl_event(ctrl_data_node,event_node);
}


void	 gfx_ctrl_init()
{
	mem_zone_ref		*control_ptr,*control_ptr_last;

	ctrl_tpo_mod_list.zone	=	PTR_NULL;

	allocate_new_zone	(0x00,sizeof(mem_zone_ref)*8	,&ctrl_tpo_mod_list);

	control_ptr			=	get_zone_ptr(&ctrl_tpo_mod_list,0);
	control_ptr_last	=	get_zone_ptr(&ctrl_tpo_mod_list,0xFFFFFFFF);

	kernel_log				(kernel_log_id,"gfx ctrl init \n");

	while(control_ptr<control_ptr_last)
	{
		control_ptr->zone=PTR_NULL;
		control_ptr++;
	}
}


unsigned int gfx_ctrl_new_control(const char *ctrl_name)
{
	unsigned int cnt;
	char		file_path[256];
	mem_stream  file_stream;
	mem_zone_ref		*control_ptr,*control_ptr_last;
	mem_zone_ref		*new_control;
	ctrl_t				*new_ctrl_ptr;


	strcpy_s(file_path,256,"/system/libs/ui/");
	strcat_s(file_path,256,ctrl_name);
	strcat_s(file_path,256,".tpo");
	file_stream.data.zone=PTR_NULL;

	if(!file_system_open_file	("isodisk",file_path,&file_stream))
	{
		kernel_log(kernel_log_id,"could not find ctrl mod '");
		writestr  (file_path);
		writestr  ("'\n");

		return 0;
	}

	


	control_ptr			=	get_zone_ptr(&ctrl_tpo_mod_list,0);
	control_ptr_last	=	get_zone_ptr(&ctrl_tpo_mod_list,0xFFFFFFFF);
	new_control			=	PTR_NULL;
	cnt					=	0;

	
	while(control_ptr<control_ptr_last)
	{
		ctrl_t				*ctrl_ptr;
		if(control_ptr->zone==PTR_NULL)	
		{
			new_control=control_ptr;
			break;
		}

		ctrl_ptr=get_zone_ptr(control_ptr,0);
		if(!strncmp_c(ctrl_ptr->tpo_mod.name,ctrl_name,strlen_c(ctrl_name)))
		{
			return 1;
		}
		control_ptr++;
		cnt++;
	}


	if(new_control == PTR_NULL)
	{
		if(expand_zone(&ctrl_tpo_mod_list,(cnt+1)*sizeof(mem_zone_ref))<0){
			kernel_log(kernel_log_id,"unable to expand control list \n");
			return 0;
		}

		control_ptr				=	get_zone_ptr	(&ctrl_tpo_mod_list,0);
		control_ptr_last		=	get_zone_ptr	(&ctrl_tpo_mod_list,0xFFFFFFFF);

		new_control				=	&control_ptr[cnt];
		control_ptr				=	new_control;

		while(control_ptr<control_ptr_last)
		{
			control_ptr->zone				=	PTR_NULL;
			control_ptr++;
		}
	}


	allocate_new_zone	(0,sizeof(ctrl_t),new_control);

	new_ctrl_ptr	  =		get_zone_ptr(new_control,0);

	tpo_mod_init			(&new_ctrl_ptr->tpo_mod);
	tpo_mod_load_tpo		(&file_stream,&new_ctrl_ptr->tpo_mod,0);
	mem_stream_close		(&file_stream);


	return 1;
}
	
ctrl_t *gfx_ctrl_find_control(const char *ctrl_name)
{
	mem_zone_ref		*control_ptr,*control_ptr_last;
	ctrl_t				*ctrl_ptr;
	control_ptr			=	get_zone_ptr(&ctrl_tpo_mod_list,0);
	control_ptr_last	=	get_zone_ptr(&ctrl_tpo_mod_list,0xFFFFFFFF);

	while(control_ptr<control_ptr_last)
	{
		if(control_ptr->zone==PTR_NULL)return PTR_NULL;

		ctrl_ptr=get_zone_ptr(control_ptr,0);
		if(!strncmp_c(ctrl_ptr->tpo_mod.name,ctrl_name,strlen_c(ctrl_name)))break;
		control_ptr++;
	}

	return ctrl_ptr;
}