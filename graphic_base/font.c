#define GFX_API C_EXPORT

#include <std_def.h>
#include <std_mem.h>
#include <kern.h>
#include <mem_base.h>
#include <lib_c.h>
#include <tree.h>
#include <sys/mem_stream.h>
#include <sys/file_system.h>
#include <sys/tpo_mod.h>
#include <sys/task.h>
#include "filters/filters.h"
#include <gfx/graphic_object.h>
#include <gfx/graphic_base.h>

#include <ft2build.h>
#include <freetype/ftoutln.h> 

#include FT_FREETYPE_H

#include "font.h"


mem_zone_ref					font_object_list		={PTR_INVALID};
unsigned int					num_sys_fonts			=0xFFFFFFFF;
FT_Library						font_library			={PTR_INVALID};
extern unsigned int				kernel_log_id;




int init_font_loader()
{
  unsigned int	error;


  kernel_log	(kernel_log_id,"init free type font \n");

  error = FT_Init_FreeType( &font_library );
  if ( error )
  {
	  kernel_log	(kernel_log_id,"error init font loader \n");
	  return 0;
  }
  
  font_object_list.zone	=	NULL;
  allocate_new_zone	(0x00,sizeof(gfx_font_object_t)*8		,&font_object_list);
  num_sys_fonts	=	0;
  kernel_log	(kernel_log_id,"font loader initialised \n");

   return 1;
}

gfx_font_object_t	*get_new_font	   (const char *name)
{
	gfx_font_object_t	*fonts,*font;
	
	fonts		=	get_zone_ptr(&font_object_list,0);
	font		=	&fonts[num_sys_fonts];
	
	strcpy_s(font->name,32,name);
	font->crc_name=calc_crc32_c(font->name,32);
	
	num_sys_fonts++;
	return font;
}

gfx_font_object_t	*find_font_crc	 (unsigned int crc_name)
{
	gfx_font_object_t		*font;
	unsigned int			n;
	
	font			=	get_zone_ptr(&font_object_list,0);
	n	=	0;
	while(n<num_sys_fonts)
	{
		if(font[n].crc_name==crc_name)return &font[n];
		n++;
	}

	return PTR_NULL;
}
gfx_font_object_t	*find_font	 (const char *name)
{
	return find_font_crc		(calc_crc32_c(name,32));
}



void init_hspan_list(struct gfx_hspan_list_t *list,unsigned int n)
{
	list->num_span_alloc		=	n;
	list->num_spans				=	0;
	allocate_new_zone		(0,list->num_span_alloc*sizeof(struct gfx_hspan_t),&list->span_list);
}



void add_hspan		(struct gfx_hspan_list_t *list,short y,const FT_Span *span)
{
	struct gfx_hspan_t  *new_span;

	expand_zone				(&list->span_list,(list->num_spans+2)*sizeof(struct gfx_hspan_t));

	new_span=get_zone_ptr	(&list->span_list,(list->num_spans)*sizeof(struct gfx_hspan_t));
	new_span->pos[0]	=span->x;
	new_span->pos[1]	=y;
	new_span->len		=span->len;
	new_span->coverage	=span->coverage;
	
	list->num_spans++;

}


void graySpans(int y, int count,const FT_Span *spans,void *user)
  {
	int i;
	struct gfx_hspan_list_t *list=(struct gfx_hspan_list_t *)user;

    //gfx_renderer_device_t *painter = (gfx_renderer_device_t *)user;
    y = -y;

    for (i = 0; i < count; i++)
    {
		add_hspan(list,y,&spans[i]);
    }
  }

void	gfx_init_glyph_list	(struct gfx_font_glyph_list_t *glyph_list)
{
	glyph_list->span_list.span_list.zone=PTR_NULL;
	glyph_list->span_list.num_spans=0;

	init_hspan_list			(&glyph_list->span_list,16);

}
OS_API_C_FUNC(int) gfx_font_init(struct gfx_font_glyph_list_t *glyph_list,const char *font_name,unsigned int size_x,unsigned int size_y,unsigned int first_glyph,unsigned int last_glyph)
{

	FT_Error				error;
	FT_GlyphSlot			slot;
	gfx_font_object_t		*sys_font;
	unsigned int			pix_type;
	unsigned int			cnt_glyph;
	unsigned int			max_char_w,max_char_h;
	//unsigned int			size_x,size_y;

	sys_font	=	find_font(font_name);
	if(sys_font==PTR_NULL)
	{
		kernel_log	(kernel_log_id,"font not found ");
		writestr	(font_name);
		writestr	("\n"); 
		return 0;
	}
	glyph_list->size_x	=size_x;
	glyph_list->size_y	=size_y;

	gfx_init_glyph_list			(glyph_list);


	error = FT_Set_Char_Size( sys_font->face, size_x*64, size_y*64,72 , 72  );
	if(error )
	{
		kernel_log		(kernel_log_id,"error set font size ");
		writestr		(sys_font->name);
		writestr		(" "); 
		writeint		(size_x,10);
		writestr		("\n"); 
		return 0;
	}



	max_char_w=0;
	max_char_h=0;
	
	
	cnt_glyph		=	first_glyph;
	while(cnt_glyph<last_glyph)
	{
		unsigned int bytes_width;
		unsigned int pix_width;
		unsigned int bytes_width_align;

		error = FT_Load_Char( sys_font->face, cnt_glyph, FT_LOAD_RENDER  );

		//glyph_index								= FT_Get_Char_Index	(sys_font->face,cnt_glyph);
		//error									= FT_Load_Glyph		(sys_font->face,glyph_index,FT_LOAD_DEFAULT);

		if(!error)
		{
			unsigned int			char_w,char_h;

			pix_width=0;

			slot  = sys_font->face->glyph;
				
			switch(slot->bitmap.pixel_mode)
			{
				default:pix_type=GFX_PIX_FORMAT_NONE;break;
				case  FT_PIXEL_MODE_MONO:
					pix_type	 =	GFX_PIX_FORMAT_MONO;
					bytes_width	 =	slot->bitmap.pitch;
					pix_width	 =	bytes_width>>3;
				break;
				case  FT_PIXEL_MODE_GRAY:
					pix_type			=	GFX_PIX_FORMAT_GRAY;
					bytes_width			=	slot->bitmap.width;
					bytes_width_align	=	bytes_width&0x7;
					
					if(bytes_width_align<bytes_width)
						bytes_width = bytes_width_align+8;

					pix_width	 =	bytes_width;
				break;
				/*
				case  FT_PIXEL_MODE_LCD:break;
				case  FT_PIXEL_MODE_LCD_V:break;
				*/
			}
	
			char_w			=	pix_width;
			char_h			=	slot->bitmap.rows;

			if(char_w>max_char_w)max_char_w=char_w;
			if(char_h>max_char_h)max_char_h=char_h;

//			surf_total								+=	pix_width*slot->bitmap.rows;

			glyph_list->glyph_metrics[cnt_glyph].box_top		=slot->bitmap_top;
			glyph_list->glyph_metrics[cnt_glyph].box_left		=slot->bitmap_left;
			glyph_list->glyph_metrics[cnt_glyph].adv_x			=(slot->advance.x>>6);
			glyph_list->glyph_metrics[cnt_glyph].adv_y			=slot->bitmap.rows;//(iabs_c(slot->advance.y)>>6);

		}
		cnt_glyph++;
	}

	glyph_list->max_char_w=max_char_w;
	glyph_list->max_char_h=max_char_h;

	return 1;
}


int load_font_spans		(struct gfx_font_glyph_list_t *font_span,const char *font_name,unsigned int first_glyph,unsigned int last_glyph)
{
	unsigned int	cnt_glyph;
	unsigned int size_x,size_y;
	FT_Error		error;
	gfx_font_object_t	*new_font;


	new_font		=	find_font(font_name);
	if(new_font		==PTR_NULL)
	{
		kernel_log(kernel_log_id,"cannot find font (span)\n");
		return 0;
	}

	size_x=font_span->size_x;
	size_y=font_span->size_y;

	error = FT_Set_Char_Size( new_font->face, size_x*64, size_y*64,72, 72 );
	if(error)
	{
		kernel_log(kernel_log_id,"error setting glyph size\n");
		return 0;
	}

	cnt_glyph			=	first_glyph;
	while(cnt_glyph<last_glyph)
	{
		FT_Raster_Params params;
		FT_Outline	*outline;
		int			error;
		unsigned int glyph_index,first_span ;
		
		
		error									= FT_Set_Char_Size  (new_font->face, size_x*64, size_y*64,72, 72 );
		glyph_index								= FT_Get_Char_Index	(new_font->face,cnt_glyph);
		error									= FT_Load_Glyph		(new_font->face,glyph_index,FT_LOAD_NO_BITMAP);

		if(!error)
		{
			 params.target		= 0;
			 params.flags		= FT_RASTER_FLAG_DIRECT | FT_RASTER_FLAG_AA;
			 params.user		= &font_span->span_list;
			 params.gray_spans	= graySpans;
			 params.black_spans = 0;
			 params.bit_set		= 0;
			 params.bit_test	= 0;

			 first_span		=	font_span->span_list.num_spans;

			 outline								=&new_font->face->glyph->outline;
			 
			 error		=FT_Outline_Render				(font_library,outline,&params);

			 if(!error)
			 {
				 font_span->glyph_infos[cnt_glyph].glyph_span_first	=	first_span;
				 font_span->glyph_infos[cnt_glyph].glyph_span_last  =	font_span->span_list.num_spans;
				 


				/*
				  kernel_log(kernel_log_id,"loaded new glyph ");
				 writeint(cnt_glyph,10);
				 writestr(" ");
				 writeint(font_span->glyph_infos[cnt_glyph].glyph_span_first,10);
				 writestr(" ");
				 writeint(font_span->glyph_infos[cnt_glyph].glyph_span_last,10);
				 writestr("\n");
				 */
				 
				 
			 }
			 else
			 {
				 kernel_log		(kernel_log_id,"error rendering font glyph [");
				 writeint		(cnt_glyph,16);
				 writestr		("]\n");
			 }
		}
		else
		{
			kernel_log(kernel_log_id,"error loading font glyph [");
			writeint		(cnt_glyph,16);
			writestr		("]\n");
		}
		
		cnt_glyph++;
	}

	return 1;
}

OS_API_C_FUNC(unsigned int) gfx_load_font (const char *font_file,char *font_name,int str_len)
{
	mem_stream					file;
	mem_ptr						file_ptr;
	mem_size					file_size;
	FT_Error					error;
	gfx_font_object_t			*new_font;
	FT_Face						newFace;

	file.data.zone	=	PTR_NULL;
	if(file_system_open_file	("isodisk",font_file,&file)!=1)
	{
		kernel_log	(kernel_log_id,"cannot open font file "); 
		writestr	(font_file);
		writestr	("\n"); 
		return 0;
	}
	file_ptr		=	get_zone_ptr	(&file.data,file.current_ptr+file.buf_ofs);
	file_size		=	get_zone_size	(&file.data);

	error = FT_New_Memory_Face(font_library,file_ptr,file_size,0,&newFace);
	if ( error ) 
	{
		kernel_log	(kernel_log_id,"error loading font ");
		writestr	(font_file);
		writestr	("\n"); 
		return 0;
	}

	if(find_font(newFace->family_name)==PTR_NULL)
	{
		new_font		=	get_new_font(newFace->family_name);
		new_font->face	=	newFace;

		if(font_name!=PTR_NULL);
			strcpy_s(font_name,str_len,newFace->family_name);



		kernel_log	(kernel_log_id,"new font loaded ");
		writestr	(new_font->name);
		writestr	("\n");


		

		return 1;
	}
	return 0;
}





void   gfx_font_spans(gfx_font_object_t		*sys_font,unsigned int cnt_glyph)
{
	
}
 
