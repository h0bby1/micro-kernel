#define GFX_API C_EXPORT
#include <std_def.h>
#include <std_mem.h>
#include <kern.h>
#include <mem_base.h>

#include <lib_c.h>
#include <tree.h>
#include <sys/mem_stream.h>
#include <sys/file_system.h>
#include <sys/tpo_mod.h>
#include <sys/task.h>
#include <sys/ctrl.h>

#include "filters/filters.h"
#include "gfx/graphic_object.h"
#include "gfx/graphic_base.h"
#include "gfx/graphic_render.h"
#include "../kernel/bus_manager/bus_drv.h"

#include "image.h"
#include "cursor.h"

extern unsigned int kernel_log_id;

mem_zone_ref				cursor_ref			=	{PTR_INVALID};
unsigned int				n_cursors			=	0xFFFFFFFF;
gfx_image_t					cursor_imgs	[16]	=	{PTR_INVALID};


unsigned int gfx_init_cursors()
{
	if(!allocate_new_zone	(0x00,sizeof(struct gfx_cursor_t)*8		,&cursor_ref))return 0;
	n_cursors			=	0;

	return 1;
}

unsigned int gfx_load_cursor_image(const char *fs_name,const char *file_path,cursor_imgs_idx img_idx)
{
	mem_stream				file;

	file.data.zone	=	PTR_NULL;

	if(file_system_open_file	(fs_name,file_path,&file)!=1)
	{
		kernel_log	(kernel_log_id,"cannot load cursor img \n");
		return 0;
	}
	gfx_load_png_image	(&cursor_imgs[img_idx],&file);
	mem_stream_close	(&file);
	return 1;
}


struct gfx_cursor_t	*gfx_get_new_cursor()
{
	struct	gfx_cursor_t	*cursor;

	cursor									=	get_zone_ptr(&cursor_ref,n_cursors*sizeof(struct	gfx_cursor_t));
	cursor->selected_ofset[0]				=	0;
	cursor->selected_ofset[1]				=	0;

	cursor->selected_ofset_ext[0]			=	0;
	cursor->selected_ofset_ext[1]			=	0;

	cursor->top_sel_ofs[0]					=	0;
	cursor->top_sel_ofs[1]					=	0;
	
	cursor->cursor_pos[0]					=	0;
	cursor->cursor_pos[1]					=	0;
	cursor->image_id						=	0;
	cursor->visible							=	0;
	cursor->last_acvity						=	0;

	cursor->mouse_over_container_id			=	0xFFFFFFFF;
	cursor->top_clicked_container_id		=	0xFFFFFFFF;

	cursor->dragged_cont_id					=	0xFFFFFFFF;
	cursor->dragged_ctrl_id					=	0xFFFFFFFF;
	cursor->dragged_ctrl_obj_id				=	0xFFFFFFFF;


	n_cursors++;
	return cursor;
}


gfx_image_t *gfx_get_cursor_image(cursor_imgs_idx img_idx)
{
	return &cursor_imgs[img_idx];
}