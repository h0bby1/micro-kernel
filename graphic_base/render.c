#define GFX_API C_EXPORT
#include <std_def.h>
#include <std_mem.h>
#include <kern.h>
#include <mem_base.h>

#include <lib_c.h>
#include <tree.h>
#include <sys/mem_stream.h>
#include <sys/file_system.h>
#include <sys/tpo_mod.h>
#include <sys/task.h>
#include <sys/ctrl.h>

#include "filters/filters.h"
#include "gfx/graphic_object.h"
#include "gfx/graphic_base.h"
#include "gfx/graphic_render.h"
#include "../kernel/bus_manager/bus_drv.h"
#include <ft2build.h>
#include FT_FREETYPE_H

#include "gfx_lib/scene.h"

#include "image.h"
#include "font.h"
#include "cursor.h"
#include "render.h"

//#include "ctrl.h"
//#include "scene.h"



gfx_render_container_ptr_t	container_list_zone[1024]			=	{PTR_INVALID};
unsigned int				container_list_count				=	0xFFFFFFFF;

mem_zone_ref				render_device_list					=	{PTR_INVALID};
unsigned int				kernel_log_id						=	0xFFFFFFFF;
unsigned int				next_id								=	1;
unsigned int				max_z_order							=	0xFFFFFFFF;

int	C_API_FUNC  gfx_mouse_event				(device_type_t	dev_type,const mem_zone_ref	*hid_infos);
int	C_API_FUNC  gfx_joystick_event			(device_type_t	dev_type,const mem_zone_ref	*hid_infos);

unsigned int gfx_render_scene_rec		(const mem_zone_ref *scene_node,gfx_renderer_device_t	*renderer,const gfx_rect_t *cur_rect,const vec_2s_t	cur_zoom,const vec_2s_t	cur_trans);

int	init_render_list(unsigned int num)
{
	gfx_renderer_device_t	*renderer,*renderer_last;
	render_device_list.zone	=	PTR_NULL;
	allocate_new_zone	(0x00,sizeof(gfx_renderer_device_t)*num	,&render_device_list);

	renderer		=	get_zone_ptr(&render_device_list,0);
	renderer_last	=	get_zone_ptr(&render_device_list,0xFFFFFFFF);
	while(renderer<renderer_last)
	{
		renderer->id=0xFFFFFFFF;
		renderer++;
	}

	return 1;
}


OS_API_C_FUNC(unsigned int) gfx_init()
{	
	struct	gfx_cursor_t	*cursor;


	if (!init_render_list(4)) { 
		kernel_log(kernel_log_id, "init_render_list failed \n");
		return 0; 
	}
	if (!init_font_loader()) {
		kernel_log(kernel_log_id, "init_font_loader failed \n");
		return 0;
	}
	if (!init_images()) {

		kernel_log(kernel_log_id, "init_images failed \n");
		return 0;
	}
	if (!gfx_ctrl_init()) 
	{
		kernel_log(kernel_log_id, "gfx_ctrl_init failed \n");
		return 0; 
	}
	if (!gfx_init_cursors()) { 
		kernel_log(kernel_log_id, "gfx_init_cursors failed \n");
		return 0; 
	}
	

	max_z_order			=	0;
	container_list_count=	0;


	gfx_load_cursor_image	("isodisk","/system/imgs/cursor.png"	,GFX_CURSOR_DEFAULT);
	gfx_load_cursor_image	("isodisk","/system/imgs/cursor_2.png"	,GFX_CURSOR_SELECTED);
	gfx_load_cursor_image	("isodisk","/system/imgs/cursor_3.png"	,GFX_CURSOR_MOVE_RIGHT);

	cursor			 =	gfx_get_new_cursor	();
	cursor->image_id = GFX_CURSOR_DEFAULT;
	cursor->cursor_pos[0]=16;
	cursor->cursor_pos[1]=16;

	cursor->buttons[0]=0;
	cursor->buttons[1]=0;
	cursor->last_acvity=0;
	cursor->visible=1;

	
	

	bus_manager_add_dev_event_listener	(DEVICE_TYPE_MOUSE,gfx_mouse_event);
	bus_manager_add_dev_event_listener	(DEVICE_TYPE_JOYSTICK,gfx_joystick_event);

	return 1;
}

OS_API_C_FUNC(int) gfx_init_render_device	(unsigned int bus_drv_id,unsigned int dev_id)
{
	tpo_mod_file			*driver_mod;
	gfx_renderer_device_t	*renderer,*renderer_cur,*renderer_last;


	if(bus_manager_get_device_driver		(bus_drv_id,dev_id,&driver_mod)==0xFFFFFFFF)
	{
		kernel_log	(kernel_log_id,"could not find render device driver \n");
		return 0;
	}

	kernel_log	(kernel_log_id,"renderer drv:'");
	writestr	(driver_mod->name);
	writestr	("' bus id:");
	writeint	(bus_drv_id,16);
	writestr	(" dev id:");
	writeint	(dev_id,16);
	writestr	("\n");


	register_tpo_exports				(driver_mod,driver_mod->name);

	renderer_cur	=	get_zone_ptr(&render_device_list,0);
	renderer_last	=	get_zone_ptr(&render_device_list,0xFFFFFFFF);
	renderer		=	PTR_NULL;

	while(renderer_cur<renderer_last)
	{
		if(renderer_cur->id==0xFFFFFFFF)
		{
			renderer=renderer_cur;
			break;
		}
		renderer_cur++;
	}
	if(renderer==PTR_NULL)return 0;

	renderer->id					=next_id;
	renderer->cursor_img_id			=0;
	renderer->create_surface_2D		=get_tpo_mod_exp_addr_name			(driver_mod,"create_surface_2D");
	renderer->load_surface_data		=get_tpo_mod_exp_addr_name			(driver_mod,"load_surface_data");
	renderer->load_font_surface_data=get_tpo_mod_exp_addr_name			(driver_mod,"load_font_surface_data");
	renderer->free_surface_2D		=get_tpo_mod_exp_addr_name			(driver_mod,"free_surface_2D");
	renderer->set_color_4uc			=get_tpo_mod_exp_addr_name			(driver_mod,"set_color_4uc");
	renderer->draw_rect				=get_tpo_mod_exp_addr_name			(driver_mod,"draw_rect");
	renderer->blit_surf				=get_tpo_mod_exp_addr_name			(driver_mod,"blit_surf");
	renderer->blit_surf_resize		=get_tpo_mod_exp_addr_name			(driver_mod,"blit_surf_resize");
	renderer->draw_image			=get_tpo_mod_exp_addr_name			(driver_mod,"draw_image");
	renderer->draw_line				=get_tpo_mod_exp_addr_name			(driver_mod,"draw_line");
	renderer->draw_hspan_list		=get_tpo_mod_exp_addr_name			(driver_mod,"draw_hspan_list");
	renderer->draw_image_resize		=get_tpo_mod_exp_addr_name			(driver_mod,"draw_image_resize");
	renderer->swap_buffers			=get_tpo_mod_exp_addr_name			(driver_mod,"swap_buffers");
	renderer->clear_buffers			=get_tpo_mod_exp_addr_name			(driver_mod,"clear_buffers");
	renderer->set_clear_color_4uc	=get_tpo_mod_exp_addr_name			(driver_mod,"set_clear_color_4uc");
	renderer->set_clip_rect			=get_tpo_mod_exp_addr_name			(driver_mod,"set_clip_rect");
	renderer->select_font			=get_tpo_mod_exp_addr_name			(driver_mod,"select_font");
	renderer->draw_text				=get_tpo_mod_exp_addr_name			(driver_mod,"draw_text");
	renderer->get_text_vec			=get_tpo_mod_exp_addr_name			(driver_mod,"get_text_vec");
	renderer->load_cursor			=get_tpo_mod_exp_addr_name			(driver_mod,"load_cursor");
	renderer->draw_cursor			=get_tpo_mod_exp_addr_name			(driver_mod,"draw_cursor");


	next_id++;
	return renderer->id;
}

OS_API_C_FUNC(unsigned int) gfx_get_all_container_list()
{
	mem_zone_ref		task_list_ref={PTR_NULL};
	mem_zone_ref		container_list={PTR_NULL};
	mem_zone_ref_ptr	task;
	
	if(!task_manager_get_first_task(&task_list_ref,&task))return 0;

	container_list_count=0;
	while(task->zone!=PTR_NULL)
	{
		if(tree_manager_get_child_value_ptr	(task,NODE_HASH("container list"),0,&container_list.zone))
		{
			gfx_render_container_t	*container,*container_last;

			if(get_zone_size(&container_list)>0)
			{
				container		=	get_zone_ptr(&container_list,0);
				container_last	=	get_zone_ptr(&container_list,0xFFFFFFFF);
		
				while((container<container_last)&&(container->id!=0xFFFFFFFF))
				{
					if(container_list_count<1024)
					{
						container_list_zone[container_list_count]=container;
					}	
					container_list_count++;
					container++;
				}
			}
		}
		if(!tree_manager_get_next_child	(&task_list_ref,&task))break;
	}

	return 1;
}

unsigned int load_font_to_surface(gfx_renderer_device_t	*renderer,struct gfx_font_glyph_list_t *glyph_list,const char *font_name,unsigned int first_glyph,unsigned int last_glyph)
{
	gfx_font_object_t			*sys_font;
	unsigned int				cnt_glyph;
	FT_Error					error;
	FT_GlyphSlot				slot;

		
		

	sys_font	=	find_font(font_name);
	if(sys_font==PTR_NULL)
	{
		kernel_log		(kernel_log_id,"cannot find font to surface \n");
		return 0;
	}

	cnt_glyph			=	first_glyph;
	while(cnt_glyph<last_glyph)
	{
		unsigned int	pix_type;
		gfx_rect_t		src_rect;
		error = FT_Load_Char( sys_font->face, cnt_glyph, FT_LOAD_RENDER );
		if(!error)
		{
			slot  = sys_font->face->glyph;
			src_rect.pos[0]		= 0;
			src_rect.pos[1]		= 0;
			src_rect.size[0]	= slot->bitmap.width;
			src_rect.size[1]	= slot->bitmap.rows;

			switch(slot->bitmap.pixel_mode)
			{
				default:pix_type=GFX_PIX_FORMAT_NONE;break;
				case  FT_PIXEL_MODE_MONO:
					pix_type	 =	GFX_PIX_FORMAT_MONO;
				break;
				case  FT_PIXEL_MODE_GRAY:
					pix_type		=	GFX_PIX_FORMAT_GRAY;
				break;
			}
			
			renderer->load_font_surface_data	(&src_rect,cnt_glyph,slot->bitmap.pitch,pix_type,slot->bitmap.buffer);
					

		}
		cnt_glyph++;
	}
	return 1;
}
int	compute_node_image		(mem_zone_ref_ptr node,mem_zone_ref_const_ptr last_obj_node)
{
	char						fs_name[32];
	char						image_path[256];
	mem_zone_ref				file_path={PTR_NULL};
	gfx_image_t					*new_image;
	unsigned int				image_id;

	if(tree_manager_get_child_value_i32		(node,NODE_HASH("image_id"),&image_id))return 1;

	if(!tree_node_find_child_by_type			(node,NODE_FILE_SYSTEM_PATH,&file_path,0))
	{
		kernel_log(kernel_log_id,"no image path ! \n");
		return 0;
	}

	tree_manager_get_child_value_str		(&file_path,NODE_HASH("fs name"),fs_name,32,0);
	tree_manager_get_child_value_str		(&file_path,NODE_HASH("path" )  ,image_path,256,0);

	new_image	=	gfx_get_new_image		(fs_name,image_path);

	if(new_image == PTR_NULL)return  0;

	tree_manager_set_child_value_i32		(node,"image_id"	,new_image->image_id);
	tree_manager_set_child_value_i32		(node,"width"		,new_image->width);
	tree_manager_set_child_value_i32		(node,"height"		,new_image->height);
	tree_manager_set_child_value_i32		(node,"pixel format",new_image->pixfmt);


	return 1;

	
}

int	compute_node_text		(gfx_renderer_device_t	*renderer,mem_zone_ref_const_ptr node,mem_zone_ref_const_ptr last_obj_node)
{
	mem_zone_ref				 text_node={PTR_NULL};
	mem_zone_ref				 style_node={PTR_NULL};
	struct gfx_rect				 txt_rect;
	int							 text_x,text_y;
	int							 font_size_x,font_size_y;
	char						 *text_txt;
	char						*font_name;
	struct gfx_font_glyph_list_t *glyph_list;
	unsigned int				  is_new,pos;


	text_txt	 	 =PTR_NULL;

	tree_manager_get_child_data_ptr			(node,NODE_HASH("text"),&text_txt);
	if(strlen_c(text_txt)<=0)return 0;
	
	font_name	 	 ="Flama";
	font_size_x	 	 =0;
	font_size_y	 	 =0;
	text_x		 	 =0;
	text_y		 	 =0;
	pos				 =0;


	if(tree_node_find_child_by_name	(node,"style",&style_node))
	{
		struct node_hash_val_t		  node_hash_val_list[16];

		node_hash_val_list[0].crc	=NODE_HASH("pos");
		node_hash_val_list[0].data	=&pos;
		node_hash_val_list[1].crc	=NODE_HASH("p_x");
		node_hash_val_list[1].data	=&text_x;
		node_hash_val_list[2].crc	=NODE_HASH("p_y");
		node_hash_val_list[2].data	=&text_y;
		node_hash_val_list[3].crc	=NODE_HASH("font_size_x");
		node_hash_val_list[3].data	=&font_size_x;
		node_hash_val_list[4].crc	=NODE_HASH("font_size_y");
		node_hash_val_list[4].data	=&font_size_y;
		node_hash_val_list[5].crc	=NODE_HASH("font_name");
		node_hash_val_list[5].data	=&font_name;
		node_hash_val_list[6].crc	=0;
		node_hash_val_list[6].data	=PTR_NULL;

		tree_node_read_childs			(&style_node,node_hash_val_list);
		release_zone_ref				(&style_node);
	}

	//writestr_fmt("pos:%d,font:'%s',size:%dx%d\n",pos,font_name,font_size_x,font_size_y);

	if(renderer!=PTR_NULL)
	{
		if(!renderer->select_font	(font_name,font_size_x,font_size_y,&is_new,&glyph_list))
		{
			txt_rect.size[0]=font_size_x;
			txt_rect.size[1]=font_size_y;
			writestr("could not select font compute node text \n");
		}
		
		if(is_new==1)
		{
			load_font_spans			(glyph_list,font_name,32,175);
			load_font_to_surface	(renderer,glyph_list,font_name,32,175);
			font_size_x	   =glyph_list->size_x;
			font_size_y	   =glyph_list->size_y;
		}
		renderer->get_text_vec			(text_txt,PTR_NULL,&txt_rect);
	}
	else
	{
		txt_rect.size[0]=font_size_x*strlen_c(text_txt);
		txt_rect.size[1]=font_size_y;
	}

	

	if((last_obj_node!=PTR_NULL)&&(last_obj_node->zone!=PTR_NULL))
	{

		

		if(pos==1)
		{
			mem_zone_ref r_node={PTR_NULL};
			
			
			if(tree_node_find_child_by_name			(last_obj_node,"rect",&r_node))
			{
				gfx_rect_t rect;

				tree_mamanger_get_node_signed_dword	(&r_node,0 ,&rect.pos[0]);
				tree_mamanger_get_node_signed_dword	(&r_node,4 ,&rect.pos[1]);
				tree_mamanger_get_node_dword		(&r_node,8 ,&rect.size[0]);
				tree_mamanger_get_node_dword		(&r_node,12,&rect.size[1]);
				release_zone_ref					(&r_node);

				text_x	=	text_x	+	rect.pos[0]+rect.size[0];
				text_y	=	text_y	+	rect.pos[1];
			
			}
		}

		if(pos==2)
		{
			mem_zone_ref r_node={PTR_NULL};
			if(tree_node_find_child_by_name			(last_obj_node,"rect",&r_node))
			{
				gfx_rect_t rect;

				tree_mamanger_get_node_signed_dword	(&r_node,0 ,&rect.pos[0]);
				tree_mamanger_get_node_signed_dword	(&r_node,4 ,&rect.pos[1]);
				tree_mamanger_get_node_dword		(&r_node,8 ,&rect.size[0]);
				tree_mamanger_get_node_dword		(&r_node,12,&rect.size[1]);
				release_zone_ref					(&r_node);

				text_x	=	text_x	+	rect.pos[0];
				text_y	=	text_y	+	rect.pos[1];
			}
		}
	}

	txt_rect.pos[0]	=	text_x;
	txt_rect.pos[1]	=	text_y;


	if(tree_node_find_child_by_name		(node,"rect",&text_node))
	{
		tree_manager_write_node_signed_dword	(&text_node,0,txt_rect.pos[0]);
		tree_manager_write_node_signed_dword	(&text_node,4,txt_rect.pos[1]);
		tree_manager_write_node_dword			(&text_node,8,txt_rect.size[0]);
		tree_manager_write_node_dword			(&text_node,12,txt_rect.size[1]);
		release_zone_ref						(&text_node);
	}

	/*writestr_fmt("compute text %s %d %d %d %d \n", text_txt, txt_rect.pos[0], txt_rect.pos[1], txt_rect.size[0], txt_rect.size[1]);*/

	return 1;
}


int	compute_node_text_list		(gfx_renderer_device_t	*renderer,mem_zone_ref_const_ptr node)
{
	mem_zone_ref				 style_node	={PTR_NULL};
	mem_zone_ref				 txt_list	={PTR_NULL};
	mem_zone_ref				 val_node	={PTR_NULL};
	mem_zone_ref_ptr			 txt_item;
	
	struct gfx_rect				 txt_rect;
	int							 text_x,text_y;
	int							 font_size_x,font_size_y;
	unsigned int				 border,visible;
	char						 text_txt[64];
	char						 *font_name;
	struct gfx_font_glyph_list_t *glyph_list;
	unsigned int				  is_new;

	font_size_x	  	=12;
	font_size_y	  	=10;
	font_name		="Flama";
	text_x		  	=0;
	text_y		  	=0;
	border			=0;
	visible			=1;


	txt_rect.pos[0]	=0;
	txt_rect.pos[1]	=0;
	txt_rect.size[0]=0;
	txt_rect.size[1]=0;
	
	if(tree_node_find_child_by_name	(node,"style",&style_node))
	{
		struct node_hash_val_t		  node_hash_val_list[16];

		//tree_manager_dump_node_rec(node,0,3);

		node_hash_val_list[0].crc	=NODE_HASH("border");
		node_hash_val_list[0].data	=&border;
		node_hash_val_list[1].crc	=NODE_HASH("p_x");
		node_hash_val_list[1].data	=&text_x;
		node_hash_val_list[2].crc	=NODE_HASH("p_y");
		node_hash_val_list[2].data	=&text_y;
		node_hash_val_list[3].crc	=NODE_HASH("font_size_x");
		node_hash_val_list[3].data	=&font_size_x;
		node_hash_val_list[4].crc	=NODE_HASH("font_size_y");
		node_hash_val_list[4].data	=&font_size_y;
		node_hash_val_list[5].crc	=NODE_HASH("font_name");
		node_hash_val_list[5].data	=&font_name;
		node_hash_val_list[6].crc	=NODE_HASH("visible");
		node_hash_val_list[6].data	=&visible;
		node_hash_val_list[7].crc	=0;
		node_hash_val_list[7].data	=PTR_NULL;

		tree_node_read_childs			(&style_node,node_hash_val_list);
		release_zone_ref				(&style_node);
	}

	if(renderer!=PTR_NULL)
	{
		if(!renderer->select_font	(font_name,font_size_x,font_size_y,&is_new,&glyph_list))
		{
			txt_rect.size[0]=font_size_x;
			txt_rect.size[1]=font_size_y;
			writestr("could not select font compute node text list \n");
			tree_manager_dump_node_rec(node,0,3);
		}
		
		if(is_new==1)
		{
			load_font_spans			(glyph_list,font_name,32,175);
			load_font_to_surface	(renderer,glyph_list,font_name,32,175);
			font_size_x	   =glyph_list->size_x;
			font_size_y	   =glyph_list->size_y;
		}
	}


	if(!tree_manager_get_first_child(node,&txt_list,&txt_item))return 0;

	while(txt_item->zone!=PTR_NULL)
	{
		if(tree_mamanger_get_node_type(txt_item)==NODE_GFX_TEXT_LIST_ENTRY)
		{	
			if(tree_manager_get_child_value_str(txt_item,NODE_HASH("text"),text_txt,64,0))
			{
				int			t_tx=0;
		
				tree_manager_get_child_value_si32	(txt_item,NODE_HASH("pos_x"),&t_tx);

				if(renderer!=PTR_NULL)
					renderer->get_text_vec				(text_txt,PTR_NULL,&txt_rect);

				txt_rect.pos[0]			+=	(text_x+t_tx);
				txt_rect.pos[1]			+=	text_y;

				val_node.zone			=	PTR_NULL;

				tree_manager_set_child_value_rect	(txt_item,"rect",&txt_rect);

				/*
				if(tree_node_find_child_by_name		(txt_item,"rect",&val_node))
				{
					tree_manager_write_node_signed_dword	(&val_node,0 ,txt_rect.pos[0]);
					tree_manager_write_node_signed_dword	(&val_node,4 ,txt_rect.pos[1]);
					tree_manager_write_node_dword			(&val_node,8 ,txt_rect.size[0]);
					tree_manager_write_node_dword			(&val_node,12,txt_rect.size[1]);
					release_zone_ref						(&val_node);
				}
				*/
				text_y+=txt_rect.size[1];
			}
		}
		if(!tree_manager_get_next_child(&txt_list,&txt_item))break;
	}

	return 1;

}

void	render_node_text_list		(gfx_renderer_device_t	*renderer,mem_zone_ref_const_ptr node,const gfx_rect_t *container_rect,const vec_2s_t zoom,const vec_2s_t trans)
{
	struct node_hash_val_t		  node_hash_val_list[16];
	char						 text_txt[64];
	char						 *font_name;
	struct gfx_rect				 txt_rect;
	mem_zone_ref				 txt_list	={PTR_NULL};
	mem_zone_ref_ptr			 txt_item;
	mem_zone_ref				 val_node	={PTR_NULL};
	mem_zone_ref				 style_node	={PTR_NULL};
	vec_4uc_t					 txt_col,bk_col;
	struct gfx_font_glyph_list_t *glyph_list;
	
	unsigned int				 border,visible;
	int							 text_x,text_y;
	int							 font_size_x,font_size_y;
	unsigned int				  is_new;
	
	
	font_size_x	  =0;
	font_size_y	  =0;
	text_x		  =0;
	text_y		  =0;
	font_name="Flama";


	txt_rect.pos[0]	=0;
	txt_rect.pos[1]	=0;
	txt_rect.size[0]=0;
	txt_rect.size[1]=0;
	
	if(!tree_node_find_child_by_name	(node,"style",&style_node))return ;
	
	node_hash_val_list[0].crc	=NODE_HASH("border");
	node_hash_val_list[0].data	=&border;
	node_hash_val_list[1].crc	=NODE_HASH("p_x");
	node_hash_val_list[1].data	=&text_x;
	node_hash_val_list[2].crc	=NODE_HASH("p_y");
	node_hash_val_list[2].data	=&text_y;
	node_hash_val_list[3].crc	=NODE_HASH("font_size_x");
	node_hash_val_list[3].data	=&font_size_x;
	node_hash_val_list[4].crc	=NODE_HASH("font_size_y");
	node_hash_val_list[4].data	=&font_size_y;
	node_hash_val_list[5].crc	=NODE_HASH("font_name");
	node_hash_val_list[5].data	=&font_name;
	node_hash_val_list[6].crc	=NODE_HASH("visible");
	node_hash_val_list[6].data	=&visible;
	node_hash_val_list[7].crc	=0;
	node_hash_val_list[7].data	=PTR_NULL;

	tree_node_read_childs			(&style_node,node_hash_val_list);
	release_zone_ref				(&style_node);
	
	if(renderer!=PTR_NULL)
	{
		if(!renderer->select_font	(font_name,font_size_x,font_size_y,&is_new,&glyph_list))
		{
			txt_rect.size[0]=font_size_x;
			txt_rect.size[1]=font_size_y;
			writestr("could not select font render node text list \n");
			tree_manager_dump_node_rec(node,0,3);
		}
		if(is_new==1)
		{
			load_font_spans			(glyph_list,font_name,32,175);
			load_font_to_surface	(renderer,glyph_list,font_name,32,175);
			font_size_x	   =glyph_list->size_x;
			font_size_y	   =glyph_list->size_y;
		}
	}
	if(!visible)return;

	if(!tree_manager_get_first_child(node,&txt_list,&txt_item))return ;
	while(txt_item->zone!=PTR_NULL)
	{
		vec_2s_t	extent;

		if(tree_mamanger_get_node_type(txt_item)==NODE_GFX_TEXT_LIST_ENTRY)
		{
			if(!tree_manager_get_child_value_4uc(txt_item,NODE_HASH("text_color"),txt_col))
				memset_c(txt_col,0,4);

			if(!tree_manager_get_child_value_4uc(txt_item,NODE_HASH("bk_color"),bk_col))		
				memset_c(bk_col,0,4);

			if(tree_manager_get_child_value_str(txt_item,NODE_HASH("text"),text_txt,64,0))
			{
				int			t_tx=0;
				int			p_x,p_y;
				int			size[2];
				int			c_size[2];
		
				tree_manager_get_child_value_si32(txt_item,NODE_HASH("pos_x"),&t_tx);
				

				val_node.zone			=	PTR_NULL;
				if(tree_node_find_child_by_name		(txt_item,"rect",&val_node))
				{
					tree_mamanger_get_node_signed_dword	(&val_node,0 ,&txt_rect.pos[0]);
					tree_mamanger_get_node_signed_dword	(&val_node,4 ,&txt_rect.pos[1]);

					tree_mamanger_get_node_dword		(&val_node,8 ,&txt_rect.size[0]);
					tree_mamanger_get_node_dword		(&val_node,12,&txt_rect.size[1]);
					release_zone_ref					(&val_node);
				}
				p_x							=	(text_x+t_tx);
				p_y							=	text_y;



				if(zoom!=PTR_NULL)			{p_x	 = (p_x*zoom[0])/128;		p_y	 = (p_y*zoom[1])/128;}
				if(trans!=PTR_NULL)			{p_x	-= trans[0];				p_y	-= trans[1];}
				if(container_rect!=PTR_NULL){p_x	+= container_rect->pos[0];	p_y	+= container_rect->pos[1];}

				if(zoom!=PTR_NULL)			{txt_rect.pos[0]	 = (txt_rect.pos[0]*zoom[0])/128;		txt_rect.pos[1]	 = (txt_rect.pos[1]*zoom[1])/128;}
				if(trans!=PTR_NULL)			{txt_rect.pos[0]	-= trans[0];				txt_rect.pos[1]	-= trans[1];}
				if(container_rect!=PTR_NULL){txt_rect.pos[0]	+= container_rect->pos[0];	txt_rect.pos[1]	+= container_rect->pos[1];}
				
				size[0]						=	txt_rect.size[0];
				size[1]						=	txt_rect.size[1];
				c_size[0]					=	container_rect->size[0];
				c_size[1]					=	container_rect->size[1];



				if(   (container_rect==PTR_NULL) ||
					  ((p_x+size[0])>=container_rect->pos[0])&&(p_x<(container_rect->pos[0]+c_size[0]))
					&&((p_y+size[1])>=container_rect->pos[1])&&(p_y<(container_rect->pos[1]+c_size[1]))
				)
				{
					vec_2s_t		i_trans;

					i_trans[0]	=	p_x;
					i_trans[1]	=	p_y;

					if(renderer!=PTR_NULL)
					{
						if( bk_col[3]!=0x00)
						{
							renderer->set_color_4uc	(bk_col[0],bk_col[1],bk_col[2],bk_col[3]);
							renderer->draw_rect(&txt_rect);
						}
						renderer->set_color_4uc	(txt_col[0],txt_col[1],txt_col[2],txt_col[3]);
						renderer->draw_text		(text_txt,i_trans,zoom,extent);
					}
				}
				text_y+=txt_rect.size[1];
			}
		}
		if(!tree_manager_get_next_child(&txt_list,&txt_item))break;
	}

}





void	render_node_text		(gfx_renderer_device_t	*renderer,mem_zone_ref_const_ptr node,const gfx_rect_t *container_rect,const vec_2s_t zoom,const vec_2s_t trans)
{
	mem_zone_ref				 text_node={PTR_NULL};
	mem_zone_ref				 style_node={PTR_NULL};

	struct gfx_rect				 txt_rect;
	int							 text_x,text_y;
	int							 font_size_x,font_size_y;
	char						 *text_txt;
	char						 *font_name;
	struct gfx_font_glyph_list_t *glyph_list;
	unsigned int				  is_new;
	vec_4uc_t					  txt_col,bk_col;
	struct node_hash_val_t		  node_hash_val_list[16];
	int							  size[2];
	int							  c_size[2];

	text_txt	  =PTR_NULL;

	tree_manager_get_child_data_ptr			(node,NODE_HASH("text"),&text_txt);
	if(strlen_c(text_txt)<=0)return;

	memset_c	(&txt_rect,0,sizeof(gfx_rect_t));
	font_name	  ="Flama";
	font_size_x	  =0;
	font_size_y	  =0;
	text_x		  =0;
	text_y		  =0;
	
	if(tree_node_find_child_by_name			(node,"rect",&text_node))
	{
		tree_mamanger_get_node_signed_dword	(&text_node,0 ,&txt_rect.pos[0]);
		tree_mamanger_get_node_signed_dword	(&text_node,4 ,&txt_rect.pos[1]);
		tree_mamanger_get_node_dword		(&text_node,8 ,&txt_rect.size[0]);
		tree_mamanger_get_node_dword		(&text_node,12,&txt_rect.size[1]);
		release_zone_ref					(&text_node);
	}


	memset_c	(txt_col,0,4);
	memset_c	(bk_col,0,4);
	


	
	if(tree_node_find_child_by_name	(node,"style",&style_node))
	{
		node_hash_val_list[0].crc	=NODE_HASH("text_color");
		node_hash_val_list[0].data	=txt_col;
		node_hash_val_list[1].crc	=NODE_HASH("bk_color");
		node_hash_val_list[1].data	=bk_col;
		node_hash_val_list[2].crc	=NODE_HASH("font_name");
		node_hash_val_list[2].data	=&font_name;
		node_hash_val_list[3].crc	=NODE_HASH("font_size_x");
		node_hash_val_list[3].data	=&font_size_x;
		node_hash_val_list[4].crc	=NODE_HASH("font_size_y");
		node_hash_val_list[4].data	=&font_size_y;
		node_hash_val_list[5].crc	=0;
		node_hash_val_list[5].data	=PTR_NULL;

		tree_node_read_childs			(&style_node,node_hash_val_list);
		release_zone_ref				(&style_node);
	}

	//writestr_fmt("col:%x,bcol:%x,font:'%s',size:%dx%d\n",*((unsigned int *)(txt_col)),*((unsigned int *)(bk_col)),font_name,font_size_x,font_size_y);

	text_x=txt_rect.pos[0];
	text_y=txt_rect.pos[1]+font_size_y;


	if(renderer!=PTR_NULL)
	{
		if(!renderer->select_font	(font_name,font_size_x,font_size_y,&is_new,&glyph_list))
		{
			txt_rect.size[0]=font_size_x;
			txt_rect.size[1]=font_size_y;
			writestr("could not select font render node text \n");
		}
		if(is_new==1)
		{
			load_font_spans			(glyph_list,font_name,32,175);
			load_font_to_surface	(renderer,glyph_list,font_name,32,175);
			font_size_x	   =glyph_list->size_x;
			font_size_y	   =glyph_list->size_y;
		}
	}	


	if(zoom!=PTR_NULL)			text_x			 = (text_x*zoom[0])/128;
	if(trans!=PTR_NULL)			text_x			-= trans[0];
	if(container_rect!=PTR_NULL)text_x			+= container_rect->pos[0];

	if(zoom!=PTR_NULL)			text_y			 = (text_y*zoom[1])/128;
	if(trans!=PTR_NULL)			text_y			-= trans[1];
	if(container_rect!=PTR_NULL)text_y			+= container_rect->pos[1];

	size[0]						=	txt_rect.size[0];
	size[1]						=	txt_rect.size[1];
	c_size[0]					=	container_rect->size[0];
	c_size[1]					=	container_rect->size[1];


	/*writestr_fmt("render text %s x:%d y:%d w:%d h:%d \n", text_txt, text_x,text_y, size[0], size[1]);*/
	/*writestr_fmt("crender text %s x:%d y:%d w:%d h:%d \n", text_txt, container_rect->pos[0], container_rect->pos[1], c_size[0], c_size[1]);*/

	if( (container_rect==PTR_NULL) ||
		(((text_x+size[0])>= (container_rect->pos[0])) &&(text_x<((container_rect->pos[0]+c_size[0])))&&
		((text_y +size[1])>= (container_rect->pos[1]))&&(text_y<((container_rect->pos[1]+c_size[1]))))
	)
	{
		if(renderer!=PTR_NULL)
		{
			vec_2s_t		i_trans;

			i_trans[0]	=	text_x;
			i_trans[1]	=	text_y;
			if(bk_col[3]!=0x00)
			{
				renderer->set_color_4uc	(bk_col[0],bk_col[1],bk_col[2],bk_col[3]);
				renderer->draw_rect		(&txt_rect);
			}

			renderer->set_color_4uc	(txt_col[0],txt_col[1],txt_col[2],txt_col[3]);
			renderer->draw_text		(text_txt,i_trans,zoom,PTR_NULL);

			/*writestr("draw text\n");*/
		}
	}
}

OS_API_C_FUNC(void)			render_node_rect		(gfx_renderer_device_t	*renderer,mem_zone_ref_const_ptr node,const gfx_rect_t *container_rect,const vec_2s_t zoom,const vec_2s_t trans)
{
	mem_zone_ref				 style_node={PTR_NULL};
	mem_zone_ref				 r_node={PTR_NULL};
	unsigned int				 border,width,height;
	gfx_rect_t					 rect,r_rect;
	int							 p_x,p_y;
	vec_4uc_t					 color;
	int							 size[2];
	int							 c_size[2];
	rect.pos[0]=0;
	rect.pos[1]=0;
	rect.size[0]=0;
	rect.size[1]=0;

	border=0;
	p_x=0;
	p_y=0;
	width=0;
	height=0;


	if(tree_node_find_child_by_name	(node,"style",&style_node))
	{
		struct node_hash_val_t		  node_hash_val_list[16];

		node_hash_val_list[0].crc	=NODE_HASH("border");
		node_hash_val_list[0].data	=&border;
		node_hash_val_list[1].crc	=NODE_HASH("p_x");
		node_hash_val_list[1].data	=&p_x;
		node_hash_val_list[2].crc	=NODE_HASH("p_y");
		node_hash_val_list[2].data	=&p_y;
		node_hash_val_list[3].crc	=NODE_HASH("width");
		node_hash_val_list[3].data	=&width;
		node_hash_val_list[4].crc	=NODE_HASH("height");
		node_hash_val_list[4].data	=&height;
		node_hash_val_list[5].crc	=NODE_HASH("color");
		node_hash_val_list[5].data	=color;
		node_hash_val_list[6].crc	=0;
		node_hash_val_list[6].data	=PTR_NULL;

		

		tree_node_read_childs			(&style_node,node_hash_val_list);
		release_zone_ref				(&style_node);
	}

	//writestr_fmt("col:%x,bcol:%d,font:'%s',size:%dx%d\n",*((unsigned int *)(color)),border,width,height);

	release_zone_ref(&style_node);

	rect.pos[0]=p_x+border;
	rect.pos[1]=p_y+border;
		
	rect.size[0]=width;
	rect.size[1]=height;


	if(tree_node_find_child_by_name			(node,"rect",&r_node))
	{
		tree_mamanger_get_node_signed_dword	(&r_node,0 ,&r_rect.pos[0]);
		tree_mamanger_get_node_signed_dword	(&r_node,4 ,&r_rect.pos[1]);
		tree_mamanger_get_node_dword		(&r_node,8 ,&r_rect.size[0]);
		tree_mamanger_get_node_dword		(&r_node,12,&r_rect.size[1]);
		release_zone_ref					(&r_node);
	}
	else
	{
		r_rect.pos[0]	=rect.pos[0];
		r_rect.pos[1]	=rect.pos[1];
		r_rect.size[0]	=rect.size[0];
		r_rect.size[1]	=rect.size[1];
	}

	size[0]		=	rect.size[0];
	size[1]		=	rect.size[1];

	if(container_rect!=PTR_NULL)
	{
		c_size[0]	=	container_rect->size[0];
		c_size[1]	=	container_rect->size[1];
	}
	else
	{
		c_size[0]	=	r_rect.size[0];
		c_size[1]	=	r_rect.size[1];
	}


	if(zoom!=PTR_NULL)			rect.pos[0]	 = (rect.pos[0]*zoom[0])>>7;
	if(trans!=PTR_NULL)			rect.pos[0]	-= trans[0];
	if(container_rect!=PTR_NULL)rect.pos[0]	+= container_rect->pos[0];

	if(zoom!=PTR_NULL)			rect.pos[1]	 = (rect.pos[1]*zoom[1])>>7;
	if(trans!=PTR_NULL)			rect.pos[1]	-= trans[1];
	if(container_rect!=PTR_NULL)rect.pos[1]	+= container_rect->pos[1];

	if( (container_rect==PTR_NULL) ||

		(((rect.pos[0]+size[0])>= (container_rect->pos[0])) &&(rect.pos[0]<((container_rect->pos[0]+c_size[0])))&&
		 ((rect.pos[1]+size[1])>= (container_rect->pos[1]))&&(rect.pos[1]<((container_rect->pos[1]+c_size[1]))))

	 )
	
	{
		if(renderer!=PTR_NULL)
		{
			renderer->set_color_4uc		(color[0],color[1],color[2],color[3]);
			renderer->draw_rect			(&rect);
			renderer->set_color_4uc		(0,0,0,255);

			if(border>0)
			{
				gfx_rect_t r;

				r.pos[0] =p_x;
				r.pos[1] =p_y;
				r.size[0] =border;
				r.size[1] =height;
				renderer->draw_rect		(&r);

				r.pos[0] =p_x+border+width;
				r.pos[1] =p_y;
				r.size[0]=border;
				r.size[1] =height;
				renderer->draw_rect		(&r);

				r.pos[0] =p_x+border;
				r.pos[1] =p_y;
				r.size[0]=width;
				r.size[1] =border;
				renderer->draw_rect		(&r);

				r.pos[0]  =p_x+border;
				r.pos[1]  =p_y+border+height;
				r.size[0] =width;
				r.size[1] =border;
				renderer->draw_rect		(&r);

			}
		}
	}	
	


}


OS_API_C_FUNC(void) render_node_image		(gfx_renderer_device_t	*renderer,mem_zone_ref_const_ptr node,const gfx_rect_t *container_rect,const vec_2s_t zoom,const vec_2s_t trans)
{
	struct node_hash_val_t		  node_hash_val_list[16];
	mem_zone_ref			img_node;
	unsigned int			cnt,img_id,scene_id;
	int						width,height;
	int						z_width,z_height;
	int						img_x,img_y;
	int						size[2];
	gfx_image_t				*img;

	img_node.zone	=	PTR_NULL;

	
	img_x		  	=	0;
	img_id		  	=	0;
	scene_id	  	=	0;
	img_y		  	=	0;
	cnt			  	=	0;




	node_hash_val_list[0].crc	=NODE_HASH("image_id");
	node_hash_val_list[0].data	=&img_id;
	node_hash_val_list[1].crc	=NODE_HASH("pos_x");
	node_hash_val_list[1].data	=&img_x;
	node_hash_val_list[2].crc	=NODE_HASH("pos_y");
	node_hash_val_list[2].data	=&img_y;
	node_hash_val_list[3].crc	=NODE_HASH("width");
	node_hash_val_list[3].data	=&width;
	node_hash_val_list[4].crc	=NODE_HASH("height");
	node_hash_val_list[4].data	=&height;
	node_hash_val_list[5].crc	=NODE_HASH("scene id");
	node_hash_val_list[5].data	=&scene_id;
	node_hash_val_list[6].crc	=0;
	node_hash_val_list[6].data	=PTR_NULL;

	
	tree_node_read_childs			(node,node_hash_val_list);
	//writestr_fmt("id:%d,pos_x:%x,pos_y:%x,size:%dx%d\n",img_id,img_x,img_y,width,height);

	/*writestr_fmt("render_node_image %d x:%d y:%d \n", img_id, img_x, img_y);*/

	img			=	gfx_find_image	(img_id);
	if(img==PTR_NULL)
	{
		kernel_log(kernel_log_id,"could not find image structure ");
		writeint(img_id,16);
		writestr(" of ");
		writeint(scene_id,16);
		writestr("\n");
		return ;
	}

	if(zoom!=PTR_NULL)			img_x	 = (img_x*zoom[0])>>7;
	if(trans!=PTR_NULL)			img_x	-= trans[0];
	if(container_rect!=PTR_NULL)img_x	+= container_rect->pos[0];

	if(zoom!=PTR_NULL)			img_y	 = (img_y*zoom[1])>>7;
	if(trans!=PTR_NULL)			img_y	-= trans[1];
	if(container_rect!=PTR_NULL)img_y	+= container_rect->pos[1];

	z_width =((width*zoom[0])>>7);
	z_height=((height*zoom[1])>>7);

	size[0]	=	0;
	size[1]	=	container_rect->size[1];


	if(((img_y+z_height)>=container_rect->pos[1])&&(img_y<(container_rect->pos[1]+size[1])))
	{
		if(renderer!=PTR_NULL)
		{
			if((zoom[0]==128)&&(zoom[1]==128))
			{
				renderer->draw_image(img_x,img_y,img,PTR_NULL);
			}
			else
			{
				struct gfx_rect		src,dst;

				src.pos[0]=0;
				src.pos[1]=0;
				src.size[0]=width;
				src.size[1]=height;

				dst.pos[0]=img_x;
				dst.pos[1]=img_y;
				dst.size[0]=z_width;
				dst.size[1]=z_height;

				renderer->draw_image_resize		(img,&dst,&src);
			}
		}
	}
}



gfx_renderer_device_t	*get_renderer_by_id(unsigned int render_id)
{
	gfx_renderer_device_t	*renderer,*renderer_last;
	
	
	renderer		=	get_zone_ptr(&render_device_list,0);
	renderer_last	=	get_zone_ptr(&render_device_list,0xFFFFFFFF);
	
	while(renderer<renderer_last)
	{
		if(renderer->id==render_id)
			return renderer;
		renderer++;
	}
	return PTR_NULL;
}

unsigned int cursor_loaded = 0xFFFFFF;



void gfx_render_cursor(unsigned int render_id,struct gfx_cursor_t	*cursor)
{
	gfx_renderer_device_t	*renderer;


	renderer	=	get_renderer_by_id		(render_id);
	if(renderer==PTR_NULL)
	{
		kernel_log		(kernel_log_id,"renderer not found \n");
		return ;
	}

	renderer->set_clip_rect	(0,0,0,0);

	/*
	writestr_fmt("cursor %d %d %d \n", cursor->visible, cursor->cursor_pos[0], cursor->cursor_pos[1]);
	writestr_fmt("cursor image %d %d \n", renderer->cursor_img_id, cursor->image_id);
	*/
	
	if(renderer->cursor_img_id!=cursor->image_id)
	{
		if(renderer->load_cursor		(gfx_get_cursor_image(cursor->image_id))!=0)
			renderer->cursor_img_id	=	cursor->image_id;
	}


	renderer->set_color_4uc					(255,255,255,255);
	
	
	if(cursor->visible==1)
		renderer->draw_cursor			(cursor->cursor_pos[0],cursor->cursor_pos[1]);
	
}

unsigned int gfx_render_scene_rec		(const mem_zone_ref *scene_node,gfx_renderer_device_t	*renderer,const gfx_rect_t *cur_rect,const vec_2s_t	cur_zoom,const vec_2s_t	cur_trans)
{
	char					ctrl_class[32];
	mem_zone_ref			scene_list={PTR_NULL};
	mem_zone_ref			last_obj_node={PTR_NULL};
	mem_zone_ref_ptr		obj_node;
	unsigned int			node_type;

	if(!tree_manager_get_first_child(scene_node,&scene_list,&obj_node))return 1;

	while(obj_node->zone!=PTR_NULL)
	{
		node_type=tree_mamanger_get_node_type(obj_node);

		if(renderer!=PTR_NULL)
			renderer->set_clip_rect			(cur_rect->pos[0],cur_rect->pos[1],cur_rect->size[0],cur_rect->size[1]);

		switch(node_type)
		{
			case NODE_GFX_TEXT_LIST:
				if(compute_node_text_list(renderer,obj_node)==1)
					render_node_text_list (renderer,obj_node,cur_rect,cur_zoom,cur_trans);
			break;
			case NODE_GFX_RECT_OBJ:
				render_node_rect	 (renderer,obj_node,cur_rect,cur_zoom,cur_trans);
			break;

			
			case NODE_GFX_TEXT:
				if(compute_node_text	 (renderer,obj_node,&last_obj_node)==1)
					render_node_text	 (renderer,obj_node,cur_rect,cur_zoom,cur_trans);
			break;
			
			case NODE_GFX_IMAGE_OBJ:
				if(compute_node_image(obj_node,&last_obj_node)==1)
					render_node_image	 (renderer,obj_node,cur_rect,cur_zoom,cur_trans);

			break;
			case NODE_GFX_CTRL:

				if(!tree_manager_get_child_value_str		(obj_node,NODE_HASH("ctrl class"),ctrl_class,32,0))break;
				if(!gfx_ctrl_find_control			(ctrl_class))
				{
					if(!gfx_ctrl_new_control				(ctrl_class))
					{
						kernel_log			(kernel_log_id,"could not create new control class '");
						writestr			(ctrl_class);
						writestr			("'\n");
						break;
					}
				}

				
				if(gfx_ctrl_compute	(ctrl_class,obj_node))
				{
					
					mem_zone_ref			ctrl_scene={PTR_NULL};
					gfx_rect_t				ctrl_rect;


					if(renderer==PTR_NULL)
						tree_manager_set_child_value_i32		(obj_node,"render_mode"	,1);
					else
						tree_manager_set_child_value_i32		(obj_node,"render_mode"	,0);
					

					gfx_ctrl_render						(ctrl_class,obj_node);


					if(!tree_manager_get_child_value_rect	(obj_node,NODE_HASH("rect"),&ctrl_rect))
					{
						ctrl_rect.pos[0]	=		0;
						ctrl_rect.pos[1]	=		0;
						ctrl_rect.size[0]	=		cur_rect->size[0];
						ctrl_rect.size[1]	=		cur_rect->size[1];
					}

					ctrl_rect.pos[0]		+=		cur_rect->pos[0];
					ctrl_rect.pos[1]		+=		cur_rect->pos[1];

					if(tree_node_find_child_by_type	(obj_node,NODE_GFX_SCENE,&ctrl_scene,0))
					{
			
						gfx_render_scene_rec			(&ctrl_scene,renderer,&ctrl_rect,cur_zoom,cur_trans);
						release_zone_ref				(&ctrl_scene);
					}
				}
				

			break;
			
		}
		copy_zone_ref				(&last_obj_node,obj_node);
		if(!tree_manager_get_next_child	(&scene_list,&obj_node))break;
	}
	release_zone_ref(&last_obj_node);

	return 1;

}

int comp(const_mem_ptr d,const_mem_ptr s)
{
	return ( ((gfx_render_container_ptr_t)(d))->z_order-((gfx_render_container_ptr_t)(s))->z_order);
}


OS_API_C_FUNC(unsigned int) gfx_render_container	  (const gfx_render_container_t *container,gfx_renderer_device_t	*renderer)
{

	gfx_rect_t				cur_rect;
	vec_2s_t				cur_zoom,cur_trans;
	
	
	cur_rect.pos[0]	=container->rect.pos[0];
	cur_rect.pos[1]	=container->rect.pos[1];
	cur_rect.size[0]=container->rect.size[0];
	cur_rect.size[1]=container->rect.size[1];

	cur_zoom[0]		=container->zoom_fac[0];
	cur_zoom[1]		=container->zoom_fac[1];

	cur_trans[0]	=container->trans[0];
	cur_trans[1]	=container->trans[1];

	//writestr("render container \n");

	if(renderer!=PTR_NULL)
	{
		renderer->set_clip_rect			(container->rect.pos[0],container->rect.pos[1],container->rect.size[0],container->rect.size[1]);
		renderer->set_color_4uc			(128,128,128,255);
		renderer->draw_rect				(&container->rect);
		renderer->set_color_4uc			(0,0,0,0);
	}

	gfx_render_scene_rec		(&container->scene_ref,renderer,&cur_rect,cur_zoom,cur_trans);


	cur_rect.pos[0]	=container->rect.pos[0]-4;
	cur_rect.pos[1]	=container->rect.pos[1]-20;
	cur_rect.size[0]=container->rect.size[0]+8;
	cur_rect.size[1]=container->rect.size[1]+24;

	cur_trans[0]	=-4;
	cur_trans[1]	=-20;


	gfx_render_scene_rec		(&container->ctrl_scene_ref,renderer,&cur_rect,PTR_NULL,cur_trans);
	return 1;
}


OS_API_C_FUNC(unsigned int) gfx_render_all_containers(unsigned int render_id)
{
	unsigned int			n;
	struct	gfx_cursor_t	*cursor;
	gfx_renderer_device_t	*renderer;



	if(gfx_get_all_container_list()==0)return 0;
	
	renderer	=	get_renderer_by_id		(render_id);
	if(renderer!=PTR_NULL)
	{
		renderer->set_clear_color_4uc	(64,64,64,255);
		renderer->clear_buffers			(0);
	}
	
	
	
	//container		=	get_zone_ptr(&container_list,0);
	//container_last	=	get_zone_ptr(&container_list,0xFFFFFFFF);

	qsort_c	(container_list_zone,container_list_count,sizeof(gfx_render_container_ptr_t),comp);

	for(n=0;n<container_list_count;n++)
	{
		gfx_render_container_ptr_t	container=container_list_zone[n];

		if(container->id!=0xFFFFFFFF)
			gfx_render_container	(container,renderer);
	}

	cursor					=	get_zone_ptr(&cursor_ref,0);

	
	if(renderer!=PTR_NULL)
	{
		gfx_render_cursor		(render_id,cursor);
		renderer->swap_buffers	(0);
	}

	
	
	return 1;
	

}



