

	
typedef struct
{
	char							name[32];
	unsigned int					crc_name;
	FT_Face							face;
}gfx_font_object_t;


int						init_font_loader			();
gfx_font_object_t	*	find_font					(const char *name);
gfx_font_object_t	*	find_font_crc				(unsigned int crc_name);
void					gfx_font_vec_text			(struct gfx_font_glyph_list_t	*font_spans,const char *text,struct gfx_hspan_list_t *list,struct gfx_rect *text_box);

void					gfx_init_glyph_list			(struct gfx_font_glyph_list_t *glyph_list);
void					init_hspan_list				(struct gfx_hspan_list_t *list,unsigned int n);
int						load_font_spans				(struct gfx_font_glyph_list_t *font_span,const char *font_name,unsigned int first_glyph,unsigned int last_glyph);

