#define GFX_API C_EXPORT
#include <std_def.h>
#include <std_mem.h>
#include <kern.h>
#include <mem_base.h>
#include <lib_c.h>
#include <tree.h>
#include <sys/mem_stream.h>
#include <sys/file_system.h>
#include <sys/tpo_mod.h>
#include <sys/task.h>

#include "filters/filters.h"
#include "gfx/graphic_object.h"
#include "gfx/graphic_base.h"
#include "../kernel/bus_manager/bus_drv.h"

#include <ft2build.h>
#include FT_FREETYPE_H
#include "font.h"
#include "image.h"
#include "ctrl.h"
#include "scene.h"
#include "render.h"



mem_zone_ref	container_list		={PTR_INVALID};
mem_zone_ref	cursor_ref			={PTR_INVALID};
unsigned int	n_cursors			=0xFFFFFFFF;
unsigned int	next_container_id	=0xFFFFFFFF;
unsigned int	kernel_log_id		=0xFFFFFFFF;
unsigned int	max_z_order			=0xFFFFFFFF;
const vec_4uc_t	white				={0xFF,0xFF,0xFF,0xFF};
const vec_4uc_t	black				={0x00,0x00,0x00,0xFF};
gfx_image_t		cursor_imgs		[16]={PTR_INVALID};
int C_API_FUNC	gfx_mouse_event			(device_type_t	dev_type,const mem_zone_ref	*hid_infos);




OS_API_C_FUNC(int) gfx_create_rect_style(struct obj_array_t *rect_style_ar,const gfx_rect_t *rect,const vec_4uc_t color)
{

	tree_manager_create_obj				(rect_style_ar);
	tree_manager_add_obj				(rect_style_ar			,PTR_NULL,0);
	tree_manager_add_obj_sint_val		(rect_style_ar,"p_x"	,rect->pos[0]);
	tree_manager_add_obj_sint_val		(rect_style_ar,"p_y"	,rect->pos[1]);
	tree_manager_add_obj_int_val		(rect_style_ar,"width"	,rect->size[0]);
	tree_manager_add_obj_int_val		(rect_style_ar,"height"	,rect->size[1]);
	tree_manager_add_obj_int_val		(rect_style_ar,"color"	,*((unsigned int *)(color)));
	tree_manager_end_obj				(rect_style_ar);

	return 1;
}

OS_API_C_FUNC(int) gfx_create_set_ctrl_event(mem_zone_ref_ptr ctrl_data_node,const char *type,const gfx_rect *rect,const char *prop,unsigned int prop_val)
{
	mem_zone_ref	 ev_lst={PTR_NULL};
	mem_zone_ref	event_item={PTR_NULL};
	mem_zone_ref	event_rect={PTR_NULL};

	
	
	tree_manager_create_node			("event",NODE_GFX_EVENT,&event_item);
	tree_manager_set_child_value_i32	(&event_item,prop			,prop_val);
	tree_manager_set_child_value_str	(&event_item,"event type"	,type);

	if(rect!=PTR_NULL)
	{
		if(tree_manager_add_child_node			(&event_item,"rect"			,NODE_GFX_RECT,&event_rect))
		{
			tree_manager_write_node_signed_dword	(&event_rect,0,rect->pos[0]);
			tree_manager_write_node_signed_dword	(&event_rect,4,rect->pos[1]);
			tree_manager_write_node_dword			(&event_rect,8,rect->size[0]);
			tree_manager_write_node_dword			(&event_rect,12,rect->size[1]);
			release_zone_ref						(&event_rect);
		}
	}
	
	tree_manager_find_child_node			(ctrl_data_node,NODE_HASH("event list"),NODE_GFX_EVENT_LIST,&ev_lst);
	tree_manager_node_add_child				(&ev_lst,&event_item);
	release_zone_ref						(&ev_lst);
	release_zone_ref						(&event_item);
	

	return 1;
}
/*
unsigned int gfx_container_top	(const vec_2s_t	pos,vec_2s_t ofset)
{


	gfx_render_container_t	*container,*container_last;

	container		=	get_zone_ptr(&container_list,0);
	container_last	=	get_zone_ptr(&container_list,0xFFFFFFFF);
	
	while(container<container_last)
	{
		gfx_rect_t rect;

		if(container->id!=0xFFFFFFFF)
		{
			if(gfx_scene_find_rect(container->ctrl_scene_id,container->top_rect_id,&rect))
			{
				//writestr_fmt("rect id [%d,%d] [%d,%d] [%d,%d]->[%d,%d] \n",pos[0],pos[1],container->rect.pos[0],container->rect.pos[1],rect.pos[0],rect.pos[1],rect.size[0],rect.size[1]);
				if( (pos[0]>= (container->rect.pos[0]+rect.pos[0]))&&
					(pos[1]>= (container->rect.pos[1]+rect.pos[1]))&&
					(pos[0]< ((container->rect.pos[0]+rect.pos[0])+rect.size[0]))&&
					(pos[1]< ((container->rect.pos[1]+rect.pos[1])+rect.size[1]))
				  )
				{
					ofset[0]	=	pos[0]-(container->rect.pos[0]);
					ofset[1]	=	pos[1]-(container->rect.pos[1]);
					return container->id;
				}
			}
		}
		container++;
	}
	return 0xFFFFFFFF;
}
*/

gfx_render_container_t	*get_container_by_id(unsigned int container_id)
{
	gfx_render_container_t	*container,*container_last;

	if(container_id==0xFFFFFFFF)return PTR_NULL;

	container		=	get_zone_ptr(&container_list,0);
	container_last	=	get_zone_ptr(&container_list,0xFFFFFFFF);
	
	while(((container+1)<container_last)&&(container->id!=0xFFFFFFFF))
	{
		if(container->id==container_id)return container;
		container++;
	}
	return PTR_NULL;
}

unsigned int gfx_add_container_event (gfx_render_container_t	*container,const char *event_name,mem_ptr event_data_ptr,unsigned int data_size)
{
	mem_zone_ref			new_event={PTR_NULL};

	
	if(!node_array_get_free_element(&container->event_array,&new_event))return 0;
	tree_manager_set_node_name		(&new_event,event_name);
	tree_manager_write_node_data	(&new_event,event_data_ptr,sizeof(unsigned int),data_size);
	release_zone_ref				(&new_event);
	return 1;
}

OS_API_C_FUNC(int) gfx_pop_container_event (unsigned int container_id,mem_zone_ref *event_node)
{
	gfx_render_container_t		*container;
	
	container	=	get_container_by_id		(container_id);
	if(container==PTR_NULL)
	{
		writestr_fmt("could not pop event from container [%d] \n",container_id);
		return 0;
	}

	return node_array_pop				(&container->event_array,event_node);;
}
/*
int C_API_FUNC	gfx_mouse_event			(device_type_t	dev_type,const mem_zone_ref	*hid_infos)
{
	mem_zone_ref			x_node		={PTR_NULL};
	mem_zone_ref			y_node		={PTR_NULL};
	mem_zone_ref			w_node		={PTR_NULL};
	mem_zone_ref			b1_node		={PTR_NULL};
	mem_zone_ref			b2_node		={PTR_NULL};
	int						button[4];
	int						pos[2];
	struct	gfx_cursor_t	*cursor;
	gfx_render_container_t	*cursor_mouse_over_container;
	gfx_render_container_t	*last_cursor_mouse_over_container;
	int						changed;
	

	
	tree_find_child_node_by_member_name(hid_infos,NODE_HID_INPUT,NODE_HID_USAGE,"X"			,	&x_node);
	tree_find_child_node_by_member_name(hid_infos,NODE_HID_INPUT,NODE_HID_USAGE,"Y"			,	&y_node);
	tree_find_child_node_by_member_name(hid_infos,NODE_HID_INPUT,NODE_HID_USAGE,"Wheel"		,	&w_node);
	tree_find_child_node_by_member_name(hid_infos,NODE_HID_INPUT,NODE_HID_USAGE,"button 1"	,	&b1_node);
	tree_find_child_node_by_member_name(hid_infos,NODE_HID_INPUT,NODE_HID_USAGE,"button 2"	,	&b2_node);

	
	

	cursor					=	get_zone_ptr(&cursor_ref,0);
	cursor->last_acvity		=	system_time();
	cursor->visible			=	1;

	pos[0]					=	cursor->cursor_pos[0];
	pos[1]					=	cursor->cursor_pos[1];


	get_input_value(&x_node,&pos[0]);
	get_input_value(&y_node,&pos[1]);

	get_input_value(&b1_node,&button[0]);
	get_input_value(&b2_node,&button[1]);

	release_zone_ref(&x_node);
	release_zone_ref(&y_node);
	release_zone_ref(&w_node);
	release_zone_ref(&b1_node);
	release_zone_ref(&b2_node);



	changed=0;
	last_cursor_mouse_over_container=PTR_NULL;


	if(	
		(pos[0]!=cursor->cursor_pos[0])||
		(pos[1]!=cursor->cursor_pos[1])
		)
	{
		
		gfx_render_container_t		*selected_container;

		if(cursor->top_clicked_container_id!=0xFFFFFFFF)
		{
			gfx_render_container_t		*cursor_top_clicked_container;

			cursor_top_clicked_container	=   get_container_by_id		(cursor->top_clicked_container_id);
			if(cursor_top_clicked_container)
			{
				cursor_top_clicked_container->rect.pos[0]=cursor->cursor_pos[0]-cursor->top_sel_ofs[0];
				cursor_top_clicked_container->rect.pos[1]=cursor->cursor_pos[1]-cursor->top_sel_ofs[1];
			}
		}


		selected_container					=   gfx_find_container_pos	(pos,cursor->selected_ofset);

		if(selected_container!=PTR_NULL)
		{
			cursor->selected_ofset_ext[0]=cursor->selected_ofset[0];
			cursor->selected_ofset_ext[1]=cursor->selected_ofset[1];
		}

		cursor_mouse_over_container			=   get_container_by_id		(cursor->mouse_over_container_id);
		last_cursor_mouse_over_container	=	cursor_mouse_over_container;

		if(selected_container	!=	cursor_mouse_over_container)
		{

			if(cursor_mouse_over_container!=PTR_NULL)
				gfx_add_container_event(cursor_mouse_over_container	,"mouse_out",PTR_NULL,0);

			if(selected_container!=PTR_NULL)
			{
				gfx_add_container_event	(selected_container			,"mouse_over",cursor->selected_ofset_ext,sizeof(cursor->selected_ofset_ext));
				cursor->mouse_over_container_id=selected_container->id;
			}
			else
				cursor->mouse_over_container_id=0xFFFFFFFF;

		}
		else if(selected_container!=PTR_NULL)
		{
			gfx_add_container_event	(selected_container,"mouse_moved",cursor->selected_ofset_ext,sizeof(cursor->selected_ofset_ext));
		}

		if(selected_container!=PTR_NULL)
		{
			if((iabs_c(cursor->selected_ofset_ext[0]-selected_container->rect.size[0])<16))
			{
				cursor->image_id=GFX_CURSOR_MOVE_RIGHT;
			}
			else
				cursor->image_id=GFX_CURSOR_DEFAULT;
		}
		else
			cursor->image_id=GFX_CURSOR_DEFAULT;

		changed=1;
		cursor->cursor_pos[0]=pos[0];
		cursor->cursor_pos[1]=pos[1];
	}

	
	cursor_mouse_over_container			=   get_container_by_id(cursor->mouse_over_container_id);
	if(button[0]!=cursor->buttons[0])
	{
		if(button[0])
		{
			cursor->top_clicked_container_id=gfx_container_top	(cursor->cursor_pos,cursor->top_sel_ofs);
			if(cursor->top_clicked_container_id!=0xFFFFFFFF)
			{
				 get_container_by_id(cursor->top_clicked_container_id)->z_order	=	max_z_order+1;
				 max_z_order													=	get_container_by_id(cursor->top_clicked_container_id)->z_order;
			}

			if(cursor_mouse_over_container!=PTR_NULL)
			{
				cursor_mouse_over_container	->z_order	=	max_z_order+1;
				max_z_order								=	cursor_mouse_over_container	->z_order;

				gfx_add_container_event(cursor_mouse_over_container	,"mouse_1_click"	,cursor->selected_ofset_ext,sizeof(cursor->selected_ofset_ext));
			}

			cursor->image_id = GFX_CURSOR_SELECTED;
		}
		else
		{
			cursor->top_clicked_container_id=0xFFFFFFFF;
			if(cursor_mouse_over_container!=PTR_NULL)gfx_add_container_event(cursor_mouse_over_container	,"mouse_1_release"	,cursor->selected_ofset_ext,sizeof(cursor->selected_ofset_ext));

			cursor->image_id = GFX_CURSOR_DEFAULT;
		}
		changed=1;
		cursor->buttons[0]=button[0];
		
	}

	//writestr_fmt("cursor %d,%d\n",cursor->cursor_pos[0],cursor->cursor_pos[1]);

	

	return 1;

}


OS_API_C_FUNC(int)	gfx_joystick_event			(device_type_t	dev_type,const mem_zone_ref	*hid_infos)
{
#if 0
	gfx_render_container_t  *selected_container;
	struct	gfx_cursor_t	*cursor;
	const char				*event_name;
	unsigned int			node_type;
	mem_zone_ref			val_node;


	val_node.zone=PTR_NULL;

	if(tree_node_find_child_by_type	(dev_infos,NODE_HID_USAGE,&val_node,0)==0)return 0;

		
	cursor					=	get_zone_ptr(&cursor_ref,1*sizeof(struct gfx_cursor_t));


	cursor->visible			=	1;
	cursor->last_acvity=get_system_time_c();

	selected_container	=	get_container_by_id(cursor->selected_container_id);

	
	event_name			=	tree_mamanger_get_node_name(&val_node);		
	release_zone_ref(&val_node);

	if(tree_node_find_child_by_name	(dev_infos,"last value",&val_node,1)!=0)
	{
		int		value;

		node_type	=	tree_mamanger_get_node_type(&val_node);



		value=0;

		if(node_type==NODE_GFX_INT)
		{
			tree_mamanger_get_node_dword(&val_node,0,&value);
		}
		if(node_type==NODE_GFX_SIGNED_INT)
		{
			tree_mamanger_get_node_signed_dword(&val_node,0,&value);
		}

		if(!strcmp_c(event_name,"button 1"))
		{
			
			cursor->buttons[0]=value;
			
			if(value==0)
			{
				cursor->selected_container_id	=0xFFFFFFFF;
				selected_container				=PTR_NULL;
			}
			else
			{
				selected_container	=	gfx_find_container_pos(cursor->cursor_pos,cursor->selected_ofset);
				
				if(selected_container!=PTR_NULL)
				{
					struct gfx_rect rect;

					gfx_scene_get_extent		(selected_container->scene_id,&rect);

					cursor->selected_ofset_ext[0]=cursor->cursor_pos[0]-rect.pos[0];
					cursor->selected_ofset_ext[1]=cursor->cursor_pos[1]-rect.pos[1];

					cursor->selected_container_id=selected_container->id;
				}
				else
				{
					selected_container		=PTR_NULL;
					cursor->selected_container_id	=0xFFFFFFFF;
				}
			}

		}
		if(!strcmp_c(event_name,"button 2"))
		{
			cursor->buttons[1]=value;
		}



		if(!strcmp_c(event_name,"X"))
		{
			
			cursor->cursor_pos[0]=value;

			if(selected_container!=PTR_NULL)
				selected_container->rect.pos[0]=(cursor->cursor_pos[0]-cursor->selected_ofset[0]);

		}

		if(!strcmp_c(event_name,"Y"))
		{
			
			cursor->cursor_pos[1]=value;
			
			if(selected_container!=PTR_NULL)
				selected_container->rect.pos[1]=(cursor->cursor_pos[1]-cursor->selected_ofset[1]);
		}
	
		release_zone_ref(&val_node);
	}

	
#endif

	return 1;

}
*/



OS_API_C_FUNC(unsigned int) gfx_init	()
{
	mem_stream				file;
	gfx_render_container_t  *container,*container_last;
	

	kernel_log_id			=	get_new_kern_log_id("gfxbase:",0x0D);

	kernel_log				(kernel_log_id,"gfx alloc \n");


	
	container_list.zone		=	PTR_NULL;

	
	allocate_new_zone	(0x00,sizeof(gfx_render_container_t)*8	,&container_list);

	/*
	allocate_new_zone	(0x00,sizeof(struct gfx_cursor_t)*8		,&cursor_ref);
	n_cursors			=	0;
	max_z_order			=	0;


	kernel_log	(kernel_log_id,"loading cursor \n");

	if(file_system_open_file	("isodisk","/system/imgs/cursor.png",&file)!=1)
	{
		kernel_log	(kernel_log_id,"cannot load cursor img \n");
		return 0;
	}
	gfx_load_png_image	(&cursor_imgs[GFX_CURSOR_DEFAULT],&file);
	mem_stream_close	(&file);

	if(file_system_open_file	("isodisk","/system/imgs/cursor_2.png",&file)!=1)
	{
		kernel_log	(kernel_log_id,"cannot load cursor img sel\n");
		return 0;
	}
	gfx_load_png_image	(&cursor_imgs[GFX_CURSOR_SELECTED],&file);
	mem_stream_close	(&file);

	if(file_system_open_file	("isodisk","/system/imgs/cursor_3.png",&file)!=1)
	{
		kernel_log	(kernel_log_id,"cannot load cursor img sel\n");
		return 0;
	}
	gfx_load_png_image	(&cursor_imgs[GFX_CURSOR_MOVE_RIGHT],&file);
	mem_stream_close	(&file);


	cursor			 =	gfx_get_new_cursor	();

	cursor->image_id = GFX_CURSOR_DEFAULT;

	*/

	init_render_list(64);

	gfx_ctrl_init	();
	gfx_init_scenes	(64);

	container		=	get_zone_ptr(&container_list,0);
	container_last	=	get_zone_ptr(&container_list,0xFFFFFFFF);


	kernel_log				(kernel_log_id,"gfx init \n");

	while(container<container_last)
	{
		container->id=0xFFFFFFFF;
		container->event_array.zone=PTR_NULL;
		container++;
	}

	task_manager_set_container_list		(&container_list);

	bus_manager_add_dev_event_listener(DEVICE_TYPE_MOUSE,gfx_mouse_event);
	bus_manager_add_dev_event_listener(DEVICE_TYPE_JOYSTICK,gfx_joystick_event);

	init_font_loader();
	next_id=1;
	

	return 1;
}



gfx_render_container_t *get_new_container()
{
	gfx_render_container_t	*container,*container_last;
	gfx_render_container_t	*new_container;
	unsigned int			cnt;

	container		=	get_zone_ptr(&container_list,0);
	container_last	=	get_zone_ptr(&container_list,0xFFFFFFFF);
	new_container	=	PTR_NULL;
	cnt				=	0;

	while(container<container_last)
	{
		if(container->id==0xFFFFFFFF)
		{
			new_container=container;
			break;
		}
		cnt++;
		container++;
	}

	if(new_container == PTR_NULL)
	{
		if(expand_zone(&container_list,(cnt+1)*sizeof(gfx_render_container_t *))<0){
			kernel_log(kernel_log_id,"unable to realloc container list \n");
			return PTR_NULL;
		}

		container				=	get_zone_ptr	(&container_list,0);
		container_last			=	get_zone_ptr	(&container_list,0xFFFFFFFF);

		new_container			=	&container[cnt];
		container				=	new_container;

		while(container<container_last)
		{
			container->id				=	0xFFFFFFFF;
			container->event_array.zone	=	PTR_NULL;
			container++;
		}
	}
	
	
	new_container->id					=	++next_id;
	
	new_container->zoom_fac[0]			=	128;
	new_container->zoom_fac[1]			=	128;
	new_container->trans[0]				=	0;
	new_container->trans[1]				=	0;

	new_container->event_array.zone		=	PTR_NULL;
	new_container->event_list_sem		=	task_manager_new_semaphore	(1,4);

	init_node_array			(&new_container->event_array,1024,NODE_GFX_EVENT,8);

	writestr_fmt			("event list created [%d] %p \n",new_container->id,new_container->event_array.zone);

	
	return new_container;
	
}


OS_API_C_FUNC(unsigned int)	gfx_ctrl_new		(struct obj_array_t *ctrl_data,const char *style)
{
	if(!tree_manager_create_obj_array				(ctrl_data))return 0;
	tree_manager_add_obj							(ctrl_data,"style_str"	,NODE_GFX_STR);
	tree_manager_add_obj_str_val					(ctrl_data,"style"		,style);
	tree_manager_add_obj_array						(ctrl_data,"item_list"	,NODE_GFX_CTRL_ITEM_LIST);
	return 1;
}

OS_API_C_FUNC(unsigned int)	gfx_ctrl_new_row		(struct obj_array_t *ctrl_data,const char *style,const char *cols)
{
	size_t pos,len;

	if(!tree_manager_create_obj_array				(ctrl_data))return 0;
	tree_manager_add_obj							(ctrl_data,"style_str"	,NODE_GFX_STR);
	tree_manager_add_obj_str_val					(ctrl_data,"style"		,style);
	tree_manager_add_obj_array						(ctrl_data,"column list",NODE_GFX_CTRL_DATA_COLUMN_LIST);
	
	pos		=	0;
	len		=	strlen_c(cols);

	while(pos<len)
	{
		char				col_name[256];
		char				col_fld[256];
		char				col_style[256];
		size_t				next_pos,next_pos_x;
		


		tree_manager_add_obj			(ctrl_data,"data_column",NODE_GFX_CTRL_DATA_COLUMN);

		next_pos	=	strlpos_c(cols,pos,':');
		if(next_pos==INVALID_SIZE)break;

		strncpy_c			(col_name,&cols[pos],next_pos-pos);
		pos			=	next_pos+1;

		
		tree_manager_add_obj_str_val	(ctrl_data,"label"		,col_name);
		
		next_pos	=	strlpos_c(cols,pos,',');
		if(next_pos==INVALID_SIZE)next_pos=len;

		next_pos_x	=	strlpos_c(cols,pos,'{');
		
		if(next_pos_x==INVALID_SIZE)
			next_pos_x=next_pos;

		if(next_pos_x>next_pos)
			next_pos_x=next_pos;

		if(next_pos_x>pos)
		{
			strncpy_c			(col_fld,&cols[pos],next_pos_x-pos);
		}

		if(next_pos_x<next_pos)
		{
			size_t next_pos_xx;
			
			next_pos_xx=strlpos_c(cols,pos,'}');
			if(next_pos_xx!=INVALID_SIZE)
			{
				strcpy_c						(col_style,"{(\"style\",0x04000020)");
				strncat_c						(col_style,&cols[next_pos_x+1],(next_pos_xx+1)-(next_pos_x+1));
				tree_manager_add_obj_str_val	(ctrl_data,"style_str"		,col_style);
				next_pos	=	next_pos_xx+1;
			}
		}
		
		pos=next_pos+1;
				
		tree_manager_add_obj_str_val	(ctrl_data,"field"		,col_fld);
	}

	tree_manager_end_obj_array		(ctrl_data);
	tree_manager_add_obj_array		(ctrl_data,"item_list"	,NODE_GFX_CTRL_ITEM_LIST);

	return 1;
}


OS_API_C_FUNC(unsigned int)	gfx_ctrl_add_item (struct obj_array_t *ctrl_data,const char *name,const char *label,unsigned int item_id)
{
	tree_manager_add_obj						(ctrl_data,name		,NODE_GFX_CTRL_ITEM);
	tree_manager_add_obj_int_val				(ctrl_data,"item_id",item_id);
	tree_manager_add_obj_str_val				(ctrl_data,"label"	,label);

	return 1;
}



OS_API_C_FUNC(unsigned int)	gfx_ctrl_create_object (struct obj_array_t *ctrl_data,const char *class_name,const char *name,mem_zone_ref_ptr ctrl_data_node)
{
	unsigned int	n_col;
	mem_zone_ref	col={PTR_NULL};
	mem_zone_ref	col_list={PTR_NULL};

	tree_manager_end_obj_array				(ctrl_data);
	tree_manager_end_obj_array				(ctrl_data);

	if(!tree_manager_create_node_childs		(name,NODE_GFX_CTRL,ctrl_data_node,get_zone_ptr(&ctrl_data->char_buffer,0)))return 0;
	
	tree_manager_set_child_value_str		(ctrl_data_node,"ctrl class",class_name);

	tree_node_find_child_by_type			(ctrl_data_node,NODE_GFX_CTRL_DATA_COLUMN_LIST,&col_list,0);

	n_col			=	0;
	while(tree_manager_get_child_at(&col_list,n_col++,&col))
	{
		char				*style_str;
		mem_zone_ref		style_node={PTR_NULL};
		
		if(!tree_manager_get_child_data_ptr			(&col,NODE_HASH("style_str"),&style_str))continue;
		if(!tree_manager_create_node_params			(&style_node,style_str))continue;
		tree_manager_node_add_child					(&col,&style_node);
		release_zone_ref							(&style_node);
		release_zone_ref							(&col);
	}

	release_zone_ref							(&col_list);

	return 1;

	
}

OS_API_C_FUNC(unsigned int)	gfx_ctrl_add_item_data (mem_zone_ref_ptr ctrl_node,unsigned int item_id,mem_zone_ref_ptr item_data)
{
	int ret;
	mem_zone_ref	item_node		={PTR_NULL};
	mem_zone_ref	item_list		={PTR_NULL};

	if(!tree_node_find_child_by_type		(ctrl_node,NODE_GFX_CTRL_ITEM_LIST,&item_list,0))return 0;

	if(tree_find_child_node_by_id_name		(&item_list,NODE_GFX_CTRL_ITEM,"item_id",item_id,&item_node))
	{
		mem_zone_ref	item_data_node	={PTR_NULL};

		if(!tree_manager_find_child_node		(&item_node,NODE_HASH("item_data"),NODE_GFX_CTRL_ITEM_DATA,&item_data_node))
			tree_manager_add_child_node			(&item_node,"item_data",NODE_GFX_CTRL_ITEM_DATA,&item_data_node);

		ret = tree_manager_node_add_child		(&item_data_node,item_data);
		release_zone_ref						(&item_data_node);
		release_zone_ref						(&item_node);
	}

	release_zone_ref						(&item_list);
	
	return ret;
}



OS_API_C_FUNC(unsigned int)	gfx_container_add_text (unsigned int container_id,const char *text,const char *style)
{
	gfx_render_container_t			*container;
		
	container	=	get_container_by_id(container_id);
	if(container == PTR_NULL)
	{
		kernel_log		(kernel_log_id,"add text could not find container ");
		writeint		(container_id,16);
		writestr		("\n");
		return 0;
	}

	//,int p_x,int p_y,const char *font_name,unsigned int size_x,unsigned int size_y
	//,p_x,p_y,font_name,size_x,size_y,black

	return gfx_scene_add_text		(container->scene_id,text,style);
}

OS_API_C_FUNC(unsigned int)	gfx_container_add_ctrl		(unsigned int container_id,mem_zone_ref_ptr ctrl_data_node)
{
	gfx_render_container_t			*container;

	container	=	get_container_by_id(container_id);
	if(container == PTR_NULL)
	{
		kernel_log		(kernel_log_id,"add text could not find container ");
		writeint		(container_id,16);
		writestr		("\n");
		return 0;
	}

	return gfx_scene_add_ctrl		(container->scene_id,ctrl_data_node);
}

OS_API_C_FUNC(unsigned int)	gfx_container_add_obj_style (unsigned int container_id,unsigned int obj_id,const char *style)
{
	gfx_render_container_t			*container;
		
	container	=	get_container_by_id(container_id);
	if(container == PTR_NULL)
	{
		kernel_log		(kernel_log_id,"add_obj_style tcould not find container ");
		writeint		(container_id,16);
		writestr		("\n");
		return 0;
	}

	return gfx_scene_add_obj_style	(container->scene_id,obj_id,style);
}

OS_API_C_FUNC(unsigned int)	gfx_container_rem_obj	(unsigned int container_id,unsigned int obj_id)
{
	gfx_render_container_t			*container;
		
	container	=	get_container_by_id(container_id);
	if(container == PTR_NULL)
	{
		kernel_log		(kernel_log_id,"add_obj_style tcould not find container ");
		writeint		(container_id,16);
		writestr		("\n");
		return 0;
	}

	return gfx_scene_rem_obj	(container->scene_id,obj_id);
}
OS_API_C_FUNC(unsigned int)	gfx_container_add_text_list (unsigned int container_id,const char *style)
{
	gfx_render_container_t			*container;
		
	container	=	get_container_by_id(container_id);
	if(container == PTR_NULL)
	{
		kernel_log		(kernel_log_id,"add text lst tcould not find container ");
		writeint		(container_id,16);
		writestr		("\n");
		return 0;
	}
	return gfx_scene_add_text_list	(container->scene_id,style);
}

OS_API_C_FUNC(unsigned int)	gfx_container_add_text_to_list (unsigned int container_id,unsigned int list_id,const char *text,const char *style)
{
	gfx_render_container_t			*container;
	gfx_scene_t						*scene;
		
	container	=	get_container_by_id(container_id);
	if(container == PTR_NULL)
	{
		kernel_log		(kernel_log_id,"add text find container \n");
		return 0;
	}

	scene	=	gfx_find_scene(container->scene_id);
	if(scene == PTR_NULL)
	{
		kernel_log	(kernel_log_id,"cannot find scene ");
		writeint	(container->scene_id,16);
		writestr	("\n");
		return 0;
	}
	return gfx_scene_add_text_to_list	(scene,list_id,text,style);
}


OS_API_C_FUNC(unsigned int)	gfx_container_add_image (unsigned int container_id,unsigned int p_x,unsigned int p_y,const char *fs_name,const char *image_path)
{
	gfx_render_container_t		*container;
	container	=	get_container_by_id(container_id);
	if(container == PTR_NULL)
	{
		kernel_log		(kernel_log_id,"could not find container [");
		writeint		(container_id,16);
		writestr		("]\n");
		return 0;
	}
	return gfx_scene_add_image		(container->scene_id,p_x,p_y,fs_name,image_path);
}

OS_API_C_FUNC(unsigned int)	gfx_container_get_events (unsigned int container_id)
{
	
	return 1;
}

void add_container_windows (gfx_render_container_t	*container,unsigned int border,const char *name)
{
	gfx_rect_t				left_rect,top_rect,right_rect,bottom_rect;
	mem_zone_ref			new_text={PTR_NULL};
	struct obj_array_t		rect_style_ar;
	char					*txt_style="{(\"style\",0x04000020)p_x:4,p_y:-16,font_name:\"Flama\",font_size_x:12,font_size_y:12,text_color:0xFF0000FF}";


	container->ctrl_scene_id	=	gfx_get_new_scene(name);

	left_rect.pos[0]		=	-(int)border;
	left_rect.pos[1]		=	0;
	left_rect.size[0]		=	border;
	left_rect.size[1]		=	container->rect.size[1];

	right_rect.pos[0]		=	container->rect.size[0];
	right_rect.pos[1]		=	0;
	right_rect.size[0]		=	border;
	right_rect.size[1]		=	container->rect.size[1];

	top_rect.pos[0]			=	-(int)border;
	top_rect.pos[1]			=	-20;
	top_rect.size[0]		=	container->rect.size[0]+border*2;
	top_rect.size[1]		=	20;

	bottom_rect.pos[0]		=	-(int)border;
	bottom_rect.pos[1]		=	container->rect.size[1];
	bottom_rect.size[0]		=	container->rect.size[0]+border*2;
	bottom_rect.size[1]		=	border;

	if(!tree_manager_create_obj						(&rect_style_ar))
	{
		kernel_log(kernel_log_id,"unable to create rect style \n");
		return ;
	}

	gfx_create_rect_style	(&rect_style_ar,&left_rect,white);
	gfx_scene_add_rect		(container->ctrl_scene_id,get_zone_ptr(&rect_style_ar.char_buffer,0));
	release_zone_ref		(&rect_style_ar.char_buffer);

	gfx_create_rect_style	(&rect_style_ar,&right_rect,white);
	gfx_scene_add_rect		(container->ctrl_scene_id,get_zone_ptr(&rect_style_ar.char_buffer,0));
	release_zone_ref		(&rect_style_ar.char_buffer);

	gfx_create_rect_style	(&rect_style_ar,&top_rect,white);
	container->top_rect_id	=	gfx_scene_add_rect		(container->ctrl_scene_id,get_zone_ptr(&rect_style_ar.char_buffer,0));
	release_zone_ref		(&rect_style_ar.char_buffer);

	gfx_create_rect_style	(&rect_style_ar,&bottom_rect,white);
	gfx_scene_add_rect		(container->ctrl_scene_id,get_zone_ptr(&rect_style_ar.char_buffer,0));
	release_zone_ref		(&rect_style_ar.char_buffer);

	gfx_scene_add_text		(container->ctrl_scene_id,name,txt_style);

}



OS_API_C_FUNC(unsigned int) gfx_create_render_container (const char *container_name,int x,int y,unsigned int width,unsigned int height)
{
	
	gfx_render_container_t	*new_container;
	mem_zone_ref			new_text={PTR_NULL};
	mem_zone_ref			text_node={PTR_NULL};
	

	new_container	=	get_new_container();
	if(new_container == PTR_NULL)
	{
		kernel_log		(kernel_log_id,"could not create new container \n");
		return 0;
	}

	new_container->scene_id		=	gfx_get_new_scene(container_name);

	new_container->rect.pos[0]	=	x;
	new_container->rect.pos[1]	=	y;

	new_container->rect.size[0]	=	width;
	new_container->rect.size[1] =	height;

	new_container->z_order		=	++max_z_order;
	
	add_container_windows			(new_container,4,container_name);
	
	/*
	if(task_func!=PTR_NULL)
	{
		mem_zone_ref			task_data={PTR_NULL};

		tree_manager_create_node				("data",NODE_TASK_DATA,&task_data);
		tree_manager_set_child_value_i32		(&task_data,"container id",new_container->id);
		new_container->task_id		=task_manager_new_task(container_name,task_func,&task_data,0);
		release_zone_ref						(&task_data);
	}
	*/

	return new_container->id;
}




OS_API_C_FUNC(unsigned int)	gfx_container_get_obj_node	(unsigned int container_id,unsigned int obj_id,mem_zone_ref_ptr	node)
{
	
	gfx_render_container_t		*container;
	container	=	get_container_by_id(container_id);
	if(container	==PTR_NULL)return 0;

	return gfx_get_scene_obj	(container->scene_id,obj_id,node);
}
		


OS_API_C_FUNC(unsigned int)	gfx_container_process_events	(unsigned int container_id)
{
	mem_zone_ref				event_node	={PTR_NULL};
	mem_zone_ref				obj_node	={PTR_NULL};
	gfx_render_container_t		*container;
	int							processed;
	
	

	container	=	get_container_by_id(container_id);
	
	while(gfx_pop_container_event(container_id,&event_node)==1)
	{
		
		mem_zone_ref		crtl_event_node={PTR_NULL};
		mem_zone_ref		ctrl_item_data_node={PTR_NULL};
		unsigned int		bid;
		
		processed =		1;
		bid		  =		gfx_scene_event_test(container->scene_id,&event_node,container->trans,&crtl_event_node);
	
		switch(gfx_render_containers_get_obj_type(container_id,bid))
		{
			case NODE_GFX_CTRL:

				if(gfx_get_scene_obj					(container->scene_id,bid,&obj_node))
				{
					gfx_ctrl_event						(&obj_node,&crtl_event_node);
				}
				

			break;
		}
		
		tree_manager_write_node_dword		(&event_node,0,0);
		release_zone_ref					(&event_node);
		
	}

	return 1;
}


OS_API_C_FUNC(void) gfx_process_all_containers_events()
{
	unsigned int count;
	gfx_render_container_t	*container,*container_last;

	container		=	get_zone_ptr(&container_list,0);
	container_last	=	get_zone_ptr(&container_list,0xFFFFFFFF);

	count=0;

	while(container<container_last)
	{
		if(container->id!=0xFFFFFFFF)
		{
			gfx_container_process_events(container->id);
		}
		container++;
	}
	return ;
}

OS_API_C_FUNC(unsigned int) gfx_dump_container			(unsigned int container_id)
{
	gfx_scene_t				*scene;
	gfx_render_container_t	*container;

	container	=	get_container_by_id(container_id);
	if(container == PTR_NULL)
	{
		kernel_log		(kernel_log_id,"could find container [");
		writeint		(container_id,0x16);
		writestr		("]\n");
		return 0;
	}
	scene	=	gfx_find_scene(container->scene_id);
	if(scene == PTR_NULL)
	{
		kernel_log	(kernel_log_id,"cannot find container scene [");
		writeint	(container->scene_id,16);
		writestr	("]\n");
		return 0;
	}

	tree_manager_dump_node_rec(&scene->root,0,2);

	kernel_log	(kernel_log_id,"container id :");
	writeint	(container->id,10);
	writestr	(" size : [");
	writeint	(container->rect.pos[0],10);
	writestr	(" ");
	writeint	(container->rect.pos[1],10);
	writestr	(" ");
	writeint	(container->rect.size[0],10);
	writestr	(" ");
	writeint	(container->rect.size[1],10);
	writestr	("]\n");
	return 1;
}


OS_API_C_FUNC(void) gfx_dump_all_containers()
{
	unsigned int count;
	gfx_render_container_t	*container,*container_last;

	container		=	get_zone_ptr(&container_list,0);
	container_last	=	get_zone_ptr(&container_list,0xFFFFFFFF);

	kernel_log	(kernel_log_id,"container list ");
	writeptr	(container);
	writeptr	(container_last);
	writestr	("\n");

	count=0;

	while(container<container_last)
	{
		kernel_log	(kernel_log_id,"container list #");
		writeint	(count,10);
		writestr	(" ");
		writeptr	(container);
		writestr	("\n");
		if(container->id!=0xFFFFFFFF)
		{
			gfx_dump_container(container->id);
		}
		container++;
		count++;
	}
	return ;
}

OS_API_C_FUNC(unsigned int) gfx_render_container_dump(unsigned int container_id)
{
	gfx_render_container_t		*container;
	container	=	get_container_by_id(container_id);
	if(container == PTR_NULL)
	{
		kernel_log		(kernel_log_id,"could find container (dump)\n");
		return 0;
	}
	kernel_log	(kernel_log_id,"container :'");
	writeint	(container->rect.pos[0],10);
	writestr	(" ");
	writeint	(container->rect.pos[1],10);
	writestr	(" ");
	writeint	(container->rect.size[0],10);
	writestr	(" ");
	writeint	(container->rect.size[1],10);
	writestr	("\n");
	return 1;
}
OS_API_C_FUNC(void) gfx_render_containers_set_scroll_y(unsigned int container_id,int scroll_x,int scroll_y)
{
	gfx_render_container_t		*container;

	container	=	get_container_by_id(container_id);
	if(container == PTR_NULL)
	{
		kernel_log		(kernel_log_id,"could not find container (set scroll)\n");
		return ;
	}

	container->trans[0]=scroll_x;
	container->trans[1]=scroll_y;
}



OS_API_C_FUNC(int) gfx_render_containers_set_pos(unsigned int container_id,unsigned int pos_x,unsigned int pos_y)
{
	gfx_render_container_t		*container;

	container	=	get_container_by_id(container_id);
	if(container == PTR_NULL)
	{
		kernel_log		(kernel_log_id,"could not set container pos ");
		writeint		(container_id,16);
		writestr		("\n");
		return 0;
	}

	container->rect.pos[0]=pos_x;
	container->rect.pos[1]=pos_y;

	return 1;
}

OS_API_C_FUNC(int) gfx_render_containers_get_cursor_abs(unsigned int container_id,const vec_2s_t cursor,vec_2s_t out)
{
	gfx_render_container_t		*container;

	container	=	get_container_by_id(container_id);
	if(container == PTR_NULL)
	{
		kernel_log		(kernel_log_id,"could not get container pos ");
		writeint		(container_id,16);
		writestr		("\n");
		return 0;
	}

	out[0]=cursor[0]+container->rect.pos[0];
	out[1]=cursor[1]+container->rect.pos[1];
	return 1;
}

OS_API_C_FUNC(int) gfx_render_containers_get_pos(unsigned int container_id,unsigned int *pos_x,unsigned int *pos_y)
{
	gfx_render_container_t		*container;

	container	=	get_container_by_id(container_id);
	if(container == PTR_NULL)
	{
		kernel_log		(kernel_log_id,"could not get container pos ");
		writeint		(container_id,16);
		writestr		("\n");
		return 0;
	}

	(*pos_x)=container->rect.pos[0];
	(*pos_y)=container->rect.pos[1];

	return 1;
}


OS_API_C_FUNC(int) gfx_render_containers_get_size(unsigned int container_id,unsigned int *size_x,unsigned int *size_y)
{
	gfx_render_container_t		*container;

	container	=	get_container_by_id(container_id);
	if(container == PTR_NULL)
	{
		kernel_log		(kernel_log_id,"could not get container pos ");
		writeint		(container_id,16);
		writestr		("\n");
		return 0;
	}

	(*size_x)=container->rect.size[0];
	(*size_y)=container->rect.size[1];

	return 1;
}

OS_API_C_FUNC(void) gfx_render_containers_set_zoom	(unsigned int container_id,vec_2s_t zoom)
{
	gfx_render_container_t		*container;

	container	=	get_container_by_id(container_id);
	if(container == PTR_NULL)
	{
		kernel_log		(kernel_log_id,"could not find container (set zoom)\n");
		return ;
	}

	container->zoom_fac[0]=zoom[0];
	container->zoom_fac[1]=zoom[1];
}


/*
OS_API_C_FUNC(unsigned int ) gfx_render_containers_get_selected(unsigned int container_id,vec_2s_t cursor,mem_zone_ref_ptr event_node)
{
	gfx_render_container_t		*container;

	

	container	=	get_container_by_id(container_id);
	if(container == PTR_NULL)
	{
		kernel_log		(kernel_log_id,"could not find container (extent)\n");
		return 0;
	}

	return gfx_scene_get_selected	(container->scene_id,cursor,container->trans,event_node);
}
*/
OS_API_C_FUNC(unsigned int ) gfx_render_containers_set_obj_prop(unsigned int container_id,unsigned int obj_id,const char *name,unsigned int val)
{
	gfx_render_container_t		*container;
	mem_zone_ref				sel_node={PTR_NULL};
	

	container	=	get_container_by_id(container_id);
	if(container == PTR_NULL)
	{
		kernel_log		(kernel_log_id,"could not find container (extent)\n");
		return 0;
	}


	return gfx_set_scene_obj_val_ui(container->scene_id,obj_id,name,val);
}

OS_API_C_FUNC(unsigned int ) gfx_render_containers_get_obj_prop(unsigned int container_id,unsigned int obj_id,const char *name,unsigned int *val)
{
	gfx_render_container_t		*container;
	mem_zone_ref				sel_node={PTR_NULL};
	

	container	=	get_container_by_id(container_id);
	if(container == PTR_NULL)
	{
		kernel_log		(kernel_log_id,"could not find container (extent)\n");
		return 0;
	}


	return gfx_get_scene_obj_val_ui(container->scene_id,obj_id,name,val);
}

OS_API_C_FUNC(unsigned int ) gfx_render_containers_get_obj_prop_str(unsigned int container_id,unsigned int obj_id,const char *name,char *val,unsigned int str_len)
{
	gfx_render_container_t		*container;
	mem_zone_ref				sel_node={PTR_NULL};
	

	container	=	get_container_by_id(container_id);
	if(container == PTR_NULL)
	{
		kernel_log		(kernel_log_id,"could not find container (extent)\n");
		return 0;
	}


	return gfx_get_scene_obj_val_str(container->scene_id,obj_id,name,val,str_len);
}

OS_API_C_FUNC(unsigned int ) gfx_render_containers_get_obj_prop_by_type(unsigned int container_id,unsigned int obj_id,unsigned int type,mem_zone_ref_ptr out)
{
	gfx_render_container_t		*container;
	mem_zone_ref				sel_node={PTR_NULL};
	

	container	=	get_container_by_id(container_id);
	if(container == PTR_NULL)
	{
		kernel_log		(kernel_log_id,"could not find container (extent)\n");
		return 0;
	}
	return gfx_get_scene_obj_prop_by_type(container->scene_id,obj_id,type,out);
	//unsigned int  gfx_get_scene_obj_val_str(container->scene_id,obj_id,name,val,str_len);
}





OS_API_C_FUNC(unsigned int ) gfx_render_containers_get_obj_type(unsigned int container_id,unsigned int obj_id)
{
	gfx_render_container_t		*container;
	mem_zone_ref				sel_node={PTR_NULL};

	if(obj_id==0)return 0;

	container	=	get_container_by_id(container_id);
	if(container == PTR_NULL)
	{
		kernel_log		(kernel_log_id,"could not find container (extent)\n");
		return 0;
	}

	return gfx_get_scene_obj_type(container->scene_id,obj_id);
}




OS_API_C_FUNC(unsigned int ) gfx_render_containers_get_extent(unsigned int container_id,struct gfx_rect *out_rect)
{
	gfx_render_container_t		*container;

	container	=	get_container_by_id(container_id);
	if(container == PTR_NULL)
	{
		kernel_log		(kernel_log_id,"could not find container (extent)\n");
		return 0;
	}
	gfx_scene_get_extent(container->scene_id,out_rect);

	return 1;

}


