


int						init_render_list			(unsigned int num);
int						gfx_set_render_device		(unsigned int bus_drv_id,unsigned int dev_id);
void					gfx_render_cursor			(unsigned int render_id,struct gfx_cursor_t	*cursor);
extern mem_zone_ref		cursor_ref;