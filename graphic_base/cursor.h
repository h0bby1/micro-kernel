struct gfx_cursor_t
{
unsigned int			visible;
unsigned int			last_acvity;

cursor_imgs_idx			image_id;

unsigned int 			dragged_cont_id;
unsigned int 			dragged_ctrl_id;
unsigned int 			dragged_ctrl_obj_id;

vec_2s_t				cursor_pos;

vec_2s_t				drag_start_pos;

vec_2s_t				selected_ofset;
vec_2s_t				selected_ofset_ext;

unsigned int			mouse_over_container_id;
unsigned int			top_clicked_container_id;
vec_2s_t				top_sel_ofs;
unsigned int			buttons[8];
};

unsigned int			gfx_init_cursors		();
struct gfx_cursor_t	*	gfx_get_new_cursor		();
unsigned int			gfx_load_cursor_image	(const char *fs_name,const char *file_path,cursor_imgs_idx img_idx);
gfx_image_t				*gfx_get_cursor_image	(cursor_imgs_idx img_idx);