#define GFX_LIB_API C_EXPORT

#include <std_def.h>
#include <std_mem.h>
#include <kern.h>
#include <mem_base.h>
#include <lib_c.h>
#include <tree.h>
#include <sys/mem_stream.h>
#include <sys/file_system.h>
#include <sys/tpo_mod.h>
#include <sys/task.h>
#include <sys/ctrl.h>

#include "../filters/filters.h"
#include "gfx/graphic_object.h"
#include "gfx/graphic_base.h"
#include "../../kernel/bus_manager/bus_drv.h"


#include "gfx_lib/scene.h"



const		 vec_4uc_t	white					=	{0xFF,0xFF,0xFF,0xFF};
const		 vec_4uc_t	black					=	{0x00,0x00,0x00,0xFF};
unsigned int container_next_id					=	0x01;
unsigned int kernel_log_id						=	0xFFFFFFFF;


gfx_render_container_t *get_new_container()
{
	gfx_render_container_t	*container,*container_last;
	gfx_render_container_t	*new_container;
	char					 cont_id[32];
	char					 cont_name[64];
	unsigned int			cnt,new_id;
	mem_zone_ref			container_list={PTR_NULL};
	
	if(!task_manager_get_container_list(&container_list))
	{
		kernel_log(kernel_log_id,"could not find container list \n");
		return 0;
	}

	if(get_zone_size(&container_list)==0)
	{
		if(!allocate_new_zone					(0,16*sizeof(gfx_render_container_t),&container_list))
		{
			kernel_log(kernel_log_id,"could not allocate container list \n");
		}
		

		container		=	get_zone_ptr(&container_list,0);
		container_last	=	get_zone_ptr(&container_list,0xFFFFFFFF);

		while(container<container_last)
		{
			container->id				=	0xFFFFFFFF;
			container->event_array.zone	=	PTR_NULL;
			container++;
		}

		task_manager_set_container_list		(&container_list);

		new_container	=	get_zone_ptr(&container_list,0);
	}
	else
	{
		container		=	get_zone_ptr(&container_list,0);
		container_last	=	get_zone_ptr(&container_list,0xFFFFFFFF);
		new_container	=	PTR_NULL;
		cnt				=	0;

		while(container<container_last)
		{
			if(container->id==0xFFFFFFFF)
			{
				new_container=container;
				break;
			}
			cnt++;
			container++;
		}
		if(new_container == PTR_NULL)
		{
			if(expand_zone(&container_list,(cnt+1)*sizeof(gfx_render_container_t *))<0){
				kernel_log(kernel_log_id,"unable to realloc container list \n");
				return PTR_NULL;
			}

			task_manager_set_container_list				(&container_list);

			container				=	get_zone_ptr	(&container_list,0);
			container_last			=	get_zone_ptr	(&container_list,0xFFFFFFFF);

			new_container			=	&container[cnt];
			container				=	new_container;

			while(container<container_last)
			{
				container->id				=	0xFFFFFFFF;
				container->event_array.zone	=	PTR_NULL;
				container++;
			}
		}
	}

	
	
	new_id								=	++container_next_id;
	
	new_container->zoom_fac[0]			=	128;
	new_container->zoom_fac[1]			=	128;
	new_container->trans[0]				=	0;
	new_container->trans[1]				=	0;

	new_container->event_array.zone		=	PTR_NULL;
	new_container->event_list_sem		=	task_manager_new_semaphore	(1,4);

	itoa_s								(new_id,cont_id,32,10);
	strcpy_s							(cont_name,64,"container [");
	strcat_s							(cont_name,64,cont_id);
	strcat_s							(cont_name,64,"]");

	init_node_array						(&new_container->event_array,32,cont_name,NODE_GFX_EVENT,8);

	
	new_container->id					=	new_id;
	
	return new_container;
	
}




gfx_render_container_t	*get_container_by_id(unsigned int container_id)
{
	gfx_render_container_t	*container,*container_last;
	mem_zone_ref			container_list={PTR_NULL};
	
	if(container_id==0xFFFFFFFF)return PTR_NULL;

	if(!task_manager_get_container_list(&container_list))return 0;

	container		=	get_zone_ptr(&container_list,0);
	container_last	=	get_zone_ptr(&container_list,0xFFFFFFFF);
	
	while((container<container_last)&&(container->id!=0xFFFFFFFF))
	{
		if(container->id==container_id)return container;
		container++;
	}
	return PTR_NULL;
}

unsigned int	gfx_container_remove_all_text (unsigned int container_id)
{	
	gfx_render_container_t		*container;
	container	=	get_container_by_id(container_id);
	if(container == PTR_NULL)
	{
		kernel_log		(kernel_log_id,"could not find container (remove txt) \n");
		return 0;
	}
	tree_remove_child_by_type	(&container->scene_ref,NODE_GFX_TEXT		);
	tree_remove_child_by_type	(&container->scene_ref,NODE_GFX_TEXT_LIST	);
	return 1;
}

OS_API_C_FUNC(unsigned int)	gfx_container_add_text (unsigned int container_id,const char *text,const char *style)
{
	gfx_render_container_t			*container;
		
	container	=	get_container_by_id(container_id);
	if(container == PTR_NULL)
	{
		kernel_log		(kernel_log_id,"add text could not find container ");
		writeint		(container_id,16);
		writestr		("\n");
		return 0;
	}

	//,int p_x,int p_y,const char *font_name,unsigned int size_x,unsigned int size_y
	//,p_x,p_y,font_name,size_x,size_y,black

	return gfx_scene_add_text		(&container->scene_ref,text,style);
}

OS_API_C_FUNC(unsigned int)	gfx_container_add_ctrl		(unsigned int container_id,mem_zone_ref_ptr ctrl_data_node)
{
	gfx_render_container_t			*container;

	container	=	get_container_by_id(container_id);
	if(container == PTR_NULL)
	{
		kernel_log		(kernel_log_id,"add text could not find container ");
		writeint		(container_id,16);
		writestr		("\n");
		return 0;
	}

	return gfx_scene_add_ctrl		(&container->scene_ref,ctrl_data_node);
}

OS_API_C_FUNC(unsigned int) gfx_container_add_obj_style (unsigned int container_id,unsigned int obj_id,const char *style)
{
	gfx_render_container_t			*container;
		
	container	=	get_container_by_id(container_id);
	if(container == PTR_NULL)
	{
		kernel_log		(kernel_log_id,"add_obj_style tcould not find container ");
		writeint		(container_id,16);
		writestr		("\n");
		return 0;
	}

	return gfx_scene_add_obj_style	(&container->scene_ref,obj_id,style);
}

OS_API_C_FUNC(unsigned int) gfx_container_rem_obj	(unsigned int container_id,unsigned int obj_id)
{
	gfx_render_container_t			*container;
		
	container	=	get_container_by_id(container_id);
	if(container == PTR_NULL)
	{
		kernel_log		(kernel_log_id,"add_obj_style tcould not find container ");
		writeint		(container_id,16);
		writestr		("\n");
		return 0;
	}

	return gfx_scene_rem_obj	(&container->scene_ref,obj_id);
}
OS_API_C_FUNC(unsigned int)	gfx_container_add_text_list (unsigned int container_id,const char *style)
{
	gfx_render_container_t			*container;
		
	container	=	get_container_by_id(container_id);
	if(container == PTR_NULL)
	{
		kernel_log		(kernel_log_id,"add text lst tcould not find container ");
		writeint		(container_id,16);
		writestr		("\n");
		return 0;
	}
	return gfx_scene_add_text_list	(&container->scene_ref,style);
}

OS_API_C_FUNC(unsigned int)	gfx_container_add_text_to_list (unsigned int container_id,unsigned int list_id,const char *text,const char *style)
{
	gfx_render_container_t			*container;
		
	container	=	get_container_by_id(container_id);
	if(container == PTR_NULL)
	{
		kernel_log		(kernel_log_id,"could not find container (add txt to list) \n");
		return 0;
	}


	return gfx_scene_add_text_to_list	(&container->scene_ref,list_id,text,style);
}


OS_API_C_FUNC(unsigned int)	gfx_container_add_image (unsigned int container_id,unsigned int p_x,unsigned int p_y,const char *fs_name,const char *image_path)
{
	gfx_render_container_t		*container;
	container	=	get_container_by_id(container_id);
	if(container == PTR_NULL)
	{
		kernel_log		(kernel_log_id,"could not find container [");
		writeint		(container_id,16);
		writestr		("]\n");
		return 0;
	}
	return gfx_scene_add_image		(&container->scene_ref,p_x,p_y,fs_name,image_path);
}


void add_container_windows (gfx_render_container_t	*container,unsigned int border,const char *name)
{
	struct obj_array_t		rect_style_ar;
	char					*txt_style="{(\"style\",0x04000020)p_x:4,p_y:-16,font_name:\"Flama\",font_size_x:12,font_size_y:12,text_color:0xFF0000FF}";


	//container->ctrl_scene_id	=	gfx_get_new_scene(name);
	//copy_zone_ref(&container->ctrl_scene_ref,&gfx_find_scene(gfx_get_new_scene(name))->root);

	tree_manager_create_node			("scene",NODE_GFX_SCENE,&container->ctrl_scene_ref);

	container->ctrl_rect[0].pos[0]		=	-(int)border;
	container->ctrl_rect[0].pos[1]		=	-20;
	container->ctrl_rect[0].size[0]		=	container->rect.size[0]+border*2;
	container->ctrl_rect[0].size[1]		=	20;

	container->ctrl_rect[1].pos[0]		=	container->rect.size[0];
	container->ctrl_rect[1].pos[1]		=	0;
	container->ctrl_rect[1].size[0]		=	border;
	container->ctrl_rect[1].size[1]		=	container->rect.size[1];

	container->ctrl_rect[2].pos[0]		=	-(int)border;
	container->ctrl_rect[2].pos[1]		=	0;
	container->ctrl_rect[2].size[0]		=	border;
	container->ctrl_rect[2].size[1]		=	container->rect.size[1];

	container->ctrl_rect[3].pos[0]		=	-(int)border;
	container->ctrl_rect[3].pos[1]		=	container->rect.size[1];
	container->ctrl_rect[3].size[0]		=	container->rect.size[0]+border*2;
	container->ctrl_rect[3].size[1]		=	border;

	if(!tree_manager_create_obj						(&rect_style_ar))
	{
		kernel_log(kernel_log_id,"unable to create rect style \n");
		return ;
	}

	gfx_create_rect_style	(&rect_style_ar,&container->ctrl_rect[2],white);
	gfx_scene_add_rect		(&container->ctrl_scene_ref,get_zone_ptr(&rect_style_ar.char_buffer,0));
	release_zone_ref		(&rect_style_ar.char_buffer);

	gfx_create_rect_style	(&rect_style_ar,&container->ctrl_rect[1],white);
	gfx_scene_add_rect		(&container->ctrl_scene_ref,get_zone_ptr(&rect_style_ar.char_buffer,0));
	release_zone_ref		(&rect_style_ar.char_buffer);

	gfx_create_rect_style	(&rect_style_ar,&container->ctrl_rect[0],white);
	gfx_scene_add_rect		(&container->ctrl_scene_ref,get_zone_ptr(&rect_style_ar.char_buffer,0));
	release_zone_ref		(&rect_style_ar.char_buffer);

	gfx_create_rect_style	(&rect_style_ar,&container->ctrl_rect[3],white);
	gfx_scene_add_rect		(&container->ctrl_scene_ref,get_zone_ptr(&rect_style_ar.char_buffer,0));
	release_zone_ref		(&rect_style_ar.char_buffer);

	gfx_scene_add_text		(&container->ctrl_scene_ref,name,txt_style);

}



OS_API_C_FUNC(unsigned int) gfx_create_render_container (const char *container_name,int x,int y,unsigned int width,unsigned int height)
{
	gfx_render_container_t	*new_container;


	new_container	=	get_new_container();
	if(new_container == PTR_NULL)
	{
		kernel_log		(kernel_log_id,"could not create new container \n");
		return 0;
	}

	new_container->scene_ref.zone=PTR_NULL;

	//copy_zone_ref(&new_container->scene_ref,&gfx_find_scene(gfx_get_new_scene(container_name))->root);
	tree_manager_create_node			("scene",NODE_GFX_SCENE,&new_container->scene_ref);

	new_container->rect.pos[0]	=	x;
	new_container->rect.pos[1]	=	y;

	new_container->rect.size[0]	=	width;
	new_container->rect.size[1] =	height;

	new_container->z_order		=	0;
	
	add_container_windows			(new_container,4,container_name);
	
	return new_container->id;
}



unsigned int	gfx_container_get_obj_node	(unsigned int container_id,unsigned int obj_id,mem_zone_ref_ptr	node)
{
	
	gfx_render_container_t		*container;
	container	=	get_container_by_id(container_id);
	if(container	==PTR_NULL)return 0;

	return gfx_scene_get_obj	(&container->scene_ref,obj_id,node);
}
		


OS_API_C_FUNC(unsigned int)	gfx_process_events	()
{
	mem_zone_ref				container_list	={PTR_NULL};
	
	mem_zone_ref				obj_node		={PTR_NULL};
	gfx_render_container_t		*container,*container_last;
	
	if(!task_manager_get_container_list(&container_list))return 0;
	container		=	get_zone_ptr(&container_list,0);
	if(container ==	PTR_INVALID)return 0;

	container_last	=	get_zone_ptr(&container_list,0xFFFFFFFF);
	
	while((container<container_last)&&(container->id!=0xFFFFFFFF))
	{
		mem_zone_ref				event_node		={PTR_NULL};

		while(node_array_pop (&container->event_array,&event_node)==1)
		{
			const char			*node_name;
			mem_zone_ref		crtl_event_node={PTR_NULL};
			unsigned int		bid;


			

		    node_name=tree_mamanger_get_node_name	(&event_node);
			if(!strcmp_c(node_name,"item_drag"))
			{
				int				drag_ofset[2];
				mem_zone_ref	drag_ctrl={PTR_NULL};
				int				dragged_ctrl_obj_id,dragged_ctrl_id;
			

				tree_mamanger_get_node_signed_dword	(&event_node,4	,&drag_ofset[0]);
				tree_mamanger_get_node_signed_dword	(&event_node,8	,&drag_ofset[1]);
				tree_mamanger_get_node_signed_dword	(&event_node,12	,&dragged_ctrl_id);
				tree_mamanger_get_node_signed_dword	(&event_node,16	,&dragged_ctrl_obj_id);
				
				if(tree_node_find_child_by_id		(&container->scene_ref,dragged_ctrl_id,&drag_ctrl))
				{
					gfx_ctrl_drag					(&drag_ctrl,dragged_ctrl_obj_id,drag_ofset);
					release_zone_ref				(&drag_ctrl);
				}
				
			}
			else
			{
				bid		  =		gfx_scene_event_test(&container->scene_ref,&event_node,container->trans,&crtl_event_node);
			
				switch(gfx_render_containers_get_obj_type(container->id,bid))
				{
					case NODE_GFX_CTRL:
						if(gfx_scene_get_obj					(&container->scene_ref,bid,&obj_node))
						{
							gfx_ctrl_event						(&obj_node,&crtl_event_node);
						}
						

					break;
				}
			}
				
			tree_manager_write_node_dword		(&event_node,0,0);
			release_zone_ref					(&event_node);
			
		}
		container++;
	}


	return 1;
}

/*
void gfx_process_all_containers_events()
{
	unsigned int count;
	gfx_render_container_t	*container,*container_last;

	container		=	get_zone_ptr(&container_list,0);
	container_last	=	get_zone_ptr(&container_list,0xFFFFFFFF);

	count=0;

	while(container<container_last)
	{
		if(container->id!=0xFFFFFFFF)
		{
			gfx_container_process_events(container->id);
		}
		container++;
	}
	return ;
}

void gfx_dump_all_containers()
{
	unsigned int count;
	gfx_render_container_t	*container,*container_last;

	container		=	get_zone_ptr(&container_list,0);
	container_last	=	get_zone_ptr(&container_list,0xFFFFFFFF);

	kernel_log	(kernel_log_id,"container list ");
	writeptr	(container);
	writeptr	(container_last);
	writestr	("\n");

	count=0;

	while(container<container_last)
	{
		kernel_log	(kernel_log_id,"container list #");
		writeint	(count,10);
		writestr	(" ");
		writeptr	(container);
		writestr	("\n");
		if(container->id!=0xFFFFFFFF)
		{
			gfx_dump_container(container->id);
		}
		container++;
		count++;
	}
	return ;
}

*/
unsigned int gfx_dump_container			(unsigned int container_id)
{
	gfx_render_container_t	*container;
	container	=	get_container_by_id(container_id);
	if(container == PTR_NULL)
	{
		kernel_log		(kernel_log_id,"could find container [");
		writeint		(container_id,0x16);
		writestr		("]\n");
		return 0;
	}

	tree_manager_dump_node_rec(&container->scene_ref,0,2);

	kernel_log	(kernel_log_id,"container id :");
	writeint	(container->id,10);
	writestr	(" size : [");
	writeint	(container->rect.pos[0],10);
	writestr	(" ");
	writeint	(container->rect.pos[1],10);
	writestr	(" ");
	writeint	(container->rect.size[0],10);
	writestr	(" ");
	writeint	(container->rect.size[1],10);
	writestr	("]\n");
	return 1;
}


unsigned int gfx_render_container_dump(unsigned int container_id)
{
	gfx_render_container_t		*container;
	container	=	get_container_by_id(container_id);
	if(container == PTR_NULL)
	{
		kernel_log		(kernel_log_id,"could not dump container \n");
		return 0;
	}
	kernel_log	(kernel_log_id,"container :'");
	writeint	(container->rect.pos[0],10);
	writestr	(" ");
	writeint	(container->rect.pos[1],10);
	writestr	(" ");
	writeint	(container->rect.size[0],10);
	writestr	(" ");
	writeint	(container->rect.size[1],10);
	writestr	("\n");
	return 1;
}
void gfx_render_containers_set_scroll_y(unsigned int container_id,int scroll_x,int scroll_y)
{
	gfx_render_container_t		*container;

	container	=	get_container_by_id(container_id);
	if(container == PTR_NULL)
	{
		kernel_log		(kernel_log_id,"could not find container (set scroll) \n");
		return ;
	}

	container->trans[0]=scroll_x;
	container->trans[1]=scroll_y;
}



int gfx_render_containers_set_pos(unsigned int container_id,unsigned int pos_x,unsigned int pos_y)
{
	gfx_render_container_t		*container;

	container	=	get_container_by_id(container_id);
	if(container == PTR_NULL)
	{
		kernel_log		(kernel_log_id,"could not set container pos ");
		writeint		(container_id,16);
		writestr		("\n");
		return 0;
	}

	container->rect.pos[0]=pos_x;
	container->rect.pos[1]=pos_y;

	return 1;
}

int gfx_render_containers_get_cursor_abs(unsigned int container_id,const vec_2s_t cursor,vec_2s_t out)
{
	gfx_render_container_t		*container;

	container	=	get_container_by_id(container_id);
	if(container == PTR_NULL)
	{
		kernel_log		(kernel_log_id,"could not get container pos ");
		writeint		(container_id,16);
		writestr		("\n");
		return 0;
	}

	out[0]=cursor[0]+container->rect.pos[0];
	out[1]=cursor[1]+container->rect.pos[1];
	return 1;
}

int gfx_render_containers_get_pos(unsigned int container_id,unsigned int *pos_x,unsigned int *pos_y)
{
	gfx_render_container_t		*container;

	container	=	get_container_by_id(container_id);
	if(container == PTR_NULL)
	{
		kernel_log		(kernel_log_id,"could not get container pos ");
		writeint		(container_id,16);
		writestr		("\n");
		return 0;
	}

	(*pos_x)=container->rect.pos[0];
	(*pos_y)=container->rect.pos[1];

	return 1;
}


int gfx_render_containers_get_size(unsigned int container_id,unsigned int *size_x,unsigned int *size_y)
{
	gfx_render_container_t		*container;

	container	=	get_container_by_id(container_id);
	if(container == PTR_NULL)
	{
		kernel_log		(kernel_log_id,"could not get container pos ");
		writeint		(container_id,16);
		writestr		("\n");
		return 0;
	}

	(*size_x)=container->rect.size[0];
	(*size_y)=container->rect.size[1];

	return 1;
}

void gfx_render_containers_set_zoom	(unsigned int container_id,vec_2s_t zoom)
{
	gfx_render_container_t		*container;

	container	=	get_container_by_id(container_id);
	if(container == PTR_NULL)
	{
		kernel_log		(kernel_log_id,"could not find container (set zoom) \n");
		return ;
	}

	container->zoom_fac[0]=zoom[0];
	container->zoom_fac[1]=zoom[1];
}


/*
OS_API_C_FUNC(unsigned int ) gfx_render_containers_get_selected(unsigned int container_id,vec_2s_t cursor,mem_zone_ref_ptr event_node)
{
	gfx_render_container_t		*container;

	

	container	=	get_container_by_id(container_id);
	if(container == PTR_NULL)
	{
		kernel_log		(kernel_log_id,"could not find container (extent)\n");
		return 0;
	}

	return gfx_scene_get_selected	(container->scene_id,cursor,container->trans,event_node);
}
*/
unsigned int gfx_render_containers_set_obj_prop(unsigned int container_id,unsigned int obj_id,const char *name,unsigned int val)
{
	gfx_render_container_t		*container;
	

	container	=	get_container_by_id(container_id);
	if(container == PTR_NULL)
	{
		kernel_log		(kernel_log_id,"could not find container (extent)\n");
		return 0;
	}


	return gfx_set_scene_obj_val_ui(&container->scene_ref,obj_id,name,val);
}

unsigned int gfx_render_containers_get_obj_prop(unsigned int container_id,unsigned int obj_id,const char *name,unsigned int *val)
{
	gfx_render_container_t		*container;

	

	container	=	get_container_by_id(container_id);
	if(container == PTR_NULL)
	{
		kernel_log		(kernel_log_id,"could not find container (extent)\n");
		return 0;
	}


	return gfx_get_scene_obj_val_ui(&container->scene_ref,obj_id,name,val);
}

unsigned int gfx_render_containers_get_obj_prop_str(unsigned int container_id,unsigned int obj_id,const char *name,char *val,unsigned int str_len)
{
	gfx_render_container_t		*container;


	container	=	get_container_by_id(container_id);
	if(container == PTR_NULL)
	{
		kernel_log		(kernel_log_id,"could not find container (extent)\n");
		return 0;
	}


	return gfx_get_scene_obj_val_str(&container->scene_ref,obj_id,name,val,str_len);
}

unsigned int gfx_render_containers_get_obj_prop_by_type(unsigned int container_id,unsigned int obj_id,unsigned int type,mem_zone_ref_ptr out)
{
	gfx_render_container_t		*container;


	container	=	get_container_by_id(container_id);
	if(container == PTR_NULL)
	{
		kernel_log		(kernel_log_id,"could not find container (extent)\n");
		return 0;
	}
	return gfx_get_scene_obj_prop_by_type(&container->scene_ref,obj_id,type,out);
	//unsigned int  gfx_get_scene_obj_val_str(container->scene_id,obj_id,name,val,str_len);
}





unsigned int gfx_render_containers_get_obj_type(unsigned int container_id,unsigned int obj_id)
{
	gfx_render_container_t		*container;

	if(obj_id==0)return 0;

	container	=	get_container_by_id(container_id);
	if(container == PTR_NULL)
	{
		kernel_log		(kernel_log_id,"could not find container (extent)\n");
		return 0;
	}

	return gfx_get_scene_obj_type(&container->scene_ref,obj_id);
}




unsigned int gfx_render_containers_get_extent(unsigned int container_id,struct gfx_rect *out_rect)
{
	gfx_render_container_t		*container;

	container	=	get_container_by_id(container_id);
	if(container == PTR_NULL)
	{
		kernel_log		(kernel_log_id,"could not find container (extent)\n");
		return 0;
	}
	gfx_scene_get_extent(&container->scene_ref,out_rect);

	return 1;

}


