#define GFX_LIB_API C_EXPORT
#include <std_def.h>
#include <kern.h>
#include <std_mem.h>
#include <mem_base.h>
#include <lib_c.h>
#include <tree.h>
#include <sys/mem_stream.h>
#include <sys/file_system.h>
#include <sys/tpo_mod.h>
#include <sys/task.h>
#include <sys/ctrl.h>

#include "../filters/filters.h"
#include <gfx/graphic_object.h>
#include <gfx/graphic_base.h>

#include "../image.h"

#include "gfx_lib/scene.h"





extern unsigned int	kernel_log_id;
unsigned int		scene_obj_next_id	=	1;



unsigned int C_API_FUNC gfx_set_scene_obj_val_ui(mem_zone_ref_ptr scene_root,unsigned int obj_id,const char *name,unsigned int val)
{

	int					ret;
	mem_zone_ref		node_obj={PTR_NULL};

	if(!tree_node_find_child_by_id(scene_root,obj_id,&node_obj))
	{
		kernel_log	(kernel_log_id,"gfx scene object not found ");
		writeint	(obj_id,16);
		writestr	("\n");
		return 0;
	}

	ret	=	tree_manager_set_child_value_i32	(&node_obj,name,val);

	release_zone_ref							(&node_obj);

	return ret;
}


unsigned int C_API_FUNC gfx_set_scene_obj_val_str(mem_zone_ref_ptr scene_root,unsigned int obj_id,const char *name,const char *val)
{

	int					ret;
	mem_zone_ref		node_obj={PTR_NULL};

	if(!tree_node_find_child_by_id(scene_root,obj_id,&node_obj))
	{
		kernel_log	(kernel_log_id,"gfx scene object not found ");
		writeint	(obj_id,16);
		writestr	("\n");
		return 0;
	}
	ret	=	tree_manager_set_child_value_str	(&node_obj,name,val);
	release_zone_ref							(&node_obj);

	return ret;
}


unsigned int gfx_get_scene_obj_val_str(mem_zone_ref_ptr scene_root,unsigned int obj_id,const char *name,char *val,unsigned int str_len)
{
	unsigned int		ret;

	mem_zone_ref		node_obj={PTR_NULL};

	if(!tree_node_find_child_by_id(scene_root,obj_id,&node_obj))
	{
		kernel_log	(kernel_log_id,"gfx scene object not found ");
		writeint	(obj_id,16);
		writestr	("\n");
		return 0;
	}

	ret = tree_manager_get_child_value_str(&node_obj,NODE_HASH(name),val,str_len,16);
	release_zone_ref(&node_obj);

	return ret;
}

unsigned int C_API_FUNC gfx_scene_get_obj	(mem_zone_ref_ptr scene_root,unsigned int obj_id,mem_zone_ref_ptr dst_node)
{
	return tree_node_find_child_by_id(scene_root,obj_id,dst_node);
}

unsigned int gfx_get_scene_obj_prop_by_type(mem_zone_ref_ptr	scene_root,unsigned int obj_id,unsigned int type,mem_zone_ref_ptr out)
{
	mem_zone_ref		node_obj={PTR_NULL};
	int					ret;
	if(!tree_node_find_child_by_id(scene_root,obj_id,&node_obj))
	{
		kernel_log	(kernel_log_id,"gfx scene object not found ");
		writeint	(obj_id,16);
		writestr	("\n");
		return 0;
	}

	ret =tree_node_find_child_by_type	(&node_obj,type,out,0);
	release_zone_ref					(&node_obj);

	return ret;
}


unsigned int gfx_get_scene_obj_val_ui(mem_zone_ref_ptr scene_root,unsigned int obj_id,const char *name,unsigned int *val)
{
	mem_zone_ref		node_obj={PTR_NULL};
	int					ret;


	if(!tree_node_find_child_by_id(scene_root,obj_id,&node_obj))
	{
		kernel_log	(kernel_log_id,"gfx scene object not found ");
		writeint	(obj_id,16);
		writestr	("\n");
		return 0;
	}

	ret = tree_manager_get_child_value_i32(&node_obj,NODE_HASH(name),val);
	release_zone_ref					(&node_obj);

	return ret;
}


unsigned int gfx_get_scene_obj_type(mem_zone_ref_ptr scene_root,unsigned int obj_id)
{
	unsigned int		ret;
	mem_zone_ref		node_obj={PTR_NULL};

	if(!tree_node_find_child_by_id(scene_root,obj_id,&node_obj))
	{
		kernel_log	(kernel_log_id,"gfx scene object (type) not found ");
		writeint	(obj_id,16);
		writestr	("\n");
		tree_manager_dump_node_rec(scene_root,0,3);
		return 0;
	}

	ret=tree_mamanger_get_node_type	(&node_obj);
	release_zone_ref				(&node_obj);
	return ret;

}

void gfx_get_scene_dump_obj(mem_zone_ref_ptr scene_root,unsigned int obj_id)
{
	mem_zone_ref				node_obj={PTR_NULL};


	if(!tree_node_find_child_by_id(scene_root,obj_id,&node_obj))
	{
		kernel_log	(kernel_log_id,"gfx scene object not found ");
		writeint	(obj_id,16);
		writestr	("\n");
		return ;
	}

	tree_manager_dump_node_rec	(&node_obj,0,4);
	release_zone_ref				(&node_obj);
	return ;

}



unsigned int gfx_scene_rem_obj	(mem_zone_ref_ptr scene_root,unsigned int obj_id)
{
	if(!tree_remove_child_by_id				(scene_root,obj_id))
	{
		kernel_log	(kernel_log_id,"cannot rem obj  ");
		writeint	(obj_id,16);
		writestr	("\n");
		return 0;
	}

	return 1;
}


unsigned int C_API_FUNC gfx_scene_clear	(mem_zone_ref_ptr scene_root)
{
	tree_remove_children(scene_root);
	return 1;
}


unsigned int gfx_scene_add_obj_style	(mem_zone_ref_ptr scene_root,unsigned int obj_id,const char *style)
{
	mem_zone_ref		obj_node={PTR_NULL};
	mem_zone_ref		style_node={PTR_NULL};
	int					ret;

	if(!tree_node_find_child_by_id				(scene_root,obj_id,&obj_node))
	{
		kernel_log	(kernel_log_id,"cannot find obj  ");
		writeint	(obj_id,16);
		writestr	("\n");
		return 0;
	}

	if(tree_node_find_child_by_name			(&obj_node,"style",&style_node))
	{
		ret=tree_manager_add_node_childs		(&style_node,style,1);
		if(!ret)kernel_log(kernel_log_id,"unable to add obj style\n");
		release_zone_ref(&style_node);

	}
	else
	{
		ret=tree_manager_create_node_params		(&style_node	,style);
		if(ret)
		{
			ret=tree_manager_node_add_child	(&obj_node,&style_node);
			release_zone_ref				(&style_node);
		}
		else
			kernel_log(kernel_log_id,"unable to create new style\n");
	}
	
	release_zone_ref(&obj_node);
	return ret;
}


unsigned int C_API_FUNC gfx_scene_add_text_list	(mem_zone_ref_ptr	scene_root,const char *style)
{
	mem_zone_ref		new_text ={PTR_NULL};
	mem_zone_ref		style_node={PTR_NULL};
	struct obj_array_t  txt_lst;
	unsigned int		cur_id;
	char				*def_style="{(\"style\",0x04000020)p_x:0,p_y:0,width:100,height:100,font_size_x:12,font_size_y:12,border:1,font_name:\"Flama\",visible:1}";



	cur_id		=	++scene_obj_next_id;

	tree_manager_create_obj				(&txt_lst);
	tree_manager_add_obj				(&txt_lst,"TEXT LIST"	,NODE_GFX_TEXT_LIST);
	tree_manager_add_obj_int_val		(&txt_lst,"id"			,cur_id);
	tree_manager_end_obj				(&txt_lst);

	if(txt_lst.char_buffer.zone==PTR_NULL){
		writestr("error text list txt buffer \n");
		return 0;
	}
	
	if(!tree_manager_create_node_params		(&new_text	,get_zone_ptr(&txt_lst.char_buffer,0)))
	{
		tree_manager_free_obj_array			(&txt_lst);

		kernel_log	(kernel_log_id,"cannot create new text entry\n");
		kernel_log	(kernel_log_id,get_zone_ptr(&txt_lst.char_buffer,0));
		writestr	("\n");
		return 0;
	}
	
	tree_manager_free_obj_array			(&txt_lst);

	
	if(!tree_manager_create_node_params		(&style_node,def_style))
	{
		kernel_log	(kernel_log_id,"cannot create new text entry def style\n");
		kernel_log	(kernel_log_id,def_style);
		writestr	("\n");
		return 0;
	}

	if(!tree_manager_add_node_childs		(&style_node,style,1))
	{
		kernel_log	(kernel_log_id,"cannot copy text entry style\n");
		kernel_log	(kernel_log_id,style);
		writestr	("\n");
		return 0;
	}
	
	tree_manager_node_add_child			(&new_text	 ,&style_node);
	
	tree_manager_add_child_node			(&new_text	 ,"event list",NODE_GFX_EVENT_LIST,PTR_NULL);
	

	tree_manager_node_add_child			(scene_root,&new_text);

	release_zone_ref					(&style_node);
	release_zone_ref					(&new_text);
	


	return cur_id;

}
unsigned int C_API_FUNC  gfx_scene_add_text_to_list	(mem_zone_ref_ptr scene_root,unsigned int text_list_id,const char *text,const char *style)
{
	mem_zone_ref		new_text		={PTR_NULL};
	mem_zone_ref		text_list_node	={PTR_NULL};
	mem_zone_ref		style_node		={PTR_NULL};
	mem_zone_ref		text_rect_node	={PTR_NULL};
	struct obj_array_t  txt_lst;
	int					lpos_x,lpos_y;
	int					pos_x;
	unsigned int		size_x,size_y,border;
	unsigned int		cur_id;
	char				*def_style="{(\"style\",0x04000020)p_x:0,border:1,text_color:0xFF000000,bk_color:0x00000000,visible:1}";



	cur_id		=	++scene_obj_next_id;


	tree_manager_create_obj				(&txt_lst);
	tree_manager_add_obj				(&txt_lst,"TEXT ENTRY"	,NODE_GFX_TEXT_LIST_ENTRY);
	tree_manager_add_obj_int_val		(&txt_lst,"id"			,cur_id);
	tree_manager_add_obj_str_val		(&txt_lst,"text"		,text);
	tree_manager_end_obj				(&txt_lst);
	
	if(txt_lst.char_buffer.zone==PTR_NULL)
	{
		writestr("error text to list txt buffer \n");
		return 0;
	}

	if(!tree_manager_create_node_params		(&new_text	,get_zone_ptr(&txt_lst.char_buffer,0)))
	{	
		tree_manager_free_obj_array			(&txt_lst);
		kernel_log(kernel_log_id,"unable to create new text entry\n");
		return 0;
	}
	tree_manager_free_obj_array			(&txt_lst);
	
	if(!tree_node_find_child_by_id				(scene_root,text_list_id,&text_list_node))
	{
		kernel_log	(kernel_log_id,"cannot find text list ");
		writeint	(text_list_id,16);
		writestr	("\n");
		release_zone_ref(&new_text);
		return 0;
	}

	if(!tree_manager_create_node_params		(&style_node,def_style))
	{
		kernel_log(kernel_log_id,"unable to create new text entry def style \n");
		kernel_log(kernel_log_id,def_style);
		writestr  ("\n");

		release_zone_ref(&new_text);
		release_zone_ref(&text_list_node);
		return 0;
	}

	if(!tree_manager_add_node_childs		(&style_node,style,1))
	{
		release_zone_ref(&style_node);
		release_zone_ref(&new_text);
		release_zone_ref(&text_list_node);

		kernel_log(kernel_log_id,"unable to copy new text entry style\n");
		return 0;
	}
	
	tree_manager_get_child_value_si32	(&style_node,NODE_HASH("p_x"),&pos_x);
	tree_manager_get_child_value_i32	(&style_node,NODE_HASH("border"),&border);

	tree_manager_node_add_child			(&new_text	,&style_node);
	release_zone_ref					(&style_node);

	if(tree_node_find_child_by_name			(&text_list_node,"style",&style_node))
	{
		tree_manager_get_child_value_si32		(&style_node,NODE_HASH("p_x"),&lpos_x);
		tree_manager_get_child_value_si32		(&style_node,NODE_HASH("p_y"),&lpos_y);

		tree_manager_get_child_value_i32		(&style_node,NODE_HASH("font_size_x"),&size_x);
		tree_manager_get_child_value_i32		(&style_node,NODE_HASH("font_size_y"),&size_y);
		release_zone_ref						(&style_node);
	}
	else
	{
		lpos_x	=	0;
		lpos_y	=	0;
		size_x	=	12;
		size_y	=	12;

	}
	
	if(tree_manager_add_child_node			(&new_text,"rect",NODE_GFX_RECT,&text_rect_node))
	{
		tree_manager_write_node_signed_dword	(&text_rect_node,0 ,lpos_x+pos_x);
		tree_manager_write_node_signed_dword	(&text_rect_node,4 ,lpos_y);
		tree_manager_write_node_dword			(&text_rect_node,8 ,size_x*(strlen_c(text)+1));
		tree_manager_write_node_dword			(&text_rect_node,12,size_y);
		release_zone_ref						(&text_rect_node);
	}

	tree_manager_node_add_child			(&text_list_node,&new_text);
	release_zone_ref					(&new_text);	
	release_zone_ref					(&text_list_node);

	return 1;
}




unsigned int C_API_FUNC gfx_scene_add_text	(mem_zone_ref_ptr scene_root,const char *text,const char *style)
{
	mem_zone_ref		new_text	={PTR_NULL};
	mem_zone_ref		style_node	={PTR_NULL};
	mem_zone_ref		r_node		={PTR_NULL};
	gfx_rect_t			txt_rect;
	struct obj_array_t  txt_obj;
	unsigned int		cur_id;
	int					p_x,p_y;
	unsigned int		size_x,size_y,border;
	char				*def_style="{(\"style\",0x04000020)p_x:0,p_y:0,width:100,height:100,font_size_x:12,font_size_y:12,border:1,font_name:\"Flama\",bk_color:0x00000000,text_color:0xFF00FF00,visible:1}";

	cur_id		=	++scene_obj_next_id;


	tree_manager_create_obj				(&txt_obj);
	tree_manager_add_obj				(&txt_obj,"TEXT"		,NODE_GFX_TEXT);
	tree_manager_add_obj_int_val		(&txt_obj,"id"			,cur_id);
	tree_manager_add_obj_str_val		(&txt_obj,"text"		,text);
	tree_manager_end_obj				(&txt_obj);

	if(txt_obj.char_buffer.zone==PTR_NULL)return 0;
	

	if(!tree_manager_create_node_params		(&new_text	,get_zone_ptr(&txt_obj.char_buffer,0)))
	{
		kernel_log	(kernel_log_id,"cannot create new text entry\n");
		kernel_log	(kernel_log_id,get_zone_ptr(&txt_obj.char_buffer,0));
		writestr	("\n");
		return 0;
	}

	
	if(!tree_manager_create_node_params		(&style_node,def_style))
	{
		kernel_log	(kernel_log_id,"cannot create new text entry def style\n");
		kernel_log	(kernel_log_id,def_style);
		writestr	("\n");
		return 0;
	}



	if(!tree_manager_add_node_childs		(&style_node,style,1))
	{
		kernel_log	(kernel_log_id,"cannot copy text entry style\n");
		kernel_log	(kernel_log_id,style);
		writestr	("\n");
		return 0;
	}

	tree_manager_get_child_value_si32	(&style_node,NODE_HASH("p_x"),&p_x);
	tree_manager_get_child_value_si32	(&style_node,NODE_HASH("p_y"),&p_y);
	tree_manager_get_child_value_i32	(&style_node,NODE_HASH("font_size_x"),&size_x);
	tree_manager_get_child_value_i32	(&style_node,NODE_HASH("font_size_y"),&size_y);
	tree_manager_get_child_value_i32	(&style_node,NODE_HASH("border"),&border);
	
	txt_rect.pos[0]		=p_x;	
	txt_rect.pos[1]		=p_y;

	txt_rect.size[0]	=size_x*(strlen_c(text)+1);
	txt_rect.size[1]	=size_y;

	if(tree_manager_add_child_node				(&new_text,"rect",NODE_GFX_RECT,&r_node))
	{
		tree_manager_write_node_signed_dword	(&r_node,0,txt_rect.pos[0]);
		tree_manager_write_node_signed_dword	(&r_node,4,txt_rect.pos[1]);
		tree_manager_write_node_dword			(&r_node,8,txt_rect.size[0]);
		tree_manager_write_node_dword			(&r_node,12,txt_rect.size[1]);
		release_zone_ref						(&r_node);
	}

	tree_manager_add_child_node					(&new_text,"event list",NODE_GFX_EVENT_LIST,PTR_NULL);

	

	tree_manager_node_add_child				(&new_text	 ,&style_node);
	tree_manager_node_add_child				(scene_root,&new_text);

	release_zone_ref					(&style_node);
	release_zone_ref					(&new_text);
	return cur_id;
}




unsigned int C_API_FUNC gfx_scene_add_rect	(mem_zone_ref_ptr scene_root,const char *style)
{
	int				p_x;
	int				p_y;
	unsigned int	size_x;
	unsigned int	size_y;
	unsigned int	border;
	mem_zone_ref	new_rect		={PTR_NULL};
	mem_zone_ref	new_rect_node	={PTR_NULL};
	mem_zone_ref	style_node		={PTR_NULL};
	unsigned int	cur_id;
	char			*def_style="{(\"style\",0x04000020)p_x:0,p_y:0,width:100,height:100,border:0,color:0xFFFFFFFF,visible:1}";

		


	if(!tree_manager_add_child_node				(scene_root,"RECT_OBJ",NODE_GFX_RECT_OBJ,&new_rect))
	{
		kernel_log	(kernel_log_id,"cannot add rect scene \n");
		return 0;
	}

	cur_id			=	++scene_obj_next_id;

	tree_manager_set_child_value_si32		(&new_rect,"id",cur_id);


	if(!tree_manager_create_node_params		(&style_node,def_style))
	{
		kernel_log	(kernel_log_id,"cannot create new rect def style\n");
		kernel_log	(kernel_log_id,def_style);
		writestr	("\n");
		return 0;
	}

	if(!tree_manager_add_node_childs		(&style_node,style,1))
	{
		kernel_log	(kernel_log_id,"cannot copy text entry style\n");
		kernel_log	(kernel_log_id,style);
		writestr	("\n");
		return 0;
	}



	tree_manager_get_child_value_si32	(&style_node,NODE_HASH("p_x"),&p_x);
	tree_manager_get_child_value_si32	(&style_node,NODE_HASH("p_y"),&p_y);
	tree_manager_get_child_value_i32	(&style_node,NODE_HASH("width"),&size_x);
	tree_manager_get_child_value_i32	(&style_node,NODE_HASH("height"),&size_y);
	tree_manager_get_child_value_i32	(&style_node,NODE_HASH("border"),&border);

	if(tree_manager_add_child_node				(&new_rect,"rect",NODE_GFX_RECT,&new_rect_node))
	{
		tree_manager_write_node_signed_dword	(&new_rect_node,0,p_x-border);
		tree_manager_write_node_signed_dword	(&new_rect_node,4,p_y-border);
		tree_manager_write_node_dword			(&new_rect_node,8,size_x+border*2);
		tree_manager_write_node_dword			(&new_rect_node,12,size_y+border*2);
		release_zone_ref						(&new_rect_node);
	}

	
	tree_manager_add_child_node				(&new_rect_node,"event list",NODE_GFX_EVENT_LIST,PTR_NULL);


	tree_manager_node_add_child				(&new_rect	,&style_node);
	release_zone_ref						(&new_rect);
	release_zone_ref						(&style_node);

	return cur_id;
}



OS_API_C_FUNC(unsigned int) gfx_itr_get_scene	(scene_itr_t	*scene_itr)
{
	scene_itr->clear				=	gfx_scene_clear;
	scene_itr->add_text				=	gfx_scene_add_text;
	scene_itr->add_image			=	gfx_scene_add_image;
	
	scene_itr->swap_ctrl			=	gfx_scene_swap_ctrl;
	scene_itr->add_rect				=	gfx_scene_add_rect;
	scene_itr->get_obj				=	gfx_scene_get_obj;
	scene_itr->set_obj_val_ui		=	gfx_set_scene_obj_val_ui;
	scene_itr->set_obj_val_str		=	gfx_set_scene_obj_val_str;
	

	

	

	return 1;
}


unsigned int C_API_FUNC gfx_scene_swap_ctrl		(mem_zone_ref_ptr scene_root,unsigned int obj_id,mem_zone_ref_ptr src_node)
{
	mem_zone_ref		c_style_node	={PTR_NULL};
	mem_zone_ref		style_node		={PTR_NULL};
	mem_zone_ref		ctrl_obj_node	={PTR_NULL};
	char				*def_style		="{(\"style\",0x04000020)p_x:0,p_y:0,item_height:16,width:100,height:350,font_size_x:12,font_size_y:12,border:1,font_name:\"Flama\",bk_color:0xFFFFFFFF,text_color:0xFF000000,visible:1}";
	char				*node_style		=PTR_NULL;
	unsigned int		cur_id;


	if(tree_manager_get_child_value_i32(src_node,NODE_HASH("id"),&cur_id))
	{
		copy_zone_ref	(&ctrl_obj_node,src_node);
	}
	else
	{
		mem_zone_ref	new_rect_node={PTR_NULL};
		int				p_x,p_y;
		unsigned int	size_x,size_y;
		unsigned int	border;

		tree_manager_node_dup							(PTR_NULL,src_node,&ctrl_obj_node);

		cur_id				=	++scene_obj_next_id;

		tree_manager_set_child_value_i32				(&ctrl_obj_node					,	"id",cur_id);

		if(!tree_manager_create_node_params		(&style_node,def_style))
		{
			release_zone_ref	(&ctrl_obj_node);
			kernel_log			(kernel_log_id,"cannot create new ctrl def style\n");
			kernel_log			(kernel_log_id,def_style);
			writestr			("\n");
			return 0;
		}
		
		if(tree_node_find_child_by_name		(src_node			,"style_str",&c_style_node))
		{
			tree_manager_get_child_data_ptr	(&c_style_node	,NODE_HASH("style"),&node_style);
			release_zone_ref				(&c_style_node);

			if(!tree_manager_add_node_childs		(&style_node,node_style,1))
			{
				kernel_log	(kernel_log_id,"cannot copy ctrl style\n");
				writestr	("style : '");
				kernel_log	(kernel_log_id,node_style);
				writestr	("'\n");
			}
		}

		tree_manager_node_add_child			(&ctrl_obj_node						,	&style_node);


		tree_manager_get_child_value_si32	(&style_node,NODE_HASH("p_x")	,&p_x);
		tree_manager_get_child_value_si32	(&style_node,NODE_HASH("p_y")	,&p_y);
		tree_manager_get_child_value_i32	(&style_node,NODE_HASH("width")	,&size_x);
		tree_manager_get_child_value_i32	(&style_node,NODE_HASH("height"),&size_y);
		tree_manager_get_child_value_i32	(&style_node,NODE_HASH("border"),&border);

		if(tree_manager_add_child_node				(&ctrl_obj_node,"rect",NODE_GFX_RECT,&new_rect_node))
		{
			tree_manager_write_node_signed_dword	(&new_rect_node,0,p_x-border);
			tree_manager_write_node_signed_dword	(&new_rect_node,4,p_y-border);
			tree_manager_write_node_dword			(&new_rect_node,8,size_x+border*2);
			tree_manager_write_node_dword			(&new_rect_node,12,size_y+border*2);
			release_zone_ref						(&new_rect_node);
		}

		release_zone_ref					(&style_node);
	}

	if(!tree_swap_child_node_by_id	(scene_root,obj_id,&ctrl_obj_node))return 0;

	

	copy_zone_ref			(src_node,&ctrl_obj_node);
	release_zone_ref		(&ctrl_obj_node);
	
	return cur_id;
}
unsigned int gfx_scene_add_ctrl				(mem_zone_ref_ptr	scene_root,mem_zone_ref_ptr src_node)
{
	char				ctrl_class		[32];
	mem_zone_ref		c_style_node	={PTR_NULL};
	mem_zone_ref		style_node		={PTR_NULL};
	mem_zone_ref		ctrl_obj_node	={PTR_NULL};
	mem_zone_ref		new_rect_node	={PTR_NULL};
	char				*def_style		="{(\"style\",0x04000020)p_x:0,p_y:0,width:100,height:200,item_height:16,font_size_x:12,font_size_y:12,border:1,font_name:\"Flama\",bk_color:0xFFFFFFFF,text_color:0xFF000000,visible:1}";
	char				*node_style		=PTR_NULL;
	unsigned int		cur_id;
	int					p_x,p_y;
	unsigned int		size_x,size_y;
	unsigned int		border;

	if(!tree_manager_get_child_value_str	(src_node,NODE_HASH("ctrl class"),ctrl_class,32,0))
	{
		kernel_log(kernel_log_id,"ctrl class not found \n");
		return 0;
	}
	
	
	
	if(!tree_manager_create_node_params		(&style_node,def_style))
	{
		kernel_log	(kernel_log_id,"cannot create new ctrl def style\n");
		kernel_log	(kernel_log_id,def_style);
		writestr	("\n");
		return 0;
	}

	if(tree_node_find_child_by_name		(src_node			,"style_str",&c_style_node))
	{
		tree_manager_get_child_data_ptr	(&c_style_node	,NODE_HASH("style"),&node_style);
		release_zone_ref				(&c_style_node);
	}

	if(!tree_manager_add_node_childs		(&style_node,node_style,1))
	{
		kernel_log	(kernel_log_id,"cannot copy ctrl style\n");
		writestr	("style : '");
		kernel_log	(kernel_log_id,node_style);
		writestr	("'\n");
		return 0;
	}
	cur_id		=	++scene_obj_next_id;

	tree_manager_node_dup				(PTR_NULL		,src_node,&ctrl_obj_node);



	tree_manager_node_add_child			(&ctrl_obj_node	,&style_node);



	tree_manager_set_child_value_i32	(&ctrl_obj_node,"id"				,cur_id);
	tree_manager_set_child_value_i32	(&ctrl_obj_node,"need_recompute"	,1);


	tree_manager_get_child_value_si32	(&style_node,NODE_HASH("p_x")	,&p_x);
	tree_manager_get_child_value_si32	(&style_node,NODE_HASH("p_y")	,&p_y);
	tree_manager_get_child_value_i32	(&style_node,NODE_HASH("width")	,&size_x);
	tree_manager_get_child_value_i32	(&style_node,NODE_HASH("height"),&size_y);
	tree_manager_get_child_value_i32	(&style_node,NODE_HASH("border"),&border);

	if(tree_manager_add_child_node				(&ctrl_obj_node,"rect",NODE_GFX_RECT,&new_rect_node))
	{
		tree_manager_write_node_signed_dword	(&new_rect_node,0,p_x-border);
		tree_manager_write_node_signed_dword	(&new_rect_node,4,p_y-border);
		tree_manager_write_node_dword			(&new_rect_node,8,size_x+border*2);
		tree_manager_write_node_dword			(&new_rect_node,12,size_y+border*2);
		release_zone_ref						(&new_rect_node);
	}
	release_zone_ref				(&style_node);


	tree_manager_node_add_child		(scene_root,&ctrl_obj_node);
	release_zone_ref				(&ctrl_obj_node);

	return cur_id;
}






unsigned int C_API_FUNC gfx_scene_add_image	(mem_zone_ref_ptr	scene_root,int p_x,int p_y,const char *fs_name,const char *image_path)
{
	mem_zone_ref		file_path		={PTR_NULL};
	mem_zone_ref		new_image_node	={PTR_NULL};
	unsigned int		cur_id;
	


	cur_id		=	++scene_obj_next_id;

	tree_manager_create_node				("IMAGE_OBJ",NODE_GFX_IMAGE_OBJ,&new_image_node);
	
	tree_manager_add_child_node				(&new_image_node,"event list",NODE_GFX_EVENT_LIST,PTR_NULL);
	

	tree_manager_set_child_value_i32		(&new_image_node,"id"			,cur_id);

	tree_manager_set_child_value_si32		(&new_image_node,"pos_x"		,p_x);
	tree_manager_set_child_value_si32		(&new_image_node,"pos_y"		,p_y);


	tree_manager_add_child_node				(&new_image_node,"file path",NODE_FILE_SYSTEM_PATH,&file_path);
	tree_manager_set_child_value_str		(&file_path,"fs name",fs_name);
	tree_manager_set_child_value_str		(&file_path,"path" 	 ,image_path);
	release_zone_ref						(&file_path);

	/*writestr_fmt("gfx_scene_add_image %d %s x:%d y:%d \n",cur_id, image_path,p_x, p_y);*/
	
	if(!tree_manager_node_add_child	(scene_root,&new_image_node))
	{
		kernel_log	(kernel_log_id,"cannot add image to scene \n");
		return 0;
	}
	release_zone_ref						(&new_image_node);

	return cur_id;

}

unsigned int gfx_scene_event_test(mem_zone_ref_ptr scene_root,mem_zone_ref_ptr container_event,vec_2s_t trans,mem_zone_ref_ptr ctrl_event_node)
{
	mem_zone_ref				child_list={PTR_NULL};
	mem_zone_ref_ptr			obj_node;
	mem_zone_ref				node={PTR_NULL};
	gfx_rect_t					rect;
	vec_2s_t					cursor;
	unsigned int				obj_id;
	const char					*container_event_name;
	int							node_max_x,node_min_x;
	int							node_max_y,node_min_y;


	container_event_name	=	tree_mamanger_get_node_name	(container_event);

	if(!tree_manager_get_last_child	(scene_root,&child_list,&obj_node))return 0;


	while(obj_node!=PTR_NULL)
	{
		mem_zone_ref ev_list={PTR_NULL};
		unsigned int init=0;

		tree_manager_get_child_value_i32				(obj_node,NODE_HASH("initialized")	,&init);

		if((init==1)&&(tree_manager_find_child_node		(obj_node	 ,NODE_HASH("event list"),NODE_GFX_EVENT_LIST,&ev_list)))
		{
			mem_zone_ref_ptr	event_node;
			mem_zone_ref		event_list		={PTR_NULL};

			tree_manager_get_child_value_i32	(obj_node	 ,NODE_HASH("id"),&obj_id);
			
			if(tree_manager_get_first_child		(&ev_list,&event_list,&event_node))
			{
				while(event_node->zone!=PTR_NULL)
				{
					char event_type[32];

					tree_manager_get_child_value_str	(event_node,NODE_HASH("event type"),event_type,32,0);
					if(!strcmp_c(container_event_name,event_type))
					{
						if(!strncmp_c(event_type,"mouse_",6))
						{
							if(tree_node_find_child_by_name		(event_node,"rect",&node)==1)
							{
								tree_mamanger_get_node_signed_dword	(&node,0 ,&rect.pos[0]);
								tree_mamanger_get_node_signed_dword	(&node,4 ,&rect.pos[1]);
								tree_mamanger_get_node_dword		(&node,8 ,&rect.size[0]);
								tree_mamanger_get_node_dword		(&node,12,&rect.size[1]);
								release_zone_ref					(&node);

								tree_mamanger_get_node_signed_dword	(container_event,4	,&cursor[0]);
								tree_mamanger_get_node_signed_dword	(container_event,8	,&cursor[1]);

								node_min_x	=	rect.pos[0]-trans[0];
								node_min_y	=	rect.pos[1]-trans[1];

								node_max_x	=	node_min_x+(rect.size[0]);
								node_max_y	=	node_min_y+(rect.size[1]);

								if( (cursor[0]>=node_min_x)&&
									(cursor[1]>=node_min_y)&&
									(cursor[0]< node_max_x)&&
									(cursor[1]< node_max_y))
								{

									copy_zone_ref	 (ctrl_event_node,event_node);

									dec_zone_ref	 (event_node);
									release_zone_ref (&event_list);
												
									release_zone_ref (&ev_list);
									dec_zone_ref	 (obj_node);
									release_zone_ref (&child_list);
								
									return obj_id;
								}
							}
						}
					}
					if(!tree_manager_get_next_child(&event_list,&event_node))break;
				}
			}
			release_zone_ref (&ev_list);
		}
		tree_manager_get_prev_child(&child_list,&obj_node);
	}
	

	return 0;

}


unsigned int gfx_scene_get_extent(mem_zone_ref_ptr scene_root,struct gfx_rect *out_rect)
{
	gfx_rect_t					txt_rect;
	mem_zone_ref				node={PTR_NULL},r_node={PTR_NULL};
	unsigned int				n,nn;
	unsigned int				node_type;
	int							pos_x,pos_y;
	int							size[2];
	int							node_max_x,node_min_x;
	int							node_max_y,node_min_y;
	unsigned int				img_width,img_height;
	mem_zone_ref				scene_list={PTR_NULL};
	mem_zone_ref_ptr			obj_node;



	n=0;

	out_rect->pos[0] =10000;
	out_rect->pos[1] =10000;
	out_rect->size[0]=0;
	out_rect->size[1]=0;


	if(!tree_manager_get_first_child(scene_root,&scene_list,&obj_node))return 1;
	
	while(obj_node->zone!=PTR_NULL)
	{
		node_type=tree_mamanger_get_node_type(obj_node);
		switch(node_type)
		{
			case NODE_GFX_TEXT:
			r_node.zone=PTR_NULL;

			if(tree_node_find_child_by_name		(obj_node,"rect",&r_node)>0)
			{
				tree_mamanger_get_node_signed_dword	(&r_node,0 ,&txt_rect.pos[0]);
				tree_mamanger_get_node_signed_dword	(&r_node,4 ,&txt_rect.pos[1]);
				tree_mamanger_get_node_dword		(&r_node,8 ,&txt_rect.size[0]);
				tree_mamanger_get_node_dword		(&r_node,12,&txt_rect.size[1]);
				release_zone_ref					(&r_node);
			}
			node_min_x	=	txt_rect.pos[0];
			node_min_y	=	txt_rect.pos[1];

			node_max_x	=	node_min_x+(txt_rect.size[0]);
			node_max_y	=	node_min_y+(txt_rect.size[1]);

			size[0]		=	out_rect->size[0];
			size[1]		=	out_rect->size[1];


			if(node_max_x>size[0])out_rect->size[0]=node_max_x;
			if(node_max_y>size[1])out_rect->size[1]=node_max_y;

			if(node_min_x<out_rect->pos[0])out_rect->pos[0]=node_min_x;
			if(node_min_y<out_rect->pos[1])out_rect->pos[1]=node_min_y;

			break;

			case NODE_GFX_TEXT_LIST:

				
				nn			=	0;

				while(tree_manager_get_child_at(obj_node,nn,&node))
				{
					if(tree_mamanger_get_node_type(&node)==NODE_GFX_TEXT_LIST_ENTRY)
					{
						r_node.zone=PTR_NULL;

						if(tree_node_find_child_by_name		(&node,"rect",&r_node)>0)
						{
							tree_mamanger_get_node_signed_dword	(&r_node,0 ,&txt_rect.pos[0]);
							tree_mamanger_get_node_signed_dword	(&r_node,4 ,&txt_rect.pos[1]);
							tree_mamanger_get_node_dword		(&r_node,8 ,&txt_rect.size[0]);
							tree_mamanger_get_node_dword		(&r_node,12,&txt_rect.size[1]);
							release_zone_ref					(&r_node);

							node_min_x	=	txt_rect.pos[0];
							node_min_y	=	txt_rect.pos[1];

							node_max_x	=	node_min_x+(txt_rect.size[0]);
							node_max_y	=	node_min_y+(txt_rect.size[1]);

							size[0]		=	out_rect->size[0];
							size[1]		=	out_rect->size[1];


							if(node_max_x>size[0])out_rect->size[0]=node_max_x;
							if(node_max_y>size[1])out_rect->size[1]=node_max_y;

							if(node_min_x<out_rect->pos[0])out_rect->pos[0]=node_min_x;
							if(node_min_y<out_rect->pos[1])out_rect->pos[1]=node_min_y;
						}
					}
					release_zone_ref	(&node);
					nn++;
				}

			break;
			case NODE_GFX_IMAGE_OBJ:
				tree_manager_get_child_value_si32		(obj_node,NODE_HASH("pos_x"),&pos_x);
				tree_manager_get_child_value_si32		(obj_node,NODE_HASH("pos_y"),&pos_y);

				tree_manager_get_child_value_i32		(obj_node,NODE_HASH("width"),&img_width);
				tree_manager_get_child_value_i32		(obj_node,NODE_HASH("height"),&img_height);

				node_min_x	=	pos_x;
				node_min_y	=	pos_y;

				node_max_x	=	node_min_x+img_width;
				node_max_y	=	node_min_y+img_height;

				size[0]		=	out_rect->size[0];
				size[1]		=	out_rect->size[1];

				if(node_max_x>size[0])out_rect->size[0]=node_max_x;
				if(node_max_y>size[1])out_rect->size[1]=node_max_y;

				if(node_min_x<out_rect->pos[0])out_rect->pos[0]=node_min_x;
				if(node_min_y<out_rect->pos[1])out_rect->pos[1]=node_min_y;
			break;
		}

		tree_manager_get_next_child	(&scene_list,&obj_node);
		n++;
	}

	return 1;

}
