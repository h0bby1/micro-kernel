//	VirtualDub - Video processing and capture application
//	Copyright (C) 1998-2001 Avery Lee
//
//	This program is free software; you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation; either version 2 of the License, or
//	(at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program; if not, write to the Free Software
//	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

#ifndef f_VIRTUALDUB_VBITMAP_H
#define f_VIRTUALDUB_VBITMAP_H


typedef unsigned long	Pixel;
typedef unsigned long	Pixel32;
typedef unsigned char	Pixel8;
typedef long			PixCoord;
typedef	long			PixDim;
typedef	long			PixOffset;

struct VBitmap 
{
	Pixel *			data;
	Pixel *			palette;
	int				depth;
	PixCoord		w, h;
	PixOffset		pitch;
	PixOffset		modulo;
	PixOffset		size;
	PixOffset		offset;
};


union vd_quad
{
	unsigned long long  v;
	struct 
	{
		unsigned long lo;
		long hi;
	}_v;
} ;

struct ResampleInfo
{

	//public:
	#ifdef _WIN32
		union 
		{
			__int64 v;
			struct 
			{
				unsigned long lo;
				long hi;
			};
		} dudx_int, u0_int;
	#else
		union vd_quad dudx_int;
		union vd_quad u0_int;

		
	#endif	

	long x1_int, dx_int;

	struct ResampleBounds 
	{

		long precopy, preclip, postclip, postcopy, unclipped, allclip;
		long preclip2, postclip2;	// cubic4 only
	} clip;
};


#endif
