

#define VDASSUME(exp)
#define VDASSERT(exp)		if (static bool active = true) if (exp); else switch(VDAssert   (#exp, __FILE__, __LINE__)) { case kVDAssertBreak: VDBREAK; break; case kVDAssertIgnore: active = false; } else
#define VDASSERTPTR(exp) 	if (static bool active = true) if (exp); else switch(VDAssertPtr(#exp, __FILE__, __LINE__)) { case kVDAssertBreak: VDBREAK; break; case kVDAssertIgnore: active = false; } else
#define VDVERIFY(exp)		if (exp); else if (static bool active = true) switch(VDAssert   (#exp, __FILE__, __LINE__)) { case kVDAssertBreak: VDBREAK; break; case kVDAssertIgnore: active = false; } else
#define VDVERIFYPTR(exp) 	if (exp); else if (static bool active = true) switch(VDAssertPtr(#exp, __FILE__, __LINE__)) { case kVDAssertBreak: VDBREAK; break; case kVDAssertIgnore: active = false; } else
#define VDASSERTCT(exp)		(void)sizeof(int[(exp)?1:-1])

#define NEVER_HERE			do { if (VDAssert( "[never here]", __FILE__, __LINE__ )) VDBREAK; __assume(false); } while(false)
#define	VDNEVERHERE			do { if (VDAssert( "[never here]", __FILE__, __LINE__ )) VDBREAK; __assume(false); } while(false)

#if defined(_MSC_VER)
#define VDBREAK		__asm { int 3 }
#elif defined(__GNUC__)
#define VDBREAK		__asm__ volatile ("int3" : : )
#else
#define VDBREAK		*(volatile char *)0 = *(volatile char *)0
#endif
typedef enum {
	kVDAssertBreak,
	kVDAssertContinue,
	kVDAssertIgnore
}VDAssertResult ;

extern VDAssertResult VDAssertPtr(const char *exp, const char *file, int line) ;
extern VDAssertResult VDAssert(const char *exp, const char *file, int line) ;
extern int VDRoundToInt(double x);