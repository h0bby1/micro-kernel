#ifndef FILTER_API	
#define FILTER_API	C_IMPORT
#endif

#include "VBitmap.h"
#include "resample.h"

typedef int64_t				sint64;
typedef uint64_t			uint64;



typedef signed int			sint32;
//typedef unsigned int		uint32;
typedef signed short		sint16;
typedef unsigned short		uint16;
typedef signed char			sint8;
typedef unsigned char		uint8;

typedef sint64				int64;
//typedef sint32			int32;
typedef sint16				int16;
typedef sint8				int8;


enum {
		LinearInterp	= 1,
		CubicInterp	= 2,
		CubicInterp060	= 7,
		LinearDecimate	= 3,
		CubicDecimate075	= 4,
		CubicDecimate060	= 5,
		CubicDecimate100	= 6,
		Lanczos3		= 8
};

struct ResampleFilterData 
{
	unsigned long in_width, in_height;
	unsigned long new_x, new_y;
	unsigned long max_x, max_y;
	long d_x, d_y;
	long s_x, s_y;
	int filter_mode;
	struct VBitmap *src;
	struct VBitmap *dst;
	mem_zone_ref   resampler_ref;
	//Resampler *resampler;
} ;

FILTER_API int  			C_API_FUNC resize_free			(struct ResampleFilterData *fd);
FILTER_API int  			C_API_FUNC resize_start			(struct ResampleFilterData *fd);
FILTER_API long 			C_API_FUNC resize_param			(struct ResampleFilterData *fd);
FILTER_API int  			C_API_FUNC resize_run			(struct ResampleFilterData *fd,unsigned int *out_buffer) ;
FILTER_API void 			C_API_FUNC bmp_init				(struct VBitmap *bmp,void *data, PixDim w, PixDim pitch, PixDim h, int depth);
FILTER_API void 			C_API_FUNC simple_resize		(struct ResampleFilterData *fd,unsigned char *color);
FILTER_API void 			C_API_FUNC init_resampler		(mem_zone_ref	*resampler_ref);
FILTER_API void 			C_API_FUNC simple_resize_to_text(struct ResampleFilterData *fd,unsigned char *color,const unsigned char *color_palette);
FILTER_API void				C_API_FUNC simple_resize_to_mono(struct ResampleFilterData *fd);
FILTER_API unsigned char 	C_API_FUNC palette_lookup		(const unsigned char *color_palette,unsigned char r,unsigned char g,unsigned char b);
FILTER_API int			  	C_API_FUNC setup_clipped_resize	(struct ResampleFilterData	*fd,const struct gfx_rect *clip_rect,const struct gfx_rect *dst_rect,const struct gfx_rect *src_rect,struct VBitmap *ImageClip,unsigned int image_pitch,mem_zone_ref *image_data);
FILTER_API void		  		C_API_FUNC do_clip				(const struct gfx_rect *clip_rect,const vec_2s_t d_pos,const struct gfx_rect *src_rect,struct gfx_rect *new_rect);

