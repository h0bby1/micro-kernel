//
//	VirtualDub - Video processing and capture application
//	Copyright (C) 1998-2001 Avery Lee
//
//	This program is free software; you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation; either version 2 of the License, or
//	(at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//clip->Video.resdata.dst->data=(unsigned long *)out_buffer;
//	You should have received a copy of the GNU General Public License
//	along with this program; if not, write to the Free Software
//	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.


#define FILTER_API C_EXPORT
#include <std_def.h>
#include "kern.h"
#include <std_mem.h>
#include "mem_base.h"
#include "lib_c.h"
#include "resample.h"
#include "VBitmap.h"
#include "cpuaccel.h"
#include "filters.h"
#include "vdtypes.h"
#include "gfx/graphic_object.h"

int cubic4_tbl[256*4*2]={0xFF};



///////////////////////////////////////////////////////////////////////////
int MMX_enabled=1;

static __inline void stats_print() {}

#define PROFILE_START
#define PROFILE_ADD(x)

///////////////////////////////////////////////////////////////////////////

OS_API_XTRN_ASM_FUNC(void) asm_resize_nearest(Pixel32 *dst,
		const Pixel32 *src,
		long width,
		PixDim height,
		PixOffset dstpitch,
		PixOffset srcpitch,
		unsigned long xaccum,
		unsigned long yaccum,
		unsigned long xfrac,
		unsigned long yfrac,
		long xistep,
		PixOffset yistep,
		const Pixel32 *precopysrc,
		unsigned long precopy,
		const Pixel32 *postcopysrc,
		unsigned long postcopy);

OS_API_XTRN_ASM_FUNC(void) asm_resize_bilinear(
		void *dst,
		void *src,
		long w,
		PixDim h,
		PixOffset dstpitch,
		PixOffset srcpitch,
		unsigned long xaccum,
		unsigned long yaccum,
		unsigned long xfrac,
		unsigned long yfrac,
		long xistep,
		PixOffset yistep,
		Pixel32 *precopysrc,
		unsigned long precopy,
		Pixel32 *postcopysrc,
		unsigned long postcopy);

void asm_resize_interp_row_run(
			void *dst,
			const void *src,
			unsigned long width,
			__int64 xaccum,
			__int64 x_inc);

void asm_resize_interp_col_run(
			void *dst,
			const void *src1,
			const void *src2,
			unsigned long width,
			unsigned long yaccum);

void asm_resize_ccint(Pixel *dst, const Pixel *src1, const Pixel *src2, const Pixel *src3, const Pixel *src4, long count, long xaccum, long xint, const int *table);
void asm_resize_ccint_col(Pixel *dst, const Pixel *src1, const Pixel *src2, const Pixel *src3, const Pixel *src4, long count, const int *table);
void asm_resize_ccint_col_MMX(Pixel *dst, const Pixel *src1, const Pixel *src2, const Pixel *src3, const Pixel *src4, long count, const int *table);
void asm_resize_ccint_col_SSE2(Pixel *dst, const Pixel *src1, const Pixel *src2, const Pixel *src3, const Pixel *src4, long count, const int *table);

long resize_table_col_MMX(Pixel *out, const Pixel *const*in_table, const int *filter, int filter_width, PixDim w, long frac);
long resize_table_col_by2linear_MMX(Pixel *out, const Pixel *const*in_table, PixDim w);
long resize_table_col_by2cubic_MMX(Pixel *out, const Pixel *const*in_table, PixDim w);


 __inline mem_ptr bmp_Address32i(const struct VBitmap *bmp,PixCoord x, PixCoord y) {

	return (mem_add(bmp->data , y*bmp->pitch + ((x*bmp->depth)/8)));
}
 __inline mem_ptr bmp_Address32 (const struct VBitmap *bmp,PixCoord x, PixCoord y){
	return bmp_Address32i(bmp,x, bmp->h-y-1);
}

 PixOffset  bmp_PitchAlign8(struct VBitmap *bmp) {
	return ((bmp->w * bmp->depth + 63)/64)*8;
}

 PixOffset  bmp_Modulo(struct VBitmap *bmp) {
	return bmp->pitch - (bmp->w*bmp->depth+7)/8;
}

 PixOffset  bmp_Size(struct VBitmap *bmp) {
	return bmp->pitch*bmp->h;
}

void bmp_AlignTo8(struct VBitmap *bmp) 
{

	bmp->modulo		= bmp_Modulo(bmp);
	bmp->size		= bmp_Size(bmp);
}



int VDRoundToInt(double x) {
	return (int)floor_c(x + 0.5);
}
/*
VDAssertResult VDAssert(const char *exp, const char *file, int line) {
	char szText[1024];

	wsprintf(szText,
		"Assert failed in module %s, line %d:\n"
		"\n"
		"\t%s\n"
		"\n"
		"Break into debugger?", file, line, exp);

	switch(MessageBox(NULL, szText, "Assert failure", MB_ABORTRETRYIGNORE|MB_ICONWARNING|MB_TASKMODAL)) {
	case IDABORT:
		return kVDAssertBreak;
	case IDRETRY:
		return kVDAssertContinue;
	default:
		VDNEVERHERE;
	case IDIGNORE:
		return kVDAssertIgnore;
	}
	return kVDAssertBreak;

}

VDAssertResult VDAssertPtr(const char *exp, const char *file, int line) {
	char szText[1024];

	wsprintf(szText,
		"Assert failed in module %s, line %d:\n"
		"\n"
		"\t(%s) not a valid pointer\n"
		"\n"
		"Break into debugger?", file, line, exp);

	switch(MessageBox(NULL, szText, "Assert failure", MB_ABORTRETRYIGNORE|MB_ICONWARNING|MB_TASKMODAL)) {
	case IDABORT:
		return kVDAssertBreak;
	case IDRETRY:
		return kVDAssertContinue;
	default:
		VDNEVERHERE;
	case IDIGNORE:
		return kVDAssertIgnore;
	}
	return kVDAssertBreak;
}
*/
///////////////////////////////////////////////////////////////////////////

//	void MakeCubic4Table(
//		int *table,			pointer to 256x4 int array
//		double A,			'A' value - determines characteristics
//		mmx_table);			generate interleaved table
//
//	Generates a table suitable for cubic 4-point interpolation.
//
//	Each 4-int entry is a set of four coefficients for a point
//	(n/256) past y[1].  They are in /16384 units.
//
//	A = -1.0 is the original VirtualDub bicubic filter, but it tends
//	to oversharpen video, especially on rotates.  Use A = -0.75
//	for a filter that resembles Photoshop's.


void MakeCubic4Table(int *table, double A, int mmx_table) {
	int i;

	for(i=0; i<256; i++) {
		double d = (double)i / 256.0;
		int y1, y2, y3, y4, ydiff;

		// Coefficients for all four pixels *must* add up to 1.0 for
		// consistent unity gain.
		//
		// Two good values for A are -1.0 (original VirtualDub bicubic filter)
		// and -0.75 (closely matches Photoshop).

		y1 = (int)floor_c(0.5 + (        +     A*d -       2.0*A*d*d +       A*d*d*d) * 16384.0);
		y2 = (int)floor_c(0.5 + (+ 1.0             -     (A+3.0)*d*d + (A+2.0)*d*d*d) * 16384.0);
		y3 = (int)floor_c(0.5 + (        -     A*d + (2.0*A+3.0)*d*d - (A+2.0)*d*d*d) * 16384.0);
		y4 = (int)floor_c(0.5 + (                  +           A*d*d -       A*d*d*d) * 16384.0);

		// Normalize y's so they add up to 16384.

		ydiff = (16384 - y1 - y2 - y3 - y4)/4;
//		_ASSERT(ydiff > -16 && ydiff < 16);

		y1 += ydiff;
		y2 += ydiff;
		y3 += ydiff;
		y4 += ydiff;

		if (mmx_table) {
			table[i*4 + 0] = table[i*4 + 1] = (y2<<16) | (y1 & 0xffff);
			table[i*4 + 2] = table[i*4 + 3] = (y4<<16) | (y3 & 0xffff);
		} else {
			table[i*4 + 0] = y1;
			table[i*4 + 1] = y2;
			table[i*4 + 2] = y3;
			table[i*4 + 3] = y4;
		}
	}
}
const int *GetStandardCubic4Table(){
	
	
	if (cubic4_tbl[1]==0xFF) 
	{
		MakeCubic4Table(cubic4_tbl, -0.75, 0);
		MakeCubic4Table(cubic4_tbl+1024, -0.75, 1);
	}

	return cubic4_tbl;
}

const int *GetBetterCubic4Table(){

	if (cubic4_tbl[1]==0xFF) 
	{
		MakeCubic4Table(cubic4_tbl, -0.6, 0);
		MakeCubic4Table(cubic4_tbl+1024, -0.6, 1);
	}

	return cubic4_tbl;
}


///////////////////////////////////////////////////////////////////////////

static int permute_index(int a, int b) {
	return (b-(a>>8)-1) + (a&255)*b;
}

static void normalize_table(int *table, int filtwidth) {
	int i, j, v, v2;
	int64_t Long;

	for(i=0; i<256*filtwidth; i+=filtwidth) {
		v=0;
		v2=0;
		for(j=0; j<filtwidth; j++)
			v += table[i+j];

		for(j=0; j<filtwidth; j++)
		{
				Long=table[i+j];
				Long=Long*0x4000;
				Long=Long/v;
				v2 += table[i+j]=Long;
			//	v2 += table[i+j] = MulDiv(table[i+j], 0x4000, v);
		}

		v2 = 0x4000 - v2;

#if 0
		for(j=0; j<filtwidth; j++)
			_RPT3(0, "table[%04x+%02x] = %04x\n", i, j, table[i+j]);
		Sleep(1);
#endif

		if (MMX_enabled) {
			for(j=0; j<filtwidth; j+=2) {
				int a = table[i+j];
				int b = table[i+j+1];

				a = (a & 0xffff) | (b<<16);

				table[i+j+0] = a;
				table[i+j+1] = a;
			}
		}

//		_RPT2(0,"table_error[%02x] = %04x\n", i, v2);
	}
}



int *Resampler_CreateLinearDecimateTable(struct Resampler *sampler,double dx, double sx, int *filtwidth,mem_zone_ref *table_ref) { 
	double filtwidth_fracd;
	long filtwidth_frac;
	double filt_max;
	int i;
	int *table;
	//mem_zone_ref	table_ref;

	filtwidth_fracd = sx*256.0/dx;
	if (filtwidth_fracd < 256.0)
		filtwidth_fracd = 256.0;

	filtwidth_frac = (long)ceil_c(filtwidth_fracd);
	(*filtwidth) = ((filtwidth_frac + 255) >> 8)<<1;

	
	
	/*
	table=ttable;
	if(table!=NULL)free(table);
	table=(int *)malloc(256 * filtwidth* sizeof(int) );
	if (!table)return NULL;
	*/
	
	allocate_new_zone	(0,256 * (*filtwidth)* sizeof(int),table_ref);
	table=get_zone_ptr	(table_ref,0);
	

	
	table[(*filtwidth)-1] = 0;

	if (sx <= dx)
		filt_max = 16384.0;
	else
		filt_max = (dx*16384.0)/sx;

	for(i=0; i<128*(*filtwidth); i++) {
		int y = 0;
		double d = i / filtwidth_fracd;

		if (d<1.0)
			y = VDRoundToInt(filt_max*(1.0 - d));

		table[permute_index(128*(*filtwidth) + i, (*filtwidth))]
			= table[permute_index(128*(*filtwidth) - i, (*filtwidth))]
			= y;
	}

	normalize_table(table, (*filtwidth));

	return table;
}

int *Resampler_CreateCubicDecimateTable(struct Resampler *sampler,double dx, double sx, int  *filtwidth, double A,mem_zone_ref *table_ref) { 
	int i;
	long filtwidth_frac;
	double filtwidth_fracd;
	double filt_max;
	int *table;
	

	filtwidth_fracd = sx*256.0/dx;
	if (filtwidth_fracd < 256.0)
		filtwidth_fracd = 256.0;
	filtwidth_frac = (long)ceil_c(filtwidth_fracd);
	(*filtwidth) = ((filtwidth_frac + 255) >> 8)<<2;

	/*
		if(table!=NULL)free(table);
		table=(int *)malloc(256 * filtwidth* sizeof(int));
		if (!table)return NULL;
		table=ttable;
	*/

	
	
	
	allocate_new_zone(0,256 * (*filtwidth),table_ref);
	table=get_zone_ptr(table_ref,0);

	table[(*filtwidth)-1] = 0;

	if (sx <= dx)
		filt_max = 16384.0;
	else
		filt_max = (dx*16384.0)/sx;

	for(i=0; i<128*(*filtwidth); i++) {
		int y = 0;
		double d = (double)i / filtwidth_fracd;

		if (d < 1.0)
			y = (int)floor_c(0.5 + (1.0 - (A+3.0)*d*d + (A+2.0)*d*d*d) * filt_max);
		else if (d < 2.0)
			y = (int)floor_c(0.5 + (-4.0*A + 8.0*A*d - 5.0*A*d*d + A*d*d*d) * filt_max);

		table[permute_index(128*(*filtwidth) + i, (*filtwidth))]
			= table[permute_index(128*(*filtwidth) - i, (*filtwidth))]
			= y;
	}

	normalize_table(table, (*filtwidth));

	return table;
}

static __inline double sinc(double x) {
	return fabs_c(x) < 1e-9 ? 1.0 : sin_c(x) / x;
}

int *Resampler_CreateLanczos3DecimateTable(struct Resampler *sampler,double dx, double sx, int *filtwidth,mem_zone_ref	*table_ref) { 
	int i;
	long filtwidth_frac;
	double filtwidth_fracd;
	double filt_max;
	int *table;
	

	filtwidth_fracd = sx*256.0/dx;
	if (filtwidth_fracd < 256.0)
		filtwidth_fracd = 256.0;

	filtwidth_frac = (long)ceil_c(filtwidth_fracd);
	(*filtwidth) = ((filtwidth_frac + 255) >> 8)*6;
	
	
	allocate_new_zone(0,256 * (*filtwidth),table_ref);
	table=get_zone_ptr(table_ref,0);
	/*
		if(table!=NULL)free(table);
		table=(int *)malloc(256 * filtwidth* sizeof(int));
		if (!table)return NULL;
		table=ttable;
	*/ 
		
	table[(*filtwidth)-1] = 0;
	

	if (sx <= dx)
		filt_max = 16384.0;
	else
		filt_max = (dx*16384.0)/sx;

	for(i=0; i<128*(*filtwidth); i++) {
		static const double pi  = 3.1415926535897932384626433832795;	// pi
		static const double pi3 = 1.0471975511965977461542144610932;	// pi/3
		int y = 0;
		double d = (double)i / filtwidth_fracd;

		if (d < 3.0)
			y = (int)floor_c(0.5 + sinc(pi*d) * sinc(pi3*d) * filt_max);

		table[permute_index(128*(*filtwidth) + i, (*filtwidth))]
			= table[permute_index(128*(*filtwidth) - i, (*filtwidth))]
			= y;
	}

	
	normalize_table(table, (*filtwidth));

	return table;
}


static sint32 pixel_loc(sint64 u, sint64 dudx, sint32 bound) 
{
	sint32 x = (sint32)((u + (dudx-1)) / dudx);
	return x<0 ? 0 : x < bound ? x : bound;
}
static sint32 tap_location(sint64 u, sint64 dudx, sint32 tap, sint32 pixel) 
{
	return (sint32)((u + dudx * pixel)>>32) + tap;
}

void infos_computeBounds(struct ResampleInfo *infos,__int64 u, __int64 dudx, unsigned int dx, unsigned int kernel, unsigned long limit) 
{
	sint64	preclipbound;
	sint64	unclipbound	;
	sint64	postclipbound;	
	sint64	postcopybound;	

	sint32	preclippt;	
	sint32	unclippt;		
	sint32	postclippt;
	sint32	postcopypt;

	
	// clear bounds used only by 4-point special case
	infos->clip.preclip2 = infos->clip.postclip2 = 0;

	infos->clip.allclip = 0;
	infos->clip.unclipped = 0;

	// check for point-sampled case
	if (kernel == 1) 
	{
		infos->clip.preclip = infos->clip.postclip = 0;		// point-sampling never has to partially clip
		infos->clip.precopy = infos->clip.postcopy = 0;
		if (u < 0)
			infos->clip.precopy = pixel_loc(-u, dudx, dx);

		if ((u + dudx*(dx-1)) >= limit)
			infos->clip.postcopy = dx - pixel_loc(((sint64)(limit-1)<<32) - u, dudx, dx);

		infos->clip.unclipped = dx - (infos->clip.precopy + infos->clip.postcopy);
	
		return;
	}

	// Rebias U to point at first pixel of kernel rather than floor of center
	u		-= (sint64)((kernel>>1) - 1)<<32;

	// Compute bounds:
	//		precopy			left region where all taps are identical
	//		preclip			left region where some taps are clipped
	//		unclipped		all taps in range
	//		postclip		right region where some taps are clipped
	//		postcopy		right region where all taps are identical
	//
	// In rare cases:
	//		allclipped		both left and right taps are clipped

	preclipbound	= (sint64)-(kernel-2) << 32;		// first pixel at which more than one tap is in range
	unclipbound		= 0;								// first pixel at which all taps are in range
	postclipbound	= (sint64)(limit-kernel+1) << 32;	// first pixel at which at least one right tap is out of range
	postcopybound	= (sint64)(limit-1) << 32;			// first pixel at which only one tap is in range

	preclippt		= pixel_loc(preclipbound  - u, dudx, dx);
	unclippt		= pixel_loc(unclipbound   - u, dudx, dx);
	postclippt		= pixel_loc(postclipbound - u, dudx, dx);
	postcopypt		= pixel_loc(postcopybound - u, dudx, dx);

	infos->clip.precopy		= preclippt;

	// If the unclipped region is negative, it means that the filter kernel
	// cannot fit within the source at all.

	if (postclippt < unclippt) 
	{
		infos->clip.preclip		= 0;
		infos->clip.allclip		= postcopypt - preclippt;
		infos->clip.postclip		= 0;
	} 
	else 
	{
		infos->clip.preclip		= unclippt - preclippt;
		infos->clip.unclipped		= postclippt - unclippt;
		infos->clip.postclip		= postcopypt - postclippt;
	}

	infos->clip.postcopy		= dx - postcopypt;

}

void infos_computeBounds4(struct ResampleInfo *infos,__int64 u, __int64 dudx, unsigned int dx, unsigned long limit) {
	__int64 dulimit;
	// The kernel length must always be even.  The precopy region covers all
	// pixels where the kernel is completely off to the left; this occurs when
	// u < -(kernel/2-1).
	//
	// We round the halfkernel value so that things work out for point-sampling
	// kernels (k=1).

	const __int64 halfkernel = 2;
	const __int64 halfkernelm1 = 1;

	if (u < (-halfkernelm1<<32))
		infos->clip.precopy = ((-halfkernelm1<<32) - u + (dudx-1)) / dudx;
	else
		infos->clip.precopy = 0;

	// Preclip region occurs anytime the left side of the kernel is off the
	// border.  The kernel extends 1 off the left, so that's how far we
	// need to step....

	// Preclip2: [a b|c d]		pass: u >= 0.0

	if (u < (halfkernelm1<<32))
		infos->clip.preclip2 = (-u + (dudx-1)) / dudx - infos->clip.precopy;
	else
		infos->clip.preclip2 = 0;

	// Preclip: [a|b c d]		pass: u >= 1.0

	if (u < (halfkernelm1<<32))
		infos->clip.preclip = ((halfkernelm1<<32) - u + (dudx-1)) / dudx - (infos->clip.precopy+infos->clip.preclip2);
	else
		infos->clip.preclip = 0;

	// Postcopy region occurs if we step onto or past (limit).

	dulimit = u + dudx * (dx-1);

	if ((long)(dulimit>>32) >= (long)limit)
		infos->clip.postcopy = dx - ((((__int64)limit<<32) - u - 1) / dudx + 1);
	else
		infos->clip.postcopy = 0;

	// Postclip2: [a b|c d]		pass: u < limit-1

	if ((long)(dulimit>>32) >= (long)(limit-(long)halfkernelm1))
		infos->clip.postclip2 = dx - infos->clip.postcopy - ((((__int64)(limit-1)<<32) - u - 1) / dudx + 1);
	else
		infos->clip.postclip2 = 0;

	// Postclip: [a b c|d]		pass: u < limit-2

	if ((long)(dulimit>>32) >= (long)(limit-(long)halfkernelm1-1))
		infos->clip.postclip = dx - infos->clip.postcopy - infos->clip.postclip2 - ((((__int64)(limit-2)<<32) - u - 1) / dudx + 1);
	else
		infos->clip.postclip = 0;

	infos->clip.unclipped = dx - (infos->clip.precopy + infos->clip.preclip2 + infos->clip.preclip + infos->clip.postcopy + infos->clip.postclip + infos->clip.postclip2);

	infos->clip.allclip = 0;
	
	if (infos->clip.unclipped < 0) 
	{
		infos->clip.allclip = dx - (infos->clip.precopy + infos->clip.postcopy);

		infos->clip.preclip = infos->clip.preclip2 = infos->clip.postclip2 = infos->clip.postclip = 0;
		infos->clip.unclipped = 0;
	}
}
int init_sampler_infos(struct ResampleInfo *infos,double x, double dx, double u, double du, unsigned long xlimit, unsigned long ulimit, int kw, int bMapCorners, int bClip4) {
	
	double prestep;
	double dudx;
	double x_c,dx_c;
	// Undo any destination flips.
	
	
	if (dx < 0) 
	{
		x += dx;
		dx = -dx;
	}
	
	// Precondition: destination area must not be empty.
	// Compute slopes.

	
	
	if (bMapCorners) 
	{
		if (dx <= 1.0)
			return 0;
		
		dudx = (du > 1.0 ? du - 1.0 : du < 1.0 ? du + 1.0 : 0.0)  / (dx-1.0);
	} 
	else 
	{
		if (dx <= 0.0)
			return 0;
		
		dudx = du / dx;
		
		// Prestep destination pixels so that we step on pixel centers.  We're going
		// to be using Direct3D's screen-space system, where pixels sit on integer
		// coordinates and the fill-convention is top-left.  However, OpenGL's system
		// is easier to use from the client point of view, so we'll subtract 0.5 from
		// all coordinates to compensate.  This means that a 1:1 blit of an 8x8
		// rectangle should be (0,0)-(8,8) in both screen and texture space.
		
		x -= 0.5;
		u -= 0.5;
	}

	// Compute integer destination rectangle.

	
	x_c			  = ceil_c(x);
	dx_c		  = ceil_c(x + dx) - x;

	infos->x1_int = x_c;
	infos->dx_int =dx_c;

	// Clip destination rectangle.

	if (infos->x1_int<0) 
	{
		infos->dx_int -= infos->x1_int;
		infos->x1_int = 0;
	}

	if (infos->x1_int+infos->dx_int > xlimit)
		infos->dx_int = xlimit - infos->x1_int;

	if (infos->dx_int<=0)
		return 0;

	// Prestep source.

	
	

	prestep = (infos->x1_int - x) * dudx;
	u += prestep;
	du -= prestep;

	// Compute integer step values.  Rounding toward zero is usually a pretty
	// safe bet.

	infos->dudx_int.v = (__int64)(4294967296.0 * dudx);

	// Compute starting sampling coordinate.

	infos->u0_int.v = (__int64)(u*4294967296.0);

	// Compute clipping parameters.

	
	
	if (bClip4)
		infos_computeBounds4(infos,infos->u0_int.v, infos->dudx_int.v, infos->dx_int, ulimit);
	else
		infos_computeBounds(infos,infos->u0_int.v,infos-> dudx_int.v, infos->dx_int, kw, ulimit);

	// Advance source to beginning of clipped region.

	infos->u0_int.v += infos->dudx_int.v * infos->clip.precopy;

	
	

	return 1;
}

long resize_table_row_MMX(Pixel *out, const Pixel *in, const int *filter, int filter_width, PixDim w, long accum, long frac);
long resize_table_row_protected_MMX(Pixel *out, const Pixel *in, const int *filter, int filter_width, PixDim w, long accum, long frac, long limit);
long resize_table_row_by2linear_MMX(Pixel *out, const Pixel *in, PixDim w);
long resize_table_row_by2cubic_MMX(Pixel *out, const Pixel *in, PixDim w, unsigned long accum, unsigned long fstep, unsigned long istep);

void resize_table_row(Pixel *out, const Pixel *in, const int *filter, int filter_width, PixDim w, PixDim w_left, PixDim w_right, PixDim w_all, PixDim w2, long accum, long frac, int accel_code) {
	const Pixel *in0 = in;

	in -= filter_width/2 - 1;

	if (w_all > 0) 
	{
		accum = resize_table_row_protected_MMX(out, in0, filter, filter_width, w_all, accum - ((filter_width/2-1)<<16), frac, w2-1) + ((filter_width/2-1)<<16);
		out += w_all;
		return;
	}
	if (w_left > 0) 
	{
		accum = resize_table_row_protected_MMX(out, in0, filter, filter_width, w_left, accum - ((filter_width/2-1)<<16), frac, w2-1) + ((filter_width/2-1)<<16);
		out += w_left;
	}
	if (w > 0) 
	{
		accum = resize_table_row_MMX(out, in, filter, filter_width, w, accum, frac);
		out += w;
	}
	if (w_right > 0)
			resize_table_row_protected_MMX(out, in0, filter, filter_width, w_right, accum - ((filter_width/2-1)<<16), frac, w2-1);
}

///////////////////////////////////////////////////////////////////////////

void resize_table_col(Pixel *out, const Pixel *const*in_rows, int *filter, int filter_width, PixDim w, long frac, int accel_code) {
	int x;

	if (MMX_enabled) {
		PROFILE_START

/*		if (accel_code == ACCEL_BICUBICBY2)
			resize_table_col_by2cubic_MMX(out, in_rows, w);
		else if (accel_code == ACCEL_BILINEARBY2)
			resize_table_col_by2linear_MMX(out, in_rows, w);
		else*/
			resize_table_col_MMX(out, in_rows, filter, filter_width, w, frac);

		PROFILE_ADD(column)

		return;
	}

	x = 0;
	do {
		int x2, r, g, b;
		const Pixel *const *in_row;
		const int *filter2;

		x2 = filter_width;
		in_row = in_rows;
		filter2 = filter + frac*filter_width;
		r = g = b = 0;
		do {
			Pixel c;
			int a;

			c =  (*in_row++)[x];
			a = *filter2++;

			r += ((c>>16)&255) * a;
			g += ((c>> 8)&255) * a;
			b += ((c    )&255) * a;

		} while(--x2);

		r = (r + 8192)>>14;
		g = (g + 8192)>>14;
		b = (b + 8192)>>14;

		if (r<0) r=0; else if (r>255) r=255;
		if (g<0) g=0; else if (g>255) g=255;
		if (b<0) b=0; else if (b>255) b=255;

		*out++ = (r<<16) + (g<<8) + b;
	} while(++x < w);
}


#define RED(x) ((signed long)((x)>>16)&255)
#define GRN(x) ((signed long)((x)>> 8)&255)
#define BLU(x) ((signed long)(x)&255)

static __inline Pixel cc(const Pixel *yptr, const int *tbl) {
	const Pixel y1 = yptr[0];
	const Pixel y2 = yptr[1];
	const Pixel y3 = yptr[2];
	const Pixel y4 = yptr[3];
	long red, grn, blu;

	red = RED(y1)*tbl[0] + RED(y2)*tbl[1] + RED(y3)*tbl[2] + RED(y4)*tbl[3];
	grn = GRN(y1)*tbl[0] + GRN(y2)*tbl[1] + GRN(y3)*tbl[2] + GRN(y4)*tbl[3];
	blu = BLU(y1)*tbl[0] + BLU(y2)*tbl[1] + BLU(y3)*tbl[2] + BLU(y4)*tbl[3];

	if (red<0) red=0; else if (red>4194303) red=4194303;
	if (grn<0) grn=0; else if (grn>4194303) grn=4194303;
	if (blu<0) blu=0; else if (blu>4194303) blu=4194303;

	return ((red<<2) & 0xFF0000) | ((grn>>6) & 0x00FF00) | (blu>>14);
}

#undef RED
#undef GRN
#undef BLU

void cc_row(Pixel *dst, const Pixel *src, long w, long xs_left2, long xs_left, long xs_right, long xs_right2, long xaccum, long xinc, const int *table) {

	src += xaccum>>16;
	xaccum&=0xffff;

	if (xs_left2) {
		Pixel x[4] = { src[1], src[1], src[1], src[2] };

		do {
			*dst++ = cc(x, table + 4*((xaccum>>8)&0xff));

			xaccum += xinc;
			src += xaccum>>16;
			xaccum&=0xffff;
		} while(--xs_left2);
	}

	if (xs_left) {
		Pixel x[4] = { src[0], src[0], src[1], src[2] };

		do {
			*dst++ = cc(x, table + 4*((xaccum>>8)&0xff));

			xaccum += xinc;
			src += xaccum>>16;
			xaccum&=0xffff;
		} while(--xs_left);
	}

	if (!MMX_enabled) {
		do {
			*dst++ = cc(src-1, table + 4*((xaccum>>8)&0xff));

			xaccum += xinc;
			src += xaccum>>16;
			xaccum&=0xffff;
		} while(--w);
	} else {
		asm_resize_ccint(dst, src-1, src, src+1, src+2, w, xaccum, xinc, table+1024);

		dst += w;

		xaccum += xinc*w;
		src += xaccum>>16;
		xaccum &= 0xffff;
	}

	if (xs_right) do {
		Pixel x[4] = { src[-1], src[0], src[1], src[1] };

		*dst++ = cc(x, table + 4*((xaccum>>8)&0xff));

		xaccum += xinc;
		src += xaccum>>16;
		xaccum&=0xffff;
	} while(--xs_right);

	if (xs_right2) do {
		Pixel x[4] = { src[-1], src[0], src[0], src[0] };

		*dst++ = cc(x, table + 4*((xaccum>>8)&0xff));

		xaccum += xinc;
		src += xaccum>>16;
		xaccum&=0xffff;
	} while(--xs_right2);
}

void cc_row_protected(Pixel *dst, const Pixel *src_low, const Pixel *src_high, long w, long xaccum, long xinc, const int *table) {
	const Pixel32 *src = src_low + (xaccum>>16);

	xaccum&=0xffff;

	if (w) {
		do {
			Pixel32 x[4];

			if (src-1 < src_low)
				x[0] = src_low[0];
			else if (src-1 >= src_high)
				x[0] = src_high[-1];
			else
				x[0] = src[-1];

			if (src < src_low)
				x[1] = src_low[0];
			else if (src >= src_high)
				x[1] = src_high[-1];
			else
				x[1] = src[0];

			if (src+1 < src_low)
				x[2] = src_low[0];
			else if (src+1 >= src_high)
				x[2] = src_high[-1];
			else
				x[2] = src[+1];

			if (src+2 < src_low)
				x[3] = src_low[0];
			else if (src+2 >= src_high)
				x[3] = src_high[-1];
			else
				x[3] = src[+2];

			*dst++ = cc(x, table + 4*((xaccum>>8)&0xff));

			xaccum += xinc;
			src += xaccum>>16;
			xaccum&=0xffff;
		} while(--w);
	}

}

void resampler_Free(struct Resampler *sampler) 
{

	if(sampler->rowmemalloc_ref.zone !=PTR_NULL)
	{
		release_zone_ref(&sampler->rowmemalloc_ref);
	}
	if(sampler->rows_ref.zone!=PTR_NULL)
	{
		release_zone_ref(&sampler->rows_ref);
	}
	if(sampler->xtable_ref.zone!=PTR_NULL) 
	{
		release_zone_ref(&sampler->xtable_ref);
	}
	if(sampler->ytable_ref.zone!=PTR_NULL) 
	{
		release_zone_ref(&sampler->ytable_ref);
	}
	
}

void resampler_Init(struct Resampler *sampler,eFilter horiz_filt, eFilter vert_filt, double dx, double dy, double sx, double sy) {

	// Delete any previous allocations.

	resampler_Free(sampler);

	sampler->srcw = sx;
	sampler->srch = sy;
	sampler->dstw = dx;
	sampler->dsth = dy;

	/*
	writestr("init resampler ");
	writeint(dx,10);
	writestr(" ");
	writeint(sampler->dstw,10);
	writestr("\n");
	*/

	// Compute number of rows we need to store.
	//
	// point:			none.
	// linearinterp:	2 if horiz != linearinterp
	// cubicinterp:		4
	// lineardecimate:	variable
	// cubicdecimate:	variable
	//
	// Create tables and get interpolation values.

	sampler->rowcount = 0;
	sampler->ubias = sampler->vbias = 1.0 / 512.0;

	switch(horiz_filt) 
	{
		case kPoint:
			sampler->xfiltwidth = 1;
			sampler->ubias = 1.0 / 2.0;
		break;
		
		case kLinearDecimate:
		if (sx > dx) 
			{
				sampler->xtable = Resampler_CreateLinearDecimateTable(sampler,dx, sx, &sampler->xfiltwidth,&sampler->xtable_ref);
				break;
			}
			horiz_filt = kLinearInterp;
		
		case kLinearInterp:
			sampler->xfiltwidth = 2;
		break;
		
		case kCubicDecimate060:
		case kCubicDecimate075:
		case kCubicDecimate100:
			if (sx > dx) 
			{
				switch(horiz_filt) 
				{
					case kCubicDecimate060:
						sampler->xtable = Resampler_CreateCubicDecimateTable(sampler,dx, sx,& sampler->xfiltwidth, -0.60,&sampler->xtable_ref);
					break;
					case kCubicDecimate075:
						sampler->xtable = Resampler_CreateCubicDecimateTable(sampler,dx, sx, &sampler->xfiltwidth, -0.75,&sampler->xtable_ref);
					break;
					case kCubicDecimate100:
						sampler->xtable = Resampler_CreateCubicDecimateTable(sampler,dx, sx, &sampler->xfiltwidth, -1.00,&sampler->xtable_ref);
					break;
				}
				break;
			}
			horiz_filt = kCubicInterp060;
		
		case kCubicInterp060:
			GetBetterCubic4Table();
			sampler->xfiltwidth = 4;
		break;
		
		case kCubicInterp:
			GetStandardCubic4Table();
			sampler->xfiltwidth = 4;
		break;
	
		case kLanczos3:
			sampler->xtable = Resampler_CreateLanczos3DecimateTable(sampler,dx, sx, &sampler->xfiltwidth,&sampler->xtable_ref);
			sampler->rowcount = sampler->xfiltwidth;
		break;
	}

	switch(vert_filt) 
	{
		case kPoint:
			if (horiz_filt != kPoint)
				sampler->rowcount = 1;
			sampler->yfiltwidth = 1;
			sampler->vbias = 1.0 / 2.0;
		break;
	
		
		case kLinearDecimate:
			if (sy > dy) 
			{
				sampler->ytable = Resampler_CreateLinearDecimateTable(sampler,dy, sy, &sampler->yfiltwidth,&sampler->ytable_ref);
				sampler->rowcount = sampler->yfiltwidth;
				break;
			}
			vert_filt = kLinearInterp;
	
		case kLinearInterp:
			if (horiz_filt != kLinearInterp)
				sampler->rowcount = 2;
			else
				sampler->vbias = 1.0/32.0;
		
			sampler->yfiltwidth = 2;
		break;
				
		case kCubicDecimate060:
		case kCubicDecimate075:
		case kCubicDecimate100:
		if (sy > dy) 
		{
			switch(vert_filt) 
			{
				case kCubicDecimate060:
					sampler->ytable = Resampler_CreateCubicDecimateTable(sampler,dy, sy, &sampler->yfiltwidth, -0.60,&sampler->ytable_ref);
				break;
				case kCubicDecimate075:
					sampler->ytable = Resampler_CreateCubicDecimateTable(sampler,dy, sy, &sampler->yfiltwidth, -0.75,&sampler->ytable_ref);
				break;
				case kCubicDecimate100:
					sampler->ytable = Resampler_CreateCubicDecimateTable(sampler,dy, sy, &sampler->yfiltwidth, -1.00,&sampler->ytable_ref);
				break;
			}
			sampler->rowcount = sampler->yfiltwidth;
			break;
		}
		vert_filt = kCubicInterp060;
	
		case kCubicInterp060:
			GetBetterCubic4Table();
			sampler->yfiltwidth = 4;
			sampler->rowcount = 4;
		break;
		
		case kCubicInterp:
			GetStandardCubic4Table();
			sampler->yfiltwidth = 4;
			sampler->rowcount = 4;
		break;
		
		case kLanczos3:
			sampler->ytable = Resampler_CreateLanczos3DecimateTable(sampler,dy, sy, &sampler->yfiltwidth,&sampler->ytable_ref);
			sampler->rowcount = sampler->yfiltwidth;
		break;
	}

	// Allocate the rows.

	if (sampler->rowcount) 
	{
		sampler->rowpitch = ((int)ceil_c(dx)+1)&~1;

		sampler->rowmemalloc_ref.zone=PTR_NULL;
		sampler->rows_ref.zone=PTR_NULL;

		allocate_new_zone(0,(sampler->rowpitch * sampler->rowcount+2)*sizeof(Pixel32),&sampler->rowmemalloc_ref);
		allocate_new_zone(0,(sampler->rowcount * 2)*sizeof(Pixel32*),&sampler->rows_ref);
		
		sampler->rowmemalloc =get_zone_ptr(&sampler->rowmemalloc_ref,0);
		
		
		//sampler->rowmemalloc = new Pixel32[sampler->rowpitch * sampler->rowcount+2];
		sampler->rowmem = (Pixel32 *)uint_to_mem(((mem_to_uint(sampler->rowmemalloc)+7)&~7));
		sampler->rows	=	get_zone_ptr(&sampler->rows_ref,0);
		//sampler->rows= new Pixel32*[sampler->rowcount * 2];
		
/*
		if(rowmemalloc==NULL)
		{
			rowmemalloc = (Pixel32 *)malloc((rowpitch * rowcount+2)*sizeof(Pixel32));
			rowmem      = (Pixel32 *)(((long)rowmemalloc+7)&~7);
		}
		if(rows==NULL)
			rows= (Pixel32 **)malloc(rowcount * 2 * sizeof(Pixel32 *));
*/

	}
	sampler->nHorizFilt = horiz_filt;
	sampler->nVertFilt = vert_filt;
}

void Resampler_DoRow(struct Resampler *sampler,Pixel32 *dstp, const Pixel32 *srcp, long srcw) 
{
	int x;
	for(x=0; x < sampler->horiz.clip.precopy; ++x)
		dstp[x] = srcp[0];

	
	if (sampler->xtable)
		resize_table_row(
			dstp + sampler->horiz.clip.precopy,
			srcp,
			sampler->xtable,
			sampler->xfiltwidth,
			sampler->horiz.clip.unclipped,
			sampler->horiz.clip.preclip,
			sampler->horiz.clip.postclip,
			sampler->horiz.clip.allclip,
			srcw,
			(long)(sampler->horiz.u0_int.v >> 16),
			(long)(sampler->horiz.dudx_int.v >> 16),
			0);
	else
	switch(sampler->nHorizFilt) {
		case kPoint:
			asm_resize_nearest(
					dstp +sampler-> horiz.clip.precopy + sampler->horiz.clip.unclipped,			// destination pointer, right side
					srcp + sampler->horiz.u0_int.hi,
					-sampler->horiz.clip.unclipped*4,		// -width*4
					1,								// height
					0,								// dstpitch
					0,								// srcpitch
					sampler->horiz.u0_int.lo,				// xaccum
					0,								// yaccum
					sampler->horiz.dudx_int.lo,				// xfrac
					0,								// yfrac
					sampler->horiz.dudx_int.hi,				// xinc
					0,								// yinc
					srcp,
					sampler->horiz.clip.precopy,				// precopy
					srcp + srcw - 1,
					sampler->horiz.clip.postcopy				// postcopy
					);
			break;

		case kLinearInterp:

			asm_resize_interp_row_run(
				dstp + sampler->horiz.clip.precopy,
				srcp,
				sampler->horiz.clip.unclipped,
				sampler->horiz.u0_int.v,
				sampler->horiz.dudx_int.v);
			break;

		case kCubicInterp:
			if (sampler->horiz.clip.allclip)
				cc_row_protected(dstp + sampler->horiz.clip.precopy,
					srcp, srcp+srcw,
					sampler->horiz.clip.allclip,
					(long)(sampler->horiz.u0_int.v >> 16),
					(long)(sampler->horiz.dudx_int.v >> 16),
					GetStandardCubic4Table());
			else
				cc_row(dstp + sampler->horiz.clip.precopy,
					srcp,
					sampler->horiz.clip.unclipped,
					sampler->horiz.clip.preclip2,
					sampler->horiz.clip.preclip,
					sampler->horiz.clip.postclip,
					sampler->horiz.clip.postclip2,
					(long)(sampler->horiz.u0_int.v >> 16),
					(long)(sampler->horiz.dudx_int.v >> 16),
					GetStandardCubic4Table());
			break;
		case kCubicInterp060:
			if (sampler->horiz.clip.allclip)
				cc_row_protected(dstp + sampler->horiz.clip.precopy,
					srcp, srcp+srcw,
					sampler->horiz.clip.allclip,
					(long)(sampler->horiz.u0_int.v >> 16),
					(long)(sampler->horiz.dudx_int.v >> 16),
					GetBetterCubic4Table());
			else
				cc_row(dstp + sampler->horiz.clip.precopy,
					srcp,
					sampler->horiz.clip.unclipped,
					sampler->horiz.clip.preclip2,
					sampler->horiz.clip.preclip,
					sampler->horiz.clip.postclip,
					sampler->horiz.clip.postclip2,
					(long)(sampler->horiz.u0_int.v >> 16),
					(long)(sampler->horiz.dudx_int.v >> 16),
					GetBetterCubic4Table());
			break;
	};

	for(x=0; x < sampler->horiz.clip.postcopy; ++x)
		dstp[sampler->horiz.dx_int - sampler->horiz.clip.postcopy + x] = srcp[srcw-1];
}

unsigned int kernel_log_id=1;
int resampler_Process(struct Resampler *sampler,const struct VBitmap *dst, double _x2, double _y2, const struct VBitmap *src, double _x1, double _y1, int bMapCorners){

Pixel32 *dstp ;
Pixel32 *srcp;
long dx ;
long dy ;
int xprecopy;
int xpreclip2;	
int xpreclip1;	
int xunclipped;	
int xpostclip1;	
int xpostclip2;	
int xpostcopy;	
int xrightborder;

int yprecopy;
int yinterp	;	
int ypostcopy;	
int i, y;
__int64	xaccum;	
__int64	xinc;	
__int64 yaccum	;
__int64 yinc	;
int rowlastline ;
int pos;


	// No format conversions!!
	if (src->depth != dst->depth)
		return 0;

	// Right now, only do 32-bit stretch.  (24-bit is a pain, 16-bit is slow.)

	if (dst->depth != 32)
		return 0;

	// Compute clipping parameters.


	if (!init_sampler_infos(&sampler->horiz,_x2, sampler->dstw, _x1 + sampler->ubias, sampler->srcw, dst->w, src->w, sampler->xfiltwidth, bMapCorners, sampler->nHorizFilt == kCubicInterp || sampler->nHorizFilt == kCubicInterp060))
		return 0;

	

	if (!init_sampler_infos(&sampler->vert,_y2, sampler->dsth, _y1 + sampler->vbias, sampler->srch, dst->h, src->h, sampler->yfiltwidth, bMapCorners, 0))
		return 0;

	dx = sampler->horiz.dx_int;
	dy = sampler->vert.dx_int;

	// Call texturing routine.

	
	set_mmx_protect_c(1);

	dstp			= bmp_Address32(dst,sampler->horiz.x1_int, sampler->vert.x1_int);
	

	xprecopy	= sampler->horiz.clip.precopy;
	xpreclip2	= sampler->horiz.clip.preclip2;
	xpreclip1	= sampler->horiz.clip.preclip;
	xunclipped	= sampler->horiz.clip.unclipped;
	xpostclip1	= sampler->horiz.clip.postclip;
	xpostclip2	= sampler->horiz.clip.postclip2;
	xpostcopy	= sampler->horiz.clip.postcopy;
	xrightborder= xprecopy+xpreclip2+xpreclip1+xunclipped+xpostclip1+xpostclip2;

	yprecopy	= sampler->vert.clip.precopy;
	yinterp		= sampler->vert.clip.preclip + sampler->vert.clip.unclipped + sampler->vert.clip.postclip + sampler->vert.clip.allclip;
	ypostcopy	= sampler->vert.clip.postcopy;

	xaccum	= sampler->horiz.u0_int.v;
	xinc	= sampler->horiz.dudx_int.v;
	yaccum	= sampler->vert.u0_int.v;
	yinc	= sampler->vert.dudx_int.v;
	rowlastline = -1;
	

	
	if (sampler->vert.clip.precopy || sampler->vert.clip.preclip || sampler->vert.clip.allclip) {
		srcp = bmp_Address32(src,0, 0);

		if (sampler->rows) {
			Resampler_DoRow(sampler,sampler->rowmem, srcp, src->w);

			for(i=0; i<sampler->rowcount*2; ++i)
				sampler->rows[i] = sampler->rowmem;

			rowlastline = 0;

			if (sampler->vert.clip.precopy) {
				for(y=0; y<yprecopy; ++y) {
					memcpy_c(dstp, sampler->rowmem, 4*dx);

					dstp = (Pixel32 *)((char *)dstp - dst->pitch);
				}
			}
		} else {
			Pixel32 *dstp0 = dstp;

			Resampler_DoRow(sampler,dstp0, srcp, src->w);

			dstp = (Pixel32 *)((char *)dstp - dst->pitch);

			if (sampler->vert.clip.precopy) {
				for(y=1; y<yprecopy; ++y) {
					memcpy_c(dstp, dstp0, 4*dx);

					dstp = (Pixel32 *)((char *)dstp - dst->pitch);
				}
			}
		}
	}

	
	if (sampler->nHorizFilt == sampler->nVertFilt && sampler->nHorizFilt == kPoint) {
		/*
		kernel_log	(kernel_log_id,"resize  5.1 ");
		writeint	(sampler->horiz.clip.unclipped,10);
		writestr	(" ");
		writeint	(sampler->horiz.x1_int,10);
		writestr	(" ");
		writeint	(src->pitch,10);
		writestr	(" ");
		writeint	(dst->pitch,10);
		writestr	(" ");
		writeint	(xpostcopy,10);
		writestr	(" ");
		writeint	(sampler->horiz.clip.precopy,10);	
		writestr	(" ");
		writeint	(sampler->vert.clip.precopy,10);
		writestr	(" ");
		writeptr	(bmp_Address32(src,sampler->horiz.u0_int.hi, sampler->vert.u0_int.hi));
		writestr	(" ");
		writeptr	(asm_resize_nearest);
		writestr	("\n");
		*/
		

		asm_resize_nearest(	bmp_Address32(dst,sampler->horiz.x1_int + sampler->horiz.clip.precopy + sampler->horiz.clip.unclipped, sampler->vert.x1_int + sampler->vert.clip.precopy),			// destination pointer, right side
				bmp_Address32(src,sampler->horiz.u0_int.hi, sampler->vert.u0_int.hi),
				-sampler->horiz.clip.unclipped*4,		// -width*4
				sampler->vert.clip.unclipped,			// height
				-dst->pitch,					// dstpitch
				-src->pitch,					// srcpitch
				sampler->horiz.u0_int.lo,				// xacbmp_Address32cum
				sampler->vert.u0_int.lo,					// yaccum
				sampler->horiz.dudx_int.lo,				// xfrac
				sampler->vert.dudx_int.lo,				// yfrac
				sampler->horiz.dudx_int.hi,				// xinc
				-sampler->vert.dudx_int.hi * src->pitch,	// yinc
				bmp_Address32(src,0, sampler->vert.u0_int.hi),
				xprecopy,						// precopy
				bmp_Address32(src,src->w-1, sampler->vert.u0_int.hi),
				xpostcopy						// postcopy
				);
				

		dstp = (Pixel32 *)((char *)dstp - dst->pitch * yinterp);

	} else if (sampler->nHorizFilt == sampler->nVertFilt && sampler->nHorizFilt == kLinearInterp) {
		
		asm_resize_bilinear(
				bmp_Address32(dst,sampler->horiz.x1_int + xprecopy + sampler->horiz.clip.unclipped, sampler->vert.x1_int + yprecopy),			// destination pointer, right side
				bmp_Address32(src,sampler->horiz.u0_int.hi, sampler->vert.u0_int.hi),
				-sampler->horiz.clip.unclipped*4,		// -width*4
				sampler->vert.clip.unclipped,			// height
				-dst->pitch,					// dstpitch
				-src->pitch,					// srcpitch
				sampler->horiz.u0_int.lo,				// xaccum
				sampler->vert.u0_int.lo,					// yaccum
				sampler->horiz.dudx_int.lo,				// xfrac
				sampler->vert.dudx_int.lo,				// yfrac
				sampler->horiz.dudx_int.hi,				// xinc
				-sampler->vert.dudx_int.hi * src->pitch,	// yinc
				bmp_Address32(src,0, sampler->vert.u0_int.hi),
				-xprecopy*4,				// precopy
				bmp_Address32(src,src->w-1, sampler->vert.u0_int.hi),
				-xpostcopy*4			// postcopy
				);

		dstp = (Pixel32 *)((char *)dstp - dst->pitch * yinterp);
	} else
	{
		kernel_log	(kernel_log_id,"resize  5.3 \n");
		for(y=0; y<yinterp; ++y) {
			long lastline = (long)(yaccum >> 32) + sampler->rowcount/2;

			if (rowlastline < lastline) {
				int delta;
				int pos;

				if (rowlastline < lastline-sampler->rowcount)
					rowlastline = lastline-sampler->rowcount;

				delta = rowlastline - lastline;

				srcp = bmp_Address32(src,0, rowlastline+1);
				
				if(sampler->rowcount==0)return 0;
				pos = rowlastline % sampler->rowcount;

				do {
					Pixel32 *row;

					++rowlastline;
					if (++pos >= sampler->rowcount)
						pos = 0;

					if (rowlastline >= src->h) {
						sampler->rows[pos] = sampler->rows[pos+sampler->rowcount] = sampler->rows[(pos+sampler->rowcount-1)%sampler->rowcount];
						continue;
					}
					if(!sampler->rows)return 0; 
					sampler->rows[pos] = sampler->rows[pos+sampler->rowcount] = sampler->rowmem + sampler->rowpitch * pos;
					row = sampler->rows[pos];

					Resampler_DoRow(sampler,row, srcp, src->w);

					srcp = (Pixel32 *)((char *)srcp - src->pitch);
				} while(++delta);

				rowlastline = lastline;
			}

			if(!sampler->rowcount)return 0;
			pos = (rowlastline+1) % sampler->rowcount;

			if (sampler->ytable)
				resize_table_col(dstp, sampler->rows+pos, sampler->ytable, sampler->yfiltwidth, dx, ((unsigned long)yaccum>>24), 0);
			else switch(sampler->nVertFilt) {
				case kPoint:
					memcpy_c(dstp, sampler->rows[pos], dx*4);
					break;
				case kLinearInterp:
					asm_resize_interp_col_run(
							dstp,
							sampler->rows[pos],
							sampler->rows[pos+1],
							dx,
							(unsigned long)yaccum >> 16);
					break;
				case kCubicInterp:
					if (MMX_enabled)
						asm_resize_ccint_col_MMX(dstp, sampler->rows[pos], sampler->rows[pos+1], sampler->rows[pos+2], sampler->rows[pos+3], dx, GetStandardCubic4Table()+1024+4*((unsigned long)yaccum>>24));
					else
						asm_resize_ccint_col(dstp, sampler->rows[pos], sampler->rows[pos+1], sampler->rows[pos+2], sampler->rows[pos+3], dx, GetStandardCubic4Table()+4*((unsigned long)yaccum>>24));
					break;
				case kCubicInterp060:
					if (MMX_enabled)
						asm_resize_ccint_col_MMX(dstp, sampler->rows[pos], sampler->rows[pos+1], sampler->rows[pos+2], sampler->rows[pos+3], dx, GetBetterCubic4Table()+1024+4*((unsigned long)yaccum>>24));
					else
						asm_resize_ccint_col(dstp, sampler->rows[pos], sampler->rows[pos+1], sampler->rows[pos+2], sampler->rows[pos+3], dx, GetBetterCubic4Table()+4*((unsigned long)yaccum>>24));
					break;
			}

			yaccum += yinc;
			dstp = (Pixel32 *)((char *)dstp - dst->pitch);
		}
	}

	/*
	writestr	("\n");
	kernel_log	(kernel_log_id,"resize  6 ");
	writeint	(sampler->vert.clip.postcopy,10);
	writestr	(" ");
	writeint	(sampler->horiz.dx_int,10);
	writestr	(" ");
	writeint	(ypostcopy,10);
	writestr	(" ");
	writeptr	(srcp);
	writestr	(" ");
	writeptr	(dstp);
	writestr	(" ");
	writeint	(dst->pitch,10);
	writestr	(" ");
	writeptr	(bmp_Address32(src,0,src->h-1));
	writestr	("\n");
	*/

	if (sampler->vert.clip.postcopy) {
			srcp = dstp;

			Resampler_DoRow(sampler,dstp, bmp_Address32(src,0,src->h-1), src->w);

			for(y=1; y<ypostcopy; ++y) {
				dstp = (Pixel32 *)((char *)dstp - dst->pitch);

				memcpy_c(dstp, srcp, 4*sampler->horiz.dx_int);
			}
	}
	
	
	set_mmx_protect_c(0);

	if (MMX_enabled)	
	#if defined(_MSC_VER)
		__asm emms;
	#else
		asm ("emms");
	#endif

	stats_print();

	return 1;
}


	OS_API_C_FUNC(int) resize_run(struct ResampleFilterData *fd,unsigned int *out_buffer) 
	{
		struct ResampleFilterData *mfd = (struct ResampleFilterData *)fd;
		struct Resampler	*resampler;
			Pixel *dst, *src;
		if(fd->new_x<16)return 1;
		if(fd->new_y<16)return 1;
		
		if(fd->dst==NULL)return 0;
		if(fd->src==NULL)return 0;

		if(fd->src==NULL)return 0;



		fd->dst->data = (Pixel *)out_buffer;
		dst			  = fd->dst->data;
		src			  = fd->src->data;

		resampler	 = (struct Resampler	*)get_zone_ptr		(&mfd->resampler_ref,0);

		resampler_Process	(resampler,fd->dst, fd->d_x, fd->d_y, fd->src,fd->s_x, fd->s_y, 0);

		return 0;
	}

	OS_API_C_FUNC(long) resize_param(struct ResampleFilterData *fd) 
	{
		eFilter fmode;	
		struct Resampler	*resampler;
		

		//fd->dst->init(out_buffer,fd->max_x,fd->max_y,32);
		
	/*	fd->dst->data=(Pixel *)out_buffer;*/
		fd->dst->w=fd->max_x;
		fd->dst->h=fd->max_y;
		fd->src->w=fd->in_width;
		fd->src->h=fd->in_height;

		
		bmp_AlignTo8(fd->dst);
		bmp_AlignTo8(fd->src);
		
		switch(fd->filter_mode) {
			case 0					:fmode = kPoint;break;
			case LinearInterp		:fmode = kLinearInterp;break;
			case CubicInterp		:fmode = kCubicInterp;break;
			case CubicInterp060		:fmode = kCubicInterp060;break;
			case LinearDecimate		:fmode = kLinearDecimate;break;
			case CubicDecimate075	:fmode = kCubicDecimate075;break;
			case CubicDecimate060	:fmode = kCubicDecimate060;break;
			case CubicDecimate100	:fmode = kCubicDecimate100;break;
			case Lanczos3	        :fmode = kLanczos3;break;
			default					:fmode = kLinearInterp;break;
		}

		
		resampler	 = (struct Resampler	*)get_zone_ptr		(&fd->resampler_ref,0);


		resampler_Init(resampler,fmode, fmode, fd->new_x, fd->new_y, fd->src->w, fd->src->h);
		
		return 1;
	}


	/*
	int resize_start(struct ResampleFilterData *fd) {
		if (fd->new_x<16 || fd->new_y<16)return 1;

		if(fd->resampler_ref.zone!=PTR_NULL)return 1;
		
		allocate_new_zone(0x0,sizeof(struct Resampler),&fd->resampler_ref);
		return 0;
	}
	*/
	OS_API_C_FUNC(int) resize_free(struct ResampleFilterData *fd) {
		struct Resampler	*resampler;

		resampler	 = (struct Resampler	*)get_zone_ptr		(&fd->resampler_ref,0);

		resampler_Free		(resampler);

		release_zone_ref	(&fd->resampler_ref);

		return 0;
	}
	OS_API_C_FUNC(void) bmp_init(struct VBitmap *bmp,void *data, PixDim w, PixDim pitch, PixDim h, int depth) 
	{
		bmp->data		= (Pixel *)data;
		bmp->palette	= NULL;
		bmp->depth		= depth;
		bmp->w			= w;
		bmp->h			= h;
		bmp->offset		= 0;
		bmp->pitch		= pitch;
		bmp_AlignTo8(bmp);
	}

	OS_API_C_FUNC(void) simple_resize	(struct ResampleFilterData *fd,unsigned char *color)
	{
		unsigned char		*dst_line_buffer;
		unsigned char		*dst_mem_buffer;
		unsigned char		*src_line_buffer;
		unsigned char		*src_mem_buffer;
		unsigned int		n_x,n_y;
		unsigned int		xerr,yerr;
		unsigned char		src_pix[4];
		unsigned char		dest_pix[4];
		unsigned char		inv_alpha;
		unsigned int		bit_pix_src,bit_pix_dst;

		dst_mem_buffer		=	bmp_Address32i(fd->dst,fd->d_x,fd->d_y);
		src_mem_buffer		=   bmp_Address32i(fd->src,fd->s_x,fd->s_y);
	

		
		yerr=0;
		n_y	=	0;
		while(n_y<fd->new_y)
		{
			dst_line_buffer		 =	dst_mem_buffer;
			src_line_buffer		 =	src_mem_buffer;
			bit_pix_src			 =	0;
			bit_pix_dst			 =	0;

			n_x	=	0;
			xerr=0;
			while(n_x<fd->new_x)
			{
				//fd->src
				if(fd->src->depth==8)
				{
					src_pix[0]	= (color[0]*(*src_line_buffer))>>8;
					src_pix[1]	= (color[1]*(*src_line_buffer))>>8;
					src_pix[2]	= (color[2]*(*src_line_buffer))>>8;
					inv_alpha	= 255-(*src_line_buffer);
				}

				if(fd->src->depth==32)
				{
					src_pix[0]	= (color[0]*(src_line_buffer[0]))>>8;
					src_pix[1]	= (color[1]*(src_line_buffer[1]))>>8;
					src_pix[2]	= (color[2]*(src_line_buffer[2]))>>8;
					inv_alpha	= 255-(src_line_buffer[3]);
				}

				if(fd->dst->depth==32)
				{
					*((unsigned int *)(dest_pix))=*((unsigned int *)(dst_line_buffer));
					dst_line_buffer[0]			=	((dest_pix[0]*inv_alpha)>>8)+src_pix[0];
					dst_line_buffer[1]			=	((dest_pix[1]*inv_alpha)>>8)+src_pix[1];
					dst_line_buffer[2]			=	((dest_pix[2]*inv_alpha)>>8)+src_pix[2];
				}

				xerr=xerr+fd->in_width;
				while(xerr>=fd->new_x)
				{
					bit_pix_src			+=	fd->src->depth;
					while(bit_pix_src>=8)
					{
						src_line_buffer++;
						bit_pix_src-=8;
					}
					xerr-=fd->new_x;
				}

				bit_pix_dst			+=	fd->dst->depth;
				while(bit_pix_dst>=8)
				{
					dst_line_buffer++;
					bit_pix_dst-=8;
				}

		
				n_x++;
			}

			yerr=yerr+fd->in_height;
			while(yerr>=fd->new_y)
			{
				src_mem_buffer=mem_add(src_mem_buffer,fd->src->pitch);
				yerr-=fd->new_y;
			}

			dst_mem_buffer=mem_add(dst_mem_buffer,fd->dst->pitch);

			n_y++;
		}
	}

	OS_API_C_FUNC(unsigned char) palette_lookup(const unsigned char *color_palette,unsigned char r,unsigned char g,unsigned char b)
	{
		int		i;
		int		color_diff[3];
		int		c_d,min_cd,sq;
		unsigned char closest;
		i=0;
		min_cd=1000000;

		closest=0xFF;

		while(i<16)
		{
			color_diff[0]		 =r-color_palette[i*3+0];
			color_diff[1]		 =g-color_palette[i*3+1];
			color_diff[2]		 =b-color_palette[i*3+2];
			sq					 =color_diff[0]*color_diff[0]+color_diff[1]*color_diff[1]+color_diff[2]*color_diff[2];
			if(sq<1)
				c_d	=0;
			else
				c_d	=isqrt(sq);


			if(c_d<=min_cd)
			{
				min_cd=c_d;
				closest=i;
			}
			i++;
		}

		return closest;
	}

	OS_API_C_FUNC(void) simple_resize_to_text	(struct ResampleFilterData *fd,unsigned char *color,const unsigned char *color_palette)
	{
		unsigned char		*dst_line_buffer;
		unsigned char		*dst_mem_buffer;
		unsigned char		*src_line_buffer;
		unsigned char		*src_mem_buffer;
		unsigned int		n_x,n_y;
		unsigned int		xerr,yerr;
		unsigned int		src_pix[4];
		unsigned char		inv_alpha,alpha;
		unsigned int		bit_pix_src,bit_pix_dst;

		dst_mem_buffer		=	bmp_Address32i(fd->dst,fd->d_x,fd->d_y);

		if(fd->new_y==1)fd->s_y=fd->in_height/2;
		if(fd->new_x==1)fd->s_x=fd->in_width/2;


		src_mem_buffer		=   bmp_Address32i(fd->src,fd->s_x,fd->s_y);
	
	
		yerr	=	0;
		n_y		=	0;
		while(n_y<fd->new_y)
		{
			dst_line_buffer		 =	dst_mem_buffer;
			src_line_buffer		 =	src_mem_buffer;
			bit_pix_src			 =	0;
			bit_pix_dst			 =	0;

			n_x	=	0;
			xerr=0;
			while(n_x<fd->new_x)
			{
				//fd->src
				if(fd->src->depth==8)
				{
					src_pix[0]	= (color[0]*(*src_line_buffer))>>8;
					src_pix[1]	= (color[1]*(*src_line_buffer))>>8;
					src_pix[2]	= (color[2]*(*src_line_buffer))>>8;
					alpha		=  (*src_line_buffer);
					inv_alpha	= 255-alpha;
				}

				if(fd->src->depth==32)
				{
					src_pix[0]	= (color[0]*(src_line_buffer[0]))>>8;
					src_pix[1]	= (color[1]*(src_line_buffer[1]))>>8;
					src_pix[2]	= (color[2]*(src_line_buffer[2]))>>8;

					alpha		= (src_line_buffer[3]);
					inv_alpha	= 255-alpha;
				}

				if((fd->new_x<4)&&(fd->new_y<4))alpha=255;
				

				if(alpha>0)
				{

					src_pix[0]		= src_pix[0];
					src_pix[1]		= src_pix[1];
					src_pix[2]		= src_pix[2];

					/*
					dest_pix[0]			=	color_palette[(dst_line_buffer[1]&0xF)*3+0];
					dest_pix[1]			=	color_palette[(dst_line_buffer[1]&0xF)*3+1];
					dest_pix[2]			=	color_palette[(dst_line_buffer[1]&0xF)*3+2];
					*/

					/*
					dest_pix[0]			=	((dest_pix[0]*inv_alpha)>>8)+src_pix[0];
					dest_pix[1]			=	((dest_pix[1]*inv_alpha)>>8)+src_pix[1];
					dest_pix[2]			=	((dest_pix[2]*inv_alpha)>>8)+src_pix[2];
					*/
					
					dst_line_buffer[0]	=	219;
					dst_line_buffer[1]	=	palette_lookup(color_palette,src_pix[0],src_pix[1],src_pix[2]);
				}


				xerr=xerr+fd->in_width;
				while(xerr>=fd->new_x)
				{
					bit_pix_src			+=	fd->src->depth;
					while(bit_pix_src>=8)
					{
						src_line_buffer++;
						bit_pix_src-=8;
					}
					xerr-=fd->new_x;
				}


				bit_pix_dst			+=	fd->dst->depth;
				while(bit_pix_dst>=8)
				{
					dst_line_buffer++;
					bit_pix_dst-=8;
				}

		
				n_x++;
			}

			yerr=yerr+fd->in_height;
			while(yerr>=fd->new_y)
			{
				src_mem_buffer=mem_add(src_mem_buffer,fd->src->pitch);
				yerr-=fd->new_y;
			}

			dst_mem_buffer=mem_add(dst_mem_buffer,fd->dst->pitch);

			n_y++;
		}
	}


	OS_API_C_FUNC(void) simple_resize_to_mono	(struct ResampleFilterData *fd)
	{
		unsigned char		*dst_line_buffer;
		unsigned char		*dst_mem_buffer;
		unsigned char		*src_line_buffer;
		unsigned char		*src_mem_buffer;
		unsigned int		n_x,n_y;
		unsigned int		xerr,yerr;
		unsigned int		src_pix[4];
		unsigned char		inv_alpha,alpha;
		unsigned int		bit_pix_src,bit_pix_dst;

		dst_mem_buffer		=	mem_add(fd->dst->data,fd->d_y*fd->dst->pitch);
		src_mem_buffer		=   bmp_Address32i(fd->src,fd->s_x,fd->s_y);
	
	
		yerr	=	0;
		n_y		=	0;
		while(n_y<fd->new_y)
		{
			dst_line_buffer		 =	dst_mem_buffer;
			src_line_buffer		 =	src_mem_buffer;
			bit_pix_src			 =	0;
			bit_pix_dst			 =	fd->d_x;

			n_x	=	0;
			xerr=0;
			while(n_x<fd->new_x)
			{
				//fd->src
				if(fd->src->depth==8)
				{
					src_pix[0]	= ((*src_line_buffer));
					src_pix[1]	= ((*src_line_buffer));
					src_pix[2]	= ((*src_line_buffer));
					alpha		=  (*src_line_buffer);
					inv_alpha	= 255-alpha;
				}

				if(fd->src->depth==32)
				{
					src_pix[0]	= ((src_line_buffer[0]));
					src_pix[1]	= ((src_line_buffer[1]));
					src_pix[2]	= ((src_line_buffer[2]));

					alpha		= (src_line_buffer[3]);
					inv_alpha	= 255-alpha;
				}


				
				if(alpha>64)
				{
					(*dst_line_buffer) |= (1<<(7-bit_pix_dst));
				}
				else
				{
					(*dst_line_buffer) &= ~(1<<(7-bit_pix_dst));
				}
				
				//(*dst_line_buffer)	=	0;


				
				xerr=xerr+fd->in_width;
				while(xerr>=fd->new_x)
				
				{
				
					bit_pix_src			+=	fd->src->depth;
					while(bit_pix_src>=8)
					{
						src_line_buffer++;
						bit_pix_src-=8;
					}

				
					xerr=xerr-fd->new_x;
				}
				

				bit_pix_dst			+=	fd->dst->depth;
				while(bit_pix_dst>=8)
				{
					dst_line_buffer--;
					bit_pix_dst-=8;
				}

		
				n_x++;
			}

			
			yerr=yerr+fd->in_height;
			while(yerr>=fd->new_y)
			{
				src_mem_buffer=mem_add(src_mem_buffer,fd->src->pitch);
				yerr=yerr-fd->new_y;
			}


			dst_mem_buffer=mem_add(dst_mem_buffer,fd->dst->pitch);

			n_y++;
		}
	}

	OS_API_C_FUNC(void) init_resampler	(mem_zone_ref	*resampler_ref)
	{
		struct Resampler	*sampler;

		allocate_new_zone	(0x0,sizeof(struct Resampler),resampler_ref);
		sampler	=	get_zone_ptr(resampler_ref,0);
		sampler->rowmemalloc_ref.zone	=PTR_NULL;
		sampler->rows_ref.zone			=PTR_NULL;
		sampler->xtable_ref.zone		=PTR_NULL;
		sampler->ytable_ref.zone		=PTR_NULL;

	}


	OS_API_C_FUNC(void) do_clip(const struct gfx_rect *clip_rect,const vec_2s_t d_pos,const struct gfx_rect *src_rect,struct gfx_rect *new_rect)
	{
		int					draw_width,draw_height;
		int					draw_x,draw_y;
		int					src_x,src_y;

		src_x		=	0;
		draw_x		=	d_pos[0];
		
		
		if(draw_x<clip_rect->pos[0])
		{
			src_x		=	clip_rect->pos[0]-draw_x;
			draw_x		=	clip_rect->pos[0];
			draw_width	=	src_rect->size[0]-src_x;
		}
		else
		{
			
			draw_width	=	src_rect->size[0];
		}

		draw_y		=	d_pos[1];
		src_y		=	0;

		if(draw_y<clip_rect->pos[1])
		{
			src_y		=	clip_rect->pos[1]-draw_y;
			draw_y		=	clip_rect->pos[1];
			draw_height	=	src_rect->size[1]-src_y;
		}
		else
		{
			
			draw_height	=	src_rect->size[1];
		}



		if( (int)(draw_x+draw_width)>=
			(clip_rect->pos[0]+clip_rect->size[0]))
		{
			draw_width	=	(clip_rect->pos[0]+clip_rect->size[0])-(draw_x+src_x);
		}

		if( (int)(draw_y+draw_height)>=
			(clip_rect->pos[1]+clip_rect->size[1]))
		{
			draw_height	=	(clip_rect->pos[1]+clip_rect->size[1])-(draw_y+src_y);
		}

		new_rect->pos[0]=draw_x;
		new_rect->pos[1]=draw_y;

		new_rect->size[0]=src_x;
		new_rect->size[1]=src_y;

	}

	OS_API_C_FUNC(int) setup_clipped_resize(struct ResampleFilterData	*fd,const struct gfx_rect *clip_rect,const struct gfx_rect *dst_rect,const struct gfx_rect *src_rect,struct VBitmap *ImageClip,unsigned int image_pitch,mem_zone_ref *image_data)
	{
		unsigned int		clipped_w,clipped_h;
		unsigned int		clipped_ofs_x,clipped_ofs_y;
		unsigned int		clip_w,clip_h;
		int					ofs_w,ofs_h;


		clipped_ofs_x				=0;
		clipped_ofs_y				=0;


		if(dst_rect->pos[0]<(clip_rect->pos[0]))
		{
			clipped_ofs_x=clip_rect->pos[0]-dst_rect->pos[0];
		}
		
		if(dst_rect->pos[1]<(clip_rect->pos[1]))
		{
			clipped_ofs_y=clip_rect->pos[1]-dst_rect->pos[1];
		}

		clipped_w					=dst_rect->size[0]-clipped_ofs_x;
		clipped_h					=dst_rect->size[1]-clipped_ofs_y;

		if((dst_rect->pos[0]+(int)clipped_ofs_x+(int)clipped_w)>(clip_rect->pos[0]+(int)clip_rect->size[0]))
		{
			clipped_w=(clip_rect->pos[0]+clip_rect->size[0])-(dst_rect->pos[0]+clipped_ofs_x)+1;
		}
		if((dst_rect->pos[1]+(int)clipped_ofs_y+(int)clipped_h)>(clip_rect->pos[1]+(int)clip_rect->size[1]))
		{
			clipped_h=(clip_rect->pos[1]+clip_rect->size[1])-(dst_rect->pos[1]+clipped_ofs_y)+1;
		}

		if(clipped_w<1)return 0;
		if(clipped_h<1)return 0;

		clip_w=((clipped_w*src_rect->size[0]))/dst_rect->size[0];
		clip_h=((clipped_h*src_rect->size[1]))/dst_rect->size[1];


		ofs_w=((clipped_ofs_x*src_rect->size[0]))/dst_rect->size[0];
		ofs_h=((clipped_ofs_y*src_rect->size[1]))/dst_rect->size[1];




		
		fd->new_x			=	clipped_w;//clipped_w;
		fd->new_y			=	clipped_h;//clipped_h;
		fd->in_width		=	clip_w;//src_rect->size[0];
		fd->in_height		=	clip_h;//src_rect->size[1];
		 
		fd->d_x				=	dst_rect->pos[0]+clipped_ofs_x;
		fd->d_y				=	dst_rect->pos[1]+clipped_ofs_y;
		//rendering_context_ptr->back_buf->height-((dst_rect->pos[1]+clipped_ofs_y)+clipped_h);
		 
		fd->s_x				=	src_rect->pos[0];//+ofs_w;
		fd->s_y				=	0;//src_rect->pos[1]+ofs_h;


		bmp_init			(ImageClip	,get_zone_ptr(image_data,(src_rect->pos[1]+ofs_h)*image_pitch+ofs_w),  fd->in_width, image_pitch, fd->in_height , 32);

		//bmp_init			(&src		,get_zone_ptr(&surface->pix_buff,0),  surface->fd.in_width, surface->scan_line, surface->fd.in_height , 32);
		return 1;

	}

