//	VirtualDub - Video processing and capture application
//	Copyright (C) 1998-2001 Avery Lee
//
//	This program is free software; you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation; either version 2 of the License, or
//	(at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program; if not, write to the Free Software
//	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

#ifndef f_RESAMPLE_H
#define f_RESAMPLE_H

#include "VBitmap.h"


void	MakeCubic4Table(int *table, double A, int mmx_table);
const int *GetStandardCubic4Table();

#ifndef _WIN32
typedef unsigned long long __int64;
#endif

typedef enum 
{
	kPoint			= 0,
	kLinearInterp	= 1,
	kCubicInterp	= 2,
	kCubicInterp060	= 7,
	kLinearDecimate	= 3,
	kCubicDecimate075	= 4,
	kCubicDecimate060	= 5,
	kCubicDecimate100	= 6,
	kLanczos3		= 8
}eFilter;

struct Resampler 
{

	mem_zone_ref	rowmemalloc_ref;
	mem_zone_ref	rows_ref;
	mem_zone_ref	xtable_ref,ytable_ref;
	Pixel32			*rowmem, *rowmemalloc, **rows;
	int				*xtable, *ytable;

	int				xfiltwidth, yfiltwidth;
	double			dstw, dsth, srcw, srch;
	double			ubias, vbias;

	int				rowpitch;
	int				rowcount;
	
	eFilter				nHorizFilt, nVertFilt;
	struct ResampleInfo	horiz, vert;
};
#endif
