#if defined(_MSC_VER)
#if !defined(AFX_STDAFX_H__CFE4C0C2_BADF_4A03_9AE3_FA776414F10B__INCLUDED_)
#define AFX_STDAFX_H__CFE4C0C2_BADF_4A03_9AE3_FA776414F10B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// stdafx.h : include file for standard system include files,
//      or project specific include files that are used frequently,
//      but are changed infrequently

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers
#ifdef PLUGGLE_ATL
#include <afxctl.h>         // MFC support for ActiveX Controls
#include <afxext.h>         // MFC extensions
#endif
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Comon Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__CFE4C0C2_BADF_4A03_9AE3_FA776414F10B__INCLUDED_)
#endif
