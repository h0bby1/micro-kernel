int					init_images					();
gfx_image_t			*gfx_get_new_image			(const char *fs_name,const char *image_path);
gfx_image_t			*gfx_find_image				(unsigned int image_id);
int					 gfx_load_png_image			(gfx_image_t	*img,mem_stream *file);