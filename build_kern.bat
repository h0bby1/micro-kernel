@echo off

SET THIS_PATH=C:\coding\devOS\tree
del %THIS_PATH%\bin\kern.img

@echo on

C:/coding/tools/code/nasm/NASM.exe -f bin -I%THIS_PATH%\include_asm\ "%THIS_PATH%/kern/kern.asm" -o "%THIS_PATH%/bin/kern.img"

pause
